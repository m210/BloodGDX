// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.dmulscale;
import static ru.m210projects.Build.Pragmas.klabs;

public class AIBAT {

    public static AISTATE batSleep;
    public static AISTATE batIdle;
    public static AISTATE batSearch;
    public static AISTATE batChase;
    public static AISTATE batTurn;
    public static AISTATE batUp;
    public static AISTATE batDown;
    public static AISTATE batRecoil;
    public static AISTATE batGoto;
    public static AISTATE batBite;
    public static AISTATE batThinkMove;
    public static AISTATE batThinkIdle;
    public static AISTATE batDodgeUp;
    public static AISTATE batDodgeDown;

    public static void Init() {
        batSleep = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                myThinkTarget(sprite, xsprite);
            }
        };
        batIdle = new AISTATE(Type.idle, 6, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                myThinkTarget(sprite, xsprite);
            }
        };
        batChase = new AISTATE(Type.other, 6, null, 0, false, true, true, batIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        batThinkMove = new AISTATE(Type.other, 6, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkMove(sprite, xsprite);
            }
        };
        batGoto = new AISTATE(Type.tgoto, 6, null, 600, false, true, true, batIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        batBite = new AISTATE(Type.attack, 7, i -> BiteCallback(i), 60, false, false, false, batThinkMove);


        batRecoil = new AISTATE(Type.recoil, 5, null, 0, false, false, false, batChase);

        batSearch = new AISTATE(Type.search, 6, null, 120, false, true, true, batIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        batUp = new AISTATE(Type.other, 6, null, 0, false, true, true, batChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveUp(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        batDown = new AISTATE(Type.other, 6, null, 60, false, true, true, batChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveDown(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        batTurn = new AISTATE(Type.other, 6, null, 60, false, true, false, batChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }
        };
        batThinkIdle = new AISTATE(Type.other, 6, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                thinkIdle(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }
        };
        batDodgeUp = new AISTATE(Type.other, 6, null, 120, false, true, false, batChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myDodgeUp(sprite, xsprite);
            }
        };
        batDodgeDown = new AISTATE(Type.other, 6, null, 120, false, true, false, batChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myDodgeDown(sprite, xsprite);
            }
        };
    }

    private static void BiteCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        if (!boardService.isValidSprite(pXSprite.getTarget()))
            throw new AssertException("isValidSprite(pXSprite.getTarget())");
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;

        int eyeAboveZ = dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight * pSprite.getYrepeat() << 2;

        int z1 = 0;
        if (pXSprite.getTarget() != -1) {
            if (IsDudeSprite(pTarget))
                z1 = dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight * pTarget.getYrepeat() << 2;
        }
        int dz = z1 - eyeAboveZ;

        actFireVector(pSprite, 0, 0, dx, dy, dz, kVectorBatBite);
    }

    private static void thinkIdle(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pSprite.getZ() - pXSprite.getTargetZ() >= 4096) {
            aiSetTarget(pXSprite, pSprite.getX(), pSprite.getY(), boardService.getSector(pSprite.getSectnum()).getCeilingz());
        } else {
            pXSprite.getDudeExtra().setActive(false);
            pSprite.setHitag(0);
            aiNewState(pSprite, pXSprite, batSleep);
        }
    }

    private static void myThinkTarget(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        DudeExtra dudeExtra = pXSprite.getDudeExtra();
        if (dudeExtra.isActive()) {
            if (dudeExtra.getThinkTime() >= 10) {
                dudeExtra.setThinkTime(0);
                pXSprite.setGoalAng(pXSprite.getGoalAng() + kAngle45);
                Vector3 kSprite = pSprite.getKSprite();
                aiSetTarget(pXSprite, (int) kSprite.x, (int) kSprite.y, (int) kSprite.z);
                aiNewState(pSprite, pXSprite, batTurn);
                return;
            } else {
                dudeExtra.setThinkTime(dudeExtra.getThinkTime()+1);
            }
        }

        if (!Chance(pDudeInfo.alertChance / 2)) return;

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            PLAYER pPlayer = gPlayer[i];

            if (pPlayer.pXsprite.getHealth() == 0 || powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0)
                continue;

            int x = pPlayer.pSprite.getX();
            int y = pPlayer.pSprite.getY();
            int z = pPlayer.pSprite.getZ();
            short nSector = pPlayer.pSprite.getSectnum();

            int dx = x - pSprite.getX();
            int dy = y - pSprite.getY();

            int dist = EngineUtils.qdist(dx, dy);

            if (dist <= pDudeInfo.seeDist || dist <= pDudeInfo.hearDist) {
                int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

                // is there a line of sight to the player?
                if (engine.cansee(x, y, z, nSector, pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                    int nAngle = EngineUtils.getAngle(dx, dy);
                    int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;

                    if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                        aiSetTarget(pXSprite, pPlayer.nSprite);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }

                    if (dist < pDudeInfo.hearDist) {
                        aiSetTarget(pXSprite, x, y, z);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }
                }
            }
        }
    }

    private static void myMoveForward(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) return;

        if (pXSprite.getTarget() == -1) pSprite.setAng((short) ((pSprite.getAng() + kAngle45) & kAngleMask));

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();

        if (Random(64) >= 32 || EngineUtils.qdist(dx, dy) > 512) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            int fvel = kFrameTicks * pDudeInfo.frontSpeed;

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            if (pXSprite.getTarget() == -1) vel += fvel;
            else vel += fvel >> 1;

            pSprite.setVelocityX(dmulscale(vel, cos, svel, sin, 30));
            pSprite.setVelocityY(dmulscale(vel, sin, -svel, cos, 30));
        }
    }

    private static void myMoveUp(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pSprite.setAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x2000) || EngineUtils.qdist(dx, dy) > 512) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * pDudeInfo.frontSpeed;

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            vel += fvel >> 1;

            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30),
            dmulscale(vel, sin, -svel, cos, 30),
            -185685);
        }
    }

    private static void myMoveDown(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pXSprite.setGoalAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x300) || EngineUtils.qdist(dx, dy) > 512) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * pDudeInfo.frontSpeed;
            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            vel += fvel >> 1;
            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30), dmulscale(vel, sin, -svel, cos, 30), 279620);
        }
    }

    public static void myDodgeUp(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        long sin = Sin(pSprite.getAng());
        long cos = Cos(pSprite.getAng());

        // find vel and svel relative to current angle
        long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
        long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

        if (pXSprite.getDodgeDir() > 0) svel += pDudeInfo.sideSpeed;
        else svel -= pDudeInfo.sideSpeed;

        // reconstruct x and y velocities
        pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30), dmulscale(vel, sin, -svel, cos, 30), -338602);
    }

    public static void myDodgeDown(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        if (pXSprite.getDodgeDir() == 0) return;

        long sin = Sin(pSprite.getAng());
        long cos = Cos(pSprite.getAng());

        // find vel and svel relative to current angle
        long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
        long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

        if (pXSprite.getDodgeDir() > 0) svel += pDudeInfo.sideSpeed;
        else svel -= pDudeInfo.sideSpeed;

        // reconstruct x and y velocities
        pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30), dmulscale(vel, sin, -svel, cos, 30), 279620);
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        myThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery)
            aiNewState(pSprite, pXSprite, batSearch);

        myThinkTarget(pSprite, pXSprite);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, batGoto);
            return;
        }

        int dx, dy, dist;

        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        if (!boardService.isValidSprite(pXSprite.getTarget()))
            throw new AssertException("isValidSprite(pXSprite.getTarget())");
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, batSearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, batSearch);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            GetSpriteExtents(pSprite);

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());
                    int floorz = engine.getflorzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY());

                    if (klabs(losAngle) < kAngle15) {
                        int targetZ = pDudeInfo.eyeHeight * pTarget.getYrepeat() << 2;
                        if (targetZ - eyeAboveZ < 0x2000 && dist < 512) {
                            aiNewState(pSprite, pXSprite, batBite);
                            return;
                        }
                        if ((targetZ - eyeAboveZ > 20480 || floorz - extents_zBot > 20480) && dist < 5120 && dist > 2048) {
                            aiNewState(pSprite, pXSprite, batDown);
                            return;
                        }

                        if (targetZ - eyeAboveZ < 12288 || floorz - extents_zBot < 12288) {
                            aiNewState(pSprite, pXSprite, batUp);
                            return;
                        }
                    }
                    return;
                }
            } else {
                aiNewState(pSprite, pXSprite, batUp);
                return;
            }
        }

        aiNewState(pSprite, pXSprite, batThinkIdle);
        pXSprite.setTarget(-1);

    }

    private static void thinkMove(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, batSearch);
            return;
        }

        int dx, dy, dist;

        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        if (!boardService.isValidSprite(pXSprite.getTarget()))
            throw new AssertException("isValidSprite(pXSprite.getTarget())");
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, batSearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, batSearch);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;
            int dz = (pDudeInfo.eyeHeight * pTarget.getYrepeat() << 2) - eyeAboveZ;

            GetSpriteExtents(pSprite);

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                aiSetTarget(pXSprite, pXSprite.getTarget());
                if (klabs(losAngle) < kAngle15) {
                    if (dist < 5120 && dist > 2048) {
                        if (dz < 8192) {
                            aiNewState(pSprite, pXSprite, batDodgeUp);
                            return;
                        }
                        if (dz > 24576) {
                            aiNewState(pSprite, pXSprite, batDodgeDown);
                            return;
                        }
                    }
                    if (dist < 6144 && dist > 3072) {
                        if (dz < 12288) {
                            aiNewState(pSprite, pXSprite, batDodgeUp);
                            return;
                        }
                        if (dz > 20480) {
                            aiNewState(pSprite, pXSprite, batDodgeDown);
                            return;
                        }
                    }
                    if (dist < 512 || dist > 5120) {
                        if (dz < 8192) {
                            aiNewState(pSprite, pXSprite, batDodgeUp);
                            return;
                        }
                    }
                }
                if (dz <= 16384) aiNewState(pSprite, pXSprite, batDodgeUp);
                else aiNewState(pSprite, pXSprite, batDodgeDown);
                return;
            }
        }

        aiNewState(pSprite, pXSprite, batGoto);
        pXSprite.setTarget(-1);
    }
}

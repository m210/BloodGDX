// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSECTOR;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.DudeInfo.gPlayerTemplate;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqFrame;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.Pragmas.*;

public class AIBEAST {

    public static AISTATE[] beastIdle = new AISTATE[2];
    public static AISTATE[] beastSearch = new AISTATE[2];
    public static AISTATE[] beastGoto = new AISTATE[2];
    public static AISTATE[] beastDodge = new AISTATE[2];
    public static AISTATE[] beastChase = new AISTATE[2];
    public static AISTATE[] beastRecoil = new AISTATE[2];
    public static AISTATE[] beastHack = new AISTATE[2];
    public static AISTATE beastRTesla;
    public static AISTATE beastStomps;
    public static AISTATE beastTransforming;
    public static AISTATE beastTransform;
    public static AISTATE beastUp;

    public static void Init() {
        beastIdle[LAND] = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        beastIdle[WATER] = new AISTATE(Type.idle, 9, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        beastChase[LAND] = new AISTATE(Type.other, 8, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        beastChase[WATER] = new AISTATE(Type.other, 9, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForwardWater(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChaseWater(sprite, xsprite);
            }
        };
        beastDodge[LAND] = new AISTATE(Type.other, 8, null, 60, false, true, false, beastChase[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        beastDodge[WATER] = new AISTATE(Type.other, 9, null, 90, false, true, false, beastChase[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        beastGoto[LAND] = new AISTATE(Type.tgoto, 8, null, 600, false, true, true, beastIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        beastGoto[WATER] = new AISTATE(Type.tgoto, 9, null, 600, false, true, true, beastIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGotoWater(sprite, xsprite);
            }
        };
        beastHack[LAND] = new AISTATE(Type.other, 6, nXSprite -> HackCallback(nXSprite), 120, false, false, false, beastChase[LAND]);
        beastHack[WATER] = new AISTATE(Type.other, 9, null, 120, false, false, true, beastChase[WATER]) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChaseWater(sprite, xsprite);
            }
        };
        beastStomps = new AISTATE(Type.other, 7, nXSprite -> StompsCallback(nXSprite), 120, false, false, false, beastChase[LAND]);
        beastSearch[LAND] = new AISTATE(Type.search, 8, null, 120, false, true, true, beastIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        beastSearch[WATER] = new AISTATE(Type.search, 9, null, 120, false, true, true, beastIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        beastRecoil[LAND] = new AISTATE(Type.other, 5, null, 0, false, false, false, beastDodge[LAND]);
        beastRecoil[WATER] = new AISTATE(Type.other, 5, null, 0, false, false, false, beastDodge[WATER]);

        beastRTesla = new AISTATE(Type.other, 4, null, 0, false, false, false, beastDodge[LAND]);

        beastTransform = new AISTATE(Type.other, -1, null, 0, true, false, false, beastIdle[LAND]) {
            @Override
            public void enter(BloodSprite sprite, XSPRITE xsprite) {
                thinkTransform(sprite, xsprite);
            }
        };
        beastTransforming = new AISTATE(Type.other, 2576, null, 0, false, false, false, beastTransform);
        beastUp = new AISTATE(Type.other, 9, null, 0, false, true, true, beastChase[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveTarget(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChaseWater(sprite, xsprite);
            }
        };
    }

    private static void myMoveTarget(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pSprite.setAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x2000) || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long vel = (long) kFrameTicks * pDudeInfo.frontSpeed;
            vel = (vel >> 1) + dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);
            int z1 = 0;
            if (pXSprite.getTarget() != -1) {
                Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
                z1 = pTarget.getZ();
                if (IsDudeSprite(pTarget)) {
                    z1 += dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight;
                }
            }
            int z2 = pSprite.getZ() + pDudeInfo.eyeHeight;

            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30),
            dmulscale(vel, sin, -svel, cos, 30),
            8L * (z1 - z2));
        }
    }

    private static void myMoveForward(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();

        if (EngineUtils.qdist(dx, dy) > 1024 || Random(64) >= 32) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            pSprite.addVelocity(mulscale(cos, pDudeInfo.frontSpeed, 30), mulscale(sin, pDudeInfo.frontSpeed, 30), 0);
        }
    }

    private static void myMoveForwardWater(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            return;
        }

        if (pXSprite.getTarget() == -1) {
            pSprite.setAng((short) ((pSprite.getAng() + kAngle45) & kAngleMask));
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();

        if (Random(64) >= 32 || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            int fvel = kFrameTicks * pDudeInfo.frontSpeed;

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            if (pXSprite.getTarget() == -1) {
                vel += fvel;
            } else {
                vel += fvel >> 2;
            }

            pSprite.setVelocityX(dmulscale(vel, cos, svel, sin, 30));
            pSprite.setVelocityY(dmulscale(vel, sin, -svel, cos, 30));
        }
    }

    private static void HackCallback(int nXSprite) {
        XSPRITE pXSprite = boardService.getXSprite(nXSprite);

        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (pXSprite.getTarget() == -1) {
            return;
        }

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = pSprite.getZ() - boardService.getSprite(pXSprite.getTarget()).getZ();

        dx += BiRandom2(4000 - 700 * pGameInfo.nDifficulty);
        dy += BiRandom2(4000 - 700 * pGameInfo.nDifficulty);

        actFireVector(pSprite, 0, 0, dx, dy, dz, 13);
        actFireVector(pSprite, 0, 0, dx, dy, dz, 13);
        actFireVector(pSprite, 0, 0, dx, dy, dz, 13);
        sfxStart3DSound(pSprite, Random(2) + 9012, -1, 0);// Attack
    }

    private static void StompsCallback(int nXSprite) {
        int nSprite = boardService.getXSprite(nXSprite).getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        int radius = 400 << 4;
        int minDamage = 2 * pGameInfo.nDifficulty + 5;
        int maxDamage = 30 * pGameInfo.nDifficulty + 25;

        gSectorExp[0] = -1;
        gWallExp[0] = -1;
        NearSectors(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), 400, gSectorExp, gSpriteExp, gWallExp);
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            int nTarget = node.getIndex();
            if (nTarget != nSprite) {
                final BloodSprite pTarget = (BloodSprite) node.get();
                if (boardService.getXSprite(pTarget.getExtra()) != null && pTarget.getLotag() != kDudeTheBeast && (pTarget.getHitag() & kAttrFree) == 0 && (gSpriteExp[pTarget.getSectnum() >> 3] & (1 << (pTarget.getSectnum() & 7))) != 0 && CheckProximity(pTarget, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), radius)) {
                    GetSpriteExtents(pSprite);

                    if ((extents_zBot - boardService.getSector(pSprite.getSectnum()).getFloorz()) == 0) {
                        int dist = EngineUtils.sqrt((pSprite.getX() - pTarget.getX()) * (pSprite.getX() - pTarget.getX()) + (pSprite.getY() - pTarget.getY()) * (pSprite.getY() - pTarget.getY()));
                        if (dist <= radius) {
                            int damage = minDamage + maxDamage;
                            if (dist != 0) {
                                damage = minDamage + maxDamage * (radius - dist) / radius;
                            }

                            if (IsPlayerSprite(pTarget)) {
                                PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
                                pPlayer.quakeTime += damage << 2;
                            }
                            actDamageSprite(nSprite, pTarget, kDamageFall, damage << 4);
                        }
                    }
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(kStatThing); node != null; node = node.getNext()) {
            final BloodSprite pTarget = (BloodSprite) node.get();
            XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());
            if (pXTarget == null) {
                continue;
            }

            if ((pTarget.getHitag() & kAttrFree) == 0 && (gSpriteExp[pTarget.getSectnum() >> 3] & (1 << (pTarget.getSectnum() & 7))) != 0 && pXTarget.getLocked() == 0 && CheckProximity(pTarget, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), radius)) {
                int dist = EngineUtils.sqrt((pSprite.getX() - pTarget.getX()) * (pSprite.getX() - pTarget.getX()) + (pSprite.getY() - pTarget.getY()) * (pSprite.getY() - pTarget.getY()));
                if (dist <= radius) {
                    int damage = minDamage + maxDamage;
                    if (dist != 0) {
                        damage = minDamage + maxDamage * (radius - dist) / radius;
                    }

                    actDamageSprite(nSprite, pTarget, kDamageFall, damage << 4);
                }
            }
        }

        sfxStart3DSound(pSprite, Random(2) + 9015, -1, 0);
    }


    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int nXSector = boardService.getSector(pSprite.getSectnum()).getExtra();

        XSECTOR pXSector = null;
        if (nXSector > 0) {
            pXSector = xsector[nXSector];
        }

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
            if (pXSector != null && pXSector.Underwater) {
                aiNewState(pSprite, pXSprite, beastSearch[WATER]);
            } else {
                aiNewState(pSprite, pXSprite, beastSearch[LAND]);
            }
        }

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGotoWater(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
            aiNewState(pSprite, pXSprite, beastSearch[WATER]);
        }

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkTransform(BloodSprite pSprite, XSPRITE pXSPrite) {
        actHealDude(pXSPrite, dudeInfo[51].startHealth, dudeInfo[51].startHealth);
        pSprite.setLotag(kDudeTheBeast);
    }

    private static void thinkChaseWater(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));
        if (pXTarget == null || pXTarget.getHealth() == 0) {
            aiNewState(pSprite, pXSprite, beastSearch[WATER]);
            return;
        }
        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, beastSearch[WATER]);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);

        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());

                    if (dist < 1024 && klabs(losAngle) < kAngle15) {
                        aiNewState(pSprite, pXSprite, beastHack[WATER]);
                        int nXSprite = pSprite.getExtra();
                        if (!IsOriginalDemo() && seqFrame(SS_SPRITE, nXSprite) == 1) {
                            HackCallback(nXSprite);
                        }

                        return;
                    }
                    aiPlaySound(pSprite, Random(2) + 9009, 1, -1);
                    aiNewState(pSprite, pXSprite, beastUp);
                }
            } else {
                aiNewState(pSprite, pXSprite, beastUp);
            }
        } else {
            aiNewState(pSprite, pXSprite, beastGoto[WATER]);
            pXSprite.setTarget(-1);
        }
    }
	
	/*
	private static void thinkAttack( BloodSprite pSprite, XSPRITE pXSprite, XSECTOR pXSector, int dist, int nAngle, int hittype )
	{
		if ( dist < 921 )
        {
			if ( klabs(nAngle) < 28 )
		    {
				if(hittype != 3 || boardService.getSprite(pHitInfo.hitsprite).type != pSprite.type) 
				{
					if(pXSector != null && pXSector.Underwater)
						aiNewState(pSprite, pXSprite, beastHack[WATER]);
					else aiNewState(pSprite, pXSprite, beastHack[LAND]);
				} else { 
					if(pXSector != null && pXSector.Underwater)
						aiNewState(pSprite, pXSprite, beastDodge[WATER]);
					else aiNewState(pSprite, pXSprite, beastDodge[LAND]);
				}
		    }
		}
	}
	*/

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        int nXSector = boardService.getSector(pSprite.getSectnum()).getExtra();

        XSECTOR pXSector = null;
        if (nXSector > 0) {
            pXSector = xsector[nXSector];
        }

        if (pXSprite.getTarget() == -1) {
            if (pXSector != null && pXSector.Underwater) {
                aiNewState(pSprite, pXSprite, beastSearch[WATER]);
            } else {
                aiNewState(pSprite, pXSprite, beastSearch[LAND]);
            }
            return;
        }

        int dx, dy, dist;

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            if (pXSector != null && pXSector.Underwater) {
                aiNewState(pSprite, pXSprite, beastSearch[WATER]);
            } else {
                aiNewState(pSprite, pXSprite, beastSearch[LAND]);
            }
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                if (pXSector != null && pXSector.Underwater) {
                    aiNewState(pSprite, pXSprite, beastSearch[WATER]);
                } else {
                    aiNewState(pSprite, pXSprite, beastSearch[LAND]);
                }
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);

        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());
                    if (dist > 0) {
                        pXSprite.getDudeExtra().setDudeSlope(divscale(pTarget.getZ() - pSprite.getZ(), dist, 10));
                    }
                    int hitType = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);

                    if (dist < 5120 && dist > 2560 && klabs(losAngle) < kAngle15 && (pTarget.getHitag() & kAttrGravity) != 0 && IsPlayerSprite(pTarget) && (Chance(0x4000)) && pXTarget.getHealth() > gPlayerTemplate[0].startHealth / 2) {
                        if (hitType == SS_SPRITE && boardService.getSprite(pHitInfo.hitsprite).getLotag() == pSprite.getLotag()) {
                            if (pXSector != null && pXSector.Underwater) {
                                aiNewState(pSprite, pXSprite, beastDodge[WATER]);
                            } else {
                                aiNewState(pSprite, pXSprite, beastDodge[LAND]);
                            }
                            return;
                        }

                        if (pXSector == null || !pXSector.Underwater) {
                            aiNewState(pSprite, pXSprite, beastStomps);
                        }
                    }

                    if (dist < 921 && klabs(losAngle) < kAngle5) {
                        if (hitType == SS_SPRITE && boardService.getSprite(pHitInfo.hitsprite).getLotag() == pSprite.getLotag()) {
                            if (pXSector != null && pXSector.Underwater) {
                                aiNewState(pSprite, pXSprite, beastDodge[WATER]);
                            } else {
                                aiNewState(pSprite, pXSprite, beastDodge[LAND]);
                            }
                            return;
                        }

                        if (pXSector != null && pXSector.Underwater) {
                            aiNewState(pSprite, pXSprite, beastHack[WATER]);
                        } else {
                            aiNewState(pSprite, pXSprite, beastHack[LAND]);
                        }
                    }

					/*
					if ( dist >= 5120 || dist <= 2560 ) {
						thinkAttack( pSprite, pXSprite, pXSector, dist, losAngle, hitType );
						return;
					}

					if ( klabs(losAngle) >= kAngle15
				        || (pTarget.flags & kAttrGravity) == 0
				        || !IsPlayerSprite(pTarget)
				        || (!Chance(0x4000))
						|| pXTarget.health <= gPlayerTemplate[0].startHealth / 2)
					{
						thinkAttack( pSprite, pXSprite, pXSector, dist, losAngle, hitType );
						return;
					}
					
					if(hitType == -1)
					{
						if(pXSector == null || !pXSector.Underwater)
							aiNewState(pSprite, pXSprite, beastStomps);
						thinkAttack( pSprite, pXSprite, pXSector, dist, losAngle, hitType );
						return;
					}
					  
					if(hitType == SS_SPRITE)
					{
						if(boardService.getSprite(pHitInfo.hitsprite).type == pSprite.type) {
							if(pXSector != null && pXSector.Underwater)
								aiNewState(pSprite, pXSprite, beastDodge[WATER]);
							else aiNewState(pSprite, pXSprite, beastDodge[LAND]);
						} else if(pXSector == null || !pXSector.Underwater)
							aiNewState(pSprite, pXSprite, beastStomps);
						thinkAttack( pSprite, pXSprite, pXSector, dist, losAngle, hitType );
						return;
					} 
					if(pXSector == null || !pXSector.Underwater)
						aiNewState(pSprite, pXSprite, beastStomps);
					thinkAttack( pSprite, pXSprite, pXSector, dist, losAngle, hitType );
					*/
					
					/*
					if ( hitType == SS_SPRITE && boardService.getSprite(pHitInfo.hitsprite).type == pSprite.type) {
						if(pXSector != null && pXSector.Underwater)
							aiNewState(pSprite, pXSprite, beastDodge[WATER]);
						else aiNewState(pSprite, pXSprite, beastDodge[LAND]);
						return;
					}
						
					if ( dist > 2560 && dist < 5120 && klabs(losAngle) < kAngle15
						&& (pTarget.flags & kAttrGravity) != 0
						&& IsPlayerSprite(pTarget)
						&& pXTarget.health > gPlayerTemplate[0].startHealth / 2
						&& Chance(0x4000) )
					{
						if(pXSector == null || !pXSector.Underwater)
							aiNewState(pSprite, pXSprite, beastStomps);
					}
					
					if(dist < 921 && klabs(losAngle) < kAngle5) {
						if(pXSector != null && pXSector.Underwater)
							aiNewState(pSprite, pXSprite, beastHack[WATER]);
						else aiNewState(pSprite, pXSprite, beastHack[LAND]);
					}
					*/
                }
                return;
            }
        }

        if (pXSector != null && pXSector.Underwater) {
            aiNewState(pSprite, pXSprite, beastGoto[WATER]);
        } else {
            aiNewState(pSprite, pXSprite, beastGoto[LAND]);
        }
        pXSprite.setTarget(-1);
    }
}

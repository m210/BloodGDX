// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.*;

public class AIBONEEL {

    public static AISTATE boneelIdle;
    public static AISTATE boneelChase;
    public static AISTATE boneelGoto;
    public static AISTATE boneelBite;
    public static AISTATE boneelRecoil;
    public static AISTATE boneelSearch;
    public static AISTATE boneelDown;
    public static AISTATE boneelUp;
    public static AISTATE boneelTurn;

    public static void Init() {
        boneelIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                myThinkTarget(sprite, xsprite);
            }
        };
        boneelChase = new AISTATE(Type.other, kSeqDudeIdle, null, 0, false, true, true, boneelIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        boneelGoto = new AISTATE(Type.tgoto, kSeqDudeIdle, null, 600, false, false, true, boneelIdle) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        boneelBite = new AISTATE(Type.other, 7, nXSprite -> BiteCallback(nXSprite), 60, false, false, false, boneelChase);
        boneelRecoil = new AISTATE(Type.other, 5, null, 1, false, false, false, boneelChase) {
            @Override
            public void callback(int nXSprite) {
                if (IsOriginalDemo()) BiteCallback(nXSprite);
            }
        };

        boneelSearch = new AISTATE(Type.search, kSeqDudeIdle, null, 120, false, true, true, boneelIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        boneelDown = new AISTATE(Type.other, kSeqDudeIdle, null, 60, false, true, true, boneelChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveDown(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        boneelUp = new AISTATE(Type.other, kSeqDudeIdle, null, 0, false, true, true, boneelChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveUp(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        boneelTurn = new AISTATE(Type.other, kSeqDudeIdle, null, 60, false, true, false, boneelChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }
        };
    }

    private static void BiteCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz;
        if (IsOriginalDemo()) {
            if (!IsDudeSprite(pTarget)) return;

            dz = dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight * pTarget.getYrepeat() << 2;
            dz -= dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight * pSprite.getYrepeat() << 2;
        } else {
            dz = dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight * pSprite.getYrepeat();
            if (pXSprite.getTarget() != -1) dz = pTarget.getZ() - pSprite.getZ();
        }

        actFireVector(pSprite, 0, 0, dx, dy, dz, kVectorBoneelBite);
    }

    private static void myMoveDown(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x4000) || EngineUtils.qdist(dx, dy) > 921) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * (pDudeInfo.frontSpeed - (divscale(kFrameTicks - pGameInfo.nDifficulty, kTimerRate, 26) / kTimerRate));

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            vel += fvel >> 1;
            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30),
            dmulscale(vel, sin, -svel, cos, 30),
            139810);
        }
    }

    private static void myMoveUp(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x2000) || EngineUtils.qdist(dx, dy) > 921) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * (pDudeInfo.frontSpeed - (divscale(kFrameTicks - pGameInfo.nDifficulty, kTimerRate, 26) / kTimerRate));

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            vel += fvel >> 1;

            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30),
            dmulscale(vel, sin, -svel, cos, 30),
            -32768);
        }
    }

    private static void myMoveForward(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) return;

        if (pXSprite.getTarget() == -1) pSprite.setAng((short) ((pSprite.getAng() + kAngle45) & kAngleMask));

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();

        if (EngineUtils.qdist(dx, dy) > 921) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * (pDudeInfo.frontSpeed - (divscale(kFrameTicks - pGameInfo.nDifficulty, kTimerRate, 26) / kTimerRate));

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            if (pXSprite.getTarget() == -1) vel += fvel;
            else vel += fvel >> 1;

            pSprite.setVelocityX(dmulscale(vel, cos, svel, sin, 30));
            pSprite.setVelocityY(dmulscale(vel, sin, -svel, cos, 30));
        }
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        myThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery)
            aiNewState(pSprite, pXSprite, boneelSearch);

        myThinkTarget(pSprite, pXSprite);
    }


    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, boneelGoto);
            return;
        }

        int dx, dy, dist;
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, boneelSearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, boneelSearch);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            GetSpriteExtents(pSprite);
            int zTop1 = extents_zTop;
            GetSpriteExtents(pTarget);
            int zTop2 = extents_zTop;
            int zBot2 = extents_zBot;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());

                    if (dist <= 921 && klabs(losAngle) < kAngle15) {
                        if (zTop2 > zTop1) {
                            aiNewState(pSprite, pXSprite, boneelDown);
                        } else aiNewState(pSprite, pXSprite, boneelBite);
                        return;
                    }

                    if (zBot2 > zTop1 && klabs(losAngle) < kAngle15) aiNewState(pSprite, pXSprite, boneelDown);
                    else {
                        if (zTop2 < zTop1 && klabs(losAngle) < kAngle15) aiNewState(pSprite, pXSprite, boneelUp);
                    }
                }
            }
        } else {
            aiNewState(pSprite, pXSprite, boneelSearch);
            pXSprite.setTarget(-1);
        }
    }

    private static void myThinkTarget(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        DudeExtra dudeExtra = pXSprite.getDudeExtra();

        if (dudeExtra.isActive()) {
            if (dudeExtra.getThinkTime() >= 10) {
                dudeExtra.setThinkTime(0);
                pXSprite.setGoalAng(pXSprite.getGoalAng() + kAngle45);
                Vector3 kSprite = pSprite.getKSprite();
                aiSetTarget(pXSprite, (int) kSprite.x, (int) kSprite.y, (int) kSprite.z);
                aiNewState(pSprite, pXSprite, boneelTurn);
                return;
            } else {
                dudeExtra.setThinkTime(dudeExtra.getThinkTime() + 1);
            }
        }

        if (!Chance(pDudeInfo.alertChance / 2)) return;

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            PLAYER pPlayer = gPlayer[i];

            if (pPlayer.pXsprite.getHealth() == 0 || powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0)
                continue;

            int x = pPlayer.pSprite.getX();
            int y = pPlayer.pSprite.getY();
            int z = pPlayer.pSprite.getZ();
            short nSector = pPlayer.pSprite.getSectnum();

            int dx = x - pSprite.getX();
            int dy = y - pSprite.getY();

            int dist = EngineUtils.qdist(dx, dy);

            if (dist <= pDudeInfo.seeDist || dist <= pDudeInfo.hearDist) {
                int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;
                // is there a line of sight to the player?
                if (engine.cansee(x, y, z, nSector, pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                    int nAngle = EngineUtils.getAngle(dx, dy);
                    int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;

                    if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                        dudeExtra.setThinkTime(0);

                        aiSetTarget(pXSprite, pPlayer.nSprite);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }

                    if (dist < pDudeInfo.hearDist) {
                        dudeExtra.setThinkTime(0);
                        aiSetTarget(pXSprite, x, y, z);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }
                }
            }
        }
    }
}

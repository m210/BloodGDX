// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSECTOR;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.Pragmas.*;

public class AICALEB {

    public static AISTATE[] calebIdle = new AISTATE[2];
    public static AISTATE[] calebSearch = new AISTATE[2];
    public static AISTATE[] calebGoto = new AISTATE[2];
    public static AISTATE[] calebDodge = new AISTATE[2];
    public static AISTATE[] calebChase = new AISTATE[2];
    public static AISTATE[] calebRecoil = new AISTATE[2];
    public static AISTATE[] calebHack = new AISTATE[2];
    public static AISTATE calebRTesla;
    public static AISTATE calebMoveTarget;

    public static void Init() {
        calebIdle[LAND] = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        calebIdle[WATER] = new AISTATE(Type.idle, 10, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        calebChase[LAND] = new AISTATE(Type.other, 6, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        calebChase[WATER] = new AISTATE(Type.other, 8, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChaseWater(sprite, xsprite);
            }
        };
        calebDodge[LAND] = new AISTATE(Type.other, 6, null, 90, false, true, false, calebChase[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        calebDodge[WATER] = new AISTATE(Type.other, 8, null, 90, false, true, false, calebChase[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        calebGoto[LAND] = new AISTATE(Type.tgoto, 6, null, 600, false, true, true, calebIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        calebGoto[WATER] = new AISTATE(Type.tgoto, 8, null, 600, false, true, true, calebIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGotoWater(sprite, xsprite);
            }
        };

        CALLPROC hackCallback = nXSprite -> HackCallback(nXSprite);

        calebHack[LAND] = new AISTATE(Type.other, 0, hackCallback, 120, false, false, false, calebChase[LAND]);
        calebHack[WATER] = new AISTATE(Type.other, 10, hackCallback, 0, false, false, true, calebChase[WATER]) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChaseWater(sprite, xsprite);
            }
        };
        calebSearch[LAND] = new AISTATE(Type.search, 6, null, 120, false, true, true, calebIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        calebSearch[WATER] = new AISTATE(Type.search, 8, null, 120, false, true, true, calebIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        calebRecoil[LAND] = new AISTATE(Type.other, 5, null, 0, false, false, false, calebDodge[LAND]);
        calebRecoil[WATER] = new AISTATE(Type.other, 5, null, 0, false, false, false, calebDodge[WATER]);
        calebRTesla = new AISTATE(Type.other, 4, null, 0, false, false, false, calebDodge[LAND]);
        calebMoveTarget = new AISTATE(Type.other, 8, null, 0, false, true, true, calebChase[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveTarget(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChaseWater(sprite, xsprite);
            }
        };
    }

    private static void HackCallback(int nXSprite) {
        XSPRITE pXSprite = boardService.getXSprite(nXSprite);
        Sprite pSprite = boardService.getSprite(pXSprite.getReference());

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = pXSprite.getDudeExtra().getDudeSlope();

        dx += BiRandom(1500);
        dy += BiRandom(1500);
        dz += BiRandom(1500);

        for (int i = 0; i < 2; i++) {
            int vz = dz + BiRandom2(500);
            int vy = dy + BiRandom2(1000);
            int vx = dx + BiRandom2(1000);
            actFireVector(pSprite, 0, 0, vx, vy, vz, kVectorShell);
        }

        if (Chance(0x4000)) {
            sfxStart3DSound(pSprite, Random(5) + 10000, -1, 0);
        }

        if (Chance(0x4000)) {
            sfxStart3DSound(pSprite, 1001, -1, 0);
        } else {
            sfxStart3DSound(pSprite, 1002, -1, 0);
        }
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int nXSector = boardService.getSector(pSprite.getSectnum()).getExtra();

        XSECTOR pXSector = null;
        if (nXSector > 0) {
            pXSector = xsector[nXSector];
        }

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
            if (pXSector != null && pXSector.Underwater) {
                aiNewState(pSprite, pXSprite, calebSearch[WATER]);
            } else {
                aiNewState(pSprite, pXSprite, calebSearch[LAND]);
            }
        }

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGotoWater(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
            aiNewState(pSprite, pXSprite, calebSearch[WATER]);
        }

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        int nXSector = boardService.getSector(pSprite.getSectnum()).getExtra();
        XSECTOR pXSector = null;
        if (nXSector > 0) {
            pXSector = xsector[nXSector];
        }

        if (pXSprite.getTarget() == -1) {
            if (pXSector != null && pXSector.Underwater) {
                aiNewState(pSprite, pXSprite, calebSearch[WATER]);
            } else {
                aiNewState(pSprite, pXSprite, calebSearch[LAND]);
            }
            return;
        }

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));
        if (pXTarget == null || pXTarget.getHealth() == 0) {
            if (pXSector != null && pXSector.Underwater) {
                aiNewState(pSprite, pXSprite, calebSearch[WATER]);
            } else {
                aiPlaySound(pSprite, Random(4) + 11000, 1, -1);
                aiNewState(pSprite, pXSprite, calebSearch[LAND]);
            }
            return;
        }
        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, calebSearch[WATER]);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);

        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(),
                    pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());
                    if (dist > 0) {
                        pXSprite.getDudeExtra().setDudeSlope(divscale(pTarget.getZ() - pSprite.getZ(), dist, 10));
                    }

                    if (dist < 1433 && klabs(losAngle) < kAngle5) {
                        int hitType = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                        if (hitType == SS_SPRITE && boardService.getSprite(pHitInfo.hitsprite).getLotag() == pSprite.getLotag()) {
                            if (pXSector != null && pXSector.Underwater) {
                                aiNewState(pSprite, pXSprite, calebDodge[WATER]);
                            } else {
                                aiNewState(pSprite, pXSprite, calebDodge[LAND]);
                            }
                            return;
                        }

                        if (pXSector != null && pXSector.Underwater) {
                            aiNewState(pSprite, pXSprite, calebHack[WATER]);
                        } else {
                            aiNewState(pSprite, pXSprite, calebHack[LAND]);
                        }
                    }
                }
                return;
            }
        }

        if (pXSector != null && pXSector.Underwater) {
            aiNewState(pSprite, pXSprite, calebGoto[WATER]);
        } else {
            aiNewState(pSprite, pXSprite, calebGoto[LAND]);
        }
        if (Chance(0x1000)) {
            sfxStart3DSound(pSprite, Random(5) + 10000, -1, 0);
        }
        pXSprite.setTarget(-1);

    }

    private static void thinkChaseWater(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, calebGoto[WATER]);
            return;
        }

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));
        if (pXTarget == null || pXTarget.getHealth() == 0) {
            aiNewState(pSprite, pXSprite, calebSearch[WATER]);
            return;
        }
        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, calebSearch[WATER]);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);

        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(),
                    pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());

                    if (dist < 1024 && klabs(losAngle) < kAngle15) {
                        aiNewState(pSprite, pXSprite, calebHack[WATER]);
                        return;
                    }
                    aiNewState(pSprite, pXSprite, calebMoveTarget);
                }
            } else {
                aiNewState(pSprite, pXSprite, calebMoveTarget);
            }
        } else {
            aiNewState(pSprite, pXSprite, calebGoto[WATER]);
            pXSprite.setTarget(-1);
        }
    }

    private static void myMoveForward(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            return;
        }

        if (pXSprite.getTarget() == -1) {
            pSprite.setAng((short) ((pSprite.getAng() + kAngle45) & kAngleMask));
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();

        if (Random(64) >= 32 || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            int fvel = kFrameTicks * pDudeInfo.frontSpeed;

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            if (pXSprite.getTarget() == -1) {
                vel += fvel;
            } else {
                vel += (fvel >> 2);
            }

            pSprite.setVelocityX(dmulscale(vel, cos, svel, sin, 30));
            pSprite.setVelocityY(dmulscale(vel, sin, -svel, cos, 30));
        }
    }

    private static void myMoveTarget(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pSprite.setAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x2000) || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long vel = (long) kFrameTicks * pDudeInfo.frontSpeed;
            vel = (vel >> 1) + dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            int z1 = 0;
            if (pXSprite.getTarget() != -1) {
                Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
                z1 = pTarget.getZ();
                if (IsDudeSprite(pTarget)) {
                    z1 += dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight;
                }
            }
            int z2 = pSprite.getZ() + pDudeInfo.eyeHeight;

            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30),
                    dmulscale(vel, sin, -svel, cos, 30),
                    8L * (z1 - z2));
        }
    }
}

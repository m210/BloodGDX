// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.Seq.SeqInst;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.evPost;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.GetInstance;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqFrame;
import static ru.m210projects.Blood.VERSION.getSeq;
import static ru.m210projects.Blood.VERSION.kCultProneOffset;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Build.Pragmas.klabs;

public class AICULTIST {

    public static final int kSlopeThrow = -8192;
    private static final int[] TeslaChance = {0x2000, 0x4000, 0x8000, 0xA000, 0xE000};
    public static AISTATE[] cultistIdle = new AISTATE[3];
    public static AISTATE[] cultistSearch = new AISTATE[3];
    public static AISTATE[] cultistGoto = new AISTATE[3];
    public static AISTATE cultistTThrow;
    public static AISTATE cultistSThrow;
    public static AISTATE cultistTESThrow;
    public static AISTATE[] cultistDodge = new AISTATE[3];
    public static AISTATE[] cultistChase = new AISTATE[3];
    public static AISTATE[] cultistTFire = new AISTATE[3];
    public static AISTATE[] cultistTESFire = new AISTATE[3];
    public static AISTATE[] cultistSFire = new AISTATE[3];
    public static AISTATE cultistRTesla;
    public static AISTATE[] cultistRecoil = new AISTATE[3];
    public static AISTATE cultistSProne;
    public static AISTATE cultistTProne;
    public static AISTATE cultistTurn;
    public static AISTATE cultistGThrow1;
    public static AISTATE cultistGThrow2;

    public static void Init() {
        CALLPROC tommyCallback = nXSprite -> TommyCallback(nXSprite);

        CALLPROC teslaCallback = nXSprite -> TeslaCallback(nXSprite);

        CALLPROC shotCallback = nXSprite -> ShotCallback(nXSprite);

        cultistIdle[LAND] = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget((BloodSprite) sprite, xsprite);
            }
        };
        cultistIdle[WATER] = new AISTATE(Type.idle, 13, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget((BloodSprite) sprite, xsprite);
            }
        };
        cultistSearch[LAND] = new AISTATE(Type.search, 9, null, 1800, false, true, true, cultistIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        cultistSearch[WATER] = new AISTATE(Type.search, 13, null, 1800, false, true, true, cultistIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        cultistGoto[LAND] = new AISTATE(Type.tgoto, 9, null, 600, false, true, true, cultistIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        cultistGoto[WATER] = new AISTATE(Type.tgoto, 13, null, 600, false, true, true, cultistIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        cultistChase[LAND] = new AISTATE(Type.other, 9, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        cultistDodge[LAND] = new AISTATE(Type.other, 9, null, 90, false, true, false, cultistChase[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        cultistChase[DUCK] = new AISTATE(Type.other, 14, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        cultistDodge[DUCK] = new AISTATE(Type.other, 14, null, 90, false, true, false, cultistChase[DUCK]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        cultistChase[WATER] = new AISTATE(Type.other, 13, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        cultistDodge[WATER] = new AISTATE(Type.other, 13, null, 90, false, true, false, cultistChase[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        cultistTFire[LAND] = new AISTATE(Type.other, kSeqDudeAttack, tommyCallback, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        cultistTFire[LAND].next = cultistTFire[LAND];
        cultistSFire[LAND] = new AISTATE(Type.other, kSeqDudeAttack, shotCallback, 60, false, false, false, cultistChase[LAND]);

        cultistTESFire[LAND] = new AISTATE(Type.other, kSeqDudeAttack, teslaCallback, 0, false, true, true, cultistChase[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        cultistTFire[WATER] = new AISTATE(Type.other, 8, tommyCallback, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        cultistTFire[WATER].next = cultistTFire[WATER];
        cultistSFire[WATER] = new AISTATE(Type.other, 8, shotCallback, 60, false, false, false, cultistChase[WATER]);

        cultistTESFire[WATER] = new AISTATE(Type.other, 8, teslaCallback, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        cultistTESFire[WATER].next = cultistTESFire[WATER];

        cultistTFire[DUCK] = new AISTATE(Type.other, 8, tommyCallback, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        cultistTFire[DUCK].next = cultistTFire[DUCK];
        cultistSFire[DUCK] = new AISTATE(Type.other, 8, shotCallback, 60, false, false, false, cultistChase[DUCK]);
        cultistTESFire[DUCK] = new AISTATE(Type.other, 8, teslaCallback, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                if (!IsOriginalDemo()) {
                    thinkChase(sprite, xsprite);
                }
            }
        };

        CALLPROC throwCallback = nXSprite -> ThrowCallback(nXSprite);

        cultistTESFire[DUCK].next = cultistTESFire[DUCK];
        cultistTThrow = new AISTATE(Type.other, 7, throwCallback, 120, false, false, false, cultistTFire[LAND]);
        cultistSThrow = new AISTATE(Type.other, 7, throwCallback, 120, false, false, false, cultistSFire[LAND]);
        cultistTESThrow = new AISTATE(Type.other, 7, throwCallback, 120, false, false, false, cultistTESFire[LAND]);

        cultistRTesla = new AISTATE(Type.other, kSeqDudeTesla, null, 0, false, false, false, cultistDodge[LAND]);
        cultistRecoil[LAND] = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, null);
        cultistRecoil[WATER] = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, cultistDodge[WATER]);
        cultistRecoil[DUCK] = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, null);
        cultistRecoil[LAND].next = cultistDodge[DUCK];
        cultistRecoil[DUCK].next = cultistDodge[LAND];

        cultistSProne = new AISTATE(Type.other, getSeq(kCultProneOffset), null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget((BloodSprite) sprite, xsprite);
            }
        };
        cultistTProne = new AISTATE(Type.other, getSeq(kCultProneOffset), null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget((BloodSprite) sprite, xsprite);
            }
        };
        cultistTurn = new AISTATE(Type.other, 0, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        cultistGThrow1 = new AISTATE(Type.other, 7, throwCallback, 120, false, false, false, cultistChase[LAND]);
        cultistGThrow2 = new AISTATE(Type.other, 7, nXSprite -> GThrowCallback(nXSprite), 120, false, false, false, cultistChase[LAND]);
    }

    private static void TeslaCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        DudeExtra pDudeExtra = pXSprite.getDudeExtra();

        if (Chance(TeslaChance[pGameInfo.nDifficulty] >> 1)) {
            int dx = Cos(pSprite.getAng()) >> 16;
            int dy = Sin(pSprite.getAng()) >> 16;
            int dz = pDudeExtra.getDudeSlope();

            // dispersal modifiers here
            dx += BiRandom2(5000 - 1000 * pGameInfo.nDifficulty);
            dy += BiRandom2(5000 - 1000 * pGameInfo.nDifficulty);
            dz += BiRandom2(2500 - 500 * pGameInfo.nDifficulty);

            actFireMissile(pSprite, 0, 0, dx, dy, dz, 306);
            sfxStart3DSound(pSprite, 470, -1, 0);
        }
    }

    private static void TommyCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        DudeExtra pDudeExtra = pXSprite.getDudeExtra();

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = pDudeExtra.getDudeSlope();

        // dispersal modifiers here
        dx += BiRandom2(5000 - 1000 * pGameInfo.nDifficulty);
        dy += BiRandom2(5000 - 1000 * pGameInfo.nDifficulty);
        dz += BiRandom2(2500 - 500 * pGameInfo.nDifficulty);

        actFireVector(pSprite, 0, 0, dx, dy, dz, kVectorBullet);
        sfxStart3DSound(pSprite, 4001, -1, 0); //kSfxTomFire
    }

    private static void ShotCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        DudeExtra pDudeExtra = pXSprite.getDudeExtra();

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = pDudeExtra.getDudeSlope();

        // aim modifiers
        dx += BiRandom(4500 - 1000 * pGameInfo.nDifficulty);
        dy += BiRandom(4500 - 1000 * pGameInfo.nDifficulty);
        dz += BiRandom(2500 - 500 * pGameInfo.nDifficulty);

        for (int i = 0; i < 8; i++) {
            int ddz = dz + BiRandom2(500);
            int ddy = dy + BiRandom2(1000);
            int ddx = dx + BiRandom2(1000);

            actFireVector(pSprite, 0, 0, ddx, ddy, ddz, kVectorShell); //ok
        }

        if (Chance(0x4000)) {
            sfxStart3DSound(pSprite, 1001, -1, 0); //kSfxShotFire
        } else {
            sfxStart3DSound(pSprite, 1002, -1, 0);
        }
    }

    private static void ThrowCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (!IsDudeSprite(pSprite)) {
            return;
        }

        if (boardService.isValidSprite(pXSprite.getTarget())) {
            Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
            sfxStart3DSound(pSprite, 455, -1, 0);
            int nTile = kThingTNTStick;
            if (pGameInfo.nDifficulty > 2) {
                nTile = kThingTNTBundle;
            }

            int dx = pTarget.getX() - pSprite.getX();
            int dy = pTarget.getY() - pSprite.getY();
            int dz = pTarget.getZ() - pSprite.getZ();

            int dist = EngineUtils.qdist(dx, dy);

            Sprite pThing = actFireThing(nSprite, 0, 0, (dz / 128) - 14500, nTile, divscale(dist / 540, kTimerRate, 23));
            boolean chance = Chance(0x3000);
            if (dist <= 7680 && chance) {
                boardService.getXSprite(pThing.getExtra()).setImpact(true);
            } else {
                evPost(pThing.getXvel(), SS_SPRITE, Integer.toUnsignedLong(kTimerRate * Random(2) + kTimerRate), 1);
            }
        } else if (!IsOriginalDemo()) {
            GThrowCallback(nXIndex);
        }
    }

    private static void GThrowCallback(int nXIndex) //Green cultist
    {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        DudeExtra pDudeExtra = pXSprite.getDudeExtra();

        sfxStart3DSound(pSprite, 455, -1, 0); //kSfxTNTToss
        int nTile = kThingTNTStick;
        if (pGameInfo.nDifficulty > 2) {
            nTile = kThingTNTBundle;
        }

        Sprite pThing = actFireThing(nSprite, 0, 0, pDudeExtra.getDudeSlope() - 9460, nTile, 0x133333);

        evPost(pThing.getXvel(), SS_SPRITE, Integer.toUnsignedLong(kTimerRate * Random(2) + 240), 1);
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget2((BloodSprite) pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(10.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
            if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                aiNewState(pSprite, pXSprite, cultistSearch[WATER]);
            } else {
                aiNewState(pSprite, pXSprite, cultistSearch[LAND]);
            }
        }
        aiThinkTarget((BloodSprite) pSprite, pXSprite);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                aiNewState(pSprite, pXSprite, cultistGoto[WATER]);
            } else {
                aiNewState(pSprite, pXSprite, cultistGoto[LAND]);
            }
            return;
        }

        int dx, dy, dist;

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());
        if (!IsOriginalDemo() && !IsDudeSprite(pTarget)) {
            pXTarget = null;
        }

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                aiNewState(pSprite, pXSprite, cultistSearch[WATER]);
            } else {
                aiNewState(pSprite, pXSprite, cultistSearch[LAND]);
                if (pSprite.getLotag() == kDudeTommyCultist) {
                    aiPlaySound((BloodSprite) pSprite, Random(4) + 4021, 1, -1);
                } else {
                    aiPlaySound((BloodSprite) pSprite, Random(4) + 1021, 1, -1);
                }
            }
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                    aiNewState(pSprite, pXSprite, cultistSearch[WATER]);
                } else {
                    aiNewState(pSprite, pXSprite, cultistSearch[LAND]);
                }
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());
                    if (dist > 0) {
                        pXSprite.getDudeExtra().setDudeSlope(divscale(pTarget.getZ() - pSprite.getZ(), dist, 10));
                    }

                    switch (pSprite.getLotag()) {
                        case kDudeTommyCultist:
						/*
						if ( dist < 7680 && dist > 3584 && klabs(losAngle) < kAngle15 && pGameInfo.nDifficulty > 2
						&& !TargetNearExplosion(pTarget) && (pTarget.flags & kAttrGravity) != 0	
						&& ( IsPlayerSprite(pTarget) && gPlayer[pTarget.type - kDudePlayer1].Run )
						&& Chance(0x4000) ) {
							int pHit = HitScan(pSprite, pSprite.z, dx, dy, 0, pHitInfo, 16777280, 0);
							switch(pHit)
							{
								case SS_SPRITE:
									SPRITE pHitSprite = boardService.getSprite(pHitInfo.hitsprite);
									if(pHitSprite.type != pSprite.type && pHitSprite.type != kDudeShotgunCultist)
									{
										if(pXSprite.palette != 1 && pXSprite.palette != 2) {
											aiNewState(pSprite, pXSprite, cultistTThrow);
											return;
										}
									}
									break;
								case SS_WALL:
								case SS_MASKED:
									return;
								default:
									if(pXSprite.palette != 1 && pXSprite.palette != 2) {
										aiNewState(pSprite, pXSprite, cultistTThrow);
										return;
									}
									break;
							}
						}
						else 
						*/
                            if (dist < 17920 && klabs(losAngle) < kAngle5) {
                                int state = checkAttackState(pSprite, pXSprite);
                                int pHit = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                                if (pHit == -1) {
                                    if (state == 1 && checkDudeSeq(pSprite, 13)) {
                                        aiNewState(pSprite, pXSprite, cultistTFire[WATER]);
                                    }
                                    if (state == 2) {
                                        aiNewState(pSprite, pXSprite, cultistTFire[DUCK]);
                                    }
                                    if (state == 3) {
                                        aiNewState(pSprite, pXSprite, cultistTFire[LAND]);
                                    }
                                } else {
                                    if (pHit == 3) {
                                        int type = boardService.getSprite(pHitInfo.hitsprite).getLotag();
                                        if (type == pSprite.getLotag() || type == kDudeShotgunCultist) {
                                            if (state == 1) {
                                                aiNewState(pSprite, pXSprite, cultistDodge[WATER]);
                                            }
                                            if (state == 2) {
                                                aiNewState(pSprite, pXSprite, cultistDodge[DUCK]);
                                            }
                                            if (state == 3) {
                                                aiNewState(pSprite, pXSprite, cultistDodge[LAND]);
                                            }
                                            return;
                                        }
                                    }

                                    if (state == 1) {
                                        aiNewState(pSprite, pXSprite, cultistTFire[WATER]);
                                    }
                                    if (state == 2) {
                                        aiNewState(pSprite, pXSprite, cultistTFire[DUCK]);
                                    }
                                    if (state == 3) {
                                        aiNewState(pSprite, pXSprite, cultistTFire[LAND]);
                                    }
                                }
                            }
                            break;
                        case kDudeShotgunCultist:
                        case kDudeBeastCultist:
                            if (pSprite.getLotag() != kDudeBeastCultist && dist < 11264 && dist > 5120 && !TargetNearExplosion(pTarget) && (pTarget.getHitag() & kAttrGravity) != 0 && pGameInfo.nDifficulty >= 2 && IsPlayerSprite(pTarget) && Chance(0x4000)) {
                                int pHit = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                                switch (pHit) {
                                    case SS_SPRITE:
                                        Sprite pHitSprite = boardService.getSprite(pHitInfo.hitsprite);
                                        if (pHitSprite.getLotag() != pSprite.getLotag() && pHitSprite.getLotag() != kDudeShotgunCultist) {
                                            if (pXSprite.getPalette() != 1 && pXSprite.getPalette() != 2) {
                                                aiNewState(pSprite, pXSprite, cultistSThrow);
                                                return;
                                            }
                                        }
                                        break;
                                    case SS_WALL:
                                    case SS_MASKED:
                                        return;
                                    case -1:
                                        if (pXSprite.getPalette() != 1 && pXSprite.getPalette() != 2) {
                                            aiNewState(pSprite, pXSprite, cultistSThrow);
                                            return;
                                        }
                                        break;
                                    default:
                                        aiNewState(pSprite, pXSprite, cultistSThrow);
                                        break;
                                }
                            } else if (dist < 12800 && klabs(losAngle) < kAngle5) {
                                int state = checkAttackState(pSprite, pXSprite);
                                int pHit = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                                if (pHit == -1) {
                                    if (state == 1) {
                                        aiNewState(pSprite, pXSprite, cultistSFire[WATER]);
                                    }
                                    if (state == 2) {
                                        aiNewState(pSprite, pXSprite, cultistSFire[DUCK]);
                                    }
                                    if (state == 3) {
                                        aiNewState(pSprite, pXSprite, cultistSFire[LAND]);
                                    }
                                } else {
                                    if (pHit == 3) {
                                        int type = boardService.getSprite(pHitInfo.hitsprite).getLotag();
                                        if (type == pSprite.getLotag() || type == kDudeTommyCultist) {
                                            if (state == 1) {
                                                aiNewState(pSprite, pXSprite, cultistDodge[WATER]);
                                            }
                                            if (state == 2) {
                                                aiNewState(pSprite, pXSprite, cultistDodge[DUCK]);
                                            }
                                            if (state == 3) {
                                                aiNewState(pSprite, pXSprite, cultistDodge[LAND]);
                                            }
                                            return;
                                        }
                                    }
                                    if (state == 1) {
                                        aiNewState(pSprite, pXSprite, cultistSFire[WATER]);
                                    }
                                    if (state == 2) {
                                        aiNewState(pSprite, pXSprite, cultistSFire[DUCK]);
                                    }
                                    if (state == 3) {
                                        aiNewState(pSprite, pXSprite, cultistSFire[LAND]);
                                    }
                                }
                            }
                            break;
                        case kDudeTeslaCultist:
						/*
						if ( dist < 7680 && dist > 3584 && !TargetNearExplosion(pTarget)
						&& (pTarget.flags & kAttrGravity) != 0	
						&& pGameInfo.nDifficulty > 2
						&& ( IsPlayerSprite(pTarget) && gPlayer[pTarget.type - kDudePlayer1].Run )
						&& Chance(0x4000) ) {
							int pHit = HitScan(pSprite, pSprite.z, dx, dy, 0, pHitInfo, 16777280, 0);
							switch(pHit)
							{
								case SS_SPRITE:
									SPRITE pHitSprite = boardService.getSprite(pHitInfo.hitsprite);
									if(pHitSprite.type != pSprite.type && pHitSprite.type != kDudeShotgunCultist)
									{
										if(pXSprite.palette != 1 && pXSprite.palette != 2) {
											aiNewState(pSprite, pXSprite, cultistTESThrow);
											return;
										}
									}
									break;
								case SS_WALL:
								case SS_MASKED:
									return;
								default:
									if(pXSprite.palette != 1 && pXSprite.palette != 2) {
										aiNewState(pSprite, pXSprite, cultistTESThrow);
										return;
									}
									break;
							}
						}
						else 
						*/
                            if (dist < 12800 && klabs(losAngle) < kAngle5) {
                                int state = checkAttackState(pSprite, pXSprite);
                                int pHit = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                                if (pHit == -1) {
                                    if (state == 1) {
                                        aiNewState(pSprite, pXSprite, cultistTESFire[WATER]);
                                    }
                                    if (state == 2) {
                                        aiNewState(pSprite, pXSprite, cultistTESFire[DUCK]);
                                    }
                                    if (state == 3) {
                                        aiNewState(pSprite, pXSprite, cultistTESFire[LAND]);
                                    }
                                } else {
                                    if (pHit == 3) {
                                        int type = boardService.getSprite(pHitInfo.hitsprite).getLotag();
                                        if (type == pSprite.getLotag() || type == kDudeTommyCultist) {
                                            if (state == 1) {
                                                aiNewState(pSprite, pXSprite, cultistDodge[WATER]);
                                            }
                                            if (state == 2) {
                                                aiNewState(pSprite, pXSprite, cultistDodge[DUCK]);
                                            }
                                            if (state == 3) {
                                                aiNewState(pSprite, pXSprite, cultistDodge[LAND]);
                                            }
                                            return;
                                        }
                                    }

                                    if (state == 1) {
                                        aiNewState(pSprite, pXSprite, cultistTESFire[WATER]);
                                    }
                                    if (state == 2) {
                                        aiNewState(pSprite, pXSprite, cultistTESFire[DUCK]);
                                    }
                                    if (state == 3) {
                                        aiNewState(pSprite, pXSprite, cultistTESFire[LAND]);
                                    }
                                }
                            }
                            break;

                        case kDudeDynamiteCultist:
                            if (klabs(losAngle) < kAngle15 && (pTarget.getHitag() & kAttrGravity) != 0 && IsPlayerSprite(pTarget)) {
                                switch (HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0)) {
                                    case SS_SPRITE:
                                        Sprite pHitSprite = boardService.getSprite(pHitInfo.hitsprite);
                                        if (pHitSprite.getLotag() != pSprite.getLotag() && pHitSprite.getLotag() != kDudeShotgunCultist) {
                                            if (pXSprite.getPalette() != 1 && pXSprite.getPalette() != 2) {
                                                if (dist < 11264 && dist > 5120) {
                                                    aiNewState(pSprite, pXSprite, cultistGThrow1);
                                                } else if (dist < 5120) {
                                                    aiNewState(pSprite, pXSprite, cultistGThrow2);
                                                }
                                                return;
                                            }
                                        }
                                        break;
                                    case SS_MASKED:
                                        return;
                                    default:
                                        if (pXSprite.getPalette() != 1 && pXSprite.getPalette() != 2) {
                                            if (dist < 11264 && dist > 5120) {
                                                aiNewState(pSprite, pXSprite, cultistGThrow1);
                                            } else if (dist < 5120) {
                                                aiNewState(pSprite, pXSprite, cultistGThrow2);
                                            }
                                            return;
                                        }
                                        break;
                                }
                            }
                    }
                    return;
                }
            }
        }
        if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
            aiNewState(pSprite, pXSprite, cultistGoto[WATER]);
        } else {
            aiNewState(pSprite, pXSprite, cultistGoto[LAND]);
        }
        pXSprite.setTarget(-1);
    }

    public static int checkAttackState(BloodSprite pSprite, XSPRITE pXSprite) {
        //DUCK - seq 14
        if (checkDudeSeq(pSprite, 14) || pXSprite.getPalette() != 0) {
            if (!checkDudeSeq(pSprite, 14) || pXSprite.getPalette() != 0) {
                if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                    return 1; //water
                }
            } else {
                return 2; //duck
            }
        } else {
            return 3; //land
        }
        return 0;
    }

    public static boolean checkDudeSeq(BloodSprite pSprite, int nSeqID) {
        if (pSprite.getStatnum() == kStatDude && IsDudeSprite(pSprite)) {
            final int nXSprite = pSprite.getExtra();
            SeqInst pInst = GetInstance(SS_SPRITE, nXSprite);
            return pInst.getSeqIndex() == (dudeInfo[pSprite.getLotag() - kDudeBase].seqStartID + nSeqID) && seqFrame(SS_SPRITE, nXSprite) >= 0;
        }
        return false;
    }

    public static boolean TargetNearExplosion(Sprite pSprite) {
        for (ListNode<Sprite> node = boardService.getSectNode(pSprite.getSectnum()); node != null; node = node.getNext()) {
            // check for TNT sticks or explosions in the same sector as the target
            if (node.get().getLotag() == kThingTNTStick || node.get().getStatnum() == kStatExplosion) {
                return true; // indicate danger
            }
        }
        return false;
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.WeaponAim;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.dmulscale;
import static ru.m210projects.Build.Pragmas.klabs;

public class AIGARG {

    private static final WeaponAim gGargData = new WeaponAim(65536, 65536, 256, 85, 0x1AAAAA);
    public static AISTATE gargoyleIdle;
    public static AISTATE gargoyleSearch;
    public static AISTATE gargoyleChase;
    public static AISTATE gargoyleRecoil;
    public static AISTATE gargoyleGoto;
    public static AISTATE gargoyleFThrow;
    public static AISTATE gargoyleFSlash;
    public static AISTATE gargoyleSThrow;
    public static AISTATE gargoyleTurn;
    public static AISTATE gargoyleUp;
    public static AISTATE gargoyleDown;
    public static AISTATE statueIdle;
    public static AISTATE statueFTransform;
    public static AISTATE statueFChase;
    public static AISTATE statueSTransform;
    public static AISTATE statueSChase;
    public static AISTATE statueFTransformNew;
    public static AISTATE statueSTransformNew;

    public static void Init() {
        statueIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, false, null);

        gargoyleIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                myThinkTarget(sprite, xsprite);
            }
        };

        statueFTransform = new AISTATE(Type.other, -1, null, 0, true, false, false, gargoyleIdle) {
            @Override
            public void enter(BloodSprite sprite, XSPRITE xsprite) {
                actHealDude(xsprite, dudeInfo[6].startHealth, dudeInfo[6].startHealth);
                sprite.setLotag(kDudeFleshGargoyle);
            }
        };

        statueFTransformNew = new AISTATE(Type.other, 5, null, 0, true, false, true, statueFTransform) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiPlaySound((BloodSprite) sprite, 313, 1, -1);
            }
        };

        statueFChase = new AISTATE(Type.other, 6, null, 0, false, false, false, statueFTransform);

        statueSTransform = new AISTATE(Type.other, -1, null, 0, true, false, false, gargoyleIdle) {
            @Override
            public void enter(BloodSprite sprite, XSPRITE xsprite) {
                actHealDude(xsprite, dudeInfo[7].startHealth, dudeInfo[7].startHealth);
                sprite.setLotag(kDudeStoneGargoyle);
            }
        };

        statueSTransformNew = new AISTATE(Type.other, 5, null, 0, true, false, true, statueSTransform) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiPlaySound((BloodSprite) sprite, 313, 1, -1);
            }
        };

        statueSChase = new AISTATE(Type.other, 6, null, 0, false, false, false, statueSTransform);

        gargoyleGoto = new AISTATE(Type.tgoto, kSeqDudeIdle, null, 600, false, true, true, gargoyleIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        gargoyleSearch = new AISTATE(Type.search, kSeqDudeIdle, null, 120, false, true, true, gargoyleIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        gargoyleChase = new AISTATE(Type.other, kSeqDudeIdle, null, 0, false, true, true, gargoyleIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        gargoyleRecoil = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, gargoyleChase);
        gargoyleFSlash = new AISTATE(Type.other, 6, nXSprite -> SlashCallback(nXSprite), 120, false, false, false, gargoyleChase);
        gargoyleFThrow = new AISTATE(Type.other, 6, nXSprite -> FThrowCallback(nXSprite), 120, false, false, false, gargoyleChase);
        gargoyleSThrow = new AISTATE(Type.other, 7, nXSprite -> SThrowCallback(nXSprite), 60, false, true, false, gargoyleChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveAttack(sprite, xsprite);
            }
        };
        gargoyleDown = new AISTATE(Type.other, kSeqDudeIdle, null, 120, false, true, true, gargoyleChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveDown(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        gargoyleUp = new AISTATE(Type.other, kSeqDudeIdle, null, 120, false, true, true, gargoyleChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveUp(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };

        gargoyleTurn = new AISTATE(Type.other, kSeqDudeIdle, null, 120, false, true, false, gargoyleChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }
        };
    }

    private static void FThrowCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        actFireThing(pXSprite.getReference(), 0, 0, pXSprite.getDudeExtra().getDudeSlope() - 7500, kThingBoneClub, 978670);
    }

    private static void SThrowCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        Sprite pTarget = pXSprite.getTarget() != -1 ? boardService.getSprite(pXSprite.getTarget()) : null;
        bRandom();

        UpdateEnemyAim(pSprite, nXIndex, gGargData);

        if (IsPlayerSprite(pTarget) || !IsOriginalDemo()) {
            actFireMissile(pSprite, -120, 0, (int) EnemyAim.x, (int) EnemyAim.y, (int) EnemyAim.z, kMissileBone);
            actFireMissile(pSprite, 120, 0, (int) EnemyAim.x, (int) EnemyAim.y, (int) EnemyAim.z, kMissileBone);
        }
    }

    private static void SlashCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (!IsDudeSprite(pSprite)) return;

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz;

        if (IsOriginalDemo()) {
            int nZOffset1 = dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight * pSprite.getYrepeat() << 2;
            int nZOffset2 = 0;
            if (pXSprite.getTarget() != -1) {
                Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
                if (IsDudeSprite(pTarget))
                    nZOffset2 = dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight * pTarget.getYrepeat() << 2;
            }
            dz = nZOffset1 - nZOffset2;
        } else {
            dz = dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight * pSprite.getYrepeat() << 2;
            if (pXSprite.getTarget() != -1) {
                Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
                dz = pTarget.getZ() - pSprite.getZ();
            }
        }

        int vx = dx, vy = dy, vz = dz;
        actFireVector(pSprite, 0, 0, vx, vy, vz, kVectorGargSlash);
        vy = dy - Random(50);
        vx = dx + Random(50);
        actFireVector(pSprite, 0, 0, vx, vy, vz, kVectorGargSlash);
        vy = dy + Random(50);
        vx = dx - Random(50);
        actFireVector(pSprite, 0, 0, vx, vy, vz, kVectorGargSlash);
    }

    private static void myMoveAttack(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pXSprite.setGoalAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x300) || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            int fvel = kFrameTicks * pDudeInfo.frontSpeed;
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            pSprite.setVelocityX(dmulscale(fvel >> 1, cos, svel >> 1, sin, 30));
            pSprite.setVelocityY(dmulscale(fvel >> 1, sin, -(svel >> 1), cos, 30));

            if (pSprite.getLotag() == 206) pSprite.setVelocityZ(279620);
            else if (pSprite.getLotag() == 207) pSprite.setVelocityZ(218453);
        }
    }

    private static void myMoveDown(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pXSprite.setGoalAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x300) || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * pDudeInfo.frontSpeed;
            int vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            vel += fvel >> 1;
            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30),
            dmulscale(vel, sin, -svel, cos, 30),
            vel);
        }
    }

    private static void myMoveUp(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pSprite.setAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x2000) || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * pDudeInfo.frontSpeed;

            int vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            vel += fvel >> 1;

            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30),
            dmulscale(vel, sin, -svel, cos, 30),
            -vel);
        }
    }

    private static void myMoveForward(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) return;

        if (pXSprite.getTarget() == -1) pSprite.setAng((short) ((pSprite.getAng() + kAngle45) & kAngleMask));

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();

        if (Random(64) >= 32 || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            int fvel = kFrameTicks * pDudeInfo.frontSpeed;

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            if (pXSprite.getTarget() == -1) vel += fvel;
            else vel += fvel >> 1;

            pSprite.setVelocityX(dmulscale(vel, cos, svel, sin, 30));
            pSprite.setVelocityY(dmulscale(vel, sin, -svel, cos, 30));
        }
    }

    private static void myThinkTarget(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        DudeExtra dudeExtra = pXSprite.getDudeExtra();
        
        if (dudeExtra.isActive()) {
            if (dudeExtra.getThinkTime() >= 10) {
                pXSprite.setGoalAng(pXSprite.getGoalAng() + kAngle45);
                Vector3 kSprite = ((BloodSprite) pSprite).getKSprite();
                aiSetTarget(pXSprite, (int) kSprite.x, (int) kSprite.y, (int) kSprite.z);
                aiNewState(pSprite, pXSprite, gargoyleTurn);
                return;
            } else { dudeExtra.setThinkTime(dudeExtra.getThinkTime() + 1); }
        }

        if (!Chance(pDudeInfo.alertChance / 2)) return;

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            PLAYER pPlayer = gPlayer[i];

            if (pPlayer.pXsprite.getHealth() == 0 || powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0)
                continue;

            int x = pPlayer.pSprite.getX();
            int y = pPlayer.pSprite.getY();
            int z = pPlayer.pSprite.getZ();
            short nSector = pPlayer.pSprite.getSectnum();

            int dx = x - pSprite.getX();
            int dy = y - pSprite.getY();

            int dist = EngineUtils.qdist(dx, dy);

            if (dist <= pDudeInfo.seeDist || dist <= pDudeInfo.hearDist) {
                int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

                // is there a line of sight to the player?
                if (engine.cansee(x, y, z, nSector, pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                    int nAngle = EngineUtils.getAngle(dx, dy);
                    int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;

                    if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                        dudeExtra.setThinkTime(0);
                        aiSetTarget(pXSprite, pPlayer.nSprite);
                        aiActivateDude((BloodSprite) pSprite, pXSprite);
                        return;
                    }

                    if (dist < pDudeInfo.hearDist) {
                        dudeExtra.setThinkTime(0);
                        aiSetTarget(pXSprite, x, y, z);
                        aiActivateDude((BloodSprite) pSprite, pXSprite);
                        return;
                    }
                }
            }
        }
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget2((BloodSprite) pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery)
            aiNewState(pSprite, pXSprite, gargoyleSearch);

        aiThinkTarget((BloodSprite) pSprite, pXSprite);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, gargoyleGoto);
            return;
        }

        int dx, dy, dist;
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, gargoyleSearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, gargoyleSearch);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            GetSpriteExtents(pSprite);

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());
                    int floorz = engine.getflorzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY());

                    switch (pSprite.getLotag()) {
                        case kDudeFleshGargoyle:
                            if (dist < 6144 && dist > 3072 && klabs(losAngle) < kAngle15) {
                                int nInfo = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                                if (nInfo == SS_WALL || nInfo == SS_MASKED) return;

                                if (nInfo != SS_SPRITE || (nInfo == SS_SPRITE && pSprite.getLotag() != boardService.getSprite(pHitInfo.hitsprite).getLotag() && boardService.getSprite(pHitInfo.hitsprite).getLotag() != kDudeStoneGargoyle)) {
                                    sfxStart3DSound(pSprite, 1408, 0, 0);
                                    aiNewState(pSprite, pXSprite, gargoyleFThrow);
                                }
                            } else {
                                if (dist < 1024) {
                                    if (klabs(losAngle) < kAngle15) {
                                        int nInfo = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                                        if (nInfo == SS_WALL || nInfo == SS_MASKED) return;
                                        if (nInfo != SS_SPRITE || (nInfo == SS_SPRITE && pSprite.getLotag() != boardService.getSprite(pHitInfo.hitsprite).getLotag() && boardService.getSprite(pHitInfo.hitsprite).getLotag() != kDudeStoneGargoyle)) {
                                            sfxStart3DSound(pSprite, 1406, 0, 0);
                                            aiNewState(pSprite, pXSprite, gargoyleFSlash);
                                        }

                                    }
                                } else {
                                    int targetZ = pDudeInfo.eyeHeight * pTarget.getYrepeat() << 2;
                                    if (targetZ - eyeAboveZ <= 0x2000 && floorz - extents_zBot <= 0x2000 || dist >= 5120 || dist <= 2560) {
                                        if (targetZ - eyeAboveZ < 0x2000 || floorz - extents_zBot < 0x2000) {
                                            if (klabs(losAngle) < kAngle15) aiPlaySound((BloodSprite) pSprite, 1400, 1, -1);
                                        }
                                    } else {
                                        aiPlaySound((BloodSprite) pSprite, 1400, 1, -1);
                                        aiNewState(pSprite, pXSprite, gargoyleDown);
                                    }
                                }
                            }
                            break;
                        case kDudeStoneGargoyle:
                            if (dist < 6144 && dist > 3072 && klabs(losAngle) < kAngle15) {
                                int nInfo = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                                if (nInfo == SS_WALL || nInfo == SS_MASKED) return;

                                if (nInfo != SS_SPRITE || (nInfo == SS_SPRITE && pSprite.getLotag() != boardService.getSprite(pHitInfo.hitsprite).getLotag() && boardService.getSprite(pHitInfo.hitsprite).getLotag() != kDudeFleshGargoyle)) {
                                    sfxStart3DSound(pSprite, 1457, 0, 0);
                                    aiNewState(pSprite, pXSprite, gargoyleSThrow);
                                }
                            } else {
                                if (dist < 1024) {
                                    if (klabs(losAngle) < kAngle15) {
                                        int nInfo = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                                        if (nInfo == SS_WALL || nInfo == SS_MASKED) return;
                                        if (nInfo != SS_SPRITE || (nInfo == SS_SPRITE && pSprite.getLotag() != boardService.getSprite(pHitInfo.hitsprite).getLotag() && boardService.getSprite(pHitInfo.hitsprite).getLotag() != kDudeFleshGargoyle)) {
                                            sfxStart3DSound(pSprite, 1406, 0, 0);
                                            aiNewState(pSprite, pXSprite, gargoyleFSlash);
                                        }
                                    }
                                } else {
                                    int targetZ = pDudeInfo.eyeHeight * pTarget.getYrepeat() << 2;
                                    if (targetZ - eyeAboveZ <= 0x2000 && floorz - extents_zBot <= 0x2000 || dist >= 5120 || dist <= 2048) {
                                        if (targetZ - eyeAboveZ < 0x2000 || floorz - extents_zBot < 0x2000) {
                                            if (klabs(losAngle) < kAngle15) {
                                                aiPlaySound((BloodSprite) pSprite, 1450, 1, -1);
                                            }
                                        }
                                    } else {
                                        aiPlaySound((BloodSprite) pSprite, 1450, 1, -1);
                                        aiNewState(pSprite, pXSprite, gargoyleDown);
                                    }
                                }
                            }
                            break;
                    }
                }
            } else {
                aiNewState(pSprite, pXSprite, gargoyleUp);
            }
        } else {
            aiNewState(pSprite, pXSprite, gargoyleGoto);
            pXSprite.setTarget(-1);
        }
    }
}

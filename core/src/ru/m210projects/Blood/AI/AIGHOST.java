// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.WeaponAim;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.dmulscale;
import static ru.m210projects.Build.Pragmas.klabs;

public class AIGHOST {

    private static final WeaponAim gSkullData = new WeaponAim(65536, 65536, 256, 85, 0x1AAAAA);
    public static AISTATE ghostIdle;
    public static AISTATE ghostSearch;
    public static AISTATE ghostChase;
    public static AISTATE ghostRTesla;
    public static AISTATE ghostRecoil;
    public static AISTATE ghostGoto;
    public static AISTATE ghostBite;
    public static AISTATE ghostThrow;
    public static AISTATE ghostTurn;
    public static AISTATE ghostUp;
    public static AISTATE ghostDown;

    public static void Init() {
        ghostIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                myThinkTarget(sprite, xsprite);
            }
        };
        ghostChase = new AISTATE(Type.other, kSeqDudeIdle, null, 0, false, true, true, ghostIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        ghostGoto = new AISTATE(Type.tgoto, kSeqDudeIdle, null, 600, false, true, true, ghostIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        ghostBite = new AISTATE(Type.other, 6, nXSprite -> BiteCallback(nXSprite), 120, false, false, false, ghostChase);
        ghostThrow = new AISTATE(Type.other, 6, nXSprite -> ThrowCallback(nXSprite), 120, false, true, false, ghostChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveAttack(sprite, xsprite);
            }
        };
        ghostRTesla = new AISTATE(Type.other, 4, null, 0, false, false, false, ghostChase);
        ghostRecoil = new AISTATE(Type.other, 5, null, 0, false, false, false, ghostChase);

        ghostSearch = new AISTATE(Type.search, kSeqDudeIdle, null, 120, false, true, true, ghostIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        ghostDown = new AISTATE(Type.other, kSeqDudeIdle, null, 120, false, true, true, ghostChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveDown(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        ghostUp = new AISTATE(Type.other, kSeqDudeIdle, null, 0, false, true, true, ghostChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveUp(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        ghostTurn = new AISTATE(Type.other, kSeqDudeIdle, null, 120, false, true, false, ghostChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }
        };
    }

    private static void ThrowCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        Sprite pTarget = pXSprite.getTarget() != -1 ? boardService.getSprite(pXSprite.getTarget()) : null;
        bRandom();

        UpdateEnemyAim(pSprite, nXIndex, gSkullData);

        sfxStart3DSound(pSprite, 489, 0, 0);
        if (IsPlayerSprite(pTarget) || !IsOriginalDemo())
            actFireMissile(pSprite, 0, 0, (int) EnemyAim.x, (int) EnemyAim.y, (int) EnemyAim.z, kMissileEctoSkull);
    }

    private static void BiteCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;

        if (!IsDudeSprite(pSprite)) return;

        int dz = dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight * pSprite.getYrepeat() << 2;

        if (IsOriginalDemo()) {
            if (pXSprite.getTarget() != -1) {
                Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
                if (IsDudeSprite(pTarget))
                    dz -= dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight * pTarget.getYrepeat() << 2;
            }
        } else {
            if (pXSprite.getTarget() != -1) {
                Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
                dz = pTarget.getZ() - pSprite.getZ();
            }
        }

        sfxStart3DSound(pSprite, 1406, 0, 0);
        actFireVector(pSprite, 0, 0, dx, dy, dz, kVectorGhost);
        int fy = dy - Random(50);
        int fx = dx + Random(50);
        actFireVector(pSprite, 0, 0, fx, fy, dz, kVectorGhost);
        fy = dy + Random(50);
        fx = dx - Random(50);
        actFireVector(pSprite, 0, 0, fx, fy, dz, kVectorGhost);
    }

    private static void myMoveForward(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) return;

        if (pXSprite.getTarget() == -1) pSprite.setAng((short) ((pSprite.getAng() + kAngle45) & kAngleMask));

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();

        if (Random(64) >= 32 || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            int fvel = kFrameTicks * pDudeInfo.frontSpeed;

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            if (pXSprite.getTarget() == -1) vel += fvel;
            else vel += fvel >> 1;

            pSprite.setVelocityX(dmulscale(vel, cos, svel, sin, 30));
            pSprite.setVelocityY(dmulscale(vel, sin, -svel, cos, 30));
        }
    }

    private static void myMoveUp(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pSprite.setAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x2000) || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * pDudeInfo.frontSpeed;

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            vel += fvel >> 1;

            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30),
                    dmulscale(vel, sin, -svel, cos, 30),
                    -vel);
        }
    }

    private static void myMoveAttack(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pXSprite.setGoalAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x300) || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * pDudeInfo.frontSpeed;
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            pSprite.setVelocity(dmulscale(fvel >> 1, cos, svel >> 1, sin, 30),
                    dmulscale(fvel >> 1, sin, -(svel >> 1), cos, 30),
                    279620);
        }
    }

    private static void myMoveDown(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pXSprite.setGoalAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x300) || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            int fvel = kFrameTicks * pDudeInfo.frontSpeed;

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            vel += (fvel >> 1);
            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30),
                    dmulscale(vel, sin, -svel, cos, 30), vel);

        }
    }

    private static void myThinkTarget(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        DudeExtra dudeExtra = pXSprite.getDudeExtra();

        if (dudeExtra.isActive()) {
            if (dudeExtra.getThinkTime() >= 10) {
                pXSprite.setGoalAng(pXSprite.getGoalAng() + kAngle45);
                Vector3 kSprite = pSprite.getKSprite();
                aiSetTarget(pXSprite, (int) kSprite.x, (int) kSprite.y, (int) kSprite.z);
                aiNewState(pSprite, pXSprite, ghostTurn);
                return;
            } else {
                dudeExtra.setThinkTime(dudeExtra.getThinkTime() + 1);
            }
        }

        if (!Chance(pDudeInfo.alertChance / 2)) return;

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            PLAYER pPlayer = gPlayer[i];

            if (pPlayer.pXsprite.getHealth() == 0 || powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0)
                continue;

            int x = pPlayer.pSprite.getX();
            int y = pPlayer.pSprite.getY();
            int z = pPlayer.pSprite.getZ();
            short nSector = pPlayer.pSprite.getSectnum();

            int dx = x - pSprite.getX();
            int dy = y - pSprite.getY();

            int dist = EngineUtils.qdist(dx, dy);

            if (dist <= pDudeInfo.seeDist || dist <= pDudeInfo.hearDist) {
                int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

                // is there a line of sight to the player?
                if (engine.cansee(x, y, z, nSector, pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                    int nAngle = EngineUtils.getAngle(dx, dy);
                    int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;

                    if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                        dudeExtra.setThinkTime(0);
                        aiSetTarget(pXSprite, pPlayer.nSprite);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }

                    if (dist < pDudeInfo.hearDist) {
                        dudeExtra.setThinkTime(0);
                        aiSetTarget(pXSprite, x, y, z);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }
                }
            }
        }
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery)
            aiNewState(pSprite, pXSprite, ghostSearch);

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, ghostGoto);
            return;
        }

        int dx, dy, dist;
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, ghostSearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, ghostSearch);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            GetSpriteExtents(pSprite);

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());
                    int floorz = engine.getflorzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY());

                    if (dist > 4096 && dist < 8192 && klabs(losAngle) < kAngle15) {
                        int nInfo = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                        if (nInfo == SS_WALL || nInfo == SS_MASKED) return;
                        if (nInfo != SS_SPRITE || (nInfo == SS_SPRITE && pSprite.getLotag() != boardService.getSprite(pHitInfo.hitsprite).getLotag() && boardService.getSprite(pHitInfo.hitsprite).getLotag() != kDudePhantasm))
                            aiNewState(pSprite, pXSprite, ghostThrow);

                        return;
                    }

                    if (dist < 1024 && klabs(losAngle) < kAngle15) {
                        int nInfo = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                        if (nInfo == SS_WALL || nInfo == SS_MASKED) return;

                        if (nInfo != SS_SPRITE || (nInfo == SS_SPRITE && pSprite.getLotag() != boardService.getSprite(pHitInfo.hitsprite).getLotag() && boardService.getSprite(pHitInfo.hitsprite).getLotag() != kDudePhantasm))
                            aiNewState(pSprite, pXSprite, ghostBite);

                        return;
                    }

                    int targetZ = pDudeInfo.eyeHeight * pTarget.getYrepeat() << 2;
                    if (targetZ - eyeAboveZ <= 0x2000 && floorz - extents_zBot <= 0x2000 || dist >= 5120 || dist <= 2048) {
                        if (targetZ - eyeAboveZ < 0x2000 || floorz - extents_zBot < 0x2000) {
                            if (klabs(losAngle) < kAngle15) aiPlaySound(pSprite, 1600, 1, -1);
                        }
                    } else {
                        aiPlaySound(pSprite, 1600, 1, -1);
                        aiNewState(pSprite, pXSprite, ghostDown);
                    }

                }
            } else {
                aiNewState(pSprite, pXSprite, ghostUp);
            }
        } else {
            aiNewState(pSprite, pXSprite, ghostGoto);
            pXSprite.setTarget(-1);
        }
    }
}

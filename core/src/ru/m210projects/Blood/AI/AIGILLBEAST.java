// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSECTOR;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.Pragmas.*;

public class AIGILLBEAST {

    public static AISTATE[] gillBeastIdle = new AISTATE[2];
    public static AISTATE[] gillBeastChase = new AISTATE[2];
    public static AISTATE[] gillBeastSearch = new AISTATE[2];
    public static AISTATE[] gillBeastGoto = new AISTATE[2];
    public static AISTATE[] gillBeastDodge = new AISTATE[2];
    public static AISTATE[] gillBeastHack = new AISTATE[2];
    public static AISTATE[] gillBeastRecoil = new AISTATE[2];
    public static AISTATE gillBeastAttack;

    public static void Init() {
        gillBeastIdle[LAND] = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        gillBeastChase[LAND] = new AISTATE(Type.other, 9, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkLandChase(sprite, xsprite);
            }
        };
        gillBeastDodge[LAND] = new AISTATE(Type.other, 9, null, 90, false, true, false, gillBeastChase[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        gillBeastGoto[LAND] = new AISTATE(Type.tgoto, 9, null, 600, false, true, true, gillBeastIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkLandGoto(sprite, xsprite);
            }
        };
        gillBeastHack[LAND] = new AISTATE(Type.other, 6, nXSprite -> HackCallback(nXSprite), 120, false, false, false, gillBeastChase[LAND]);
        gillBeastSearch[LAND] = new AISTATE(Type.search, 9, null, 120, false, true, true, gillBeastIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        gillBeastRecoil[LAND] = new AISTATE(Type.other, 5, null, 0, false, false, false, gillBeastDodge[LAND]);
        gillBeastIdle[WATER] = new AISTATE(Type.idle, 10, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        gillBeastChase[WATER] = new AISTATE(Type.other, 10, null, 600, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveChase(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkWaterChase(sprite, xsprite);
            }
        };
        gillBeastDodge[WATER] = new AISTATE(Type.other, 10, null, 90, false, true, false, gillBeastChase[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        gillBeastGoto[WATER] = new AISTATE(Type.tgoto, 10, null, 600, false, true, true, gillBeastIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkWaterGoto(sprite, xsprite);
            }
        };
        gillBeastSearch[WATER] = new AISTATE(Type.search, 10, null, 120, false, true, true, gillBeastIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        gillBeastHack[WATER] = new AISTATE(Type.other, 7, nXSprite -> HackCallback(nXSprite), 0, false, false, true, gillBeastChase[WATER]) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkWaterChase(sprite, xsprite);
            }
        };
        gillBeastRecoil[WATER] = new AISTATE(Type.other, 5, null, 0, false, false, false, gillBeastDodge[WATER]);

        gillBeastAttack = new AISTATE(Type.other, 10, null, 0, false, true, true, gillBeastChase[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                myMoveTarget(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkWaterChase(sprite, xsprite);
            }
        };
    }

    private static void myMoveTarget(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            pSprite.setAng((short) ((pSprite.getAng() + kAngle90) & kAngleMask));
            return;
        }

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (!Chance(0x2000) || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long vel = (long) kFrameTicks * (pDudeInfo.frontSpeed - (divscale(kFrameTicks - pGameInfo.nDifficulty, kTimerRate, 27) / kTimerRate));
            vel = (vel >> 1) + dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            int z1 = 0;
            if (pXSprite.getTarget() != -1) {
                Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
                z1 = pTarget.getZ();
                if (IsDudeSprite(pTarget)) z1 += dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight;
            }
            int z2 = pSprite.getZ() + pDudeInfo.eyeHeight;

            pSprite.setVelocity(dmulscale(vel, cos, svel, sin, 30), dmulscale(vel, sin, -svel, cos, 30), 8L * (z1 - z2));
        }
    }


    private static void aiMoveChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        if (klabs(dang) > kAngle60) return;

        if (pXSprite.getTarget() == -1) pSprite.setAng((short) ((pSprite.getAng() + kAngle45) & kAngleMask));

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();
        if (Random(64) >= 32 || EngineUtils.qdist(dx, dy) > 1024) {
            int sin = Sin(pSprite.getAng());
            int cos = Cos(pSprite.getAng());

            long fvel = (long) kFrameTicks * (pDudeInfo.frontSpeed - (divscale(kFrameTicks - pGameInfo.nDifficulty, kTimerRate, 27) / kTimerRate));

            long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
            long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

            if (pXSprite.getTarget() == -1) vel += fvel;
            else vel += fvel >> 2;

            pSprite.setVelocityX(dmulscale(vel, cos, svel, sin, 30));
            pSprite.setVelocityY(dmulscale(vel, sin, -svel, cos, 30));
        }
    }

    private static void thinkLandGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
            XSECTOR pXSector = boardService.getSector(pSprite.getSectnum()).getExtra() >= 0 ? xsector[boardService.getSector(pSprite.getSectnum()).getExtra()] : null;
            if (pXSector != null && pXSector.Underwater) aiNewState(pSprite, pXSprite, gillBeastSearch[WATER]);
            else aiNewState(pSprite, pXSprite, gillBeastSearch[LAND]);
        }

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkLandChase(BloodSprite pSprite, XSPRITE pXSprite) {
        XSECTOR pXSector = boardService.getSector(pSprite.getSectnum()).getExtra() >= 0 ? xsector[boardService.getSector(pSprite.getSectnum()).getExtra()] : null;
        if (pXSprite.getTarget() == -1) {
            if (pXSector != null && pXSector.Underwater) aiNewState(pSprite, pXSprite, gillBeastSearch[WATER]);
            else aiNewState(pSprite, pXSprite, gillBeastSearch[LAND]);
            return;
        }

        int dx, dy, dist;
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            if (pXSector != null && pXSector.Underwater) aiNewState(pSprite, pXSprite, gillBeastSearch[WATER]);
            else aiNewState(pSprite, pXSprite, gillBeastSearch[LAND]);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                if (pXSector != null && pXSector.Underwater) aiNewState(pSprite, pXSprite, gillBeastSearch[WATER]);
                else aiNewState(pSprite, pXSprite, gillBeastSearch[LAND]);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());
                    int dz = pTarget.getZ() - pSprite.getZ();
                    int tan = divscale(dz, dist, 10);
                    DudeExtra pDudeExtra = pXSprite.getDudeExtra();
                    pDudeExtra.setDudeSlope(tan);

                    if (dist < 921 && klabs(losAngle) < kAngle5) {
                        int nInfo = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                        if (nInfo == SS_SPRITE && pSprite.getLotag() == boardService.getSprite(pHitInfo.hitsprite).getLotag()) {
                            if (pXSector != null && pXSector.Underwater)
                                aiNewState(pSprite, pXSprite, gillBeastDodge[WATER]);
                            else aiNewState(pSprite, pXSprite, gillBeastDodge[LAND]);
                            return;
                        }
                        if (pXSector != null && pXSector.Underwater)
                            aiNewState(pSprite, pXSprite, gillBeastHack[WATER]);
                        else aiNewState(pSprite, pXSprite, gillBeastHack[LAND]);
                    }
                }
                return;
            }
        }
        if (pXSector != null && pXSector.Underwater) aiNewState(pSprite, pXSprite, gillBeastGoto[WATER]);
        else aiNewState(pSprite, pXSprite, gillBeastGoto[LAND]);
        sfxStart3DSound(pSprite, 1701, -1, 0);
        pXSprite.setTarget(-1);
    }

    private static void thinkWaterChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, gillBeastGoto[WATER]);
            return;
        }

        int dx, dy, dist;
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, gillBeastSearch[WATER]);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, gillBeastSearch[WATER]);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), -eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());
                    if (dist < 1024 && klabs(losAngle) < kAngle15) {
                        aiNewState(pSprite, pXSprite, gillBeastHack[WATER]);
                        return;
                    }
                    aiPlaySound(pSprite, 1700, 1, -1);
                    aiNewState(pSprite, pXSprite, gillBeastAttack);
                }
            } else aiNewState(pSprite, pXSprite, gillBeastAttack);

            return;
        }

        aiNewState(pSprite, pXSprite, gillBeastGoto[WATER]);
        pXSprite.setTarget(-1);
    }

    private static void thinkWaterGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
            aiNewState(pSprite, pXSprite, gillBeastSearch[WATER]);
        }

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void HackCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = pSprite.getZ();
        if (pXSprite.getTarget() != -1) {
            Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
            dz -= pTarget.getZ();
        }

        dx += BiRandom2(2000);
        dy += BiRandom2(2000);

        actFireVector(pSprite, 0, 0, dx, dy, dz, 8);
        actFireVector(pSprite, 0, 0, dx, dy, dz, 8);
        actFireVector(pSprite, 0, 0, dx, dy, dz, 8);
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget(pSprite, pXSprite);
    }

}

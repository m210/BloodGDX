// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.IsPlayerSprite;
import static ru.m210projects.Blood.Gameutils.M2X;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.Pragmas.klabs;

public class AIHOUND {

    public static AISTATE houndIdle;
    public static AISTATE houndSearch;
    public static AISTATE houndChase;
    public static AISTATE houndRecoil;
    public static AISTATE houndRTesla;
    public static AISTATE houndGoto;
    public static AISTATE houndBite;
    public static AISTATE houndBurn;

    public static void Init() {
        houndIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        houndSearch = new AISTATE(Type.search, 8, null, 1800, false, true, true, houndIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        houndChase = new AISTATE(Type.other, 8, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        houndRecoil = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, houndSearch);
        houndRTesla = new AISTATE(Type.other, kSeqDudeTesla, null, 0, false, false, false, houndSearch);

        houndGoto = new AISTATE(Type.tgoto, 8, null, 600, false, true, true, houndIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        houndBite = new AISTATE(Type.other, 6, nXSprite -> BiteCallback(nXSprite), 60, false, false, false, houndChase);
        houndBurn = new AISTATE(Type.other, 7, nXSprite -> BurnCallback(nXSprite), 60, false, false, false, houndChase);
    }

    private static void BiteCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = pTarget.getZ() - pSprite.getZ();
        if (IsPlayerSprite(pTarget) || !IsOriginalDemo()) actFireVector(pSprite, 0, 0, dx, dy, dz, kVectorHoundBite);
    }

    private static void BurnCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = 0;

        actFireMissile(pSprite, 0, 0, dx, dy, dz, kMissileHoundFire);
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery)
            aiNewState(pSprite, pXSprite, houndSearch);

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, houndGoto);
            return;
        }

        int dx, dy, dist;
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, houndSearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, houndSearch);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());

                    if (dist < 2816 && dist > 1280 && klabs(losAngle) < kAngle15)
                        aiNewState(pSprite, pXSprite, houndBurn);
                    else if (dist < 614 && klabs(losAngle) < kAngle15) aiNewState(pSprite, pXSprite, houndBite);

                    return;
                }
            }
        }

        aiNewState(pSprite, pXSprite, houndGoto);
        pXSprite.setTarget(-1);
    }

}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqSpawn;
import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Build.Pragmas.klabs;

public class AIPOD {

    public static final int kSlopeThrow = -14500;

    public static AISTATE podIdle;
    public static AISTATE podSearch;
    public static AISTATE podChase;
    public static AISTATE podRecoil;
    public static AISTATE podGoto;
    public static AISTATE podHack;

    public static AISTATE tentacleIdle;
    public static AISTATE tentacleSearch;
    public static AISTATE tentacleChase;
    public static AISTATE tentacleRecoil;
    public static AISTATE tentacleDown;
    public static AISTATE tentacleHack;

    public static void Init() {
        podIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        podSearch = new AISTATE(Type.search, kSeqDudeIdle, null, 3600, false, true, true, podSearch) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        podSearch.next = podSearch;
        podGoto = new AISTATE(Type.tgoto, 7, null, 3600, false, true, true, podSearch) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        podChase = new AISTATE(Type.other, 6, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        podHack = new AISTATE(Type.other, 8, nXSprite -> pHackCallback(nXSprite), 600, false, false, false, podChase);
        podRecoil = new AISTATE(Type.other, 5, null, 0, false, false, false, podChase);


        tentacleIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        tentacleSearch = new AISTATE(Type.search, kSeqDudeIdle, null, 3600, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        tentacleDown = new AISTATE(Type.other, 8, null, 3600, false, true, true, tentacleSearch) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        tentacleChase = new AISTATE(Type.other, 6, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        tentacleHack = new AISTATE(Type.other, 6, nXSprite -> tHackCallback(nXSprite), 120, false, false, false, tentacleChase);
        tentacleRecoil = new AISTATE(Type.other, 5, null, 0, false, false, false, tentacleChase);
    }


    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
            switch (pSprite.getLotag()) {
                case kDudeGreenPod:
                case kDudeFirePod:
                    aiNewState(pSprite, pXSprite, podSearch);
                    break;
                case kDudeGreenTentacle:
                case kDudeFireTentacle:
                    aiNewState(pSprite, pXSprite, tentacleSearch);
                    break;
            }
        }
        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            switch (pSprite.getLotag()) {
                case kDudeGreenPod:
                case kDudeFirePod:
                    aiNewState(pSprite, pXSprite, podGoto);
                    break;
                case kDudeGreenTentacle:
                case kDudeFireTentacle:
                    aiNewState(pSprite, pXSprite, tentacleDown);
                    break;
            }
            return;
        }

        int dx, dy, dist;

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            switch (pSprite.getLotag()) {
                case kDudeGreenPod:
                case kDudeFirePod:
                    aiNewState(pSprite, pXSprite, podSearch);
                    break;
                case kDudeGreenTentacle:
                case kDudeFireTentacle:
                    aiNewState(pSprite, pXSprite, tentacleSearch);
                    break;
            }
            return;
        }

        dist = EngineUtils.qdist(dx, dy);

        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());

                    // check to see if we can attack
                    if (klabs(losAngle) < kAngle15) {
                        if (pTarget.getLotag() != 221 && pTarget.getLotag() != 223) {
                            switch (pSprite.getLotag()) {
                                case kDudeGreenPod:
                                case kDudeFirePod:
                                    aiNewState(pSprite, pXSprite, podHack);
                                    break;
                                case kDudeGreenTentacle:
                                case kDudeFireTentacle:
                                    aiNewState(pSprite, pXSprite, tentacleHack);
                                    break;
                            }
                        }
                    }
                    return;
                }
            }
        }

        switch (pSprite.getLotag()) {
            case kDudeGreenPod:
            case kDudeFirePod:
                aiNewState(pSprite, pXSprite, podGoto);
                break;
            case kDudeGreenTentacle:
            case kDudeFireTentacle:
                aiNewState(pSprite, pXSprite, tentacleDown);
                break;
        }
        pXSprite.setTarget(-1);
    }

    private static void tHackCallback(int nXSprite) {
        int nSprite = boardService.getXSprite(nXSprite).getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        sfxStart3DSound(pSprite, 2502, -1, 0);       // tentacle sweeps
        int nDamageType, nDistance, nBurnTime, nDamageHit = 5 * pGameInfo.nDifficulty + 5;
        if (pSprite.getLotag() == kDudeGreenTentacle) {
            nDamageType = 2;
            nDistance = 50;
            nBurnTime = 0;
        } else {
            nDamageType = 3;
            nDistance = 75;
            nBurnTime = 120 * pGameInfo.nDifficulty;
        }
        actDistanceDamage(nSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), nDistance, 1, nDamageType, nDamageHit, 2, nBurnTime);
    }

    private static void pHackCallback(int nXSprite) {
        XSPRITE pXSprite = boardService.getXSprite(nXSprite);
        Sprite pSprite = boardService.getSprite(pXSprite.getReference());
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        int nTarget = pXSprite.getTarget();
        Sprite pTarget = boardService.getSprite(nTarget);
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - 200];

        int dx = pTarget.getX() - pSprite.getX();
        int dy = pTarget.getY() - pSprite.getY();
        int dz = pTarget.getZ() - pSprite.getZ();

        dx += BiRandom(1000);
        dy += BiRandom(1000);
        dz += 8000;

        int dist = EngineUtils.qdist(dx, dy);
        if (dist > pDudeInfo.seeDist / 10) {

            int nSlope = (dz / 128) + kSlopeThrow;
            Sprite pThing = null;
            int velocity = divscale(dist / 540, 120, 23);

            switch (pSprite.getLotag()) {
                case kDudeGreenPod:
                    if (Chance(0x4000)) sfxStart3DSound(pSprite, 2474, -1, 0);
                    else sfxStart3DSound(pSprite, 2475, -1, 0);

                    //if(gFrameClock == 131804) {
                    //System.err.println(pSprite.xvel);
                    //}

                    pThing = actFireThing(pSprite.getXvel(), 0, -8000, nSlope, kThingPodGreen, velocity);
                    if (pThing != null) seqSpawn(68, SS_SPRITE, pThing.getExtra(), null);
                    break;
                case kDudeFirePod:
                    sfxStart3DSound(pSprite, 2454, -1, 0);
                    pThing = actFireThing(pSprite.getXvel(), 0, -8000, nSlope, kThingPodFire, velocity);
                    if (pThing != null) seqSpawn(22, SS_SPRITE, pThing.getExtra(), null);
                    break;
            }
        }
        for (int i = 0; i < 4; i++)
            actSpawnTentacleBlood(pSprite);
    }
}

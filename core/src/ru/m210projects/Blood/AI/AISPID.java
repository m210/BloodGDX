// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.totalKills;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.powerupActivate;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.Pragmas.klabs;

public class AISPID {

    public static AISTATE spidIdle;
    public static AISTATE spidSearch;
    public static AISTATE spidChase;
    public static AISTATE spidDodge;
    public static AISTATE spidRecoil;
    public static AISTATE spidGoto;
    public static AISTATE spidBite;
    public static AISTATE spidJump;
    public static AISTATE spidSpawn;

    public static void Init() {
        spidIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        spidChase = new AISTATE(Type.other, 7, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        spidDodge = new AISTATE(Type.other, 7, null, 90, false, true, false, spidChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        spidGoto = new AISTATE(Type.tgoto, 7, null, 600, false, true, true, spidIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        spidSearch = new AISTATE(Type.search, 7, null, 1800, false, true, true, spidIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        spidBite = new AISTATE(Type.other, 6, nXSprite -> BiteCallback(nXSprite), 60, false, false, false, spidChase);

        spidJump = new AISTATE(Type.other, 8, nXSprite -> JumpCallback(nXSprite), 60, false, true, false, spidChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }
        };
        spidSpawn = new AISTATE(Type.other, 0, nXSprite -> SpawnCallback(nXSprite), 60, false, false, false, spidIdle);
    }

    private static void SpawnCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        DudeExtra dudeExtra = pXSprite.getDudeExtra();

        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());

        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        int dist = EngineUtils.qdist(dx, dy);

        Sprite pSpawned = null;
        if (IsPlayerSprite(pTarget)) {
            if (dudeExtra.getThinkTime() < 10) {
                int losAngle = pSprite.getAng() - nAngle;
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    if (dist < 6656 && dist > 5120)
                        pSpawned = actSpawnDude(pSprite, kDudeRedSpider, pSprite.getClipdist());
                    else if (dist < 3072 || (dist < 5120 && dist > 3072))
                        pSpawned = actSpawnDude(pSprite, kDudeBrownSpider, pSprite.getClipdist());
                }
            }
        }

        if (pSpawned != null) {
            dudeExtra.setThinkTime(dudeExtra.getThinkTime() + 1);
            pSpawned.setOwner((short) nSprite);
            totalKills++;
        }
    }

    private static void JumpCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null || !IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = 0;

        dx += BiRandom(200);
        dy += BiRandom(200);
        dz += BiRandom(200);

        if (IsPlayerSprite(pTarget)) {
            if (pSprite.getLotag() == kDudeBrownSpider || pSprite.getLotag() == kDudeBlackSpider) {
                dz += pTarget.getZ() - pSprite.getZ();
                pSprite.setVelocity((long) dx <<16, (long) dy << 16, (long) dz << 16);
            }
        }
    }


    private static void spidAttack(XSPRITE pXDude, int add, int max) {
        if (pXDude == null) throw new AssertException("pXDude != null");
        Sprite pDude = boardService.getSprite(pXDude.getReference());
        if (IsPlayerSprite(pDude)) {
            PLAYER pPlayer = gPlayer[pDude.getLotag() - kDudePlayer1];
            pPlayer.blindEffect = ClipHigh(pPlayer.blindEffect + (add << 4), max << 4);
        }
    }

    private static void BiteCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = 0;

        // dispersal modifiers here
        dx += BiRandom(2000);
        dy += BiRandom(2000);
        dz += BiRandom(2000);

        if (IsPlayerSprite(pTarget)) {
            if (HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0) == SS_SPRITE) {
                if (IsPlayerSprite(boardService.getSprite(pHitInfo.hitsprite))) {
                    PLAYER pPlayer = gPlayer[boardService.getSprite(pHitInfo.hitsprite).getLotag() - kDudePlayer1];
                    XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());
                    switch (pSprite.getLotag()) {
                        case kDudeBrownSpider:
                            actFireVector(pSprite, 0, 0, dx, dy, pTarget.getZ() - pSprite.getZ() + dz, kVectorSpiderBite);
                            if (!pPlayer.godMode) {
                                if (powerupCheck(pPlayer, kItemInvulnerability - kItemBase) <= 0) {
                                    if (Chance(0x2000)) powerupActivate(pPlayer, kItemShroomDelirium - kItemBase);
                                }
                            }
                            break;
                        case kDudeRedSpider:
                            actFireVector(pSprite, 0, 0, dx, dy, pTarget.getZ() - pSprite.getZ() + dz, kVectorSpiderBite);
                            if (Chance(0x2800)) spidAttack(pXTarget, 4, 16);
                            break;
                        case kDudeBlackSpider:
                            actFireVector(pSprite, 0, 0, dx, dy, pTarget.getZ() - pSprite.getZ() + dz, kVectorSpiderBite);
                            spidAttack(pXTarget, 8, 16);
                            break;
                        case kDudeMotherSpider:
                            actFireVector(pSprite, 0, 0, dx, dy, pTarget.getZ() - pSprite.getZ() + dz, kVectorSpiderBite);
                            actFireVector(pSprite, 0, 0, BiRandom(0x2000) + dx, BiRandom(0x2000) + dy, BiRandom(0x2000) + (pTarget.getZ() - pSprite.getZ() + dz), kVectorSpiderBite);
                            spidAttack(pXTarget, 8, 16);
                            break;
                    }
                }
            }
        }
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) return;
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery)
            aiNewState(pSprite, pXSprite, spidSearch);

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, spidGoto);
            return;
        }

        int dx, dy, dist;

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, spidSearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, spidSearch);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());

                    switch (pSprite.getLotag()) {
                        case kDudeRedSpider:
                            if (dist < 921 && klabs(losAngle) < kAngle15) aiNewState(pSprite, pXSprite, spidBite);
                            break;
                        case kDudeBrownSpider:
                        case kDudeBlackSpider:
                            if (dist > 921 && dist < 1843) {
                                if (klabs(losAngle) < kAngle15) aiNewState(pSprite, pXSprite, spidJump);
                            } else {
                                if (dist < 921 && klabs(losAngle) < kAngle15) aiNewState(pSprite, pXSprite, spidBite);
                            }
                            break;
                        case kDudeMotherSpider:
                            if (dist > 921 && dist < 1843 && klabs(losAngle) < kAngle15)
                                aiNewState(pSprite, pXSprite, spidJump);
                            else if (Chance(0x4000)) aiNewState(pSprite, pXSprite, spidSpawn);
                            break;
                    }

                    return;
                }
            }
        }

        aiNewState(pSprite, pXSprite, spidGoto);
        pXSprite.setTarget(-1);
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.WeaponAim;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.evPostCallback;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Weapon.kAimMaxSlope;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.*;

public class AITCHERNOBOG {

    private static final WeaponAim gMissileData = new WeaponAim(65536, 65536, 256, 85, 0x100000);
    public static AISTATE tchernobogIdle;
    public static AISTATE tchernobogSearch;
    public static AISTATE tchernobogChase;
    public static AISTATE tchernobogDodge;
    public static AISTATE tchernobogRecoil;
    public static AISTATE tchernobogGoto;
    public static AISTATE tchernobogTurn;
    public static AISTATE tchernobogBurn;
    public static AISTATE tchernobogShoot;
    public static AISTATE tchernobogShootTarget;

    public static void Init() {
        tchernobogIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                myThinkTarget(sprite, xsprite);
            }
        };
        tchernobogSearch = new AISTATE(Type.search, 8, null, 1800, false, true, true, tchernobogIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        tchernobogChase = new AISTATE(Type.other, 8, null, 1800, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        tchernobogRecoil = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, tchernobogSearch);
        tchernobogGoto = new AISTATE(Type.tgoto, 8, null, 600, false, true, true, tchernobogIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        tchernobogShoot = new AISTATE(Type.other, 6, nXSprite -> ShootCallback(nXSprite), 60, false, false, false, tchernobogChase);
        tchernobogShootTarget = new AISTATE(Type.other, 6, nXSprite -> ShootTargetCallback(nXSprite), 60, false, false, false, tchernobogChase);
        tchernobogBurn = new AISTATE(Type.other, 7, nXSprite -> BurnCallback(nXSprite), 60, false, false, false, tchernobogChase);

        tchernobogTurn = new AISTATE(Type.other, 8, null, 60, false, true, false, tchernobogChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }
        };
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, tchernobogGoto);
            return;
        }

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        if (pTarget == null) {
            aiNewState(pSprite, pXSprite, tchernobogSearch);
            return;
        }

        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());
        int dx, dy, dist;

        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, tchernobogSearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, tchernobogSearch);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);

        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());
                    if (klabs(losAngle) < kAngle15) {
                        if (dist > 1280 && dist < 2816) aiNewState(pSprite, pXSprite, tchernobogShoot);
                        else if (dist > 2816 && dist < 3328) aiNewState(pSprite, pXSprite, tchernobogShootTarget);
                        else if (dist > 3328 && dist < 7936) aiNewState(pSprite, pXSprite, tchernobogBurn);
                    }
                    return;
                }
            }
        }
        aiNewState(pSprite, pXSprite, tchernobogGoto);
        pXSprite.setTarget(-1);
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget((BloodSprite) pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        if (dist < 512 && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery)
            aiNewState(pSprite, pXSprite, tchernobogSearch);

        aiThinkTarget((BloodSprite) pSprite, pXSprite);
    }

    private static void myThinkTarget(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        DudeExtra dudeExtra = pXSprite.getDudeExtra();

        if (dudeExtra.isActive()) {
            if (dudeExtra.getThinkTime() >= 10) {
                pXSprite.setGoalAng(pXSprite.getGoalAng() + kAngle45);
                Vector3 kSprite = ((BloodSprite) pSprite).getKSprite();
                aiSetTarget(pXSprite, (int) kSprite.x, (int) kSprite.y, (int) kSprite.z);
                aiNewState(pSprite, pXSprite, tchernobogTurn);
                return;
            } else { dudeExtra.setThinkTime(dudeExtra.getThinkTime() + 1); }
        }

        if (!Chance(pDudeInfo.alertChance / 2)) return;

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            PLAYER pPlayer = gPlayer[i];

            if (pPlayer.pXsprite.getHealth() == 0 || powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0)
                continue;

            int x = pPlayer.pSprite.getX();
            int y = pPlayer.pSprite.getY();
            int z = pPlayer.pSprite.getZ();
            short nSector = pPlayer.pSprite.getSectnum();

            int dx = x - pSprite.getX();
            int dy = y - pSprite.getY();

            int dist = EngineUtils.qdist(dx, dy);

            if (dist <= pDudeInfo.seeDist || dist <= pDudeInfo.hearDist) {
                int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

                // is there a line of sight to the player?
                if (engine.cansee(x, y, z, nSector, pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                    int nAngle = EngineUtils.getAngle(dx, dy);
                    int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;

                    if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                        dudeExtra.setThinkTime(0);
                        aiSetTarget(pXSprite, pPlayer.nSprite);
                        aiActivateDude((BloodSprite) pSprite, pXSprite);
                        return;
                    }

                    if (dist < pDudeInfo.hearDist) {
                        dudeExtra.setThinkTime(0);
                        aiSetTarget(pXSprite, x, y, z);
                        aiActivateDude((BloodSprite) pSprite, pXSprite);
                        return;
                    }
                }
            }
        }
    }

    private static void BurnCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pXSprite.getTarget() == -1) return;
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());
        if (pXTarget == null) return;

        if (pXTarget.getBurnTime() == 0) evPostCallback(pXSprite.getTarget(), 3, 0, 0);

        int burnTime;
        if (pTarget.getStatnum() == kStatDude) burnTime = 2400;
        else burnTime = 1200;

        pXTarget.setBurnTime(ClipHigh(pXTarget.getBurnTime() + 40, burnTime));
        pXTarget.setBurnSource(actSetBurnSource(nSprite));

        if (Chance(12288)) aiNewState(pSprite, pXSprite, tchernobogShootTarget);
    }

    private static void ShootCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeExtra pDudeExtra = pXSprite.getDudeExtra();
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

        int x = pSprite.getX();
        int y = pSprite.getY();

        int ax = Cos(pSprite.getAng()) >> 16;
        int x2 = ax;
        int ay = Sin(pSprite.getAng()) >> 16;
        int y2 = ay;
        int az = pDudeExtra.getDudeSlope();

        int closest = 0x7FFFFFFF;
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            final int nDude = node.getIndex();
            final BloodSprite pDude = (BloodSprite) node.get();

            // don't target yourself!
            if (pDude == pSprite) continue;

            if ((pDude.getHitag() & kAttrAiming) == 0) continue;

            int tx, ty, tz;

            tx = pDude.getX();
            ty = pDude.getY();
            tz = pDude.getZ();

            int dist = EngineUtils.qdist(tx - x, ty - y);

            if (dist == 0 || dist > 10240) continue;

            if (gMissileData.kSeeker != 0) {
                int k = (dist << 12) / gMissileData.kSeeker;
                tx += (int) (k * pDude.getVelocityX() >> 12);
                ty += (int) (k * pDude.getVelocityY() >> 12);
                tz += (int) (k * pDude.getVelocityZ() >> 8);
            }

            int z1 = mulscale(dist, pDudeExtra.getDudeSlope(), 10) + eyeAboveZ;
            int z2 = mulscale(kAimMaxSlope, dist, 10);

            GetSpriteExtents(pDude);

            if ((z1 - z2 > extents_zBot) || (z1 + z2 < extents_zTop)) continue;

            int dx = mulscale(dist, Cos(pSprite.getAng()), 30) + x;
            int dy = mulscale(dist, Sin(pSprite.getAng()), 30) + y;

            int dist2 = EngineUtils.sqrt(((dx - tx) >> 4) * ((dx - tx) >> 4) + ((dy - ty) >> 4) * ((dy - ty) >> 4) + ((z1 - tz) >> 8) * ((z1 - tz) >> 8));

            if (dist2 < closest) {
                int ang = EngineUtils.getAngle(tx - x, ty - y);
                if (klabs(((ang - pSprite.getAng() + kAngle180) & kAngleMask) - kAngle180) > gMissileData.kDudeAngle)
                    continue;

                int dz = pDude.getZ() - pSprite.getZ();

                if (engine.cansee(x, y, eyeAboveZ, pSprite.getSectnum(), tx, ty, tz, pDude.getSectnum())) {
                    closest = dist2;

                    ax = Cos(ang) >> 16;
                    ay = Sin(ang) >> 16;
                    az = divscale(dz, dist, 10);
                } else az = dz;
            }
        }

        actFireMissile(pSprite, 350, 0, ax, ay, -az, kMissileTchernobog2);
        actFireMissile(pSprite, -350, 0, x2, y2, 0, kMissileTchernobog2);
    }

    private static void ShootTargetCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        DudeExtra pDudeExtra = pXSprite.getDudeExtra();
        int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

        int x = pSprite.getX();
        int y = pSprite.getY();

        int ax = Cos(pSprite.getAng()) >> 16;
        int ay = Sin(pSprite.getAng()) >> 16;
        int az = pDudeExtra.getDudeSlope();

        int closest = 0x7FFFFFFF;
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            final BloodSprite pDude = (BloodSprite) node.get();

            // don't target yourself!
            if (pDude == pSprite) continue;

            if ((pDude.getHitag() & kAttrAiming) == 0) continue;

            int tx, ty, tz;

            tx = pDude.getX();
            ty = pDude.getY();
            tz = pDude.getZ();

            int dist = EngineUtils.qdist(tx - x, ty - y);

            if (dist == 0 || dist > 10240) continue;

            if (gMissileData.kSeeker != 0) {
                int k = (dist << 12) / gMissileData.kSeeker;
                tx += (int) (k * pDude.getVelocityX() >> 12);
                ty += (int) (k * pDude.getVelocityY() >> 12);
                tz += (int) (k * pDude.getVelocityZ() >> 8);
            }

            int z1 = mulscale(dist, pDudeExtra.getDudeSlope(), 10) + eyeAboveZ;
            int z2 = mulscale(kAimMaxSlope, dist, 10);

            GetSpriteExtents(pDude);

            if ((z1 - z2 > extents_zBot) || (z1 + z2 < extents_zTop)) continue;

            int dx = mulscale(dist, Cos(pSprite.getAng()), 30) + x;
            int dy = mulscale(dist, Sin(pSprite.getAng()), 30) + y;

            int dist2 = EngineUtils.sqrt(((dx - tx) >> 4) * ((dx - tx) >> 4) + ((dy - ty) >> 4) * ((dy - ty) >> 4) + ((z1 - tz) >> 8) * ((z1 - tz) >> 8));

            if (dist2 < closest) {
                int ang = EngineUtils.getAngle(tx - x, ty - y);
                if (klabs(((ang - pSprite.getAng() + kAngle180) & kAngleMask) - kAngle180) > gMissileData.kDudeAngle)
                    continue;

                int dz = pDude.getZ() - pSprite.getZ();

                if (engine.cansee(x, y, eyeAboveZ, pSprite.getSectnum(), tx, ty, tz, pDude.getSectnum())) {
                    closest = dist2;

                    ax = Cos(ang) >> 16;
                    ay = Sin(ang) >> 16;
                    az = divscale(dz, dist, 10);
                } else az = dz;
            }
        }

        actFireMissile(pSprite, -350, 0, ax, ay, az, kMissileTchernobog2);
        actFireMissile(pSprite, 350, 0, ax, ay, az, kMissileTchernobog2);
    }

}

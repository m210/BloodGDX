package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.*;
import ru.m210projects.Blood.Types.Seq.SeqInst;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.totalKills;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.SOUND.sfxStart3DSoundCP;
import static ru.m210projects.Blood.Strings.seq;
import static ru.m210projects.Blood.Tile.tileLoadVoxel;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Trigger.getTargetDist;
import static ru.m210projects.Blood.Trigger.isActive;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.*;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.Pragmas.*;

public class AIUNICULT {

    public static final int kSlopeThrow = -8192;
    public static final int kMaxGenDudeSndMode = 11;
    public static final GENDUDESND[] gCustomDudeSnd = {new GENDUDESND(1003, 2, 0, true),    // spot sound
            new GENDUDESND(1013, 2, 2, true),    // pain sound
            new GENDUDESND(1018, 2, 4, false),    // death sound
            new GENDUDESND(1031, 2, 6, true),    // burning state sound
            new GENDUDESND(1018, 2, 8, false),    // explosive death or end of burning state sound
            new GENDUDESND(4021, 2, 10, true),    // target of dude is dead
            new GENDUDESND(1005, 2, 12, true),    // chase sound
            new GENDUDESND(-1, 0, 14, false),    // weapon attack
            new GENDUDESND(-1, 0, 15, false),    // throw attack
            new GENDUDESND(-1, 0, 16, false),    // melee attack
            new GENDUDESND(9008, 0, 17, false),    // transforming in other dude
    };
    public static AISTATE[] GDXGenDudeIdle = new AISTATE[3];
    public static AISTATE[] GDXGenDudeSearch = new AISTATE[3];
    public static AISTATE[] GDXGenDudeGoto = new AISTATE[3];
    public static AISTATE[] GDXGenDudeDodge = new AISTATE[3];
    public static AISTATE[] GDXGenDudeDodgeDmg = new AISTATE[3];
    public static AISTATE[] GDXGenDudeChase = new AISTATE[3];
    public static AISTATE[] GDXGenDudeFire = new AISTATE[3];
    public static AISTATE[] GDXGenDudeRecoil = new AISTATE[3];
    public static AISTATE GDGenDudeThrow;
    public static AISTATE GDGenDudeThrow2;
    public static AISTATE GDXGenDudePunch;
    public static AISTATE GDXGenDudeRTesla;

    public static void Init() {

        GDXGenDudeIdle[LAND] = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget((BloodSprite) sprite, xsprite);
            }
        };

        GDXGenDudeIdle[WATER] = new AISTATE(Type.idle, 13, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget((BloodSprite) sprite, xsprite);
            }
        };

        GDXGenDudeSearch[LAND] = new AISTATE(Type.search, 9, null, 600, false, true, true, GDXGenDudeIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiGenDudeMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };

        GDXGenDudeSearch[WATER] = new AISTATE(Type.search, 13, null, 600, false, true, true, GDXGenDudeIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiGenDudeMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };

        GDXGenDudeGoto[LAND] = new AISTATE(Type.other, 9, null, 600, false, true, true, GDXGenDudeIdle[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiGenDudeMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };

        GDXGenDudeGoto[WATER] = new AISTATE(Type.tgoto, 13, null, 600, false, true, true, GDXGenDudeIdle[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiGenDudeMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };

        GDXGenDudeChase[LAND] = new AISTATE(Type.other, 9, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiGenDudeMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };

        GDXGenDudeChase[DUCK] = new AISTATE(Type.other, 14, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiGenDudeMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };

        GDXGenDudeChase[WATER] = new AISTATE(Type.other, 13, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiGenDudeMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };

        GDXGenDudeDodge[LAND] = new AISTATE(Type.other, 9, null, 90, false, true, false, GDXGenDudeChase[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);

            }
        };

        GDXGenDudeDodge[DUCK] = new AISTATE(Type.other, 14, null, 90, false, true, false, GDXGenDudeChase[DUCK]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };

        GDXGenDudeDodge[WATER] = new AISTATE(Type.other, 13, null, 90, false, true, false, GDXGenDudeChase[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };

        // Those is for dodging after receive the damage
        // -----------------------------------------
        GDXGenDudeDodgeDmg[LAND] = new AISTATE(Type.other, 9, null, 60, false, true, false, GDXGenDudeChase[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);

            }
        };

        GDXGenDudeDodgeDmg[DUCK] = new AISTATE(Type.other, 14, null, 60, false, true, false, GDXGenDudeChase[DUCK]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };

        GDXGenDudeDodgeDmg[WATER] = new AISTATE(Type.other, 13, null, 60, false, true, false, GDXGenDudeChase[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }
        };
        // -----------------------------------------

        CALLPROC attack = nXSprite -> GDXCultistAttack1(nXSprite);

        GDXGenDudeFire[LAND] = new AISTATE(Type.other, kSeqDudeAttack, attack, 0, false, true, true, GDXGenDudeFire[LAND]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };

        GDXGenDudeFire[WATER] = new AISTATE(Type.other, 8, attack, 0, false, true, true, GDXGenDudeFire[WATER]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };

        GDXGenDudeFire[DUCK] = new AISTATE(Type.other, 8, attack, 0, false, true, true, GDXGenDudeFire[DUCK]) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };

        GDGenDudeThrow = new AISTATE(Type.other, 7, nXSprite -> ThrowCallback(nXSprite, true), 0, false, false, false, GDXGenDudeChase[LAND]);

        GDGenDudeThrow2 = new AISTATE(Type.other, 7, nXSprite -> ThrowCallback(nXSprite, false), 0, false, false, false, GDXGenDudeChase[LAND]);

        GDXGenDudeRTesla = new AISTATE(Type.other, kSeqDudeTesla, null, 0, false, false, false, GDXGenDudeDodge[LAND]);

        GDXGenDudeRecoil[LAND] = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, GDXGenDudeChase[LAND]);
        GDXGenDudeRecoil[WATER] = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, GDXGenDudeChase[WATER]);
        GDXGenDudeRecoil[DUCK] = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, GDXGenDudeChase[DUCK]);

        GDXGenDudePunch = new AISTATE(Type.other, 10, nXSprite -> {
            punchCallback(nXSprite);
//				punch = true;
        }, 0, false, false, true, GDXGenDudeChase[LAND]) {
            final boolean punch = false;

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                // Required for those who don't have fire trigger in punch seq
                if (!punch && seqFrame(SS_SPRITE, sprite.getExtra()) == -1) {
                    int nXSprite = sprite.getExtra();
                    punchCallback(nXSprite);
                }
            }
        };
    }

    private static void punchCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        int nAngle = EngineUtils.getAngle(pXSprite.getTargetX() - pSprite.getX(), pXSprite.getTargetY() - pSprite.getY());

        int nZOffset1 = dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight;
        int nZOffset2 = 0;
        if (pXSprite.getTarget() != -1) {
            Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
            if (IsDudeSprite(pTarget)) nZOffset2 = dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight;

            int dx = Cos(nAngle) >> 16;
            int dy = Sin(nAngle) >> 16;
            int dz = nZOffset1 - nZOffset2;

            if (!sfxPlayGDXGenDudeSound(pSprite, 9)) sfxStart3DSound(pSprite, 530, 1, 0);

            actFireVector(pSprite, 0, 0, dx, dy, dz, 22);
        }
    }

    private static void GDXCultistAttack1(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        DudeExtra pXDude = pXSprite.getDudeExtra();

        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        int dx, dy, dz;
        int weapon = pXSprite.getData1();
        if (weapon >= 0 && weapon < kVectorMax) {

            dx = Cos(pSprite.getAng()) >> 16;
            dy = Sin(pSprite.getAng()) >> 16;
            dz = pXDude.getDudeSlope();

            VECTORDATA pVectorData = gVectorData[weapon];
            int vdist = pVectorData.maxDist;

            // dispersal modifiers here in case if non-melee enemy
            if (vdist <= 0 || vdist > 1280) {
                dx += BiRandom2(3000 - 1000 * pGameInfo.nDifficulty);
                dy += BiRandom2(3000 - 1000 * pGameInfo.nDifficulty);
                dz += BiRandom2(1000 - 500 * pGameInfo.nDifficulty);
            }

            actFireVector(pSprite, 0, 0, dx, dy, dz, weapon);
            if (!sfxPlayGDXGenDudeSound(pSprite, 7)) sfxPlayVectorSound(pSprite, weapon);

        } else if (weapon >= kDudeBase && weapon < kDudeMax) {

            Sprite pSpawned = null;
            int dist = pSprite.getClipdist() * 6;
            if ((pSpawned = actSpawnDude(pSprite, weapon, dist)) == null) return;
            DudeExtra dudeExtra = pXSprite.getDudeExtra();
            dudeExtra.setThinkTime(dudeExtra.getThinkTime()+1);
            pSpawned.setOwner((short) nSprite);
            pSpawned.setX(pSpawned.getX() + dist + (BiRandom(dist)));
            if (pSpawned.getExtra() > -1) {
                boardService.getXSprite(pSpawned.getExtra()).setTarget(pXSprite.getTarget());
                if (pXSprite.getTarget() > -1) aiActivateDude((BloodSprite) pSpawned, boardService.getXSprite(pSpawned.getExtra()));
            }
            totalKills++;


            if (!sfxPlayGDXGenDudeSound(pSprite, 7))
                sfxStart3DSoundCP(pSprite, 379, 1, 0, 0x10000 - BiRandom(0x3000), 0);

            if (Chance(0x3500)) {
                int state = checkAttackState(pSprite, pXSprite);
                switch (state) {
                    case 1:
                        aiNewState(pSprite, pXSprite, GDXGenDudeDodge[WATER]);
                        break;
                    case 2:
                        aiNewState(pSprite, pXSprite, GDXGenDudeDodge[DUCK]);
                        break;
                    default:
                        aiNewState(pSprite, pXSprite, GDXGenDudeDodge[LAND]);
                        break;
                }
            }


        } else if (weapon >= kMissileBase && weapon < kMissileMax) {

            dx = Cos(pSprite.getAng()) >> 16;
            dy = Sin(pSprite.getAng()) >> 16;
            dz = pXDude.getDudeSlope();

            // dispersal modifiers here
            dx += BiRandom2(3000 - 1000 * pGameInfo.nDifficulty);
            dy += BiRandom2(3000 - 1000 * pGameInfo.nDifficulty);
            dz += BiRandom2(1000 - 500 * pGameInfo.nDifficulty);

            actFireMissile(pSprite, 0, 0, dx, dy, dz, weapon);
            if (!sfxPlayGDXGenDudeSound(pSprite, 7)) sfxPlayMissileSound(pSprite, weapon);
        }
    }

    private static void ThrowCallback(int nXIndex, boolean impact) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (!boardService.isValidSprite(pXSprite.getTarget())) return;

        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax)) return;

        int thingType = pXSprite.getData1();
        if (thingType >= kThingBase && thingType < kThingMax) {

            THINGINFO pThinkInfo = thingInfo[thingType - kThingBase];
            if (pThinkInfo.allowThrow == 1) {

                if (!sfxPlayGDXGenDudeSound(pSprite, 8)) sfxStart3DSound(pSprite, 455, -1, 0);

                int dx = pTarget.getX() - pSprite.getX();
                int dy = pTarget.getY() - pSprite.getY();
                int dz = pTarget.getZ() - pSprite.getZ();

                int dist = EngineUtils.qdist(dx, dy);
                int zThrow = 14500;
                Sprite pThing = null;
                Sprite pLeech = null;
                XSPRITE pXLeech = null;
                if (thingType == kGDXThingCustomDudeLifeLeech) {
                    if ((pLeech = leechIsDropped(pSprite)) != null) {
                        // pickup life leech before throw it again
                        pXLeech = boardService.getXSprite(pLeech.getExtra());
                        removeLeech(pLeech);
                    }

                    zThrow = 5000;
                }

                pThing = actFireThing(nSprite, 0, 0, (dz / 128) - zThrow, thingType, divscale(dist / 540, kTimerRate, 23));
                if (pThing == null) return;
                if (pThing.getLotag() != kGDXThingThrowableRock) {
                    if (pThinkInfo.picnum < 0) pThing.setPicnum(0);
                    else tileLoadVoxel(pThinkInfo.picnum);
                }

                pThing.setOwner(pSprite.getXvel());
                switch (thingType) {
                    case 428:
                        impact = true;
                        pThing.setXrepeat(24);
                        pThing.setYrepeat(24);
                        boardService.getXSprite(pThing.getExtra()).setData4(3 + pGameInfo.nDifficulty);
                        break;

                    case kGDXThingThrowableRock:
                        int[] sPics;
                        sPics = new int[6];
                        sPics[0] = 2406;
                        sPics[1] = 2280;
                        sPics[2] = 2185;
                        sPics[3] = 2155;
                        sPics[4] = 2620;
                        sPics[5] = 3135;

                        pThing.setPicnum((short) sPics[Random(5)]);
                        pThing.setPal(5);
                        pThing.setCstat(pThing.getCstat() | 0x0001);
                        pThing.setXrepeat((short) (24 + Random(42)));
                        pThing.setYrepeat((short) (24 + Random(42)));


                        if (Chance(0x3000)) pThing.setCstat(pThing.getCstat() | 0x0004);
                        if (Chance(0x3000)) pThing.setCstat(pThing.getCstat() | 0x0008);

                        if (pThing.getXrepeat() > 60) boardService.getXSprite(pThing.getExtra()).setData1(43);
                        else if (pThing.getXrepeat() > 40) boardService.getXSprite(pThing.getExtra()).setData1(33);
                        else if (pThing.getXrepeat() > 30) boardService.getXSprite(pThing.getExtra()).setData1(23);
                        else boardService.getXSprite(pThing.getExtra()).setData1(12);

                        impact = false;
                        return;
                    case kThingTNTBarrel:
                    case kThingTNTProx:
                    case kThingSprayBundle:
                        impact = false;
                        break;
                    case kGDXThingTNTProx:
                        boardService.getXSprite(pThing.getExtra()).setState(0);
                        boardService.getXSprite(pThing.getExtra()).setProximity(true);
                        return;
                    case kThingLifeLeech:
                    case kGDXThingCustomDudeLifeLeech:
                        XSPRITE pXThing = boardService.getXSprite(pThing.getExtra());
                        if (pLeech != null) pXThing.setHealth(pXLeech.getHealth());
                        else pXThing.setHealth(300 * pGameInfo.nDifficulty);

                        sfxStart3DSound(pSprite, 490, -1, 0);

                        if (pGameInfo.nDifficulty <= 2) pXThing.setData3(32700);
                        else pXThing.setData3((short) Random(10));
                        pThing.setPal(6);
                        pXThing.setTarget(pTarget.getXvel());
                        pXThing.setProximity(true);
                        pXThing.setStateTimer(1);
                        evPostCallback(pThing.getXvel(), SS_SPRITE, 80, 20);
                        return;
                }

                if (impact && dist <= 7680) boardService.getXSprite(pThing.getExtra()).setImpact(true);
                else {
                    boardService.getXSprite(pThing.getExtra()).setImpact(false);
                    evPost(pThing.getXvel(), SS_SPRITE, Integer.toUnsignedLong(kTimerRate * Random(2) + kTimerRate), 1);
                }
            }

        }

    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
		
		/*if (ceilIsTooLow(pSprite))
			aiNewState(pSprite,pXSprite,GDXGenDudeSearch[WATER]);
		else
			aiNewState(pSprite,pXSprite,GDXGenDudeIdle[LAND]);*/

        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget2(pSprite, pXSprite);
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {

        int dx, dy, dist;
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax))
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(10.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
            if (spriteIsUnderwater(pSprite, false)) aiNewState(pSprite, pXSprite, GDXGenDudeSearch[WATER]);
            else aiNewState(pSprite, pXSprite, GDXGenDudeSearch[LAND]);
        }
        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {

        if (pSprite.getLotag() < kDudeBase || pSprite.getLotag() >= kDudeMax)
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        else if (pXSprite.getTarget() <= -1) {
            if (spriteIsUnderwater(pSprite, false)) aiNewState(pSprite, pXSprite, GDXGenDudeGoto[WATER]);
            else aiNewState(pSprite, pXSprite, GDXGenDudeGoto[LAND]);
            return;
        } else if (!boardService.isValidSprite(pXSprite.getTarget()))
            throw new AssertException("isValidSprite(pXSprite.getTarget())");

        int dx, dy;
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = (!IsDudeSprite(pTarget) || pTarget.getExtra() < 0) ? null : boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            if (spriteIsUnderwater(pSprite, false)) aiNewState(pSprite, pXSprite, GDXGenDudeSearch[WATER]);
            else {
                aiNewState(pSprite, pXSprite, GDXGenDudeSearch[LAND]);
                sfxPlayGDXGenDudeSound(pSprite, 5);
            }
            return;
        } else if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                if (spriteIsUnderwater(pSprite, false)) aiNewState(pSprite, pXSprite, GDXGenDudeSearch[WATER]);
                else aiNewState(pSprite, pXSprite, GDXGenDudeSearch[LAND]);
                return;
            }
        }

        int dist = EngineUtils.qdist(dx, dy);
        if (dist == 0) dist = 1;
        int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;
        if (dist > pDudeInfo.seeDist || !engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {

            if (spriteIsUnderwater(pSprite, false)) aiNewState(pSprite, pXSprite, GDXGenDudeGoto[WATER]);
            else aiNewState(pSprite, pXSprite, GDXGenDudeGoto[LAND]);
            pXSprite.setTarget(-1);
            return;
        }

        int nAngle = EngineUtils.getAngle(dx, dy);
        int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
        // is the target visible?
        if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {

            if (pXSprite.getTarget() < 0) aiSetTarget(pXSprite, pXSprite.getTarget());
            if ((gFrameClock & 64) == 0 && Chance(0x1000) && !spriteIsUnderwater(pSprite, false))
                sfxPlayGDXGenDudeSound(pSprite, 6);

            pXSprite.getDudeExtra().setDudeSlope(divscale(pTarget.getZ() - pSprite.getZ(), dist, 10));

            Sprite pLeech = null;
            VECTORDATA meleeVector = gVectorData[22];
            if (pXSprite.getData1() >= kThingBase && pXSprite.getData1() < kThingMax) {
                if (pXSprite.getData1() == kThingLifeLeech) pXSprite.setData1(kGDXThingCustomDudeLifeLeech);
                if ((pLeech = leechIsDropped(pSprite)) != null && boardService.getXSprite(pLeech.getExtra()).getTarget() != pXSprite.getTarget())
                    boardService.getXSprite(pLeech.getExtra()).setTarget(pXSprite.getTarget());

                if (klabs(losAngle) < kAngle15) {
                    if (dist < 12264 && dist > 7680 && !spriteIsUnderwater(pSprite, false) && pXSprite.getData1() != kGDXThingCustomDudeLifeLeech) {
                        int pHit = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
                        switch (pHit) {
                            case SS_WALL:
                            case SS_MASKED:
                                return;
                            default:
                                aiNewState(pSprite, pXSprite, GDGenDudeThrow);
                        }

                    } else if (dist > 4072 && dist <= 9072 && !spriteIsUnderwater(pSprite, false) && pSprite.getOwner() != 32666) {
                        switch (pXSprite.getData1()) {
                            case kGDXThingCustomDudeLifeLeech:
                                if (pLeech == null) {
                                    aiNewState(pSprite, pXSprite, GDGenDudeThrow2);
                                    GDGenDudeThrow2.next = GDXGenDudeDodge[LAND];
                                    return;
                                }

                                XSPRITE pXLeech = boardService.getXSprite(pLeech.getExtra());
                                int ldist = getTargetDist(pTarget, pDudeInfo, pLeech);
                                //System.out.println("LDIST: "+ldist);
                                if (ldist > 3 || !engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pLeech.getX(), pLeech.getY(), pLeech.getZ(), pLeech.getSectnum()) || pXLeech.getTarget() == -1) {

                                    aiNewState(pSprite, pXSprite, GDGenDudeThrow2);
                                    GDGenDudeThrow2.next = GDXGenDudeDodge[LAND];

                                } else {
                                    GDGenDudeThrow2.next = GDXGenDudeChase[LAND];
                                    if (pXLeech.getTarget() != pXSprite.getTarget())
                                        pXLeech.setTarget(pXSprite.getTarget());
                                    if (dist > 5072 && Chance(0x3000)) {

                                        if (!canDuck(pSprite) || Chance(0x2000))
                                            aiNewState(pSprite, pXSprite, GDXGenDudeDodge[LAND]);
                                        else aiNewState(pSprite, pXSprite, GDXGenDudeDodge[DUCK]);

                                    } else {
                                        aiNewState(pSprite, pXSprite, GDXGenDudeChase[LAND]);
                                    }
                                }
                                return;
                            case kGDXThingThrowableRock:
                                if (Chance(0x2000)) aiNewState(pSprite, pXSprite, GDGenDudeThrow2);
                                else sfxPlayGDXGenDudeSound(pSprite, 0);
                                return;
                            default:
                                aiNewState(pSprite, pXSprite, GDGenDudeThrow2);
                        }

                    } else if (dist <= meleeVector.maxDist) {

                        if (spriteIsUnderwater(pSprite, false)) {
                            if (Chance(0x7000)) aiNewState(pSprite, pXSprite, GDXGenDudePunch);
                            else aiNewState(pSprite, pXSprite, GDXGenDudeDodge[WATER]);

                        } else if (Chance(0x7000)) aiNewState(pSprite, pXSprite, GDXGenDudePunch);
                        else aiNewState(pSprite, pXSprite, GDXGenDudeDodge[LAND]);

                    } else {
                        int state = checkAttackState(pSprite, pXSprite);
                        if (state == 1) aiNewState(pSprite, pXSprite, GDXGenDudeChase[WATER]);
                        else if (state == 2) {
                            if (Chance(0x3000)) aiNewState(pSprite, pXSprite, GDXGenDudeChase[DUCK]);
                            else aiNewState(pSprite, pXSprite, GDXGenDudeChase[LAND]);
                        } else aiNewState(pSprite, pXSprite, GDXGenDudeChase[LAND]);
                    }
                }

            } else {

                int defDist = 17920;
                int vdist = defDist;

                if (pXSprite.getData1() > 0 && pXSprite.getData1() < kVectorMax) {

                    if (pXSprite.getData1() == 19) {
                        pXSprite.setData1(2);
                    }

                    VECTORDATA pVectorData = gVectorData[pXSprite.getData1()];
                    vdist = pVectorData.maxDist;
                    if (vdist <= 0 || vdist > defDist) vdist = defDist;

                } else if (pXSprite.getData1() >= kDudeBase && pXSprite.getData1() < kDudeMax) {
                    DudeExtra dudeExtra = pXSprite.getDudeExtra();

                    if (dudeExtra.getThinkTime() > 0) {
                        updateTargetOfSlaves(pSprite);
                        if (pXSprite.getTarget() >= 0 && boardService.getSprite(pXSprite.getTarget()).getOwner() == pSprite.getXvel()) {
                            aiSetTarget(pXSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ());
                            return;
                        }
                    }

                    int state = checkAttackState(pSprite, pXSprite);
                    if (dudeExtra.getThinkTime() <= pGameInfo.nDifficulty && dist > meleeVector.maxDist) {
                        vdist = (vdist / 2) + Random(vdist / 2);
                    } else if (dist <= meleeVector.maxDist) {
                        aiNewState(pSprite, pXSprite, GDXGenDudePunch);
                        return;
                    } else {

                        if (state == 1) aiNewState(pSprite, pXSprite, GDXGenDudeChase[WATER]);
                        else if (state == 2) aiNewState(pSprite, pXSprite, GDXGenDudeChase[DUCK]);
                        else aiNewState(pSprite, pXSprite, GDXGenDudeChase[LAND]);
                        return;
                    }


                } else if (pXSprite.getData1() >= kMissileBase && pXSprite.getData1() < kMissileMax) {
                    // special handling for flame, explosive and life leech missiles
                    int state = checkAttackState(pSprite, pXSprite);
                    int mdist = (pXSprite.getData1() != kMissileStarburstFlare) ? 3000 : 2500;
                    switch (pXSprite.getData1()) {
                        case kMissileLifeLeech:
                            // pickup life leech if it was thrown previously
                            if ((pLeech = leechIsDropped(pSprite)) != null) removeLeech(pLeech);
                            break;
                        case kMissileStarburstFlare:
                        case kMissileFireball:
                        case kMissileNapalm:
                        case kMissileTchernobog:
                        case kMissileTchernobog2:
                            if (dist > mdist || pXSprite.getLocked() == 1) break;
                            else if (dist <= meleeVector.maxDist && Chance(0x7000))
                                aiNewState(pSprite, pXSprite, GDXGenDudePunch);
                            else if (state == 1) aiNewState(pSprite, pXSprite, GDXGenDudeChase[WATER]);
                            else if (state == 2) aiNewState(pSprite, pXSprite, GDXGenDudeChase[DUCK]);
                            else aiNewState(pSprite, pXSprite, GDXGenDudeChase[LAND]);
                            return;
                        case kMissileSprayFlame:
                        case kMissileHoundFire:
                            if (spriteIsUnderwater(pSprite, false)) {
                                if (dist > meleeVector.maxDist) aiNewState(pSprite, pXSprite, GDXGenDudeChase[WATER]);
                                else if (Chance(0x4000)) aiNewState(pSprite, pXSprite, GDXGenDudePunch);
                                else aiNewState(pSprite, pXSprite, GDXGenDudeDodge[WATER]);
                                return;
                            }

                            vdist = 4200;
                            if ((gFrameClock & 16) == 0) vdist += Random(800);
                            break;
                    }

                } else if (pXSprite.getData1() >= kThingHiddenExploder && pXSprite.getData1() < (kThingHiddenExploder + kExplodeMax) - 1) {

                    int nType = pXSprite.getData1() - kThingHiddenExploder;
                    EXPLODE pExpl = gExplodeData[nType];
                    boolean inRange = CheckProximity(pSprite, pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pExpl.radius / 2);
                    if (pExpl != null && inRange && doExplosion(pSprite, nType))
                        actKillSprite(pSprite.getXvel(), pSprite, 3, 65535);
                    return;

                    // scared dude - no weapon. Still can punch you sometimes.
                } else {

                    int state = checkAttackState(pSprite, pXSprite);
                    if (Chance(0x0500) && !spriteIsUnderwater(pSprite, false)) sfxPlayGDXGenDudeSound(pSprite, 6);

                    if (Chance(0x0200)) {
                        if (dist <= meleeVector.maxDist) aiNewState(pSprite, pXSprite, GDXGenDudePunch);
                        else if (state == 1) aiNewState(pSprite, pXSprite, GDXGenDudeDodge[WATER]);
                        else if (state == 2) aiNewState(pSprite, pXSprite, GDXGenDudeDodge[DUCK]);
                        else aiNewState(pSprite, pXSprite, GDXGenDudeDodge[LAND]);
                    } else if (state == 1) aiNewState(pSprite, pXSprite, GDXGenDudeSearch[WATER]);
                    else aiNewState(pSprite, pXSprite, GDXGenDudeSearch[LAND]);

                    aiSetTarget(pXSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ());
                    return;
                }


                if (dist <= vdist && pXSprite.getAIState() == GDXGenDudeChase[DUCK])
                    aiNewState(pSprite, pXSprite, GDXGenDudeChase[LAND]);

                int state = checkAttackState(pSprite, pXSprite);
                if (dist < vdist /*&& klabs(losAngle) < 32*/ && klabs(losAngle) < kAngle5) {

                    switch (state) {
                        case 1:
                            aiNewState(pSprite, pXSprite, GDXGenDudeFire[WATER]);
                            pXSprite.getAIState().next = GDXGenDudeFire[WATER];
                            return;
                        case 2:
                            aiNewState(pSprite, pXSprite, GDXGenDudeFire[DUCK]);
                            pXSprite.getAIState().next = GDXGenDudeFire[DUCK];
                            return;
                        default:
                            aiNewState(pSprite, pXSprite, GDXGenDudeFire[LAND]);
                            pXSprite.getAIState().next = GDXGenDudeFire[LAND];
                    }

                } else {
                    int nSeq = (state < 3) ? 8 : 6;

                    SeqInst pInst = GetInstance(SS_SPRITE, pSprite.getExtra());
                    if (pInst.getSeqIndex() == (boardService.getXSprite(pSprite.getExtra()).getData2() + nSeq)) {
                        //System.err.println("SAME SEQ FOUND "+(getXSprite()[pSprite.extra].data2 + nSeq));
                        if (state == 1) pXSprite.getAIState().next = GDXGenDudeChase[WATER];
                        else if (state == 2) pXSprite.getAIState().next = GDXGenDudeChase[DUCK];
                        else pXSprite.getAIState().next = GDXGenDudeChase[LAND];

                    } else if (state == 1 && pXSprite.getAIState() != GDXGenDudeChase[WATER] && pXSprite.getAIState() != GDXGenDudeFire[WATER]) {
                        aiNewState(pSprite, pXSprite, GDXGenDudeChase[WATER]);
                        pXSprite.getAIState().next = GDXGenDudeFire[WATER];

                    } else if (state == 2 && pXSprite.getAIState() != GDXGenDudeChase[DUCK] && pXSprite.getAIState() != GDXGenDudeFire[DUCK]) {
                        aiNewState(pSprite, pXSprite, GDXGenDudeChase[DUCK]);
                        pXSprite.getAIState().next = GDXGenDudeFire[DUCK];

                    } else if (pXSprite.getAIState() != GDXGenDudeChase[LAND] && pXSprite.getAIState() != GDXGenDudeFire[LAND]) {
                        aiNewState(pSprite, pXSprite, GDXGenDudeChase[LAND]);
                        pXSprite.getAIState().next = GDXGenDudeFire[LAND];
                    }

                }
            }
        }
    }

    public static int checkAttackState(Sprite pSprite, XSPRITE pXSprite) {
        //DUCK - seq 14
        if (checkUniCultistSeq(pSprite, 14) || spriteIsUnderwater(pSprite, false)) {
            if (!checkUniCultistSeq(pSprite, 14) || spriteIsUnderwater(pSprite, false)) {
                if (spriteIsUnderwater(pSprite, false)) return 1; //water
            } else return 2; //duck
        } else return 3; //land
        return 0;
    }

    public static boolean checkUniCultistSeq(Sprite pSprite, int nSeqID) {
        if (pSprite.getStatnum() == kStatDude && IsDudeSprite(pSprite)) {
            SeqInst pInst = GetInstance(SS_SPRITE, pSprite.getExtra());
            return pInst.getSeqIndex() == (boardService.getXSprite(pSprite.getExtra()).getData2() + nSeqID) && seqFrame(SS_SPRITE, pSprite.getExtra()) >= 0;
        }
        return false;
    }

    public static boolean TargetNearThing(Sprite pSprite, int thingType) {
        for (ListNode<Sprite> node = boardService.getSectNode(pSprite.getSectnum()); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            // check for required things or explosions in the same sector as the target
            if (boardService.getSprite(nSprite).getLotag() == thingType || boardService.getSprite(nSprite).getStatnum() == kStatExplosion)
                return true; // indicate danger
        }
        return false;
    }

    ///// For gen dude

    public static long getGenDudeMoveSpeed(Sprite pSprite, int which, boolean mul, boolean shift) {
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        int speed = -1;
        int step = 2500;
        int maxSpeed = 146603;
        switch (which) {
            case 0:
                speed = pDudeInfo.frontSpeed;
                break;
            case 1:
                speed = pDudeInfo.sideSpeed;
                break;
            case 2:
                speed = pDudeInfo.backSpeed;
                break;
            case 3:
                speed = pDudeInfo.angSpeed;
                break;
            default:
                return -1;
        }
        if (pXSprite.getBusyTime() > 0) speed /= 3;
        if (speed > 0 && mul) {

            //System.err.println(pXSprite.busyTime);
            if (pXSprite.getBusyTime() > 0) speed += (step * pXSprite.getBusyTime());
        }

        if (shift) speed *= kFrameTicks >> 4;
        if (speed > maxSpeed) speed = maxSpeed;

        return speed;
    }

    public static void aiGenDudeMoveForward(BloodSprite pSprite, XSPRITE pXSprite) {
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;

        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) return;

        int sin = Sin(pSprite.getAng());
        int cos = Cos(pSprite.getAng());

        long frontSpeed = getGenDudeMoveSpeed(pSprite, 0, true, false);
        pSprite.addVelocity(mulscale(cos, frontSpeed, 30), mulscale(sin, frontSpeed, 30), 0);
    }

    public static boolean sfxPlayGDXGenDudeSound(Sprite pSprite, int mode) {

        if (mode < 0 || mode >= kMaxGenDudeSndMode) return false;
        GENDUDESND sndInfo = gCustomDudeSnd[mode];
        boolean gotSnd = false;
        short sndStartId = boardService.getXSprite(pSprite.getExtra()).getData3();
        int rand = sndInfo.randomRange;
        int sndId = (sndStartId <= 0) ? sndInfo.defaultSndId : sndStartId + sndInfo.sndIdOffset;

        if (sndId < 0) return false;
        else if (sndStartId <= 0) {
            sndId += Random(rand);
            gotSnd = true;
        } else {

            // Let's try to get random snd
            int maxRetries = 5;
            while (maxRetries-- > 0) {
                int random = Random(rand);
                if (!game.getCache().contains(sndId + random, "sfx")) continue;
                sndId = sndId + random;
                gotSnd = true;
                break;
            }

            // If no success in getting random snd, get first existing one
            if (!gotSnd) {
                while (sndId++ <= sndId + rand) {
                    if (!game.getCache().contains(sndId, "sfx")) continue;
                    gotSnd = true;
                    break;
                }
            }

        }

        if (!gotSnd) return false;
        else if (sndInfo.aiPlaySound) aiPlaySound((BloodSprite) pSprite, sndId, 2, -1);
        else sfxStart3DSound(pSprite, sndId, -1, 0);
        return true;
    }


    public static boolean spriteIsUnderwater(Sprite pSprite, boolean oldWay) {
        if (oldWay) {
            return boardService.getXSprite(pSprite.getExtra()).getPalette() == 1 || boardService.getXSprite(pSprite.getExtra()).getPalette() == 2;
        }

        if (boardService.getSector(pSprite.getSectnum()).getExtra() < 0) return false;
        else return xsector[boardService.getSector(pSprite.getSectnum()).getExtra()].Underwater;
    }

    public static Sprite leechIsDropped(Sprite pSprite) {
        for (ListNode<Sprite> node = boardService.getStatNode(kStatThing); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            if (boardService.getSprite(nSprite).getLotag() == kGDXThingCustomDudeLifeLeech && boardService.getSprite(nSprite).getOwner() == pSprite.getXvel())
                return boardService.getSprite(nSprite);
        }

        return null;

    }

    public static void removeDudeStuff(Sprite pSprite) {
        for (ListNode<Sprite> node = boardService.getStatNode(kStatThing); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            if (boardService.getSprite(nSprite).getOwner() != pSprite.getXvel()) continue;
            switch (boardService.getSprite(nSprite).getLotag()) {
                case 401:
                case 402:
                case 433:
                    engine.deletesprite(nSprite);
                    break;
                case kGDXThingCustomDudeLifeLeech:
                    killDudeLeech(boardService.getSprite(nSprite));
                    break;
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            if (boardService.getSprite(nSprite).getOwner() != pSprite.getXvel()) continue;
            actDamageSprite(boardService.getSprite(nSprite).getOwner(), boardService.getSprite(nSprite), kDamageFall, 65535);
        }
    }

    public static void removeLeech(Sprite pLeech) {
        if (pLeech != null) {
            Sprite pEffect = actSpawnEffect(52, pLeech.getSectnum(), pLeech.getX(), pLeech.getY(), pLeech.getZ(), pLeech.getAng());
            if (pEffect != null) {
                pEffect.setCstat(kSpriteFace);
                pEffect.setPal(6);
                int repeat = 64 + Random(50);
                pEffect.setXrepeat((short) repeat);
                pEffect.setYrepeat((short) repeat);
            }
            sfxStart3DSoundCP(pLeech, 490, -1, 0, 60000, 0);
            engine.deletesprite(pLeech.getXvel());
        }
    }

    public static void killDudeLeech(BloodSprite pLeech) {
        actDamageSprite(pLeech.getOwner(), pLeech, 3, 65535);
        removeLeech(pLeech);
        sfxStart3DSoundCP(pLeech, 522, -1, 0, 60000, 0);
    }


    public static void updateTargetOfSlaves(Sprite pSprite) {
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            if (boardService.getSprite(nSprite).getOwner() != pSprite.getXvel() || boardService.getSprite(nSprite).getExtra() < 0)
                continue;

            XSPRITE pXSprite = boardService.getXSprite(node.get().getExtra());
            if (boardService.getXSprite(pSprite.getExtra()).getTarget() != pXSprite.getTarget() && IsDudeSprite(boardService.getSprite(boardService.getXSprite(pSprite.getExtra()).getTarget()))) {
                aiSetTarget(pXSprite, boardService.getXSprite(pSprite.getExtra()).getTarget());
            }

            if (pXSprite.getTarget() >= 0) {
                // don't attack mates
                if (boardService.getSprite(pXSprite.getTarget()).getOwner() == boardService.getSprite(nSprite).getOwner())
                    aiSetTarget(pXSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ());
            }

            if (!isActive(pXSprite) && pXSprite.getTarget() >= 0)
                aiActivateDude((BloodSprite) boardService.getSprite(nSprite), pXSprite);
        }
    }

    public static boolean dudeIsMelee(XSPRITE pXSprite) {
        int meleeDist = 2048;
        int vdist = meleeDist;
        if (pXSprite.getData1() >= 0 && pXSprite.getData1() < kVectorMax) {
            int vector = pXSprite.getData1();
            if (vector <= 0) vector = 2;
            VECTORDATA pVectorData = ru.m210projects.Blood.Actor.gVectorData[vector];
            vdist = pVectorData.maxDist;

            return vdist > 0 && vdist <= meleeDist;

        } else {

            return pXSprite.getData1() >= kThingHiddenExploder && pXSprite.getData1() < (kThingHiddenExploder + kExplodeMax) - 1;
			
			/*switch(pXSprite.data1){
				case kMissileSprayFlame:
				case kMissileHoundFire:
					return true;
				default:
					return false;
			}*/
        }
    }

    public static int getBaseChanceModifier(int baseChance) {
        return ((pGameInfo.nDifficulty > 0) ? baseChance - (0x0300 * pGameInfo.nDifficulty) : baseChance);
    }

    public static int getRecoilChance(Sprite pSprite) {
        int mass = getDudeMassBySpriteSize(pSprite);
        int cumulDmg = 0;
        int baseChance = getBaseChanceModifier(0x6000);
        if (pSprite.getExtra() >= 0) {
            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
            baseChance += (pXSprite.getBurnTime() / 2);
            cumulDmg = pXSprite.getDudeExtra().getCumulDamage();
            if (dudeIsMelee(pXSprite)) baseChance = 0x500;
        }

        baseChance += cumulDmg;
        int chance = ((baseChance / mass) << 7);
        return chance;

    }

    public static int getDodgeChance(Sprite pSprite) {
        int mass = getDudeMassBySpriteSize(pSprite);
        int baseChance = getBaseChanceModifier(0x2000);
        if (pSprite.getExtra() >= 0) {
            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
            baseChance += pXSprite.getBurnTime();
            if (dudeIsMelee(pXSprite)) baseChance = 0x200;
        }


        int chance = ((baseChance / mass) << 7);
        return chance;
    }

    public static void dudeLeechOperate(BloodSprite pSprite, XSPRITE pXSprite, int evCommand) {
        if (evCommand == kCommandOff) {
            actPostSprite(pSprite.getXvel(), kStatFree);
            return;
        }

        if (boardService.isValidSprite(pXSprite.getTarget()) && pXSprite.getStateTimer() == 0) {
            BloodSprite pTarget = boardService.getSprite(pXSprite.getTarget());

            if (pTarget != null && IsDudeSprite(pTarget) && (pTarget.getHitag() & kAttrFree) == 0) {
                if (boardService.getXSprite(pTarget.getExtra()) != null) {
                    GetSpriteExtents(pSprite);
                    DudeInfo pDudeInfo = dudeInfo[pTarget.getLotag() - kDudeBase];
                    int dz = extents_zTop - pSprite.getZ() - 256;
                    long dist = EngineUtils.qdist(pTarget.getX() - pSprite.getX(), pTarget.getY() - pSprite.getY());
                    if (dist != 0) {
                        if (engine.cansee(pSprite.getX(), pSprite.getY(), extents_zTop, pSprite.getSectnum(), pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum())) {
                            short oldang = pSprite.getAng();
                            long vel = divscale(dist, 0x1AAAAA, 12);
                            pSprite.setAng(EngineUtils.getAngle(mulscale(vel, pTarget.getVelocityX(), 12) + pTarget.getX() - pSprite.getX(), mulscale(vel, pTarget.getVelocityY(), 12) + pTarget.getY() - pSprite.getY()));
                            int eyeAboveZ = pTarget.getZ() - (pDudeInfo.aimHeight * pTarget.getYrepeat() << 2) - extents_zTop - 256;
                            int ax = Cos(pSprite.getAng()) >> 16;
                            int ay = Sin(pSprite.getAng()) >> 16;
                            int az = divscale(eyeAboveZ, dist, 10);
                            int time = 12;
                            int missileType = kMissileAltLeech1;
                            if (pXSprite.getData3() != 0) { //has ammo
                                time = 36;
                                missileType += 1;
                            }

                            Sprite pMissile = actFireMissile(pSprite, 0, dz, ax, ay, az, missileType);
                            if (pMissile != null) {
                                pMissile.setOwner(pSprite.getOwner());
                                pXSprite.setStateTimer(1);
                                pXSprite.setData3((short) ClipLow(pXSprite.getData3() - 1, 0));
                                evPostCallback(pSprite.getXvel(), SS_SPRITE, time, 20);
                            }
                            pSprite.setAng(oldang);
                        }
                    }
                }
            }
        }

    }

    public static boolean ceilIsTooLow(Sprite pSprite) {
        if (pSprite != null) {

            Sector pSector = boardService.getSector(pSprite.getSectnum());
            int a = pSector.getCeilingz() - pSector.getFloorz();
            GetSpriteExtents(pSprite);
            int b = extents_zTop - extents_zBot;
            ///System.err.println(a +" / "+ b);
            return a > b;
			/*GetZRange(pSprite, pSprite.clipdist << 2, CLIPMASK0 | 0x3000);
			GetSpriteExtents(pSprite);
			int ceilzofslope = engine.getceilzofslope(pSprite.sectnum, pSprite.x, pSprite.y);
			//System.err.println(zBot +" / "+ gz_floorZ);
			if (extents_zBot > gz_floorZ || ceilzofslope >= extents_zTop)
				return true;*/
        }

        return false;
    }

    public static boolean doExplosion(Sprite pSprite, int nType) {
        int nExplosion = actSpawnSprite(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), kStatExplosion, true);
        int nSeq = 4;
        int nSnd = 304;

        EXPLODE pExpl = gExplodeData[nType];
        if (nExplosion >= 0) {

            Sprite pExplosion = boardService.getSprite(nExplosion);
            pExplosion.setYrepeat(pExpl.size);
            pExplosion.setXrepeat(pExpl.size);
            pExplosion.setLotag((short) (nType));
            pExplosion.setCstat(pExplosion.getCstat() | ((short) kSpriteInvisible | kSpriteOriginAlign));
            pExplosion.setOwner(pSprite.getXvel());

            if (pExplosion.getExtra() >= 0) {
                boardService.getXSprite(pExplosion.getExtra()).setTarget(0);
                boardService.getXSprite(pExplosion.getExtra()).setData1((short) pExpl.liveCount);
                boardService.getXSprite(pExplosion.getExtra()).setData2((short) pExpl.quake);
                boardService.getXSprite(pExplosion.getExtra()).setData3((short) pExpl.used3);


                if (nType == 0) {
                    nSeq = 3;
                    nSnd = 303;
                    pExplosion.setZ(pSprite.getZ());
                } else if (nType == 2) {
                    nSeq = 4;
                    nSnd = 305;
                } else if (nType == 3) {
                    nSeq = 9;
                    nSnd = 307;
                } else if (nType == 4) {
                    nSeq = 5;
                    nSnd = 307;
                } else if (nType <= 6) {
                    nSeq = 4;
                    nSnd = 303;
                } else if (nType == 7) {
                    nSeq = 4;
                    nSnd = 303;
                }


                if (game.getCache().contains(nSeq, seq)) seqSpawn(nSeq, SS_SPRITE, pExplosion.getExtra(), null);
                sfxStart3DSound(pExplosion, nSnd, -1, 0);

                return true;
            }
        }

        return false;

    }

    public static boolean canSwim(Sprite pSprite) {
        return (game.getCache().contains(boardService.getXSprite(pSprite.getExtra()).getData2() + 8, seq) && game.getCache().contains(boardService.getXSprite(pSprite.getExtra()).getData2() + 13, seq) && game.getCache().contains(boardService.getXSprite(pSprite.getExtra()).getData2() + 17, seq));
    }


    public static boolean canDuck(Sprite pSprite) {
        return (game.getCache().contains(boardService.getXSprite(pSprite.getExtra()).getData2() + 8, seq) && game.getCache().contains(boardService.getXSprite(pSprite.getExtra()).getData2() + 14, seq));

    }

    public static boolean CDCanMove(Sprite pSprite) {
        return (game.getCache().contains(boardService.getXSprite(pSprite.getExtra()).getData2() + 9, seq) && game.getCache().contains(boardService.getXSprite(pSprite.getExtra()).getData2() + 13, seq) && game.getCache().contains(boardService.getXSprite(pSprite.getExtra()).getData2() + 14, seq));

    }

    public static boolean inDodge(AISTATE aiState) {
        return (aiState == GDXGenDudeDodgeDmg[WATER] || aiState == GDXGenDudeDodgeDmg[DUCK] || aiState == GDXGenDudeDodgeDmg[LAND]);
    }

    public static boolean inIdle(AISTATE aiState) {
        return (aiState == GDXGenDudeIdle[WATER] || aiState == GDXGenDudeIdle[DUCK] || aiState == GDXGenDudeIdle[LAND]);
    }

    public static int getSeqStartId(XSPRITE pXSprite) {

        int seqStartId = dudeInfo[boardService.getSprite(pXSprite.getReference()).getLotag() - kDudeBase].seqStartID;
        // Store seqStartId in data2 field
        if (pXSprite.getData2() > 0) {
            seqStartId = pXSprite.getData2();
            // check for full set of animations
            for (int i = seqStartId; i <= seqStartId + 19; i++) {

                // exceptions
                switch (i - seqStartId) {
                    case 3:        // burning dude
                    case 4:    // electrocution
                    case 8:        // attack u/d
                    case 11:    // reserved
                    case 12:    // reserved
                    case 13:    // move u
                    case 14:    // move d
                    case 16:    // burning death 2
                    case 17:    // idle w
                    case 18:    // transformation in another dude
                    case 19:    // reserved
                        continue;
                }

                if (!game.getCache().contains(i, seq)) {
                    Console.out.println("No SEQ file with ID " + i + " found for custom dude #" + pXSprite.getReference() + "!\nData2 (SEQ Base): " + pXSprite.getData2() + "\nSwitching to default animation!", OsdColor.YELLOW);
                    pXSprite.setData2((short) dudeInfo[boardService.getSprite(pXSprite.getReference()).getLotag() - kDudeBase].seqStartID);
                    return pXSprite.getData2();
                }

            }

        } else {
            pXSprite.setData2((short) seqStartId);
        }

        return seqStartId;
    }

    //public static int getDispersionModifier(SPRITE pSprite) {
    //return 1;

    //}
    //////////
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.IsPlayerSprite;
import static ru.m210projects.Blood.Gameutils.M2X;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.Pragmas.klabs;

public class AIZOMBA {

    public static final int kAxeZombieMeleeDist = M2X(2.0);

    public static AISTATE zombieAIdle;
    public static AISTATE zombieAChase;
    public static AISTATE zombieAPonder;
    public static AISTATE zombieAGoto;
    public static AISTATE zombieAHack;
    public static AISTATE zombieASearch;
    public static AISTATE zombieARecoil;
    public static AISTATE zombieARTesla;
    public static AISTATE zombieAFall;
    public static AISTATE zombieAUp;
    public static AISTATE zombieEIdle;
    public static AISTATE zombieEUp;
    public static AISTATE zombieEUp2;
    public static AISTATE zombieSLIdle;
    public static AISTATE zombieSLUP;

    public static void Init() {

        zombieAIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, true, false, true, null) {
            @Override
            public void enter(BloodSprite sprite, XSPRITE xsprite) {
                xsprite.setTarget(-1);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };

        zombieEIdle = new AISTATE(Type.idle, 12, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };

        zombieAPonder = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveTurn(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkPonder(sprite, xsprite);
            }
        };
        zombieARecoil = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, zombieAPonder);
        zombieARTesla = new AISTATE(Type.other, kSeqDudeTesla, null, 0, false, false, false, zombieAPonder);
        zombieAUp = new AISTATE(Type.other, 11, null, 0, false, false, false, zombieAPonder);
        zombieAFall = new AISTATE(Type.other, kSeqDudeDeath1, null, 360, false, false, false, zombieAUp);
        zombieAHack = new AISTATE(Type.other, kSeqDudeAttack, nXSprite -> HackCallback(nXSprite), 80, false, false, false, zombieAPonder);
        zombieAChase = new AISTATE(Type.other, kSeqDudeWalk, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };
        zombieAGoto = new AISTATE(Type.tgoto, kSeqDudeWalk, null, 1800, false, true, true, zombieAIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };
        zombieASearch = new AISTATE(Type.search, kSeqDudeWalk, null, 1800, false, true, true, zombieAIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };
        zombieEUp2 = new AISTATE(Type.other, kSeqDudeIdle, null, 1, true, false, false, zombieASearch) {
            @Override
            public void enter(BloodSprite sprite, XSPRITE xsprite) {
                entryEZombie(sprite, xsprite);
            }
        };

        zombieEUp = new AISTATE(Type.other, 9, null, 180, true, false, false, zombieEUp2) {
            @Override
            public void enter(BloodSprite sprite, XSPRITE xsprite) {
                entryEZombieUp(sprite, xsprite);
            }
        };

        zombieSLIdle = new AISTATE(Type.idle, 10, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };
        zombieSLUP = new AISTATE(Type.other, 11, null, 0, true, false, false, zombieAPonder) {
            @Override
            public void enter(BloodSprite sprite, XSPRITE xsprite) {
                entryEZombie(sprite, xsprite);
            }
        };

    }

    /****************************************************************************
     ** entryEZombie()
     **
     **
     ****************************************************************************/
    private static void entryEZombie(BloodSprite pSprite, XSPRITE pXSprite) {
        pSprite.setHitag(pSprite.getHitag() | kAttrMove);
        pSprite.setLotag(kDudeAxeZombie);
    }

    private static void entryEZombieUp(BloodSprite pSprite, XSPRITE pXSprite) {
        sfxStart3DSound(pSprite, 1100, -1, 0);
        pSprite.setAng(EngineUtils.getAngle(pXSprite.getTargetX() - pSprite.getX(), pXSprite.getTargetY() - pSprite.getY()));
    }

    /****************************************************************************
     ** thinkSearch()
     **
     **
     ****************************************************************************/
    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget2(pSprite, pXSprite);
    }

    /****************************************************************************
     ** thinkGoto()
     **
     **
     ****************************************************************************/
    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy;

        if (!IsDudeSprite(pSprite)) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        long dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode

        if (dist < M2X(1.8) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
//			System.out.println("Axe zombie " +  getXSprite()[pSprite.extra].reference + " switching to search mode");
            aiNewState(pSprite, pXSprite, zombieASearch);
        }

        aiThinkTarget(pSprite, pXSprite);

    }

    /****************************************************************************
     ** thinkChase()
     **
     **
     ****************************************************************************/
    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, zombieASearch);
            return;
        }

        int dx, dy;

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, zombieASearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0 || powerupCheck(pPlayer, kItemDeathMask - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, zombieAGoto);
                return;
            }
        }

        long dist = EngineUtils.qdist(dx, dy);

        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());

                    // check to see if we can attack
                    if (dist < kAxeZombieMeleeDist && klabs(losAngle) < kAngle15) {
                        aiNewState(pSprite, pXSprite, zombieAHack);
                    }
                    return;
                }
            }
        }
        //System.out.println("Axe zombie " + pXSprite.reference + " lost sight of target " + pXSprite.target);
        aiNewState(pSprite, pXSprite, zombieAGoto);
        pXSprite.setTarget(-1);
    }

    private static void thinkPonder(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, zombieASearch);
            return;
        }

        int dx, dy, dist;

        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget())) return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, zombieASearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0 || powerupCheck(pPlayer, kItemDeathMask - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, zombieAGoto);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);

        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());

                    // check to see if we can attack
                    if (dist < kAxeZombieMeleeDist) {
                        if (klabs(losAngle) < kAngle15) {
                            aiNewState(pSprite, pXSprite, zombieAHack);
                        }
                        return;
                    }
                }
            }
        }
        aiNewState(pSprite, pXSprite, zombieAChase);
    }

    private static void HackCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (!IsDudeSprite(pSprite)) return;

        int nAngle = EngineUtils.getAngle(pXSprite.getTargetX() - pSprite.getX(), pXSprite.getTargetY() - pSprite.getY());

        int nZOffset1 = dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight * pSprite.getYrepeat() << 2;
        int nZOffset2 = 0;
        if (pXSprite.getTarget() != -1) {
            Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
            if (IsDudeSprite(pTarget))
                nZOffset2 = dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight * pTarget.getYrepeat() << 2;
        }
        int dx = Cos(nAngle) >> 16;
        int dy = Sin(nAngle) >> 16;
        int dz = nZOffset1 - nZOffset2;

        sfxStart3DSound(pSprite, 1101, 1, 0); //kSfxAxeAir

        actFireVector(pSprite, 0, 0, dx, dy, dz, kVectorAxe);
    }
}

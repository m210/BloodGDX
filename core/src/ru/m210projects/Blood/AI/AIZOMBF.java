// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.Pragmas.klabs;

public class AIZOMBF {

    public static AISTATE zombieFIdle;
    public static AISTATE zombieFChase;
    public static AISTATE zombieFGoto;
    public static AISTATE zombieFDodge;
    public static AISTATE zombieFSearch;
    public static AISTATE zombieFRecoil;
    public static AISTATE zombieFRTesla;
    public static AISTATE zombieFThrow;
    public static AISTATE zombieFPuke;
    public static AISTATE zombieFHack;

    public static void Init() {
        zombieFIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, true, null) {
            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                aiThinkTarget(sprite, xsprite);
            }
        };

        zombieFGoto = new AISTATE(Type.tgoto, kSeqDudeWalk, null, 600, false, true, true, zombieFIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkGoto(sprite, xsprite);
            }
        };

        zombieFChase = new AISTATE(Type.other, kSeqDudeWalk, null, 0, false, true, true, null) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };

        zombieFDodge = new AISTATE(Type.other, kSeqDudeWalk, null, 0, false, true, true, zombieFChase) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveDodge(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkChase(sprite, xsprite);
            }
        };

        zombieFHack = new AISTATE(Type.other, kSeqDudeAttack, nXSprite -> HackCallback(nXSprite), 120, false, false, false, zombieFChase);
        zombieFPuke = new AISTATE(Type.other, 9, nXSprite -> PukeCallback(nXSprite), 120, false, false, false, zombieFChase);

        zombieFThrow = new AISTATE(Type.other, kSeqDudeAttack, nXSprite -> ThrowCallback(nXSprite), 120, false, false, false, zombieFChase);

        zombieFSearch = new AISTATE(Type.search, kSeqDudeWalk, null, 1800, false, true, true, zombieFIdle) {
            @Override
            public void move(BloodSprite sprite, XSPRITE xsprite) {
                aiMoveForward(sprite, xsprite);
            }

            @Override
            public void think(BloodSprite sprite, XSPRITE xsprite) {
                thinkSearch(sprite, xsprite);
            }
        };

        zombieFRecoil = new AISTATE(Type.other, kSeqDudeRecoil, null, 0, false, false, false, zombieFChase);
        zombieFRTesla = new AISTATE(Type.other, kSeqDudeTesla, null, 0, false, false, false, zombieFChase);
    }

    private static void thinkChase(BloodSprite pSprite, XSPRITE pXSprite) {
        if (pXSprite.getTarget() == -1) {
            aiNewState(pSprite, pXSprite, zombieFGoto);
            return;
        }

        int dx, dy, dist;
        if (!IsDudeSprite(pSprite) || !boardService.isValidSprite(pXSprite.getTarget()))
            return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        dx = pTarget.getX() - pSprite.getX();
        dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(pSprite, pXSprite, EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(pSprite, pXSprite, zombieFSearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0
                    || powerupCheck(pPlayer, kItemDeathMask - kItemBase) > 0) {
                aiNewState(pSprite, pXSprite, zombieFSearch);
                return;
            }
        }

        dist = EngineUtils.qdist(dx, dy);

        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(),
                    pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                    aiSetTarget(pXSprite, pXSprite.getTarget());

                    // check to see if we can attack
                    if (dist < 5120 && dist > 3584 && klabs(losAngle) < kAngle15) {
                        if (checkTarget(pSprite, dx, dy)) {
                            aiNewState(pSprite, pXSprite, zombieFThrow);
                        } else aiNewState(pSprite, pXSprite, zombieFDodge);
                    } else if (dist < 5120 && dist > 1536 && klabs(losAngle) < kAngle15) {
                        if (checkTarget(pSprite, dx, dy)) {
                            aiNewState(pSprite, pXSprite, zombieFPuke);
                        } else aiNewState(pSprite, pXSprite, zombieFDodge);
                    } else if (dist < 1024 && klabs(losAngle) < kAngle15) {
                        if (checkTarget(pSprite, dx, dy)) {
                            aiNewState(pSprite, pXSprite, zombieFHack);
                        } else aiNewState(pSprite, pXSprite, zombieFDodge);
                    }
                    return;
                }
            }
        }
        aiNewState(pSprite, pXSprite, zombieFSearch);
        pXSprite.setTarget(-1);
    }

    private static boolean checkTarget(BloodSprite pSprite, int dx, int dy) {
        int pInfo = HitScan(pSprite, pSprite.getZ(), dx, dy, 0, pHitInfo, 16777280, 0);
        return pInfo <= -1 || pInfo != 3 || pSprite.getLotag() != boardService.getSprite(pHitInfo.hitsprite).getLotag();
    }

    private static void thinkGoto(BloodSprite pSprite, XSPRITE pXSprite) {
        int dx, dy, dist;

        if (!IsDudeSprite(pSprite))
            return;

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        dx = pXSprite.getTargetX() - pSprite.getX();
        dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(pSprite, pXSprite, nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.0) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery)
            aiNewState(pSprite, pXSprite, zombieFSearch);

        aiThinkTarget(pSprite, pXSprite);
    }

    private static void thinkSearch(BloodSprite pSprite, XSPRITE pXSprite) {
        aiChooseDirection(pSprite, pXSprite, pXSprite.getGoalAng());
        aiThinkTarget(pSprite, pXSprite);
    }

    private static void PukeCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        if (!IsDudeSprite(pSprite))
            return;

        int nAngle = EngineUtils.getAngle(pXSprite.getTargetX() - pSprite.getX(), pXSprite.getTargetY() - pSprite.getY());
        int dx = Cos(nAngle) >> 16;
        int dy = Sin(nAngle) >> 16;
        int dz = 0;

        int nZOffset1 = dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight * pSprite.getYrepeat();
        int nZOffset2 = 0;
        if (pXSprite.getTarget() != -1) {
            Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
            if (IsDudeSprite(pTarget))
                nZOffset2 = dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight * pTarget.getYrepeat();
        }
        sfxStart3DSound(pSprite, 1203, 1, 0);
        actFireMissile(pSprite, 0, nZOffset2 - nZOffset1, dx, dy, dz, kMissileGreenPuke);
    }

    private static void ThrowCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        int dx = Cos(pSprite.getAng()) >> 16;
        int dy = Sin(pSprite.getAng()) >> 16;
        int dz = 0;

        int nZOffset = 0;
        if (IsDudeSprite(pSprite))
            nZOffset = dudeInfo[pSprite.getLotag() - kDudeBase].eyeHeight;

        actFireMissile(pSprite, 0, -nZOffset, dx, dy, dz, kMissileButcherKnife);
    }

    private static void HackCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        if (pXSprite != null) {
            Sprite pSprite = boardService.getSprite(pXSprite.getReference());
            if (pSprite != null && pSprite.getLotag() == kDudeButcher) {
                int dx = Cos(pSprite.getAng()) >> 16;
                int dy = Sin(pSprite.getAng()) >> 16;
                int dz = dudeInfo[4].eyeHeight * pSprite.getYrepeat();
                if (pXSprite.getTarget() != -1) {
                    Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
                    if (IsDudeSprite(pTarget))
                        dz -= dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight * pTarget.getYrepeat();
                }
                actFireVector(pSprite, 0, 0, dx, dy, dz, kVectorCleaver);
            }
        }
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.AI;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.WeaponAim;
import ru.m210projects.Blood.Types.XSECTOR;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;

import static ru.m210projects.Blood.AI.AIBAT.*;
import static ru.m210projects.Blood.AI.AIBEAST.*;
import static ru.m210projects.Blood.AI.AIBONEEL.*;
import static ru.m210projects.Blood.AI.AIBURN.*;
import static ru.m210projects.Blood.AI.AICALEB.*;
import static ru.m210projects.Blood.AI.AICERBERUS.*;
import static ru.m210projects.Blood.AI.AICULTIST.*;
import static ru.m210projects.Blood.AI.AIGARG.*;
import static ru.m210projects.Blood.AI.AIGHOST.*;
import static ru.m210projects.Blood.AI.AIGILLBEAST.*;
import static ru.m210projects.Blood.AI.AIHAND.*;
import static ru.m210projects.Blood.AI.AIHOUND.*;
import static ru.m210projects.Blood.AI.AIINNOCENT.*;
import static ru.m210projects.Blood.AI.AIPOD.*;
import static ru.m210projects.Blood.AI.AIRAT.*;
import static ru.m210projects.Blood.AI.AISPID.*;
import static ru.m210projects.Blood.AI.AITCHERNOBOG.*;
import static ru.m210projects.Blood.AI.AIUNICULT.*;
import static ru.m210projects.Blood.AI.AIZOMBA.*;
import static ru.m210projects.Blood.AI.AIZOMBF.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.checkEventList;
import static ru.m210projects.Blood.EVENT.getNextIncarnation;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxKill3DSound;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Strings.seq;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqFrame;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqSpawn;
import static ru.m210projects.Blood.Warp.gLowerLink;
import static ru.m210projects.Blood.Warp.gUpperLink;
import static ru.m210projects.Blood.Weapon.kAimMaxSlope;
import static ru.m210projects.Build.Engine.CLIPMASK0;
import static ru.m210projects.Build.Engine.pHitInfo;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.*;


public class Ai {

    public static final int LAND = 0;
    public static final int WATER = 1;
    public static final int DUCK = 2;
    public static final int kAIThinkMask = 3;
    public static AISTATE genIdle;
    public static AISTATE genRecoil;
    public static Vector3 EnemyAim = new Vector3();

    static {
        AIZOMBA.Init();
        AIRAT.Init();
        AIINNOCENT.Init();
        AIHAND.Init();
        AICULTIST.Init();
        AIZOMBF.Init();
        AIGILLBEAST.Init();
        AIBURN.Init();
        AIGARG.Init();
        AIBAT.Init();
        AISPID.Init();
        AIHOUND.Init();
        AIGHOST.Init();
        AIBONEEL.Init();
        AITCHERNOBOG.Init();
        AIPOD.Init();
        AIBEAST.Init();
        AICALEB.Init();
        AICERBERUS.Init();

        AIUNICULT.Init();
    }

    public static void aiNewState(BloodSprite pSprite, XSPRITE pXSprite, AISTATE pState) {
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        //System.out.println(pState.seqId +" / "+ pState.ticks);
        pXSprite.setStateTimer(pState.ticks);
        pXSprite.setAIState(pState);
        int seqStartId = pDudeInfo.seqStartID;
        if (pState.seqId >= 0) {
            switch (pSprite.getLotag()) {
                case kGDXDudeUniversalCultist:
                case kGDXGenDudeBurning:
                    seqStartId = pXSprite.getData2();
                    break;
            }

            if (!game.getCache().contains(seqStartId + pState.seqId, seq)) {
                //System.err.println("NO SEQ: fullSeq = "+ (seqStartId + pState.seqId)+" / "+pXSprite.data2+" / "+seqStartId);
                //System.err.println("aiNewState : NULL sequence, dudeType = " + pSprite.lotag +", seqId = " + pState.seqId+ " data1: "+pXSprite.data1);
                return;
            }

            seqSpawn(seqStartId + pState.seqId, SS_SPRITE, pSprite.getExtra(), pState.callback);

        }

        // call the enter function if defined
        if (pState.enter) {
            pState.enter(pSprite, pXSprite);
        }
    }

    public static boolean dudeIsImmune(Sprite pSprite, int dmgType) {
        if (dmgType < 0 || dmgType > 6) {
            return true;
        }
        if (dudeInfo[pSprite.getLotag() - kDudeBase].startDamage[dmgType] == 0) {
            return true;
        }
        // if dude is locked, it immune to any dmg.
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        return pXSprite != null && pXSprite.getLocked() == 1;
    }

    protected static boolean CanMove(Sprite pSprite, int nTarget, int ang, int dist) {
        int dx = mulscale(dist, Cos(ang), 30);
        int dy = mulscale(dist, Sin(ang), 30);

        int x = pSprite.getX();
        int y = pSprite.getY();
        int z = pSprite.getZ();

        HitScan(pSprite, z, Cos(ang) >> 16, Sin(ang) >> 16, 0, pHitInfo, CLIPMASK0, dist);
        int hitDist = EngineUtils.qdist(x - pHitInfo.hitx, y - pHitInfo.hity);

        if (hitDist - (pSprite.getClipdist() << 2) < dist) {
            // okay to be blocked by target
            return pHitInfo.hitsprite != -1 && pHitInfo.hitsprite == nTarget;
        }

        x += dx;
        y += dy;

        if (!FindSector(x, y, z, pSprite.getSectnum())) {
            return false;
        }

        long floorZ = engine.getflorzofslope(foundSector, x, y);
        int nXSector = boardService.getSector(foundSector).getExtra();
        boolean Water = false, Underwater = false, Depth = false, Crusher = false;
        XSECTOR pXSector = null;
        if (nXSector > 0) {
            pXSector = xsector[nXSector];
            if (pXSector.Underwater) {
                Underwater = true;
            }
            if (pXSector.Depth != 0) {
                Depth = true;
            }
            if (boardService.getSector(foundSector).getLotag() == 618 || pXSector.damageType != 0) {
                switch (pSprite.getLotag()) {
                    case kDudeCerberus:
                    case kDudeCerberus2:
                        if (!dudeIsImmune(pSprite, pXSector.damageType) || IsOriginalDemo()) {
                            Crusher = true;
                        }
                        break;
                    default:
                        Crusher = true;
                        break;
                }
            }
        }
        int nUpper = gUpperLink[foundSector];
        int nLower = gLowerLink[foundSector];
        if (nUpper >= 0) {
            Sprite pUpper = boardService.getSprite(nUpper);
            if (pUpper.getLotag() == kStatPurge || pUpper.getLotag() == kStatSpares) { // wrong? should be water and goo markers.
                Depth = true;
                Water = true;
            }
        }
        if (nLower >= 0) {
            Sprite pLower = boardService.getSprite(nLower);
            if (pLower.getLotag() == kStatMarker || pLower.getLotag() == kStatFlare) // wrong? should be water and goo markers.
            {
                Depth = true;
            }
        }

        GetSpriteExtents(pSprite);
        switch (pSprite.getLotag()) {
            case kDudeButcher:
            case kDudeHound:
            case kDudeBrownSpider:
            case kDudeRedSpider:
            case kDudeBlackSpider:
            case kDudeMotherSpider:
            case kDudeInnocent:
            case kDudeCerberus:
            case kDudeRat:
                if (Crusher) {
                    return false;
                }
                if (Depth || Underwater) {
                    return false;
                }
                return floorZ - extents_zBot <= M2Z(1.0);
            case kDudeEel:
                if (Water || !Underwater) {
                    return false;
                }
                if (Underwater) {
                    return true;
                }
                return true;
            case kDudeBat:
            case kDudeFleshGargoyle:
            case kDudeStoneGargoyle:
                if (pSprite.getClipdist() > hitDist) {
                    return false;
                }

                return !Depth;
            case kGDXDudeUniversalCultist:
            case kGDXGenDudeBurning:
                return (!Crusher || dudeIsImmune(pSprite, pXSector.damageType)) && ((!Water && !Underwater) || canSwim(pSprite));
            default:
                if (Crusher) {
                    return false;
                }
                break;
        }

        return (pXSector != null && pXSector.Depth != 0) || (pXSector != null && pXSector.Underwater) || floorZ - extents_zBot <= M2Z(1.0);
    }

    protected static void aiChooseDirection(BloodSprite pSprite, XSPRITE pXSprite, int ang) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax)) {
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        }

        int dang = ((kAngle180 + ang - pSprite.getAng()) & kAngleMask) - kAngle180;

        int sin = Sin(pSprite.getAng());
        int cos = Cos(pSprite.getAng());

        // find vel relative to current angle
        long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);

//		int avoidDist = (int) (vel * (kTimerRate / 2 / kFrameTicks) >> 13);

        int avoidDist = (int) (((15 * vel >> 12) - (15 * vel >> 43)) >> 1);

        int turnTo = kAngle60;
        if (dang < 0) {
            turnTo = -turnTo;
        }

        // clear movement toward target?
        if (CanMove(pSprite, pXSprite.getTarget(), pSprite.getAng() + dang, avoidDist)) {
            pXSprite.setGoalAng((pSprite.getAng() + dang) & kAngleMask);
        }
        // clear movement partially toward target?
        else if (CanMove(pSprite, pXSprite.getTarget(), pSprite.getAng() + dang / 2, avoidDist)) {
            pXSprite.setGoalAng((pSprite.getAng() + dang / 2) & kAngleMask);
        } else if (CanMove(pSprite, pXSprite.getTarget(), pSprite.getAng() - dang / 2, avoidDist)) {
            pXSprite.setGoalAng((pSprite.getAng() - dang / 2) & kAngleMask);
        }
        // try turning in target direction
        else if (CanMove(pSprite, pXSprite.getTarget(), pSprite.getAng() + turnTo, avoidDist)) {
            pXSprite.setGoalAng((pSprite.getAng() + turnTo) & kAngleMask);
        }
        // clear movement straight?
        else if (CanMove(pSprite, pXSprite.getTarget(), pSprite.getAng(), avoidDist)) {
            pXSprite.setGoalAng(pSprite.getAng());
        }
        // try turning away
        else if (CanMove(pSprite, pXSprite.getTarget(), pSprite.getAng() - turnTo, avoidDist)) {
            pXSprite.setGoalAng((pSprite.getAng() - turnTo) & kAngleMask);
        } else {
            pXSprite.setGoalAng((pSprite.getAng() + kAngle60) & kAngleMask);
        }

        // choose dodge direction
        pXSprite.setDodgeDir(Chance(0x4000) ? 1 : -1);

        if (!CanMove(pSprite, pXSprite.getTarget(), pSprite.getAng() + kAngle90 * pXSprite.getDodgeDir(), 512)) {
            pXSprite.setDodgeDir(-pXSprite.getDodgeDir());
            if (!CanMove(pSprite, pXSprite.getTarget(), pSprite.getAng() + kAngle90 * pXSprite.getDodgeDir(), 512)) {
                pXSprite.setDodgeDir(0);
            }
        }
    }

    protected static void aiMoveForward(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax)) {
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        }
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) {
            return;
        }

        int sin = Sin(pSprite.getAng());
        int cos = Cos(pSprite.getAng());

        int frontSpeed = pDudeInfo.frontSpeed;
        if (pSprite.getLotag() == kDudeBurning) {
            if (IsOriginalDemo()) //Burning Innocent original bug
            {
                frontSpeed = 0;
            }
        }

        pSprite.addVelocity(mulscale(cos, frontSpeed, 30), mulscale(sin, frontSpeed, 30), 0);
    }

    protected static void aiMoveTurn(Sprite pSprite, XSPRITE pXSprite) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax)) {
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        }
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;

        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));
    }

    protected static void aiMoveDodge(BloodSprite pSprite, XSPRITE pXSprite) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax)) {
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        }
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        if (pXSprite.getDodgeDir() == 0) {
            return;
        }

        long sin = Sin(pSprite.getAng());
        long cos = Cos(pSprite.getAng());

        // find vel and svel relative to current angle
        long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
        long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

        if (pXSprite.getDodgeDir() > 0) {
            svel += pDudeInfo.sideSpeed;
        } else {
            svel -= pDudeInfo.sideSpeed;
        }

        // reconstruct x and y velocities
        pSprite.setVelocityX(dmulscale(vel, cos, svel, sin, 30));
        pSprite.setVelocityY(dmulscale(vel, sin, -svel, cos, 30));
    }

    public static void aiActivateDude(BloodSprite pSprite, XSPRITE pXPad) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax)) {
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        }

        XSPRITE pXSprite = pSprite.getXSprite();
        if (pXSprite.getState() == 0) {
            // this doesn't take into account sprites triggered w/o a target location....
            int nAngle = EngineUtils.getAngle(pXSprite.getTargetX() - pSprite.getX(), pXSprite.getTargetY() - pSprite.getY());
            aiChooseDirection(pSprite, pXSprite, nAngle);
            pXSprite.setState(1);
        }

        DudeExtra pDudeExtra = pXSprite.getDudeExtra();
        switch (pSprite.getLotag()) {
            case kDudePhantasm:
                pDudeExtra.setActive(true);
                pDudeExtra.setThinkTime(0);
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, ghostSearch);
                } else {
                    aiPlaySound(pSprite, 1600, 1, -1);
                    aiNewState(pSprite, pXSprite, ghostChase);
                }
                break;

            case kDudeTommyCultist:
            case kDudeShotgunCultist:
            case kDudeBeastCultist:
            case kDudeTeslaCultist:
            case kDudeDynamiteCultist:
                pDudeExtra.setActive(true);
                if (pXSprite.getTarget() == -1) {
                    if (pXSprite.getPalette() != 0 && pXSprite.getPalette() <= 2) {
                        aiNewState(pSprite, pXSprite, cultistSearch[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, cultistSearch[LAND]);
                        if (Chance(0x4000)) {
                            if (pSprite.getLotag() == kDudeTommyCultist) {
                                aiPlaySound(pSprite, Random(5) + 4008, 1, -1);
                            } else {
                                aiPlaySound(pSprite, Random(5) + 1008, 1, -1);
                            }
                        }
                    }
                } else {
                    if (Chance(0x4000)) {
                        if (pSprite.getLotag() == kDudeTommyCultist) {
                            aiPlaySound(pSprite, Random(5) + 4003, 1, -1);
                        } else {
                            aiPlaySound(pSprite, Random(5) + 1003, 1, -1);
                        }
                    }
                    if (pXSprite.getPalette() != 0 && pXSprite.getPalette() <= 2) {
                        aiNewState(pSprite, pXSprite, cultistChase[WATER]);
                    } else {
                        if (pSprite.getLotag() == kDudeTommyCultist) {
                            aiNewState(pSprite, pXSprite, cultistTurn);
                        } else {
                            aiNewState(pSprite, pXSprite, cultistChase[LAND]);
                        }
                    }
                }
                break;
            case kGDXDudeUniversalCultist:
                pDudeExtra.setActive(true);
                if (pXSprite.getTarget() == -1) {
                    if (spriteIsUnderwater(pSprite, false)) {
                        aiNewState(pSprite, pXSprite, GDXGenDudeSearch[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, GDXGenDudeSearch[LAND]);
                        if (Chance(0x4000)) {
                            sfxPlayGDXGenDudeSound(pSprite, 0);
                        }
                    }
                } else {
                    if (Chance(0x4000)) {
                        sfxPlayGDXGenDudeSound(pSprite, 0);
                    }

                    if (spriteIsUnderwater(pSprite, false)) {
                        aiNewState(pSprite, pXSprite, GDXGenDudeChase[WATER]);
                    } else
                    //aiNewState(pSprite, pXSprite, GDXGenDudeTurn);
                    {
                        aiNewState(pSprite, pXSprite, GDXGenDudeChase[LAND]);
                    }

                }
                break;
            case kDudeFanaticProne:
                pSprite.setLotag(kDudeTommyCultist);
                pDudeExtra.setActive(true);
                if (pXSprite.getTarget() == -1) {
                    if (pXSprite.getPalette() != 0 && pXSprite.getPalette() <= 2) {
                        aiNewState(pSprite, pXSprite, cultistSearch[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, cultistSearch[LAND]);
                        if (Chance(0x4000)) {
                            aiPlaySound(pSprite, Random(5) + 4008, 1, -1);
                        }
                    }
                } else {
                    if (Chance(0x4000)) {
                        aiPlaySound(pSprite, Random(5) + 4008, 1, -1);
                    }
                    if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                        aiNewState(pSprite, pXSprite, cultistChase[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, cultistChase[DUCK]);
                    }
                }
                break;

            case kDudeCultistProne:
                pSprite.setLotag(kDudeShotgunCultist);
                pDudeExtra.setActive(true);
                if (pXSprite.getTarget() == -1) {
                    if (pXSprite.getPalette() != 0 && pXSprite.getPalette() <= 2) {
                        aiNewState(pSprite, pXSprite, cultistSearch[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, cultistSearch[LAND]);
                        if (Chance(0x4000)) {
                            aiPlaySound(pSprite, Random(5) + 1008, 1, -1);
                        }
                    }
                } else {
                    if (Chance(0x4000)) {
                        aiPlaySound(pSprite, Random(5) + 1003, 1, -1);
                    }
                    if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                        aiNewState(pSprite, pXSprite, cultistChase[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, cultistChase[DUCK]);
                    }
                }
                break;

            case kDudeCultistBurning:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, burnSearch[CULTIST]);
                } else {
                    aiNewState(pSprite, pXSprite, burnChase[CULTIST]);
                }
                break;

            case kGDXGenDudeBurning:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, burnSearch[GDXGENDUDE]);
                } else {
                    aiNewState(pSprite, pXSprite, burnChase[GDXGENDUDE]);
                }
                break;

            case kDudeBat:
                pDudeExtra.setActive(true);
                pDudeExtra.setThinkTime(0);
                if (pSprite.getHitag() == 0) {
                    pSprite.setHitag((kAttrMove | kAttrAiming));
                }
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, batSearch);
                } else {
                    if (Chance(0x5000)) {
                        aiPlaySound(pSprite, 2000, 1, -1);
                    }
                    aiNewState(pSprite, pXSprite, batChase);
                }
                break;

            case kDudeEel:
                pDudeExtra.setActive(true);
                pDudeExtra.setThinkTime(0);
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, boneelSearch);
                } else {
                    if (Chance(0x4000)) {
                        aiPlaySound(pSprite, 1501, 1, -1);
                    } else {
                        aiPlaySound(pSprite, 1500, 1, -1);
                    }
                    aiNewState(pSprite, pXSprite, boneelChase);
                }
                break;

            case kDudeGillBeast:
                pDudeExtra.setActive(true);
                pDudeExtra.setThinkTime(0);
                XSECTOR pXSector = boardService.getSector(pSprite.getSectnum()).getExtra() >= 0 ? xsector[boardService.getSector(pSprite.getSectnum()).getExtra()] : null;
                if (pXSprite.getTarget() == -1) {
                    if (pXSector != null && pXSector.Underwater) {
                        aiNewState(pSprite, pXSprite, gillBeastSearch[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, gillBeastSearch[LAND]);
                    }
                } else {
                    if (Chance(0x2000)) {
                        aiPlaySound(pSprite, 1701, 1, -1);
                    } else {
                        aiPlaySound(pSprite, 1700, 1, -1);
                    }
                    if (pXSector != null && pXSector.Underwater) {
                        aiNewState(pSprite, pXSprite, gillBeastChase[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, gillBeastChase[LAND]);
                    }
                }
                break;
            case kDudeAxeZombie:
                pDudeExtra.setThinkTime(1);
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, zombieASearch);
                } else {
                    if (!Chance(0x5000)) {
                        aiNewState(pSprite, pXSprite, zombieAChase);
                    } else {
                        aiPlaySound(pSprite, Random(3) + 1103, 1, -1);
                        aiNewState(pSprite, pXSprite, zombieAChase);
                    }
                }
                break;

            case kDudeEarthZombie:
                pDudeExtra.setThinkTime(1);
                if (pXSprite.getAIState() == zombieEIdle) {
                    aiNewState(pSprite, pXSprite, zombieEUp);
                }
                break;

            case kDudeSleepZombie:
                pDudeExtra.setThinkTime(1);
                if (pXSprite.getAIState() == zombieSLIdle) {
                    aiNewState(pSprite, pXSprite, zombieSLUP);
                }
                break;

            case kDudeButcher:
                pDudeExtra.setThinkTime(1);
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, zombieFSearch);
                } else {
                    if (Chance(0x4000)) {
                        aiPlaySound(pSprite, 1201, 1, -1);
                    } else {
                        aiPlaySound(pSprite, 1200, 1, -1);
                    }
                    aiNewState(pSprite, pXSprite, zombieFChase);
                }
                break;

            case kDudeAxeZombieBurning:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, burnSearch[ZOMBIE]);
                } else {
                    aiNewState(pSprite, pXSprite, burnChase[ZOMBIE]);
                }
                break;

            case kDudeTinyCalebburning:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, burnSearch[TINYCALEB]);
                } else {
                    aiNewState(pSprite, pXSprite, burnChase[TINYCALEB]);
                }
                break;

            case kDudeBloatedButcherBurning:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, burnSearch[BUTCHER]);
                } else {
                    aiNewState(pSprite, pXSprite, burnChase[INNOCENT]);
                }
                break;

            case kDudeFleshGargoyle:
            case kDudeStoneGargoyle:
                pDudeExtra.setActive(true);
                pDudeExtra.setThinkTime(0);
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, gargoyleSearch);
                } else {
                    aiNewState(pSprite, pXSprite, gargoyleChase);

                    if (pSprite.getLotag() == kDudeFleshGargoyle) {
                        if (Chance(0x4000)) {
                            aiPlaySound(pSprite, 1401, 1, -1);
                        } else {
                            aiPlaySound(pSprite, 1400, 1, -1);
                        }
                    } else {
                        if (Chance(0x4000)) {
                            aiPlaySound(pSprite, 1451, 1, -1);
                        } else {
                            aiPlaySound(pSprite, 1450, 1, -1);
                        }
                    }
                }
                break;
            case kDudeFleshStatue:
            case kDudeStoneStatue:

                if (IsOriginalDemo() || pXSprite.getData1() != 1) {
                    if (Chance(0x4000)) {
                        aiPlaySound(pSprite, 1401, 1, -1);
                    } else {
                        aiPlaySound(pSprite, 1400, 1, -1);
                    }

                    if (pSprite.getLotag() == kDudeFleshStatue) {
                        aiNewState(pSprite, pXSprite, statueFChase);
                    } else {
                        aiNewState(pSprite, pXSprite, statueSChase);
                    }
                } else {
                    if (pSprite.getLotag() == kDudeFleshStatue) {
                        aiNewState(pSprite, pXSprite, statueFTransformNew);
                    } else {
                        aiNewState(pSprite, pXSprite, statueSTransformNew);
                    }
                }

                break;
            case kDudeCerberus:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, cerberusSearch[C1]);
                } else {
                    aiPlaySound(pSprite, 2300, 1, -1);
                    aiNewState(pSprite, pXSprite, cerberusChase[C1]);
                }
                break;
            case kDudeCerberus2:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, cerberusSearch[C2]);
                } else {
                    aiPlaySound(pSprite, 2300, 1, -1);
                    aiNewState(pSprite, pXSprite, cerberusChase[C2]);
                }
                break;

            case kDudeHound:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, houndSearch);
                } else {
                    aiPlaySound(pSprite, 1300, 1, -1);
                    aiNewState(pSprite, pXSprite, houndChase);
                }
                break;

            case kDudeHand:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, handSearch);
                } else {
                    aiPlaySound(pSprite, 1900, 1, -1);
                    aiNewState(pSprite, pXSprite, handChase);
                }
                break;

            case kDudeRat:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, ratSearch);
                } else {
                    aiPlaySound(pSprite, 2100, 1, -1);
                    aiNewState(pSprite, pXSprite, ratChase);
                }
                break;

            case kDudeInnocent:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, innocentSearch);
                } else {
                    if (pXSprite.getHealth() > 0) {
                        aiPlaySound(pSprite, BiRandom(6) + 7000, 1, -1); //ICRYING
                    }
                    aiNewState(pSprite, pXSprite, innocentChase);
                }
                break;

            case kDudeTchernobog:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, tchernobogSearch);
                } else {
                    aiPlaySound(pSprite, Random(7) + 2350, 1, -1);
                    aiNewState(pSprite, pXSprite, tchernobogChase);
                }
                break;

            case kDudeBrownSpider:
            case kDudeRedSpider:
            case kDudeBlackSpider:
            case kDudeMotherSpider:
                if (pSprite.getLotag() == kDudeMotherSpider) {
                    pDudeExtra.setActive(true);
                }
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteFlipY);
                pSprite.setHitag(pSprite.getHitag() | kAttrGravity);
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, spidSearch);
                } else {
                    if (pSprite.getLotag() != kDudeMotherSpider) {
                        aiPlaySound(pSprite, 1800, 1, -1);
                    } else {
                        aiPlaySound(pSprite, Random(1) + 1853, 1, -1);
                    }
                    aiNewState(pSprite, pXSprite, spidChase);
                }
                break;

            case kDudeTinyCaleb:
                pDudeExtra.setThinkTime(1);
                if (pXSprite.getTarget() == -1) {
                    if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                        aiNewState(pSprite, pXSprite, calebSearch[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, calebSearch[LAND]);
                    }
                } else {
                    if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                        aiNewState(pSprite, pXSprite, calebChase[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, calebChase[LAND]);
                    }
                }
                break;
            case kDudeTheBeast:
                if (pXSprite.getTarget() == -1) {
                    if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                        aiNewState(pSprite, pXSprite, beastSearch[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, beastSearch[LAND]);
                    }
                } else {
                    aiPlaySound(pSprite, Random(2) + 9009, 1, -1);
                    if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                        aiNewState(pSprite, pXSprite, beastChase[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, beastChase[LAND]);
                    }
                }
                break;
            case kDudeGreenPod:
            case kDudeFirePod:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, podSearch);
                } else {
                    if (pSprite.getLotag() == kDudeFirePod) {
                        aiPlaySound(pSprite, 2453, 1, -1);
                    } else {
                        aiPlaySound(pSprite, 2473, 1, -1);
                    }

                    aiNewState(pSprite, pXSprite, podChase);
                }
                break;
            case kDudeGreenTentacle:
            case kDudeFireTentacle:
                if (pXSprite.getTarget() == -1) {
                    aiNewState(pSprite, pXSprite, tentacleSearch);
                } else {
                    aiPlaySound(pSprite, 2503, 1, -1);
                    aiNewState(pSprite, pXSprite, tentacleChase);
                }
                break;
        }
    }

    /*******************************************************************************
     FUNCTION:		aiSetTarget()

     DESCRIPTION:	Target a location (as opposed to a sprite)
     *******************************************************************************/
    public static void aiSetTarget(XSPRITE pXSprite, int x, int y, int z) {
        pXSprite.setTarget(-1);
        pXSprite.setTargetX(x);
        pXSprite.setTargetY(y);
        pXSprite.setTargetZ(z);
    }

    public static void aiSetTarget(XSPRITE pXSprite, int nTarget) {

        if (!boardService.isValidSprite(nTarget)) {
            throw new AssertException("isValidSprite(nTarget)");
        }
        Sprite pTarget = boardService.getSprite(nTarget);
        if (pTarget.getLotag() < kDudeBase || pTarget.getLotag() >= kDudeMax) {
            return;
        }

        if (nTarget == boardService.getSprite(pXSprite.getReference()).getOwner()) {
            return;
        }

        DudeInfo pTargetInfo = dudeInfo[pTarget.getLotag() - kDudeBase];

        pXSprite.setTarget(nTarget);
        pXSprite.setTargetX(pTarget.getX());
        pXSprite.setTargetY(pTarget.getY());
        pXSprite.setTargetZ(pTarget.getZ() - (pTargetInfo.eyeHeight * pTarget.getYrepeat() << 2));
    }

    public static int aiDamageSprite(BloodSprite pSprite, XSPRITE pXSprite, int nSource, int nDamageType, int nDamage) {
        if (!boardService.isValidSprite(nSource)) {
            throw new AssertException("isValidSprite(nSource)");
        }

        if (pXSprite.getHealth() == 0) {
            return 0;
        }

        pXSprite.setHealth(ClipLow(pXSprite.getHealth() - nDamage, 0));
        DudeExtra xDude = pXSprite.getDudeExtra();
        xDude.setCumulDamage(xDude.getCumulDamage() + nDamage);

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        if (nSource >= 0) {
            if (nSource == pXSprite.getReference()) {
                return 0;
            }

            if (pXSprite.getTarget() == -1) {
                // give a dude a target
                aiSetTarget(pXSprite, nSource);
                aiActivateDude(pSprite, pXSprite);
            } else if (nSource != pXSprite.getTarget()) {
                // retarget
                int nThresh = nDamage;

                if (boardService.getSprite(nSource).getLotag() == pSprite.getLotag()) {
                    nThresh *= pDudeInfo.changeTargetKin;
                } else {
                    nThresh *= pDudeInfo.changeTarget;
                }

                if (Chance(nThresh / 2)) {
                    aiSetTarget(pXSprite, nSource);
                    aiActivateDude(pSprite, pXSprite);
                }
            }

            if (nDamageType == 6) {
                xDude.setTeslaHit(true);
            } else {
                if (!IsOriginalDemo()) {
                    xDude.setTeslaHit(false);
                }
            }

            // you DO need special processing here or somewhere else (your choice) for dodging
            switch (pSprite.getLotag()) {
                case kDudeFleshGargoyle:
                    aiNewState(pSprite, pXSprite, gargoyleChase);
                    break;
                case kDudeCultistBurning:
                    if (Chance(0x2000) && gFrameClock > xDude.getClock()) {
                        aiPlaySound(pSprite, Random(2) + 1031, 2, -1);
                        xDude.setClock(gFrameClock + 360);
                    }
                    boolean tommyChance = Chance(0x300);
                    if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                        if (tommyChance) {
                            pSprite.setLotag(kDudeTommyCultist);
                        } else {
                            pSprite.setLotag(kDudeShotgunCultist);
                        }
                        pXSprite.setBurnTime(0);
                        aiNewState(pSprite, pXSprite, cultistGoto[WATER]);
                    }
                    break;

                case kGDXGenDudeBurning:
                    if (Chance(0x2000) && gFrameClock > xDude.getClock()) {
                        sfxPlayGDXGenDudeSound(pSprite, 3);
                        xDude.setClock(gFrameClock + 360);
                    }

                    if (pXSprite.getBurnTime() <= 0) {
                        pXSprite.setBurnTime(2400);
                    }
                    if (spriteIsUnderwater(pSprite, false)) {

                        pSprite.setLotag(kGDXDudeUniversalCultist);
                        pXSprite.setBurnTime(0);
                        aiNewState(pSprite, pXSprite, GDXGenDudeGoto[WATER]);
                    }
                    break;

                case kDudeInnocent:
                    if (nDamageType == 1 && pXSprite.getHealth() <= pDudeInfo.fleeHealth) {
                        aiPlaySound(pSprite, 361, 0, -1);
                        pSprite.setLotag(kDudeBurning);
                        xDude.setClock(gFrameClock + 360);
                        aiNewState(pSprite, pXSprite, burnGoto[CULTIST]);
                        actHealDude(pXSprite, dudeInfo[39].startHealth, dudeInfo[39].startHealth);
                        if (!IsOriginalDemo()) {
                            if (pXSprite.getBurnTime() == 0) {
                                pXSprite.setBurnTime(1200);
                            }
                        }
                        checkEventList(pXSprite.getReference(), SS_SPRITE, 0);
                    }
                    break;
                case kDudeBeastCultist:
                    if (pXSprite.getHealth() <= pDudeInfo.fleeHealth) {
                        aiPlaySound(pSprite, 9008, 0, -1);
                        pSprite.setLotag(kDudeTheBeast);
                        aiNewState(pSprite, pXSprite, beastTransforming);
                        actHealDude(pXSprite, dudeInfo[51].startHealth, dudeInfo[51].startHealth);
                    }
                    break;
                case kDudeTinyCaleb:
                    if (nDamageType == 1 && pXSprite.getHealth() <= pDudeInfo.fleeHealth) {
                        aiPlaySound(pSprite, 361, 0, -1);
                        if (!IsOriginalDemo()) {
                            if (pXSprite.getBurnTime() == 0) {
                                pXSprite.setBurnTime(1200);
                            }
                            pSprite.setLotag(kDudeTinyCalebburning);
                            aiNewState(pSprite, pXSprite, burnGoto[TINYCALEB]);
                            actHealDude(pXSprite, dudeInfo[51].startHealth, dudeInfo[51].startHealth);
                        } else {
                            pSprite.setLotag(kDudeBurning);
                            aiNewState(pSprite, pXSprite, burnGoto[CULTIST]);
                            actHealDude(pXSprite, dudeInfo[39].startHealth, dudeInfo[39].startHealth);
                        }
                        xDude.setClock(gFrameClock + 360);
                        checkEventList(pXSprite.getReference(), SS_SPRITE, 0);
                    }
                    break;
                case kGDXDudeUniversalCultist:
                    if (nDamageType == kDamageBurn) {
                        if (pXSprite.getHealth() <= pDudeInfo.fleeHealth) {
                            if (getNextIncarnation(pXSprite) == null) {
                                removeDudeStuff(pSprite);
                                if (pXSprite.getData1() >= kThingHiddenExploder && pXSprite.getData1() < (kThingHiddenExploder + kExplodeMax) - 1) {
                                    doExplosion(pSprite, pXSprite.getData1() - kThingHiddenExploder);
                                }

                                if (spriteIsUnderwater(pSprite, false)) {
                                    pXSprite.setHealth(0);
                                    break;
                                }

                                if ((game.getCache().contains(pXSprite.getData2() + 15, seq) || game.getCache().contains(pXSprite.getData2() + 16, seq)) && game.getCache().contains(pXSprite.getData2() + 3, seq)) {

                                    aiPlaySound(pSprite, 361, 0, -1);
                                    sfxPlayGDXGenDudeSound(pSprite, 3);

                                    pSprite.setLotag(kGDXGenDudeBurning);

                                    if (pXSprite.getData2() == 11520) // don't inherit palette for burning if using default animation
                                    {
                                        pSprite.setPal(12);
                                    }

                                    aiNewState(pSprite, pXSprite, burnGoto[CULTIST]);
                                    actHealDude(pXSprite, dudeInfo[55].startHealth, dudeInfo[55].startHealth);
                                    xDude.setClock(gFrameClock + 360);
                                    checkEventList(pXSprite.getReference(), SS_SPRITE, 0);
                                }
                            } else {
                                actKillSprite(nSource, pSprite, nDamageType, nDamage);
                            }
                        }

                    } else if (!inDodge(pXSprite.getAIState())) {

                        int chance3 = getDodgeChance(pSprite);
                        if (Chance(chance3) || inIdle(pXSprite.getAIState())) {
                            //System.err.println("DODGE ON: "+chance3);
                            if (!spriteIsUnderwater(pSprite, false)) {
                                if (!canDuck(pSprite) || !checkUniCultistSeq(pSprite, 14)) {
                                    aiNewState(pSprite, pXSprite, GDXGenDudeDodgeDmg[LAND]);
                                } else {
                                    aiNewState(pSprite, pXSprite, GDXGenDudeDodgeDmg[DUCK]);
                                }

                                if (Chance(0x0200)) {
                                    sfxPlayGDXGenDudeSound(pSprite, 1);
                                }

                            } else if (checkUniCultistSeq(pSprite, 13)) {
                                aiNewState(pSprite, pXSprite, GDXGenDudeDodgeDmg[WATER]);
                            }
                        }
                    }
                    break;

                case kDudeTommyCultist:
                case kDudeShotgunCultist:
                case kDudeTeslaCultist:
                case kDudeDynamiteCultist:
                    if (nDamageType == kDamageBurn) {
                        if (pXSprite.getHealth() <= pDudeInfo.fleeHealth) {
                            if (!IsOriginalDemo()) {
                                if (pXSprite.getPalette() != 0) {
                                    pXSprite.setHealth(0);
                                    break;
                                }
                                if (pXSprite.getBurnTime() == 0) {
                                    pXSprite.setBurnTime(1200);
                                }
                            }

                            aiPlaySound(pSprite, 361, 0, -1);
                            aiPlaySound(pSprite, 1031 + Random(2), 2, -1);
                            pSprite.setLotag(kDudeCultistBurning);
                            aiNewState(pSprite, pXSprite, burnGoto[CULTIST]);
                            actHealDude(pXSprite, dudeInfo[40].startHealth, dudeInfo[40].startHealth);
                            xDude.setClock(gFrameClock + 360);
                            checkEventList(pXSprite.getReference(), SS_SPRITE, 0);
                        }
                    } else {
                        if (pXSprite.getPalette() == 0) {
                            if (!checkDudeSeq(pSprite, 14)) {
                                aiNewState(pSprite, pXSprite, cultistDodge[LAND]);
                            } else {
                                aiNewState(pSprite, pXSprite, cultistDodge[DUCK]);
                            }
                        } else if (checkDudeSeq(pSprite, 13) && (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2)) {
                            aiNewState(pSprite, pXSprite, cultistDodge[WATER]);
                        }
                    }

                    break;
                case kDudeAxeZombie:
                case kDudeEarthZombie:
                    if (nDamageType == kDamageBurn && pXSprite.getHealth() <= pDudeInfo.fleeHealth) {
                        aiPlaySound(pSprite, 361, 0, -1);
                        aiPlaySound(pSprite, 1106, 2, -1);
                        pSprite.setLotag(kDudeAxeZombieBurning);
                        aiNewState(pSprite, pXSprite, burnGoto[ZOMBIE]);
                        if (!IsOriginalDemo()) {
                            if (pXSprite.getBurnTime() == 0) {
                                pXSprite.setBurnTime(1200);
                            }
                        }
                        actHealDude(pXSprite, dudeInfo[41].startHealth, dudeInfo[41].startHealth);
                        checkEventList(pXSprite.getReference(), SS_SPRITE, 0);
                    }
                    break;
                case kDudeButcher:
                    if (nDamageType == 1 && pXSprite.getHealth() <= pDudeInfo.fleeHealth) {
                        aiPlaySound(pSprite, 361, 0, -1);
                        aiPlaySound(pSprite, 1202, 2, -1);

                        pSprite.setLotag(kDudeBloatedButcherBurning);
                        aiNewState(pSprite, pXSprite, burnGoto[BUTCHER]);
                        if (!IsOriginalDemo()) {
                            if (pXSprite.getBurnTime() == 0) {
                                pXSprite.setBurnTime(1200);
                            }
                        }
                        actHealDude(pXSprite, dudeInfo[42].startHealth, dudeInfo[42].startHealth);
                        checkEventList(pXSprite.getReference(), SS_SPRITE, 0);
                    }
                    break;
            }
        }

        return nDamage;
    }

    protected static void RecoilDude(BloodSprite pSprite) {
        boolean chance = Chance(0x4000);
        if (pSprite.getStatnum() != kStatDude || !IsDudeSprite(pSprite)) {
            return;
        }

        XSPRITE pXSprite = pSprite.getXSprite();
        DudeExtra dudeExtra = pXSprite.getDudeExtra();
        switch (pSprite.getLotag()) {
            case kGDXDudeUniversalCultist:

                int mass = getDudeMassBySpriteSize(pSprite);
                int chance4 = getRecoilChance(pSprite);
                boolean chance3 = Chance(chance4);
                if (dudeExtra.isTeslaHit() && (inIdle(pXSprite.getAIState()) || mass < 155 || (mass >= 155 && chance3)) && !spriteIsUnderwater(pSprite, false)) {
                    sfxPlayGDXGenDudeSound(pSprite, 1);

                    if (game.getCache().contains(pXSprite.getData2() + 4, seq)) {
                        GDXGenDudeRTesla.next = (Chance(chance4 * 2) ? GDXGenDudeDodge[LAND] : GDXGenDudeDodgeDmg[LAND]);
                        aiNewState(pSprite, pXSprite, GDXGenDudeRTesla);
                    } else if (canDuck(pSprite) && (Chance(chance4) || pGameInfo.nDifficulty == 0)) {
                        aiNewState(pSprite, pXSprite, GDXGenDudeRecoil[DUCK]);
                    } else if (canSwim(pSprite) && spriteIsUnderwater(pSprite, false)) {
                        aiNewState(pSprite, pXSprite, GDXGenDudeRecoil[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, GDXGenDudeRecoil[LAND]);
                    }
                    break;
                }


                if (inDodge(pXSprite.getAIState())) {
                    sfxPlayGDXGenDudeSound(pSprite, 1);
                    break;
                }

                if (chance3 || inIdle(pXSprite.getAIState()) || Chance(getRecoilChance(pSprite)) || (!dudeIsMelee(pXSprite) && mass < 155)) {
                    sfxPlayGDXGenDudeSound(pSprite, 1);

                    if (canDuck(pSprite) && (Chance(chance4) || pGameInfo.nDifficulty == 0)) {
                        aiNewState(pSprite, pXSprite, GDXGenDudeRecoil[DUCK]);
                    } else if (canSwim(pSprite) && spriteIsUnderwater(pSprite, false)) {
                        aiNewState(pSprite, pXSprite, GDXGenDudeRecoil[WATER]);
                    } else {
                        aiNewState(pSprite, pXSprite, GDXGenDudeRecoil[LAND]);
                    }
                }

                break;
            case kDudeTommyCultist:
            case kDudeShotgunCultist:
            case kDudeTeslaCultist:
            case kDudeDynamiteCultist:
            case kDudeBeastCultist:
                if (pSprite.getLotag() == kDudeTommyCultist) {
                    aiPlaySound(pSprite, Random(2) + 4013, 2, -1);
                } else {
                    aiPlaySound(pSprite, Random(2) + 1013, 2, -1);
                }

                if (pXSprite.getPalette() == 0 && dudeExtra.isTeslaHit()) {
                    aiNewState(pSprite, pXSprite, cultistRTesla);
                    dudeExtra.setTeslaHit(false);
                    return;
                }
                if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                    aiNewState(pSprite, pXSprite, cultistRecoil[WATER]);
                } else {
                    if (!chance || (pGameInfo.nDifficulty == 0)) {
                        aiNewState(pSprite, pXSprite, cultistRecoil[DUCK]);
                    } else {
                        aiNewState(pSprite, pXSprite, cultistRecoil[LAND]);
                    }
                }

                break;
            case kDudeCultistBurning:
                aiNewState(pSprite, pXSprite, burnGoto[CULTIST]);
                break;

            case kGDXGenDudeBurning:
                aiNewState(pSprite, pXSprite, burnGoto[GDXGENDUDE]);
                break;

            case kDudeTinyCalebburning:
                aiNewState(pSprite, pXSprite, burnGoto[TINYCALEB]);
                break;

            case kDudeButcher:
                aiPlaySound(pSprite, 1202, 2, -1);
                if (dudeExtra.isTeslaHit()) {
                    aiNewState(pSprite, pXSprite, zombieFRTesla);
                } else {
                    aiNewState(pSprite, pXSprite, zombieFRecoil);
                }
                break;

            case kDudeAxeZombie:
            case kDudeEarthZombie:
                aiPlaySound(pSprite, 1106, 2, -1); //AZOMPAIN
                if (dudeExtra.isTeslaHit() && pXSprite.getData3() > dudeInfo[3].startHealth / 3) {
                    aiNewState(pSprite, pXSprite, zombieARTesla);
                } else if (pXSprite.getData3() <= dudeInfo[3].startHealth / 3) {
                    aiNewState(pSprite, pXSprite, zombieARecoil);
                } else {
                    aiNewState(pSprite, pXSprite, zombieAFall);
                }
                break;

            case kDudeAxeZombieBurning:
                aiPlaySound(pSprite, 1106, 2, -1);
                aiNewState(pSprite, pXSprite, burnGoto[ZOMBIE]);
                break;

            case kDudeBloatedButcherBurning:
                aiPlaySound(pSprite, 1202, 2, -1);
                aiNewState(pSprite, pXSprite, burnGoto[BUTCHER]);
                break;

            case kDudeFleshGargoyle:
            case kDudeStoneGargoyle:
                aiPlaySound(pSprite, 1402, 2, -1);
                aiNewState(pSprite, pXSprite, gargoyleRecoil);
                break;

            case kDudeCerberus:
                aiPlaySound(pSprite, Random(2) + 2302, 2, -1);
                if (dudeExtra.isTeslaHit() && pXSprite.getData3() > dudeInfo[3].startHealth / 3) {
                    aiNewState(pSprite, pXSprite, cerberusRTesla);
                } else {
                    aiNewState(pSprite, pXSprite, cerberusRecoil[C1]);
                }
                break;
            case kDudeCerberus2:
                aiPlaySound(pSprite, Random(2) + 2302, 2, -1);
                aiNewState(pSprite, pXSprite, cerberusRecoil[C2]);
                break;

            case kDudeHound:
                aiPlaySound(pSprite, 1302, 2, -1);
                if (dudeExtra.isTeslaHit()) {
                    aiNewState(pSprite, pXSprite, houndRTesla);
                } else {
                    aiNewState(pSprite, pXSprite, houndRecoil);
                }
                break;
            case kDudeTchernobog:
                aiPlaySound(pSprite, Random(6) + 2370, 2, -1);
                aiNewState(pSprite, pXSprite, tchernobogRecoil);
                break;

            case kDudeHand:
                aiPlaySound(pSprite, 1902, 2, -1);
                aiNewState(pSprite, pXSprite, handRecoil);
                break;

            case kDudeRat:
                aiPlaySound(pSprite, 2102, 2, -1);
                aiNewState(pSprite, pXSprite, ratRecoil);
                break;

            case kDudeBat:
                aiPlaySound(pSprite, 2002, 2, -1);
                aiNewState(pSprite, pXSprite, batRecoil);
                break;

            case kDudeEel:
                aiPlaySound(pSprite, 1502, 2, -1);
                aiNewState(pSprite, pXSprite, boneelRecoil);
                break;

            case kDudeGillBeast:
                XSECTOR pXSector = boardService.getSector(pSprite.getSectnum()).getExtra() >= 0 ? xsector[boardService.getSector(pSprite.getSectnum()).getExtra()] : null;
                aiPlaySound(pSprite, 1702, 2, -1);
                if (pXSector != null && pXSector.Underwater) {
                    aiNewState(pSprite, pXSprite, gillBeastRecoil[WATER]);
                } else {
                    aiNewState(pSprite, pXSprite, gillBeastRecoil[LAND]);
                }
                break;

            case kDudePhantasm:
                aiPlaySound(pSprite, 1602, 2, -1);
                if (dudeExtra.isTeslaHit()) {
                    aiNewState(pSprite, pXSprite, ghostRTesla);
                } else {
                    aiNewState(pSprite, pXSprite, ghostRecoil);
                }
                break;

            case kDudeRedSpider:
            case kDudeBrownSpider:
            case kDudeBlackSpider:
                aiPlaySound(pSprite, Random(1) + 1802, 2, -1);
                aiNewState(pSprite, pXSprite, spidDodge);
                break;

            case kDudeMotherSpider:
                aiPlaySound(pSprite, Random(1) + 1851, 2, -1);
                aiNewState(pSprite, pXSprite, spidDodge);
                break;

            case kDudeInnocent:
                aiPlaySound(pSprite, BiRandom(2) + 7007, 2, -1); //ISCREAM1
                if (dudeExtra.isTeslaHit()) {
                    aiNewState(pSprite, pXSprite, innocentTesla);
                } else {
                    aiNewState(pSprite, pXSprite, innocentRecoil);
                }
                break;
            case kDudeTinyCaleb:
                if (dudeExtra.isTeslaHit()) {
                    aiNewState(pSprite, pXSprite, calebRTesla);
                    dudeExtra.setTeslaHit(false);
                    return;
                }

                if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                    aiNewState(pSprite, pXSprite, calebRecoil[WATER]);
                } else {
                    aiNewState(pSprite, pXSprite, calebRecoil[LAND]);
                }
                break;
            case kDudeTheBeast:
                aiPlaySound(pSprite, BiRandom(2) + 9004, 2, -1);
                if (pXSprite.getPalette() != 1 && pXSprite.getPalette() != 2 && dudeExtra.isTeslaHit()) {
                    aiNewState(pSprite, pXSprite, beastRTesla);
                    dudeExtra.setTeslaHit(false);
                    return;
                }

                if (pXSprite.getPalette() == 1 || pXSprite.getPalette() == 2) {
                    aiNewState(pSprite, pXSprite, beastRecoil[WATER]);
                } else {
                    aiNewState(pSprite, pXSprite, beastRecoil[LAND]);
                }
                break;
            case kDudeGreenPod:
            case kDudeFirePod:
                aiNewState(pSprite, pXSprite, podRecoil);
                break;
            case kDudeGreenTentacle:
            case kDudeFireTentacle:
                aiNewState(pSprite, pXSprite, tentacleRecoil);
                break;
            default:
                aiNewState(pSprite, pXSprite, genRecoil);
                break;
            case kDudeFleshStatue:
            case kDudeStoneStatue:
                break;
        }
        dudeExtra.setTeslaHit(false);
    }

    protected static void aiPlaySound(BloodSprite pSprite, int nSound, int soundonce, int nChannel) {
        if (soundonce != 0) {
            DudeExtra dudeExtra = pSprite.getXSprite().getDudeExtra();
            if (soundonce > dudeExtra.getSoundOnce() || gFrameClock >= dudeExtra.getClock()) {
                sfxKill3DSound(pSprite, -1, -1);
                sfxStart3DSound(pSprite, nSound, nChannel, 0);
                dudeExtra.setSoundOnce(soundonce);
                dudeExtra.setClock(gFrameClock + 120);
            }
        } else {
            sfxStart3DSound(pSprite, nSound, nChannel, 2);
        }
    }

    protected static void aiThinkTarget(BloodSprite pSprite, XSPRITE pXPad) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax)) {
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        }
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        if (!Chance(pDudeInfo.alertChance / 2)) {
            return;
        }

        XSPRITE pXSprite = pSprite.getXSprite();
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            PLAYER pPlayer = gPlayer[i];

            // skip this player if the he owns the dude or is invisible
            if (pSprite.getOwner() == pPlayer.nSprite || pPlayer.pXsprite.getHealth() == 0 || powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                continue;
            }

            int x = pPlayer.pSprite.getX();
            int y = pPlayer.pSprite.getY();
            int z = pPlayer.pSprite.getZ();
            short nSector = pPlayer.pSprite.getSectnum();

            int dx = x - pSprite.getX();
            int dy = y - pSprite.getY();

            long dist = EngineUtils.qdist(dx, dy);

            if (dist <= pDudeInfo.seeDist || dist <= pDudeInfo.hearDist) {
                int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

                // is there a line of sight to the player?
                if (engine.cansee(x, y, z, nSector, pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                    int nAngle = EngineUtils.getAngle(dx, dy);
                    int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;

                    // is the player visible?
                    if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                        aiSetTarget(pXSprite, pPlayer.nSprite);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }

                    // we may want to make hearing a function of sensitivity, rather than distance
                    if (dist < pDudeInfo.hearDist) {
                        aiSetTarget(pXSprite, x, y, z);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }
                }
            }
        }
    }

    protected static void aiThinkTarget2(BloodSprite pSprite, XSPRITE pXPad) {
        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax)) {
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        }
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        if (!Chance(pDudeInfo.alertChance / 2)) {
            return;
        }

        XSPRITE pXSprite = pSprite.getXSprite();
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            PLAYER pPlayer = gPlayer[i];

            // skip this player if the he owns the dude or is invisible
            if (pSprite.getOwner() == pPlayer.nSprite || pPlayer.pXsprite.getHealth() == 0 || powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0) {
                continue;
            }

            int x = pPlayer.pSprite.getX();
            int y = pPlayer.pSprite.getY();
            int z = pPlayer.pSprite.getZ();
            short nSector = pPlayer.pSprite.getSectnum();

            int dx = x - pSprite.getX();
            int dy = y - pSprite.getY();

            long dist = EngineUtils.qdist(dx, dy);

            if (dist <= pDudeInfo.seeDist || dist <= pDudeInfo.hearDist) {
                int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

                // is there a line of sight to the player?
                if (engine.cansee(x, y, z, nSector, pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                    int nAngle = EngineUtils.getAngle(dx, dy);
                    int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;

                    // is the player visible?
                    if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                        aiSetTarget(pXSprite, pPlayer.nSprite);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }

                    // we may want to make hearing a function of sensitivity, rather than distance
                    if (dist < pDudeInfo.hearDist) {
                        aiSetTarget(pXSprite, x, y, z);
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }
                }
            }
        }

        if (pXSprite.getState() != 0) {
            for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
                Sprite pTarget = node.get();
                if (pTarget.getLotag() == kDudeInnocent) {
                    int dx = pTarget.getX() - pSprite.getX();
                    int dy = pTarget.getY() - pSprite.getY();
                    long dist = EngineUtils.qdist(dx, dy);
                    if (dist <= dudeInfo[45].seeDist || dist <= dudeInfo[45].hearDist) {
                        aiSetTarget(pXSprite, pTarget.getXvel());
                        aiActivateDude(pSprite, pXSprite);
                        return;
                    }
                }
            }
        }
    }

    protected static int aiTickHandler(Sprite pSprite, XSPRITE pXSprite) {
        int ticks = pXSprite.getAIState().ticks;
        if (IsOriginalDemo()) {
            if (pSprite.getLotag() == kDudeEel && pXSprite.getAIState() == boneelRecoil) //Bone Eel original bug
            {
                ticks = 0;
            }
            if (pSprite.getLotag() == kDudeTheBeast && pXSprite.getAIState() == beastHack[WATER]) //The Beast original bug
            {
                ticks = 0;
            }
        }

        return ticks;
    }

    public static void aiProcessDudes() {
        // process active sprites
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            BloodSprite pSprite = (BloodSprite) node.get();

            if ((pSprite.getHitag() & kAttrFree) != 0 || !IsDudeSprite(pSprite)) {
                continue;
            }

            final int nXSprite = pSprite.getExtra();
            XSPRITE pXSprite = boardService.getXSprite(nXSprite);
            final DudeExtra xDude = pXSprite.getDudeExtra();
            DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

            // don't manipulate players or dead guys
            if (IsPlayerSprite(pSprite) || (pXSprite.getHealth() == 0)) {
                continue;
            }

            pXSprite.setStateTimer(ClipLow(pXSprite.getStateTimer() - kFrameTicks, 0));
            if (pXSprite.getAIState() != null && pXSprite.getAIState().move) {
                pXSprite.getAIState().move(pSprite, pXSprite);
            }
            if (pXSprite.getAIState() != null && pXSprite.getAIState().think && (gFrame & kAIThinkMask) == (nSprite & kAIThinkMask)) {
                pXSprite.getAIState().think(pSprite, pXSprite);
            }
            if (pXSprite.getAIState() != null && pXSprite.getStateTimer() == 0 && pXSprite.getAIState().next != null) {
                if (pXSprite.getAIState() != null && aiTickHandler(pSprite, pXSprite) > 0) {
                    aiNewState(pSprite, pXSprite, pXSprite.getAIState().next);
                } else if (seqFrame(SS_SPRITE, nXSprite) < 0) {
                    aiNewState(pSprite, pXSprite, pXSprite.getAIState().next);
                }
            }
            // process dudes for recoil
            if (pXSprite.getHealth() != 0 && xDude.getCumulDamage() >= pDudeInfo.hinderDamage << 4) {
                pXSprite.setData3(xDude.getCumulDamage());
                RecoilDude(pSprite);
            }
        }

        // reset the cumulative damages for the next frame
        for (ListNode<XSPRITE> node = boardService.getXSpriteNode(); node != null; node = node.getNext()) {
            node.get().getDudeExtra().setCumulDamage(0);
        }
    }

    /*******************************************************************************
     FUNCTION:		aiInit()

     DESCRIPTION:

     PARAMETERS:		void

     RETURNS:		void

     NOTES:
     *******************************************************************************/
    public static void aiInit(boolean isOriginal) {
        genIdle = new AISTATE(Type.idle, kSeqDudeIdle, null, 0, false, false, false, null);
        genRecoil = new AISTATE(Type.recoil, kSeqDudeRecoil, null, 20, false, false, false, genIdle);

        for (ListNode<XSPRITE> node = boardService.getXSpriteNode(); node != null; node = node.getNext()) {
            node.get().getDudeExtra().clear();
        }

        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            BloodSprite pDude = (BloodSprite) node.get();
            aiInit(pDude, isOriginal);
        }
    }

    public static void aiInit(BloodSprite pDude, boolean isOriginal) {
        XSPRITE pXDude = boardService.getXSprite(pDude.getExtra());
        DudeExtra dudeExtra = pXDude.getDudeExtra();

        switch (pDude.getLotag()) {
            case kGDXDudeUniversalCultist:
                dudeExtra.setActive(false);
                aiNewState(pDude, pXDude, GDXGenDudeIdle[LAND]);
                break;
            case kDudeTommyCultist:
            case kDudeShotgunCultist:
            case kDudeTeslaCultist:
            case kDudeDynamiteCultist:
            case kDudeBeastCultist:
                dudeExtra.setActive(false);
                aiNewState(pDude, pXDude, cultistIdle[LAND]);
                break;

            case kDudeFanaticProne:
                dudeExtra.setActive(false);
                aiNewState(pDude, pXDude, cultistSProne);
                break;

            case kDudeCultistProne:
                dudeExtra.setActive(false);
                aiNewState(pDude, pXDude, cultistTProne);
                break;

            case kDudeButcher:
                dudeExtra.setThinkTime(0);
                aiNewState(pDude, pXDude, zombieFIdle);
                break;

            case kDudeAxeZombie:
                dudeExtra.setThinkTime(0);
                aiNewState(pDude, pXDude, zombieAIdle);
                break;

            case kDudeSleepZombie:
                dudeExtra.setThinkTime(0);
                aiNewState(pDude, pXDude, zombieSLIdle);
                break;

            case kDudeEarthZombie:
                dudeExtra.setThinkTime(0);
                aiNewState(pDude, pXDude, zombieEIdle);
                break;

            case kDudeFleshGargoyle:
            case kDudeStoneGargoyle:
                dudeExtra.setActive(false);
                dudeExtra.setThinkTime(0);
                aiNewState(pDude, pXDude, gargoyleIdle);
                break;

            case kDudeFleshStatue:
            case kDudeStoneStatue:
                aiNewState(pDude, pXDude, statueIdle);
                break;
            case kDudeCerberus:
                dudeExtra.setThinkTime(0);
                aiNewState(pDude, pXDude, cerberusIdle[C1]);
                break;

            case kDudeCerberus2:
                dudeExtra.setThinkTime(0);
                aiNewState(pDude, pXDude, cerberusIdle[C2]);
                break;

            case kDudeHound:
                aiNewState(pDude, pXDude, houndIdle);
                break;

            case kDudeHand:
                aiNewState(pDude, pXDude, handIdle);
                break;

            case kDudePhantasm:
                dudeExtra.setThinkTime(0);
                dudeExtra.setActive(false);
                aiNewState(pDude, pXDude, ghostIdle);
                break;

            case kDudeInnocent:
                aiNewState(pDude, pXDude, innocentIdle);
                break;

            case kDudeRat:
                aiNewState(pDude, pXDude, ratIdle);
                break;

            case kDudeEel:
                dudeExtra.setThinkTime(0);
                dudeExtra.setActive(false);
                aiNewState(pDude, pXDude, boneelIdle);
                break;

            case kDudeGillBeast:
                Sector pSector = boardService.getSector(pDude.getSectnum());
                if (isOriginal) {
                    aiNewState(pDude, pXDude, gillBeastIdle[LAND]);
                    break;
                }

                if (pSector.getExtra() >= 0 && xsector[pSector.getExtra()].Underwater) {
                    aiNewState(pDude, pXDude, gillBeastIdle[WATER]);
                } else {
                    aiNewState(pDude, pXDude, gillBeastIdle[LAND]);
                }
                break;

            case kDudeBat:
                dudeExtra.setThinkTime(0);
                dudeExtra.setActive(false);
                aiNewState(pDude, pXDude, batSleep);
                break;

            case kDudeBrownSpider:
            case kDudeRedSpider:
            case kDudeBlackSpider:
            case kDudeMotherSpider:
                dudeExtra.setThinkTime(0);
                dudeExtra.setActive(false);
                aiNewState(pDude, pXDude, spidIdle);
                break;

            case kDudeTchernobog:
                dudeExtra.setThinkTime(0);
                aiNewState(pDude, pXDude, tchernobogIdle);
                break;
            case kDudeTinyCaleb:
                aiNewState(pDude, pXDude, calebIdle[LAND]);
                break;
            case kDudeTheBeast:
                aiNewState(pDude, pXDude, beastIdle[LAND]);
                break;
            case kDudeGreenPod:
            case kDudeFirePod:
                aiNewState(pDude, pXDude, podIdle);
                break;
            case kDudeGreenTentacle:
            case kDudeFireTentacle:
                aiNewState(pDude, pXDude, tentacleIdle);
                break;
            case kDudeBurning:
            case kDudeCultistBurning:
                aiNewState(pDude, pXDude, burnGoto[CULTIST]);
                pXDude.setBurnTime(1200);
                break;
            case kGDXGenDudeBurning:
                aiNewState(pDude, pXDude, burnGoto[GDXGENDUDE]);
                pXDude.setBurnTime(1200);
                break;
            case kDudeAxeZombieBurning:
                aiNewState(pDude, pXDude, burnGoto[ZOMBIE]);
                pXDude.setBurnTime(1200);
                break;
            case kDudeBloatedButcherBurning:
                aiNewState(pDude, pXDude, burnGoto[BUTCHER]);
                pXDude.setBurnTime(1200);
                break;
            case kDudeTinyCalebburning:
                aiNewState(pDude, pXDude, burnGoto[TINYCALEB]);
                pXDude.setBurnTime(1200);
                break;
            case kDudeTheBeastburning:
                aiNewState(pDude, pXDude, burnGoto[BEAST]);
                pXDude.setBurnTime(1200);
                break;

            default:
                aiNewState(pDude, pXDude, genIdle);
                break;
        }

        aiSetTarget(pXDude, 0, 0, 0);

        pXDude.setStateTimer(0);

        switch (pDude.getLotag()) {
            case kDudeEarthZombie:
            case kDudeSleepZombie:
                pDude.setHitag((kAttrMove | kAttrGravity | kAttrFalling));
                break;

            case kDudeMotherSpider:
            case kDudeBrownSpider:
            case kDudeRedSpider:
            case kDudeBlackSpider:
                if (pDude.getLotag() == kDudeMotherSpider && isOriginal) {
                    pDude.setHitag((kAttrMove | kAttrGravity | kAttrFalling | kAttrAiming));
                    break;
                }

                if ((pDude.getCstat() & kSpriteFlipY) != 0) {
                    pDude.setHitag((kAttrMove | kAttrAiming));
                } else {
                    pDude.setHitag((kAttrMove | kAttrGravity | kAttrFalling | kAttrAiming));
                }
                break;


            case kDudeEel:
            case kDudeBat:
            case kDudeFleshGargoyle:
            case kDudeStoneGargoyle:
            case kDudePhantasm:
                pDude.setHitag((kAttrMove | kAttrAiming));
                break;
            case kDudeGillBeast:
                XSECTOR pXSector = boardService.getSector(pDude.getSectnum()).getExtra() > 0 ? xsector[boardService.getSector(pDude.getSectnum()).getExtra()] : null;
                if (pXSector != null && pXSector.Underwater) {
                    pDude.setHitag((kAttrMove | kAttrAiming));
                } else {
                    pDude.setHitag((kAttrMove | kAttrGravity | kAttrFalling | kAttrAiming));
                }
                break;

            case kDudeMotherPod: // Fake dude type
                break;
            // These will now have no gravity, so it's possible to flipY
            // and place on ceilings if hitag = 1.
            case kDudeGreenPod:
            case kDudeFirePod:
            case kDudeGreenTentacle:
            case kDudeFireTentacle:
            case kDudeMotherTentacle:
                if ((pDude.getCstat() & kSpriteFlipY) != 0) {
                    pDude.setHitag(kAttrAiming);
                    break;
                }

                // go default
            default:
                pDude.setHitag((kAttrMove | kAttrGravity | kAttrFalling | kAttrAiming));
                break;
        }
    }

    public static void UpdateEnemyAim(Sprite pSprite, int nXSprite, WeaponAim pAimData) {
        if (!IsDudeSprite(pSprite)) {
            return;
        }

        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

        int x = pSprite.getX();
        int y = pSprite.getY();
        DudeExtra pXDude = boardService.getXSprite(nXSprite).getDudeExtra();

        EnemyAim.x = Cos(pSprite.getAng()) >> 16;
        EnemyAim.y = Sin(pSprite.getAng()) >> 16;
        EnemyAim.z = pXDude.getDudeSlope();

        int closest = 0x7FFFFFFF;
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            final int nDude = node.getIndex();
            final BloodSprite pDude = (BloodSprite) node.get();

            // don't target yourself!
            if (pDude == pSprite) {
                continue;
            }

            if ((pDude.getHitag() & kAttrAiming) == 0) {
                continue;
            }

            int tx, ty, tz;

            tx = pDude.getX();
            ty = pDude.getY();
            tz = pDude.getZ();

            int dist = EngineUtils.qdist(tx - x, ty - y);

            if (dist == 0 || dist > 10240) {
                continue;
            }

            if (pAimData.kSeeker != 0) {
                int k = (dist << 12) / pAimData.kSeeker;
                tx += (int) (k * pDude.getVelocityX() >> 12);
                ty += (int) (k * pDude.getVelocityY() >> 12);
                tz += (int) (k * pDude.getVelocityZ() >> 8);
            }

            int z1 = mulscale(dist, pXDude.getDudeSlope(), 10) + eyeAboveZ;
            int z2 = mulscale(kAimMaxSlope, dist, 10);

            GetSpriteExtents(pDude);

            if ((z1 - z2 > extents_zBot) || (z1 + z2 < extents_zTop)) {
                continue;
            }

            int dx = mulscale(dist, Cos(pSprite.getAng()), 30) + x;
            int dy = mulscale(dist, Sin(pSprite.getAng()), 30) + y;

            int dist2 = EngineUtils.sqrt(((dx - tx) >> 4) * ((dx - tx) >> 4) + ((dy - ty) >> 4) * ((dy - ty) >> 4) + ((z1 - tz) >> 8) * ((z1 - tz) >> 8));

            if (dist2 < closest) {
                int ang = EngineUtils.getAngle(tx - x, ty - y);
                if (klabs(((ang - pSprite.getAng() + kAngle180) & kAngleMask) - kAngle180) > pAimData.kDudeAngle) {
                    continue;
                }

                int dz = pDude.getZ() - pSprite.getZ();

                if (engine.cansee(x, y, eyeAboveZ, pSprite.getSectnum(), tx, ty, tz, pDude.getSectnum())) {
                    closest = dist2;

                    EnemyAim.x = Cos(ang) >> 16;
                    EnemyAim.y = Sin(ang) >> 16;

                    if ((dz < -819 && dz > -2867) || (dz < -2867 && dz > -12288)) {
                        EnemyAim.z = divscale(dz, dist, 10) + 9460;
                        continue;
                    }
                    if (dz < -12288) {
                        EnemyAim.z = divscale(dz, dist, 10) - 7500;
                        continue;
                    }
//					EnemyAim.z = (int) divscale(dz, dist, 10);
                }
                EnemyAim.z = divscale(dz, dist, 10); //Stone gargoyle fix
            }
        }
    }
}




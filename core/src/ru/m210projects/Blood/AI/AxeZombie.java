package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;

import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.checkEventList;
import static ru.m210projects.Blood.EVENT.evPostCallback;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Gib.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.levelAddKills;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.DamageDude;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.*;
import static ru.m210projects.Build.Pragmas.klabs;

public class AxeZombie extends GeneticDude {

    private static final int kAxeZombieMeleeDist = M2X(2.0);

    private final Runnable think = () -> aiThinkTarget();
    private final Runnable move = () -> aiMoveForward();

    private final AiState zombieAIdle = new AiState(kSeqDudeIdle, null, 0, () -> pXSprite.setTarget(-1), null, think, null);

    private final AiState zombieEIdle = new AiState(12, null, 0, null, null, think, null);
    private final Runnable entryEZombie = () -> {
        pSprite.setHitag(pSprite.getHitag() | kAttrMove);
        pSprite.setLotag(kDudeAxeZombie);
    };    private final AiState zombieAPonder = new AiState(kSeqDudeIdle, null, 0, null, () -> aiMoveTurn(), () -> thinkPonder(), null);
    private final AiState zombieSLIdle = new AiState(10, null, 0, null, null, think, null);    private final AiState zombieARecoil = new AiState(kSeqDudeRecoil, null, 0, null, null, null, zombieAPonder);

    public AxeZombie(BloodSprite pSprite) {
        super(pSprite);

        System.err.println("Zombie " + pSprite.getXvel());

        pSprite.setHitag((kAttrMove | kAttrGravity | kAttrFalling));
        switch (pSprite.getLotag()) {
            case kDudeSleepZombie:
                aiNewState(zombieSLIdle);
                break;
            case kDudeEarthZombie:
                aiNewState(zombieEIdle);
                break;
            case kDudeAxeZombie:
                aiNewState(zombieAIdle);
                pSprite.setHitag(pSprite.getHitag() | kAttrAiming);
                break;
            case kDudeAxeZombieBurning:
//			aiNewState(burnGoto[ZOMBIE]); XXX
                pSprite.setHitag(pSprite.getHitag() | kAttrAiming);
                pXSprite.setBurnTime(1200);
                break;
        }

        setTarget(0, 0, 0);

        pXSprite.setStateTimer(0);
    }    private final AiState zombieARTesla = new AiState(kSeqDudeTesla, null, 0, null, null, null, zombieAPonder);

    @Override
    public void activate() {
        if (pXSprite.getState() == 0) {
            // this doesn't take into account sprites triggered w/o a target location....
            aiChooseDirection(EngineUtils.getAngle(pXSprite.getTargetX() - pSprite.getX(), pXSprite.getTargetY() - pSprite.getY()));
            pXSprite.setState(1);
        }

        switch (pSprite.getLotag()) {
            case kDudeSleepZombie:
                if (aiState == zombieSLIdle) aiNewState(zombieSLUP);
                break;
            case kDudeEarthZombie:
                if (aiState == zombieEIdle) aiNewState(zombieEUp);
                break;
            case kDudeAxeZombie:
                if (pXSprite.getTarget() == -1) aiNewState(zombieASearch);
                else {
                    if (!Chance(0x5000)) aiNewState(zombieAChase);
                    else {
                        aiPlaySound(Random(3) + 1103, 1, -1);
                        aiNewState(zombieAChase);
                    }
                }
                break;
            case kDudeAxeZombieBurning:
//			if (pXSprite.target == -1)  XXX
//				aiNewState(burnSearch[ZOMBIE]);
//			else
//				aiNewState(burnChase[ZOMBIE]);
//			break;
        }
    }    private final AiState zombieAUp = new AiState(11, null, 0, null, null, null, zombieAPonder);

    @Override
    public int damage(int nSource, int nDamageType, int nDamage) {
        if (pXSprite.getHealth() == 0) return 0;

        pXSprite.setHealth(ClipLow(pXSprite.getHealth() - nDamage, 0));
        cumulDamage += nDamage;    // add to cumulative damage

        if (nSource >= 0) {
            if (nSource == pXSprite.getReference()) return 0;

            if (pXSprite.getTarget() == -1) {
                // give a dude a target
                setTarget(nSource);
                activate();
            } else if (nSource != pXSprite.getTarget()) {
                // retarget
                int nThresh = nDamage;

                if (boardService.getSprite(nSource).getLotag() == pSprite.getLotag())
                    nThresh *= pDudeInfo.changeTargetKin;
                else nThresh *= pDudeInfo.changeTarget;

                if (Chance(nThresh / 2)) {
                    setTarget(nSource);
                    activate();
                }
            }

            if (nDamageType == 6) aiTeslaHit = 1;
            else {
                if (!IsOriginalDemo()) aiTeslaHit = 0;
            }

            // you DO need special processing here or somewhere else (your choice) for dodging
            switch (pSprite.getLotag()) {
                case kDudeEarthZombie:
                case kDudeAxeZombie:
                    if (nDamageType == kDamageBurn && pXSprite.getHealth() <= pDudeInfo.fleeHealth) {
                        aiPlaySound(361, 0, -1);
                        aiPlaySound(1106, 2, -1);
                        pSprite.setLotag(kDudeAxeZombieBurning);
//				    	aiNewState(burnGoto[ZOMBIE]); XXX
                        if (!IsOriginalDemo() && pXSprite.getBurnTime() == 0) pXSprite.setBurnTime(1200);
                        heal(dudeInfo[41].startHealth, dudeInfo[41].startHealth);
                        checkEventList(pXSprite.getReference(), SS_SPRITE, 0);
                    }
                    break;
            }
        }

        return nDamage;
    }    private final AiState zombieAFall = new AiState(kSeqDudeDeath1, null, 360, null, null, null, zombieAUp);

    @Override
    public void kill(int nSource, int nDamageType, int nDamage) {
        pSprite.setHitag(pSprite.getHitag() | (kAttrMove | kAttrGravity | kAttrFalling));

        if (pXSprite.getKey() > 0) DropPickupObject(pSprite, kItemKey1 + pXSprite.getKey() - 1);
        if (pXSprite.getDropMsg() != 0) {
            DropPickupObject(pSprite, pXSprite.getDropMsg());
            if (!IsOriginalDemo()) pXSprite.setDropMsg(0);
        }

        int deathType = kSeqDudeDeath1;
        switch (nDamageType) {
            case kDamageBurn:
                deathType = kSeqDudeDeath3;
                sfxStart3DSound(pSprite, 351, -1, 0);
                break;
            case kDamageExplode:
                deathType = kSeqDudeDeath2;
                break;
            case kDamageSpirit:
                switch (pSprite.getLotag()) {
                    case kDudeAxeZombie:
                    case kDudeEarthZombie:
                        deathType = 14;
                        break;
                }
                break;
        }

        final int nXSprite = pSprite.getExtra();

        // are we missing this sequence? if so, just delete it
        if (!game.getCache().contains(pDudeInfo.seqStartID + deathType, "SEQ")) {
            levelAddKills(pSprite);

            seqKill(SS_SPRITE, nXSprite); // make sure we remove any active sequence
            actPostSprite(pSprite.getXvel(), kStatFree);
            return;
        }

        switch (pSprite.getLotag()) {
            case kDudeAxeZombie:
                sfxStart3DSound(pSprite, Random(2) + 1107, -1, 0);
                if (deathType == 2) {
                    seqSpawn(pDudeInfo.seqStartID + 2, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    GetSpriteExtents(pSprite);
                    startPos.set(pSprite.getX(), pSprite.getY(), extents_zTop);
                    startVel.set(pSprite.getVelocityX() >> 1, pSprite.getVelocityY() >> 1, -838860);
                    actGenerateGibs(pSprite, 27, startPos, startVel);
                } else if (deathType != 1 || !Chance(0x2000)) {
                    if (deathType == 14) seqSpawn(pDudeInfo.seqStartID + 14, SS_SPRITE, nXSprite, null);
                    else if (deathType == 3)
                        seqSpawn(pDudeInfo.seqStartID + 13, SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                    else {
                        seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    }
                } else {
                    seqSpawn(pDudeInfo.seqStartID + 7, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 5);
                    pXSprite.setData1(35);
                    pXSprite.setData2(5);
                    GetSpriteExtents(pSprite);
                    startPos.set(pSprite.getX(), pSprite.getY(), extents_zTop);
                    startVel.set(pSprite.getVelocityX() >> 1, pSprite.getVelocityY() >> 1, -1118481);
                    actGenerateGibs(pSprite, 27, startPos, startVel);
                    sfxStart3DSound(pSprite, 362, -1, 0);
                }
                break;
            case kDudeAxeZombieBurning:
                if (Chance(0x4000) && deathType == kSeqDudeDeath3) sfxStart3DSound(pSprite, 1109, -1, 0);
                else sfxStart3DSound(pSprite, Random(2) + 1107, -1, 0);
                nDamageType = kDamageExplode;
                if (Chance(0x4000)) {
                    seqSpawn(13 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    GetSpriteExtents(pSprite);
                    startPos.set(pSprite.getX(), pSprite.getY(), extents_zTop);
                    startVel.set(pSprite.getVelocityX() >> 1, pSprite.getVelocityY() >> 1, -838860);
                    actGenerateGibs(pSprite, 27, startPos, startVel);
                } else seqSpawn(13 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                break;

            default:
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;
        }

        if (nDamageType == kDamageExplode) {
            for (int i = 0; i < 3; i++) {
                int nGibType = pDudeInfo.nGibType[i];
                if (nGibType > -1) actGenerateGibs(pSprite, nGibType, null, null);

            }
            for (int j = 0; j < 4; ++j)
                actSpawnBlood(pSprite);
        }

        levelAddKills(pSprite);
        actCheckRespawn(pSprite);

        if (!IsOriginalDemo()) {
            if (boardService.getXSprite(nXSprite).isProximity()) boardService.getXSprite(nXSprite).setProximity(false);
        }

        pSprite.setLotag(kThingGib);
        actPostSprite(pSprite.getXvel(), kStatThing);
    }    private final AiState zombieAHack = new AiState(kSeqDudeAttack, nXSprite -> HackCallback(), 80, null, null, null, zombieAPonder);

    @Override
    public void warp(int type) {
        switch (type) {
            case kMarkerUpperWater:
            case kMarkerUpperGoo:
                if (pSprite.getLotag() == kDudeAxeZombie) {
                    pXSprite.setBurnTime(0);
                    evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 11);
                    sfxStart3DSound(pSprite, 720, -1, 0);
                    aiNewState(zombieAGoto);
                }
                break;
        }
    }    private final AiState zombieAChase = new AiState(kSeqDudeWalk, null, 0, null, move, () -> thinkChase(), null);

    @Override
    protected void recoil(boolean chance) {
        switch (pSprite.getLotag()) {
            case kDudeAxeZombie:
            case kDudeEarthZombie:
                aiPlaySound(1106, 2, -1); //AZOMPAIN
                if (aiTeslaHit != 0 && pXSprite.getData3() > pDudeInfo.startHealth / 3) aiNewState(zombieARTesla);
                else if (pXSprite.getData3() <= pDudeInfo.startHealth / 3) aiNewState(zombieARecoil);
                else aiNewState(zombieAFall);
                break;

            case kDudeAxeZombieBurning:
                aiPlaySound(1106, 2, -1);
//				aiNewState(burnGoto[ZOMBIE]); XXX
                break;
        }

        aiTeslaHit = 0;
    }    private final AiState zombieAGoto = new AiState(kSeqDudeWalk, null, 1800, null, move, () -> thinkGoto(), zombieAIdle);

    @Override
    public void touch() { /* nothing */ }    private final AiState zombieASearch = new AiState(kSeqDudeWalk, null, 1800, null, move, () -> thinkSearch(), zombieAIdle);

    private void thinkPonder() {
        if (pXSprite.getTarget() == -1) {
            aiNewState(zombieASearch);
            return;
        }

        if (!boardService.isValidSprite(pXSprite.getTarget()))
            throw new AssertException("isValidSprite(pXSprite.getTarget())");

        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        int dx = pTarget.getX() - pSprite.getX();
        int dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(zombieASearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0 || powerupCheck(pPlayer, kItemDeathMask - kItemBase) > 0) {
                aiNewState(zombieAGoto);
                return;
            }
        }

        long dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    setTarget(pXSprite.getTarget());

                    // check to see if we can attack
                    if (dist < kAxeZombieMeleeDist) {
                        if (klabs(losAngle) < kAngle15) {
                            aiNewState(zombieAHack);
                        }
                        return;
                    }
                }
            }
        }
        aiNewState(zombieAChase);
    }

    private void HackCallback() {
        int nAngle = EngineUtils.getAngle(pXSprite.getTargetX() - pSprite.getX(), pXSprite.getTargetY() - pSprite.getY());

        int nZOffset1 = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;
        int nZOffset2 = 0;
        if (pXSprite.getTarget() != -1) {
            Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
            if (IsDudeSprite(pTarget))
                nZOffset2 = dudeInfo[pTarget.getLotag() - kDudeBase].eyeHeight * pTarget.getYrepeat() << 2;
        }
        int dx = Cos(nAngle) >> 16;
        int dy = Sin(nAngle) >> 16;
        int dz = nZOffset1 - nZOffset2;

        sfxStart3DSound(pSprite, 1101, 1, 0); //kSfxAxeAir

        actFireVector(pSprite, 0, 0, dx, dy, dz, kVectorAxe);
    }    private final AiState zombieEUp2 = new AiState(kSeqDudeIdle, null, 1, entryEZombie, null, null, zombieASearch);

    /****************************************************************************
     ** thinkSearch()
     **
     **
     ****************************************************************************/
    private void thinkSearch() {
        aiChooseDirection(pXSprite.getGoalAng());
        if (aiThinkTarget()) return;

        if (pXSprite.getState() == 0) return;

        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            Sprite pTarget = boardService.getSprite(nSprite);
            if (pTarget.getLotag() == kDudeInnocent) {
                int dx = pTarget.getX() - pSprite.getX();
                int dy = pTarget.getY() - pSprite.getY();
                long dist = EngineUtils.qdist(dx, dy);
                if (dist <= dudeInfo[45].seeDist || dist <= dudeInfo[45].hearDist) {
                    setTarget(pTarget.getXvel());
                    activate();
                    return;
                }
            }
        }
    }    private final AiState zombieEUp = new AiState(9, null, 180, () -> {
        sfxStart3DSound(pSprite, 1100, -1, 0);
        pSprite.setAng(EngineUtils.getAngle(pXSprite.getTargetX() - pSprite.getX(), pXSprite.getTargetY() - pSprite.getY()));
    }, null, null, zombieEUp2);

    /****************************************************************************
     ** thinkGoto()
     **
     **
     ****************************************************************************/
    private void thinkGoto() {
        int dx = pXSprite.getTargetX() - pSprite.getX();
        int dy = pXSprite.getTargetY() - pSprite.getY();

        int nAngle = EngineUtils.getAngle(dx, dy);
        long dist = EngineUtils.qdist(dx, dy);

        aiChooseDirection(nAngle);

        // if reached target, change to search mode
        if (dist < M2X(1.8) && klabs(pSprite.getAng() - nAngle) < pDudeInfo.periphery) {
//			System.out.println("Axe zombie " +  getXSprite()[pSprite.extra].reference + " switching to search mode");
            aiNewState(zombieASearch);
        }

        aiThinkTarget();
    }

    /****************************************************************************
     ** thinkChase()
     **
     **
     ****************************************************************************/
    private void thinkChase() {
        if (pXSprite.getTarget() == -1) {
            aiNewState(zombieASearch);
            return;
        }

        if (!boardService.isValidSprite(pXSprite.getTarget()))
            throw new AssertException("isValidSprite(pXSprite.getTarget())");

        Sprite pTarget = boardService.getSprite(pXSprite.getTarget());
        XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());

        // check target
        int dx = pTarget.getX() - pSprite.getX();
        int dy = pTarget.getY() - pSprite.getY();

        aiChooseDirection(EngineUtils.getAngle(dx, dy));

        if (pXTarget == null || pXTarget.getHealth() == 0) {
            // target is dead
            aiNewState(zombieASearch);
            return;
        }

        if (IsPlayerSprite(pTarget)) {
            PLAYER pPlayer = gPlayer[pTarget.getLotag() - kDudePlayer1];
            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0 || powerupCheck(pPlayer, kItemDeathMask - kItemBase) > 0) {
                aiNewState(zombieAGoto);
                return;
            }
        }

        long dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.seeDist) {
            int nAngle = EngineUtils.getAngle(dx, dy);
            int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;
            int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

            // is there a line of sight to the target?
            if (engine.cansee(pTarget.getX(), pTarget.getY(), pTarget.getZ(), pTarget.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                // is the target visible?
                if (klabs(losAngle) <= pDudeInfo.periphery) {
                    setTarget(pXSprite.getTarget());

                    // check to see if we can attack
                    if (dist < kAxeZombieMeleeDist && klabs(losAngle) < kAngle15) {
                        aiNewState(zombieAHack);
                    }
                    return;
                }
            }
        }
        //System.out.println("Axe zombie " + pXSprite.reference + " lost sight of target " + pXSprite.target);
        aiNewState(zombieAGoto);
        pXSprite.setTarget(-1);
    }    private final AiState zombieSLUP = new AiState(11, null, 0, entryEZombie, null, null, zombieAPonder);

























}

package ru.m210projects.Blood.AI;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DudeExtra {

    private int cumulDamage = 0;    // cumulative damage per frame
    private int dudeSlope = 0;

    private boolean aiTeslaHit;
    private int aiThinkTime;
    private boolean aiActive;
    private int aiClock;
    private int aiSoundOnce;

    public int getCumulDamage() {
        return cumulDamage;
    }

    public void setCumulDamage(int cumulDamage) {
        this.cumulDamage = cumulDamage;
    }

    public int getDudeSlope() {
        return dudeSlope;
    }

    public void setDudeSlope(int dudeSlope) {
        this.dudeSlope = dudeSlope;
    }

    public boolean isTeslaHit() {
        return aiTeslaHit;
    }

    public void setTeslaHit(boolean aiTeslaHit) {
        this.aiTeslaHit = aiTeslaHit;
    }

    public int getThinkTime() {
        return aiThinkTime;
    }

    public void setThinkTime(int aiThinkTime) {
        this.aiThinkTime = aiThinkTime;
    }

    public void setActive(boolean active) {
        this.aiActive = active;
    }

    public boolean isActive() {
        return aiActive;
    }

    public void setAiActive(boolean aiActive) {
        this.aiActive = aiActive;
    }

    public int getClock() {
        return aiClock;
    }

    public void setClock(int aiClock) {
        this.aiClock = aiClock;
    }

    public int getSoundOnce() {
        return aiSoundOnce;
    }

    public void setSoundOnce(int aiSoundOnce) {
        this.aiSoundOnce = aiSoundOnce;
    }

    public void clear() {
        cumulDamage = 0;
        dudeSlope = 0;
        aiTeslaHit = false;
        aiThinkTime = 0;
        aiActive = false;
        aiClock = 0;
        aiSoundOnce = 0;
    }

    public void copy(DudeExtra src) {
        cumulDamage = src.cumulDamage;
        dudeSlope = src.dudeSlope;
        aiTeslaHit = src.aiTeslaHit;
        aiThinkTime = src.aiThinkTime;
        aiActive = src.aiActive;
        aiClock = src.aiClock;
        aiSoundOnce = src.aiSoundOnce;
    }

    public void writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, cumulDamage);
        StreamUtils.writeInt(os, dudeSlope);
    }

    public void readObject(InputStream is) throws IOException {
        cumulDamage = StreamUtils.readInt(is);
        dudeSlope = StreamUtils.readInt(is);
    }
}

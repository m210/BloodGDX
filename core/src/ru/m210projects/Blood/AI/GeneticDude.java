package ru.m210projects.Blood.AI;

import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.DudeInfo;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sprite;

import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.SOUND.sfxKill3DSound;
import static ru.m210projects.Blood.SOUND.sfxStart3DSound;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqFrame;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqSpawn;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Build.Pragmas.*;

public abstract class GeneticDude {

    private static final int kAIThinkMask = 3;

    /*
     * aiMoveForward if(pSprite.lotag == kDudeBurning)
     * aiProcessDudes aiTickHandler
     */
    protected BloodSprite pSprite;
    protected XSPRITE pXSprite;
    protected DudeInfo pDudeInfo;
    protected AiState aiState;
    protected int aiClock;
    protected int aiSoundOnce;
    protected int aiTeslaHit;
    protected int cumulDamage; // cumulative damage per frame
    public GeneticDude(BloodSprite pSprite) {
        this.pSprite = pSprite;
        if (!IsDudeSprite(pSprite)) throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");

        this.pXSprite = boardService.getXSprite(pSprite.getExtra());
        this.pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        this.aiClock = 0;
        this.aiSoundOnce = 0;
        this.cumulDamage = 0;
    }

    public abstract void activate();

    public abstract int damage(int nSource, int nDamageType, int nDamage);

    public abstract void kill(int nSource, int nDamageType, int nDamage);

    public abstract void warp(int type);

    public abstract void touch();

    protected abstract void recoil(boolean chance);

    protected boolean aiCanMove(int nTarget, int ang, int dist) {
        int dx = mulscale(dist, Cos(ang), 30);
        int dy = mulscale(dist, Sin(ang), 30);

        int x = pSprite.getX();
        int y = pSprite.getY();
        int z = pSprite.getZ();

        HitScan(pSprite, z, Cos(ang) >> 16, Sin(ang) >> 16, 0, pHitInfo, CLIPMASK0, dist);
        int hitDist = EngineUtils.qdist(x - pHitInfo.hitx, y - pHitInfo.hity);

        if (hitDist - (pSprite.getClipdist() << 2) < dist) {
            // okay to be blocked by target
            return pHitInfo.hitsprite != -1 && pHitInfo.hitsprite == nTarget;
        }

        x += dx;
        y += dy;

        return FindSector(x, y, z, pSprite.getSectnum());
    }

    protected void aiChooseDirection(int ang) {
        int dang = ((kAngle180 + ang - pSprite.getAng()) & kAngleMask) - kAngle180;

        int sin = Sin(pSprite.getAng());
        int cos = Cos(pSprite.getAng());

        // find vel relative to current angle
        long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);

//		int avoidDist = (int) (vel * (kTimerRate / 2 / kFrameTicks) >> 13);

        int avoidDist = (int) (((15 * vel >> 12) - (15 * vel >> 43)) >> 1);

        int turnTo = kAngle60;
        if (dang < 0) turnTo = -turnTo;

        // clear movement toward target?
        if (aiCanMove(pXSprite.getTarget(), pSprite.getAng() + dang, avoidDist))
            pXSprite.setGoalAng((pSprite.getAng() + dang) & kAngleMask);
            // clear movement partially toward target?
        else if (aiCanMove(pXSprite.getTarget(), pSprite.getAng() + dang / 2, avoidDist))
            pXSprite.setGoalAng((pSprite.getAng() + dang / 2) & kAngleMask);
        else if (aiCanMove(pXSprite.getTarget(), pSprite.getAng() - dang / 2, avoidDist))
            pXSprite.setGoalAng((pSprite.getAng() - dang / 2) & kAngleMask);
            // try turning in target direction
        else if (aiCanMove(pXSprite.getTarget(), pSprite.getAng() + turnTo, avoidDist))
            pXSprite.setGoalAng((pSprite.getAng() + turnTo) & kAngleMask);
            // clear movement straight?
        else if (aiCanMove(pXSprite.getTarget(), pSprite.getAng(), avoidDist)) pXSprite.setGoalAng(pSprite.getAng());
            // try turning away
        else if (aiCanMove(pXSprite.getTarget(), pSprite.getAng() - turnTo, avoidDist))
            pXSprite.setGoalAng((pSprite.getAng() - turnTo) & kAngleMask);
        else pXSprite.setGoalAng((pSprite.getAng() + kAngle60) & kAngleMask);

        // choose dodge direction
        pXSprite.setDodgeDir(Chance(0x4000) ? 1 : -1);

        if (!aiCanMove(pXSprite.getTarget(), pSprite.getAng() + kAngle90 * pXSprite.getDodgeDir(), 512)) {
            pXSprite.setDodgeDir(-pXSprite.getDodgeDir());
            if (!aiCanMove(pXSprite.getTarget(), pSprite.getAng() + kAngle90 * pXSprite.getDodgeDir(), 512))
                pXSprite.setDodgeDir(0);
        }
    }

    protected void aiMoveForward() {
        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        // don't move forward if trying to turn around
        if (klabs(dang) > kAngle60) return;

        int sin = Sin(pSprite.getAng());
        int cos = Cos(pSprite.getAng());

        pSprite.addVelocity(mulscale(cos, pDudeInfo.frontSpeed, 30),
        mulscale(sin, pDudeInfo.frontSpeed, 30), 0);
    }

    protected void aiMoveTurn() {
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;

        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));
    }

    protected void aiMoveDodge() {
        int dang = ((kAngle180 + pXSprite.getGoalAng() - pSprite.getAng()) & kAngleMask) - kAngle180;
        int maxTurn = pDudeInfo.angSpeed * kFrameTicks >> 4;
        pSprite.setAng((short) ((pSprite.getAng() + ClipRange(dang, -maxTurn, maxTurn)) & kAngleMask));

        if (pXSprite.getDodgeDir() == 0) return;

        long sin = Sin(pSprite.getAng());
        long cos = Cos(pSprite.getAng());

        // find vel and svel relative to current angle
        long vel = dmulscale(pSprite.getVelocityX(), cos, pSprite.getVelocityY(), sin, 30);
        long svel = dmulscale(pSprite.getVelocityX(), sin, -pSprite.getVelocityY(), cos, 30);

        if (pXSprite.getDodgeDir() > 0) svel += pDudeInfo.sideSpeed;
        else svel -= pDudeInfo.sideSpeed;

        // reconstruct x and y velocities
        pSprite.setVelocityX(dmulscale(vel, cos, svel, sin, 30));
        pSprite.setVelocityY(dmulscale(vel, sin, -svel, cos, 30));
    }

    protected boolean aiThinkTarget() {
        if (!Chance(pDudeInfo.alertChance / 2)) return true;

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            PLAYER pPlayer = gPlayer[i];

            // skip this player if the he owns the dude or is invisible
            if (pSprite.getOwner() == pPlayer.nSprite || pPlayer.pXsprite.getHealth() == 0 || powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) > 0)
                continue;

            int x = pPlayer.pSprite.getX();
            int y = pPlayer.pSprite.getY();
            int z = pPlayer.pSprite.getZ();
            short nSector = pPlayer.pSprite.getSectnum();

            int dx = x - pSprite.getX();
            int dy = y - pSprite.getY();

            long dist = EngineUtils.qdist(dx, dy);

            if (dist <= pDudeInfo.seeDist || dist <= pDudeInfo.hearDist) {
                int eyeAboveZ = pDudeInfo.eyeHeight * pSprite.getYrepeat() << 2;

                // is there a line of sight to the player?
                if (engine.cansee(x, y, z, nSector, pSprite.getX(), pSprite.getY(), pSprite.getZ() - eyeAboveZ, pSprite.getSectnum())) {
                    int nAngle = EngineUtils.getAngle(dx, dy);
                    int losAngle = ((kAngle180 + nAngle - pSprite.getAng()) & kAngleMask) - kAngle180;

                    // is the player visible?
                    if (dist < pDudeInfo.seeDist && klabs(losAngle) <= pDudeInfo.periphery) {
                        setTarget(pPlayer.nSprite);
                        activate();
                        return true;
                    }

                    // we may want to make hearing a function of sensitivity, rather than distance
                    if (dist < pDudeInfo.hearDist) {
                        setTarget(x, y, z);
                        activate();
                        return true;
                    }
                }
            }
        }

        return false;
    }

    protected void aiPlaySound(int nSound, int soundonce, int nChannel) {
        if (soundonce != 0) {
            if (soundonce > aiSoundOnce || gFrameClock >= aiClock) {
                sfxKill3DSound(pSprite, -1, -1);
                sfxStart3DSound(pSprite, nSound, nChannel, 0);
                aiSoundOnce = soundonce;
                aiClock = gFrameClock + 120;
            }
        } else sfxStart3DSound(pSprite, nSound, nChannel, 2);
    }

    protected void aiNewState(AiState pState) {
        pXSprite.setStateTimer(pState.ticks);
        this.aiState = pState;
        int seqStartId = pDudeInfo.seqStartID;
        if (pState.seqId >= 0) {
            if (!game.getCache().contains(seqStartId + pState.seqId, "SEQ")) {
                //System.err.println("NO SEQ: fullSeq = "+ (seqStartId + pState.seqId)+" / "+pXSprite.data2+" / "+seqStartId);
                return;
            }

            seqSpawn(seqStartId + pState.seqId, SS_SPRITE, pSprite.getExtra(), pState.callback);
        }

        // call the enter function if defined
        if (pState.enter != null) pState.enter.run();
    }

    public void setTarget(int x, int y, int z) {
        pXSprite.setTarget(-1);
        pXSprite.setTargetX(x);
        pXSprite.setTargetY(y);
        pXSprite.setTargetZ(z);
    }

    public void setTarget(int nTarget) {
        if (!boardService.isValidSprite(nTarget)) {
            throw new AssertException("isValidSprite(nTarget)");
        }

        if (nTarget == pSprite.getOwner()) return;

        Sprite pTarget = boardService.getSprite(nTarget);
        if (!IsDudeSprite(pTarget)) return;

        DudeInfo pTargetInfo = dudeInfo[pTarget.getLotag() - kDudeBase];

        pXSprite.setTarget(nTarget);
        pXSprite.setTargetX(pTarget.getX());
        pXSprite.setTargetY(pTarget.getY());
        pXSprite.setTargetZ(pTarget.getZ() - (pTargetInfo.eyeHeight * pTarget.getYrepeat() << 2));
    }

    public void process() {
        if ((pSprite.getHitag() & kAttrFree) != 0) return;

        int nXSprite = pSprite.getExtra();

        // don't manipulate dead guys
        if ((pXSprite.getHealth() == 0)) return;

        pXSprite.setStateTimer(ClipLow(pXSprite.getStateTimer() - kFrameTicks, 0));
        if (aiState != null && aiState.move != null) aiState.move.run();
        if (aiState != null && aiState.think != null && (gFrame & kAIThinkMask) == (pSprite.getXvel() & kAIThinkMask))
            aiState.think.run();
        if (aiState != null && pXSprite.getStateTimer() == 0 && aiState.next != null) {
            if (aiState != null && aiState.ticks > 0) aiNewState(aiState.next);
            else if (seqFrame(SS_SPRITE, nXSprite) < 0) aiNewState(aiState.next);
        }

        // process dudes for recoil
        if (pXSprite.getHealth() != 0 && cumulDamage >= pDudeInfo.hinderDamage << 4) {
            pXSprite.setData3((short) cumulDamage);

            boolean chance = Chance(0x4000);
            if (pSprite.getStatnum() != kStatDude || !IsDudeSprite(pSprite)) return;

            recoil(chance);
        }

        // reset the cumulative damages for the next frame
        cumulDamage = 0;
    }

    public boolean heal(int value, int max) {
        value <<= 4; // fix this later in the calling code
        max <<= 4;
        if (pXSprite.getHealth() < max) {
            pXSprite.setHealth(ClipHigh(pXSprite.getHealth() + value, max));
            return true;
        }
        return false;
    }

    public int mass() {
        return pDudeInfo.mass;
    }

    protected static class AiState {
        final int seqId, ticks;
        final AiState next;
        final Runnable enter, move, think;
        final CALLPROC callback;

        public AiState(int seqId, CALLPROC callback, int ticks, Runnable enter, Runnable move, Runnable think, AiState next) {
            this.seqId = seqId;
            this.callback = callback;
            this.ticks = ticks;
            this.enter = enter;
            this.move = move;
            this.think = think;
            this.next = next;
        }
    }

}

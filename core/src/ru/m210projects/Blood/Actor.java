package ru.m210projects.Blood;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.AI.DudeExtra;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.Types.*;
import ru.m210projects.Blood.Types.Seq.SeqInst;
import ru.m210projects.Blood.Types.Seq.SeqType;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.collections.Pool;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Blood.AI.AIBURN.*;
import static ru.m210projects.Blood.AI.AICULTIST.cultistGoto;
import static ru.m210projects.Blood.AI.AIGILLBEAST.gillBeastGoto;
import static ru.m210projects.Blood.AI.AIUNICULT.*;
import static ru.m210projects.Blood.AI.AIZOMBA.zombieAGoto;
import static ru.m210projects.Blood.AI.AIZOMBF.zombieFGoto;
import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Gib.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.levelAddKills;
import static ru.m210projects.Blood.LEVELS.levelCalcKills;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.*;
import static ru.m210projects.Blood.SOUND.*;
import static ru.m210projects.Blood.Strings.seq;
import static ru.m210projects.Blood.Tile.*;
import static ru.m210projects.Blood.Trig.*;
import static ru.m210projects.Blood.Trigger.*;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.*;
import static ru.m210projects.Blood.VERSION.*;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Blood.Warp.*;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.*;

public class Actor {

    public static final int kAttrMove = 0x0001; // is affected by movement physics
    public static final int kAttrGravity = 0x0002; // is affected by gravity
    public static final int kAttrFalling = 0x0004; // in z motion
    public static final int kAttrAiming = 0x0008;
    public static final int kAttrRespawn = 0x0010;
    public static final int kAttrFree = 0x0020;
    public static final int kAttrSmoke = 0x0100; // receives tsprite smoke/steam
    public static final int kAttrFlipX = 0x0400;
    public static final int kAttrFlipY = 0x0800;
    public static final int kDamageFall = 0;
    public static final int kDamageBurn = 1;
    public static final int kDamageBullet = 2;
    public static final int kDamageExplode = 3;
    public static final int kDamageDrown = 4;
    public static final int kDamageSpirit = 5;
    public static final int kDamageTesla = 6;
    public static final int kDamageMax = 7;
    public static final int[] pSkillShift = {512, 384, 256, 208, 160};
    public static final int[] pPlayerShift = {144, 208, 256, 304, 368};
    public static final int kScreamVel = 0x155555; // zvel at which player
    // screams
//    public static final int kGruntVel = 700;
    public static final int kDudeDrag = 0x2A00;
    public static final int kMinDudeVel = 4096;
    public static final int kPowerUpTime = (kTimerRate * 30);
    public static final int kMaxPowerUpTime = ((kTimerRate * 60) * 60);
    public static final int kVectorTine = 0;
    public static final int kVectorShell = 1;
    public static final int kVectorBullet = 2;
    // 3
    // 4
    // 5
    public static final int kVectorBatBite = 6;
    public static final int kVectorBoneelBite = 7;
    // 8
    // 9
    public static final int kVectorAxe = 10;
    public static final int kVectorCleaver = 11;
    public static final int kVectorGhost = 12;
    public static final int kVectorGargSlash = 13;
    public static final int kVectorCerberusHack = 14;
    public static final int kVectorHoundBite = 15;
    public static final int kVectorRatBite = 16;
    public static final int kVectorSpiderBite = 17;
    // 18
    // 19
    // 20
    // 21
    public static final int kVectorMax = 23;
    public static final VECTORDATA[] gVectorData = { // 1.21 ok
            new VECTORDATA(2, 17, 174762, 1152, 10240, 0, 1, 20480, // 0 kVectorTine
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(43, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(43, 6, -1, 502), new SURFACE(43, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, 7, 502), new SURFACE(43, 6, 7, 502), new SURFACE(-1, -1, -1, 503), new SURFACE(43, -1, -1, -1), new SURFACE(-1, 6, -1, 503), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{1207, 1207}),

            new VECTORDATA(2, 4, 65536, 0, 8192, 0, 1, 12288, // 1 kVectorShell
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(43, 5, -1, -1), new SURFACE(-1, 5, -1, 501), new SURFACE(43, 6, -1, -1), new SURFACE(43, 0, -1, -1), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(43, 6, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(43, -1, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{1001, 1001}),

            new VECTORDATA(2, 7, 21845, 0, 32768, 0, 1, 12288, // 2 kVectorBullet
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(43, 5, 7, 510), new SURFACE(-1, 5, 7, 511), new SURFACE(43, 6, -1, 512), new SURFACE(43, 0, -1, 513), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, 7, 512), new SURFACE(43, 6, 7, 512), new SURFACE(-1, -1, -1, 513), new SURFACE(43, -1, -1, 513), new SURFACE(-1, 6, -1, 513), new SURFACE(-1, 6, -1, 513), new SURFACE(-1, 6, -1, 513), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{4001, 4002}),

            new VECTORDATA(2, 20, 65536, 0, 16384, 0, 1, 20480, // 3
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(43, 5, 7, 510), new SURFACE(-1, 5, 7, 511), new SURFACE(43, 6, -1, 512), new SURFACE(43, 0, -1, 513), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, 7, 512), new SURFACE(43, 6, 7, 512), new SURFACE(-1, -1, -1, 513), new SURFACE(43, -1, -1, 513), new SURFACE(-1, 6, -1, 513), new SURFACE(-1, 6, -1, 513), new SURFACE(-1, 6, -1, 513), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{431, 431}),

            new VECTORDATA(2, 6, 87381, 0, 12288, 0, 1, 6144, // 4
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(43, 5, -1, -1), new SURFACE(-1, 5, -1, 501), new SURFACE(43, 6, -1, -1), new SURFACE(43, 0, -1, -1), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(43, 6, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(43, -1, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{1002, 1002}),

            new VECTORDATA(2, 12, 65536, 0, 16384, 0, 1, 12288, // 5
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(43, 5, 7, 510), new SURFACE(-1, 5, 7, 511), new SURFACE(43, 6, -1, 512), new SURFACE(43, 0, -1, 513), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, 7, 512), new SURFACE(43, 6, 7, 512), new SURFACE(-1, -1, -1, 513), new SURFACE(43, -1, -1, 513), new SURFACE(-1, 6, -1, 513), new SURFACE(-1, 6, -1, 513), new SURFACE(-1, 6, -1, 513), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{359, 359}),

            new VECTORDATA(2, 4, 0, 921, 0, 0, 1, 4096, // 6 kVectorBatBite
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1), new SURFACE(-1, 5, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{521, 521}),

            new VECTORDATA(2, 12, 0, 1177, 0, 0, 0, 0, // 7 kVectorBoneelBite
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{513, 513}),

            new VECTORDATA(2, 9, 0, 1177, 0, 0, 0, 0, // 8
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{499, 499}),

            new VECTORDATA(3, 50, 43690, 1024, 8192, 0, 4, 32768, // 9
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{9012, 9014}),

            new VECTORDATA(2, 18, 436906, 1024, 16384, 0, 2, 20480, // 10
                    // kVectorAxe
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{1101, 1101}),

            new VECTORDATA(2, 9, 218453, 1024, 0, 0, 1, 24576, // 11
                    // kVectorCleaver
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{1207, 1207}),

            new VECTORDATA(2, 20, 436906, 1024, 16384, 0, 3, 24576, // 12
                    // kVectorGhost
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{499, 495}),

            new VECTORDATA(2, 16, 218453, 1024, 8192, 0, 4, 20480, // 13
                    // kVectorGargSlash
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1)}, new int[]{495, 496}),

            new VECTORDATA(2, 19, 218453, 614, 8192, 0, 2, 24576, // 14
                    // kVectorCerberusHack
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{9013, 499}),

            new VECTORDATA(2, 10, 218453, 614, 8192, 0, 2, 24576, // 15
                    // kVectorHoundBite
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{1307, 1308}),

            new VECTORDATA(2, 4, 0, 921, 0, 0, 1, 24576, // 16 kVectorRatBite
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, -1), new SURFACE(-1, 5, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{499, 499}),

            new VECTORDATA(2, 8, 0, 614, 0, 0, 1, 24576, // 17 kVectorSpiderBite
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{499, 499}),

            new VECTORDATA(2, 9, 0, 512, 0, 0, 0, 0, // 18
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, 5, -1, 500), new SURFACE(-1, 5, -1, 501), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 0, -1, 503), new SURFACE(-1, 4, -1, -1), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, 6, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 502), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{499, 499}),

            new VECTORDATA(-1, 0, 0, 2560, 0, 0, 0, 0, // 19
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, '\"', '#', -1), new SURFACE(-1, '\"', '#', -1), new SURFACE(-1, '\"', '#', -1), new SURFACE(-1, '\"', '#', -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, '\"', '#', -1), new SURFACE(-1, '\"', '#', -1), new SURFACE(-1, '\"', '#', -1), new SURFACE(-1, '\"', '#', -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{0, 0}),

            new VECTORDATA(1, 2, 0, 0, 0, 15, 0, 0, // 20
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{351, 351}),

            new VECTORDATA(5, 25, 0, 0, 0, 0, 0, 0, // 21
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, -1)}, new int[]{0, 0}),

            new VECTORDATA(0, 37, 874762, 620, 0, 0, 0, 0, // 22 kVectorGenDudePunch
                    new SURFACE[]{new SURFACE(-1, -1, -1, -1), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357), new SURFACE(-1, -1, -1, 357)}, new int[]{357, 499}),

    };
    public static final int kExplodeMax = 9; // Max explosions
    public static final EXPLODE[] gExplodeData = { // 1.21 ok
            new EXPLODE(40, 10, 10, 75, 450, 0, 60, 80, 40), new EXPLODE(80, 20, 10, 150, 900, 0, 60, 160, 60), new EXPLODE(120, 40, 15, 225, 1350, 0, 60, 240, 80), new EXPLODE(80, 5, 10, 120, 20, 10, 60, 0, 40), new EXPLODE(120, 10, 10, 180, 40, 10, 60, 0, 80), new EXPLODE(160, 15, 10, 240, 60, 10, 60, 0, 120), new EXPLODE(40, 20, 10, 120, 0, 10, 30, 60, 40), new EXPLODE(80, 20, 10, 150, 800, 5, 60, 160, 60)};
    public static final MissileType[] gMissileData = { // 1.21 ok
            new MissileType(2138, 978670, 512, 40, 40, 240, 16, new int[]{1207, 1207}), new MissileType(2424, 3145728, 0, 32, 32, 128, 32, new int[]{420, 420}), new MissileType(3056, 2796202, 0, 32, 32, 128, 32, new int[]{471, 471}), new MissileType(2424, 2446677, 0, 32, 32, 128, 4, new int[]{421, 421}), new MissileType(0, 1118481, 0, 24, 24, 128, 16, new int[]{1309, 351}), new MissileType(0, 1118481, 0, 32, 32, 128, 32, new int[]{480, 480}), new MissileType(2130, 2796202, 0, 32, 32, 128, 16, new int[]{470, 470}), new MissileType(870, 699050, 0, 32, 32, 232, 32, new int[]{489, 490}), new MissileType(0, 1118481, 0, 24, 24, 128, 16, new int[]{462, 351}), new MissileType(0, 838860, 0, 16, 16, 240, 16, new int[]{1203, 172}), new MissileType(0, 838860, 0, 8, 8, 0, 16, new int[]{0, 0}), new MissileType(3056, 2097152, 0, 32, 32, 128, 16, new int[]{1457, 249}), new MissileType(0, 2446677, 0, 30, 30, 128, 24, new int[]{480, 489}), new MissileType(0, 2446677, 0, 30, 30, 128, 24, new int[]{489, 480}), new MissileType(0, 1398101, 0, 24, 24, 128, 16, new int[]{480, 489}), new MissileType(2446, 2796202, 0, 32, 32, 128, 16, new int[]{491, 491}), new MissileType(3056, 2446677, 0, 16, 16, 128, 16, new int[]{520, 520}), new MissileType(3056, 1747626, 0, 32, 32, 128, 16, new int[]{520, 250}),};
    public static final int kFXMax = 57;
    public static final EFFECT[] gEffectInfo = { // 1.21 ok
            new EFFECT(-1, 0, 4107, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 0, 4108, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 0, 4109, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 0, 4110, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 0, 7, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 0, 4102, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 0, 4103, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 0, 4104, 1, -128, 8192, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 2, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 2, 4099, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 2, 4100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), // 10
            new EFFECT(-1, 1, 4106, 3, -256, 8192, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 1, 4118, 3, -256, 8192, 0, 0, 0, 0, 0, 0, 0), new EFFECT(14, 2, 0, 1, 46603, 2048, 480, 2154, 40, 40, 0, 244, 0), new EFFECT(-1, 2, 0, 3, 46603, 5120, 480, 2269, 24, 24, 0, 128, 0), new EFFECT(-1, 2, 0, 3, 46603, 5120, 480, 1720, 24, 24, 0, 128, 0), new EFFECT(-1, 1, 0, 1, 58254, 3072, 480, 2280, 48, 48, 0, 128, 0), new EFFECT(-1, 1, 0, 1, 58254, 3072, 480, 3135, 48, 48, 0, 128, 0), new EFFECT(-1, 0, 0, 3, 58254, 1024, 480, 3261, 32, 32, 0, 0, 0), new EFFECT(-1, 1, 0, 3, 58254, 1024, 480, 3265, 32, 32, 0, 0, 0), new EFFECT(-1, 1, 0, 3, 58254, 1024, 480, 3269, 32, 32, 0, 0, 0), // 20
            new EFFECT(-1, 1, 0, 3, 58254, 1024, 480, 3273, 32, 32, 0, 0, 0), new EFFECT(-1, 1, 0, 3, 58254, 1024, 480, 3277, 32, 32, 0, 0, 0), new EFFECT(-1, 2, 0, 1, -27962, 8192, 600, 1128, 16, 16, 514, 240, 0), new EFFECT(-1, 2, 0, 1, -18641, 8192, 600, 1128, 12, 12, 514, 240, 0), new EFFECT(-1, 2, 0, 1, -9320, 8192, 600, 1128, 8, 8, 514, 240, 0), new EFFECT(-1, 2, 0, 1, -18641, 8192, 600, 1131, 32, 32, 514, 240, 0), new EFFECT(14, 2, 0, 3, 27962, 4096, 480, 733, 32, 32, 0, 240, 0), new EFFECT(-1, 1, 0, 3, 18641, 4096, 120, 2261, 12, 12, 0, 128, 0), new EFFECT(-1, 0, 4105, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 1, 0, 3, 58254, 3328, 480, 2185, 48, 48, 0, 0, 0), // 30
            new EFFECT(-1, 0, 0, 3, 58254, 1024, 480, 2620, 48, 48, 0, 0, 0), new EFFECT(-1, 1, 4113, 1, -13981, 5120, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 1, 4114, 1, -13981, 5120, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 1, 4115, 1, 0, 2048, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 1, 4116, 1, 0, 2048, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 2, 0, 0, 0, 0, 960, 956, 32, 32, 610, 0, 0), new EFFECT(16, 2, 4120, 0, 46603, 1024, 0, 0, 0, 0, 0, 0, 0), new EFFECT(16, 2, 4121, 0, 46603, 1024, 0, 0, 0, 0, 0, 0, 0), new EFFECT(16, 2, 4122, 0, 46603, 1024, 0, 0, 0, 0, 0, 0, 0), new EFFECT(16, 2, 4123, 0, 46603, 1024, 0, 0, 0, 0, 0, 0, 0), // 40
            new EFFECT(16, 2, 4124, 0, 46603, 1024, 0, 0, 0, 0, 0, 0, 0), new EFFECT(16, 2, 4125, 0, 46603, 1024, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 1, 0, 3, 0, 0, 0, 838, 16, 16, 80, 248, 0), // BullerHole
            new EFFECT(-1, 0, 0, 3, 34952, 8192, 0, 2078, 64, 64, 0, 248, 0), new EFFECT(-1, 0, 0, 3, 34952, 8192, 0, 1106, 64, 64, 0, 248, 0), new EFFECT(-1, 0, 0, 3, 58254, 3328, 480, 2406, 48, 48, 0, 0, 0), new EFFECT(-1, 1, 0, 3, 46603, 4096, 480, 3511, 64, 64, 0, 128, 0), new EFFECT(-1, 0, 8, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 2, 11, 3, -256, 8192, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 2, 11, 3, 0, 8192, 0, 0, 0, 0, 0, 0, 0), // 50
            new EFFECT(-1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0), new EFFECT(-1, 1, 30, 3, 0, 0, 0, 0, 40, 40, 80, 248, 0), // 52
            new EFFECT(19, 2, 0, 3, 27962, 4096, 480, 4023, 32, 32, 0, 240, 0), new EFFECT(19, 2, 0, 3, 27962, 4096, 480, 4028, 32, 32, 0, 240, 0), new EFFECT(-1, 2, 0, 0, 0, 0, 480, 926, 32, 32, 610, 244, 0), new EFFECT(-1, 1, 4113, 1, -13981, 5120, 0, 0, 0, 0, 0, 0, 0)};
    public static final THINGINFO[] thingInfo = { // 1.21 ok
            /*0*/new THINGINFO(25, 250, 32, 11, 4096, 80, 384, 907, 0, 0, 0, 0, new int[]{256, 256, 128, 64, 0, 0, 128}, 1), // kThingTNTBarrel
            /*1*/new THINGINFO(5, 5, 16, 3, 24576, 1600, 256, 3444, 240, 0, 32, 32, new int[]{256, 256, 256, 64, 0, 0, 512}, 1), // kThingTNTProxArmed
            /*2*/new THINGINFO(5, 5, 16, 3, 24576, 1600, 256, 3457, 240, 0, 32, 32, new int[]{256, 256, 256, 64, 0, 0, 512}, 1), // kThingTNTRemArmed
            /*3*/new THINGINFO(1, 20, 32, 3, 32768, 80, 0, 739, 0, 0, 0, 0, new int[]{256, 0, 256, 128, 0, 0, 0}, 0), // kThingBlueVase
            /*4*/new THINGINFO(1, 150, 32, 3, 32768, 80, 0, 642, 0, 0, 0, 0, new int[]{256, 256, 256, 128, 0, 0, 0}, 0), // kThingBrownVase
            /*5*/new THINGINFO(10, 0, 0, 0, 0, 0, 0, 462, 0, 0, 0, 0, new int[]{0, 0, 0, 256, 0, 0, 0}, 0), // kThingCrateFace
            /*6*/new THINGINFO(1, 0, 0, 0, 0, 0, 0, 266, 0, 0, 0, 0, new int[]{256, 0, 256, 256, 0, 0, 0}, 0), // kThingClearGlass
            /*7*/new THINGINFO(1, 0, 0, 0, 0, 0, 0, 796, 0, 0, 0, 0, new int[]{256, 0, 256, 256, 0, 0, 512}, 0), // kThingFluorescent
            /*8*/new THINGINFO(50, 0, 0, 0, 0, 0, 0, 1127, 0, 0, 0, 0, new int[]{0, 0, 0, 256, 0, 0, 0}, 0), //kThingWallCrack
            /*9*/new THINGINFO(8, 0, 0, 0, 0, 0, 0, 1142, 0, 0, 0, 0, new int[]{256, 0, 256, 128, 0, 0, 0}, 0), // kThingWoodBeam
            /*10*/new THINGINFO(4, 0, 0, 0, 0, 0, 0, 1069, 0, 0, 0, 0, new int[]{256, 256, 64, 256, 0, 0, 128}, 0), //kThingWeb 410
            /*11*/new THINGINFO(40, 0, 0, 0, 0, 0, 0, 483, 0, 0, 0, 0, new int[]{64, 0, 128, 256, 0, 0, 0}, 0), // kThingMetalGrate1
            /*12*/new THINGINFO(1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, new int[]{0, 256, 0, 256, 0, 0, 128}, 0), //kThingFlammableTree
            /*13*/new THINGINFO(1000, 0, 0, 8, 0, 0, 0, -1, 0, 0, 0, 0, new int[]{0, 0, 128, 256, 0, 0, 512}, 0), // kThingMachineGun
            /*14*/new THINGINFO(0, 15, 8, 3, 32768, 0, 0, -1, 0, 0, 0, 0, new int[]{0, 0, 0, 0, 0, 0, 0}, 0), // kThingFallingRock
            /*15*/new THINGINFO(0, 8, 48, 3, 49152, 0, 0, 3540, 0, 0, 0, 0, new int[]{0, 0, 0, 0, 0, 0, 0}, 1), // kThingPail
            /*16*/new THINGINFO(10, 2, 0, 0, 32768, 0, 0, -1, 0, 0, 0, 0, new int[]{256, 0, 256, 256, 0, 0, 128}, 0), // kThingGibObject
            /*17*/new THINGINFO(20, 2, 0, 0, 32768, 0, 0, -1, 0, 0, 0, 0, new int[]{0, 0, 0, 256, 0, 0, 128}, 0), //kThingExplodeObject
            /*18*/new THINGINFO(5, 14, 16, 3, 24576, 1600, 256, 3422, 224, 0, 32, 32, new int[]{64, 256, 128, 64, 0, 0, 256}, 1), //kThingTNTStick
            /*19*/new THINGINFO(5, 14, 16, 3, 24576, 1600, 256, 3433, 224, 0, 32, 32, new int[]{64, 256, 128, 64, 0, 0, 256}, 1), // kThingTNTBundle
            /*20*/new THINGINFO(5, 14, 16, 3, 32768, 1600, 256, 3467, 128, 0, 32, 32, new int[]{64, 256, 128, 64, 0, 0, 256}, 1), // SprayBundle 420
            /*21*/new THINGINFO(5, 6, 16, 3, 32768, 1600, 256, 1462, 0, 0, 32, 32, new int[]{0, 0, 0, 0, 0, 0, 0}, 1), // kThingBoneClub
            /*22*/new THINGINFO(8, 3, 16, 11, 32768, 1600, 256, -1, 0, 0, 0, 0, new int[]{256, 0, 256, 256, 0, 0, 0}, 0), //kThingZombieBones
            /*23*/new THINGINFO(0, 1, 1, 2, 0, 0, 0, 1147, 0, 10, 0, 0, new int[]{0, 0, 0, 0, 0, 0, 0}, 0), // kThingWaterDrip
            /*24*/new THINGINFO(0, 1, 1, 2, 0, 0, 0, 1160, 0, 2, 0, 0, new int[]{0, 0, 0, 0, 0, 0, 0}, 0), // kThingBloodDrip
            /*25*/new THINGINFO(15, 4, 4, 3, 24576, 0, 257, -1, 0, 0, 0, 0, new int[]{128, 64, 256, 256, 0, 0, 256}, 0), //425
            /*26*/new THINGINFO(30, 30, 8, 3, 8192, 0, 257, -1, 0, 0, 0, 0, new int[]{128, 64, 256, 256, 0, 0, 64}, 0), // 426 kThingGib
            /*27*/new THINGINFO(60, 5, 32, 3, 40960, 1280, 257, 3405, 0, 0, 40, 40, new int[]{128, 64, 256, 256, 0, 0, 64}, 1), //ok kThingZombieHead
            /*28*/new THINGINFO(80, 30, 32, 3, 57344, 1600, 256, 3281, 128, 0, 32, 32, new int[]{0, 0, 0, 0, 0, 0, 0}, 1), //428
            /*29*/new THINGINFO(80, 30, 32, 3, 57344, 1600, 256, 2020, 128, 0, 32, 32, new int[]{256, 0, 256, 256, 0, 0, 0}, 1), // kThingPodFire
            /*30*/new THINGINFO(80, 30, 32, 3, 57344, 1600, 256, 1860, 128, 0, 32, 32, new int[]{256, 0, 256, 256, 0, 0, 0}, 1), // kThingPodGreen
            /*31*/new THINGINFO(150, 30, 48, 3, 32768, 1600, 257, 800, 128, 0, 48, 48, new int[]{64, 64, 112, 64, 0, 96, 96}, 1), // 431
            /*32*/new THINGINFO(1, 30, 48, 3, 0, 32768, 1600, 0, 2443, 128, 16, 16, new int[]{0, 0, 0, 0, 0, 0, 0}, 0), // 432
            // GDX THINGS
            /*33*/new THINGINFO(5, 5, 16, 3, 24576, 1600, 256, 3444, 240, 7, 32, 32, new int[]{256, 256, 256, 64, 0, 0, 512}, 1), // kThingTNTProxArmed2 (Detects only players)
            /*34*/new THINGINFO(5, 6, 16, 3, 32768, 1600, 256, 1462, 0, 0, 32, 32, new int[]{0, 0, 0, 0, 0, 0, 0}, 1), // kThingThrowableRock (similar to kThingBoneClub)
            /*35*/new THINGINFO(150, 30, 48, 3, 32768, 1600, 257, 800, 128, 0, 48, 48, new int[]{64, 64, 112, 64, 0, 96, 96}, 1), // kThingCustomDudeLifeLeech
            /*36 new THINGINFO(150, 30, 48, 3, 32768, 1600, 257, 3833, 128, 0, 48, 48, new int[] { 64, 64, 112, 64, 0, 96, 96 },0),*/ // kGDXThingCalebHat
    };
    public static final AMMOITEMDATA[] gAmmoItemData = {
            new AMMOITEMDATA(0, 618, -8, 0, 48, 48, 480, 6, 7),
            new AMMOITEMDATA(0, 589, -8, 0, 48, 48, 1, 5, 6),
            new AMMOITEMDATA(0, 589, -8, 0, 48, 48, 1, 5, 6),
            new AMMOITEMDATA(0, 809, -8, 0, 48, 48, 5, 5, 6),
            new AMMOITEMDATA(0, 811, -8, 0, 48, 48, 1, 10, 11),
            new AMMOITEMDATA(0, 810, -8, 0, 48, 48, 1, 11, 12),
            new AMMOITEMDATA(0, 820, -8, 0, 24, 24, 10, 8, 0),
            new AMMOITEMDATA(0, 619, -8, 0, 48, 48, 4, 2, 0),
            new AMMOITEMDATA(0, 812, -8, 0, 48, 48, 15, 2, 0),
            new AMMOITEMDATA(0, 813, -8, 0, 48, 48, 15, 3, 0),
            new AMMOITEMDATA(0, 525, -8, 0, 48, 48, 100, 9, 10),
            new AMMOITEMDATA(0, 814, -8, 0, 48, 48, 15, 255, 0),
            new AMMOITEMDATA(0, 817, -8, 0, 48, 48, 100, 3, 0),
            new AMMOITEMDATA(0, 548, -8, 0, 24, 24, 32, 7, 0),
            new AMMOITEMDATA(0, 0, -8, 0, 48, 48, 6, 255, 0),
            new AMMOITEMDATA(0, 0, -8, 0, 48, 48, 6, 255, 0),
            new AMMOITEMDATA(0, 816, -8, 0, 48, 48, 8, 1, 0),
            new AMMOITEMDATA(0, 818, -8, 0, 48, 48, 8, 255, 0),
            new AMMOITEMDATA(0, 819, -8, 0, 48, 48, 8, 255, 0),
            new AMMOITEMDATA(0, 801, -8, 0, 48, 48, 6, 4, 0),
            new AMMOITEMDATA(0, 0, 0, 0, 0, 0, 0, 0, 0)};
    public static final WEAPONITEMDATA[] gWeaponItemData = {new WEAPONITEMDATA(0, 4096, 0, 0, 0, 0, 0, -1, 0), // None
            new WEAPONITEMDATA(0, 559, -8, 0, 48, 48, 3, 2, 8), // Shotgun
            new WEAPONITEMDATA(0, 558, -8, 0, 48, 48, 4, 3, 50), // Tommy
            new WEAPONITEMDATA(0, 524, -8, 0, 48, 48, 2, 1, 9), // Flare
            new WEAPONITEMDATA(0, 525, -8, 0, 48, 48, 10, 9, 100), // Voodoo
            new WEAPONITEMDATA(0, 539, -8, 0, 48, 48, 8, 7, 64), // Tesla
            new WEAPONITEMDATA(0, 526, -8, 0, 48, 48, 5, 4, 6), // Napalm
            new WEAPONITEMDATA(0, 4096, 0, 0, 0, 0, 1, -1, 0), // Reserved
            new WEAPONITEMDATA(0, 618, -8, 0, 48, 48, 7, 6, 480), // SprayCan
            new WEAPONITEMDATA(0, 589, -8, 0, 48, 48, 6, 5, 1), // TNT
            new WEAPONITEMDATA(0, 800, -8, 0, 48, 48, 9, 8, 35) // LifeLeach
    };
    public static final ARMORITEMDATA[] gArmorItemData = {new ARMORITEMDATA(new int[]{800, 800, 800}, new int[]{1600, 1600, 1600}), new ARMORITEMDATA(new int[]{1600, 0, 0}, new int[]{1600, 1600, 1600}), new ARMORITEMDATA(new int[]{0, 1600, 0}, new int[]{1600, 1600, 1600}), new ARMORITEMDATA(new int[]{0, 0, 1600}, new int[]{1600, 1600, 1600}), new ARMORITEMDATA(new int[]{3200, 3200, 3200}, new int[]{3200, 3200, 3200}),};
    public static final POWERUPINFO[] gPowerUpInfo = {new POWERUPINFO(-1, true, 1, 1), // kItemKey1
            new POWERUPINFO(-1, true, 1, 1), // kItemKey2
            new POWERUPINFO(-1, true, 1, 1), // kItemKey3
            new POWERUPINFO(-1, true, 1, 1), // kItemKey4
            new POWERUPINFO(-1, true, 1, 1), // kItemKey5
            new POWERUPINFO(-1, true, 1, 1), // kItemKey6
            new POWERUPINFO(-1, true, 1, 1), // kItemKey7
            new POWERUPINFO(-1, false, 100, 100), // kItemDoctorBag
            new POWERUPINFO(-1, false, 50, 100), // kItemMedPouch
            new POWERUPINFO(-1, false, 20, 100), // kItemLifeEssence
            new POWERUPINFO(-1, false, 100, 200), // kItemLifeSeed
            new POWERUPINFO(-1, false, 2, 200), // kItemPotion1
            new POWERUPINFO(783, false, kPowerUpTime, kMaxPowerUpTime), // kItemFeatherFall
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemLtdInvisibility
            new POWERUPINFO(-1, true, kPowerUpTime, kMaxPowerUpTime), // kItemInvulnerability
            new POWERUPINFO(827, false, kPowerUpTime, kMaxPowerUpTime), // kItemJumpBoots
            new POWERUPINFO(828, false, kPowerUpTime, kMaxPowerUpTime), // kItemRavenFlight
            new POWERUPINFO(829, false, kPowerUpTime, 4 * kMaxPowerUpTime), // kItemGunsAkimbo
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemDivingSuit
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemGasMask
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemClone
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemCrystalBall
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemDecoy
            new POWERUPINFO(851, false, kPowerUpTime, kMaxPowerUpTime), // kItemDoppleganger
            new POWERUPINFO(2428, false, kPowerUpTime, kMaxPowerUpTime), // kItemReflectiveShots
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemBeastVision
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemShadowCloak
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemShroomRage
            new POWERUPINFO(-1, false, kPowerUpTime / 4, kMaxPowerUpTime), // kItemShroomDelirium
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemShroomGrow
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemShroomShrink
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemDeathMask
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemWineGoblet
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemWineBottle
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemSkullGrail
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemSilverGrail
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemTome
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemBlackChest
            new POWERUPINFO(-1, false, kPowerUpTime, kMaxPowerUpTime), // kItemWoodenChest
            new POWERUPINFO(-1, true, kPowerUpTime, kMaxPowerUpTime), // kItemAsbestosArmor 139
            new POWERUPINFO(-1, false, 1, kMaxPowerUpTime), new POWERUPINFO(-1, false, 1, kMaxPowerUpTime), new POWERUPINFO(-1, false, 1, kMaxPowerUpTime), new POWERUPINFO(-1, false, 1, kMaxPowerUpTime), new POWERUPINFO(-1, false, 1, kMaxPowerUpTime), new POWERUPINFO(0, false, 0, 0), new POWERUPINFO(0, false, 0, 0), new POWERUPINFO(0, false, 0, 0), new POWERUPINFO(0, false, 0, 0), new POWERUPINFO(-1, false, 1, kMaxPowerUpTime), // kItemJetpack
            //new POWERUPINFO(-1, true, 1, 1), // kGDXItemMap
    };
    public static final ITEMDATA[] gItemInfo = {new ITEMDATA(-1, 2552, -8, 0, 32, 32), new ITEMDATA(-1, 2553, -8, 0, 32, 32), new ITEMDATA(-1, 2554, -8, 0, 32, 32), new ITEMDATA(-1, 2555, -8, 0, 32, 32), new ITEMDATA(-1, 2556, -8, 0, 32, 32), new ITEMDATA(-1, 2557, -8, 0, 32, 32), new ITEMDATA(-1, 0, -8, 0, 0, 0), new ITEMDATA(0, 519, -8, 0, 48, 48), new ITEMDATA(-1, 822, -8, 0, 40, 40), new ITEMDATA(-1, 2169, -8, 0, 40, 40), new ITEMDATA(-1, 2433, -8, 0, 40, 40), new ITEMDATA(-1, 517, -8, 0, 40, 40), new ITEMDATA(-1, 783, -8, 0, 40, 40), new ITEMDATA(-1, 896, -8, 0, 40, 40), new ITEMDATA(-1, 825, -8, 0, 40, 40), new ITEMDATA(4, 827, -8, 0, 40, 40), new ITEMDATA(-1, 828, -8, 0, 40, 40), new ITEMDATA(6, 829, -8, 0, 40, 40), new ITEMDATA(1, 830, -8, 0, 80, 64), new ITEMDATA(-1, 831, -8, 0, 40, 40), new ITEMDATA(-1, 863, -8, 0, 40, 40), new ITEMDATA(2, 760, -8, 0, 40, 40), new ITEMDATA(-1, 836, -8, 0, 40, 40), new ITEMDATA(-1, 851, -8, 0, 40, 40), new ITEMDATA(-1, 2428, -8, 0, 40, 40), new ITEMDATA(3, 839, -8, 0, 40, 40), new ITEMDATA(-1, 768, -8, 0, 64, 64), new ITEMDATA(-1, 840, -8, 0, 48, 48), new ITEMDATA(-1, 841, -8, 0, 48, 48), new ITEMDATA(-1, 842, -8, 0, 48, 48), new ITEMDATA(-1, 843, -8, 0, 48, 48), new ITEMDATA(-1, 683, -8, 0, 40, 40), new ITEMDATA(-1, 521, -8, 0, 40, 40), new ITEMDATA(-1, 604, -8, 0, 40, 40), new ITEMDATA(-1, 520, -8, 0, 40, 40), new ITEMDATA(-1, 803, -8, 0, 40, 40), new ITEMDATA(-1, 518, -8, 0, 40, 40), new ITEMDATA(-1, 522, -8, 0, 40, 40), new ITEMDATA(-1, 523, -8, 0, 40, 40), new ITEMDATA(-1, 837, -8, 0, 80, 64), new ITEMDATA(-1, 2628, -8, 0, 64, 64), new ITEMDATA(-1, 2586, -8, 0, 64, 64), new ITEMDATA(-1, 2578, -8, 0, 64, 64), new ITEMDATA(-1, 2602, -8, 0, 64, 64), new ITEMDATA(-1, 2594, -8, 0, 64, 64), new ITEMDATA(-1, 753, -8, 0, 64, 64), new ITEMDATA(-1, 753, -8, 7, 64, 64), new ITEMDATA(-1, 3558, 128, 0, 64, 64), new ITEMDATA(-1, 3558, 128, 7, 64, 64)};
    public static final int kGlobalForceShift = 18; // arbitrary scale for all
    public static final int[] NapalmAmmo = new int[2];
    public static int gTNTCount;
    public static int gNoEnemies = 0;
    public static int[] gWallExp = new int[kMaxXWalls];
    public static int[] gSectorExp = new int[MAXSECTORS];
    public static byte[] gSpriteExp = new byte[(MAXSECTORS + 7) >> 3]; // MAXSECTORS it's not a typo
    public static Pool<POSTPONE> gPost = new Pool<>(POSTPONE::new, 4096);
    public static long[] floorVel = new long[MAXSECTORS];
    public static long[] ceilingVel = new long[MAXSECTORS];

    public static long refl_x, refl_y, refl_z;
    static int info_nSprite = -1;
    static BloodSprite info_pSprite = null;
    static int info_nWall = -1;
    static Wall info_pWall = null;
    static XWALL info_pXWall = null;
    static int info_nSector = -1;
    static Sector info_pSector = null;
    static XSECTOR info_pXSector = null;

    public static void AirDrag(BloodSprite pSprite, int nDrag) {
        int windX = 0, windY = 0;

        int nSector = pSprite.getSectnum();

        if (!boardService.isValidSector(nSector)) {
            throw new AssertException("boardService.isValidSector(nSector)");
        }
        if (boardService.getSector(nSector).getExtra() > 0) {
            int nXSector = boardService.getSector(nSector).getExtra();
            if (!(nXSector > 0 && nXSector < kMaxXSectors)) {
                throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
            }

            XSECTOR pXSector = xsector[nXSector];

            if (pXSector.windVel != 0 && (pXSector.windAlways || pXSector.busy != 0)) {
                int windVel = pXSector.windVel << 12;
                if (!pXSector.windAlways) {
                    windVel = mulscale(pXSector.busy, windVel, 16);
                }

                windX = mulscale(Cos(pXSector.windAng), windVel, 30);
                windY = mulscale(Sin(pXSector.windAng), windVel, 30);
            }
        }
        
        pSprite.addVelocity(mulscale(nDrag, windX - pSprite.getVelocityX(), 16),
                mulscale(nDrag, windY - pSprite.getVelocityY(), 16),
                -mulscale(nDrag, pSprite.getVelocityZ(), 16));
    }

    public static boolean IsNormalWall(int nWall) {
        if (!boardService.isValidWall(nWall)) {
            throw new AssertException("boardService.isValidWall(nWall)");
        }
        Wall pWall = boardService.getWall(nWall);
        int cstat = pWall.getCstat();
        if ((cstat & kWallMoveForward) != 0) {
            return false;
        } else if ((cstat & kWallMoveBackward) != 0) {
            return false;
        } else {
            if (pWall.getLotag() < 500 || pWall.getLotag() >= 512) {
                if (pWall.getNextsector() != -1) {
                    return boardService.getSector(pWall.getNextsector()).getLotag() < kSectorBase || boardService.getSector(pWall.getNextsector()).getLotag() >= kSectorMax;
                }
                return true;
            } else {
                return false;
            }
        }
    }

    public static void actFireVector(Sprite pActor, int xOffset, int zOffset, int dx, int dy, int dz, int vectorType) {
        int hitCode;

        if (!(vectorType >= 0 && vectorType < kVectorMax)) {
            throw new AssertException("vectorType >= 0 && vectorType < kVectorMax");
        }
        VECTORDATA pVectorData = gVectorData[vectorType];

        int maxDist = pVectorData.maxDist;
        if ((hitCode = VectorScan(pActor, xOffset, zOffset, dx, dy, dz, maxDist, 1)) == SS_SPRITE) {
            Sprite pSprite = boardService.getSprite(pHitInfo.hitsprite);
            if (pSprite == null) {
                throw new AssertException("pSprite != null");
            }

            if (IsPlayerSprite(pSprite) && powerupCheck(gPlayer[pSprite.getLotag() - kDudePlayer1], kItemReflectiveShots - kItemBase) != 0) {
                pHitInfo.hitsprite = pActor.getXvel();
                pHitInfo.hitx = pActor.getX();
                pHitInfo.hity = pActor.getY();
                pHitInfo.hitz = pActor.getZ();
            }
        }

        // determine a point just a bit back from the intersection to place the ricochet
        int rx = pHitInfo.hitx - mulscale(dx, 1 << 4, 14);
        int ry = pHitInfo.hity - mulscale(dy, 1 << 4, 14);
        int rz = pHitInfo.hitz - mulscale(dz, 1 << 8, 14);

        int nSurf = kSurfNone;
        short nSector = (short) pHitInfo.hitsect;
        byte[] surfType = game.getCurrentDef().surfType;

        if (maxDist == 0 || EngineUtils.qdist(pHitInfo.hitx - pActor.getX(), pHitInfo.hity - pActor.getY()) < maxDist) {
            switch (hitCode) {
                case SS_CEILING:
                    if ((boardService.getSector(pHitInfo.hitsect).getCeilingstat() & kSectorParallax) != 0) {
                        nSurf = kSurfNone;
                    } else {
                        nSurf = surfType[boardService.getSector(pHitInfo.hitsect).getCeilingpicnum()];
                    }
                    break;

                case SS_FLOOR:
                    if ((boardService.getSector(pHitInfo.hitsect).getFloorstat() & kSectorParallax) != 0) {
                        nSurf = kSurfNone;
                    } else {
                        nSurf = surfType[boardService.getSector(pHitInfo.hitsect).getFloorpicnum()];
                    }
                    break;

                case SS_WALL: {
                    int nWall = pHitInfo.hitwall;
                    if (!boardService.isValidWall(nWall)) {
                        throw new AssertException("boardService.isValidWall(nWall)");
                    }
                    nSurf = surfType[boardService.getWall(nWall).getPicnum()];
                    if (IsNormalWall(nWall)) {
                        int wx = pHitInfo.hitx - mulscale(16, dx, 14);
                        int wy = pHitInfo.hity - mulscale(16, dy, 14);
                        int wz = pHitInfo.hitz - mulscale(256, dz, 14);
                        if (!(nSurf < kSurfMax)) {
                            throw new AssertException("nSurf < kSurfMax");
                        }
                        if (pVectorData.impact[nSurf].nEffect1 >= 0) { // bullethole
                            Sprite pSpawn;
                            if ((pSpawn = actSpawnEffect(pVectorData.impact[nSurf].nEffect1, nSector, wx, wy, wz, 0)) != null) {
                                pSpawn.setCstat(pSpawn.getCstat() | kSpriteWall);
                                pSpawn.setAng((short) ((GetWallAngle(nWall) + kAngle90) & kAngleMask));
                            }
                        }
                    }
                    break;
                }

                case SS_MASKED: {
                    int nWall = pHitInfo.hitwall;
                    if (!boardService.isValidWall(nWall)) {
                        throw new AssertException("boardService.isValidWall(nWall)");
                    }
                    nSurf = surfType[boardService.getWall(nWall).getOverpicnum()];
                    int nXWall = boardService.getWall(nWall).getExtra();
                    if (nXWall > 0) {
                        XWALL pXWall = xwall[nXWall];
                        if (pXWall.triggerVector) {
                            trTriggerWall(nWall, pXWall, kCommandWallImpact);
                        }
                    }
                    break;
                }

                case SS_SPRITE: {
                    int nSprite = pHitInfo.hitsprite;
                    BloodSprite pSprite = boardService.getSprite(nSprite);
                    if (pSprite == null) {
                        throw new AssertException("pSprite != null");
                    }

                    nSector = (short) pHitInfo.hitsect;

                    nSurf = surfType[pSprite.getPicnum()];

                    // back off ricochet (squibs) even more
                    rx -= mulscale(dx, 7 << 4, 14);
                    ry -= mulscale(dy, 7 << 4, 14);
                    rz -= mulscale(dz, 7 << 8, 14);

                    int shift = 4;
                    if (vectorType == 0) {
                        if (!IsPlayerSprite(pSprite)) {
                            shift = 3;
                        }
                    }

                    actDamageSprite(pActor.getXvel(), pSprite, pVectorData.damageType, pVectorData.damageValue << shift);

                    XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                    if (pXSprite != null && pXSprite.isVector()) {
                        trTriggerSprite(nSprite, pXSprite, kCommandSpriteImpact);
                    }

                    if (pSprite.getStatnum() == kStatThing) {
                        if (pSprite.getLotag() >= kThingBase && pSprite.getLotag() < kThingMax && thingInfo[pSprite.getLotag() - kThingBase].mass > 0) {
                            if (pVectorData.hitForce != 0) {
                                int impulse = (pVectorData.hitForce << 8) / thingInfo[pSprite.getLotag() - kThingBase].mass;
                                pSprite.addVelocity(mulscale(impulse, dx, 16), 
                                        mulscale(impulse, dy, 16), 
                                        mulscale(impulse, dz, 16));
                            }
                        }

                        if (pXSprite != null && pVectorData.burn != 0) {
                            if (pXSprite.getBurnTime() == 0) {
                                evPostCallback(nSprite, 3, 0, 0);
                            }

                            pXSprite.setBurnSource(actSetBurnSource(pActor.getXvel()));
                            pXSprite.setBurnTime(ClipHigh(pXSprite.getBurnTime() + pVectorData.burn, 1200));
                        }
                    }

                    if (pSprite.getStatnum() == kStatDude) {
                        int mass = 0;
                        if (IsDudeSprite(pSprite)) {
                            switch (pSprite.getLotag()) {
                                case kGDXDudeUniversalCultist:
                                case kGDXGenDudeBurning:
                                    mass = getDudeMassBySpriteSize(pSprite);
                                    break;
                                default:
                                    mass = dudeInfo[pSprite.getLotag() - kDudeBase].mass;
                                    break;
                            }
                        }
                        if (mass > 0) {
                            if (pVectorData.hitForce != 0) {
                                long impulse = divscale(pVectorData.hitForce, mass, 8);

                                pSprite.addVelocity(mulscale(impulse, dx, 16),
                                        mulscale(impulse, dy, 16),
                                        mulscale(impulse, dz, 16));
                            }
                        }

                        if (pXSprite != null && pVectorData.burn != 0) {
                            if (pXSprite.getBurnTime() == 0) {
                                evPostCallback(nSprite, 3, 0, 0);
                            }

                            pXSprite.setBurnSource(actSetBurnSource(pActor.getXvel()));
                            pXSprite.setBurnTime(ClipHigh(pXSprite.getBurnTime() + pVectorData.burn, 2400));
                        }

                        do {
                            if (!Chance(pVectorData.spawnBlood >> 1)) {
                                break;
                            }

                            maxDist = gVectorData[19].maxDist;
                            int hx = BiRandom2(4000) + dx;
                            int hy = BiRandom2(4000) + dy;
                            int hz = BiRandom2(4000) + dz;

                            if (HitScan(pSprite, pHitInfo.hitz, hx, hy, hz, pHitInfo, 16777280, maxDist) != 0) {
                                break;
                            }
                            if (EngineUtils.qdist(pHitInfo.hitx - pSprite.getX(), pHitInfo.hity - pSprite.getY()) > maxDist) {
                                break;
                            }
                            if (!IsNormalWall(pHitInfo.hitwall)) {
                                break;
                            }

                            int sx = pHitInfo.hitx - mulscale(16, hx, 14);
                            int sy = pHitInfo.hity - mulscale(16, hy, 14);
                            int sz = pHitInfo.hitz - mulscale(256, hz, 14);

                            int nEffect2 = gVectorData[19].impact[surfType[boardService.getWall(pHitInfo.hitwall).getPicnum()]].nEffect2;
                            int nEffect3 = gVectorData[19].impact[surfType[boardService.getWall(pHitInfo.hitwall).getPicnum()]].nEffect3;
                            BloodSprite pSpawn;
                            if (nEffect2 == -1 && nEffect3 == -1) {
                                break;
                            }

                            if (!Chance(0x2000)) {
                                pSpawn = actSpawnEffect(nEffect3, (short) pHitInfo.hitsect, sx, sy, sz, 0);
                            } else {
                                pSpawn = actSpawnEffect(nEffect2, (short) pHitInfo.hitsect, sx, sy, sz, 0);
                            }

                            if (pSpawn != null) {
                                pSpawn.setVelocityZ(8738);
                                pSpawn.setAng((short) ((GetWallAngle(pHitInfo.hitwall) + 512) & kAngleMask));
                                pSpawn.setCstat(pSpawn.getCstat() | kSpriteWall);
                            }
                        } while (false);

                        for (int i = 0; i < pVectorData.nBloodTrails; i++) {
                            if (Chance(pVectorData.nBloodChance >> 1)) {
                                actSpawnBlood(pSprite);
                            }
                        }
                    }
                    break;
                }
            }
        }

        if (pVectorData.impact[nSurf].nEffect2 >= 0) {
            actSpawnEffect(pVectorData.impact[nSurf].nEffect2, nSector, rx, ry, rz, 0);
        }
        if (pVectorData.impact[nSurf].nEffect3 >= 0) {
            actSpawnEffect(pVectorData.impact[nSurf].nEffect3, nSector, rx, ry, rz, 0);
        }
        if (pVectorData.impact[nSurf].nSoundId >= 0) {
            sfxCreate3DSound(rx, ry, rz, pVectorData.impact[nSurf].nSoundId, nSector);
        }
    }

    public static Sprite actFireThing(int nActor, int xOffset, int zOffset, int nSlope, int thingType, int velocity) {
        if (!(thingType >= kThingBase && thingType < kThingMax)) {
            throw new AssertException("thingType >= kThingBase && thingType < kThingMax");
        }

        BloodSprite pActor = boardService.getSprite(nActor);
        if (pActor == null) {
            return null;
        }

        int dx = mulscale(pActor.getClipdist(), Cos(pActor.getAng()), 28) + mulscale(Cos(pActor.getAng() + kAngle90), xOffset, 30);
        int dy = mulscale(pActor.getClipdist(), Sin(pActor.getAng()), 28) + mulscale(Sin(pActor.getAng() + kAngle90), xOffset, 30);

        int x = pActor.getX() + dx;
        int y = pActor.getY() + dy;
        int z = pActor.getZ() + zOffset;

        if (HitScan(pActor, z, dx, dy, 0, pHitInfo, CLIPMASK0, pActor.getClipdist()) != -1) {
            x = pHitInfo.hitx - mulscale((long) pActor.getClipdist() << 1, Cos(pActor.getAng()), 28);
            y = pHitInfo.hity - mulscale((long) pActor.getClipdist() << 1, Sin(pActor.getAng()), 28);
        }

        BloodSprite pThing = actSpawnThing(pActor.getSectnum(), x, y, z, thingType);
        actSetOwner(pThing, pActor);

        pThing.setAng(pActor.getAng());
        pThing.setVelocity(mulscale(velocity, Cos(pActor.getAng()), 30),
                mulscale(velocity, Sin(pActor.getAng()), 30),
                mulscale(velocity, nSlope, 14));
        pThing.addVelocity(pActor.getVelocityX() / 2,
                pActor.getVelocityY() / 2,
                pActor.getVelocityZ() / 2);
        return pThing;
    }

    public static void sfxPlayMissileSound(Sprite pSprite, int missileId) {
        MissileType pMissType = gMissileData[missileId - kMissileBase];
        if (Chance(0x4000)) {
            sfxStart3DSound(pSprite, pMissType.fireSound[0], -1, 0);
        } else {
            sfxStart3DSound(pSprite, pMissType.fireSound[1], -1, 0);
        }
    }

    public static void sfxPlayVectorSound(Sprite pSprite, int vectorId) {
        VECTORDATA pVectorData = gVectorData[vectorId];
        if (Chance(0x4000)) {
            sfxStart3DSound(pSprite, pVectorData.fireSound[0], -1, 0);
        } else {
            sfxStart3DSound(pSprite, pVectorData.fireSound[1], -1, 0);
        }
    }

    public static BloodSprite actFireMissile(BloodSprite pActor, int xoffset, int zoffset, int dx, int dy, int dz, int missileType) {
        boolean Impact = false;
        if (!(missileType >= kMissileBase && missileType < kMissileMax)) {
            throw new AssertException("missileType >= kMissileBase && missileType < kMissileMax");
        }
        MissileType pMissType = gMissileData[missileType - kMissileBase];

        int sx = pActor.getX() + mulscale(Cos(pActor.getAng()), (pActor.getClipdist() + pMissType.clipdist), 28) + mulscale(Cos(pActor.getAng() + kAngle90), xoffset, 30);
        int sy = pActor.getY() + mulscale(Sin(pActor.getAng()), (pActor.getClipdist() + pMissType.clipdist), 28) + mulscale(Sin(pActor.getAng() + kAngle90), xoffset, 30);
        int sz = pActor.getZ() + zoffset;

        int hitInfo = HitScan(pActor, sz, sx - pActor.getX(), sy - pActor.getY(), 0, pHitInfo, CLIPMASK0, pActor.getClipdist() + pMissType.clipdist);

        if (hitInfo != -1) {
            if (hitInfo != SS_SPRITE && hitInfo != SS_WALL) {
                sx = pHitInfo.hitx - mulscale(Cos(pActor.getAng()), (long) pMissType.clipdist << 1, 28);
                sy = pHitInfo.hity - mulscale(Sin(pActor.getAng()), (long) pMissType.clipdist << 1, 28);
            } else {
                Impact = true;
                sx = pHitInfo.hitx - mulscale(Cos(pActor.getAng()), 16, 30);
                sy = pHitInfo.hity - mulscale(Sin(pActor.getAng()), 16, 30);
            }
        }

        int nSprite = actSpawnSprite(pActor.getSectnum(), sx, sy, sz, kStatMissile, true);
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            return null;
        }

        show2dsprite.setBit(nSprite);

        pSprite.setLotag((short) missileType);
        pSprite.setShade((byte) pMissType.shade);
        pSprite.setPal(0);
        pSprite.setClipdist(pMissType.clipdist);
        pSprite.setHitag(kAttrMove);
        pSprite.setXrepeat((short) pMissType.xrepeat);
        pSprite.setYrepeat((short) pMissType.yrepeat);
        pSprite.setPicnum((short) pMissType.picnum);
        pSprite.setAng((short) ((pActor.getAng() + pMissType.angleOfs) & kAngleMask));

        pSprite.setVelocity(mulscale(dx, pMissType.velocity, 14),
            mulscale(dy, pMissType.velocity, 14),
            mulscale(dz, pMissType.velocity, 14));
        actSetOwner(pSprite, pActor);
        pSprite.setCstat(pSprite.getCstat() | 1);
        final int nXSprite = pSprite.getExtra();

        XSPRITE pXSprite = boardService.getXSprite(nXSprite);
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        pXSprite.setTarget(-1);

        evPostCallback(pSprite.getXvel(), SS_SPRITE, 600, 1);

        actBuildMissile(pSprite, nXSprite, pActor);

        if (Impact) {
            actImpactMissile(pSprite, hitInfo);
            pSprite = null;
        }
        return pSprite;
    }

    public static void actBuildMissile(BloodSprite pSprite, int nXSprite, BloodSprite pActor) {
        switch (pSprite.getLotag()) {
            case kMissileLifeLeech:
                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 0);
                break;
            case kMissileAltTesla:
                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 15);
                break;
            case kMissileGreenPuke:
                seqSpawn(29, SS_SPRITE, nXSprite, null);
                break;
            case kMissileButcherKnife:
                pSprite.setCstat(pSprite.getCstat() | 16);
                break;
            case kMissileTesla:
                sfxStart3DSound(pSprite, 251, 0, 0);
                break;
            case kMissileEctoSkull:
                seqSpawn(2, SS_SPRITE, nXSprite, null);
                sfxStart3DSound(pSprite, 493, 0, 0);
                break;
            case kMissileNapalm:
                seqSpawn(getSeq(kNapalm), SS_SPRITE, nXSprite, callbacks[SmokeCallback]);
                sfxStart3DSound(pSprite, 441, 0, 0);
                break;
            case kMissileFireball:
                seqSpawn(22, SS_SPRITE, nXSprite, callbacks[FireballCallback]);
                sfxStart3DSound(pSprite, 441, 0, 0);
                break;
            case kMissileHoundFire:
                seqSpawn(27, SS_SPRITE, nXSprite, null);
                pSprite.addVelocity((pActor.getVelocityX() / 2 + BiRandom(0x11111)),
                    (pActor.getVelocityY() / 2 + BiRandom(0x11111)),
                    (pActor.getVelocityZ() / 2 + BiRandom(0x11111)));
                break;
            case kMissileTchernobog:
                seqSpawn(getSeq(kNapalm), SS_SPRITE, nXSprite, callbacks[TchernobogCallback1]);
                sfxStart3DSound(pSprite, 441, 0, 0);
                break;
            case kMissileTchernobog2:
                seqSpawn(23, SS_SPRITE, nXSprite, callbacks[TchernobogCallback2]);
                pSprite.addVelocity((pActor.getVelocityX() / 2 + BiRandom(0x11111)),
                        (pActor.getVelocityY() / 2 + BiRandom(0x11111)),
                        (pActor.getVelocityZ() / 2 + BiRandom(0x11111)));break;
            case kMissileSprayFlame:
                if (Chance(0x4000)) {
                    seqSpawn(0, SS_SPRITE, nXSprite, null);
                } else {
                    seqSpawn(1, SS_SPRITE, nXSprite, null);
                }

                pSprite.addVelocity((pActor.getVelocityX() + BiRandom(0x11111)),
                        (pActor.getVelocityY() + BiRandom(0x11111)),
                        (pActor.getVelocityZ() + BiRandom(0x11111)));
                break;
            case kMissileStarburstFlare:
                evPostCallback(pSprite.getXvel(), SS_SPRITE, 30, 2);
                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 3);
                sfxStart3DSound(pSprite, 422, 0, 0);
                break;
            case kMissileFlare:
                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 3);
                sfxStart3DSound(pSprite, 422, 0, 0);
                break;
            case kMissileBone:
                sfxStart3DSound(pSprite, 252, 0, 0);
                break;
            case kMissileAltLeech2:
                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 7);
                break;
        }
    }

    public static void actTeslaImpact(Sprite pSprite, int hitInfo) {
        int x = pSprite.getX();
        int y = pSprite.getY();
        int z = pSprite.getZ();
        int nSector = pSprite.getSectnum();
        int nSource = actGetOwner(pSprite);

        gSectorExp[0] = -1;
        gWallExp[0] = -1;
        NearSectors(nSector, x, y, 300, gSectorExp, gSpriteExp, gWallExp);

        boolean shotDude = false;
        actHitInfo(hitInfo, pHitInfo);
        if (hitInfo == SS_SPRITE) {
            Sprite pHitSprite = boardService.getSprite(info_nSprite);
            if (pHitSprite != null && pHitSprite.getStatnum() == kStatDude) {
                shotDude = true;
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            final int nSprite = node.getIndex();
            if (nSprite != nSource || !shotDude) {
                final BloodSprite pDude = (BloodSprite) node.get();
                if ((pDude.getHitag() & kAttrFree) != 0) {
                    continue;
                }

                if ((gSpriteExp[pDude.getSectnum() >> 3] & (1 << (pDude.getSectnum() & 7))) != 0) {
                    if (CheckProximity(pDude, x, y, z, nSector, 300)) {
                        int dx = x - pDude.getX();
                        int dy = y - pDude.getY();

                        int nDamage = (300 - (EngineUtils.sqrt(dx * dx + dy * dy) >> 4) + 20) >> 1;
                        if (nDamage < 0) {
                            nDamage = 10;
                        }
                        if (nSprite == nSource) {
                            nDamage /= 2;
                        }

                        actDamageSprite(nSource, pDude, kDamageTesla, nDamage << 4);
                    }
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(kStatThing); node != null; node = node.getNext()) {
            final BloodSprite pThing = (BloodSprite) node.get();
            if ((pThing.getHitag() & kAttrFree) != 0) {
                continue;
            }

            if ((gSpriteExp[pThing.getSectnum() >> 3] & (1 << (pThing.getSectnum() & 7))) != 0) {
                if (CheckProximity(pThing, x, y, z, nSector, 300)) {
                    XSPRITE pXSprite = boardService.getXSprite(pThing.getExtra());
                    if (pXSprite != null && pXSprite.getLocked() == 0) {
                        int dx = x - pThing.getX();
                        int dy = y - pThing.getY();

                        int nDamage = 300 - (EngineUtils.sqrt(dx * dx + dy * dy) >> 4) + 20;
                        if (nDamage < 0) {
                            nDamage = 20;
                        }

                        actDamageSprite(nSource, pThing, kDamageTesla, nDamage << 4);
                    }
                }
            }
        }
    }

    public static void actImpactMissile(final Sprite pMissile, int hitInfo) {
        XSPRITE pXMissile = boardService.getXSprite(pMissile.getExtra());
        if (pXMissile == null) {
            throw new AssertException("pXMissile != null");
        }

        actHitInfo(hitInfo, pHitInfo);
        BloodSprite pSprite = info_pSprite, pHit;
        XSPRITE pXHit;
        Wall pWall = info_pWall;
        int nSprite = info_nSprite;
        int nWall = info_nWall;
        DudeInfo pDudeInfo = null;
        THINGINFO pThingInfo = null;
        if (hitInfo == SS_SPRITE && pSprite != null) {
            if (pSprite.getStatnum() == kStatThing) {
                pThingInfo = thingInfo[pSprite.getLotag() - kThingBase];
            } else if (pSprite.getStatnum() == kStatDude) {
                if (IsDudeSprite(pSprite)) {
                    pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
                } else {
                    pThingInfo = thingInfo[pSprite.getLotag() - kThingBase];
                }

            }
        }

        int maxtime;
        switch (pMissile.getLotag()) {
            case kMissileLifeLeech:
                if (hitInfo == 3 && pSprite != null && (pThingInfo != null || pDudeInfo != null)) {

                    int nDamage = 6;
                    if (IsOriginalDemo()) {
                        nDamage = 7;
                    }

                    actDamageSprite(actGetOwner(pMissile), pSprite, Random(nDamage), 16 * Random(7) + 112);
                    if (pThingInfo != null && pThingInfo.damageShift[1] != 0 || pDudeInfo != null && pDudeInfo.damageShift[1] != 0) {
                        XSPRITE pXSprite = pSprite.getXSprite();
                        if (pXSprite == null) {
                            throw new AssertException("pXSpriteHit != NULL");
                        }
                        
                        if (pSprite.getStatnum() == kStatDude) {
                            maxtime = 2400;
                        } else {
                            maxtime = 1200;
                        }
                        pXSprite.setBurnTime(ClipHigh(pXSprite.getBurnTime() + 360, maxtime));
                        pXSprite.setBurnSource(pMissile.getOwner());
                    }
                    // Deleted since v1.10
                    // if (hit != 0) {
                    // if (pDudeInfo != null) {
                    // int nSource = actGetBurnSource(pMissile.owner);
                    // if (nSource >= 0) {
                    // SPRITE pSource = boardService.getSprite(nSource);
                    // XSPRITE pXSource = null;
                    // if (pSource.extra > 0)
                    // pXSource = getXSprite()[pSource.extra];
                    // if (pSource.statnum == kStatDude && pXSource != null
                    // && pXSource.health != 0)
                    // actHealDude(pXSource, hit / 4, dudeInfo[pSource.lotag -
                    // kDudeBase].startHealth);
                    // }
                    // }
                    // }
                }

                actPostSprite(pMissile.getXvel(), kStatDefault);
                if (pMissile.getAng() == 1024) {
                    sfxStart3DSound(pMissile, 307, -1, 0);
                }
                pMissile.setLotag(0);
                seqSpawn(9, SS_SPRITE, pMissile.getExtra(), null);
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileAltTesla:
                actTeslaImpact(pMissile, hitInfo);
                if ((hitInfo == SS_WALL || hitInfo == SS_MASKED) && pWall != null) {
                    Sprite pEffect = actSpawnEffect(52, pMissile.getSectnum(), pMissile.getX(), pMissile.getY(), pMissile.getZ(), 0);
                    if (pEffect != null) {
                        pEffect.setAng((short) ((GetWallAngle(nWall) + kAngle90) & kAngleMask));
                    }
                }
                actGenerateGibs(pMissile, 24, null, null);
                actPostSprite(pMissile.getXvel(), kStatFree);
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileGreenPuke:
                seqKill(SS_SPRITE, pMissile.getExtra());
                if (hitInfo == SS_SPRITE && pSprite != null && (pThingInfo != null || pDudeInfo != null)) {
                    actDamageSprite(actGetOwner(pMissile), pSprite, kDamageBullet, 16 * Random(7) + 240);
                }
                actPostSprite(pMissile.getXvel(), kStatFree);
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileBone:
                sfxKill3DSound(pMissile, -1, -1);
                sfxCreate3DSound(pMissile.getX(), pMissile.getY(), pMissile.getZ(), 306, pMissile.getSectnum());
                actGenerateGibs(pMissile, 6, null, null);
                if (hitInfo == SS_SPRITE && pSprite != null && (pThingInfo != null || pDudeInfo != null)) {
                    actDamageSprite(actGetOwner(pMissile), pSprite, kDamageSpirit, 16 * Random(20) + 400);
                }
                actPostSprite(pMissile.getXvel(), kStatFree);
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileAltLeech1:
            case kMissileAltLeech2:
                sfxKill3DSound(pMissile, -1, -1);
                sfxCreate3DSound(pMissile.getX(), pMissile.getY(), pMissile.getZ(), 306, pMissile.getSectnum());
                if (hitInfo == SS_SPRITE && pSprite != null && (pThingInfo != null || pDudeInfo != null)) {
                    int damage = 3;
                    if (pMissile.getLotag() == kMissileAltLeech2) {
                        damage = 6;
                    }
                    actDamageSprite(actGetOwner(pMissile), pSprite, kDamageSpirit, 16 * (Random(damage) + damage));
                }
                actPostSprite(pMissile.getXvel(), kStatFree);
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileFireball:
            case kMissileNapalm:
                if (hitInfo == SS_SPRITE && pSprite != null && (pThingInfo != null || pDudeInfo != null)) {
                    XSPRITE pXSprite = pSprite.getXSprite();
                    if (pXSprite == null) {
                        throw new AssertException("pXSpriteHit != NULL");
                    }
                    
                    if (pThingInfo != null && pSprite.getLotag() == kThingTNTBarrel && pXSprite.getBurnTime() == 0) {
                        evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 0);
                    }
                    actDamageSprite(actGetOwner(pMissile), pSprite, kDamageBullet, 16 * Random(50) + 800);
                }
                actExplodeSprite(pMissile.getXvel());
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileStarburstFlare:
                sfxKill3DSound(pMissile, -1, -1);
                actExplodeSprite(pMissile.getXvel());
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileFlare:
                sfxKill3DSound(pMissile, -1, -1);
                if (hitInfo != SS_SPRITE || pSprite == null) {
                    actGenerateGibs(pMissile, 17, null, null);
                    actPostSprite(pMissile.getXvel(), kStatFree);
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                    return;
                }

                if (pThingInfo != null || pDudeInfo != null) {
                    if (pThingInfo != null && pThingInfo.damageShift[1] != 0 || pDudeInfo != null && pDudeInfo.damageShift[1] != 0) {
                        XSPRITE pXSprite = pSprite.getXSprite();
                        if (pXSprite == null) {
                            throw new AssertException("pXSpriteHit != NULL");
                        }
                        
                        if (pThingInfo != null && pSprite.getLotag() == kThingTNTBarrel && pXSprite.getBurnTime() == 0) {
                            evPostCallback(nSprite, 3, 0, 0);
                        }

                        if (pSprite.getStatnum() == kStatDude) {
                            maxtime = 2400;
                        } else {
                            maxtime = 1200;
                        }

                        pXSprite.setBurnTime(ClipHigh(pXSprite.getBurnTime() + 480, maxtime));
                        pXSprite.setBurnSource(pMissile.getOwner());

                        actDistanceDamage(actGetOwner(pMissile), pMissile.getX(), pMissile.getY(), pMissile.getZ(), pMissile.getSectnum(), 16, 20, 2, 10, 6, 480);
                    } else {
                        actDamageSprite(actGetOwner(pMissile), pSprite, kDamageBullet, 16 * Random(10) + 320);
                    }
                }

                if (game.getCurrentDef().surfType[pSprite.getPicnum()] == 4) {
                    pMissile.setPicnum(2123);
                    pXMissile.setTarget(nSprite);
                    pXMissile.setTargetZ(pMissile.getZ() - pSprite.getZ());
                    int nAngle = EngineUtils.getAngle(pMissile.getX() - pSprite.getX(), pMissile.getY() - pSprite.getY());
                    pXMissile.setGoalAng((short) ((nAngle - pSprite.getAng()) & 0x7FF));
                    pXMissile.setState(1);
                    actPostSprite(pMissile.getXvel(), kStatFlare);
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                } else {
                    actGenerateGibs(pMissile, 17, null, null);
                    actPostSprite(pMissile.getXvel(), kStatFree);
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                }
                break;
            case kMissileSprayFlame:
            case kMissileHoundFire:
                if (hitInfo != SS_SPRITE) {
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                    return;
                }
                pHit = boardService.getSprite(pHitInfo.hitsprite);
                if (pHit == null) {
                    throw new AssertException("pHit != null");
                }
                
                pXHit = boardService.getXSprite(pHit.getExtra());
                if (pXHit == null) {
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                    return;
                }

                if ((pHit.getStatnum() == kStatThing || pHit.getStatnum() == kStatDude) && pXHit.getBurnTime() == 0) {
                    evPostCallback(pHitInfo.hitsprite, SS_SPRITE, 0, 0);
                }

                if (pHit.getStatnum() == kStatDude) {
                    maxtime = 2400;
                } else {
                    maxtime = 1200;
                }
                pXHit.setBurnTime(ClipHigh(pXHit.getBurnTime() + (4 * pGameInfo.nDifficulty + 16), maxtime));
                pXHit.setBurnSource(pMissile.getOwner());
                actDamageSprite(actGetOwner(pMissile), pHit, kDamageBurn, 8);
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileTchernobog:
                actExplodeSprite(pMissile.getXvel());
                if (hitInfo == SS_SPRITE) {
                    pHit = boardService.getSprite(pHitInfo.hitsprite);
                    if (pHit == null) {
                        throw new AssertException("pHit != null");
                    }
                    
                    pXHit = boardService.getXSprite(pHit.getExtra());
                    if (pXHit != null) {
                        if ((pHit.getStatnum() == kStatThing || pHit.getStatnum() == kStatDude) && pXHit.getBurnTime() == 0) {
                            evPostCallback(pHitInfo.hitsprite, SS_SPRITE, 0, 0);
                        }

                        if (pHit.getStatnum() == kStatDude) {
                            maxtime = 2400;
                        } else {
                            maxtime = 1200;
                        }
                        pXHit.setBurnTime(ClipHigh(pXHit.getBurnTime() + (4 * pGameInfo.nDifficulty + 16), maxtime));
                        pXHit.setBurnSource(pMissile.getOwner());
                        actDamageSprite(actGetOwner(pMissile), pHit, kDamageBurn, 8);
                        actDamageSprite(actGetOwner(pMissile), pHit, kDamageBullet, 16 * Random(10) + 400);
                    }
                }
                actExplodeSprite(pMissile.getXvel());
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileTchernobog2:
                actExplodeSprite(pMissile.getXvel());
                if (hitInfo == SS_SPRITE) {
                    pHit = boardService.getSprite(pHitInfo.hitsprite);
                    if (pHit == null) {
                        throw new AssertException("pHit != null");
                    }
                    
                    pXHit = boardService.getXSprite(pHit.getExtra());
                    if (pXHit != null) {
                        if ((pHit.getStatnum() == kStatThing || pHit.getStatnum() == kStatDude) && pXHit.getBurnTime() == 0) {
                            evPostCallback(pHitInfo.hitsprite, SS_SPRITE, 0, 0);
                        }

                        if (pHit.getStatnum() == kStatDude) {
                            maxtime = 2400;
                        } else {
                            maxtime = 1200;
                        }
                        pXHit.setBurnTime(ClipHigh(pXHit.getBurnTime() + 32, maxtime));
                        pXHit.setBurnSource(pMissile.getOwner());
                        actDamageSprite(actGetOwner(pMissile), pHit, kDamageSpirit, 12);
                        actDamageSprite(actGetOwner(pMissile), pHit, kDamageBullet, 16 * Random(10) + 400);
                    }
                }
                actExplodeSprite(pMissile.getXvel());
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileEctoSkull:
                sfxKill3DSound(pMissile, -1, -1);
                sfxCreate3DSound(pMissile.getX(), pMissile.getY(), pMissile.getZ(), 522, pMissile.getSectnum());
                actPostSprite(pMissile.getXvel(), kStatDebris);
                seqSpawn(20, 3, pMissile.getExtra(), null);
                if (hitInfo != SS_SPRITE) {
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                    return;
                }
                pHit = boardService.getSprite(pHitInfo.hitsprite);
                if (pHit == null) {
                    throw new AssertException("pHit != null");
                }

                if (pHit.getStatnum() != kStatDude) {
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                    return;
                }
                actDamageSprite(actGetOwner(pMissile), pHit, kDamageSpirit, 16 * Random(10) + 400);
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileButcherKnife:
                actPostSprite(pMissile.getXvel(), kStatDebris);
                pMissile.setCstat(pMissile.getCstat() & ~kSpriteWall);
                pMissile.setLotag(0);
                seqSpawn(20, SS_SPRITE, pMissile.getExtra(), null);
                if (hitInfo != SS_SPRITE) {
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                    return;
                }
                pHit = boardService.getSprite(pHitInfo.hitsprite);
                if (pHit == null) {
                    throw new AssertException("pHit != null");
                }

                if (pHit.getStatnum() != kStatDude) {
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                    return;
                }
                actDamageSprite(actGetOwner(pMissile), pHit, kDamageSpirit, 16 * Random(10) + 160);

                int nOwner = actGetOwner(pMissile);
                if (IsOriginalDemo()) {
                    pHit = boardService.getSprite(nOwner);
                    if (pHit != null) {
                        pXHit = boardService.getXSprite(pHit.getExtra());
                        if (pXHit.getHealth() != 0) {
                            actHealDude(pXHit, 10, dudeInfo[pHit.getLotag() - kDudeBase].startHealth);
                        }
                    }
                } else {
                    pHit = boardService.getSprite(nOwner);
                    if (pHit != null) {
                        pXHit = boardService.getXSprite(pHit.getExtra());
                        if (pXHit != null && pXHit.getHealth() != 0 && IsDudeSprite(pHit)) {
                            int lotag = pHit.getLotag() - kDudeBase;
                            if (lotag >= 200) {
                                actHealDude(pXHit, 10, dudeInfo[lotag].startHealth);
                            }
                        }
                    }
                }
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            case kMissileTesla:
                sfxKill3DSound(pMissile, -1, -1);
                sfxCreate3DSound(pMissile.getX(), pMissile.getY(), pMissile.getZ(), 518, pMissile.getSectnum());
                if (hitInfo == 2) {
                    actGenerateGibs(pMissile, 23, null, null);
                } else {
                    actGenerateGibs(pMissile, 22, null, null);
                }

                checkEventList(pMissile.getXvel(), SS_SPRITE);
                seqKill(SS_SPRITE, pMissile.getExtra());
                actPostSprite(pMissile.getXvel(), kStatFree);
                if (hitInfo != SS_SPRITE) {
                    pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                    return;
                }

                pHit = boardService.getSprite(pHitInfo.hitsprite);
                if (pHit == null) {
                    throw new AssertException("pHit != null");
                }
                actDamageSprite(actGetOwner(pMissile), pHit, kDamageTesla, 16 * Random(10) + 240);
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
            default:
                seqKill(SS_SPRITE, pMissile.getExtra());
                actPostSprite(pMissile.getXvel(), kStatFree);
                if (hitInfo == SS_SPRITE) {
                    pHit = boardService.getSprite(pHitInfo.hitsprite);
                    if (pHit == null) {
                        throw new AssertException("pHit != null");
                    }
                    actDamageSprite(actGetOwner(pMissile), pHit, kDamageFall, 16 * Random(10) + 160);
                }
                pMissile.setCstat(pMissile.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                break;
        }
    }

    public static void actHitInfo(int hitType, Hitscan pHitInfo) {
        if (pHitInfo == null) {
            throw new AssertException("pHitInfo != NULL");
        }

        info_nSprite = -1;
        info_pSprite = null;
        info_nWall = -1;
        info_pWall = null;
        info_pXWall = null;
        info_nSector = -1;
        info_pSector = null;
        info_pXSector = null;

        switch (hitType) {
            case SS_SPRITE:
            case 5:
                info_nSprite = pHitInfo.hitsprite;
                if (!boardService.isValidSprite(info_nSprite)) {
                    throw new AssertException("isValidSprite(info_nSprite)");
                }
                info_pSprite = boardService.getSprite(info_nSprite);
                break;
            case SS_WALL:
            case SS_MASKED:
                info_nWall = pHitInfo.hitwall;
                if (!boardService.isValidWall(info_nWall)) {
                    throw new AssertException("boardService.isValidWall(nWall)");
                }
                info_pWall = boardService.getWall(info_nWall);
                if (info_pWall.getExtra() > 0) {
                    info_pXWall = xwall[info_pWall.getExtra()];
                }
                break;
            case SS_SECTOR:
            case SS_FLOOR:
            case SS_CEILING:
                info_nSector = pHitInfo.hitsect;
                if (!boardService.isValidSector(info_nSector)) {
                    throw new AssertException("boardService.isValidSector(nSector)");
                }
                info_pSector = boardService.getSector(info_nSector);
                if (info_pSector.getExtra() > 0) {
                    info_pXSector = xsector[info_pSector.getExtra()];
                }
                break;
            default:
                break;
        }
    }

    public static void MoveThing(BloodSprite pSprite) {
        final XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        if (!(pSprite.getLotag() >= kThingBase && pSprite.getLotag() < kThingMax)) {
            throw new AssertException("pSprite.type >= kThingBase && pSprite.type < kThingMax");
        }
        THINGINFO pThinkInfo = thingInfo[pSprite.getLotag() - kThingBase];
        int nSector = pSprite.getSectnum();
        if (!boardService.isValidSector(nSector)) {
            throw new AssertException("boardService.isValidSector(nSector)");
        }

        int zTop, zBot;
        GetSpriteExtents(pSprite);
        zTop = extents_zTop;
        zBot = extents_zBot;

        final SPRITEHIT pSpriteHit = pXSprite.getSpriteHit();
        int moveHit = 0;
        int floorDist = (zBot - pSprite.getZ()) / 4;
        int ceilDist = (pSprite.getZ() - zTop) / 4;

        int clipDist = pSprite.getClipdist() << 2;

        if (pSprite.getVelocityX() != 0 || pSprite.getVelocityY() != 0) {

            short oldcstat = pSprite.getCstat();
            pSprite.setCstat(pSprite.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));

            moveHit = ClipMove(pSprite.getX(), pSprite.getY(), pSprite.getZ(), (short) nSector, pSprite.getVelocityX() >> 12, pSprite.getVelocityY() >> 12, clipDist, ceilDist, floorDist, CLIPMASK0);

            pSpriteHit.moveHit = moveHit;

            pSprite.setX(clipm_px);
            pSprite.setY(clipm_py);
            pSprite.setZ(clipm_pz);
            nSector = (short) clipm_pnsectnum;

            pSprite.setCstat(oldcstat);
            if (nSector < 0) {
                throw new AssertException("nSector >= 0");
            }

            if (pSprite.getSectnum() != nSector) {
                engine.changespritesect(pSprite.getXvel(), (short) nSector);
            }

            int nHitObject = moveHit & kHitTypeMask;
            if (nHitObject == kHitWall) {
                int nWall = moveHit & kHitIndexMask;
                ReflectVector(pSprite.getVelocityX(), pSprite.getVelocityY(), nWall, pThinkInfo.nFraction);

                pSprite.setVelocityX(refl_x);
                pSprite.setVelocityY(refl_y);
                if (pSprite.getLotag() == kThingPail) {
                    sfxStart3DSound(pSprite, 374, 0, 0);
                } else if (pSprite.getLotag() == kThingZombieHead) {
                    sfxStart3DSound(pSprite, 607, 0, 0);
                    actDamageSprite(-1, pSprite, kDamageFall, 80);
                }
            }
        } else {
            if (!boardService.isValidSector(nSector)) {
                throw new AssertException("boardService.isValidSector(nSector)");
            }
            FindSector(pSprite.getX(), pSprite.getY(), pSprite.getZ(), nSector);
            nSector = foundSector;
        }

        if (pSprite.getVelocityZ() != 0) {
            pSprite.setZ((int) (pSprite.getZ() + (pSprite.getVelocityZ() >> 8)));
        }

        int ceilz, ceilhit, floorz, floorhit;
        GetZRange(pSprite, clipDist, CLIPMASK0);
        ceilz = gz_ceilZ;
        ceilhit = gz_ceilHit;
        floorz = gz_floorZ;
        floorhit = gz_floorHit;

        GetSpriteExtents(pSprite);
        zBot = extents_zBot;
        if ((pSprite.getHitag() & kAttrGravity) != 0) {
            if (zBot < floorz) {
                pSprite.setZ(pSprite.getZ() + 455);
                pSprite.addVelocityZ(58254);
                if (pSprite.getLotag() == kThingZombieHead) {
                    BloodSprite pSpawn = actSpawnEffect(27, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
                    if (pSpawn != null) {

                        int xvel = 279620;
                        int yvel = 0;
                        int zvel = 0;
                        RotateVector(xvel, yvel, 11 * gFrameClock & 0x7FF);
                        xvel = (int) rotated.x;
                        yvel = (int) rotated.y;
                        RotateVector(xvel, zvel, 5 * gFrameClock & 0x7FF);
                        xvel = (int) rotated.x;
                        zvel = (int) rotated.y;
                        RotateVector(yvel, zvel, 3 * gFrameClock & 0x7FF);
                        yvel = (int) rotated.x;
                        zvel = (int) rotated.y;

                        pSpawn.setVelocity(xvel + pSprite.getVelocityX(),
                            yvel + pSprite.getVelocityY(),
                            zvel + pSprite.getVelocityZ());
                    }
                }
            }
        }

        int warp = checkWarping(pSprite);
        if (warp != 0) {
            GetZRange(pSprite, clipDist, CLIPMASK0);
            ceilz = gz_ceilZ;
            ceilhit = gz_ceilHit;
            floorz = gz_floorZ;
            floorhit = gz_floorHit;
        }

        GetSpriteExtents(pSprite);
        zTop = extents_zTop;
        zBot = extents_zBot;

        if (!IsOriginalDemo() && (pSprite.getLotag() == kThingTNTBundle || pSprite.getLotag() == kThingSprayBundle)) { //bridge sprite hack
            if ((floorhit & kHitTypeMask) == kHitSprite) {
                Sprite pFloorSprite = boardService.getSprite(floorhit & kHitIndexMask);
                if (pFloorSprite != null && (pFloorSprite.getCstat() & kSpriteRMask) == kSpriteFloor) {
                    if (klabs(zBot - floorz) < 1024) {
                        floorz -= 1024;
                    }
                }
            }
        }

        if (floorz > zBot) {
            pSpriteHit.floorHit = 0;
            if ((pSprite.getHitag() & kAttrGravity) != 0) {
                pSprite.setHitag(pSprite.getHitag() | kAttrFalling);
            }
        } else { // hit floor?
            processTouchFloor(pSprite, pSprite.getSectnum());

            pSpriteHit.floorHit = floorhit;
            pSprite.setZ(pSprite.getZ() + (floorz - zBot));

            int dZvel = (int) (pSprite.getVelocityZ() - floorVel[pSprite.getSectnum()]);
            if (dZvel <= 0) {
                if (pSprite.getVelocityZ() == 0) {
                    pSprite.setHitag(pSprite.getHitag() & ~kAttrFalling);
                }
            } else {
                pSprite.setHitag(pSprite.getHitag() | kAttrFalling);
                long gravity = GravityVector(pSprite.getVelocityX(), pSprite.getVelocityY(), dZvel, pSprite.getSectnum(), pThinkInfo.nFraction);
                int fallDamage = mulscale(gravity, gravity, 30) - pThinkInfo.fallDamage;
                pSprite.setVelocityX(refl_x);
                pSprite.setVelocityY(refl_y);
                dZvel = (int) refl_z;

                if (fallDamage > 0) {
                    actDamageSprite(pSprite.getXvel(), pSprite, kDamageFall, fallDamage);
                }

                pSprite.setVelocityZ(dZvel);
                if (floorVel[pSprite.getSectnum()] == 0) {
                    if (klabs(dZvel) < 65536) {
                        pSprite.setVelocityZ(0);
                        pSprite.setHitag(pSprite.getHitag() & ~kAttrFalling);
                    }
                }

                switch (pSprite.getLotag()) {
                    case kThingAltNapalm:
                        if (pSprite.getVelocityZ() == 0 || Chance(0x5000)) {
                            actNapalm2Explode(pSprite, pXSprite);
                        }
                        break;
                    case kThingPail:
                        if (klabs(pSprite.getVelocityZ()) > 524288) {
                            sfxStart3DSound(pSprite, 374, 0, 0);
                        }
                        break;
                    case kThingZombieHead:
                        if (klabs(pSprite.getVelocityZ()) > 524288) {
                            sfxStart3DSound(pSprite, 607, 0, 0);
                            actDamageSprite(-1, pSprite, kDamageFall, 80);
                        }
                        break;
                }
                moveHit = (nSector | kHitSector);
            }
        }
        if (ceilz < zTop) {
            pSpriteHit.ceilHit = 0;
        } else // hit ceiling
        {
            pSpriteHit.ceilHit = ceilhit;
            if (ceilz - zTop > 0) {
                pSprite.setZ(pSprite.getZ() + ceilz - zTop);
            }

            if (pSprite.getVelocityZ() < 0) {
                pSprite.setVelocity(mulscale(49152, pSprite.getVelocityX(), 16),
                mulscale(49152, pSprite.getVelocityY(), 16),
                mulscale(16384, -pSprite.getVelocityZ(), 16));

                switch (pSprite.getLotag()) {
                    case kThingPail:
                        if (klabs(pSprite.getVelocityZ()) > 524288) {
                            sfxStart3DSound(pSprite, 374, 0, 0);
                        }
                        break;
                    case kThingZombieHead:
                        if (klabs(pSprite.getVelocityZ()) > 524288) {
                            sfxStart3DSound(pSprite, 607, 0, 0);
                            actDamageSprite(-1, pSprite, kDamageFall, 80);
                        }
                        break;
                }
            }
        }

        if (zBot >= floorz) {
            int moveDist = EngineUtils.qdist(pSprite.getVelocityX(), pSprite.getVelocityY());
            int oldDist = moveDist;
            if (moveDist > 0x11111) {
                moveDist = 0x11111;
            }

            if ((floorhit & kHitTypeMask) == kHitSprite) {
                int nUnderSprite = floorhit & kHitIndexMask;
                Sprite pUnderSprite = boardService.getSprite(nUnderSprite);

                if (pUnderSprite != null && (pUnderSprite.getCstat() & kSpriteRMask) == kSpriteFace) {
                    // push it off the face sprite
                    pSprite.addVelocityX(mulscale(kFrameTicks, pSprite.getX() - pUnderSprite.getX(), 2));
                    pSprite.addVelocityY(mulscale(kFrameTicks, pSprite.getY() - pUnderSprite.getY(), 2));
                    moveHit = pSpriteHit.moveHit;
                }
            }
            if (moveDist > 0) {
                int kv = divscale(moveDist, oldDist, 16);
                pSprite.addVelocityX(-mulscale(pSprite.getVelocityX(), kv, 16));
                pSprite.addVelocityY(-mulscale(pSprite.getVelocityY(), kv, 16));
            }
        }

        if (pSprite.getVelocityX() != 0 || pSprite.getVelocityY() != 0) {
            pSprite.setAng(EngineUtils.getAngle((int) pSprite.getVelocityX(), (int) pSprite.getVelocityY()));
        }

        if (moveHit != 0) {
            if (pXSprite.isImpact()) {
                trTriggerSprite(pSprite.getXvel(), pXSprite, 0);
            }

            switch (pSprite.getLotag()) {
                case kGDXThingThrowableRock:
                    seqSpawn(24, SS_SPRITE, pSprite.getExtra(), null);
                    if ((moveHit & kHitTypeMask) == kHitSprite) {
                        pSprite.setXrepeat(32);
                        pSprite.setYrepeat(32);
                        BloodSprite pObject = boardService.getSprite(moveHit & kHitIndexMask);
                        if (pObject == null) {
                            throw new AssertException("pObject != null");
                        }
                        actDamageSprite(actGetOwner(pSprite), pObject, kDamageFall, pXSprite.getData1());
                    }
                    break;
                case kThingBoneClub:
                    seqSpawn(24, SS_SPRITE, pSprite.getExtra(), null);
                    if ((moveHit & kHitTypeMask) == kHitSprite) {
                        BloodSprite pObject = boardService.getSprite(moveHit & kHitIndexMask);
                        if (pObject == null) {
                            throw new AssertException("pObject != null");
                        }
                        actDamageSprite(actGetOwner(pSprite), pObject, kDamageFall, 12);
                    }
                    break;
                case kThingWaterDrip:
                case kThingBloodDrip:
                    pSprite.setZ(pSprite.getZ() - 1024);
                    pSprite.setHitag(pSprite.getHitag() & ~kAttrGravity);
                    int surfType = game.getCurrentDef().GetSurfType(pSpriteHit.floorHit);
                    if (pSprite.getLotag() == kThingWaterDrip) {
                        if (surfType == 5) {
                            seqSpawn(6, SS_SPRITE, pSprite.getExtra(), null);
                            sfxStart3DSound(pSprite, 356, -1, 0);
                        } else {
                            seqSpawn(7, SS_SPRITE, pSprite.getExtra(), null);
                            sfxStart3DSound(pSprite, 354, -1, 0);
                        }
                    }
                    if (pSprite.getLotag() == kThingBloodDrip) {
                        seqSpawn(8, SS_SPRITE, pSprite.getExtra(), null);
                        sfxStart3DSound(pSprite, 354, -1, 0);// .DRIPSURF
                    }
                    break;
                case kThingPodFire:
                    actExplodeSprite(pSprite.getXvel());
                    break;
                case kThingPodGreen:
                    if ((moveHit & kHitTypeMask) == kHitFloor) {
                        actDistanceDamage(actGetOwner(pSprite), pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), 200, 1, kDamageExplode, 20, 6, 0);
                    } else if (IsOriginalDemo() || (moveHit & kHitTypeMask) == kHitSprite) {
                        BloodSprite pObject = boardService.getSprite(moveHit & kHitIndexMask);
                        if (pObject != null) {
                            actDamageSprite(actGetOwner(pSprite), pObject, kDamageFall, 12);
                        }
                    }
                    evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 19);
                    break;
            }
        }
    }

    public static void actSetOwner(Sprite pTarget, Sprite pSource) {
        int killerid;
        if (!(pTarget != null && pSource != null)) {
            throw new AssertException("pTarget != NULL && pSource != NULL");
        }
        if (IsPlayerSprite(pSource)) {
            killerid = (pSource.getLotag() - kDudePlayer1) | 0x1000;
        } else {
            killerid = pSource.getXvel();
        }
        pTarget.setOwner((short) killerid);

    }

    public static int actGetOwner(Sprite pSprite) {
        if (pSprite == null) {
            throw new AssertException("pSprite != null");
        }
        if (pSprite.getOwner() == -1) {
            return -1;
        }

        int result = (pSprite.getOwner()) & 0xFFF;
        if ((pSprite.getOwner() & 0x1000) != 0) {
            result = gPlayer[result].pSprite.getXvel();
        }

        return result;
    }

    public static int actDamageSprite(int nSource, BloodSprite pSprite, int nDamageType, int nDamage) {
        int nShift;
        if ((pSprite.getHitag() & kAttrFree) != 0) {
            return 0;
        }

        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            return 0;
        }

        if (pXSprite.getReference() != pSprite.getXvel()) {
            throw new AssertException("pXSprite.reference == pSprite.index");
        }

        if (pXSprite.getHealth() == 0 && pSprite.getStatnum() != kStatDude || pXSprite.getLocked() != 0) {
            return 0;
        }

        if (nSource == -1) {
            nSource = pSprite.getXvel();
        }

        PLAYER pPlayer = null;
        Sprite pSource = boardService.getSprite(nSource);
        if (pSource == null) {
            throw new AssertException("pSource != null");
        }

        if (IsPlayerSprite(pSource)) {
            pPlayer = gPlayer[pSource.getLotag() - kDudePlayer1];
        }

        switch (pSprite.getStatnum()) {
            case kStatThing:
                if (!(pSprite.getLotag() >= kThingBase && pSprite.getLotag() < kThingMax)) {
                    throw new AssertException("pSprite.type >= kThingBase && pSprite.type < kThingMax");
                }

                nShift = thingInfo[pSprite.getLotag() - kThingBase].damageShift[nDamageType];
                if (nShift == 0) {
                    return 0;
                }
                if (nShift != 256) {
                    nDamage = mulscale(nShift, nDamage, 8);
                }

                pXSprite.setHealth(ClipLow(pXSprite.getHealth() - nDamage, 0));

                if (pXSprite.getHealth() != 0) {
                    return nDamage >> 4;
                }

                if (pSprite.getLotag() == kThingLifeLeech/* || pSprite.lotag == kGDXThingCustomDudeLifeLeech*/) {
                    actGenerateGibs(pSprite, 14, null, null);
                    pXSprite.setData1(0);
                    pXSprite.setData2(0);
                    pXSprite.setData3(0);
                    pXSprite.setData4(0);
                    pXSprite.setStateTimer(0);
                    pXSprite.setTriggered(false);
                    pXSprite.setDudeLockout(false);

//				if (pSprite.owner >= 0 && boardService.getSprite(pSprite.owner).lotag == kGDXDudeUniversalCultist){
//					boardService.getSprite(pSprite.owner).owner = 32666; // indicates if custom dude had life leech.
//				}
                } else if ((pSprite.getHitag() & kAttrRespawn) == 0) {
                    actSetOwner(pSprite, boardService.getSprite(nSource));
                }

                trTriggerSprite(pSprite.getXvel(), pXSprite, kCommandOff);

                switch (pSprite.getLotag()) {
                    case kThingFluorescent:
                        seqSpawn(12, SS_SPRITE, pSprite.getExtra(), null);
                        actGenerateGibs(pSprite, 6, null, null);
                        break;
                    case kThingWeb:
                        seqSpawn(15, SS_SPRITE, pSprite.getExtra(), null);
                        break;
                    case kThingMetalGrate1:
                        seqSpawn(21, SS_SPRITE, pSprite.getExtra(), null);
                        actGenerateGibs(pSprite, 4, null, null);
                        break;
                    case kThingFlammableTree:
                        switch (pXSprite.getData1()) {
                            case 0:
                                seqSpawn(25, SS_SPRITE, pSprite.getExtra(), callbacks[DamageTreeCallback]);
                                sfxStart3DSound(pSprite, 351, -1, 0);
                                break;
                            case 1:
                                seqSpawn(26, SS_SPRITE, pSprite.getExtra(), callbacks[DamageTreeCallback]);
                                sfxStart3DSound(pSprite, 351, -1, 0);
                                break;
                            case -1:
                                actGenerateGibs(pSprite, 14, null, null);
                                sfxCreate3DSound(pSprite.getX(), pSprite.getY(), pSprite.getZ(), 312, pSprite.getSectnum());
                                actPostSprite(pSprite.getXvel(), kStatFree);
                                break;
                        }
                        break;
                    case kThingMachineGun:
                        seqSpawn(28, SS_SPRITE, pSprite.getExtra(), null);
                        break;
                    case 422:
                        if (seqFrame(SS_SPRITE, pSprite.getExtra()) < 0) {
                            seqSpawn(19, 3, pSprite.getExtra(), null);
                        }
                        break;
                    case kThingGibObject:
                    case kThingExplodeObject:
                    case kThingGibSmall:
                    case kThingGib:
                    case kThingZombieHead:
                        if (nDamageType == kDamageExplode && pPlayer != null && gFrameClock > pPlayer.pLaughsCount && Chance(0x2000)) {
                            int sndId = gLaughs[Random(10)];
                            if (gPlayer[gViewIndex] == pPlayer) {
                                sndStartSample(sndId, 255, 1, false);
                            } else {
                                sfxStart3DSound(pPlayer.pSprite, sndId, 0, 0);
                            }
                            pPlayer.pLaughsCount = gFrameClock + 3600;
                        }
                        break;
                }

                return nDamage >> 4;
            case kStatDude:
                if (!IsDudeSprite(pSprite)) {
                    return 0;
                }

                if (IsPlayerSprite(pSprite) && (!IsDudeSprite(pSource) || pSprite.getXvel() == nSource)) {
                    nShift = mulscale(gPlayer[pSprite.getLotag() - kDudePlayer1].pDudeInfo.startDamage[nDamageType], pPlayerShift[pGameInfo.nDifficulty], 8);
                } else {
                    nShift = dudeInfo[pSprite.getLotag() - kDudeBase].damageShift[nDamageType];
                }

                if ((pGameInfo.nGameType == kNetModeCoop || pGameInfo.nGameType == kNetModeTeams) && IsPlayerSprite(pSprite) && pPlayer != null && pSprite.getXvel() != nSource) {

                    if (pGameInfo.nGameType == kNetModeCoop) {
                        nShift = pGameInfo.nFriendlyFire;
                    } else {
                        if (pPlayer.teamID == gPlayer[pSprite.getLotag() - kDudePlayer1].teamID) {
                            nShift = pGameInfo.nFriendlyFire;
                        }
                    }
                }

                if (nShift == 0) {
                    return 0;
                }

                if (nShift != 256) {
                    nDamage = mulscale(nShift, nDamage, 8);
                }

                if (IsPlayerSprite(pSprite)) {
                    pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];

                    if (pXSprite.getHealth() != 0 || checkPlayerSeq(pPlayer, getPlayerSeq(kPlayerFatality))) {
                        playerDamageSprite(pPlayer, nSource, nDamageType, nDamage);
                        return nDamage >> 4;
                    }
                    return nDamage >> 4;
                }
                if (pXSprite.getHealth() == 0) {
                    return 0;
                }

                nDamage = aiDamageSprite(pSprite, pXSprite, nSource, nDamageType, nDamage);
                if (pXSprite.getHealth() != 0) {
                    return nDamage >> 4;
                }

                if (nDamageType == kDamageExplode && nDamage < 160) {
                    nDamageType = kDamageFall;
                }

                actKillSprite(nSource, pSprite, nDamageType, nDamage);
                return nDamage >> 4;
        }
        return 0;
    }
    // concussion

    public static void actKillSprite(int nSource, BloodSprite pSprite, int nDamageType, int nDamage) {
        if (!IsDudeSprite(pSprite)) {
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        }

        int dudeIndex = pSprite.getLotag() - kDudeBase;
        DudeInfo pDudeInfo = dudeInfo[dudeIndex];

        int nSprite = pSprite.getXvel();
        final int nXSprite = pSprite.getExtra();
        XSPRITE pXSprite = boardService.getXSprite(nXSprite);
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }
        DudeExtra dudeExtra = pXSprite.getDudeExtra();

        // handle first cerberus head death
        if (pSprite.getLotag() == kDudeCerberus) {
            seqSpawn(pDudeInfo.seqStartID + kSeqDudeDeath1, SS_SPRITE, nXSprite, null);
            return;
        }

        if (pSprite.getLotag() == kDudeTommyCultist || pSprite.getLotag() == kDudeShotgunCultist) {
            if (nDamageType == kDamageBurn) {
                if (pXSprite.getPalette() == 0) {
                    pSprite.setLotag(kDudeCultistBurning);
                    aiNewState(pSprite, pXSprite, burnGoto[CULTIST]);
                    actHealDude(pXSprite, dudeInfo[40].startHealth, dudeInfo[40].startHealth);
                    if (!IsOriginalDemo()) {
                        if (pXSprite.getBurnTime() == 0) {
                            pXSprite.setBurnTime(1200);
                        }
                    }
                    dudeExtra.setClock(gFrameClock + 360);
                    return;
                } else {
                    nDamageType = kDamageFall;
                }
            }
        }

        if (pSprite.getLotag() == kDudeTheBeast) {
            if (nDamageType == kDamageBurn && pXSprite.getPalette() == 0) {
                pSprite.setLotag(kDudeTheBeastburning);
                aiNewState(pSprite, pXSprite, burnGoto[BEAST]);
                actHealDude(pXSprite, dudeInfo[53].startHealth, dudeInfo[53].startHealth);
                if (!IsOriginalDemo()) {
                    if (pXSprite.getBurnTime() == 0) {
                        pXSprite.setBurnTime(1200);
                    }
                }
                dudeExtra.setClock(gFrameClock + 360);
                return;
            }
        }

        if (pSprite.getLotag() == kDudeInnocent) {
            if (nDamageType == kDamageBurn && pXSprite.getPalette() == 0) {
                pSprite.setLotag(kDudeBurning);
                aiNewState(pSprite, pXSprite, burnGoto[INNOCENT]);
                actHealDude(pXSprite, dudeInfo[39].startHealth, dudeInfo[39].startHealth);
                if (!IsOriginalDemo()) {
                    if (pXSprite.getBurnTime() == 0) {
                        pXSprite.setBurnTime(1200);
                    }
                }
                dudeExtra.setClock(gFrameClock + 360);
                return;
            }
        }

        if (pSprite.getLotag() == kGDXDudeUniversalCultist) {
            removeDudeStuff(pSprite);
            XSPRITE pXIncarnation = getNextIncarnation(pXSprite);
            if (pXIncarnation == null) {

                if (pXSprite.getData1() >= kThingHiddenExploder && pXSprite.getData1() < (kThingHiddenExploder + kExplodeMax) - 1 && Chance(0x4000) && nDamageType != kDamageSpirit && nDamageType != kDamageDrown) {

                    doExplosion(pSprite, pXSprite.getData1() - kThingHiddenExploder);
                    if (Chance(0x9000)) {
                        nDamageType = kDamageExplode;
                    }
                }

                if (nDamageType == kDamageBurn) {
                    if ((game.getCache().contains(pXSprite.getData2() + 15, seq) || game.getCache().contains(pXSprite.getData2() + 16, seq)) && pXSprite.getPalette() == 0) {

                        if (game.getCache().contains(pXSprite.getData2() + 3, seq)) {
                            pSprite.setLotag(kGDXGenDudeBurning);

                            if (pXSprite.getData2() == 11520) // don't inherit palette for burning if using default animation
                            {
                                pSprite.setPal(0);
                            }

                            aiNewState(pSprite, pXSprite, burnGoto[GDXGENDUDE]);
                            actHealDude(pXSprite, dudeInfo[55].startHealth, dudeInfo[55].startHealth);
                            if (pXSprite.getBurnTime() <= 0) {
                                pXSprite.setBurnTime(1200);
                            }
                            dudeExtra.setClock(gFrameClock + 360);
                            return;
                        }

                    } else {
                        pXSprite.setBurnTime(0);
                        pXSprite.setBurnSource(-1);
                        nDamageType = kDamageFall;
                    }
                }

            } else {
                int seqId = pXSprite.getData2() + 18;
                sfxPlayGDXGenDudeSound(pSprite, 10);

                if (!game.getCache().contains(seqId, seq)) {
                    seqKill(SS_SPRITE, nXSprite);

                    Sprite pEffect = actSpawnEffect(52, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getAng());
                    if (pEffect != null) {
                        pEffect.setCstat(Globals.kSpriteFace);
                        pEffect.setPal(6);
                        pEffect.setXrepeat(pSprite.getXrepeat());
                        pEffect.setYrepeat(pSprite.getYrepeat());
                    }

                    for (int i = 0; i < 3; i++) {
                        if (Chance(0x3000)) {
                            actGenerateGibs(pSprite, 6, null, null);
                        } else if (Chance(0x2000)) {
                            actGenerateGibs(pSprite, 5, null, null);
                        } else {
                            actGenerateGibs(pSprite, 17, null, null);
                        }
                    }

                    return;
                }
                seqSpawn(seqId, SS_SPRITE, nXSprite, null);
                return;
            }
        }

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            if (nSprite == gPlayer[i].fraggerID && gPlayer[i].deathTime > 0) {
                gPlayer[i].fraggerID = -1;
            }
        }
        if (pSprite.getLotag() != kDudeBeastCultist) {
            trTriggerSprite(nSprite, pXSprite, kCommandOff); // trigger death message
        }

        pSprite.setHitag(pSprite.getHitag() | (kAttrMove | kAttrGravity | kAttrFalling));

        Sprite pSource = boardService.getSprite(nSource);
        if (IsPlayerSprite(pSource) && pGameInfo.nGameType == kNetModeCoop) {
            PLAYER pPlayer = gPlayer[pSource.getLotag() - kDudePlayer1];
            pPlayer.fragCount++;
        }

        if (pXSprite.getKey() > 0) {
            DropPickupObject(pSprite, kItemKey1 + pXSprite.getKey() - 1);
        }
        if (pXSprite.getDropMsg() != 0) {
            DropPickupObject(pSprite, pXSprite.getDropMsg());
            if (!IsOriginalDemo()) {
                pXSprite.setDropMsg(0);
            }
        }

        // drop any items or weapons
        if (pSprite.getLotag() == kDudeTommyCultist) {
            int nDropCheck = Random(100);

            // constants? table?
            if (nDropCheck >= 10) {
                if (nDropCheck < 50) {
                    DropPickupObject(pSprite, 69);
                }
            } else {
                DropPickupObject(pSprite, 42);
            }
        } else if (pSprite.getLotag() == kDudeShotgunCultist) {
            int nDropCheck = Random(100);
            if (nDropCheck > 10) {
                if (nDropCheck <= 50) {
                    DropPickupObject(pSprite, 67);
                }
            } else {
                DropPickupObject(pSprite, 41);
            }
        }

        int deathType;
        switch (nDamageType) {
            case kDamageBurn:
                deathType = kSeqDudeDeath3;
                sfxStart3DSound(pSprite, 351, -1, 0);
                break;

            case kDamageExplode:
                deathType = kSeqDudeDeath2;
                switch (pSprite.getLotag()) {
                    case kGDXDudeUniversalCultist:
                    case kGDXGenDudeBurning:
                        sfxPlayGDXGenDudeSound(pSprite, 4);
                        break;
                    case kDudeTommyCultist:
                    case kDudeShotgunCultist:
                    case kDudeFanaticProne:
                    case kDudeBurning:
                    case kDudeCultistBurning:
                    case kDudeInnocent:
                    case kDudeCultistProne:
                    case kDudeTeslaCultist:
                    case kDudeDynamiteCultist:
                    case kDudeBeastCultist:
                    case kDudeTinyCaleb:
                    case kDudeTinyCalebburning:
                        sfxStart3DSound(pSprite, 717, -1, 0);
                        break;
                }
                break;
            case kDamageSpirit:
                switch (pSprite.getLotag()) {
                    case kDudeAxeZombie:
                    case kDudeEarthZombie:
                        deathType = 14;
                        break;
                    case kDudeButcher:
                        deathType = 11;
                        break;
                    default:
                        deathType = kSeqDudeDeath1;
                        break;
                }
                break;
            default:
                deathType = kSeqDudeDeath1;
                break;
        }

        // are we missing this sequence? if so, just delete it
        if (!game.getCache().contains(pDudeInfo.seqStartID + deathType, seq)) {
            System.out.println("sprite missing death sequence: deleted ");
            levelAddKills(pSprite);

            if (pSprite.getOwner() != -1 && pSprite.getOwner() != 32666) {
                int owner = actGetOwner(pSprite);
                BloodSprite spr = boardService.getSprite(owner);
                if (spr != null && spr.getLotag() == kGDXDudeUniversalCultist && spr.getXSprite() != null) {
                    DudeExtra pCultistExtra = spr.getXSprite().getDudeExtra();
                    pCultistExtra.setThinkTime(pCultistExtra.getThinkTime() - 1);
                }
            }

            seqKill(SS_SPRITE, nXSprite); // make sure we remove any active sequence
            actPostSprite(nSprite, kStatFree);
            return;
        }

        switch (pSprite.getLotag()) {
            case kDudeAxeZombie:
                sfxStart3DSound(pSprite, Random(2) + 1107, -1, 0);
                if (deathType == 2) {
                    seqSpawn(pDudeInfo.seqStartID + 2, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    GetSpriteExtents(pSprite);
                    startPos.set(pSprite.getX(), pSprite.getY(), extents_zTop);
                    startVel.set(pSprite.getVelocityX() >> 1, pSprite.getVelocityY() >> 1, -838860);
                    actGenerateGibs(pSprite, 27, startPos, startVel);
                } else if (deathType != 1 || !Chance(0x2000)) {
                    if (deathType == 14) {
                        seqSpawn(pDudeInfo.seqStartID + 14, SS_SPRITE, nXSprite, null);
                    } else if (deathType == 3) {
                        seqSpawn(pDudeInfo.seqStartID + 13, SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                    } else {
                        seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    }
                } else {
                    seqSpawn(pDudeInfo.seqStartID + 7, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 5);
                    pXSprite.setData1(35);
                    pXSprite.setData2(5);
                    GetSpriteExtents(pSprite);
                    startPos.set(pSprite.getX(), pSprite.getY(), extents_zTop);
                    startVel.set(pSprite.getVelocityX() >> 1, pSprite.getVelocityY() >> 1, -1118481);
                    actGenerateGibs(pSprite, 27, startPos, startVel);
                    sfxStart3DSound(pSprite, 362, -1, 0);
                }
                break;

            case kDudeTommyCultist:
            case kDudeShotgunCultist:
            case kDudeTeslaCultist:
            case kDudeDynamiteCultist:
                sfxStart3DSound(pSprite, Random(2) + 1018, -1, 0);
                if (deathType == kSeqDudeDeath3) {
                    seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                } else {
                    seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                }

                break;
            case kDudeCultistBurning:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 718, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1018, -1, 0);
                }
                nDamageType = kDamageExplode;
                if (Chance(0x4000)) {
                    for (int i = 0; i < 3; i++) {
                        actGenerateGibs(pSprite, 7, null, null);
                    }

                    if (GAMEVER == VER100) {
                        seqSpawn(802 + Random(1), SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    } else {
                        seqSpawn(16 + pDudeInfo.seqStartID - Random(1), SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    }

                } else {
                    seqSpawn(15 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                }
                break;
            case kGDXDudeUniversalCultist:
                sfxPlayGDXGenDudeSound(pSprite, 2);

                if (deathType == kSeqDudeDeath3) {

                    boolean seq15 = game.getCache().contains(pXSprite.getData2() + 15, seq);
                    boolean seq16 = game.getCache().contains(pXSprite.getData2() + 16, seq);
                    if (seq15 && seq16) {
                        seqSpawn((15 + Random(2)) + pXSprite.getData2(), SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                    } else if (seq16) {
                        seqSpawn(16 + pXSprite.getData2(), SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                    } else if (seq15) {
                        seqSpawn(15 + pXSprite.getData2(), SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                    } else if (game.getCache().contains(pXSprite.getData2() + deathType, seq)) {
                        seqSpawn(deathType + pXSprite.getData2(), SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                    } else {
                        seqKill(SS_SPRITE, nXSprite);
                    }

                } else {
                    seqSpawn(deathType + pXSprite.getData2(), SS_SPRITE, nXSprite, callbacks[DamageDude]);
                }

                pXSprite.setTxID(0); // to avoid second trigger.
                break;
            case kGDXGenDudeBurning:
                sfxPlayGDXGenDudeSound(pSprite, 4);
                nDamageType = kDamageExplode;

                if (Chance(0x4000)) {
                    for (int i = 0; i < 3; i++) {
                        actGenerateGibs(pSprite, 7, null, null);
                    }
                }


                int seqId = pXSprite.getData2();
                boolean seq15 = game.getCache().contains(pXSprite.getData2() + 15, seq);
                boolean seq16 = game.getCache().contains(pXSprite.getData2() + 16, seq);

                if (seq15 && seq16) {
                    seqId += (15 + Random(2));
                } else if (seq16) {
                    seqId += 16;
                } else {
                    seqId += 15;
                }

                seqSpawn(seqId, 3, nXSprite, callbacks[DamageDude]);
                break;

            case kDudeAxeZombieBurning:
                if (Chance(0x4000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1109, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1107, -1, 0);
                }
                nDamageType = kDamageExplode;
                if (Chance(0x4000)) {
                    seqSpawn(13 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                    GetSpriteExtents(pSprite);
                    startPos.set(pSprite.getX(), pSprite.getY(), extents_zTop);
                    startVel.set(pSprite.getVelocityX() >> 1, pSprite.getVelocityY() >> 1, -838860);
                    actGenerateGibs(pSprite, 27, startPos, startVel);
                } else {
                    seqSpawn(13 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                }
                break;

            case kDudeBloatedButcherBurning:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1206, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1204, -1, 0);
                }
                seqSpawn(10 + dudeInfo[4].seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeBurning:
                nDamageType = kDamageExplode;
                seqSpawn(7 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                break;

            case kDudeButcher:
                if (deathType == 14) {
                    sfxStart3DSound(pSprite, 1206, -1, 0);
                    seqSpawn(11 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1204, -1, 0);
                    if (deathType != kSeqDudeDeath3) {
                        seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                    } else {
                        seqSpawn(10 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                    }
                }
                break;

            case kDudeFleshGargoyle:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1405, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1403, -1, 0);
                }

                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeStoneGargoyle:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1455, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1453, -1, 0);
                }

                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudePhantasm:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1605, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1603, -1, 0);
                }

                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeHound:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1305, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1303, -1, 0);
                }
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeHand:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1905, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1903, -1, 0);
                }
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeBrownSpider:
            case kDudeRedSpider:
            case kDudeBlackSpider:
                if (pSprite.getOwner() != -1) {
                    BloodSprite spr = boardService.getSprite(actGetOwner(pSprite));
                    if (spr != null && spr.getXSprite() != null) {
                        DudeExtra pXDude = spr.getXSprite().getDudeExtra();
                        pXDude.setThinkTime(pXDude.getThinkTime() - 1);
                    }
                }

                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1805, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1803, -1, 0);
                }
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;
            case kDudeMotherSpider:
                sfxStart3DSound(pSprite, 1850, -1, 0);
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeGillBeast:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1705, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1703, -1, 0);
                }

                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeEel:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 1505, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 1503, -1, 0);
                }
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeBat:
                if (Chance(0x2000) && deathType == kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, 2005, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, Random(2) + 2003, -1, 0);
                }

                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeRat:
                if (!Chance(0x2000) || deathType != kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, Random(2) + 2103, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, 2105, -1, 0);
                }
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeGreenPod:
            case 225:
            case 226:
                if (!Chance(0x2000) || deathType != kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, Random(2) + 2203, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, 2205, -1, 0);
                }
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteFlipY);
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteFlipX);
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeGreenTentacle:
                if (nDamage == 5) {
                    sfxStart3DSound(pSprite, 2171, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, 2172, -1, 0);
                }
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteFlipY);
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteFlipX);
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;
            case kDudeFirePod:
                if (nDamage == 5) {
                    sfxStart3DSound(pSprite, 2151, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, 2152, -1, 0);
                }
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteFlipY);
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteFlipX);
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;
            case kDudeFireTentacle:
                sfxStart3DSound(pSprite, 2501, -1, 0);
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteFlipY);
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteFlipX);
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeCerberus:
            case kDudeCerberus2:
                if (!Chance(0x2000) || deathType != kSeqDudeDeath3) {
                    sfxStart3DSound(pSprite, Random(2) + 2305, -1, 0);
                } else {
                    sfxStart3DSound(pSprite, 2305, -1, 0);
                }
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            default:
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeTchernobog:
                sfxStart3DSound(pSprite, 2380, -1, 0);
                seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, null);
                break;

            case kDudeTinyCalebburning:
                nDamageType = kDamageExplode;
                seqSpawn(11 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                break;

            case kDudeTheBeast:

                sfxStart3DSound(pSprite, Random(2) + 9000, -1, 0);
                if (deathType == kSeqDudeDeath3) {
                    seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude2]);
                } else {
                    seqSpawn(deathType + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                }
                break;

            case kDudeTheBeastburning:
                nDamageType = kDamageExplode;
                seqSpawn(12 + pDudeInfo.seqStartID, SS_SPRITE, nXSprite, callbacks[DamageDude]);
                break;
        }
        // 32666 = custom dude had once life leech
        if (pSprite.getOwner() != -1 && pSprite.getOwner() != 32666) {
            BloodSprite spr = boardService.getSprite(actGetOwner(pSprite));
            if (spr != null && spr.getXSprite() != null) {
                switch (spr.getLotag()) {
                    case kGDXDudeUniversalCultist:
                    case kGDXGenDudeBurning:
                        DudeExtra pXDude = spr.getXSprite().getDudeExtra();
                        pXDude.setThinkTime(pXDude.getThinkTime() - 1);
                        break;
                    default:
                        break;
                }
            }
        }

        if (nDamageType == kDamageExplode) {
            for (int i = 0; i < 3; i++) {
                int nGibType = pDudeInfo.nGibType[i];
                if (nGibType > -1) {
                    actGenerateGibs(pSprite, nGibType, null, null);
                }

            }
            for (int j = 0; j < 4; ++j) {
                actSpawnBlood(pSprite);
            }
        }
        levelAddKills(pSprite);
        actCheckRespawn(pSprite);

        if (nXSprite >= 0 && !IsOriginalDemo()) {
            if (boardService.getXSprite(nXSprite).isProximity()) {
                boardService.getXSprite(nXSprite).setProximity(false);
            }
        }

        pSprite.setLotag(426);
        actPostSprite(nSprite, kStatThing);
    }

    public static void MoveDude(BloodSprite pSprite) {
        final XSPRITE pXSprite = pSprite.getXSprite();
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        final SPRITEHIT pSpriteHit = pXSprite.getSpriteHit();
        int nSprite = pXSprite.getReference();

        PLAYER pPlayer = null;
        if (IsPlayerSprite(pSprite)) {
            pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];
        }

        if (!(pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax)) {
            throw new AssertException("pSprite.type >= kDudeBase && pSprite.type < kDudeMax");
        }
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

        int zTop, zBot;
        GetSpriteExtents(pSprite);
        zTop = extents_zTop;
        zBot = extents_zBot;

        int floorDist = (zBot - pSprite.getZ()) / 4;
        int ceilDist = (pSprite.getZ() - zTop) / 4;

        int clipDist = pSprite.getClipdist() << 2;
        int nSector = pSprite.getSectnum();
        if (!boardService.isValidSector(nSector)) {
            throw new AssertException("boardService.isValidSector(nSector)");
        }

        if (pSprite.getVelocityX() != 0 || pSprite.getVelocityY() != 0) {
            if (pPlayer != null && gNoClip) {
                pSprite.setX((int) (pSprite.getX() + (pSprite.getVelocityX() >> 12)));
                pSprite.setY((int) (pSprite.getY() + (pSprite.getVelocityY() >> 12)));

                nSector = engine.updatesector(pSprite.getX(), pSprite.getY(), nSector);
                if (nSector == -1) {
                    nSector = pSprite.getSectnum();
                }
            } else {
                short oldcstat = pSprite.getCstat();
                pSprite.setCstat(pSprite.getCstat() & ~(kSpriteBlocking & kSpriteHitscan));

                pSpriteHit.moveHit = ClipMove(pSprite.getX(), pSprite.getY(), pSprite.getZ(), nSector, pSprite.getVelocityX() >> 12, pSprite.getVelocityY() >> 12, clipDist, ceilDist, floorDist, CLIPMASK0 | 0x3000);

                pSprite.setCstat(oldcstat);
                pSprite.setX(clipm_px);
                pSprite.setY(clipm_py);
                pSprite.setZ(clipm_pz);
                nSector = (short) clipm_pnsectnum;

                if (nSector == -1) {
                    nSector = pSprite.getSectnum();
                    if (pSprite.getStatnum() == kStatDude || pSprite.getStatnum() == kStatThing) {
                        actDamageSprite(nSprite, pSprite, kDamageFall, 16000);
                    }
                }

                if (boardService.getSector(nSector).getLotag() >= kSectorPath && boardService.getSector(nSector).getLotag() <= kSectorRotate) {
                    int push = engine.pushmove(pSprite.getX(), pSprite.getY(), pSprite.getZ(), nSector, clipDist, ceilDist, floorDist, CLIPMASK0);
                    pSprite.setX(pushmove_x);
                    pSprite.setY(pushmove_y);
                    pSprite.setZ(pushmove_z);

                    if (push == -1) {
                        actDamageSprite(nSprite, pSprite, kDamageFall, 16000);
                    }

                    if (pushmove_sectnum != -1) {
                        nSector = pushmove_sectnum;
                    }
                }

                if (!boardService.isValidSector(nSector)) {
                    throw new AssertException("boardService.isValidSector(nSector)");
                }
            }

            switch (pSpriteHit.moveHit & kHitTypeMask) {
                case kHitWall: {
                    int nWall = pSpriteHit.moveHit & kHitIndexMask;
                    Wall pWall = boardService.getWall(nWall);
                    if (pWall.getExtra() > 0) {
                        XWALL pXWall = xwall[pWall.getExtra()];
                        if (pXWall != null && pDudeInfo.lockOut && pXWall.triggerPush && pPlayer == null && pXWall.key == 0 && !pXWall.dudeLockout && pXWall.state == 0 && pXWall.busy == 0) {
                            trTriggerWall(nWall, pXWall, kCommandWallPush);
                        }
                    }

                    if (pWall.getNextsector() != -1) {
                        Sector pSector = boardService.getSector(pWall.getNextsector());

                        if (pSector.getExtra() > 0) {
                            XSECTOR pXSector = xsector[pSector.getExtra()];
                            if (pDudeInfo.lockOut && pXSector.Wallpush && pXSector.Key == 0 && !pXSector.dudelockout && pXSector.state == 0 && pXSector.busy == 0 && pPlayer == null) {
                                trTriggerSector(pWall.getNextsector(), pXSector, kCommandSectorPush);
                            }
                            // if ( zTop >= pSector.floorz ) ???
                            // v354 = zBot < pSector.ceilingz;
                        }
                    }

                    ReflectVector(pSprite.getVelocityX(), pSprite.getVelocityY(), nWall, 0);

                    pSprite.setVelocityX(refl_x);
                    pSprite.setVelocityY(refl_y);
                    break;
                }
                case kHitSprite: {
                    int nHSprite = pSpriteHit.moveHit & kHitIndexMask;
                    BloodSprite pHSprite = boardService.getSprite(nHSprite);
                    if (pHSprite == null) {
                        break;
                    }

                    if (pHSprite.getStatnum() == kStatMissile && (pHSprite.getHitag() & kAttrFree) == 0 && actGetOwner(pHSprite) != pSprite.getXvel()) {
                        short oldHitSect = (short) pHitInfo.hitsect;
                        short oldHitSprite = (short) pHitInfo.hitsprite;
                        int oldHitX = pHitInfo.hitx;
                        int oldHitY = pHitInfo.hity;
                        int oldHitZ = pHitInfo.hitz;
                        pHitInfo.hitsprite = pSprite.getXvel();
                        actImpactMissile(pHSprite, SS_SPRITE);
                        pHitInfo.hitsect = oldHitSect;
                        pHitInfo.hitsprite = oldHitSprite;
                        pHitInfo.hitx = oldHitX;
                        pHitInfo.hity = oldHitY;
                        pHitInfo.hitz = oldHitZ;
                    }

                    XSPRITE pXHSprite = pHSprite.getXSprite();
                    if (pXHSprite != null) {
                        // this is why touch for things never worked; they always ON
                        if (pXHSprite.isTouch() && /*pXHSprite.state == 0 &&*/ !pXHSprite.isTriggered()) {
                            if (!pXHSprite.isDudeLockout() || IsPlayerSprite(pSprite)) // allow dudeLockout for Touch flag
                            {
                                trTriggerSprite(nHSprite, pXHSprite, kCommandSpriteTouch);
                            }
                        }

                        if (pDudeInfo.lockOut && pXHSprite.isPush() && pXHSprite.getKey() == 0 && !pXHSprite.isDudeLockout() && pXHSprite.getState() == 0 && pXHSprite.getBusy() == 0 && pPlayer == null) {
                            trTriggerSprite(nHSprite, pXHSprite, kCommandSpritePush);
                        }

                    }
                    break;
                }

            }
        } else {
            if (!boardService.isValidSector(nSector)) {
                throw new AssertException("boardService.isValidSector(nSector)");
            }
            FindSector(pSprite.getX(), pSprite.getY(), pSprite.getZ(), nSector);
            nSector = foundSector;
        }

        if (pSprite.getSectnum() != nSector) {
            if (!boardService.isValidSector(nSector)) {
                throw new AssertException("boardService.isValidSector(nSector)");
            }

            int nXSector = boardService.getSector(pSprite.getSectnum()).getExtra();
            if (nXSector > 0 && xsector[nXSector].Exit && (pPlayer != null || !xsector[nXSector].dudelockout)) {
                trTriggerSector(pSprite.getSectnum(), xsector[nXSector], kCommandSectorExit);
            }

            engine.changespritesect(pSprite.getXvel(), nSector);

            nXSector = boardService.getSector(nSector).getExtra();

            if (nXSector > 0 && xsector[nXSector].Enter && (pPlayer != null || !xsector[nXSector].dudelockout)) {
                if (boardService.getSector(nSector).getLotag() == 604) {
                    if (pPlayer != null) {
                        xsector[nXSector].data = pSprite.getXvel();
                    } else {
                        xsector[nXSector].data = -1;
                    }
                }

                trTriggerSector(nSector, xsector[nXSector], kCommandSectorEnter);
            }

            nSector = pSprite.getSectnum();
        }

        boolean underwater = false, depth = false;
        if (boardService.getSector(nSector).getExtra() > 0) {
            if (xsector[boardService.getSector(nSector).getExtra()].Underwater) {
                underwater = true;
            }
            if (xsector[boardService.getSector(nSector).getExtra()].Depth != 0) {
                depth = true;
            }
        }

        Sprite pUpper = boardService.getSprite(gUpperLink[nSector]);
        Sprite pLower = boardService.getSprite(gLowerLink[nSector]);
        if (pUpper != null) {
            if (pUpper.getLotag() == kStatPurge || pUpper.getLotag() == kStatSpares) {
                depth = true;
            }
        }

        if (pLower != null) {
            if (pLower.getLotag() == kStatMarker || pLower.getLotag() == kStatFlare) {
                depth = true;
            }
        }

        if (pPlayer != null) {
            clipDist += 16;
        }

        if (pSprite.getVelocityZ() != 0) {
            pSprite.setZ((int) (pSprite.getZ() + (pSprite.getVelocityZ() >> 8)));
        }

        int ceilz, ceilhit, floorz, floorhit;

        GetZRange(pSprite, clipDist, CLIPMASK0 | 0x3000);
        ceilz = gz_ceilZ;
        ceilhit = gz_ceilHit;
        floorz = gz_floorZ;
        floorhit = gz_floorHit;
        GetSpriteExtents(pSprite);
        zTop = extents_zTop;
        zBot = extents_zBot;

        if ((pSprite.getHitag() & kAttrGravity) != 0) {
            int G = 58254;

            if (!depth) {
                if (underwater || zBot >= floorz) {
                    G = 0; // если ноги ниже уровня пола
                }
            } else {
                if (underwater) {
                    int ceilzofslope = engine.getceilzofslope(nSector, pSprite.getX(), pSprite.getY());
                    if (ceilzofslope > zTop) // если голова выше уровня воды
                    {
                        if (zBot != zTop) {
                            G += -80099 * (zBot - ceilzofslope) / (zBot - zTop);
                        }
                    } else {
                        G = 0;
                    }
                } else {
                    int florzofslope = engine.getflorzofslope(nSector, pSprite.getX(), pSprite.getY());
                    if (florzofslope < zBot && (zBot != zTop)) // если ноги ушли
                    // под воду
                    {
                        G += -80099 * (zBot - florzofslope) / (zBot - zTop);
                    }
                }
            }

            if (G != 0) {
                pSprite.setZ(pSprite.getZ() + (2 * G >> 8));
                pSprite.addVelocityZ(G);
            }
        }

        if (pPlayer != null && pSprite.getVelocityZ() > kScreamVel && !pPlayer.fScreamed && pXSprite.getHeight() != 0) {
            pPlayer.fScreamed = true;
            sfxStart3DSound(pSprite, 719, 0, 0);
        }

        // check for warping in linked sectors
        int warp = checkWarping(pSprite);
        if (warp != 0) {
            GetZRange(pSprite, clipDist, CLIPMASK0 | 0x3000);
            ceilz = gz_ceilZ;
            ceilhit = gz_ceilHit;
            floorz = gz_floorZ;
            floorhit = gz_floorHit;

            if (pPlayer != null) {
                viewUpdatePlayerLoc(pPlayer);
                sfxResetListener();
            }

			/* Make select palette for both lower and upper markers.
			   Currently now working for non-water sectors for some reason.
			if (warp >= kMarkerUpperLink && warp != kMarkerWarpDest && warp <= kMarkerLowerGoo){
				int gViewPal=0; int var1=-1;
				int nUpperTMP = gUpperLink[nSector]; int nLowerTMP = gLowerLink[nSector];

				if (nUpperTMP >= 0) var1 = nUpperTMP;
				else if (nLowerTMP >= 0) var1 = nLowerTMP;
				if (var1 != -1){
					SPRITE pLinkMarker = boardService.getSprite(var1);
					if (pLinkMarker.extra >= 0) {
						XSPRITE pXLinkMarker = getXSprite()[pLinkMarker.extra];
						gViewPal = pXLinkMarker.data2;
						if (warp == kMarkerUpperStack)System.out.println("crush"+pXLinkMarker);
					}
					if (gViewPal <= 0) {
						if (warp == kMarkerUpperWater) pXSprite.palette = 1;
						else pXSprite.palette = 3;
						// *Real* Goo palette ID is 3, previously it was 2.
						// We need fixt it because there is no control of it in view anymore
					} else {
						pXSprite.palette = gViewPal;
					}
				}
			}*/
            switch (warp) {

                case kMarkerLowerStack:
                    if (pPlayer != null && pPlayer.nPlayer == gViewIndex) {
                        setgotpic(boardService.getSector(pSprite.getSectnum()).getFloorpicnum());
                    }
                    break;
                case kMarkerUpperStack:
                    if (pPlayer != null && pPlayer.nPlayer == gViewIndex) {
                        setgotpic(boardService.getSector(pSprite.getSectnum()).getCeilingpicnum());
                    }
                    break;
                case kMarkerLowerWater:
                case kMarkerLowerGoo:
                    pXSprite.setPalette(kPalNormal);
                    if (pPlayer != null) {
                        pPlayer.moveState = 0;
                        pPlayer.bubbleTime = 0;
                        if (!pPlayer.pJump && pPlayer.pInput.Jump) {
                            pSprite.setVelocityZ(-436906);
                            pPlayer.pJump = true;
                        }
                        sfxStart3DSound(pSprite, 721, -1, 0);// out of water
                    } else {
                        switch (pSprite.getLotag()) {
                            case kDudeTommyCultist:
                            case kDudeShotgunCultist:
                            case kDudeTeslaCultist:
                            case kDudeDynamiteCultist:
                            case kDudeBeastCultist:
                                if (IsOriginalDemo() && pSprite.getLotag() >= kDudeTeslaCultist && pSprite.getLotag() <= kDudeBeastCultist) {
                                    break;
                                }
                                aiNewState(pSprite, pXSprite, cultistGoto[LAND]);
                                break;
                            case kDudeGillBeast:
                                aiNewState(pSprite, pXSprite, gillBeastGoto[LAND]);
                                pSprite.setHitag(pSprite.getHitag() | (kAttrGravity | kAttrFalling));
                                break;
                            case kDudeEel:
                                actKillSprite(pSprite.getXvel(), pSprite, 0, 16000);
                                break;
                        }
                    }
                    break;
                case kMarkerUpperWater:
                case kMarkerUpperGoo:
                    if (pUpper == null) {
                        break;
                    }

                    // look for palette in data2 of marker. If value <= 0, use default ones.
                    int gViewPal = 0;
                    XSPRITE pXUpper = boardService.getXSprite(pUpper.getExtra());
                    if (pXUpper != null) {
                        gViewPal = pXUpper.getData2();
                    }

                    if (gViewPal <= 0) {

                        // *Real* Goo palette ID is 3, but monsters think it's 2, so we need leave it as is just for them,
                        // so they can move normally in Goo and attack, while player can use any palette.
                        if (warp == kMarkerUpperWater) {
                            pXSprite.setPalette(kPalWater);
                        } else if (pPlayer != null) {
                            pXSprite.setPalette(kPalSewer);
                        } else {
                            pXSprite.setPalette(2);
                        }

                    } else if (pPlayer != null) {
                        pXSprite.setPalette(gViewPal); // Player can use any palette.
                    } else if (gViewPal > 2) {
                        pXSprite.setPalette(2); // Monsters should not use palette greater than 2, so they correct detect if they underwater.
                    }


                    if (pPlayer != null) {
                        pPlayer.moveState = 1;
                        pPlayer.pXsprite.setBurnTime(0);
                        int zvel = (int) klabs(pSprite.getVelocityZ());
                        pPlayer.bubbleTime = zvel >> 12;
                        evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 10);
                        if (pPlayer.fScreamed) {
                            sfxKill3DSound(pSprite, -1, 719);
                        }
                        sfxStart3DSound(pSprite, 720, -1, 0);// into water
                    } else {
                        switch (pSprite.getLotag()) {
                            case kDudeTommyCultist:
                            case kDudeShotgunCultist:
                            case kDudeTeslaCultist:
                            case kDudeDynamiteCultist:
                            case kDudeBeastCultist:
                                if (IsOriginalDemo() && pSprite.getLotag() >= kDudeTeslaCultist && pSprite.getLotag() <= kDudeBeastCultist) {
                                    break;
                                }

                                pXSprite.setBurnTime(0);
                                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 11);
                                sfxStart3DSound(pSprite, 720, -1, 0);
                                aiNewState(pSprite, pXSprite, cultistGoto[WATER]);
                                break;
                            case kDudeAxeZombie:
                                pXSprite.setBurnTime(0);
                                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 11);
                                sfxStart3DSound(pSprite, 720, -1, 0);
                                aiNewState(pSprite, pXSprite, zombieAGoto);
                                break;
                            case kDudeGillBeast:
                                pXSprite.setBurnTime(0);
                                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 11);
                                sfxStart3DSound(pSprite, 720, -1, 0);
                                aiNewState(pSprite, pXSprite, gillBeastGoto[WATER]);
                                pSprite.setHitag(pSprite.getHitag() & ~(kAttrGravity | kAttrFalling));
                                break;
                            case kDudeFleshGargoyle:
                            case kDudeHound:
                            case kDudeBlackSpider:
                            case kDudeBrownSpider:
                            case kDudeRedSpider:
                            case kDudeBat:
                            case kDudeRat:
                            case kDudeBurning:
                                actKillSprite(pSprite.getXvel(), pSprite, 0, 16000);
                                break;
                            case kDudeCultistBurning:
                                // There is no difference between water and goo except following chance:
                                if (Chance(warp == kMarkerUpperGoo ? 0x200 : 0xa00)) {
                                    pSprite.setLotag(kDudeTommyCultist);
                                } else {
                                    pSprite.setLotag(kDudeShotgunCultist);
                                }
                                pXSprite.setBurnTime(0);
                                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 11);
                                sfxStart3DSound(pSprite, 720, -1, 0);
                                aiNewState(pSprite, pXSprite, cultistGoto[WATER]);
                                break;
                            case kDudeButcher:
                                pXSprite.setBurnTime(0);
                                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 11);
                                sfxStart3DSound(pSprite, 720, -1, 0);
                                aiNewState(pSprite, pXSprite, zombieFGoto);
                                break;
                            case kGDXDudeUniversalCultist:
                                evPostCallback(pSprite.getXvel(), SS_SPRITE, 0, 11);
                                if (!canSwim(pSprite)) {
                                    actKillSprite(pSprite.getXvel(), pSprite, 0, 16000);
                                }
                                break;
                        }
                    }

                    break;
            }
        }

        GetSpriteExtents(pSprite);
        zTop = extents_zTop;
        zBot = extents_zBot;

        if (pPlayer != null && zBot >= floorz) {
            int ofloorz = floorz;
            int ofloorhit = floorhit;
            GetZRange(pSprite, pSprite.getClipdist() << 2, CLIPMASK0 | 0x3000);

            ceilz = gz_ceilZ;
            ceilhit = gz_ceilHit;
            floorz = gz_floorZ;
            floorhit = gz_floorHit;

            if (zBot <= floorz && pSprite.getZ() - ofloorz < floorDist) {
                floorz = ofloorz;
                floorhit = ofloorhit;
            }
        }

        if (floorz > zBot) {
            pSpriteHit.floorHit = 0;
            if ((pSprite.getHitag() & kAttrGravity) != 0) {
                pSprite.setHitag(pSprite.getHitag() | kAttrFalling);
            }
        } else { // hit floor?
            pSpriteHit.floorHit = floorhit;
            pSprite.setZ(pSprite.getZ() + (floorz - zBot));

            int dZvel = (int) (pSprite.getVelocityZ() - floorVel[pSprite.getSectnum()]);
            if (dZvel <= 0) {
                if (pSprite.getVelocityZ() == 0) {
                    pSprite.setHitag(pSprite.getHitag() & ~kAttrFalling);
                }
            } else {
                long gravity = GravityVector(pSprite.getVelocityX(), pSprite.getVelocityY(), dZvel, pSprite.getSectnum(), 0);
                int fallDamage = mulscale(gravity, gravity, 30);
                pSprite.setVelocityX(refl_x);
                pSprite.setVelocityY(refl_y);

                dZvel = (int) refl_z;
                if (pPlayer != null) {
                    pPlayer.fScreamed = false;
                    if (fallDamage > 240 && (pSprite.getHitag() & kAttrFalling) != 0) {
                        playSurfSound(pPlayer);
                    }

                    if (fallDamage > 480) {
                        sfxStart3DSound(pSprite, 701, 0, 0);
                    }
                }
                fallDamage -= 1600;
                if (fallDamage > 0) {
                    actDamageSprite(pSprite.getXvel(), pSprite, kDamageFall, fallDamage);
                }

                pSprite.setVelocityZ(dZvel);
                if (klabs(dZvel) >= 65536) {
                    pSprite.setHitag(pSprite.getHitag() | kAttrFalling);
                } else {
                    pSprite.setVelocityZ(floorVel[pSprite.getSectnum()]);
                    pSprite.setHitag(pSprite.getHitag() & ~kAttrFalling);
                }

                switch (game.getCurrentDef().GetSurfType(floorhit)) {
                    case 5:
                        actSpawnEffect(9, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), floorz, 0);
                        break;
                    case 14:
                        Sprite pSpawn = actSpawnEffect(10, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), floorz, 0);
                        if (pSpawn != null) {
                            for (int i = 0; i < 7; i++) {
                                BloodSprite pSpawn2 = actSpawnEffect(14, pSpawn.getSectnum(), pSpawn.getX(), pSpawn.getY(), pSpawn.getZ(), 0);
                                if (pSpawn2 != null) {
                                    pSpawn2.setVelocity(BiRandom(436906),
                                    BiRandom(436906),
                                    -Random(873813));
                                }
                            }
                        }
                        break;
                }
            }
        }

        if (ceilz < zTop) {
            pSpriteHit.ceilHit = 0;
        } else // hit ceiling
        {
            pSpriteHit.ceilHit = ceilhit;
            if (ceilz - zTop > 0) {
                pSprite.setZ(pSprite.getZ() + ceilz - zTop);
            }

            if (pSprite.getVelocityZ() <= 0 && (pSprite.getHitag() & kAttrFalling) != 0) {
                pSprite.setVelocityZ((-pSprite.getVelocityZ() / 8));
            }
        }

        GetSpriteExtents(pSprite);
        zBot = extents_zBot;

        if (floorz - zBot > 0) {
            pXSprite.setHeight((floorz - zBot) >> 8);
        } else {
            pXSprite.setHeight(0);
        }

        // drag and friction
        if (pSprite.getVelocityX() != 0 || pSprite.getVelocityY() != 0) {

            if ((floorhit & kHitTypeMask) == kHitSprite) {
                Sprite pUnderSprite = boardService.getSprite(floorhit & kHitIndexMask);
                if (pUnderSprite != null && (pUnderSprite.getCstat() & kSpriteRMask) == kSpriteFace) {
                    // push it off the face sprite
                    pSprite.addVelocityX(mulscale(kFrameTicks, pSprite.getX() - pUnderSprite.getX(), 2));
                    pSprite.addVelocityY(mulscale(kFrameTicks, pSprite.getY() - pUnderSprite.getY(), 2));
                    return;
                }
            }

            nSector = pSprite.getSectnum();
            if (boardService.getSector(nSector).getExtra() <= 0 || !xsector[boardService.getSector(nSector).getExtra()].Underwater) {
                if (pXSprite.getHeight() < 256) {
                    int kv = kDudeDrag;
                    if (pXSprite.getHeight() != 0) {
                        kv -= mulscale(pXSprite.getHeight(), kv, 8);
                    }

                    pSprite.addVelocityX(-dmulscale(kv, (int) pSprite.getVelocityX(), 0x8000, 1, 16));
                    pSprite.addVelocityY(-dmulscale(kv, (int) pSprite.getVelocityY(), 0x8000, 1, 16));

                    if (EngineUtils.qdist(pSprite.getVelocityX(), pSprite.getVelocityY()) < kMinDudeVel) {
                        pSprite.setVelocityX(0);
                        pSprite.setVelocityY(0);
                    }
                }
            }
        }
    }

    public static void ConcussSprite(int nSource, BloodSprite pSprite, int x, int y, int z, int damage) {

        if (pSprite == null) {
            throw new AssertException("pSprite != null");
        }
        int dx = pSprite.getX() - x;
        int dy = pSprite.getY() - y;
        int dz = (pSprite.getZ() - z) >> 4;

        int dist2 = dx * dx + dy * dy + dz * dz + (1 << kGlobalForceShift);

        ArtEntry pic = engine.getTile(pSprite.getPicnum());
        int area = pic.getWidth() * pSprite.getXrepeat() * pic.getHeight() * pSprite.getYrepeat() >> 1;
        long force = divscale(damage, dist2, kGlobalForceShift);

        if ((pSprite.getHitag() & kAttrMove) != 0) {
            int mass = 0;
            if (IsDudeSprite(pSprite)) {
                mass = dudeInfo[pSprite.getLotag() - kDudeBase].mass;
                switch (pSprite.getLotag()) {
                    case kGDXDudeUniversalCultist:
                    case kGDXGenDudeBurning:
                        mass = getDudeMassBySpriteSize(pSprite);
                        break;
                }
            } else if (pSprite.getLotag() >= kThingBase && pSprite.getLotag() < kThingMax) {
                mass = thingInfo[pSprite.getLotag() - kThingBase].mass;
            } else {
                Console.out.println("Unexpected type encountered in ConcussSprite()");
            }

            if (mass == 0) {
                throw new AssertException("mass != 0, type: " + pSprite.getLotag() + " picnum: " + pSprite.getPicnum());
            }
            int impulse = muldiv(force, area, klabs(mass));

            dx = mulscale(impulse, dx, 16);
            dy = mulscale(impulse, dy, 16);
            dz = mulscale(impulse, dz, 16);

            pSprite.addVelocity(dx, dy, dz);
        }
        actDamageSprite(nSource, pSprite, kDamageExplode, (int) force);
    }

    /*******************************************************************************
     * FUNCTION: ReflectVector()
     * DESCRIPTION: Reflects a vector off a wall
     * PARAMETERS: nFraction is elasticity (0x10000 == perfectly elastic)
     *******************************************************************************/
    public static void ReflectVector(long dx, long dy, int nWall, int nFraction) {
        refl_x = dx;
        refl_y = dy;

        // calculate normal for wall
        GetWallNormal(nWall);
        long nx = (long) normal.x;
        long ny = (long) normal.y;

        long dotProduct = dmulscale(dx, nx, dy, ny, 16);
        long sd = dmulscale(nFraction + 0x10000, dotProduct, 0x8000, 1, 16);

        refl_x -= mulscale(nx, sd, 16);
        refl_y -= mulscale(ny, sd, 16);

        // return mulscale(0x10000 - nFraction, dotProduct + 0x8000, 16);
    }

    public static long GravityVector(long dx, long dy, long dz, int nSector, int nFraction) {
        long result;
        if (boardService.getSector(nSector).getFloorheinum() != 0) {
            Wall pWall = boardService.getWall(boardService.getSector(nSector).getWallptr());
            int nAngle = EngineUtils.getAngle(boardService.getWall(pWall.getPoint2()).getX() - pWall.getX(), boardService.getWall(pWall.getPoint2()).getY() - pWall.getY()) + kAngle90;
            int nSlope = boardService.getSector(nSector).getFloorheinum() << 4;
            long dist = EngineUtils.qdist(nSlope, 65536);

            long mp = divscale(nSlope, dist, 16);
            long mg = divscale(-65536, dist, 16);

            int cx = mulscale(Cos(nAngle), mp, 30);
            int cy = mulscale(Sin(nAngle), mp, 30);

            long avel = (cx * dx + cy * dy + mg * dz) >> 16;
            long svel = mulscale(avel, nFraction + 0x10000, 16);

            dx -= mulscale(svel, cx, 16);
            dy -= mulscale(svel, cy, 16);
            dz -= mulscale(svel, mg, 16);

            result = mulscale(65536 - nFraction, avel, 16);

        } else {
            result = mulscale(65536 - nFraction, dz, 16);
            dz = result - dz;
        }
        refl_x = dx;
        refl_y = dy;
        refl_z = dz;
        return result;
    }

    private static void playSurfSound(PLAYER pPlayer) {
        Sprite pSprite = pPlayer.pSprite;
        if (pPlayer.pSprite == null) {
            return;
        }

        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            return;
        }

        SPRITEHIT pSpriteHit = pXSprite.getSpriteHit();

        if (pSpriteHit.floorHit != 0) {
            int surfType = game.getCurrentDef().GetSurfType(pSpriteHit.floorHit);
            int nHitObject = pSpriteHit.floorHit & kHitIndexMask;
            boolean IsDudeSprite = false;
            if ((pSpriteHit.floorHit & kHitTypeMask) == kHitSprite) {
                IsDudeSprite = IsDudeSprite(boardService.getSprite(nHitObject));
            }
            if (surfType != 0 && !IsDudeSprite) {
                sfxStart3DSound(pSprite, surfSfxLand[surfType], -1, 0);
            }
        }
    }

    public static void actKickObject(BloodSprite pSprite, BloodSprite pObject) {
        int moveDist = ClipLow(2 * EngineUtils.qdist(pSprite.getVelocityX(), pSprite.getVelocityY()), 0xAAAAA);

        pObject.setVelocity(mulscale(Cos(pSprite.getAng() + BiRandom(85)), moveDist, 30),
            mulscale(Sin(pSprite.getAng() + BiRandom(85)), moveDist, 30),
            mulscale(-8192, moveDist, 14));

        pObject.setHitag((kAttrMove | kAttrGravity | kAttrFalling));
    }

    public static void ProcessTouchObjects(BloodSprite pSprite, XSPRITE pXSprite) {
        int nSprite = pXSprite.getReference();
        SPRITEHIT pSpriteHit = pXSprite.getSpriteHit();
        int nHitObject = pSpriteHit.ceilHit & kHitIndexMask;

        switch (pSpriteHit.ceilHit & kHitTypeMask) {
            case kHitSprite:
                final BloodSprite pHit = boardService.getSprite(nHitObject);
                if (pHit != null && pHit.getXSprite() != null) {
                    final XSPRITE pXHit = pHit.getXSprite();
                    if (pHit.getStatnum() == kStatThing || pHit.getStatnum() == kStatDude) {
                        if (pSprite.getVelocityX() != 0 || pSprite.getVelocityY() != 0 || pSprite.getVelocityZ() != 0) {
                            if (pHit.getStatnum() == kStatThing) {
                                THINGINFO pThingInfo = thingInfo[pHit.getLotag() - kThingBase];
                                if ((pThingInfo.flags & 1) != 0) {
                                    pHit.setHitag(pHit.getHitag() | 1);
                                }
                                if ((pThingInfo.flags & 2) != 0) {
                                    pHit.setHitag(pHit.getHitag() | 4);
                                }

                                pHit.addVelocityX(mulscale(4, pHit.getX() - pSprite.getX(), 2));
                                pHit.addVelocityY(mulscale(4, pHit.getY() - pSprite.getY(), 2));

                            } else {
                                pHit.setHitag(pHit.getHitag() | 5);

                                pHit.addVelocityX(mulscale(4, pHit.getX() - pSprite.getX(), 2));
                                pHit.addVelocityY(mulscale(4, pHit.getY() - pSprite.getY(), 2));

//							if (!IsOriginalDemo() && ((IsPlayerSprite(pSprite) && isShrinked(pSprite)) || (IsPlayerSprite(pHit) && isGrown(pHit)))) {
//
//								int mass1 = dudeInfo[pHit.lotag - kDudeBase].mass;
//								int mass2 = dudeInfo[pSprite.lotag - kDudeBase].mass;
//								switch(pSprite.lotag) {
//									case kGDXDudeUniversalCultist:
//									case kGDXGenDudeBurning:
//										mass2 = getDudeMassBySpriteSize(pSprite);
//										break;
//								}
//
//								if (mass1 > mass2) {
//									int dmg = Math.abs((mass1 - mass2)*(pHit.clipdist - pSprite.clipdist));
//									if (dmg > 0)
//										actDamageSprite(pHit.xvel, pSprite, (Chance(0x1000)) ? kDamageFall : (Chance(0x2000)) ? kDamageExplode : kDamageBullet, dmg);
//
//									if (Chance(0x0200))
//										actKickObject(pHit,pSprite);
//								}
//							}
//
//							if (!IsPlayerSprite(pSprite) || !gPlayer[pSprite.lotag - kDudePlayer1].godMode) {
//								switch(pHit.lotag) {
//									case kDudeTchernobog:
//										actDamageSprite(pHit.xvel, pSprite, kDamageExplode, 4 * pXSprite.health);
//										break;
//									case kGDXDudeUniversalCultist:
//									case kGDXGenDudeBurning:
//										int dmg = (getDudeMassBySpriteSize(pHit) - getDudeMassBySpriteSize(pSprite))+pHit.clipdist;
//										if (dmg > 0) {
//											if (IsPlayerSprite(pSprite) && powerupCheck(gPlayer[pSprite.lotag - kDudePlayer1], kItemJumpBoots - kItemBase ) > 0)
//												actDamageSprite(pHit.xvel, pSprite, kDamageExplode, dmg);
//											else
//												actDamageSprite(pHit.xvel, pSprite, kDamageFall, dmg);
//										}
//										break;
//								}
//							}

                                if (pHit.getLotag() == kDudeTchernobog) {
                                    if (!IsPlayerSprite(pSprite) || !gPlayer[pSprite.getLotag() - kDudePlayer1].godMode) {
                                        actDamageSprite(pHit.getXvel(), pSprite, kDamageExplode, 4 * pXSprite.getHealth());
                                    }
                                }
                            }
                        }
                    }

                    if (pHit.getLotag() == kTrapSawBlade) {
                        if (pXHit.getState() != 0) {
                            pXHit.setData1(1);
                            pXHit.setData2((short) ClipHigh(pXHit.getData2() + 2 * kFrameTicks, kTimerRate * 5));
                            actDamageSprite(nSprite, pSprite, kDamageBullet, 16);
                        } else {
                            actDamageSprite(nSprite, pSprite, kDamageBullet, 1);
                        }
                    }
                }
                break;

            case kHitWall:
            case kHitSector:
                break;
        }

        nHitObject = pSpriteHit.moveHit & kHitIndexMask;
        switch (pSpriteHit.moveHit & kHitTypeMask) {
            case kHitSprite:
                final BloodSprite pHitObject = boardService.getSprite(nHitObject);
                if (pHitObject != null && pHitObject.getXSprite() != null) {
//				if (!IsOriginalDemo()) {
//					if ((IsPlayerSprite(pHitObject) && isShrinked(pHitObject)) || (IsPlayerSprite(pSprite) && isGrown(pSprite))) {
//						if (sprXVel[pSprite.xvel] != 0 && IsDudeSprite(pSprite) && IsDudeSprite(pHitObject)) {
//							int mass1 = dudeInfo[pSprite.lotag - kDudeBase].mass;
//							int mass2 = dudeInfo[pHitObject.lotag - kDudeBase].mass;
//							switch(pHitObject.lotag) {
//								case kGDXDudeUniversalCultist:
//								case kGDXGenDudeBurning:
//									mass2 = getDudeMassBySpriteSize(pHitObject);
//									break;
//							}
//
//							if (mass1 > mass2) {
//								actKickObject(pSprite,pHitObject);
//								sfxStart3DSound(pSprite, 357, -1, 1);
//								int dmg = (int) ((mass1-mass2) + Math.abs(sprXVel[pSprite.xvel] >> 16));
//								if (dmg > 0)
//									actDamageSprite(nSprite, pHitObject, (Chance(0x1000)) ? kDamageFall : kDamageBullet, dmg);
//							}
//						}
//					}
//
//		            switch (pSprite.lotag) {
//		                case kGDXDudeUniversalCultist:
//		                case kGDXGenDudeBurning:
//		                    if (IsDudeSprite(pHitObject) && !IsPlayerSprite(pHitObject)) {
//		                        int mass1 = getDudeMassBySpriteSize(pSprite);
//		                        int mass2 = getDudeMassBySpriteSize(pHitObject);
//
//		                        if ((mass1 - mass2) >= mass2) {
//		                            if ((pXSprite.target == pHitObject.xvel && !dudeIsMelee(pXSprite) && Chance(0x0500)) || pXSprite.target != pHitObject.xvel)
//		                                actKickObject(pSprite, pHitObject);
//		                            if (pHitObject.extra >= 0 && !isActive(pHitObject.xvel))
//		                                aiActivateDude(pHitObject, getXSprite()[pHitObject.extra]);
//		                        }
//		                    }
//		                    break;
//		            }
//				}

                    switch (pHitObject.getLotag()) {
                        case kDudeBurning:
                        case kDudeCultistBurning:
                        case kDudeAxeZombieBurning:
                        case kDudeBloatedButcherBurning:
                        case kDudeTinyCalebburning:
                        case kDudeTheBeastburning:
                            pXSprite.setBurnTime(ClipLow(pXSprite.getBurnTime() - kFrameTicks, 0));
                            actDamageSprite(actGetBurnSource(pXSprite.getBurnSource()), pSprite, kDamageBurn, 2 * kFrameTicks);
                            break;
                        case kThingPail:
                        case kThingZombieHead:
                            actKickObject(pSprite, pHitObject);
                            if (pHitObject.getLotag() == kThingZombieHead) {
                                sfxCreate3DSound(pSprite.getX(), pSprite.getY(), pSprite.getZ(), 357, pSprite.getSectnum());
                                actDamageSprite(-1, pHitObject, kDamageFall, 80);
                            }
                            break;
                    }
                }
                break;

            case kHitWall:
            case kHitSector:
                break;
        }

        nHitObject = pSpriteHit.floorHit & kHitIndexMask;
        switch (pSpriteHit.floorHit & kHitTypeMask) {
            case kHitSprite:
                final BloodSprite pHitObject = boardService.getSprite(nHitObject);
                if (pHitObject != null && pHitObject.getXSprite() != null) {
                    final XSPRITE pXHit = pHitObject.getXSprite();
                    if (IsDudeSprite(pHitObject)) {
//					if (!IsOriginalDemo()) {
//						if ((IsPlayerSprite(pHitObject) && isShrinked(pHitObject)) || (IsPlayerSprite(pSprite) && isGrown(pSprite))) {
//
//							if (IsDudeSprite(pSprite) && IsDudeSprite(pHitObject)) {
//								int mass1 = dudeInfo[pSprite.lotag - kDudeBase].mass;
//								int mass2 = dudeInfo[pHitObject.lotag - kDudeBase].mass;
//								switch(pHitObject.lotag) {
//									case kGDXDudeUniversalCultist:
//									case kGDXGenDudeBurning:
//										mass2 = getDudeMassBySpriteSize(pHitObject);
//										break;
//								}
//
//
//								if (mass1 > mass2) {
//									if ((IsPlayerSprite(pHitObject) && Chance(0x500)) || !IsPlayerSprite(pHitObject))
//										actKickObject(pSprite,pHitObject);
//
//									int dmg = (mass1 - mass2) + pSprite.clipdist;
//									if (dmg > 0)
//										actDamageSprite(nSprite, pHitObject, (Chance(0x1000)) ? kDamageFall : kDamageBullet, dmg);
//								}
//							}
//						}
//
//			            switch (pSprite.lotag) {
//			                case kGDXDudeUniversalCultist:
//			                case kGDXGenDudeBurning:
//			                {
//			                    if (IsDudeSprite(pHitObject) && !IsPlayerSprite(pHitObject)) {
//			                        int mass1 = getDudeMassBySpriteSize(pSprite);
//			                        int mass2 = getDudeMassBySpriteSize(pHitObject);
//
//			                        if ((mass1 - mass2) >= mass2) {
//			                            if (Chance((pXSprite.target == pHitObject.xvel) ? 0x0500 : 0x1000)) actKickObject(pSprite, pHitObject);
//			                            if (pHitObject.extra >= 0 && !isActive(pHitObject.xvel))
//			                                aiActivateDude(pHitObject, getXSprite()[pHitObject.extra]);
//			                        }
//			                    }
//			                    break;
//			                }
//			            }
//					}

//					if (pHitObject.lotag <= kDudePlayer8 && pHitObject.lotag != kDudeFleshStatue && pHitObject.lotag != kDudeStoneStatue
//							&& pHitObject.lotag != kDudeMotherSpider && pHitObject.lotag != kDudeEel && pHitObject.lotag != kDudeFanaticProne) {
//								if (IsPlayerSprite(pSprite) && (IsOriginalDemo() || !isShrinked(pSprite))) {
//									actDamageSprite(nSprite, pHitObject, kDamageBullet, 8);
//									return;
//						}
//					}

                        if (pHitObject.getLotag() <= kDudePlayer8 && pHitObject.getLotag() != kDudeFleshStatue && pHitObject.getLotag() != kDudeStoneStatue && pHitObject.getLotag() != kDudeMotherSpider && pHitObject.getLotag() != kDudeEel && pHitObject.getLotag() != kDudeFanaticProne) {
                            if (IsPlayerSprite(pSprite)) {
                                actDamageSprite(nSprite, pHitObject, kDamageBullet, 8);
                                return;
                            }
                        }
                    }

                    switch (pHitObject.getLotag()) {
                        case kTrapSawBlade:
                            if (pXHit.getState() != 0) {
                                pXHit.setData1(1);
                                pXHit.setData2((short) ClipHigh(pXHit.getData2() + 2 * kFrameTicks, kTimerRate * 5));
                                actDamageSprite(nSprite, pSprite, kDamageBullet, 16);
                            } else {
                                actDamageSprite(nSprite, pSprite, kDamageBullet, 1);
                            }
                            break;

                        case kThingPail:
                        case kThingZombieHead:
                            if (IsPlayerSprite(pSprite)) {
                                PLAYER pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];
                                if (gFrameClock < pPlayer.kickTime) {
                                    return;
                                }
                                pPlayer.kickTime = gFrameClock + 60;
                            }
                            sfxCreate3DSound(pSprite.getX(), pSprite.getY(), pSprite.getZ(), 357, pSprite.getSectnum());
                            actKickObject(pSprite, pHitObject);
                            if (pHitObject.getLotag() == kThingZombieHead) {
                                actDamageSprite(-1, pHitObject, kDamageFall, 80);
                            } else {
                                sfxStart3DSound(pSprite, 374, 0, 0);
                            }
                            break;
                    }
                }
                break;

            case kHitWall:
                break;
            case kHitFloor:
                processTouchFloor(pSprite, nHitObject);
                break;
        }

//		// by NoOne: add more trigger statements for Touch flag
//		if (!IsOriginalDemo()) {
//
//			// Touch sprites
//			int nHSprite = -1;
//			if ((gSpriteHit[nXSprite].moveHit & kHitTypeMask) == kHitSprite)
//				nHSprite = gSpriteHit[nXSprite].moveHit & kHitIndexMask;
//			else if ((gSpriteHit[nXSprite].floorHit & kHitTypeMask) == kHitSprite)
//				nHSprite = gSpriteHit[nXSprite].floorHit & kHitIndexMask;
//			else if ((gSpriteHit[nXSprite].ceilHit & kHitTypeMask) == kHitSprite)
//				nHSprite = gSpriteHit[nXSprite].ceilHit & kHitIndexMask;
//
//			if (nHSprite >= 0 && boardService.getSprite(nHSprite).extra >= 0) {
//				XSPRITE pXHSprite = getXSprite()[boardService.getSprite(nHSprite).extra];
//				if (pXHSprite.Touch && !pXHSprite.isTriggered && (!pXHSprite.DudeLockout || IsPlayerSprite(pSprite)))
//					trTriggerSprite(nHSprite, pXHSprite, kCommandSpriteTouch);
//			}
//
//			// Touch walls
//			int nHWall = -1;
//			if ((gSpriteHit[nXSprite].moveHit & kHitTypeMask) == kHitWall) {
//				if ((nHWall = gSpriteHit[nXSprite].moveHit & kHitIndexMask) >= 0 && boardService.getWall(nHWall).extra >= 0) {
//					XWALL pXHWall = xwall[boardService.getWall(nHWall).extra];
//					if (pXHWall.triggerReserved && !pXHWall.isTriggered && (!pXHWall.dudeLockout || IsPlayerSprite(pSprite)))
//						trTriggerWall(nHWall, pXHWall, kCommandWallTouch);
//				}
//			}
//		}

    }

    public static void processTouchFloor(BloodSprite pSprite, int nSector) {
        if (pSprite == null) {
            throw new AssertException("pSprite != null");
        }
        if (!boardService.isValidSector(nSector)) {
            throw new AssertException("boardService.isValidSector(nSector)");
        }

        Sector pSector = boardService.getSector(nSector);
        XSECTOR pXSector = pSector.getExtra() > 0 ? xsector[pSector.getExtra()] : null;
        if (pXSector != null && (pSector.getLotag() == kSectorDamage || pXSector.damageType != 0)) {
            int nDamageType = pXSector.damageType;
            if (pSector.getLotag() == kSectorDamage) {
                nDamageType = ClipRange(nDamageType, 0, 6);
            } else {
                nDamageType = ClipRange(nDamageType - 1, 0, 6);
            }

            int nDamage = 1000;
            if (pXSector.data != 0) {
                nDamage = ClipLow(pXSector.data, 0);
            }

            actDamageSprite(pSprite.getXvel(), pSprite, nDamageType, (kFrameTicks * nDamage / kTimerRate) << 4);
        }

        int surfType = game.getCurrentDef().GetSurfType(nSector | kHitFloor);
        if (surfType == 14) {
            actDamageSprite(pSprite.getXvel(), pSprite, kDamageBurn, 16);
            sfxStart3DSound(pSprite, 352, 5, 2);
        }
    }

    /***********************************************************************
     * actPostSprite()
     * Postpones deletion or status list change for a sprite. An nStatus value
     * of kStatFree passed to this function will postpone deletion of the sprite
     * until the gPostpone list is next processed.
     **********************************************************************/
    public static void actPostSprite(int nSprite, int nStatus) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("pSprite != null");
        }

        if (pSprite.getStatnum() >= kMaxStatus) {
            throw new AssertException("pSprite.getStatnum() < kMaxStatus)");
        }

        if (!(nStatus >= 0 && nStatus <= kStatFree)) {
            throw new AssertException("nStatus >= 0 && nStatus <= kStatFree ");
        }

        POSTPONE postpone = null;
        if ((pSprite.getHitag() & kAttrFree) != 0) {
            // see if it is already in the list (we may want to semaphore with
            // an attr bit for speed)
            for (int n = 0; n < gPost.getSize(); n++) {
                POSTPONE pPost = gPost.get(n);
                if (pPost.nSprite == nSprite) {
                    postpone = pPost;
                    break;
                }
            }

            if (postpone == null) {
                throw new AssertException("postpone != null");
            }
        } else {
            postpone = gPost.obtain();
            pSprite.setHitag(pSprite.getHitag() | kAttrFree);
        }

        postpone.nSprite = nSprite;
        postpone.nStatus = nStatus;
    }

    /***********************************************************************
     * actPostProcess()
     * Processes postponed sprite events to ensure that sprite list processing
     * functions normally when sprites are deleted or change status.
     **********************************************************************/
    public static void actPostProcess() {
        if (gPost.getSize() != 0) {
            for (int i = 0; i < gPost.getSize(); i++) {
                POSTPONE pPost = gPost.get(i);
                final int nSprite = pPost.nSprite;
                final BloodSprite pSprite = boardService.getSprite(nSprite);
                if (pSprite != null) {
                    pSprite.setHitag(pSprite.getHitag() & ~kAttrFree);
                    if (pPost.nStatus == kStatFree) {
                        checkEventList(nSprite, SS_SPRITE);
                        int nXSprite = pSprite.getExtra();
                        if (nXSprite >= 0) {
                            seqKill(SS_SPRITE, nXSprite);
                        }
                        engine.deletesprite(nSprite);
                    } else {
                        engine.changespritestat(nSprite, pPost.nStatus);
                    }
                }
            }
            gPost.reset();
        }
    }

    public static void actProcessEffects() {
        for (ListNode<Sprite> node = boardService.getStatNode(kStatEffect); node != null; node = node.getNext()) {
            final int nSprite = node.getIndex();
            final BloodSprite pSprite = (BloodSprite) node.get();
            viewBackupSpriteLoc(nSprite, pSprite);
            int nSector = pSprite.getSectnum();
            if (!boardService.isValidSector(nSector)) {
                actDeleteEffect2(nSprite);
                continue;
            }

            if (pSprite.getLotag() >= kFXMax) {
                actDeleteEffect2(nSprite);
                continue;
            }

            EFFECT pFXData = gEffectInfo[pSprite.getLotag()];
            AirDrag(pSprite, pFXData.velocity);

            if (pSprite.getVelocityX() != 0) {
                pSprite.setX((int) (pSprite.getX() + (pSprite.getVelocityX() >> 12)));
            }
            if (pSprite.getVelocityY() != 0) {
                pSprite.setY((int) (pSprite.getY() + (pSprite.getVelocityY() >> 12)));
            }
            if (pSprite.getVelocityZ() != 0) {
                pSprite.setZ((int) (pSprite.getZ() + (pSprite.getVelocityZ() >> 8)));
            }

            if (pSprite.getVelocityX() == 0 && pSprite.getVelocityY() == 0 && pSprite.getVelocityZ() == 0) {
                pSprite.addVelocityZ(pFXData.gravity);
                continue;
            }

            boolean gravity = pSprite.getVelocityX() == 0 && (pSprite.getVelocityY() == 0 || pSprite.getZ() < boardService.getSector(nSector).getFloorz());
            if (!gravity) {
                nSector = engine.updatesector(pSprite.getX(), pSprite.getY(), nSector);
                if (nSector == -1) {
                    actDeleteEffect2(nSprite);
                    continue;
                }
            }

            if (gravity || pSprite.getZ() < engine.getflorzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY())) {
                if (pSprite.getSectnum() != nSector) {
                    if (!boardService.isValidSector(nSector)) {
                        throw new AssertException("boardService.isValidSector(nSector)");
                    }
                    engine.changespritesect((short) nSprite, nSector);
                }
                engine.getzsofslope(nSector, pSprite.getX(), pSprite.getY(), floorz, ceilz);
                if (ceilz.get() > pSprite.getZ() && (boardService.getSector(nSector).getCeilingstat() & kSectorParallax) == 0) {
                    actDeleteEffect2(nSprite);
                    continue;
                }
                if (floorz.get() >= pSprite.getZ()) {
                    pSprite.addVelocityZ(pFXData.gravity);
                    continue;
                }
            }

            int funcID = pFXData.funcID;
            if (funcID > -1 && funcID <= kCallbackMax) {
                if (gCallback[funcID] == null) {
                    throw new AssertException("gCallback[funcID] != NULL");
                }
                gCallback[funcID].run(nSprite);
            } else {
                actDeleteEffect2(nSprite);
            }

            /*
             * boolean LABEL13 = false; if ( pSprite.getVelocityX() == 0 && (
             * pSprite.getVelocityY() == 0 || pSprite.z < boardService.getSector(nSector).floorz ) ) {
             * LABEL13 = true; } if(!LABEL13) { nSector =
             * engine.updatesector(pSprite.x, pSprite.y, nSector); if ( nSector
             * == -1 ) { actDeleteEffect2(nSprite); continue; } } if (LABEL13 ||
             * engine.getflorzofslope(pSprite.sectnum, pSprite.x, pSprite.y) >
             * pSprite.z) { if (!LABEL13 && pSprite.sectnum != nSector) {
             * if(!(boardService.isValidSector(nSector)))
             * throw new AssertException("boardService.isValidSector(nSector)");
             * changespritesect((short) nSprite, nSector); }
             *
             * if ( pSprite.getVelocityX() == 0 && pSprite.getVelocityY() == 0 &&
             * pSprite.getVelocityZ() == 0 ) { pSprite.getVelocityZ() += pFXData.gravity;
             * continue; } engine.getzsofslope(nSector, pSprite.x, pSprite.y);
             * if (ceilz.get() > pSprite.z && (boardService.getSector(nSector).ceilingstat &
             * 1) == 0) { actDeleteEffect2(nSprite); continue; } if
             * (floorz.get() < pSprite.z) { int funcID = pFXData.funcID;
             *
             * if (funcID > -1 && funcID <= 22) { if(gCallback[pFXData.funcID]
             * == null) throw new AssertException("gCallback[pFXData.funcID] != NULL");
             * gCallback[pFXData.funcID].run(nSprite); } else {
             * actDeleteEffect2(nSprite); continue; } } else pSprite.getVelocityZ() +=
             * pFXData.gravity;
             *
             * continue; } int funcID = pFXData.funcID;
             *
             * if (funcID > -1 && funcID <= 22) { if(gCallback[pFXData.funcID]
             * == null) throw new AssertException("gCallback[pFXData.funcID] != NULL");
             * gCallback[pFXData.funcID].run(nSprite); } else
             * actDeleteEffect2(nSprite);
             */
        }
    }

    public static void actProcessSprites() {
        gTNTCount = 0;

        // process things for effects
        for (ListNode<Sprite> node = boardService.getStatNode(kStatThing); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            BloodSprite pThink = (BloodSprite) node.get();
            if ((pThink.getHitag() & kAttrFree) != 0) {
                continue;
            }

            if (!IsOriginalDemo() && (pThink.getLotag() == kThingTNTRem || pThink.getLotag() == kThingTNTProx || pThink.getLotag() == kGDXThingTNTProx)) { //PriorityList overflow protect
                gTNTCount++;
                if (gTNTCount > kPQueueSize / 2) //fNodeCount < kPQueueSize protect
                {
                    actPostSprite(nSprite, kStatFree);
                    continue;
                }
            }

            XSPRITE pXThink = pThink.getXSprite();
            if (pXThink != null) {
                int type = pThink.getLotag();
                if (type == 425 || type == 426 || type == kThingZombieHead) {
                    if (pXThink.getLocked() != 0) {
                        if (gFrameClock >= pXThink.getTargetX()) {
                            pXThink.setLocked(0);
                        }
                    }
                }
                if (actGetBurnTime(pXThink) > 0) {
                    pXThink.setBurnTime(ClipLow(pXThink.getBurnTime() - kFrameTicks, 0));
                    actDamageSprite(actGetBurnSource(pXThink.getBurnSource()), pThink, kDamageBurn, 2 * kFrameTicks);
                }

                // don't process sight flag for things which is locked or triggered
				/*if (!IsOriginalDemo() && pXThink.Sight && pXThink.Locked != 1 && !pXThink.isTriggered) {
					for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
						SPRITE pSprite = gPlayer[i].pSprite;
						if (engine.cansee(pThink.x, pThink.y, pThink.z, pThink.sectnum, pSprite.x, pSprite.y, pSprite.z, pSprite.sectnum)) {
							trTriggerSprite(nSprite, pXThink, kCommandSpriteSight);
							break;
						}
					}
				}

				// RESERVED FOR FUTURE AIM FLAG
				if (pXThink.Aim && pXThink.Locked != 1 && pXThink.isTriggered != true) {
					for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
						PLAYER pPlayer = gPlayer[i]; SPRITE pSprite = pPlayer.pSprite;
							int z = pPlayer.weaponAboveZ - pSprite.z;
							int hitCode = VectorScan(pPlayer.pSprite, 0, z, (int) pPlayer.aim.x, (int) pPlayer.aim.y, (int) pPlayer.aim.z, Weapon.kAimMaxDist, 0);
							if (hitCode != SS_SPRITE || pHitInfo.hitsprite != pThink.xvel) continue;
							trTriggerSprite(nSprite, pXThink, kCommandSpriteAim);
							pXThink.Locked = 1;
							break;
					}
				}*/

                if (pXThink.isProximity()) {
                    boolean isOriginal = IsOriginalDemo();
                    // don't process locked or 1-shot things for proximity (if not demo)
                    if (!isOriginal && (pXThink.getLocked() == 1 || pXThink.isTriggered())) {
                        continue;
                    }

                    if (pThink.getLotag() == kThingLifeLeech) {
                        pXThink.setTarget(-1);
                    }

                    ListNode<Sprite> nextAffected;
                    for (ListNode<Sprite> nAffected = boardService.getStatNode(kStatDude); nAffected != null; nAffected = nextAffected) {
                        nextAffected = nAffected.getNext();

//                    Iterator<Integer> it = spriteStatMap.getIndicesOf(kStatDude).iterator();
//                    while (it.hasNext()) {
//                        int nAffected = it.next();

                        Sprite pDude = nAffected.get();

                        if ((pDude.getHitag() & kAttrFree) == 0 && boardService.getXSprite(pDude.getExtra()).getHealth() != 0) {
                            // by NoOne: allow dudeLockout for proximity flag
                            if (!isOriginal && (pThink.getLotag() != kThingLifeLeech && pXThink.isDudeLockout() && !IsPlayerSprite(pDude))) {
                                continue;
                            }

                            int proxyDist = 96;
                            if (pThink.getLotag() == kGDXThingCustomDudeLifeLeech) {
                                proxyDist = 512;
                            } else if ((pThink.getLotag() == kThingLifeLeech) && pXThink.getTarget() == -1) {
                                int nOwner = actGetBurnSource(pThink.getOwner());
                                if (nAffected.getIndex() == nOwner || nOwner == -1) {
                                    continue;
                                }

                                PLAYER pOwner = null;
                                Sprite pOwnerSprite = boardService.getSprite(nOwner);
                                if (IsPlayerSprite(pOwnerSprite)) {
                                    pOwner = gPlayer[pOwnerSprite.getLotag() - kDudePlayer1];
                                }
                                PLAYER pTarget = null;
                                if (IsPlayerSprite(pDude)) {
                                    pTarget = gPlayer[pDude.getLotag() - kDudePlayer1];
                                }

                                if (pDude.getLotag() != kDudeEarthZombie && pDude.getLotag() != kDudeRat && pDude.getLotag() != kDudeBat && (pGameInfo.nGameType != 3 || pTarget == null || (pOwner != null && pTarget.teamID != pOwner.teamID)) && (pGameInfo.nGameType != 1 || pTarget == null)) {
                                    proxyDist = 512;
                                } else {
                                    continue;
                                }
                            }

                            if (CheckProximity(pDude, pThink.getX(), pThink.getY(), pThink.getZ(), pThink.getSectnum(), proxyDist)) {
                                switch (pThink.getLotag()) {
                                    case kGDXThingTNTProx:
                                        if (!isOriginal && !IsPlayerSprite(pDude)) {
                                            continue;
                                        }
                                        pThink.setPal(0);
                                        break;
                                    case kThingLifeLeech:
                                        if ((!Chance(0x2000) && nextAffected != null) || (pDude.getCstat() & CLIPMASK0) == 0) {
                                            continue;
                                        }
                                        pXThink.setTarget(pDude.getXvel());
                                        break;
                                    case kGDXThingCustomDudeLifeLeech:
                                        if (!isOriginal && pXThink.getTarget() != pDude.getXvel()) {
                                            continue;
                                        }
                                        break;
                                }

                                if (pThink.getOwner() == -1) {
                                    actSetOwner(pThink, pDude);
                                }
                                trTriggerSprite(nSprite, pXThink, kCommandSpriteProximity);
                            }
                        }
                    }
                }
            }
        }

        // process things for movement
        for (ListNode<Sprite> node = boardService.getStatNode(kStatThing); node != null; node = node.getNext()) {
            final int nSprite = node.getIndex();
            final BloodSprite pThink = (BloodSprite) node.get();

            if ((pThink.getHitag() & kAttrFree) != 0) {
                continue;
            }

            XSPRITE pXSprite = boardService.getXSprite(pThink.getExtra());
            if (pXSprite == null) {
                throw new AssertException("pXSprite != null");
            }

            int nSector = pThink.getSectnum();
            int nXSector = boardService.getSector(nSector).getExtra();
            XSECTOR pXSector = null;
            if (nXSector > 0) {
                if (!(nXSector < kMaxXSectors)) {
                    throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
                }
                if (!(xsector[nXSector].reference == nSector)) {
                    throw new AssertException("xsector[nXSector].reference == nSector");
                }
                pXSector = xsector[nXSector];
            }

            if (pXSector != null && pXSector.panVel != 0 && (pXSector.panAlways || pXSector.busy != 0 || pXSector.state != 0)) {
                THINGINFO pThinkInfo = thingInfo[pThink.getLotag() - kThingBase];
                if ((pThinkInfo.flags & 1) != 0) {
                    pThink.setHitag(pThink.getHitag() | kAttrMove);
                }
                if ((pThinkInfo.flags & 2) != 0) {
                    pThink.setHitag(pThink.getHitag() | kAttrFalling);
                }
            }

            if ((pThink.getHitag() & (kAttrMove | kAttrGravity)) != 0) {
                viewBackupSpriteLoc(nSprite, pThink);
                if (pXSector != null && pXSector.panVel != 0) {
                    GetSpriteExtents(pThink);

                    if (engine.getflorzofslope(pThink.getSectnum(), pThink.getX(), pThink.getY()) <= extents_zBot) {
                        int panVel = 0;
                        int panAngle = pXSector.panAngle;
                        if (pXSector.panAlways || pXSector.state != 0 || pXSector.busy != 0) {
                            panVel = (pXSector.panVel & 0xFF) << 9;
                            if (!pXSector.panAlways && pXSector.busy != 0) {
                                panVel = mulscale(panVel, pXSector.busy, 16);
                            }
                        }

                        if ((boardService.getSector(nSector).getFloorstat() & kSectorRelAlign) != 0) {
                            panAngle += GetWallAngle(boardService.getSector(nSector).getWallptr()) + kAngle90;
                            panAngle &= kAngleMask;
                        }

                        int pushX = mulscale(panVel, Cos(panAngle), 30);
                        int pushY = mulscale(panVel, Sin(panAngle), 30);
                        pThink.addVelocityX(pushX);
                        pThink.addVelocityY(pushY);
                    }
                }

                AirDrag(pThink, 128);
                if ((gFrame & 0xF) == (pThink.getXvel() >> 8) && (pThink.getHitag() & kAttrGravity) != 0) {
                    pThink.setHitag(pThink.getHitag() | kAttrFalling);
                }

                if ((pThink.getHitag() & kAttrFalling) == 0 && pThink.getVelocityX() == 0 && pThink.getVelocityY() == 0 && pThink.getVelocityZ() == 0 && floorVel[pThink.getSectnum()] == 0 && ceilingVel[pThink.getSectnum()] == 0) {
                    continue;
                }
                MoveThing(pThink);
            }
        }

        // process missile sprites
        NEXTMISSILE:
        for (ListNode<Sprite> node = boardService.getStatNode(kStatMissile); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            BloodSprite pSprite = (BloodSprite) node.get();
            if ((pSprite.getHitag() & kAttrFree) != 0) {
                continue;
            }

            viewBackupSpriteLoc(nSprite, pSprite);

            final XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
            if (pXSprite == null) {
                continue;
            }
            SPRITEHIT pSpriteHit = pXSprite.getSpriteHit();

            short oldcstat = 0;
            Sprite pOwner = null;

            if (pSprite.getOwner() >= 0) {
                pOwner = boardService.getSprite(actGetOwner(pSprite));
                if (IsDudeSprite(pOwner)) {
                    oldcstat = pOwner.getCstat();
                    pOwner.setCstat(pOwner.getCstat() & 0xFEFE);
                } else {
                    pOwner = null;
                }
            }

            pHitInfo.hitsect = -1;
            pHitInfo.hitwall = -1;
            pHitInfo.hitsprite = -1;
            if (pSprite.getLotag() == kMissileSprayFlame) {
                AirDrag(pSprite, 4096);
            }

            if (pXSprite.getTarget() != -1) {
                if (pSprite.getVelocityX() != 0 || pSprite.getVelocityY() != 0 || pSprite.getVelocityZ() != 0) {
                    int nTarget = pXSprite.getTarget();
                    BloodSprite pTarget = boardService.getSprite(nTarget);
                    if (pTarget != null) {
                        XSPRITE pXTarget = pTarget.getXSprite();
                        if (pTarget.getStatnum() == kStatDude && pXTarget != null && pXTarget.getHealth() != 0) {
                            int nAngle = EngineUtils.getAngle(-(pTarget.getY() - pSprite.getY()), pTarget.getX() - pSprite.getX());
                            int x = gMissileData[pSprite.getLotag() - kMissileBase].velocity;
                            int y = 0;

                            Point out = RotatePoint(x, y, (nAngle + 1536) & kAngleMask, 0, 0);
                            pSprite.setVelocityX(out.getX());
                            pSprite.setVelocityY(out.getY());

                            int dz = pTarget.getZ() - pSprite.getZ();
                            int zvel = dz / 10;
                            if (pSprite.getZ() > pTarget.getZ()) {
                                zvel = dz / -10;
                            }
                            pSprite.addVelocityZ(zvel);
                        }
                    }
                }
            }

            int hitInfo = -1;
            int nImpact = 1;

            int x, y, z;
            int nSector;
            int xvel = (int) (pSprite.getVelocityX() >> 12);
            int yvel = (int) (pSprite.getVelocityY() >> 12);
            int zvel = (int) (pSprite.getVelocityZ() >> 8);

            while (true) {
                x = pSprite.getX();
                y = pSprite.getY();
                z = pSprite.getZ();
                nSector = pSprite.getSectnum();

                GetSpriteExtents(pSprite);

                clipmoveboxtracenum = 1;

                int moveHit;
                if (!IsOriginalDemo()) {
                    moveHit = engine.clipmove(x, y, z, nSector, (long) xvel << 14, (long) yvel << 14, pSprite.getClipdist() << 2, (pSprite.getZ() - extents_zTop) / 4, (extents_zBot - pSprite.getZ()) / 4, CLIPMASK0);

                    x = clipmove_x;
                    y = clipmove_y;
                    z = clipmove_z;
                    nSector = clipmove_sectnum;
                } else {
                    moveHit = ClipMove(x, y, z, nSector, xvel, yvel, pSprite.getClipdist() << 2, (pSprite.getZ() - extents_zTop) / 4, (extents_zBot - pSprite.getZ()) / 4, CLIPMASK0);

                    x = clipm_px;
                    y = clipm_py;
                    z = clipm_pz;
                    nSector = (short) clipm_pnsectnum;
                }

                clipmoveboxtracenum = 3;

                if (nSector == -1) {
                    if (pOwner != null) {
                        pOwner.setCstat(oldcstat);
                    }

                    if (!IsOriginalDemo() && (hitInfo = HitScan(pSprite, z, xvel, yvel, zvel, pHitInfo, CLIPMASK0, 0)) != -1) {
                        if (pOwner != null) {
                            pOwner.setCstat(oldcstat);
                        }
                        actImpactMissile(pSprite, hitInfo);
                    }

                    continue NEXTMISSILE;
                }

                if (moveHit != 0) {
                    int nHitType = moveHit & kHitTypeMask;
                    if (nHitType == kHitSprite) {
                        pHitInfo.hitsprite = (short) (moveHit & kHitIndexMask);
                        hitInfo = SS_SPRITE;
                    }
                    if (nHitType == kHitWall) {
                        pHitInfo.hitwall = (short) (moveHit & kHitIndexMask);
                        if (boardService.getWall(pHitInfo.hitwall).getNextsector() == -1) {
                            hitInfo = SS_WALL;
                        } else {
                            engine.getzsofslope(boardService.getWall(pHitInfo.hitwall).getNextsector(), pSprite.getX(), pSprite.getY(), floorz, ceilz);
                            if (pSprite.getZ() > ceilz.get() && pSprite.getZ() < floorz.get()) {
                                hitInfo = SS_MASKED;
                            } else {
                                hitInfo = SS_WALL;
                            }
                        }
                    }
                }

                if (hitInfo != SS_MASKED) {
                    break;
                }

                int nXWall = boardService.getWall(pHitInfo.hitwall).getExtra();
                if (nXWall <= 0) {
                    break;
                }
                XWALL pXWall = xwall[nXWall];
                if (!pXWall.triggerVector) {
                    break;
                }
                trTriggerWall(pHitInfo.hitwall, pXWall, kCommandWallImpact);
                if ((boardService.getWall(pHitInfo.hitwall).getCstat() & kWallHitscan) != 0) {
                    break;
                }

                hitInfo = -1;
                if (nImpact-- <= 0) {
                    hitInfo = SS_WALL;
                    if (pOwner != null) {
                        pOwner.setCstat(oldcstat);
                    }
                    actImpactMissile(pSprite, hitInfo);
                    continue NEXTMISSILE;
                }
            }

            if (hitInfo != -1 && hitInfo != SS_SPRITE) {
                int nAngle = EngineUtils.getAngle((int) pSprite.getVelocityX(), (int) pSprite.getVelocityY());
                x -= mulscale(Cos(nAngle), 16, 30);
                y -= mulscale(Sin(nAngle), 16, 30);
                int moveDist = EngineUtils.qdist(pSprite.getVelocityX(), pSprite.getVelocityY());
                zvel -= (int) ((pSprite.getVelocityZ() << 8) / moveDist);
                nSector = engine.updatesector(x, y, nSector);
            }
            int ceilz, ceilhit, floorz, floorhit;
            GetZRange(x, y, z, nSector, pSprite.getClipdist() << 2, CLIPMASK0);
            ceilz = gz_ceilZ;
            ceilhit = gz_ceilHit;
            floorz = gz_floorZ;
            floorhit = gz_floorHit;
            GetSpriteExtents(pSprite);
            extents_zTop += zvel;
            extents_zBot += zvel;
            if (extents_zBot >= floorz) {
                pSpriteHit.floorHit = floorhit;
                hitInfo = 2;
                zvel += (floorz - extents_zBot);
            }
            if (extents_zTop <= ceilz) {
                pSpriteHit.ceilHit = ceilhit;
                hitInfo = 1;
                if (ceilz - extents_zTop > 0) {
                    zvel += ceilz - extents_zTop;
                }
            }
            pSprite.setX(x);
            pSprite.setY(y);
            pSprite.setZ(zvel + z);

            int uSector = engine.updatesector(x, y, nSector);
            if (uSector >= 0 && nSector != pSprite.getSectnum()) {
                if (uSector >= boardService.getSpriteCount()) {
                    throw new AssertException("boardService.isValidSector(uSector)");
                }
                engine.changespritesect(pSprite.getXvel(), (short) uSector);
            }
            checkWarping(pSprite);
            pHitInfo.hitsect = pSprite.getSectnum();
            pHitInfo.hitx = pSprite.getX();
            pHitInfo.hity = pSprite.getY();
            pHitInfo.hitz = pSprite.getZ();

            if (pOwner != null) {
                pOwner.setCstat(oldcstat);
            }

            if (hitInfo != -1) {
                actImpactMissile(pSprite, hitInfo);
            }
        }


        // process explosions
        for (ListNode<Sprite> node = boardService.getStatNode(kStatExplosion); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            Sprite pSprite = node.get();
            if ((pSprite.getHitag() & kAttrFree) != 0) {
                continue;
            }

            int nOwner = actGetOwner(pSprite);

            int nType = pSprite.getLotag();
            if (!(nType >= 0 && nType < 8)) {
                throw new AssertException("nType >= 0 && nType < kExplodeMax: " + nType);
            }

            int nXSprite = pSprite.getExtra();
            XSPRITE pXSprite = boardService.getXSprite(nXSprite);
            if (pXSprite == null) {
                throw new AssertException("pXSprite != null");
            }

            EXPLODE pExpl = gExplodeData[nType];
            int x = pSprite.getX();
            int y = pSprite.getY();
            int z = pSprite.getZ();

            int nSector = pSprite.getSectnum();
            int radius = pXSprite.getData4();
            if (pXSprite.getData4() <= 0) {
                radius = pExpl.radius;
            }

            gSectorExp[0] = -1;
            gWallExp[0] = -1;
            NearSectors(nSector, x, y, radius, gSectorExp, gSpriteExp, gWallExp);

            int nWall;
            for (int n = 0; n < kMaxXWalls; trTriggerWall(gWallExp[n], xwall[boardService.getWall(nWall).getExtra()], kCommandWallImpact), n++) {
                nWall = gWallExp[n];
                if (nWall == -1) {
                    break;
                }
            }

            for (ListNode<Sprite> dudeNode = boardService.getStatNode(kStatDude); dudeNode != null; dudeNode = dudeNode.getNext()) {
                final int nAffected = dudeNode.getIndex();
                final BloodSprite pDude = (BloodSprite) dudeNode.get();

                if ((pDude.getHitag() & kAttrFree) != 0) {
                    continue;
                }

                if ((gSpriteExp[pDude.getSectnum() >> 3] & (1 << (pDude.getSectnum() & 7))) != 0 && pXSprite.getData1() != 0 && CheckProximity(pDude, x, y, z, nSector, radius)) {
                    if (pExpl.used1 != 0 && pXSprite.getTarget() == 0) {
                        pXSprite.setTarget(pXSprite.getTarget() | 1);
                        actDamageSprite(nOwner, pDude, kDamageFall, (Random(pExpl.used2) + pExpl.used1) << 4);
                    }

                    if (pExpl.damageType != 0) {
                        ConcussSprite(nOwner, pDude, x, y, z, pExpl.damageType);
                    }

                    if (pExpl.burnCount != 0) {
                        XSPRITE pXDude = boardService.getXSprite(pDude.getExtra());
                        if (pXDude == null) {
                            throw new AssertException("pXDude != null");
                        }

                        if (pXDude.getBurnTime() == 0) {
                            evPostCallback(nAffected, SS_SPRITE, 0, 0);
                        }

                        pXDude.setBurnSource(pSprite.getOwner());
                        pXDude.setBurnTime(ClipHigh(pXDude.getBurnTime() + 4 * pExpl.burnCount, 2400));
                    }
                }
            }

            for (ListNode<Sprite> thingNode = boardService.getStatNode(kStatThing); thingNode != null; thingNode = thingNode.getNext()) {
                final int nAffected = thingNode.getIndex();
                final BloodSprite pThing = (BloodSprite) thingNode.get();
                if ((pThing.getHitag() & kAttrFree) != 0) {
                    continue;
                }

                XSPRITE pXThing = boardService.getXSprite(pThing.getExtra());
                if (pXThing == null) {
                    continue;
                }

                if ((gSpriteExp[pThing.getSectnum() >> 3] & (1 << (pThing.getSectnum() & 7))) != 0
                        && pXSprite.getData1() != 0
                        && pXThing.getLocked() == 0
                        && CheckProximity(pThing, x, y, z, nSector, radius)) {

                    if (pExpl.damageType != 0) {
                        ConcussSprite(nOwner, pThing, x, y, z, pExpl.damageType);
                    }

                    if (pExpl.burnCount != 0) {
                        if (pThing.getLotag() == kThingTNTBarrel && pXThing.getBurnTime() == 0) {
                            evPostCallback(nAffected, SS_SPRITE, 0, 0);
                        }

                        pXThing.setBurnSource(pSprite.getOwner());
                        pXThing.setBurnTime(ClipHigh(pXThing.getBurnTime() + 4 * pExpl.burnCount, 1200));
                    }
                }
            }


            for (int nAffected = connecthead; nAffected >= 0; nAffected = connectpoint2[nAffected]) {
                Sprite pPlayer = gPlayer[nAffected].pSprite;
                int dx = (pSprite.getX() - pPlayer.getX()) >> 4;
                int dy = (pSprite.getY() - pPlayer.getY()) >> 4;
                int dz = (pSprite.getZ() - pPlayer.getZ()) >> 8;
                int dist2 = dx * dx + dy * dy + dz * dz + (1 << kGlobalForceShift);

                int force = pXSprite.getData2() << 16;

                gPlayer[nAffected].explosion += force / dist2;
            }

            // if data4 == 1, do not remove explosion. This can be useful
            // when designer wants put explosion generator in map manually
            // via sprite statnum 2.

            if (pSprite.getHitag() != 0x0001) {
                pXSprite.setData1((short) ClipLow(pXSprite.getData1() - kFrameTicks, 0));
                pXSprite.setData2((short) ClipLow(pXSprite.getData2() - kFrameTicks, 0));
                pXSprite.setData3((short) ClipLow(pXSprite.getData3() - kFrameTicks, 0));
            }

            if (pXSprite.getData1() == 0 && pXSprite.getData2() == 0 && pXSprite.getData3() == 0 && seqFrame(SS_SPRITE, nXSprite) < 0) {
                actPostSprite(nSprite, kStatFree);
            }
        }

        // process traps for effects
        for (ListNode<Sprite> node = boardService.getStatNode(kStatTraps); node != null; node = node.getNext()) {
            Sprite pTrap = node.get();

            if ((pTrap.getHitag() & kAttrFree) != 0) {
                continue;
            }

            final int nXSprite = pTrap.getExtra();
            XSPRITE pXSprite = boardService.getXSprite(nXSprite);
            if (pXSprite != null) {
                switch (pTrap.getLotag()) {
                    case 452:
                        if (pXSprite.getState() != 0 && seqFrame(SS_SPRITE, nXSprite) < 0) {
                            int x = pTrap.getX();
                            int y = pTrap.getY();
                            int z = pTrap.getZ();

                            int dx = mulscale(Cos(pTrap.getAng()), ((long) pXSprite.getData1() << 23) / 120, 30);
                            int dy = mulscale(Sin(pTrap.getAng()), ((long) pXSprite.getData1() << 23) / 120, 30);

                            for (int i = 0; i < 2; i++) {
                                BloodSprite pSpawn = actSpawnEffect(32, pTrap.getSectnum(), x, y, z, 0);
                                if (pSpawn != null) {
                                    pSpawn.setVelocity(dx + BiRandom(34952),
                                    dy + BiRandom(34952),
                                    BiRandom(34952));
                                }
                                x += dx / 2 >> 12;
                                y += dy / 2 >> 12;
                            }
                            gVectorData[20].maxDist = pXSprite.getData1() << 9;
                            actFireVector(pTrap, 0, 0, Cos(pTrap.getAng()) >> 16, Sin(pTrap.getAng()) >> 16, BiRandom(34952), 20);
                        }
                        break;
                    case 454:
                        pXSprite.setData2((short) ClipLow(pXSprite.getData2() - kFrameTicks, 0));
                        break;
                }
            }
        }

        // process dudes for effects
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            final int nSprite = node.getIndex();
            final BloodSprite pSprite = (BloodSprite) node.get();
            final int nXSprite = pSprite.getExtra();
            XSPRITE pXSprite = pSprite.getXSprite();

            if (!IsDudeSprite(pSprite)) {
                continue;
            }

            if ((pSprite.getHitag() & kAttrFree) == 0 && pXSprite != null) {
                if (actGetBurnTime(pXSprite) > 0) {
                    if ((pSprite.getLotag() < kDudeBurning || pSprite.getLotag() > kDudeBloatedButcherBurning) && pSprite.getLotag() != kDudeTinyCalebburning && pSprite.getLotag() != kDudeTheBeastburning) {
                        pXSprite.setBurnTime(ClipLow(pXSprite.getBurnTime() - kFrameTicks, 0));
                    }

                    actDamageSprite(actGetBurnSource(pXSprite.getBurnSource()), pSprite, kDamageBurn, 2 * kFrameTicks);
                }

                // handle incarnations of custom dude
                if (pSprite.getLotag() == kGDXDudeUniversalCultist && pXSprite.getHealth() <= 0 && seqFrame(SS_SPRITE, nXSprite) < 0) {
                    XSPRITE pXIncarnation = getNextIncarnation(pXSprite);
                    if (pXIncarnation != null) {
                        Sprite pIncarnation = boardService.getSprite(pXIncarnation.getReference());
                        if (pIncarnation != null) {
                            pSprite.setLotag(pIncarnation.getLotag());
                            pSprite.setPal(pIncarnation.getPal());
                            pSprite.setShade(pIncarnation.getShade());
                            pSprite.setClipdist(pIncarnation.getClipdist());
                            pSprite.setXrepeat(pIncarnation.getXrepeat());
                            pSprite.setYrepeat(pIncarnation.getYrepeat());

                            pXSprite.setTxID(pXIncarnation.getTxID());
                            pXSprite.setCommand(pXIncarnation.getCommand());
                            pXSprite.setTriggerOn(pXIncarnation.isTriggerOn());
                            pXSprite.setTriggerOff(pXIncarnation.isTriggerOff());

                            pXSprite.setBurnTime(0);
                            pXSprite.setBurnSource(-1);

                            pXSprite.setData1(pXIncarnation.getData1());
                            pXSprite.setData2(pXIncarnation.getData2());
                            pXSprite.setData3(pXIncarnation.getData3());
                            pXSprite.setData4(pXIncarnation.getData4());

                            pXSprite.setDudeGuard(pXIncarnation.isDudeGuard());
                            pXSprite.setDudeDeaf(pXIncarnation.isDudeDeaf());
                            pXSprite.setDudeAmbush(pXIncarnation.isDudeAmbush());
                            pXSprite.setDudeFlag4(pXIncarnation.isDudeFlag4());

                            pXSprite.setBusyTime(pXIncarnation.getBusyTime());

                            switch (pSprite.getLotag()) {
                                case kGDXDudeUniversalCultist:
                                case kGDXGenDudeBurning:
                                    seqSpawn(getSeqStartId(pXSprite), SS_SPRITE, nXSprite, null);
                                    break;
                                default:
                                    seqSpawn(dudeInfo[pSprite.getLotag() - kDudeBase].seqStartID, SS_SPRITE, nXSprite, null);
                                    break;
                            }

                            if (pXSprite.getData4() <= 0) {
                                pXSprite.setHealth((dudeInfo[pSprite.getLotag() - kDudeBase].startHealth << 4) & 0xfff);
                            } else {
                                long hp = ((long) pXSprite.getData4() << 4) & 0xfff;
                                pXSprite.setHealth((int) (hp > 0 ? hp : 1));
                            }

                            aiInit(pSprite, false);
                            if (pXSprite.getTarget() == -1) {
                                aiSetTarget(pXSprite, 0, 0, 0);
                            } else {
                                aiSetTarget(pXSprite, pXSprite.getTarget());
                            }
                            aiActivateDude(pSprite, pXSprite);
                        }
                    }
                }

                if (pSprite.getLotag() == kDudeCerberus && pXSprite.getHealth() == 0 && seqFrame(3, nXSprite) < 0) {
                    pXSprite.setHealth(dudeInfo[kDudeCerberus2 - kDudeBase].startHealth << 4);
                    pSprite.setLotag(kDudeCerberus2);
                    if (pXSprite.getTarget() == -1) {
                        aiSetTarget(pXSprite, 0, 0, 0);
                    } else {
                        aiSetTarget(pXSprite, pXSprite.getTarget());
                    }
                    aiActivateDude(pSprite, pXSprite);
                }

                if (pXSprite.isProximity() && !pXSprite.isTriggered()) {
                    for (ListNode<Sprite> dudeNode = boardService.getStatNode(kStatDude); dudeNode != null; dudeNode = dudeNode.getNext()) {
                        BloodSprite prSprite = (BloodSprite) dudeNode.get();
                        XSPRITE prXSprite = prSprite.getXSprite();
                        if ((prSprite.getHitag() & kAttrFree) == 0 && prXSprite != null && prXSprite.getHealth() != 0 && IsPlayerSprite(prSprite) && CheckProximity(prSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), 128)) {
                            trTriggerSprite(nSprite, pXSprite, kCommandSpriteProximity);
                        }
                    }
                }

                if (IsPlayerSprite(pSprite)) {
                    int nPlayer = pSprite.getLotag() - kDudePlayer1;
                    PLAYER pPlayer = gPlayer[nPlayer];

                    /*
                     * Voodoo v1.00 if (pPlayer.voodooCount != 0) { int aimz =
                     * (int) pPlayer.aim.z; int dz = pPlayer.weaponAboveZ -
                     * pPlayer.pSprite.z; if(newHoriz) dz += 10 * pPlayer.horiz;
                     * if (UseAmmo(pPlayer, 9, 0) >= 8) { for (int i = 0; i < 4;
                     * i++) { int nAngle = ((pPlayer.voodooUnk +
                     * pPlayer.voodooAng) & 0x7FF) - 512;
                     * actFireVector(pPlayer.pSprite, 0, dz, costable[nAngle +
                     * 512] >> 16, costable[nAngle & 0x7FF] >> 16, aimz, 21);
                     * int v6 = ((pPlayer.voodooAng + 2048 - pPlayer.voodooUnk)
                     * & 0x7FF) - 512; actFireVector(pPlayer.pSprite, 0, dz,
                     * costable[v6 + 512] >> 16, costable[v6 & 0x7FF] >> 16,
                     * aimz, 21); pPlayer.voodooUnk += 5; } pPlayer.voodooCount
                     * = ClipLow( pPlayer.voodooCount - 1, 0); } else {
                     * pPlayer.voodooCount = 0; } }
                     */

                    if (pPlayer.handDamage && Chance(0x4000)) {
                        actDamageSprite(pSprite.getXvel(), pSprite, kDamageDrown, 12);
                    }
                    if (pPlayer.Underwater) {
                        boolean haveDivingSuit = inventoryCheck(pPlayer, kInventoryDivingSuit);
                        if (haveDivingSuit || pPlayer.godMode) {
                            pPlayer.airTime = 1200;
                        } else {
                            pPlayer.airTime = ClipLow(pPlayer.airTime - kFrameTicks, 0);
                        }

                        if (pPlayer.airTime < 1080 && getInventoryAmount(pPlayer, kInventoryDivingSuit) != 0) {
                            processInventory(pPlayer, kInventoryDivingSuit);
                        }

                        if (pPlayer.airTime != 0) {
                            pPlayer.drownEffect = 0;
                        } else {
                            if (pPlayer.pXsprite.getHealth() > 0) {
                                pPlayer.drownEffect += kFrameTicks;
                            }
                            if (bRandom() < (pPlayer.drownEffect >> 1)) {
                                actDamageSprite(pSprite.getXvel(), pSprite, kDamageDrown, 48);
                            }
                        }
                        if (pSprite.getVelocityX() != 0 || pSprite.getVelocityY() != 0) {
                            sfxStart3DSound(pSprite, 709, 100, 2);// PL_UW_SW
                        }
                        pPlayer.bubbleTime = ClipLow(pPlayer.bubbleTime - kFrameTicks, 0);
                    } else if (pGameInfo.nGameType == 0 && pPlayer.pXsprite != null && pPlayer.pXsprite.getHealth() != 0 && pPlayer.stayTime >= 1200 && Chance(256)) {
                        pPlayer.stayTime = -1;
                        sfxStart3DSound(pSprite, Random(11) + 3100, 0, 2);
                    }
                }
                ProcessTouchObjects(pSprite, pXSprite);
            }
        }

        // process dudes for movement
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            final int nSprite = node.getIndex();
            final BloodSprite pSprite = (BloodSprite) node.get();
            
            int nSector = pSprite.getSectnum();
            int zBot;

            if ((pSprite.getHitag() & kAttrFree) != 0) {
                continue;
            }

            if (!IsDudeSprite(pSprite)) {
                continue;
            }

            if (!IsOriginalDemo() && pGameInfo.nGameType == kNetModeOff && numplayers < 2) {
                if (gNoEnemies == 2) {
                    pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                    pSprite.setCstat(pSprite.getCstat() & ~(kSpriteBlocking | kSpriteHitscan | kSpritePushable));
                }
            }

            XSPRITE pXDude = boardService.getXSprite(pSprite.getExtra());
            if (pXDude == null) {
                throw new AssertException("pXDude != null");
            }

            viewBackupSpriteLoc(nSprite, pSprite);

            int vel = 128;
            // special sector processing
            if (boardService.getSector(nSector).getExtra() > 0) {
                int nXSector = boardService.getSector(nSector).getExtra();
                if (!(nXSector > 0 && nXSector < kMaxXSectors)) {
                    throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
                }
                if (xsector[nXSector].reference != nSector) {
                    throw new AssertException("xsector[nXSector].reference == nSector");
                }

                XSECTOR pXSector = xsector[nXSector];
                if (pXSector != null) {
                    GetSpriteExtents(pSprite);
                    zBot = extents_zBot;// zTop = extents_zTop;

                    if (engine.getflorzofslope((short) nSector, pSprite.getX(), pSprite.getY()) <= zBot) {
                        int panVel = 0;
                        int panAngle = pXSector.panAngle;
                        if (pXSector.panAlways || pXSector.state != 0 || pXSector.busy != 0) {
                            panVel = (pXSector.panVel & 0xFF) << 9;
                            if (!pXSector.panAlways && pXSector.busy != 0) {
                                panVel = mulscale(panVel, pXSector.busy, 16);
                            }
                        }

                        if ((boardService.getSector(nSector).getFloorstat() & kSectorRelAlign) != 0) {
                            panAngle = (GetWallAngle(boardService.getSector(nSector).getWallptr()) + panAngle + kAngle90) & kAngleMask;
                        }

                        int pushX = mulscale(panVel, Cos(panAngle), 30);
                        int pushY = mulscale(panVel, Sin(panAngle), 30);
                        pSprite.addVelocityX(pushX);
                        pSprite.addVelocityY(pushY);
                    }

                    if (pXSector.Underwater) {
                        vel = 5376;
                    }
                }
            }

            AirDrag(pSprite, vel);
            if ((pSprite.getHitag() & kAttrFalling) == 0 && pSprite.getVelocityX() == 0 && pSprite.getVelocityY() == 0 && pSprite.getVelocityZ() == 0 && floorVel[pSprite.getSectnum()] == 0 && ceilingVel[pSprite.getSectnum()] == 0) {
                continue;
            }

            MoveDude(pSprite);
        }

        // process flares to keep them burning on dudes
        for (ListNode<Sprite> node = boardService.getStatNode(kStatFlare); node != null; node = node.getNext()) {
            final int nSprite = node.getIndex();
            final BloodSprite pFlare = (BloodSprite) node.get();
            if ((pFlare.getHitag() & kAttrFree) != 0) {
                continue;
            }

            XSPRITE pXSprite = boardService.getXSprite(pFlare.getExtra());
            if (pXSprite == null) {
                throw new AssertException("pXSprite != null");
            }

            int nTarget = pXSprite.getTarget();
            if (nTarget < 0) {
                throw new AssertException("nTarget >= 0");
            }
            viewBackupSpriteLoc(nSprite, pFlare);
            BloodSprite pTarget = boardService.getSprite(nTarget);
            if (pTarget == null) {
                throw new AssertException("pTarget != null");
            }
            if (pTarget.getStatnum() == kStatFree) {
                actGenerateGibs(pFlare, 17, null, null);
                actPostSprite(pFlare.getXvel(), kStatFree);
            }

            XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());
            if (pXTarget != null && pXTarget.getHealth() != 0) {
                int nAngle = pTarget.getAng() + pXSprite.getGoalAng();
                int x = pTarget.getX() + mulscaler(Cos(nAngle), (long) pTarget.getClipdist() << 1, 30); // halfway into clipdist
                int y = pTarget.getY() + mulscaler(Sin(nAngle), (long) pTarget.getClipdist() << 1, 30);
                int z = pTarget.getZ() + pXSprite.getTargetZ();

                engine.setsprite(nSprite, x, y, z);
                pFlare.setVelocity(pTarget.getVelocityX(), pTarget.getVelocityY(), pTarget.getVelocityZ());
            } else {
                actGenerateGibs(pFlare, 17, null, null);
                actPostSprite(pFlare.getXvel(), kStatFree);
            }
        }

        if (gNoEnemies == 0) {
            aiProcessDudes();
        }

        actProcessEffects();

    }

    public static void actDistanceDamage(int nSprite, int x, int y, int z, int sectnum, int Distance, int minHit, int damageType, int damageHit, int flags, int BurnTime) {
        gSectorExp[0] = -1;
        gWallExp[0] = -1;
        NearSectors(sectnum, x, y, Distance, gSectorExp, gSpriteExp, gWallExp);

        int mDist = Distance << 4;
        if ((flags & 2) != 0) {
            for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
                int i = node.getIndex();
                if (i != nSprite || (flags & 1) != 0) {
                    final BloodSprite pTarget = (BloodSprite) node.get();
                    XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());
                    if (pXTarget != null && (pTarget.getHitag() & kAttrFree) == 0 && (gSpriteExp[pTarget.getSectnum() >> 3] & (1 << (pTarget.getSectnum() & 7))) != 0 && CheckProximity(pTarget, x, y, z, sectnum, mDist)) {
                        int dx = klabs(x - pTarget.getX());
                        int dy = klabs(y - pTarget.getY());
                        int dz = klabs(z - pTarget.getZ()) >> 4;
                        int dist = EngineUtils.sqrt(dx * dx + dy * dy + dz * dz);

                        if (dist <= mDist) {
                            int damage = (dist != 0) ? (damageHit * (mDist - dist) / mDist + minHit) : damageHit + minHit;
                            actDamageSprite(nSprite, pTarget, damageType, damage << 4);
                            if (BurnTime != 0) {


                                pXTarget.setBurnTime(ClipHigh(pXTarget.getBurnTime() + BurnTime, 2400));
                                pXTarget.setBurnSource(actSetBurnSource(nSprite));
                            }
                        }
                    }
                }
            }
        }

        if ((flags & 4) != 0) {
            for (ListNode<Sprite> node = boardService.getStatNode(kStatThing); node != null; node = node.getNext()) {
                BloodSprite pTarget = (BloodSprite) node.get();

                XSPRITE pXTarget = boardService.getXSprite(pTarget.getExtra());
                if (pXTarget != null && (pTarget.getHitag() & kAttrFree) == 0 && (gSpriteExp[pTarget.getSectnum() >> 3] & (1 << (pTarget.getSectnum() & 7))) != 0 && CheckProximity(pTarget, x, y, z, sectnum, mDist)) {
                    if (pXTarget.getLocked() == 0) {
                        int dx = klabs(x - pTarget.getX());
                        int dy = klabs(y - pTarget.getY());
                        int dist = EngineUtils.sqrt(dx * dx + dy * dy);

                        if (dist <= mDist) {
                            int damage = (dist != 0) ? minHit + damageHit * (mDist - dist) / mDist : damageHit + minHit;
                            actDamageSprite(nSprite, pTarget, damageType, damage << 4);
                            if (BurnTime != 0) {
                                pXTarget.setBurnTime(ClipHigh(pXTarget.getBurnTime() + BurnTime, 1200));
                                pXTarget.setBurnSource(actSetBurnSource(nSprite));
                            }
                        }
                    }
                }
            }
        }
    }

    public static int actCheckSpawnType(int nType) {
        if (nType > 3 && nType < 13 || nType > 36) {
            return nType;
        }
        return -1;
    }

    public static void actSpawnBlood(Sprite pSprite) {
        int nSector = pSprite.getSectnum();
        if (boardService.isValidSector(nSector)) {
            boolean find = FindSector(pSprite.getX(), pSprite.getY(), pSprite.getZ(), (short) nSector);
            if (find) {
                if (!cfg.gParentalLock || pGameInfo.nGameType > 0) {
                    BloodSprite pSpawn = actSpawnEffect(27, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
                    if (pSpawn != null) {
                        pSpawn.setAng(1024);
                        pSpawn.setVelocity(BiRandom(436906),
                        BiRandom(436906),
                        -Random(1092266) - 100);
                        evPostCallback(pSpawn.getXvel(), 3, 8, 6);
                    }
                }
            }
        }
    }

    public static void actSpawnTentacleBlood(Sprite pSprite) {
        int nSector = pSprite.getSectnum();
        if (boardService.isValidSector(nSector)) {
            boolean find = FindSector(pSprite.getX(), pSprite.getY(), pSprite.getZ(), (short) nSector);
            if (find) {
                if (!cfg.gParentalLock || pGameInfo.nGameType > 0) {
                    int nType = 54;
                    if (pSprite.getLotag() == kDudeGreenPod) {
                        nType = 53;
                    }

                    BloodSprite pSpawn = actSpawnEffect(nType, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
                    if (pSpawn != null) {
                        pSpawn.setAng(1024);
                        pSpawn.setVelocity(BiRandom(436906),
                        BiRandom(436906),
                        -Random(1092266) - 100);
                        evPostCallback(pSpawn.getXvel(), 3, 8, 18);
                    }
                }
            }
        }
    }

    public static void actSpawnSSheels(BloodSprite pSprite, int z, int offset, int vel) {
        int x = mulscale(Cos(pSprite.getAng() + kAngle90), offset, 30) + mulscale(Sin(pSprite.getAng() + kAngle90), (long) (pSprite.getClipdist() - 4) << 2, 30) + pSprite.getX();
        int y = mulscale(Cos(pSprite.getAng()), offset, 30) + mulscale(Sin(pSprite.getAng()), (long) (pSprite.getClipdist() - 4) << 2, 30) + pSprite.getY();
        BloodSprite pSpawn = actSpawnEffect(40 + Random(3), pSprite.getSectnum(), x, y, z, 0);
        if (pSpawn != null) {
            int velocity = (vel << 18) / 120 + BiRandom((vel / 4 << 18) / 120);
            int nAngle = pSprite.getAng() + BiRandom(56) + kAngle90;
            pSpawn.setVelocity(mulscale(Cos(nAngle), velocity, 30),
            mulscale(Sin(nAngle), velocity, 30),
            pSprite.getVelocityZ() - 0x20000 - ((long) BiRandom(20) << 18) / 120);
        }
    }

    public static void actSpawnTSheels(BloodSprite pSprite, int z, int offset, int vel) {
        int x = mulscale(Cos(pSprite.getAng() + kAngle90), offset, 30) + mulscale(Sin(pSprite.getAng() + kAngle90), (long) (pSprite.getClipdist() - 4) << 2, 30) + pSprite.getX();
        int y = mulscale(Cos(pSprite.getAng()), offset, 30) + mulscale(Sin(pSprite.getAng()), (long) (pSprite.getClipdist() - 4) << 2, 30) + pSprite.getY();

        BloodSprite pSpawn = actSpawnEffect(37 + Random(3), pSprite.getSectnum(), x, y, z, 0);
        if (pSpawn != null) {
            int velocity = (vel << 18) / 120 + BiRandom((vel / 4 << 18) / 120);
            int nAngle = pSprite.getAng() + BiRandom(56) + kAngle90;
            pSpawn.setVelocity(mulscale(Cos(nAngle), velocity, 30),
            mulscale(Sin(nAngle), velocity, 30),
           pSprite.getVelocityZ() - 0x20000 - ((long) BiRandom(40) << 18) / 120);
        }
    }

    public static void actDeleteEffect(int nSprite) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            checkEventList(nSprite, SS_SPRITE);
            final int nXSprite = pSprite.getExtra();
            if (nXSprite != -1) {
                seqKill(SS_SPRITE, nXSprite);
            }
            engine.deletesprite(nSprite);
        }
    }

    public static void actDeleteEffect2(int nSprite) {
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            final int nXSprite = pSprite.getExtra();
            if (nXSprite != -1) {
                seqKill(SS_SPRITE, nXSprite);
            }

            if (pSprite.getStatnum() != kStatFree) {
                actPostSprite(nSprite, kStatFree);
            }
        }
    }

    public static BloodSprite actSpawnEffect(int nType, short nSector, int x, int y, int z, int nBusy) {
        if (nSector < 0 || nSector > boardService.getSectorCount()) {
            return null;
        }
        if (!FindSector(x, y, z, nSector)) {
            return null;
        }

        if (!IsOriginalDemo() && eventQ.getSize() >= 768) //priorityQueue protect
        {
            return null;
        }

        if (cfg.gParentalLock && pGameInfo.nGameType <= 0) {
            if (actCheckSpawnType(nType) == -1) {
                return null;
            }
        }
        if ((nType & 128) == 0 && nType < kFXMax) {
            EFFECT nEffect = gEffectInfo[nType];

            if (boardService.getStatSize(kStatEffect) == 512) {
                int nSprite = -1;
                for (ListNode<Sprite> node = boardService.getStatNode(kStatEffect); node != null; node = node.getNext()) {
                    int i = node.getIndex();
                    if ((node.get().getHitag() & kAttrFree) == 0 || i == -1) {
                        nSprite = i;
                        break;
                    }
                }
                if (nSprite == -1) {
                    return null;
                }

                actDeleteEffect(nSprite);
            }

            BloodSprite pSpawn = boardService.getSprite(actSpawnSprite(nSector, x, y, z, 1, false));
            if (pSpawn == null) {
                return null;
            }

            pSpawn.setLotag((short) nType);
            pSpawn.setPicnum((short) nEffect.picnum);
            pSpawn.setCstat(pSpawn.getCstat() | nEffect.cstat);
            pSpawn.setShade((byte) nEffect.shade);
            pSpawn.setPal((byte) nEffect.pal);
            pSpawn.setDetail((byte) nEffect.detail);

            if (nEffect.xrepeat > 0) {
                pSpawn.setXrepeat((short) nEffect.xrepeat);
            }
            if (nEffect.yrepeat > 0) {
                pSpawn.setYrepeat((short) nEffect.yrepeat);
            }

            if ((nEffect.flags & 1) != 0 && Chance(0x4000)) {
                pSpawn.setCstat(pSpawn.getCstat() ^ kSpriteFlipX);
            }
            if ((nEffect.flags & 2) != 0 && Chance(0x4000)) {
                pSpawn.setCstat(pSpawn.getCstat() ^ kSpriteFlipY);
            }

            if (nEffect.seqId != 0) {
                seqSpawn(nEffect.seqId, SS_SPRITE, boardService.insertXSprite(pSpawn), null);
            }

            if (nBusy == 0) {
                nBusy = nEffect.count;
            }

            if (nBusy != 0) {
                evPostCallback(pSpawn.getXvel(), SS_SPRITE, Integer.toUnsignedLong(BiRandom(nBusy >> 1) + nBusy), 1);
            }

            return pSpawn;
        }
        return null;
    }

    public static int GetDataVal(Sprite pSprite, int data) {
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            return -1;
        }
        int[] rData = new int[4];

        rData[0] = pXSprite.getData1();
        rData[2] = pXSprite.getData3();
        rData[1] = pXSprite.getData2();
        rData[3] = pXSprite.getData4();

        return rData[data];
    }

    public static int GetRandDataVal(int[] rData, Sprite pSprite) {

        if (rData != null && pSprite != null) {
            return -1;
        } else if (pSprite != null) {
            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
            if (pXSprite == null) {
                return -1;
            }

            rData = new int[4];

            rData[0] = pXSprite.getData1();
            rData[2] = pXSprite.getData3();
            rData[1] = pXSprite.getData2();
            rData[3] = pXSprite.getData4();

        } else if (rData == null) {
            return -1;
        }

        int random;
        // randomize only in case if at least 2 data fields are not empty
        int a = 1;
        int b = -1;
        for (int i = 0; i <= 3; i++) {
            if (rData[i] == 0) {
                if (a++ > 2) {
                    return -1;
                }
            } else if (b == -1) {
                b++;
            }
        }

        // try randomize few times
        int maxRetries = 10;
        while (maxRetries > 0) {
            if (pGameInfo.nGameType == kNetModeOff && numplayers < 2) {
                random = (int) (4 * Math.random());
            } else {
                random = Gameutils.Random(3);
            }

            if (rData[random] > 0) {
                return rData[random];
            }
            maxRetries--;
        }

        // if nothing, get first found data value from top
        return rData[b];
    }

    public static void DropRandomPickupObject(Sprite pSprite, short prevItem) {
        Sprite pSprite2;

        int[] rData = new int[4];
        int selected;
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        rData[0] = pXSprite.getData1();
        rData[2] = pXSprite.getData3();
        rData[1] = pXSprite.getData2();
        rData[3] = pXSprite.getData4();

        // randomize only in case if at least 2 data fields fits.
        for (int i = 0; i <= 3; i++) {
            if (rData[i] < kWeaponItemBase || rData[i] >= kItemMax) {
                rData[i] = 0;
            }
        }

        int maxRetries = 9;
        while ((selected = GetRandDataVal(rData, null)) == prevItem) {
            if (maxRetries-- <= 0) {
                break;
            }
        }

        if (selected > 0) {
            XSPRITE pXSource = boardService.getXSprite(pSprite.getExtra());
            pSprite2 = DropPickupObject(pSprite, selected);
            if (pSprite2 != null) {

                pSprite2.setX(pSprite.getX());
                pSprite2.setY(pSprite.getY());
                pSprite2.setZ(pSprite.getZ());
                pXSource.setDropMsg(pSprite2.getLotag()); // store dropped item lotag in dropMsg

                // sometimes voxels does not load.
                tileLoadVoxel(pSprite2.getPicnum());

                if ((pSprite.getHitag() & 0x0001) != 0 && boardService.insertXSprite(pSprite2) > 0) {
                    XSPRITE pXSprite2 = boardService.getXSprite(pSprite2.getExtra());

                    // inherit spawn sprite trigger settings, so designer can send command when item picked up.
                    pXSprite2.setTxID(pXSource.getTxID());
                    pXSprite2.setCommand(pXSource.getCommand());
                    pXSprite2.setTriggerOn(pXSource.isTriggerOn());
                    pXSprite2.setTriggerOff(pXSource.isTriggerOff());

                    pXSprite2.setPickup(true);
                }
            }
        }
    }

    public static Sprite spawnRandomDude(BloodSprite pSprite) {

        Sprite pSprite2 = null;
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());

        if (pXSprite != null) {
            int[] rData = new int[4];
            int selected;
            rData[0] = pXSprite.getData1();
            rData[1] = pXSprite.getData2();
            rData[2] = pXSprite.getData3();
            rData[3] = pXSprite.getData4();

            // randomize only in case if at least 2 data fields fits.
            for (int i = 0; i <= 3; i++) {
                if (rData[i] < kDudeBase || rData[i] >= kDudeMax) {
                    rData[i] = 0;
                }
            }

            if ((selected = GetRandDataVal(rData, null)) > 0) {
                pSprite2 = actSpawnDude(pSprite, selected, -1);
            }
        }

        return pSprite2;
    }

    public static void actInit(boolean loadgame, boolean isOriginal) throws WarningException {
        for (ListNode<Sprite> node = boardService.getStatNode(kStatItem); node != null; node = node.getNext()) {
            Sprite pSprite = node.get();
            if (pSprite.getLotag() == 44) {// Voodoo doll
                pSprite.setLotag(70);
            }

            switch (pSprite.getLotag()) {
                case kItemKey1:
                case kItemKey2:
                case kItemKey3:
                case kItemKey4:
                case kItemKey5:
                case kItemKey6:
                case kItemKey7:
                    XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                    if (pXSprite != null) {
                        pXSprite.setRespawn(0); //keys should respawning always
                    }
                    break;
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(kStatTraps), nextNode; node != null; node = nextNode) {
            nextNode = node.getNext();
            Sprite pSprite = node.get();
            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
            if (pXSprite == null) {
                Console.out.println("Deleting sprite " + node.getIndex() + ", pXSprite shouldn't be null!", OsdColor.RED);
                boardService.deletesprite(node.getIndex());
                continue;
            }

            if (pSprite.getLotag() == kThingFlameTrap) {
                if (pSprite.getPicnum() == 2183 && pXSprite.getState() == 0) { // firing picnum and not actived
                    seqSpawn(getSeq(kMGunClose), SS_SPRITE, pSprite.getExtra(), null);
                }
            }

            if (pSprite.getLotag() == kThingHiddenExploder) {
                pXSprite.setState(0);

                if (pXSprite.getWaitTime() < 1) {
                    pXSprite.setWaitTime(1);
                }

                pSprite.setCstat(pSprite.getCstat() & ~kSpriteBlocking);
                pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
            }
        }

        // initialize all things
        for (ListNode<Sprite> node = boardService.getStatNode(kStatThing), nextNode; node != null; node = nextNode) {
            nextNode = node.getNext();
            final int nSprite = node.getIndex();
            final BloodSprite pSprite = (BloodSprite) node.get();
            final int nXSprite = pSprite.getExtra();
            final XSPRITE pXSprite = boardService.getXSprite(nXSprite);
            if (pXSprite == null) {
                Console.out.println("Deleting sprite " + nSprite + ", pXSprite shouldn't be null!", OsdColor.RED);
                boardService.deletesprite(nSprite);
                continue;
            }

            int nThingIndex = pSprite.getLotag() - kThingBase;
            if (nThingIndex >= thingInfo.length) {
                Console.out.println("Deleting sprite " + nSprite + ", Wrong ThingInfo index " + nThingIndex, OsdColor.RED);
                boardService.deletesprite(nSprite);
                continue;
            }

            THINGINFO pThinkInfo = thingInfo[nThingIndex];
            pXSprite.setHealth(pThinkInfo.startHealth << 4);
            pSprite.setClipdist(pThinkInfo.clipdist);
            pSprite.setHitag((short) pThinkInfo.flags);
            if ((pSprite.getHitag() & kAttrGravity) != 0) {
                pSprite.setHitag(pSprite.getHitag() | kAttrFalling);
            }

            pSprite.setVelocity(0, 0, 0);
            switch (pSprite.getLotag()) {
                case kThingTNTProx:
                case kGDXThingTNTProx:
                case kThingMachineGun:
                    pXSprite.setState(0);
                    break;
                default:
                    if (pSprite.getLotag() != kThingGib) {
                        pXSprite.setState(1);
                    }
                    break;
            }

            SeqInst pInst = GetInstance(SS_SPRITE, nXSprite);
            if (pInst != null && pInst.isPlaying()) {
                if ((game.getCache().contains(pInst.getSeqIndex(), "SEQ"))) {
                    seqSpawn(pInst.getSeqIndex(), SS_SPRITE, nXSprite, null);
                }
            }
        }

        if (pGameInfo.nMonsterSettings != 0) {
            levelCalcKills();
            for (int i = 0; i < dudeInfo.length; i++) {
                for (int j = 0; j < 7; j++) {
                    dudeInfo[i].damageShift[j] = mulscale(dudeInfo[i].startDamage[j], pSkillShift[pGameInfo.nEnemyDamage], 8);
                }
            }

            for (ListNode<Sprite> node = boardService.getStatNode(kStatDude), nextNode; node != null; node = nextNode) {
                nextNode = node.getNext();
                final int nSprite = node.getIndex();
                final BloodSprite pSprite = (BloodSprite) node.get();
                final int nXSprite = pSprite.getExtra();
                final XSPRITE pXSprite = boardService.getXSprite(nXSprite);
                if (pXSprite == null) {
                    Console.out.println("Deleting sprite " + nSprite + ", pXSprite shouldn't be null!", OsdColor.RED);
                    boardService.deletesprite(nSprite);
                    continue;
                }

                int dudeIndex = pSprite.getLotag() - kDudeBase;
                if (!IsPlayerSprite(pSprite)) {
                    if (!isOriginal) {
                        if ((pSprite.getCstat() & 48) != 0) // GDX flat sprites fix
                        {
                            pSprite.setCstat(pSprite.getCstat() & ~(pSprite.getCstat() & 48));
                        }
                        pSprite.setXoffset(0);
                        pSprite.setYoffset(0);
                    }

                    switch (pSprite.getLotag()) {
                        case kDudeMotherPod: // Fake Dude type
                            break;
                        case kGDXDudeUniversalCultist:
                        case kGDXGenDudeBurning:
                            pSprite.setCstat(pSprite.getCstat() | (kSpriteBlocking | kSpriteHitscan | kSpritePushable));
                            break;
                        default:
                            if (dudeIndex >= dudeInfo.length) {
                                Console.out.println("Deleting sprite " + nSprite + ", Wrong DudeInfo index " + dudeIndex, OsdColor.RED);
                                boardService.deletesprite(nSprite);
                                continue;
                            }

                            pSprite.setCstat(pSprite.getCstat() | (kSpriteBlocking | kSpriteHitscan | kSpritePushable));
                            pSprite.setClipdist(dudeInfo[dudeIndex].clipdist);
                            break;
                    }

                    pSprite.setVelocity(0, 0, 0);

                    if (!loadgame) {
                        // By NoOne: add a way to set custom hp for every enemy.
                        if (pXSprite.getData4() <= 0 || IsOriginalDemo()) {
                            pXSprite.setHealth((dudeInfo[dudeIndex].startHealth << 4) & 0xfff);
                        } else {
                            long hp = ((long) pXSprite.getData4() << 4) & 0xfff;
                            pXSprite.setHealth((int) (hp > 0 ? hp : 1));
                        }
                    }

                }

                // by NoOne: Custom Dude stores it's SEQ in data2
                switch (pSprite.getLotag()) {
                    case kGDXDudeUniversalCultist:
                    case kGDXGenDudeBurning:
                        seqSpawn(getSeqStartId(pXSprite), SS_SPRITE, nXSprite, null);
                        break;
                    default:
                        int seqStartId = dudeInfo[dudeIndex].seqStartID + kSeqDudeIdle;
                        if (game.getCache().contains(seqStartId, seq)) {
                            seqSpawn(seqStartId, SS_SPRITE, nXSprite, null);
                        }
                        break;
                }
            }

            aiInit(isOriginal);

        } else {
            for (ListNode<Sprite> node = boardService.getStatNode(kStatDude), nextNode; node != null; node = nextNode) {
                nextNode = node.getNext();

                int nSprite = node.getIndex();
                Sprite pSprite = node.get();
                final XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                if (pXSprite.getKey() > 0) {
                    DropPickupObject(pSprite, kItemKey1 + pXSprite.getKey() - 1);
                }
                engine.deletesprite(nSprite);
            }
        }
    }

    public static int actGetRespawnTime(Sprite pSprite) {
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite != null) {
            if (IsDudeSprite(pSprite)) {
                if (!IsPlayerSprite(pSprite) && (pXSprite.getRespawn() == 2 || (pXSprite.getRespawn() != 1 && pGameInfo.nMonsterSettings == 2))) {
                    return pGameInfo.nMonsterRespawnTime;
                }
            }

            if (IsWeaponSprite(pSprite)) {
                if (pXSprite.getRespawn() == 3 || pGameInfo.nWeaponSettings == 1) {
                    return 0;
                }
                if (pXSprite.getRespawn() != 1 && pGameInfo.nWeaponSettings != 0) {
                    return pGameInfo.nWeaponRespawnTime;
                }
            }

            if (IsAmmoSprite(pSprite)) {
                if (pXSprite.getRespawn() == 2 || pXSprite.getRespawn() != 1 && pGameInfo.nWeaponSettings != 0) {
                    return pGameInfo.nWeaponRespawnTime;
                }
            }

            if (IsItemSprite(pSprite)) {
                if (pXSprite.getRespawn() == 3 && pGameInfo.nGameType == 1) {
                    return 0;
                }

                if (pXSprite.getRespawn() == 2 || (pXSprite.getRespawn() != 1 && pGameInfo.nItemSettings != 0)) {
                    switch (pSprite.getLotag()) {
                        case kItemLtdInvisibility:
                        case kItemGunsAkimbo:
                        case kItemReflectiveShots:
                            return pGameInfo.nSpecialRespawnTime;
                        case kItemInvulnerability:
                            return 2 * pGameInfo.nSpecialRespawnTime;
                        case kItemKey1:
                        case kItemKey2:
                        case kItemKey3:
                        case kItemKey4:
                        case kItemKey5:
                        case kItemKey6:
                        case kItemKey7:
                            return 200;
                        default:
                            return pGameInfo.nItemRespawnTime;
                    }
                }
            }
        }

        return -1;
    }

    public static boolean actCheckRespawn(Sprite pSprite) {
        int nSprite = pSprite.getXvel();
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite != null) {

            int respawnTime = actGetRespawnTime(pSprite);
            if (respawnTime < 0) {
                return false;
            }

            pXSprite.setRespawnPending(1);

            // don't go through respawn dot stages for things
            if (pSprite.getLotag() >= kThingBase && pSprite.getLotag() < kThingMax) {
                pXSprite.setRespawnPending(3);
                if (pSprite.getLotag() == kThingTNTBarrel) {
                    pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                }
            }

            if (respawnTime > 0) {
                if (pXSprite.getRespawnPending() == 1) {
                    respawnTime = mulscale(respawnTime, 0xA000, 16);
                }

                pSprite.setOwner(pSprite.getStatnum()); // store the sprite's status list for respawning
                actPostSprite(nSprite, kStatRespawn);
                pSprite.setHitag(pSprite.getHitag() | kAttrRespawn);
                if (!IsDudeSprite(pSprite)) {
                    pSprite.setCstat(pSprite.getCstat() & (~kSpriteBlocking & ~kSpriteHitscan));
                    Vector3 kSprite = ((BloodSprite) pSprite).getKSprite();
                    pSprite.setX((int) kSprite.x);
                    pSprite.setY((int) kSprite.y);
                    pSprite.setZ((int) kSprite.z);
                }
                evPostCallback(nSprite, SS_SPRITE, Integer.toUnsignedLong(respawnTime), kCallbackRespawn);
            }
            return true;
        }
        return false; // indicate sprite will not respawn, and should be deleted, exploded, etc.
    }

    public static void actGibObject(Sprite pSprite, XSPRITE pXSprite) {
        int gib1 = ClipRange(pXSprite.getData1(), 0, kGibMax);
        int gib2 = ClipRange(pXSprite.getData2(), 0, kGibMax);
        int gib3 = ClipRange(pXSprite.getData3(), 0, kGibMax);
        int sndId = pXSprite.getData4();
        int drop = pXSprite.getDropMsg();

        if (gib1 > 0) {
            actGenerateGibs(pSprite, gib1 - 1, null, null);
        }
        if (gib2 > 0) {
            actGenerateGibs(pSprite, gib2 - 1, null, null);
        }
        if (gib3 > 0 && pXSprite.getBurnTime() != 0) {
            actGenerateGibs(pSprite, gib3 - 1, null, null);
        }
        if (sndId > 0) {
            sfxCreate3DSound(pSprite.getX(), pSprite.getY(), pSprite.getZ(), sndId, pSprite.getSectnum());
        }
        if (drop > 0) {
            DropPickupObject(pSprite, drop);
        }

        if ((pSprite.getCstat() & kSpriteInvisible) == 0) {
            if ((pSprite.getHitag() & kAttrRespawn) == 0) {
                actPostSprite(pSprite.getXvel(), kStatFree);
            }
        }
    }

    public static BloodSprite actCloneSprite(BloodSprite pSourceSprite, int nStatus) {
        int nSprite = engine.insertsprite(pSourceSprite.getSectnum(), (short) nStatus);
        if (nSprite < 0) {
            // dprintf("Out of sprites -- reclaiming sprite from purge list\n");

            ListNode<Sprite> node = boardService.getStatNode(kStatPurge);
            if (node == null) {
                throw new AssertException("nSprite >= 0");
            }

            nSprite = node.getIndex();
            engine.changespritesect((short) nSprite, pSourceSprite.getSectnum());
            actPostSprite((short) nSprite, nStatus);
        }
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("pSprite != null");
        }

        pSprite.setX(pSourceSprite.getX());
        pSprite.setY(pSourceSprite.getY());
        pSprite.setZ(pSourceSprite.getZ());

        pSprite.setVelocity(pSourceSprite.getVelocityX(), 
                pSourceSprite.getVelocityY(), 
                pSourceSprite.getVelocityZ());
        
        pSprite.setHitag(0);
        boardService.insertXSprite(pSprite);
        return pSprite;
    }

    public static BloodSprite actSpawnDude(BloodSprite pSprite, int nType, int nDist) {
        XSPRITE pXSource = pSprite.getXSprite();
        BloodSprite pDude = actCloneSprite(pSprite, kStatDude);

        XSPRITE pXDude = boardService.getXSprite(pDude.getExtra());
        int x, y, z = pSprite.getZ(), nAngle = pSprite.getAng();
        if (nDist > 0) {
            x = pSprite.getX() + mulscaler(nDist, Cos(nAngle), 30);
            y = pSprite.getY() + mulscaler(nDist, Sin(nAngle), 30);
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
        }

        pDude.setLotag((short) nType);
        pDude.setAng((short) nAngle);
        engine.setsprite(pDude.getXvel(), x, y, z);
        pDude.setCstat(pDude.getCstat() | 0x1101);
        pDude.setClipdist(dudeInfo[nType - kDudeBase].clipdist);
        pXDude.setHealth(dudeInfo[nType - kDudeBase].startHealth << 4);
        pXDude.setRespawn(1); //GDX 18.12.2019 CommonLoon102's fix: disable respawn for spawned dudes

        if ((game.getCache().contains(dudeInfo[nType - kDudeBase].seqStartID + kSeqDudeIdle, seq))) {
            seqSpawn(dudeInfo[nType - kDudeBase].seqStartID + kSeqDudeIdle, SS_SPRITE, pDude.getExtra(), null);
        }

        if ((pSprite.getHitag() & 0x0001) != 0 && pSprite.getLotag() == kDudeSpawn) {
            //inherit pal?
            if (pDude.getPal() <= 0) {
                pDude.setPal(pSprite.getPal());
            }

            if (pXSource != null) {
                // inherit spawn sprite trigger settings, so designer can count monsters.
                pXDude.setTxID(pXSource.getTxID());
                pXDude.setCommand(pXSource.getCommand());
                pXDude.setTriggerOn(pXSource.isTriggerOn());
                pXDude.setTriggerOff(pXSource.isTriggerOff());

                // inherit drop items
                pXDude.setDropMsg(pXSource.getDropMsg());

                // inherit dude flags
                pXDude.setDudeDeaf(pXSource.isDudeDeaf());
                pXDude.setDudeGuard(pXSource.isDudeGuard());
                pXDude.setDudeAmbush(pXSource.isDudeAmbush());
                pXDude.setDudeFlag4(pXSource.isDudeFlag4());

                // the enemy can be available via rx command send.
                //if (pXDude.rxID > 0)
                //ru.m210projects.Blood.EVENT.evPut();
            }
        }

        aiInit(pDude, IsOriginalDemo());
        return pDude;
    }

    public static Sprite actSpawnCustomDude(BloodSprite pSprite, int nDist) {
        XSPRITE pXSource = pSprite.getXSprite();
        BloodSprite pDude = actCloneSprite(pSprite, kStatDude);
        XSPRITE pXDude = boardService.getXSprite(pDude.getExtra());

        int x, y, z = pSprite.getZ(), nAngle = pSprite.getAng(), nType = kGDXDudeUniversalCultist;

        if (nDist > 0) {
            x = pSprite.getX() + mulscaler(nDist, Cos(nAngle), 30);
            y = pSprite.getY() + mulscaler(nDist, Sin(nAngle), 30);
        } else {
            x = pSprite.getX();
            y = pSprite.getY();
        }

        pDude.setLotag((short) nType);
        pDude.setAng((short) nAngle);
        engine.setsprite(pDude.getXvel(), x, y, z);
        pDude.setCstat(pDude.getCstat() | 0x1101);
        pDude.setClipdist(dudeInfo[nType - kDudeBase].clipdist);

        // spawn seq
        seqSpawn(getSeqStartId(pXDude), SS_SPRITE, pDude.getExtra(), null);

        if (pXSource != null) {
            // inherit weapon, seq and sound settings.
            pXDude.setData1(pXSource.getData1());
            pXDude.setData2(pXSource.getData2());
            pXDude.setData3(pXSource.getData3());

            // inherit custom hp settings
            if (pXSource.getData4() <= 0) {
                pXDude.setHealth((dudeInfo[nType - kDudeBase].startHealth << 4) & 0xfff);
            } else {
                long hp = ((long) pXSource.getData4() << 4) & 0xfff;
                pXDude.setHealth((int) (hp > 0 ? hp : 1));
            }

            // inherit movement speed.
            pXDude.setBusyTime(pXSource.getBusyTime());
            if ((pSprite.getHitag() & 0x0001) != 0) {
                //inherit pal?
                if (pDude.getPal() <= 0) {
                    pDude.setPal(pSprite.getPal());
                }

                // inherit clipdist?
                if (pSprite.getClipdist() > 0) {
                    pDude.setClipdist(pSprite.getClipdist());
                }

                // inherit spawn sprite trigger settings, so designer can count monsters.
                pXDude.setTxID(pXSource.getTxID());
                pXDude.setCommand(pXSource.getCommand());
                pXDude.setTriggerOn(pXSource.isTriggerOn());
                pXDude.setTriggerOff(pXSource.isTriggerOff());

                // inherit drop items
                pXDude.setDropMsg(pXSource.getDropMsg());

                // inherit required key so it can be dropped
                pXDude.setKey(pXSource.getKey());

                // inherit dude flags
                pXDude.setDudeDeaf(pXSource.isDudeDeaf());
                pXDude.setDudeGuard(pXSource.isDudeGuard());
                pXDude.setDudeAmbush(pXSource.isDudeAmbush());
                pXDude.setDudeFlag4(pXSource.isDudeFlag4());
            }
        }

        aiInit(pDude, false);
        return pDude;
    }

    public static BloodSprite actSpawnThing(int nSector, int x, int y, int z, int nThingType) {
        if (!(nThingType >= kThingBase && nThingType < kThingMax)) {
            throw new AssertException("nThingType >= kThingBase && nThingType < kThingMax");
        }

        BloodSprite pThing = boardService.getSprite(actSpawnSprite(nSector, x, y, z, kStatThing, true));
        if (pThing == null) {
            return null;
        }

        pThing.setLotag(nThingType);
        XSPRITE pXThing = boardService.getXSprite(pThing.getExtra());
        if (pXThing == null) {
            throw new AssertException("pXThing != null");
        }

        THINGINFO pThinkInfo = thingInfo[nThingType - kThingBase];

        pXThing.setHealth(pThinkInfo.startHealth << 4);
        pThing.setCstat(pThing.getCstat() | pThinkInfo.cstat);
        pThing.setClipdist(pThinkInfo.clipdist);
        pThing.setHitag((short) pThinkInfo.flags);
        if ((pThing.getHitag() & kAttrGravity) != 0) {
            pThing.setHitag(pThing.getHitag() | kAttrFalling);
        }

        pThing.setPicnum((short) pThinkInfo.picnum);
        pThing.setShade((byte) pThinkInfo.shade);
        pThing.setPal((byte) pThinkInfo.pal);

        if (pThinkInfo.xrepeat != 0) {
            pThing.setXrepeat((short) pThinkInfo.xrepeat);
        }
        if (pThinkInfo.yrepeat != 0) {
            pThing.setYrepeat((short) pThinkInfo.yrepeat);
        }

        show2dsprite.setBit(pThing.getXvel());
        switch (nThingType) {
            case 432:
                pXThing.setData1(0);
                pXThing.setData3(0);
                pXThing.setData4(0);
                pXThing.setState(1);
                pXThing.setTriggered(false);
                pXThing.setTriggerOnce(true);
                break;
            case kThingLifeLeech:
            case kGDXThingCustomDudeLifeLeech:
                pXThing.setData1(0);
                pXThing.setData3(0);
                pXThing.setData4(0);
                pXThing.setState(1);
                pXThing.setTriggered(false);
                pXThing.setTriggerOnce(false);
                break;
		/*case kGDXThingCalebHat:
			pXThing.state = 1;
			pXThing.data1 = 0;
			pXThing.data2 = 0;
			pXThing.data3 = 0;
			pXThing.data4 = 0;
			pXThing.isTriggered = false;
			pXThing.triggerOnce = true;
			break;*/
            case kThingZombieHead:
                pXThing.setData1(8);
                pXThing.setData4(318);
                pXThing.setTargetX(pXThing.getTargetX() | gFrameClock + 180);
                pXThing.setState(1);
                pXThing.setLocked(1);
                pXThing.setTriggered(false);
                pXThing.setTriggerOnce(false);
                break;
            case kThingGibSmall:
            case kThingGib:
                if (nThingType == kThingGibSmall) {
                    pXThing.setData1(19);
                } else {
                    pXThing.setData1(8);
                }
                pXThing.setData2(0);
                pXThing.setData3(0);
                pXThing.setData4(319);

                pXThing.setTargetX(pXThing.getTargetX() | gFrameClock + 180);
                pXThing.setState(1);
                pXThing.setLocked(1);
                pXThing.setTriggered(false);
                pXThing.setTriggerOnce(false);

                break;
            case kThingTNTStick:
            case kThingTNTBundle:
                evPostCallback(pThing.getXvel(), SS_SPRITE, 0, kCommandToggleLock);
                sfxStart3DSound(pThing, 450, 0, 0);
                break;
            case kThingSprayBundle:
                evPostCallback(pThing.getXvel(), SS_SPRITE, 0, kCommandToggleLock);
                break;
        }
        return pThing;
    }

    /***********************************************************************
     * actSpawnSprite()
     * Spawns a new sprite at the specified world coordinates.
     **********************************************************************/
    public static int actSpawnSprite(int nSector, int x, int y, int z, int nStatus, boolean bAddXSprite) {
        if (!boardService.isValidSector(nSector)) {
            return -1;
        }

        BloodSprite pSprite;
        int nSprite = engine.insertsprite(nSector, (short) nStatus);
        if (nSprite >= 0) {
            pSprite = boardService.getSprite(nSprite);
            if (pSprite == null) {
                throw new AssertException("nSprite >= 0");
            }

            pSprite.setExtra(-1);
            pSprite.setXSprite(null);
        } else {
            ListNode<Sprite> node = boardService.getStatNode(kStatPurge);
            nSprite = node.getIndex();
            pSprite = boardService.getSprite(nSprite);
            if (pSprite == null) {
                throw new AssertException("nSprite >= 0");
            }

            engine.changespritesect((short) nSprite, nSector);
            actPostSprite(nSprite, nStatus);
        }

        engine.setsprite(nSprite, x, y, z);
        pSprite.setLotag(0);

        // optionally create an xsprite
        if (bAddXSprite && pSprite.getExtra() == -1) {
            boardService.insertXSprite(pSprite);
        }

        return nSprite;
    }

    public static Sprite DropPickupObject(Sprite pActor, int nObject) {
        if (pActor.getStatnum() >= kMaxStatus) {
            return null;
        }
        if (!((nObject >= kItemBase && nObject < kItemMax) || (nObject >= kAmmoItemBase && nObject < kAmmoItemMax) || (nObject >= kWeaponItemBase && nObject < kWeaponItemMax))) {
            return null;
        }

        int nSector = engine.updatesector(pActor.getX(), pActor.getY(), pActor.getSectnum());
        if (!IsOriginalDemo() && !boardService.isValidSector(nSector)) {
            if (boardService.isValidSector(pActor.getSectnum())) {
                nSector = pActor.getSectnum();
            } else {
                return null;
            }
        }

        int floorz = engine.getflorzofslope(nSector, pActor.getX(), pActor.getY());
        int nSprite = actSpawnSprite(nSector, pActor.getX(), pActor.getY(), floorz, kStatItem, false);
        Sprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            pSprite.setCstat(pSprite.getCstat() & (~kSpriteBlocking & ~kSpriteHitscan));

            if (nObject >= kItemBase) {
                int nItemIndex = nObject - kItemBase;
                pSprite.setLotag((short) nObject);
                pSprite.setPicnum((short) gItemInfo[nItemIndex].picnum);
                pSprite.setShade((byte) gItemInfo[nItemIndex].shade);
                pSprite.setXrepeat((short) gItemInfo[nItemIndex].xrepeat);
                pSprite.setYrepeat((short) gItemInfo[nItemIndex].yrepeat);
                if (nObject <= kItemKey7) {
                    // PF/NN: should this be in bloodbath too?
                    if (pGameInfo.nGameType == kNetModeCoop) // force permanent keys in Coop mode
                    {
                        int nXSprite = pSprite.getExtra();
                        if (nXSprite == -1) {
                            nXSprite = boardService.insertXSprite(pSprite);
                        }

                        XSPRITE pXSprite = boardService.getXSprite(nXSprite);
                        if (pXSprite != null) {
                            pXSprite.setRespawn(kRespawnPermanent);
                            SPRITEHIT pSpriteHit = pXSprite.getSpriteHit();
                            pSpriteHit.floorHit = 0;
                            pSpriteHit.ceilHit = 0;
                        }
                    }
                }
                if (nObject == kItemBlueFlag || nObject == kItemRedFlag) {
                    if (pGameInfo.nGameType == kNetModeTeams) {
                        evPostCallback(pSprite.getXvel(), SS_SPRITE, 1800, 17);
                    }
                }
            } else if (nObject >= kAmmoItemBase) {
                int nAmmoIndex = nObject - kAmmoItemBase;

                pSprite.setLotag((short) nObject);
                pSprite.setPicnum((short) gAmmoItemData[nAmmoIndex].picnum);
                pSprite.setShade((byte) gAmmoItemData[nAmmoIndex].shade);
                pSprite.setXrepeat((short) gAmmoItemData[nAmmoIndex].xrepeat);
                pSprite.setYrepeat((short) gAmmoItemData[nAmmoIndex].yrepeat);
            } else {
                int nWeaponIndex = nObject - kWeaponItemBase;

                pSprite.setLotag((short) nObject);
                pSprite.setPicnum((short) gWeaponItemData[nWeaponIndex].picnum);
                pSprite.setShade((byte) gWeaponItemData[nWeaponIndex].shade);
                pSprite.setXrepeat((short) gWeaponItemData[nWeaponIndex].xrepeat);
                pSprite.setYrepeat((short) gWeaponItemData[nWeaponIndex].yrepeat);
            }

            GetSpriteExtents(pSprite);
            if (extents_zBot >= floorz) {
                pSprite.setZ(pSprite.getZ() - (extents_zBot - floorz));
            }

            return pSprite;
        }
        return null;
    }

    public static boolean actHealDude(XSPRITE pXDude, int healValue, int maxHealthClip) {
        if (pXDude == null) {
            throw new AssertException("pXDude != null");
        }

        healValue <<= 4; // fix this later in the calling code
        maxHealthClip <<= 4;
        if (pXDude.getHealth() < maxHealthClip) {
            Sprite pSprite = boardService.getSprite(pXDude.getReference());
            if (IsPlayerSprite(pSprite)) {
                sfxCreate3DSound(pSprite.getX(), pSprite.getY(), pSprite.getZ(), 780, pSprite.getSectnum());
            }
            pXDude.setHealth(ClipHigh(pXDude.getHealth() + healValue, maxHealthClip));
            return true;
        }
        return false;
    }

    public static void actExplodeSprite(int nSprite) {
        // be carefull, nSprite, pSprite overwriting!
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("pSprite != null");
        }

        XSPRITE pXSprite = pSprite.getXSprite();
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        // already exploding?
        if (pSprite.getStatnum() == kStatExplosion) {
            return;
        }

        sfxKill3DSound(pSprite, -1, -1);
        checkEventList(nSprite, SS_SPRITE);

        int nType;
        final SPRITEHIT pSpriteHit = pXSprite.getSpriteHit();
        switch (pSprite.getLotag()) {
            case kMissileNapalm:
                nType = 7;
                seqSpawn(4, SS_SPRITE, pSprite.getExtra(), null);
                if (Chance(0x4000)) {
                    pSprite.setCstat(pSprite.getCstat() | kSpriteFlipX);
                }
                sfxStart3DSound(pSprite, 303, -1, 0);
                actGenerateGibs(pSprite, 5, null, null);

                break;
            case kMissileStarburstFlare:
                nType = 3;
                seqSpawn(9, SS_SPRITE, pSprite.getExtra(), null);
                if (Chance(0x4000)) {
                    pSprite.setCstat(pSprite.getCstat() | kSpriteFlipX);
                }
                sfxStart3DSound(pSprite, 306, (pSprite.getXvel() & 3) + 24, 0);
                actGenerateGibs(pSprite, 5, null, null);
                break;
            case kMissileTchernobog:
            case kMissileTchernobog2:
                seqSpawn(5, SS_SPRITE, pSprite.getExtra(), null);
                sfxStart3DSound(pSprite, 304, -1, 0);
                nType = 3;
                actGenerateGibs(pSprite, 5, null, null);
                break;
            case kThingTNTBarrel:
                // spawn an explosion effect
                int nEffect = actSpawnSprite(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), kStatDefault, true);
                BloodSprite pEffect = boardService.getSprite(nEffect);
                if (pEffect == null) {
                    return;
                }

                pEffect.setOwner(pSprite.getOwner()); // set owner for frag/targeting
                // place barrel on the respawn list or just delete it
                if (actCheckRespawn(pSprite)) {
                    pXSprite.setState(0);
                    pXSprite.setHealth(thingInfo[kThingTNTBarrel - kThingBase].startHealth << 4);
                } else {
                    actPostSprite(nSprite, kStatFree);
                }

                // reset locals to point at the effect, not the barrel
                nType = 2;
                nSprite = nEffect;
                pSprite = pEffect;
                pXSprite = pEffect.getXSprite();
                if (pXSprite == null) {
                    throw new AssertException("pXSprite != null");
                }

                seqSpawn(4, SS_SPRITE, pSprite.getExtra(), null); // kSeqExplodeC2L
                sfxStart3DSound(pSprite, 305, -1, 0);
                actGenerateGibs(pSprite, 14, null, null);
                break;

            case kThingPodFire:
                nType = 3;
                seqSpawn(9, SS_SPRITE, pSprite.getExtra(), null);
                sfxStart3DSound(pSprite, 307, -1, 0);
                actGenerateGibs(pSprite, 5, null, null);
                actSpawnTentacleBlood(pSprite);
                break;

            case kThingTNTStick:
                nType = 0;
                if (pSpriteHit.floorHit != 0) {
                    seqSpawn(3, SS_SPRITE, pSprite.getExtra(), null);
                } else {
                    seqSpawn(4, SS_SPRITE, pSprite.getExtra(), null);
                }

                sfxStart3DSound(pSprite, 303, -1, 0);
                actGenerateGibs(pSprite, 5, null, null);
                break;
            case kThingTNTBundle:
            case kThingTNTProx:
            case kGDXThingTNTProx:
            case kThingTNTRem:
                nType = 1;
                if (pSpriteHit.floorHit != 0) {
                    seqSpawn(3, SS_SPRITE, pSprite.getExtra(), null);
                } else {
                    seqSpawn(4, SS_SPRITE, pSprite.getExtra(), null);
                }
                sfxStart3DSound(pSprite, 304, -1, 0);
                actGenerateGibs(pSprite, 5, null, null);
                break;
            case kThingSprayBundle:
                nType = 4;
                seqSpawn(5, SS_SPRITE, pSprite.getExtra(), null);
                sfxStart3DSound(pSprite, 307, -1, 0);
                actGenerateGibs(pSprite, 5, null, null);
                break;
            case kThingHiddenExploder:

                // Defaults for exploder
                int nSnd = 304;
                int nSeq = 4;

                // Temp variables for override via data fields
                nType = pXSprite.getData1();  // Explosion type
                int tSeq = pXSprite.getData2(); // SEQ id
                int tSnd = pXSprite.getData3(); // Sound Id

                if (nType <= 1 || nType > kExplodeMax) {
                    nType = 1;
                } else if (nType == 2) {
                    nSnd = 305;
                } else if (nType == 3) {
                    nSeq = 9;
                    nSnd = 307;
                } else if (nType == 4) {
                    nSeq = 5;
                    nSnd = 307;
                } else if (nType <= 6) {
                    nSnd = 303;
                } else if (nType == 7) {
                    nSnd = 303;
                } else if (nType == 8) {
                    nType = 0;
                    nSeq = 3;
                    nSnd = 303;
                }

                // Override previous sound and seq assigns
                if (tSeq > 0) {
                    nSeq = tSeq;
                }
                if (tSnd > 0) {
                    nSnd = tSnd;
                }

                if (game.getCache().contains(pXSprite.getData2(), seq)) {
                    seqSpawn(nSeq, SS_SPRITE, pSprite.getExtra(), null);
                }

                sfxStart3DSound(pSprite, nSnd, -1, 0);
                break;

            default:
                nType = 1;
                seqSpawn(4 /* kSeqExplodeC2M */, SS_SPRITE, pSprite.getExtra(), null);
                if (Chance(0x4000)) {
                    pSprite.setCstat(pSprite.getCstat() | kSpriteFlipX);
                }
                sfxStart3DSound(pSprite, 303, -1, 0);
                actGenerateGibs(pSprite, 5, null, null);
                break;
        }

        pSprite.setVelocity(0, 0, 0);
        actPostSprite(nSprite, kStatExplosion);
        pSprite.setHitag(pSprite.getHitag() & ~(kAttrMove | kAttrGravity));

        EXPLODE pExpl = gExplodeData[nType];

        pSprite.setYrepeat(pExpl.size);
        pSprite.setXrepeat(pExpl.size);
        pSprite.setLotag(nType);

        pXSprite.setTarget(0);
        pXSprite.setData1(pExpl.liveCount);
        pXSprite.setData2(pExpl.quake);
        pXSprite.setData3(pExpl.used3);
    }

    public static boolean IsItemSprite(Sprite pSprite) {
        return pSprite.getLotag() >= kItemBase && pSprite.getLotag() < kItemMax;
    }

    public static boolean IsDudeSprite(Sprite pSprite) {
        return pSprite != null && pSprite.getLotag() >= kDudeBase && pSprite.getLotag() < kDudeMax;
    }

    public static boolean IsBurningDude(Sprite pSprite) {
        if (pSprite == null) {
            return false;
        }
        switch (pSprite.getLotag()) {
            case kDudeBurning:
            case kDudeCultistBurning:
            case kDudeAxeZombieBurning:
            case kDudeBloatedButcherBurning:
            case kDudeTinyCalebburning:
            case kDudeTheBeastburning:
            case kGDXGenDudeBurning:
                return true;
        }

        return false;
    }

    public static boolean isImmortalDude(Sprite pSprite) {
        switch (pSprite.getLotag()) {
            case kDudeFleshStatue:
            case kDudeStoneStatue:
                return true;
            default:
                return !IsDudeSprite(pSprite) || boardService.getXSprite(pSprite.getExtra()).getLocked() == 1;
        }
    }

    public static boolean IsAmmoSprite(Sprite pSprite) {
        return pSprite.getLotag() >= kAmmoItemBase && pSprite.getLotag() < kAmmoItemMax;
    }

    public static boolean IsWeaponSprite(Sprite pSprite) {
        return pSprite.getLotag() >= kWeaponItemBase && pSprite.getLotag() < kWeaponItemMax;
    }

    public static boolean IsUnderwaterSector(int sectnum) {
        int nXSector = boardService.getSector(sectnum).getExtra();
        return nXSector > 0 && xsector[nXSector].Underwater;
    }

    public static int actGetBurnTime(XSPRITE pXActor) {
        return pXActor.getBurnTime();
    }

    public static int actGetBurnSource(int nSource) {
        if (nSource != -1) {
            if ((nSource & 0x1000) != 0) {
                nSource = gPlayer[nSource & 0xFFF].pSprite.getXvel();
            } else {
                nSource &= 0xFFF;
            }
        }
        return nSource;
    }

    public static int actSetBurnSource(int nSource) {
        if (nSource != -1) {
            if (!boardService.isValidSprite(nSource)) {
                throw new AssertException("isValidSprite(nSource)");
            }
            Sprite pSprite = boardService.getSprite(nSource);
            if (IsPlayerSprite(pSprite)) {
                nSource = pSprite.getLotag() - kDudePlayer1;
                nSource |= 0x1000;
            }
        }
        return nSource;
    }

    public static void TchernobogCallback1(int nXIndex) {
        int nSprite = boardService.getXSprite(nXIndex).getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            BloodSprite pSpawn = actSpawnEffect(32, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
            if (pSpawn != null) {
                pSpawn.setVelocity(pSprite.getVelocityX(), pSprite.getVelocityY(), pSprite.getVelocityZ());
            }
        }
    }

    public static void TchernobogCallback2(int nXIndex) {
        int nSprite = boardService.getXSprite(nXIndex).getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            BloodSprite pSpawn = actSpawnEffect(33, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
            if (pSpawn != null) {
                pSpawn.setVelocity(pSprite.getVelocityX(), pSprite.getVelocityY(), pSprite.getVelocityZ());
            }
        }
    }

    public static void FireballCallback(int nXIndex) {
        int nSprite = boardService.getXSprite(nXIndex).getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            BloodSprite pSpawn = actSpawnEffect(11, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
            if (pSpawn != null) {
                pSpawn.setVelocity(pSprite.getVelocityX(), pSprite.getVelocityY(), pSprite.getVelocityZ());
            }
        }
    }

    public static void SmokeCallback(int nXIndex) {
        int nSprite = boardService.getXSprite(nXIndex).getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite != null) {
            BloodSprite pSpawn = actSpawnEffect(12, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
            if (pSpawn != null) {
                pSpawn.setVelocity(pSprite.getVelocityX(), pSprite.getVelocityY(), pSprite.getVelocityZ());
            }
        }
    }

    public static void DamageTreeCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        Sprite pSprite = boardService.getSprite(pXSprite.getReference());
        if (pSprite != null) {
            pSprite.setLotag(kThingExplodeObject);
            pXSprite.setState(1);
            pXSprite.setData1(15);
            pXSprite.setData3(0);
            pXSprite.setData4(312);

            pXSprite.setHealth(thingInfo[17].startHealth);

            pSprite.setCstat(pSprite.getCstat() | 0x101);
        }
    }

    public static void actNapalm2Explode(BloodSprite pSprite, XSPRITE pXSprite) {
        actPostSprite(pSprite.getXvel(), kStatDefault);
        seqSpawn(9, SS_SPRITE, pSprite.getExtra(), null);

        if (Chance(0x4000)) {
            pSprite.setCstat(pSprite.getCstat() | kSpriteFlipX);
        }

        sfxStart3DSound(pSprite, 303, (pSprite.getXvel() & 3) + 24, 0);
        actDistanceDamage(actGetBurnSource(pSprite.getOwner()), pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), 128, 0, 3, 60, 15, 120);

        if (pXSprite.getData4() > 4) {
            actGenerateGibs(pSprite, 5, null, null);
            int nAngle = pSprite.getAng();
            pSprite.setVelocity(0, 0, 0);
           
            NapalmAmmo[0] = pXSprite.getData4() >> 3;
            NapalmAmmo[1] = (pXSprite.getData4() >> 2) - NapalmAmmo[0];
            for (int i = 0; i < 2; i++) {
                int velocity = 0x33333 + Random(0x33333);
                pSprite.setAng((short) ((nAngle + BiRandom(113)) & kAngleMask));
                Sprite pThing = actFireThing(pSprite.getXvel(), 0, 0, -37840, kThingAltNapalm, velocity);
                if (pThing != null) {
                    XSPRITE pXThing = boardService.getXSprite(pThing.getExtra());
                    pThing.setOwner(pSprite.getOwner());
                    seqSpawn(getSeq(kNapalm), SS_SPRITE, pThing.getExtra(), callbacks[SmokeCallback]);
                    pXThing.setData4(4 * (NapalmAmmo[i] & 0xFFFF));
                }
            }
        }
    }

    public static void DamageDude(int nXIndex, int count) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        Sprite pSprite = boardService.getSprite(pXSprite.getReference());
        if (pSprite != null) {
            pSprite.setLotag(kThingGib);
            pXSprite.setState(1);
            pXSprite.setData1((short) count);
            pXSprite.setData2(0);
            pXSprite.setData3(0);
            pXSprite.setData4(319);
            pXSprite.setTargetX(gFrameClock);
            pXSprite.setTriggered(false);
            pXSprite.setTriggerOnce(false);
            pXSprite.setLocked(0);

            pXSprite.setHealth(thingInfo[kThingGib - kThingBase].startHealth);
        }
    }

    public static int getDudeMassBySpriteSize(Sprite pSprite) {
        int mass;
        int minMass = 5;
        if (!IsDudeSprite(pSprite)) {
            return minMass;
        }

        int seqStartId = dudeInfo[pSprite.getLotag() - kDudeBase].seqStartID;
        switch (pSprite.getLotag()) {
            case kGDXDudeUniversalCultist:
            case kGDXGenDudeBurning:
                seqStartId = boardService.getXSprite(pSprite.getExtra()).getData2();
                break;
        }

//        if (gSpriteMass[pSprite.getXvel()] == null) {
//            gSpriteMass[pSprite.getXvel()] = new SPRITEMASS();
//        }

        SPRITEMASS cachedMass = new SPRITEMASS(); //gSpriteMass[pSprite.getXvel()];
        if (seqStartId == cachedMass.seqId && pSprite.getXrepeat() == cachedMass.xrepeat && pSprite.getYrepeat() == cachedMass.yrepeat && pSprite.getClipdist() == cachedMass.clipdist) {
            return cachedMass.mass;
        }

        SeqType pSeq = SeqType.getInstance(seqStartId);
        int picnum = (pSeq != null) ? pSeq.getFrame(0).nTile : pSprite.getPicnum();
        int clipDist = (pSprite.getClipdist() <= 0) ? dudeInfo[pSprite.getLotag() - kDudeBase].clipdist : pSprite.getClipdist();

        ArtEntry pic = engine.getTile(picnum);

        short xrepeat = pSprite.getXrepeat();
        int x = pic.getWidth();
        if (xrepeat > 64) {
            x += ((xrepeat - 64) * 2);
        } else if (xrepeat < 64) {
            x -= ((64 - xrepeat) * 2);
        }

        short yrepeat = pSprite.getYrepeat();
        int y = pic.getHeight();
        if (yrepeat > 64) {
            y += ((yrepeat - 64) * 2);
        } else if (yrepeat < 64) {
            y -= ((64 - yrepeat) * 2);
        }

        mass = ((x + y) * clipDist) / 25;
        //if ((mass+=(x+y)) > 200) mass+=((mass - 200)*16);

        cachedMass.seqId = seqStartId;
        cachedMass.xrepeat = xrepeat;
        cachedMass.yrepeat = yrepeat;
        cachedMass.mass = ClipRange(mass, minMass, 65535);
        cachedMass.clipdist = clipDist;

        return cachedMass.mass;
    }

    public static void setgotpic(int tilenume) {
        Renderer renderer = game.getRenderer();
        renderer.getRenderedPics()[tilenume >> 3] |= (byte) pow2char[tilenume & 7];
    }
}

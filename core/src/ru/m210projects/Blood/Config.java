// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import com.badlogic.gdx.Input.Keys;
import ru.m210projects.Build.Gameutils;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.input.keymap.ControllerButton;
import ru.m210projects.Build.settings.*;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.stream.IntStream;

import static ru.m210projects.Build.input.keymap.Keymap.*;

public class Config extends GameConfig {

    public static int[] defclassickeys = {
            Keys.UP,    //Move_Forward
            Keys.DOWN,    //Move_Backward
            Keys.LEFT,    //Turn_Left
            Keys.RIGHT,    //Turn_Right
            Keys.BACKSPACE, //Turn_Around
            Keys.ALT_LEFT,    //Strafe
            Keys.COMMA,        //Strafe_Left
            Keys.PERIOD, //Strafe_Right
            Keys.A,        //Jump
            Keys.Z,            //Crouch
            Keys.SHIFT_LEFT,    //Run
            Keys.SPACE,            //Open
            Keys.CONTROL_LEFT, //Weapon_Fire
            Keys.APOSTROPHE,//Next_Weapon
            Keys.SEMICOLON,    //Previous_Weapon
            Keys.PAGE_UP,        //Look_Up
            Keys.PAGE_DOWN,    //Look_Down
            Keys.TAB,            //Map_Toggle
            Keys.EQUALS,    //Enlarge_Screen
            Keys.MINUS,    //Shrink_Screen
            Keys.T,        //Send_Message
            Keys.U,            //Mouse_Aiming
            Keys.ESCAPE,    //Menu_Toggle
            Keys.GRAVE,        //Show_Console

            0,                //Crouch_toggle
            Keys.CAPS_LOCK,        //AutoRun
            Keys.X,            //Weapon_Special_Fire
            Keys.HOME,            //Aim_Up
            Keys.END,            //Aim_Down
            Keys.NUMPAD_5,        //Aim_Center
            Keys.INSERT,        //Tilt_Left
            Keys.FORWARD_DEL, //Tilt_Right
            Keys.NUM_1,        //Weapon_1
            Keys.NUM_2,        //Weapon_2
            Keys.NUM_3,        //Weapon_3
            Keys.NUM_4,        //Weapon_4
            Keys.NUM_5,        //Weapon_5
            Keys.NUM_6,        //Weapon_6
            Keys.NUM_7,        //Weapon_7
            Keys.NUM_8,        //Weapon_8
            Keys.NUM_9,        //Weapon_9
            Keys.NUM_0,        //Weapon_10
            Keys.ENTER,    //Inventory_Use
            Keys.LEFT_BRACKET, //Inventory_Left
            Keys.RIGHT_BRACKET, //Inventory_Right
            Keys.F,            //Map_Follow_Mode
            Keys.K,            //See_Coop_View
            Keys.F7,        //See_Chase_View
            Keys.I,            //Toggle_Crosshair
            0,                //Last_Used_Weapon
            Keys.BACKSLASH, //Holster_Weapon
            Keys.W,        //Show_Opponents_Weapon
            Keys.B,            //BeastVision
            Keys.C,            //CrystalBall
            Keys.J,            //JumpBoots
            Keys.M,            //MedKit
            Keys.P,            //ProximityBombs
            Keys.R,            //RemoteBombs
            Keys.F1,        //Show_HelpScreen
            Keys.F2,        //Show_Save
            Keys.F3,        //Show_Load
            Keys.F4,        //Show_Sounds
            Keys.F5,        //Show_Options
            Keys.F6,        //QuickSave
            Keys.F8,        //ToggleMessages
            Keys.F9,        //QuickLoad
            Keys.F10,        //Quit
            Keys.F11,        //Gamma
            Keys.F12,        //MakeScreenshot
    };

    public static int[] defkeys = {
            Keys.W,    //Move_Forward 0
            Keys.S,    //Move_Backward 1
            Keys.LEFT,    //Turn_Left 2
            Keys.RIGHT,    //Turn_Right 3
            Keys.BACKSPACE, //Turn_Around 4
            Keys.ALT_LEFT,    //Strafe 5
            Keys.A,        //Strafe_Left 6
            Keys.D, //Strafe_Right 7
            Keys.SPACE,        //Jump 8
            Keys.CONTROL_LEFT,    //Crouch 9
            Keys.SHIFT_LEFT,    //Run 11
            Keys.E,            //Open 13
            0,                //Weapon_Fire 14
            Keys.APOSTROPHE,//Next_Weapon 46
            Keys.SEMICOLON,    //Previous_Weapon 47
            Keys.PAGE_UP,        //Look_Up 19
            Keys.PAGE_DOWN,    //Look_Down 20
            Keys.TAB,            //Map_Toggle 36
            Keys.EQUALS,    //Enlarge_Screen 39
            Keys.MINUS,    //Shrink_Screen 38
            Keys.T,        //Send_Message 40
            Keys.U,            //Mouse_Aiming 43
            Keys.ESCAPE,    //Open_menu 56
            Keys.GRAVE,        //Show_Console 57

            0,                    //Crouch_toggle 10
            Keys.CAPS_LOCK,        //AutoRun 12
            Keys.X,            //Weapon_Special_Fire 15
            Keys.HOME,            //Aim_Up 16
            Keys.END,            //Aim_Down 17
            Keys.NUMPAD_5,        //Aim_Center 18
            Keys.INSERT,        //Tilt_Left 21
            Keys.FORWARD_DEL, //Tilt_Right 22
            Keys.NUM_1,    //Weapon_1 23
            Keys.NUM_2,        //Weapon_2 24
            Keys.NUM_3,        //Weapon_3 25
            Keys.NUM_4,        //Weapon_4 26
            Keys.NUM_5,        //Weapon_5 27
            Keys.NUM_6,        //Weapon_6 28
            Keys.NUM_7,        //Weapon_7 29
            Keys.NUM_8,        //Weapon_8 30
            Keys.NUM_9,        //Weapon_9 31
            Keys.NUM_0,        //Weapon_10 32
            Keys.ENTER,    //Inventory_Use 33
            Keys.LEFT_BRACKET, //Inventory_Left 34
            Keys.RIGHT_BRACKET, //Inventory_Right 35
            Keys.F,            //Map_Follow_Mode 37
            Keys.K,            //See_Coop_View 41
            Keys.F7,        //See_Chase_View 42
            Keys.I,            //Toggle_Crosshair 44
            Keys.Q,                //Last_Weapon_Switch 45
            Keys.BACKSLASH, //Holster_Weapon 48
            0,                //Show_Opponents_Weapon 49
            Keys.B,            //BeastVision 50
            Keys.C,            //CrystalBall 51
            Keys.J,            //JumpBoots 52
            Keys.M,            //MedKit 53
            Keys.P,            //ProximityBombs 54
            Keys.R,            //RemoteBombs 55
            Keys.F1,        //Show_HelpScreen 58
            Keys.F2,        //Show_Save	59
            Keys.F3,        //Show_Load 60
            Keys.F4,        //Show_Sounds 61
            Keys.F5,        //Show_Options 62
            Keys.F6,        //QuickSave	63
            0,        //ToggleMessages 64
            Keys.F9,        //QuickLoad 65
            Keys.F10,        //Quit 66
            Keys.F11,        //Gamma 67
            Keys.F12,        //MakeScreenshot 68
    };

    public String[] macros = {
            "I love the smell of napalm",
            "Is that gasoline I smell?",
            "Ta da!",
            "Who wants some, huh? Who's next?",
            "I have something for you.",
            "You just gonna stand there...",
            "That'll teach ya!",
            "Ohh, that wasn't a bit nice.",
            "Amateurs!",
            "Fool! You are already dead."
    };

    public boolean gAutoAim = true;
    public boolean gCrosshair = true;
    public boolean gBobWidth = true;
    public boolean gBobHeight = true;
    public boolean gSlopeTilt = true;
    public boolean gAutoRun = true;
    public boolean gShowWeapon = true;
    public boolean gParentalLock = false;
    public boolean MessageState = true;
    public boolean useautosecretcount = false;
    public boolean showCutscenes = true;
    public boolean gVanilla = true;
    public int gViewSize = 3;
    public int gTurnSpeed = 92;
    public int gDetail = 4;
    public int gInterpolation = 1;
    public String AdultPassword = "";
    public int MessageFont = 3;
    public int showQuotes = 4;
    public int quoteTime = 5; //sec
    public int gOverlayMap = 0;
    public int gDemoSeq = 1;
    public int gStatSize = 65536;
    public int gCrossSize = 65536;
    public int gHudSize = 65536;
    public int gShowStat = 1;
    public int showMapInfo = 1;

    public int weaponIndex = -1;

    public Config(Path path) {
        super(path);
        setpName("Caleb");
    }

    public GameKey[] getKeyMap() {
        return new GameKey[]{
                GameKeys.Move_Forward,
                GameKeys.Move_Backward,
                GameKeys.Turn_Left,
                GameKeys.Turn_Right,
                GameKeys.Turn_Around,
                GameKeys.Strafe,
                GameKeys.Strafe_Left,
                GameKeys.Strafe_Right,
                GameKeys.Jump,
                GameKeys.Crouch,
                BloodKeys.Crouch_toggle,
                GameKeys.Run,
                BloodKeys.AutoRun,
                GameKeys.Open,
                GameKeys.Weapon_Fire,
                BloodKeys.Weapon_Special_Fire,
                BloodKeys.Aim_Up,
                BloodKeys.Aim_Down,
                BloodKeys.Aim_Center,
                GameKeys.Look_Up,
                GameKeys.Look_Down,
                BloodKeys.Tilt_Left,
                BloodKeys.Tilt_Right,
                BloodKeys.Weapon_1,
                BloodKeys.Weapon_2,
                BloodKeys.Weapon_3,
                BloodKeys.Weapon_4,
                BloodKeys.Weapon_5,
                BloodKeys.Weapon_6,
                BloodKeys.Weapon_7,
                BloodKeys.Weapon_8,
                BloodKeys.Weapon_9,
                BloodKeys.Weapon_10,
                BloodKeys.Inventory_Use,
                BloodKeys.Inventory_Left,
                BloodKeys.Inventory_Right,
                GameKeys.Map_Toggle,
                BloodKeys.Map_Follow_Mode,
                GameKeys.Shrink_Screen,
                GameKeys.Enlarge_Screen,
                GameKeys.Send_Message,
                BloodKeys.See_Coop_View,
                BloodKeys.See_Chase_View,
                GameKeys.Mouse_Aiming,
                BloodKeys.Toggle_Crosshair,
                BloodKeys.Last_Used_Weapon,
                GameKeys.Next_Weapon,
                GameKeys.Previous_Weapon,
                BloodKeys.Holster_Weapon,
                BloodKeys.Show_Opponents_Weapon,
                BloodKeys.BeastVision,
                BloodKeys.CrystalBall,
                BloodKeys.JumpBoots,
                BloodKeys.MedKit,
                BloodKeys.ProximityBombs,
                BloodKeys.RemoteBombs,
                GameKeys.Menu_Toggle,
                GameKeys.Show_Console,
                BloodKeys.Show_HelpScreen,
                BloodKeys.Show_SaveMenu,
                BloodKeys.Show_LoadMenu,
                BloodKeys.Show_SoundSetup,
                BloodKeys.Show_Options,
                BloodKeys.Quicksave,
                BloodKeys.Toggle_messages,
                BloodKeys.Quickload,
                BloodKeys.Quit,
                BloodKeys.Gamma,
                BloodKeys.Make_Screenshot,
        };
    }

    @Override
    protected InputContext createDefaultInputContext() {
        return new InputContext(getKeyMap(), defkeys, defclassickeys) {

            @Override
            protected void clearInput() {
                super.clearInput();
                weaponIndex = IntStream.range(0, keymap.length).filter(i -> keymap[i].equals(BloodKeys.Weapon_1)).findFirst().orElse(-1);
            }

            @Override
            public void resetInput(boolean classicKeys) {
                super.resetInput(classicKeys);

                primarykeys[MOUSE_KEYS_INDEX][BloodKeys.Weapon_Special_Fire.getNum()] = MOUSE_RBUTTON;

                bindKey(BloodKeys.Crouch_toggle, ControllerButton.BUTTON_B);
                bindKey(BloodKeys.Inventory_Use, ControllerButton.BUTTON_X);
                bindKey(BloodKeys.Inventory_Left, ControllerButton.DPAD_LEFT);
                bindKey(BloodKeys.Inventory_Right, ControllerButton.DPAD_RIGHT);
            }
        };
    }

    @Override
    protected ConfigContext createDefaultGameContext() {
        return new ConfigContext() {
            @Override
            public void load(Properties prop) {
                if (prop.setContext("Options")) {
                    gTurnSpeed = prop.getIntValue("KeyboardTurnSpeed", gTurnSpeed);
                    gViewSize = prop.getIntValue("ViewSize", gViewSize);
                    gCrosshair = prop.getBooleanValue("Crosshair", gCrosshair);
                    MessageState = prop.getBooleanValue("MessageState", MessageState);
                    gDetail = prop.getIntValue("Detail", gDetail);
                    gAutoRun = prop.getBooleanValue("AutoRun", gAutoRun);
                    gInterpolation = prop.getIntValue("Interpolation", gInterpolation);
                    gBobWidth = prop.getBooleanValue("ViewHBobbing", gBobWidth);
                    gBobHeight = prop.getBooleanValue("ViewVBobbing", gBobHeight);
                    gOverlayMap = prop.getIntValue("OverlayMap", gOverlayMap);
                    gVanilla = prop.getBooleanValue("VanillaMode", gVanilla);
                    gShowWeapon = prop.getBooleanValue("ShowOpponentsWeapon", gShowWeapon);
                    gAutoAim = prop.getBooleanValue("AutoAim", gAutoAim);
                    gSlopeTilt = prop.getBooleanValue("SlopeTilting", gSlopeTilt);
                    MessageState = prop.getBooleanValue("MessageState", MessageState);
                    showQuotes = prop.getIntValue("MessageCount", showQuotes);
                    quoteTime = prop.getIntValue("MessageTime", quoteTime);
                    MessageFont = Gameutils.BClipRange(prop.getIntValue("MessageFont", MessageFont), 0, 4);
                    gParentalLock = prop.getBooleanValue("AdultContent", gParentalLock);
                    AdultPassword = prop.getStringValue("AdultPassword", "").replaceAll("\"", "");
                    gStatSize = Math.max(prop.getIntValue("StatSize", gStatSize), 16384);
                    gCrossSize = Math.max(prop.getIntValue("CrossSize", gCrossSize), 16384);
                    gHudSize = Math.max(prop.getIntValue("HudSize", gHudSize), 16384);
                    gShowStat = prop.getIntValue("ShowStat", gShowStat);
                    showCutscenes = prop.getBooleanValue("showCutscenes", showCutscenes);
                    showMapInfo = prop.getIntValue("showMapInfo", showMapInfo);
                    useautosecretcount = prop.getBooleanValue("UseAutoSecretCount", useautosecretcount);
                    gDemoSeq = prop.getIntValue("DemoSequence", gDemoSeq);
                }
            }

            @Override
            public void save(OutputStream os) throws IOException {
                putString(os, "[Options]\r\n");
                //Options
                putInteger(os, "KeyboardTurnSpeed", gTurnSpeed);
                putInteger(os, "ViewSize", gViewSize);
                putBoolean(os, "Crosshair", gCrosshair);
                putBoolean(os, "MessageState", MessageState);
                putInteger(os, "Detail", gDetail);
                putBoolean(os, "ShowOpponentsWeapon", gShowWeapon);
                putBoolean(os, "AutoRun", gAutoRun);
                putInteger(os, "Interpolation", gInterpolation);
                putBoolean(os, "ViewHBobbing", gBobWidth);
                putBoolean(os, "ViewVBobbing", gBobHeight);
                putInteger(os, "OverlayMap", gOverlayMap);
                putBoolean(os, "VanillaMode", gVanilla);
                putBoolean(os, "SlopeTilting", gSlopeTilt);
                putBoolean(os, "AutoAim", gAutoAim);
                putBoolean(os, "MessageState", MessageState);
                putInteger(os, "MessageCount", showQuotes);
                putInteger(os, "MessageTime", quoteTime);
                putInteger(os, "MessageFont", MessageFont);
                putBoolean(os, "AdultContent", gParentalLock);
                putString(os, "AdultPassword = " + "\"\"" + "\r\n");
                putInteger(os, "StatSize", gStatSize);
                putInteger(os, "CrossSize", gCrossSize);
                putInteger(os, "HudSize", gHudSize);
                putInteger(os, "ShowStat", gShowStat);
                putBoolean(os, "showCutscenes", showCutscenes);
                putInteger(os, "showMapInfo", showMapInfo);
                putBoolean(os, "UseAutoSecretCount", useautosecretcount);
                putInteger(os, "DemoSequence", gDemoSeq);
            }
        };
    }

    public enum BloodKeys implements GameKey {
        Crouch_toggle,
        AutoRun,
        Weapon_Special_Fire,
        Aim_Up,
        Aim_Down,
        Aim_Center,
        Tilt_Left,
        Tilt_Right,
        Weapon_1,
        Weapon_2,
        Weapon_3,
        Weapon_4,
        Weapon_5,
        Weapon_6,
        Weapon_7,
        Weapon_8,
        Weapon_9,
        Weapon_10,
        Inventory_Use,
        Inventory_Left,
        Inventory_Right,
        Map_Follow_Mode,
        See_Coop_View,
        See_Chase_View,
        Toggle_Crosshair,
        Last_Used_Weapon,
        Holster_Weapon,
        Show_Opponents_Weapon,
        BeastVision,
        CrystalBall,
        JumpBoots,
        MedKit,
        ProximityBombs,
        RemoteBombs,
        Show_HelpScreen,
        Show_SaveMenu,
        Show_LoadMenu,
        Show_SoundSetup,
        Show_Options,
        Quicksave,
        Toggle_messages,
        Quickload,
        Quit,
        Gamma,
        Make_Screenshot;

        public int getNum() {
            return GameKeys.values().length + ordinal();
        }

        public String getName() {
            return name();
        }

    }

}

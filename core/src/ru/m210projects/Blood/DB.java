// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import com.badlogic.gdx.ScreenAdapter;
import ru.m210projects.Blood.Factory.BloodBoard;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.Types.XSECTOR;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Blood.Types.XWALL;
import ru.m210projects.Blood.Types.ZONE;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.io.EOFException;
import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.EVENT.evInit;
import static ru.m210projects.Blood.Gameutils.sRandom;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.levelResetKills;
import static ru.m210projects.Blood.LEVELS.levelResetSecrets;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.Mirror.InitMirrors;
import static ru.m210projects.Blood.PLAYER.*;
import static ru.m210projects.Blood.SECTORFX.InitSectorFX;
import static ru.m210projects.Blood.SOUND.*;
import static ru.m210projects.Blood.Screen.*;
import static ru.m210projects.Blood.Trigger.trInit;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.GAMEINFO.EndOfGame;
import static ru.m210projects.Blood.Types.GAMEINFO.EndOfLevel;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqKillAll;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Blood.Warp.*;
import static ru.m210projects.Blood.Weapon.defaultOrder;
import static ru.m210projects.Build.Engine.automapping;
import static ru.m210projects.Build.net.Mmulti.*;

public class DB {

    public static final int kMaxXWalls = 512;
    public static final int kMaxXSectors = 512;
    public static final int kMaxBusyValue = 0x10000;
    public static final int kFluxMask = 0x0FFFF;
    public static final int kPathMarker = 15;
    public static final int kDudeSpawn = 18;
    public static final int kEarthQuake = 19;
    public static final int kSwitchBase = 20;
    public static final int kSwitchToggle = kSwitchBase;
    public static final int kSwitchMomentary = 21;
    public static final int kSwitchCombination = 22;
    public static final int kSwitchPadlock = 23;
    public static final int kSwitchMax = 24;
    //////////////////////////////////New types are here
    public static final int kGDXTypeBase = 24;
    public static final int kGDXCustomDudeSpawn = kGDXTypeBase;
    public static final int kGDXRandomTX = 25;
    public static final int kGDXSequentialTX = 26;
    public static final int kGDXSeqSpawner = 27;
    public static final int kGDXObjPropertiesChanger = 28;
    public static final int kGDXObjPicnumChanger = 29;
    public static final int kGDXObjSizeChanger = 31;
    public static final int kGDXDudeTargetChanger = 33;
    public static final int kGDXSectorFXChanger = 34;
    public static final int kGDXObjDataChanger = 35;
    public static final int kGDXSpriteDamager = 36;
    public static final int kGDXObjDataAccumulator = 37;
    public static final int kGDXEffectSpawner = 38;
    public static final int kGDXWindGenerator = 39;
    public static final int kDudeBase = 200;
    public static final int kDudeTommyCultist = 201;
    public static final int kDudeShotgunCultist = 202;
    public static final int kDudeAxeZombie = 203;
    public static final int kDudeButcher = 204;
    public static final int kDudeEarthZombie = 205;
    public static final int kDudeFleshGargoyle = 206;
    ///////////////////////////////// End of new types
    public static final int kDudeStoneGargoyle = 207;
    public static final int kDudeFleshStatue = 208;
    public static final int kDudeStoneStatue = 209;
    public static final int kDudePhantasm = 210;
    public static final int kDudeHound = 211;
    public static final int kDudeHand = 212;
    public static final int kDudeBrownSpider = 213;
    public static final int kDudeRedSpider = 214;
    public static final int kDudeBlackSpider = 215;
    public static final int kDudeMotherSpider = 216;
    public static final int kDudeGillBeast = 217;
    public static final int kDudeEel = 218;
    public static final int kDudeBat = 219;
    public static final int kDudeRat = 220;
    public static final int kDudeGreenPod = 221;
    public static final int kDudeGreenTentacle = 222;
    public static final int kDudeFirePod = 223;
    public static final int kDudeFireTentacle = 224;
    public static final int kDudeMotherPod = 225;
    public static final int kDudeMotherTentacle = 226;
    public static final int kDudeCerberus = 227;
    public static final int kDudeCerberus2 = 228;
    public static final int kDudeTchernobog = 229;
    public static final int kDudeFanaticProne = 230;
    public static final int kDudePlayer1 = 231;
    public static final int kDudePlayer8 = 238;
    public static final int kDudeBurning = 239;
    public static final int kDudeCultistBurning = 240;
    public static final int kDudeAxeZombieBurning = 241;
    public static final int kDudeBloatedButcherBurning = 242;
    //<Reserved>			243
    public static final int kDudeSleepZombie = 244;
    public static final int kDudeInnocent = 245;
    public static final int kDudeCultistProne = 246;
    public static final int kDudeTeslaCultist = 247;
    public static final int kDudeDynamiteCultist = 248;
    public static final int kDudeBeastCultist = 249;
    public static final int kDudeTinyCaleb = 250;
    public static final int kDudeTheBeast = 251;
    public static final int kDudeTinyCalebburning = 252;
    public static final int kDudeTheBeastburning = 253;
    public static final int kGDXDudeUniversalCultist = 254;
    public static final int kGDXGenDudeBurning = 255;
    public static final int kDudeMax = 256;
    public static final String[] kDudeName = {"kDudeBase", "Tommy Cultist", "Shotgun Cultist", "Axe Zombie", "Butcher", "Earth Zombie", "Flesh Gargoyle", "Stone Gargoyle", "Flesh Statue", "Stone Statue", "Phantasm", "Hound", "Hand", "Brown Spider", "Red Spider", "Black Spider", "Mother Spider", "Gill Beast", "Eel", "Bat", "Rat", "Green Pod", "Green Tentacle", "Fire Pod", "Fire Tentacle", "Mother Pod", "Mother Tentacle", "Cerberus", "Cerberus", "Tchernobo", "Fanatic Prone", "Player1", "Player2", "Player3", "Player4", "Player5", "Player6", "Player7", "Player8", "Burning", "Cultist Burning", "Axe Zombie Burning", "Bloated Butcher Burning", "<Reserved>", "Sleep Zombie", "Innocent", "CultistProne", "Tesla Cultist", "Dynamite Cultist", "Beast Cultist", "Tiny Caleb", "The Beast", "Tiny Calebburning", "The Beastburning", "GDX Custom dude", "GDX Custom burning dude", "kDudeMax"};
    public static final int kMissileBase = 300;
    public static final int kMissileButcherKnife = kMissileBase;
    public static final int kMissileFlare = 301;
    public static final int kMissileAltTesla = 302;
    public static final int kMissileStarburstFlare = 303;
    public static final int kMissileSprayFlame = 304;
    public static final int kMissileFireball = 305;
    public static final int kMissileTesla = 306;
    public static final int kMissileEctoSkull = 307;
    public static final int kMissileHoundFire = 308;
    public static final int kMissileGreenPuke = 309;
    public static final int kMissileRedPuke = 310; //nTile = 0
    public static final int kMissileBone = 311;
    public static final int kMissileNapalm = 312;
    public static final int kMissileTchernobog = 313;
    public static final int kMissileTchernobog2 = 314;
    public static final int kMissileLifeLeech = 315;
    public static final int kMissileAltLeech1 = 316;
    public static final int kMissileAltLeech2 = 317;
    public static final int kMissileMax = 318;
    public static final int kThingBase = 400;
    public static final int kThingTNTBarrel = kThingBase;
    public static final int kThingTNTProx = 401;
    public static final int kThingTNTRem = 402;
    public static final int kThingBlueVase = 403;
    public static final int kThingBrownVase = 404;
    public static final int kThingCrateFace = 405;
    public static final int kThingClearGlass = 406;
    public static final int kThingFluorescent = 407;
    public static final int kThingWallCrack = 408;
    public static final int kThingWoodBeam = 409;
    public static final int kThingWeb = 410;
    public static final int kThingMetalGrate1 = 411;
    public static final int kThingFlammableTree = 412;
    public static final int kThingMachineGun = 413;
    public static final int kThingFallingRock = 414;
    public static final int kThingPail = 415;
    public static final int kThingGibObject = 416;
    public static final int kThingExplodeObject = 417;
    public static final int kThingTNTStick = 418;
    public static final int kThingTNTBundle = 419;
    public static final int kThingSprayBundle = 420;
    public static final int kThingBoneClub = 421;
    //422 kThingZombieBones
    public static final int kThingWaterDrip = 423;
    public static final int kThingBloodDrip = 424;
    public static final int kThingGibSmall = 425;
    public static final int kThingGib = 426;
    public static final int kThingZombieHead = 427;
    public static final int kThingAltNapalm = 428;
    public static final int kThingPodFire = 429;
    public static final int kThingPodGreen = 430;
    public static final int kThingLifeLeech = 431;
    //432
    public static final int kGDXThingTNTProx = 433; // detects only players
    public static final int kGDXThingThrowableRock = 434; // does small damage if hits target
    public static final int kGDXThingCustomDudeLifeLeech = 435; // the same as normal, except it aims in specified target
    public static final int kThingMax = 436;
    public static final int kThingFlameTrap = 452;
    public static final int kTrapSawBlade = 454;
    public static final int kTrapPoweredZap = 456;
    public static final int kThingHiddenExploder = 459;
    public static final int kWallGlass = 511;
    public static final int kGenTrigger = 700;
    public static final int kGenWaterDrip = 701;
    public static final int kGenBloodDrip = 702;
    public static final int kGenFireball = 703;
    public static final int kGenEctoSkull = 704;
    public static final int kGenDart = 705;
    public static final int kGenBubble = 706;
    public static final int kGenMultiBubble = 707;
    public static final int kGenSFX = 708;
    public static final int kGenSectorSFX = 709;
    public static final int kGenAmbientSFX = 710;
    public static final int kGenPlayerSFX = 711;
    public static final int kSectorBase = 600;
    public static final int kSectorZMotion = kSectorBase;
    public static final int kSectorZSprite = 602;
    public static final int kSectorWarp = 603;
    public static final int kSectorTeleport = 604;
    public static final int kSectorPath = 612;
    public static final int kSectorRotateStep = 613;
    public static final int kSectorSlideMarked = 614;
    public static final int kSectorRotateMarked = 615;
    public static final int kSectorSlide = 616;
    public static final int kSectorRotate = 617;
    public static final int kSectorDamage = 618;

    //605
    //606
    //607
    //608
    //609
    //610
    //611
    public static final int kSectorCounter = 619;
    public static final int kSectorMax = 620;
    public static final int kWeaponItemBase = 40; // also random weapon
    public static final int kWeaponLifeLeech = 50;
    public static final int kWeaponItemMax = 51;
    public static final int kAmmoItemBase = 60;
    public static final int kAmmoItemRandom = 80; // random ammo
    public static final int kAmmoItemMax = 81;
    public static final int kItemBase = 100;
    public static final int kItemKey1 = kItemBase;
    public static final int kItemKey2 = 101;
    public static final int kItemKey3 = 102;
    public static final int kItemKey4 = 103;
    public static final int kItemKey5 = 104;
    public static final int kItemKey6 = 105;
    public static final int kItemKey7 = 106;
    public static final int kItemDoctorBag = 107;
    public static final int kItemMedPouch = 108;
    public static final int kItemLifeEssence = 109;
    public static final int kItemLifeSeed = 110;
    public static final int kItemPotion1 = 111;
    public static final int kItemFeatherFall = 112;
    public static final int kItemLtdInvisibility = 113;
    public static final int kItemInvulnerability = 114;
    public static final int kItemJumpBoots = 115;
    public static final int kItemRavenFlight = 116;
    public static final int kItemGunsAkimbo = 117;
    public static final int kItemDivingSuit = 118;
    public static final int kItemGasMask = 119;
    public static final int kItemClone = 120;
    public static final int kItemCrystalBall = 121;
    public static final int kItemDecoy = 122;
    public static final int kItemDoppleganger = 123;
    public static final int kItemReflectiveShots = 124;
    public static final int kItemBeastVision = 125;
    public static final int kItemShadowCloak = 126;
    public static final int kItemShroomRage = 127;
    public static final int kItemShroomDelirium = 128;
    public static final int kItemShroomGrow = 129;
    public static final int kItemShroomShrink = 130;
    public static final int kItemDeathMask = 131;
    public static final int kItemWineGoblet = 132;
    public static final int kItemWineBottle = 133;
    public static final int kItemSkullGrail = 134;
    public static final int kItemSilverGrail = 135;
    public static final int kItemTome = 136;
    public static final int kItemBlackChest = 137;
    public static final int kItemWoodenChest = 138;
    public static final int kItemAsbestosArmor = 139;
    public static final int kArmorItemBase = 140;
    public static final int kItemBasicArmor = kArmorItemBase;
    public static final int kItemBodyArmor = 141;
    public static final int kItemFireArmor = 142;
    public static final int kItemSpiritArmor = 143;
    public static final int kItemSuperArmor = 144;
    public static final int kItemBlueTeamBase = 145;
    public static final int kItemRedTeamBase = 146;
    public static final int kItemBlueFlag = 147;
    public static final int kItemRedFlag = 148;
    public static final int kItemJetpack = 149;
    //public static final int		kGDXItemMapLevel = 150; // NoOne: kItemMax > 149 causes weird crash when starting multiplayer game! must be fixed!
    public static final int kItemMax = 149;
    public static final int kMaxItemTypes = (kItemMax - kItemBase);
    public static final int kMaxPowerUps = (kItemMax - kItemBase);
    public static final int kRespawnNever = 0;    // sprite cannot respawn
    public static final int kRespawnOptional = 1;    // sprite can optionally respawn in respawnTime seconds
    public static final int kRespawnAlways = 2;    // sprite always respawns in respawnTime seconds
    public static final int kRespawnPermanent = 3;    // sprite is permanent (respawnTime ignored)
    public static final String[] gItemText = { //kItemMax - kItemBase
            "Skull Key", "Eye Key", "Fire Key", "Dagger Key", "Spider Key", "Moon Key", "Key 7", "Doctor's Bag", "Medicine Pouch", "Life Essence", "Life Seed", "Red Potion", "Feather Fall", "Limited Invisibility", "INVULNERABILITY", "Boots of Jumping", "Raven Flight", "Guns Akimbo", "Diving Suit", "Gas mask", "Clone", "Crystal Ball", "Decoy", "Doppleganger", "Reflective shots", "Beast Vision", "ShadowCloak", "Rage shroom", "Delirium Shroom", "Grow shroom", "Shrink shroom", "Death mask", "Wine Goblet", "Wine Bottle", "Skull Grail", "Silver Grail", "Tome", "Black Chest", "Wooden Chest", "Asbestos Armor", "Basic Armor", "Body Armor", "Fire Armor", "Spirit Armor", "Super Armor", "Blue Team Base", "Red Team Base", "Blue Flag", "Red Flag", "Level map",};
    public static final String[] gWeaponText = {"RANDOM",    // kWeaponItemRandom
            "Sawed-off",        // kWeaponItemShotgun
            "Tommy Gun",        // kWeaponItemTommyGun
            "Flare Pistol",        // kWeaponItemFlareGun
            "Voodoo Doll",    // kWeaponItemVoodooDoll
            "Tesla Cannon",        // kWeaponItem
            "Napalm Launcher",    // kWeaponItem
            "Pitchfork",    // kWeaponItemPitchfork
            "Spray Can",        // kWeaponItemSprayCan
            "Dynamite",            // kWeaponItemTNT
            "Life Leech"};
    public static final String[] gAmmoText = {"Spray can", "Bundle of TNT*", "Bundle of TNT", "Case of TNT", "Proximity Detonator", "Remote Detonator", "Trapped Soul", "4 shotgun shells", "Box of shotgun shells", "A few bullets", "Voodoo Doll", "OBSOLETE", "Full drum of bullets", "Tesla Charge", "OBSOLETE", "OBSOLETE", "Flares", "OBSOLETE", "OBSOLETE", "Gasoline Can",};
    private static final int kFreeHead = 0;
    private static final short[] gHealthInfo = new short[kMaxPlayers];
    private static final PLAYER[] gPlayerInfo = new PLAYER[kMaxPlayers];
    public static XWALL[] xwall = new XWALL[kMaxXWalls];
    public static XSECTOR[] xsector = new XSECTOR[kMaxXSectors];
    public static int[] nextXWall = new int[kMaxXWalls];
    public static int[] nextXSector = new int[kMaxXSectors];
    public static int gSkyCount;
    public static int gVisibility;
    //// by NoOne: additional arrays for proximity and sight flag
    private static short gProxySpritesCount;
    private static byte gMaxBadProxySprites;

    private static short gSightSpritesCount;
    private static byte gMaxBadSightSprites;

    public static boolean prepareboard(final ScreenAdapter screen) {
        try {
            Console.out.println("debug: start prepareboard()", OsdColor.BLUE);
            BloodBoard board = dbLoadMap(pGameInfo.zLevelName);

            scrReset();
            PaletteView = kPalNormal;
            engine.getPaletteManager().setPalette(PaletteView);
            gViewMode = kView3D;
            gViewPos = 0;
            resetQuotes();
            gNoEnemies = 0;

            sndStopAllSounds();
            seqKillAll();

            if ((pGameInfo.uGameFlags & EndOfLevel) != 0) {
                for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
                    if (gPlayerInfo[j] == null) gPlayerInfo[j] = new PLAYER();
                    gPlayerInfo[j].copy(gPlayer[j]);
                    gPlayerInfo[j].pSprite = gPlayer[j].pSprite;
                    gHealthInfo[j] = (short) gPlayer[j].pXsprite.getHealth();
                }
            }

            sRandom(pGameInfo.uMapCRC);
            automapping = 1;

            levelResetKills();
            levelResetSecrets();

            gProxySpritesCount = 0;
            gSightSpritesCount = 0;
//            final short kMaxProximitySprites = kMaxXSprites / 16;
//            final short kMaxSightSprites = kMaxXSprites / 16;

            List<Sprite> sprites = board.getSprites();
            for (int i = 0; i < sprites.size(); i++) {
                BloodSprite pSprite = (BloodSprite) sprites.get(i);
                pSprite.setVelocity(0, 0, 0);
                XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                if (pSprite.getStatnum() != kStatFree && pXSprite != null) {

                    if (((1 << pGameInfo.nEnemyQuantity) & pXSprite.getLSkill()) != 0 || pXSprite.islS() && pGameInfo.nGameType == 0 || pXSprite.islB() && pGameInfo.nGameType == 2 || pXSprite.islT() && pGameInfo.nGameType == 3 || pXSprite.islC() && pGameInfo.nGameType == 1) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (!IsOriginalDemo(screen)) {

                        // by NoOne: add statnum for faster dude searching
                        if (pSprite.getLotag() == kGDXDudeTargetChanger) {
                            engine.changespritestat(i, kStatGDXDudeTargetChanger);
                        }

//                        // by NoOne: make Proximity and Sight flag work not just for dudes and things...
//                        if (pXSprite.isProximity() && gProxySpritesCount < kMaxProximitySprites) {
//                            switch (pSprite.getStatnum()) {
//                                // exceptions
//                                case kStatThing: // already treated in their methods
//                                case kStatDude: // already treated in their methods
//                                    if (pXSprite.isSight() && pXSprite.isDudeLockout()) pXSprite.setProximity(false);
//                                    break;
//                                case kStatEffect:
//                                case kStatExplosion:
//                                case kStatItem:
//                                case kStatPurge:
//                                case kStatMissile:
//                                case kStatSpares:
//                                case kStatFlare:
//                                case kStatInactive:
//                                case kStatFree:
//                                case kStatMarker:
//                                case kStatMarker2:
//                                    break;
//                                default:
//                                    if (pXSprite.isSight() && pXSprite.isDudeLockout()) pXSprite.setProximity(false);
//                                    else {
//                                        gProxySpritesCount++;
//                                        gMaxBadProxySprites = (byte) (gProxySpritesCount >> 2);
//                                        if (gMaxBadProxySprites <= 0) gMaxBadProxySprites = 1;
//                                        if (gProxySpritesCount == kMaxProximitySprites) {
//                                            String msg = "Max (" + kMaxProximitySprites + ") additional Proximity sprites reached!\n";
//                                            msg += "Please change your trigger system or stick with thing types (" + kThingBase + " - " + (kThingMax - 1) + "), because\n";
//                                            msg += "additional non-thing sprites with this flag only after this limit will not trigger anything.";
//
//                                            Console.out.println(msg, OsdColor.YELLOW);
//                                            Console.out.onToggle();
//                                        }
//
//                                    }
//
//                                    break;
//                            }
//                        }
//
//                        if (pXSprite.isSight() && gSightSpritesCount < kMaxSightSprites) {
//                            switch (pSprite.getStatnum()) {
//                                // exceptions
//                                case kStatEffect:
//                                case kStatExplosion:
//                                case kStatItem:
//                                case kStatPurge:
//                                case kStatMissile:
//                                case kStatSpares:
//                                case kStatFlare:
//                                case kStatInactive:
//                                case kStatFree:
//                                case kStatMarker:
//                                case kStatMarker2:
//                                    break;
//                                default:
//                                    gSightSpritesCount++;
//                                    gMaxBadSightSprites = (byte) (gSightSpritesCount >> 2);
//                                    if (gMaxBadSightSprites <= 0) gMaxBadSightSprites = 1;
//                                    if (gSightSpritesCount == kMaxSightSprites) {
//                                        String msg = "Max (" + kMaxSightSprites + ") Sight sprites reached!";
//                                        Console.out.println(msg, OsdColor.YELLOW);
//                                        Console.out.onToggle();
//                                    }
//                                    break;
//                            }
//                        }
                    }
                }
            }

            engine.getPaletteManager().loadPLUs(engine.getPaletteManager().isFogMode());
            BuildPos pos = board.getPos();
            int z = pos.getZ();
            if (pos.getSectnum() != -1) {
                z = engine.getflorzofslope(pos.getSectnum(), pos.getX(), pos.getY());
            }

            for (int i = 0; i < kMaxPlayers; i++) {
                gStartZone[i] = getStartZone(pos, z);

                if (i <= kMaxPlayers / 2) {
                    gStartZoneTeam1[i] = getStartZone(pos, z);
                    gStartZoneTeam2[i] = getStartZone(pos, z);
                }
            }

            InitSectorFX();
            InitPlayerStartZones(board);
            actInit(false, IsOriginalDemo(screen));

            // initialize all the tag buckets
            evInit(board, IsOriginalDemo(screen)); //activate original PriorityEvent


            if (pGameInfo.nFragLimit != 0) {
                Arrays.fill(nTeamCount, 0);
                Arrays.fill(nTeamClock, 0);
            }

            for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
                if ((pGameInfo.uGameFlags & EndOfLevel) == 0 || pGameInfo.nGameType > kNetModeCoop) {
                    if (numplayers == 1) {
                        game.net.gProfile[j].autoaim = cfg.gAutoAim;
                        game.net.gProfile[j].slopetilt = cfg.gSlopeTilt;
                        game.net.gProfile[j].skill = (byte) pGameInfo.nDifficulty;
                    }
                    playerInit(j, false);
                }
                playerReset(j);

                if (pGameInfo.nFragLimit != 0) {
                    gPlayer[j].fragCount = 0;
                    Arrays.fill(gPlayer[j].fragInfo, 0);
                }
            }

            if (kFakeMultiplayer) {
                for (int i = 1; i < nFakePlayers; i++) { //reset all other fake players
                    if ((pGameInfo.uGameFlags & EndOfLevel) == 0 || pGameInfo.nGameType > kNetModeCoop)
                        playerInit(i, false);
                    playerReset(i);
                }
            }

            if ((pGameInfo.uGameFlags & EndOfLevel) != 0) {
                for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
                    PLAYER pPlayer = gPlayer[j];
                    pPlayer.pXsprite.setHealth(gHealthInfo[j]);
                    pPlayer.pWeaponQAV = gPlayerInfo[j].pWeaponQAV;
                    pPlayer.currentWeapon = gPlayerInfo[j].currentWeapon;
                    pPlayer.weaponState = gPlayerInfo[j].weaponState;
                    pPlayer.weaponAmmo = gPlayerInfo[j].weaponAmmo;
                    pPlayer.weaponCallback = gPlayerInfo[j].weaponCallback;
                    pPlayer.fLoopQAV = gPlayerInfo[j].fLoopQAV;
                    pPlayer.weaponTimer = gPlayerInfo[j].weaponTimer;
                    pPlayer.updateWeapon = gPlayerInfo[j].updateWeapon;
                    pPlayer.LastWeapon = gPlayerInfo[j].LastWeapon;

                    for (int i = 0; i < kMaxPowerUps; i++) {
                        int nPowerUp = i + kItemBase;
                        if (cfg.gVanilla || (nPowerUp != kItemInvulnerability && nPowerUp != kItemGasMask && nPowerUp != kItemAsbestosArmor && nPowerUp != kItemFeatherFall && nPowerUp != kItemLtdInvisibility && nPowerUp != kItemRavenFlight && nPowerUp != kItemClone && nPowerUp != kItemDecoy && nPowerUp != kItemDoppleganger && nPowerUp != kItemReflectiveShots && nPowerUp != kItemShadowCloak && nPowerUp != kItemShroomRage && nPowerUp != kItemShroomDelirium && nPowerUp != kItemShroomGrow && nPowerUp != kItemShroomShrink && nPowerUp != kItemDeathMask && nPowerUp != kItemGunsAkimbo))
                            pPlayer.powerUpTimer[i] = gPlayerInfo[j].powerUpTimer[i];
                    }

                    if (!cfg.gVanilla && pPlayer.currentWeapon == kWeaponShotgun) { //shotgun akimbo disable
                        if (gInfiniteAmmo || (pPlayer.ammoCount[2] > 1)) pPlayer.weaponState = 3;
                        else pPlayer.weaponState = 2;
                    }

                    if (pGameInfo.nGameType == 0 && pGameInfo.nPitchforkOnly) {
                        for (int i = 0; i < 14; i++)
                            pPlayer.weaponMode[i] = 0;

                        pPlayer.hasWeapon[1] = true;
                        pPlayer.LastWeapon = 0;
                        pPlayer.currentWeapon = 0;
                        pPlayer.weaponCallback = -1;
                        pPlayer.pInput.newWeapon = 1;
                        for (int i = 0; i < 14; i++) {
                            pPlayer.weaponOrder[0][i] = defaultOrder[i];
                            pPlayer.weaponOrder[1][i] = defaultOrder[i];
                        }
                        for (int i = 0; i < 12; i++)
                            pPlayer.ammoCount[i] = 0;

                        pPlayer.weaponTimer = 0;
                        pPlayer.weaponState = 0;
                        pPlayer.pWeaponQAV = -1;
                        pPlayer.fLoopQAV = false;
                    }
                }
            }

            pGameInfo.uGameFlags &= ~(EndOfLevel | EndOfGame);

            if (SplitScreen) InsertPlayer();

            InitMirrors();
            SOUND.setReverb(false, 0);

            gFrame = 0;
            gFrameClock = 0;
            if (screen != gDemoScreen) {
                gViewIndex = myconnectindex;
            }

            trInit(board);
            ambPrepare();
            sndPlayMusic();

            Console.out.println("debug: end prepareboard()", OsdColor.BLUE);

            return true;
        } catch (Exception e) {
            Console.out.println("Load map exception: " + e);
            if (e.getMessage() != null) {
                game.GameMessage("Load map exception:\n" + e.getMessage());
            } else {
                game.GameMessage("Can't load the map " + pGameInfo.zLevelName);
            }
        }

        return false;
    }

    private static ZONE getStartZone(BuildPos pos, int z) {
        ZONE zone = new ZONE();
        zone.x = pos.getX();
        zone.y = pos.getY();
        zone.z = z;
        zone.sector = pos.getSectnum();
        zone.angle = pos.getAng();

        return zone;
    }

    public static BloodBoard dbLoadMap(Entry entry) throws WarningException {
        if (!entry.exists()) {
            throw new WarningException("Error opening map file \"" + entry.getName() + "\"");
        }
        return (BloodBoard) engine.loadboard(entry);
    }

    public static int dbInsertXSector(Sector sector, int nSector) {
        int nXSector = RemoveFree(nextXSector);
        if (nXSector == 0) System.err.println("Out of free XSectors");

        xsector[nXSector].free();
        sector.setExtra((short) nXSector);
        xsector[nXSector].reference = nSector;

        return nXSector;
    }

    public static void dbDeleteXSector(int nXSector) {
        if (xsector[nXSector].reference < 0) throw new AssertException("xsector[nXSector].reference >= 0");
        InsertFree(nextXSector, nXSector);

        // clear the references
        boardService.getSector(xsector[nXSector].reference).setExtra(-1);
        xsector[nXSector].reference = -1;    // clear the reference
    }

    public static void InsertFree(int[] next, int n) {
        next[n] = next[kFreeHead];
        next[kFreeHead] = n;
    }

    public static void InitFreeList(int[] xlist, int xlistSize) {
        for (int i = 1; i < xlistSize; i++)
            xlist[i] = (i - 1);
        xlist[kFreeHead] = (xlistSize - 1);
    }

    public static int RemoveFree(int[] next) {
        int n = next[kFreeHead];
        next[kFreeHead] = next[n];
        return n;
    }

    public static void InsertPlayer() {
        if (numplayers < kMaxPlayers) {
            connectpoint2[numplayers - 1] = numplayers;
            connectpoint2[numplayers] = -1;
            game.pNet.gNetFifoHead[numplayers] = game.pNet.gNetFifoHead[0]; // HACK 01/05/2000
            playerInit(numplayers, false);
            playerReset(numplayers);
            gPlayer[numplayers].pDudeInfo = dudeInfo[gPlayer[numplayers].pSprite.getLotag() - kDudeBase];
            gPlayer[numplayers].pSprite = boardService.getSprite(gPlayer[numplayers].nSprite);
            gPlayer[numplayers].pXsprite = boardService.getXSprite(gPlayer[numplayers].pSprite.getExtra());
            gViewIndex = numplayers;
            gMe = gPlayer[gViewIndex];

            numplayers++;
        }
    }

    public static void DeletePlayer() {
        if (numplayers > 1) {
            numplayers--;
            connectpoint2[numplayers - 1] = -1;
            engine.deletesprite((short) gPlayer[numplayers].nSprite);
            if (gViewIndex >= numplayers) {
                gViewIndex = 0;
                gMe = gPlayer[gViewIndex];
            }
        }
    }
}

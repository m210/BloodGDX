// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PriorityQueue.BPriorityQueue;
import ru.m210projects.Blood.PriorityQueue.IPriorityQueue;
import ru.m210projects.Blood.PriorityQueue.JPriorityQueue;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.XSECTOR;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.Board;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.util.List;

import static ru.m210projects.Blood.AI.Ai.aiInit;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.checkPlayerSeq;
import static ru.m210projects.Blood.SOUND.*;
import static ru.m210projects.Blood.Strings.seq;
import static ru.m210projects.Blood.Trig.*;
import static ru.m210projects.Blood.Trigger.*;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqKill;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqSpawn;
import static ru.m210projects.Blood.VERSION.getPlayerSeq;
import static ru.m210projects.Blood.VERSION.kPlayerFatality;
import static ru.m210projects.Blood.View.viewSetMessage;
import static ru.m210projects.Build.Engine.CLIPMASK0;
import static ru.m210projects.Build.Pragmas.*;

public class EVENT {

    public static final int kMaxChannels = 4096;
    public static final int kMaxID = 1024;

    public static final int kCommandOff = 0;
    public static final int kCommandOn = 1;
    public static final int kCommandState = 2;
    public static final int kCommandToggle = 3;
    public static final int kCommandNotState = 4;
    public static final int kCommandLink = 5;
    public static final int kCommandLock = 6;
    public static final int kCommandUnlock = 7;
    public static final int kCommandToggleLock = 8;
    public static final int kCommandStopOff = 9;
    public static final int kCommandStopOn = 10;
    public static final int kCommandStopNext = 11;
    public static final int kCommandCounter = 12;
    public static final int kCommandRespawn = 21;

    public static final int kCommandCallback = 20;
    public static final int kCallbackRespawn = 9;

    public static final int kCommandSpritePush = 30;
    public static final int kCommandSpriteImpact = 31;
    public static final int kCommandSpritePickup = 32;
    public static final int kCommandSpriteTouch = 33;
    public static final int kCommandSpriteSight = 34;
    public static final int kCommandSpriteProximity = 35;
    public static final int kCommandSpriteExplode = 36;

    public static final int kCommandSectorPush = 40;
    public static final int kCommandSectorImpact = 41;
    public static final int kCommandSectorEnter = 42;
    public static final int kCommandSectorExit = 43;

    public static final int kCommandWallPush = 50;
    public static final int kCommandWallImpact = 51;
    public static final int kCommandWallTouch = 52; // by NoOne: renamed from kCommandWallCross (see processTouchObjects())

    public static final int kGDXCommandPaste = 53; // used by some new types

    public static final int kCommandNumbered = 64;

    public static final int kChannelNull = 0;
    public static final int kChannelSetupSecret = 1;
    public static final int kChannelSecret = 2;
    public static final int kChannelTextOver = 3;
    public static final int kChannelEndLevelA = 4;
    public static final int kChannelEndLevelB = 5;

    public static final int kGDXChannelEndLevel = 6; // allows to select next map via kCommandNumbered

    public static final int kChannelTriggerStart = 7;    // channel triggered at startup
    public static final int kChannelTriggerMatch = 8;    // channel triggered at startup for BloodBath mode
    public static final int kChannelTriggerCoop = 9;    // channel triggered at startup for Coop mode
    public static final int kChannelTriggerTeam = 10;

    public static final int kChannelTeamADeath = 15;
    public static final int kChannelTeamBDeath = 16;

    public static final int kChannelFlag0Captured = 80;
    public static final int kChannelFlag1Captured = 81;

    public static final int kChannelRemoteFire1 = 90;
    public static final int kChannelRemoteFire2 = 91;
    public static final int kChannelRemoteFire3 = 92;
    public static final int kChannelRemoteFire4 = 93;
    public static final int kChannelRemoteFire5 = 94;
    public static final int kChannelRemoteFire6 = 95;
    public static final int kChannelRemoteFire7 = 96;
    public static final int kChannelRemoteFire8 = 97;

    public static final int kUserChannelStart = 100;
    public static final int kPQueueSize = 1024;
    public static final int kCallbackMax = 25;
    public static RXBUCKET[] rxBucket = new RXBUCKET[kMaxChannels];
    public static short[] bucketHead = new short[kMaxID + 1];
    public static IPriorityQueue eventQ;
    public static final CALLPROC[] gCallback = { //v1.21 ok
            /*0*/ EVENT::FireEffect,
            /*1*/ EVENT::KillSpriteCallback,
            /*2*/ EVENT::FlareStarburstCallback,
            /*3*/ EVENT::FlareFireEffect,
            /*4*/ EVENT::FlareFireEffect2, //not used?
            /*5*/ EVENT::ZombieOverHead,
            /*6*/ EVENT::BloodTrail,
            /*7*/ EVENT::LeechSparks,
            /*8*/ EVENT::SpawnSmoke,
            /*9*/ EVENT::RespawnCallback,
            /*10*/ EVENT::PlayerWaterBubblesCallback,
            /*11*/ EVENT::WaterBubblesCallback,
            /*12*/ EVENT::CounterCheck,
            /*13*/ EVENT::FatalityCallback,
            /*14*/ EVENT::BloodSplat,
            /*15*/ EVENT::AltTeslaEffect,
            /*16*/ EVENT::BulletFloor,
            /*17*/ EVENT::FlagCaptureCallback,
            /*18*/ EVENT::PodBloodTrail,
            /*19*/ EVENT::PodBloodSplat,
            /*20*/ EVENT::LeechCallback,
            /*21*/ nIndex -> {
    }, //not used, weapon callback
            /*22*/ EVENT::gdxFragLimitCallback, //GDX FragLimit callback
            /*23*/ EVENT::UniMissileBurstCallback, // by NoOne: similar to startBurstFlare, but for all missiles
            /*24*/ EVENT::makeMissileBlocking, // by NoOne: also required for bursting missiles
    };
    public static BPriorityQueue origEventQ;
    public static JPriorityQueue gdxEventQ;

    public static void makeMissileBlocking(int nSprite) {
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }
        pSprite.setCstat(pSprite.getCstat() | kSpriteBlocking);
    }

    public static void UniMissileBurstCallback(int nSprite) {
        BloodSprite pSprite = boardService.getSprite(nSprite);
        int nAngle = EngineUtils.getAngle((int) pSprite.getVelocityX(), (int) pSprite.getVelocityY());
        int nVel = 0x55555;
        for (int i = 0; i < 8; i++) {
            BloodSprite pBurst = actCloneSprite(pSprite, kStatMissile);
            pBurst.setLotag(pSprite.getLotag());
            pBurst.setShade(pSprite.getShade());
            pBurst.setPicnum(pSprite.getPicnum());

            pBurst.setCstat(pSprite.getCstat());
            if ((pBurst.getCstat() & kSpriteBlocking) != 0) {
                pBurst.setCstat(pBurst.getCstat() & ~kSpriteBlocking); // we don't want missiles impact each other
                evPostCallback(pBurst.getXvel(), SS_SPRITE, 100, 24); // so set blocking flag a bit later
            }

            pBurst.setPal(pSprite.getPal());
            pBurst.setXrepeat((short) (pSprite.getXrepeat() / 2));
            pBurst.setYrepeat((short) (pSprite.getYrepeat() / 2));
            pBurst.setClipdist(pSprite.getClipdist() / 2);
            pBurst.setHitag(pSprite.getHitag());
            pBurst.setAng((short) ((pSprite.getAng() + gMissileData[pSprite.getLotag() - kMissileBase].angleOfs) & kAngleMask));
            pBurst.setOwner(pSprite.getOwner());

            actBuildMissile(pBurst, pBurst.getExtra(), pSprite);

            int dxVel = 0;
            int dyVel = mulscaler(nVel, Sin(i * kAngle360 / 8), 30);
            int dzVel = mulscaler(nVel, -Cos(i * kAngle360 / 8), 30);

            if ((i & 1) != 0) {
                dyVel >>= 1;
                dzVel >>= 1;
            }

            RotateVector(dxVel, dyVel, nAngle);
            pBurst.addVelocity((int) rotated.x, (int) rotated.y, dzVel);

            evPostCallback(pBurst.getXvel(), SS_SPRITE, 960, 1);
        }
        evPostCallback(nSprite, SS_SPRITE, 0, 1);
    }

    public static XSPRITE getNextIncarnation(XSPRITE pXSprite) {
        if (pXSprite.getTxID() <= 0) {
            return null;
        }
        for (int i = bucketHead[pXSprite.getTxID()]; i < bucketHead[pXSprite.getTxID() + 1]; i++) {
            if (rxBucket[i].type != SS_SPRITE) {
                continue;
            }

            Sprite pSprite = boardService.getSprite(rxBucket[i].index);
            if (IsDudeSprite(pSprite) && pSprite.getStatnum() == kStatInactive) {
                return boardService.getXSprite(pSprite.getExtra());
            }
        }
        return null;
    }


    public static void gdxFragLimitCallback(int nIndex) {
        levelEndLevel(0);
    }

    public static void FlagCaptureCallback(int nIndex) {
        Sprite pFlag = boardService.getSprite(nIndex);
        if (pFlag == null) {
            throw new AssertException("nSprite != null");
        }

        if (pFlag.getOwner() >= 0 && pFlag.getOwner() < 4096) {
            Sprite pOwner = boardService.getSprite(pFlag.getOwner());
            int type = pFlag.getLotag();
            XSPRITE pXBase = boardService.getXSprite(pOwner.getExtra());

            if (type == kItemBlueFlag) {
                trTriggerSprite(pOwner.getXvel(), pXBase, 1);
                sndStartSample(8003, 255, 2, false);
                viewSetMessage("Blue Flag returned to base.", -1, 10);
            }

            if (type == kItemRedFlag) {
                trTriggerSprite(pOwner.getXvel(), pXBase, 1);
                sndStartSample(8002, 255, 2, false);
                viewSetMessage("Red Flag returned to base.", -1, 7);
            }
        }
        evPostCallback(nIndex, SS_SPRITE, 0, 1);
    }

    public static void SpawnSmoke(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite.getVelocityZ() != 0) {
            int radius = (engine.getTile(pSprite.getPicnum()).getWidth() / 2) * pSprite.getXrepeat() >> 2;

            BloodSprite pEffect = actSpawnEffect(7, pSprite.getSectnum(), pSprite.getX() + mulscale(radius, Cos(pSprite.getAng() - kAngle90), 30), pSprite.getY() + mulscale(radius, Sin(pSprite.getAng() - kAngle90), 30), pSprite.getZ(), 0);

            if (pEffect != null) {
                pEffect.setVelocity(pSprite.getVelocityX(), pSprite.getVelocityY(), pSprite.getVelocityZ());
            }
        }

        if (IsOriginalDemo() || eventQ.getSize() < 768) {
            evPostCallback(nIndex, SS_SPRITE, 12, 8);
        }
    }

    public static void KillSpriteCallback(int nIndex) {
        checkEventList(nIndex, SS_SPRITE);
        BloodSprite pSprite = boardService.getSprite(nIndex);
        int nXSprite = pSprite.getExtra();
        if (nXSprite >= 0) {
            seqKill(SS_SPRITE, nXSprite);
        }
        sfxKill3DSound(pSprite, -1, -1);
        engine.deletesprite(nIndex);
    }

    public static void FlareFireEffect(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        BloodSprite pEffect = actSpawnEffect(28, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);

        if (pEffect != null) {
            pEffect.setVelocity(
                    pSprite.getVelocityX() + BiRandom(109226),
            pSprite.getVelocityY() + BiRandom(109226),
            pSprite.getVelocityZ() - Random(109226));
        }
        evPostCallback(nIndex, SS_SPRITE, 4, 3);
    }

    public static void FlareFireEffect2(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        BloodSprite pEffect = actSpawnEffect(28, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
        if (pEffect != null) {
            pEffect.setVelocity(pSprite.getVelocityX() + BiRandom(109226),
            pSprite.getVelocityY() + BiRandom(109226),
            pSprite.getVelocityZ() - Random(109226));
        }
        evPostCallback(nIndex, SS_SPRITE, 12, 4);
    }

    public static void AltTeslaEffect(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        BloodSprite pEffect = actSpawnEffect(49, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
        if (pEffect != null) {
            pEffect.setVelocity(pSprite.getVelocityX() + BiRandom(109226),
            pSprite.getVelocityY() + BiRandom(109226),
            pSprite.getVelocityZ() - Random(109226));
        }

        if (IsOriginalDemo() || eventQ.getSize() < 768) {
            evPostCallback(nIndex, SS_SPRITE, 3, 15);
        }
    }

    public static void FatalityCallback(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (checkPlayerSeq(gPlayer[pSprite.getLotag() - kDudePlayer1], getPlayerSeq(kPlayerFatality))) {
            if (pXSprite.getTarget() == gMe.nSprite) {
                sndStartSample(3313, -1, 2, false);
            }
        }
    }

    public static void WaterBubblesCallback(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        GetSpriteExtents(pSprite);
        for (int i = 0; i < klabs(pSprite.getVelocityZ()) >> 18; i++) {
            int radius = pSprite.getXrepeat() * (engine.getTile(pSprite.getPicnum()).getWidth() / 2) >> 2;
            int nAngle = Random(2048);
            int dx = mulscale(Cos(nAngle), radius, 30);
            int dy = mulscale(Sin(nAngle), radius, 30);

            int range = extents_zBot - Random(extents_zBot - extents_zTop);

            BloodSprite pEffect = actSpawnEffect(Random(3) + 23, pSprite.getSectnum(), pSprite.getX() + dx, pSprite.getY() + dy, range, 0);
            if (pEffect != null) {
                pEffect.setVelocity(pSprite.getVelocityX() + BiRandom(109226),
                pSprite.getVelocityY() + BiRandom(109226),
                pSprite.getVelocityZ() + BiRandom(109226));
            }
        }

        if (IsOriginalDemo() || eventQ.getSize() < 768) {
            evPostCallback(nIndex, SS_SPRITE, 4, 11);
        }
    }

    private static void PlayerWaterBubblesCallback(int nSprite) {
        final BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        if (IsPlayerSprite(pSprite)) {
            PLAYER pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];
            if (pPlayer == null) {
                throw new AssertException("pPlayer != NULL");
            }
            if (pPlayer.bubbleTime != 0) {
                GetSpriteExtents(pSprite);
                for (int i = 0; i < pPlayer.bubbleTime >> 6; i++) {
                    ArtEntry pic = engine.getTile(pSprite.getPicnum());

                    int radius = (pSprite.getXrepeat() * (pic.getWidth() / 2)) >> 2;
                    int nAngle = Random(2048);

                    int dx = pSprite.getX() + mulscale(Cos(nAngle), radius, 30);
                    int dy = pSprite.getY() + mulscale(Sin(nAngle), radius, 30);

                    int range = extents_zBot - Random(extents_zBot - extents_zTop);

                    BloodSprite pEffect = actSpawnEffect((Random(3) + 23), pSprite.getSectnum(), dx, dy, range, 0);
                    if (pEffect != null) {
                        pEffect.setVelocity(BiRandom(0x1AAAA) + pSprite.getVelocityX(),
                        BiRandom(0x1AAAA) +pSprite.getVelocityY(),
                        BiRandom(0x1AAAA) + pSprite.getVelocityZ());
                    }
                }
                evPostCallback(nSprite, SS_SPRITE, 4, 10);
            }
        }
    }

    public static void LeechSparks(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        BloodSprite pEffect = actSpawnEffect(15, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
        if (pEffect != null) {
            pEffect.setVelocity(BiRandom(65536) + pSprite.getVelocityX(),
           BiRandom(65536) + pSprite.getVelocityY(),
           pSprite.getVelocityZ() - Random(0x1AAAA));
        }
        evPostCallback(nIndex, SS_SPRITE, 3, 7);
    }

    public static void RespawnCallback(final int nSprite) {
        final BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        if (pSprite.getStatnum() != kStatRespawn && pSprite.getStatnum() != kStatThing) {
            Console.out.println("Sprite " + nSprite + " is not on Respawn list", OsdColor.RED);
        }
        if ((pSprite.getHitag() & kAttrRespawn) == 0) {
            Console.out.println("Sprite " + nSprite + " does not have the respawn attribute", OsdColor.RED);
        }

        int respawnTime = 0;
        switch (pXSprite.getRespawnPending()) {
            case 1: //kRespawnRed
                respawnTime = mulscale(actGetRespawnTime(pSprite), 16384, 16);
                pXSprite.setRespawnPending(2);
                evPostCallback(nSprite, SS_SPRITE, Integer.toUnsignedLong(respawnTime), kCallbackRespawn);
                return;
            case 2: //kRespawnYellow
                respawnTime = mulscale(actGetRespawnTime(pSprite), 8192, 16);
                pXSprite.setRespawnPending(3);
                evPostCallback(nSprite, SS_SPRITE, Integer.toUnsignedLong(respawnTime), kCallbackRespawn);
                return;
            case 3: //kRespawnGreen
                if (pSprite.getOwner() == kStatRespawn) {
                    throw new AssertException("pSprite.owner != kStatRespawn");
                }
                engine.changespritestat(nSprite, pSprite.getOwner());
                pSprite.setOwner(-1);
                pSprite.setLotag(pSprite.getZvel());
                pSprite.setHitag(pSprite.getHitag() & ~kAttrRespawn);
                pSprite.setVelocity(0, 0, 0);

                pXSprite.setRespawnPending(0);
                pXSprite.setTriggered(false);
                pXSprite.setBurnTime(0);

                if (IsDudeSprite(pSprite)) {
                    pXSprite.setKey(0);
                    Vector3 kSprite = ((BloodSprite) pSprite).getKSprite();

                    pSprite.setX((int) kSprite.x);
                    pSprite.setY((int) kSprite.y);
                    pSprite.setZ((int) kSprite.z);
                    pSprite.setCstat(pSprite.getCstat() | 0x1101);
                    int nType = pSprite.getLotag();
                    pSprite.setClipdist(dudeInfo[nType - kDudeBase].clipdist);
                    pXSprite.setHealth(dudeInfo[nType - kDudeBase].startHealth << 4);

                    if (game.getCache().contains(dudeInfo[nType - kDudeBase].seqStartID + kSeqDudeIdle, seq)) {
                        seqSpawn(dudeInfo[nType - kDudeBase].seqStartID + kSeqDudeIdle, SS_SPRITE, pSprite.getExtra(), null);
                    }
                    aiInit(pSprite, IsOriginalDemo());
                }

                if (pSprite.getLotag() == 400) {
                    pSprite.setCstat(pSprite.getCstat() | (kSpriteBlocking | kSpriteHitscan));
                    pSprite.setCstat(pSprite.getCstat() & ~kSpriteInvisible);
                }
                actSpawnEffect(29, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
                sfxStart3DSound(pSprite, 350, -1, 0);
                return;
        }

        Console.out.println("Unexpected respawnPending value = " + pXSprite.getRespawn());
    }

    public static void ZombieOverHead(final int nSprite) {
        final BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        GetSpriteExtents(pSprite);
        BloodSprite pEffect = actSpawnEffect(27, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), extents_zTop, 0);
        if (pEffect != null) {
            pEffect.setVelocity(pSprite.getVelocityX() + BiRandom(69905),
            pSprite.getVelocityY() + BiRandom(69905),
                    pSprite.getVelocityZ() - 436906);
        }

        if (pXSprite.getData1() <= 0) {
            if (pXSprite.getData2() > 0) {
                evPostCallback(nSprite, SS_SPRITE, 60, 5);
                pXSprite.setData1(40);
                pXSprite.setData2(pXSprite.getData2() - 1);
            }
        } else {
            evPostCallback(nSprite, SS_SPRITE, 4, 5);
            pXSprite.setData1(pXSprite.getData1() - kFrameTicks);
        }
    }

    public static void FireEffect(final int nSprite) {
        final BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        GetSpriteExtents(pSprite);
        for (int i = 0; i < 3; i++) {
            int size = pSprite.getXrepeat() * (engine.getTile(pSprite.getPicnum()).getWidth() / 2) >> 3;
            int nAngle = Random(2048);
            int dx = mulscale(Cos(nAngle), size, 30);
            int dy = mulscale(Sin(nAngle), size, 30);

            BloodSprite pEffect = actSpawnEffect(32, pSprite.getSectnum(), pSprite.getX() + dx, pSprite.getY() + dy, extents_zBot - Random(extents_zBot - extents_zTop), 0);
            if (pEffect != null) {
                pEffect.setVelocity(pSprite.getVelocityX() + BiRandom(-dx),
                pSprite.getVelocityY() + BiRandom(-dy),
                pSprite.getVelocityZ() - Random(109226));
            }
        }

        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite != null && pXSprite.getBurnTime() != 0) {
            evPostCallback(nSprite, SS_SPRITE, 5, 0);
        }
    }

    public static void FlareStarburstCallback(int nSprite) {
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        int nAngle = EngineUtils.getAngle((int) pSprite.getVelocityX(), (int) pSprite.getVelocityY());
        int nVel = 0x55555;

        for (int i = 0; i < 8; i++) {
            BloodSprite pBurst = actCloneSprite(pSprite, kStatMissile);

            pBurst.setPicnum(2424);
            pBurst.setShade(-128);
            pBurst.setYrepeat(32);
            pBurst.setLotag(kMissileStarburstFlare);
            pBurst.setClipdist(2);
            pBurst.setOwner(pSprite.getOwner());
            pBurst.setXrepeat(pBurst.getYrepeat());

            int dxVel = 0;
            int dyVel = mulscaler(nVel, Sin(i * kAngle360 / 8), 30);
            int dzVel = mulscaler(nVel, -Cos(i * kAngle360 / 8), 30);

            if ((i & 1) != 0) {
                dyVel >>= 1;
                dzVel >>= 1;
            }

            RotateVector(dxVel, dyVel, nAngle);
            pBurst.addVelocity((int) rotated.x,
             (int) rotated.y,
             dzVel);

            evPostCallback(pBurst.getXvel(), SS_SPRITE, 960, 1);
        }
        evPostCallback(nSprite, SS_SPRITE, 0, 1);
    }

    public static void BulletFloor(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        GetZRange(pSprite, pSprite.getClipdist(), CLIPMASK0);

        GetSpriteExtents(pSprite);

        pSprite.setZ(pSprite.getZ() + (gz_floorZ - extents_zBot));
        int velocity = (int) (pSprite.getVelocityZ() - floorVel[pSprite.getSectnum()]);
        if (velocity <= 0) {
            if (pSprite.getVelocityZ() != 0) {
                return;
            }

            updateBullet(pSprite);
            return;
        }
        GravityVector(pSprite.getVelocityX(), pSprite.getVelocityY(), velocity, pSprite.getSectnum(), 36864);
        pSprite.setVelocity(refl_x, refl_y, refl_z);

        if (floorVel[pSprite.getSectnum()] == 0) {
            if (klabs(pSprite.getVelocityZ()) < 0x20000) {
                updateBullet(pSprite);
                return;
            }
        }

        int nChannel = (pSprite.getXvel() & 2) + 28;
        if (!IsOriginalDemo()) {
            pSprite.setAng((short) ((pSprite.getAng() + vRandom()) & kAngleMask));
        }

        if (pSprite.getLotag() < 37 || pSprite.getLotag() > 39) { //40, 41 - shotgun shells
            int snd = 612;
            if (Chance(0x4000)) {
                snd = 610;
            }
            sfxStart3DSound(pSprite, snd, nChannel, 1);
        } else { //37, 38, 39 - tommygun shells
            bRandom();
            sfxStart3DSound(pSprite, Random(2) + 608, nChannel, 1);
        }
    }

    public static void updateBullet(BloodSprite pSprite) {
        pSprite.setVelocity(0,0,0);

        int nXSprite = pSprite.getExtra();
        if (nXSprite >= 0) {
            seqKill(SS_SPRITE, nXSprite);
        }
        sfxKill3DSound(pSprite, -1, -1);

        if (pSprite.getLotag() < 37 || pSprite.getLotag() > 39) {
            pSprite.setPicnum(2464);
        } else {
            pSprite.setPicnum(2465);
        }
        pSprite.setLotag(51);
        pSprite.setYrepeat(10);
        pSprite.setXrepeat(pSprite.getYrepeat());
    }

    public static void BloodSplat(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        GetZRange(pSprite, pSprite.getClipdist(), CLIPMASK0);

        GetSpriteExtents(pSprite);

        pSprite.setZ(pSprite.getZ() + (gz_floorZ - extents_zBot));

        int nAngle = Random(2048);
        int Dist = Random(16) << 4;
        int x = pSprite.getX() + mulscale(Cos(nAngle), Dist, 28);
        int y = pSprite.getY() + mulscale(Sin(nAngle), Dist, 28);

        actSpawnEffect(48, pSprite.getSectnum(), x, y, pSprite.getZ(), 0);
        if (pSprite.getAng() == 1024) {
            int nChannel = (pSprite.getXvel() & 2) + 28;
            if (nChannel >= 32) {
                throw new AssertException("nChannel < 32");
            }
            sfxStart3DSound(pSprite, BLUDSPLT, nChannel, 1);
        }

        if (Chance(10240)) {
            Sprite pSpawn = actSpawnEffect(36, pSprite.getSectnum(), x, y, gz_floorZ - 64, 0);
            if (pSpawn != null) {
                if (!IsOriginalDemo() && pSprite.getSectnum() != pSpawn.getSectnum()) {
                    pSpawn.setZ(engine.getflorzofslope(pSpawn.getSectnum(), x, y) - 64);
                }
                pSpawn.setAng((short) nAngle);
            }
        }
        actDeleteEffect2(nIndex);
    }

    public static void PodBloodSplat(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        GetZRange(pSprite, pSprite.getClipdist(), CLIPMASK0);

        GetSpriteExtents(pSprite);

        pSprite.setZ(pSprite.getZ() + (gz_floorZ - extents_zBot));

        int nAngle = Random(2048);
        int Dist = Random(16) << 4;
        int x = pSprite.getX() + mulscale(Cos(nAngle), Dist, 28);
        int y = pSprite.getY() + mulscale(Sin(nAngle), Dist, 28);

        if (pSprite.getAng() == 1024 && pSprite.getLotag() == 53) {
            int nChannel = (pSprite.getXvel() & 2) + 28;
            sfxStart3DSound(pSprite, BLUDSPLT, nChannel, 1);
        }

        if (pSprite.getLotag() != 53 && pSprite.getLotag() != kThingPodGreen) {
            Sprite pSpawn = actSpawnEffect(32, pSprite.getSectnum(), x, y, gz_floorZ - 64, 0);
            if (pSpawn != null) {
                if (pSprite.getSectnum() != pSpawn.getSectnum()) {
                    pSpawn.setZ(engine.getflorzofslope(pSpawn.getSectnum(), x, y) - 64);
                }
                pSpawn.setAng((short) nAngle);
            }

        } else {
            if (Chance(640) || pSprite.getLotag() == kThingPodGreen) {
                Sprite pSpawn = actSpawnEffect(55, pSprite.getSectnum(), x, y, gz_floorZ - 64, 0);
                if (pSpawn != null) {
                    if (pSprite.getSectnum() != pSpawn.getSectnum()) {
                        pSpawn.setZ(engine.getflorzofslope(pSpawn.getSectnum(), x, y) - 64);
                    }
                    pSpawn.setAng((short) nAngle);
                }
            }
        }

        actDeleteEffect2(nIndex);
    }

    private static void LeechCallback(int nIndex) {
        BloodSprite pSprite = boardService.getSprite(nIndex);
        if (pSprite.getStatnum() == 4 && (pSprite.getHitag() & kAttrFree) == 0) {

            switch (pSprite.getLotag()) {
                case kThingLifeLeech:
                case kGDXThingCustomDudeLifeLeech:
                    boardService.getXSprite(pSprite.getExtra()).setStateTimer(0);
                    break;
            }
        }
    }

    public static void CounterCheck(int nSector) {
        if (!boardService.isValidSector(nSector)) {
            throw new AssertException("boardService.isValidSector(nSector)");
        }
        Sector pSector = boardService.getSector(nSector);
        // remove check below, so every sector can be counter if command 12 (this callback) received.
        // besides, it's useless anyway.
        //if ( pSector.lotag != kSectorCounter ) return;
        int nXSector = pSector.getExtra();
        if (nXSector > 0) {
            XSECTOR pXSector = xsector[nXSector];
            int data = pXSector.data;
            int waitTime = pXSector.waitTime[1];
            if (data != 0 && waitTime != 0) {
                int cnt = 0;
                for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                    if (node.get().getLotag() == data) {
                        ++cnt;
                    }
                }

                if (cnt < waitTime) {
                    evPostCallback(nSector, SS_SECTOR, 5, kCommandCounter);
                } else {
                    //pXSector.waitTime[1] = 0; // do not reset necessary objects counter to zero
                    trTriggerSector(nSector, pXSector, kCommandOn);
                    //Println("HEHE");

                    pXSector.locked = 1; //lock sector, so it can be opened again later
                }
            }
        }
    }

    public static void BloodTrail(final int nSprite) {
        final BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite == null) {
            throw new AssertException("nSprite != null");
        }

        BloodSprite pEffect = actSpawnEffect(27, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
        if (pEffect != null) {
            pEffect.setAng(0);
            pEffect.setVelocity(pSprite.getVelocityX() >> 8,
            pSprite.getVelocityY() >> 8,
            pSprite.getVelocityZ() >> 8);
        }
        evPostCallback(nSprite, SS_SPRITE, 6, 6);
    }

    public static void PodBloodTrail(final int nSprite) {
        final BloodSprite pSprite = boardService.getSprite(nSprite);

        int type = 53;
        if (pSprite.getLotag() != 53) {
            type = 54;
        }

        BloodSprite pEffect = actSpawnEffect(type, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
        if (pEffect != null) {
            pEffect.setAng(0);
            pEffect.setVelocity(pSprite.getVelocityX() >> 8,
            pSprite.getVelocityY() >> 8,
            pSprite.getVelocityZ() >> 8);
        }
        evPostCallback(nSprite, SS_SPRITE, 6, 18);
    }

    public static int getEvent(int index, int type, int command, int funcId) {
        return ((index & 0x1FFF) | (type & 7) << 13 | (command << 16) | (funcId << 24));
    }

    public static int getIndex(int event) {
        return (event & 0x1FFF);
    }

    public static int getType(int event) {
        return (event & 0xE000) >> 13;
    }

    public static int getCommand(int event) {
        return (event & 0xFF0000) >> 16;
    }

    public static int getFuncID(int event) {
        return event >> 24;
    }

    static int GetBucketChannel(RXBUCKET pBucket) {
        int nXIndex;
        switch (pBucket.type) {
            case SS_SECTOR:
                nXIndex = boardService.getSector(pBucket.index).getExtra();
                if (nXIndex < 0) {
                    throw new AssertException("Sector nXIndex > 0");
                }
                return xsector[nXIndex].rxID;

            case SS_WALL:
                nXIndex = boardService.getWall(pBucket.index).getExtra();
                if (nXIndex < 0) {
                    throw new AssertException("Wall nXIndex > 0");
                }
                return xwall[nXIndex].rxID;

            case SS_SPRITE:
                nXIndex = boardService.getSprite(pBucket.index).getExtra();
                if (nXIndex < 0) {
                    throw new AssertException("Sprite nXIndex >= 0");
                }
                return boardService.getXSprite(nXIndex).getRxID();
        }

        Console.out.println("Unexpected rxBucket type " + pBucket.type, OsdColor.RED);
        return 0;
    }

    public static int CompareChannels(RXBUCKET ref1, RXBUCKET ref2) {
        return GetBucketChannel(ref1) - GetBucketChannel(ref2);
    }

    public static void eventQueryInit(boolean isOriginal) {
        if (origEventQ == null) {
            origEventQ = new BPriorityQueue(kPQueueSize);
        }
        if (gdxEventQ == null) {
            gdxEventQ = new JPriorityQueue(kPQueueSize);
        }

        if (isOriginal) {
            eventQ = origEventQ;
        } else {
            eventQ = gdxEventQ;
        }
    }

    public static void evInit(Board board, boolean isOriginal) {
        eventQueryInit(isOriginal);

        for (int i = 0; i < kMaxChannels; i++) {
            if (rxBucket[i] == null) {
                rxBucket[i] = new RXBUCKET();
            } else {
                rxBucket[i].flush();
            }
        }

        eventQ.flush();

        int nCount = 0;
        // add all the tags to the bucket array
        for (int i = 0; i < board.getSectorCount(); i++) {
            if (board.getSector(i) == null) {
                continue;
            }

            int nXSector = board.getSector(i).getExtra();
            if (nXSector > 0 && xsector[nXSector].rxID > 0) {
                if (nCount >= kMaxChannels) {
                    throw new AssertException("nCount < kMaxChannels");
                }
                rxBucket[nCount].type = SS_SECTOR;
                rxBucket[nCount].index = i;
                nCount++;
            }
        }

        for (int i = 0; i < board.getWallCount(); i++) {
            if (board.getWall(i) == null) {
                continue;
            }

            int nXWall = board.getWall(i).getExtra();
            if (nXWall > 0 && xwall[nXWall].rxID > 0) {
                if (nCount >= kMaxChannels) {
                    throw new AssertException("nCount < kMaxChannels");
                }
                rxBucket[nCount].type = SS_WALL;
                rxBucket[nCount].index = i;
                nCount++;
            }
        }

        List<Sprite> sprites = board.getSprites();
        for (int i = 0; i < sprites.size(); i++) {
            if (sprites.get(i).getStatnum() < kMaxStatus) {
                int nXSprite = sprites.get(i).getExtra();
                if (nXSprite >= 0 && boardService.getXSprite(nXSprite).getRxID() > 0) {
                    if (nCount >= kMaxChannels) {
                        throw new AssertException("nCount < kMaxChannels");
                    }
                    rxBucket[nCount].type = SS_SPRITE;
                    rxBucket[nCount].index = i;
                    nCount++;
                }
            }
        }

        // sort the array on rx tags
//		Arrays.sort(rxBucket, 0, nCount);

        qsort(nCount);

        // create the list of header indices
        int i, j = 0;
        for (i = 0; i < kMaxID; i++) {
            bucketHead[i] = (short) j;
            while (j < nCount && GetBucketChannel(rxBucket[j]) == i) {
                j++;
            }
        }
        bucketHead[i] = (short) j;
    }

    public static boolean evGetSourceState(int type, int nIndex) {
        int nXIndex;

        switch (type) {
            case SS_SECTOR:
                nXIndex = boardService.getSector(nIndex).getExtra();
                if (!(nXIndex > 0 && nXIndex < kMaxXSectors)) {
                    throw new AssertException("nXIndex > 0 && nXIndex < kMaxXSectors");
                }
                return xsector[nXIndex].state != 0;

            case SS_WALL:
                nXIndex = boardService.getWall(nIndex).getExtra();
                if (!(nXIndex > 0 && nXIndex < kMaxXWalls)) {
                    throw new AssertException("nXIndex > 0 && nXIndex < kMaxXWalls");
                }
                return xwall[nXIndex].state != 0;

            case SS_SPRITE:
                nXIndex = boardService.getSprite(nIndex).getExtra();
                return boardService.getXSprite(nXIndex).getState() != 0;
        }

        // shouldn't reach this point
        return false;
    }

    public static void evSend(int index, int type, int to, int command) {
        if (command == kCommandState) {
            command = evGetSourceState(type, index) ? kCommandOn : kCommandOff;
        } else if (command == kCommandNotState) {
            command = evGetSourceState(type, index) ? kCommandOff : kCommandOn;
        }

        int pEvent = getEvent(index, type, command, 0);

        // handle transmit-only system triggers
        if (to > kChannelNull) {
            switch (to) {
                case kChannelSetupSecret:
                    if (command < kCommandNumbered) {
                        Console.out.println("Invalid SetupSecret command by xobject " + index, OsdColor.RED);
                    }
                    levelSetupSecret(command - kCommandNumbered);
                    break;
                case kChannelSecret:
                    if (command < kCommandNumbered) {
                        Console.out.println("Invalid Secret command by xobject " + index, OsdColor.RED);
                    }
                    levelCountSecret(command - kCommandNumbered);
                    break;
                case kChannelEndLevelA:
                    levelEndLevel(0);
                    // Hooray.  You finished the level.
                    return;
                case kChannelEndLevelB:
                    levelEndLevel(1);
                    // Hooray.  You finished the level via the secret ending.
                    return;
                case kGDXChannelEndLevel:
                    // finished level and load custom level � via kCommandNumbered.
                    levelEndLevelCustom(command - kCommandNumbered);
                    return;
                case kChannelTextOver:
                    if (command < kCommandNumbered) {
                        Console.out.println("Invalid TextOver command by xobject " + index, OsdColor.RED);
                        viewSetMessage("Invalid TextOver command", -1, 7);
                        return;
                    }
                    trTextOver(command - kCommandNumbered);
                    return;

                case kChannelRemoteFire1:
                case kChannelRemoteFire2:
                case kChannelRemoteFire3:
                case kChannelRemoteFire4:
                case kChannelRemoteFire5:
                case kChannelRemoteFire6:
                case kChannelRemoteFire7:
                case kChannelRemoteFire8:
                    // these can't use the rx buckets since they are dynamically created
                    for (ListNode<Sprite> node = boardService.getStatNode(kStatThing); node != null; node = node.getNext()) {
                        int nSprite = node.getIndex();
                        Sprite pSprite = node.get();
                        if ((pSprite.getHitag() & kAttrFree) == 0) {
                            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                            if (pXSprite != null && pXSprite.getRxID() == to) {
                                trMessageSprite(nSprite, pEvent);
                            }
                        }
                    }
                    return;
                case kChannelFlag0Captured:
                case kChannelFlag1Captured:
                    for (ListNode<Sprite> node = boardService.getStatNode(kStatItem); node != null; node = node.getNext()) {
                        int nSprite = node.getIndex();
                        Sprite pSprite = node.get();
                        if ((pSprite.getHitag() & kAttrFree) == 0) {
                            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                            if (pXSprite != null && pXSprite.getRxID() == to) {
                                trMessageSprite(nSprite, pEvent);
                            }
                        }
                    }
                    return;
            }
        }

        // the event is a broadcast message
        for (int i = bucketHead[to]; i < bucketHead[to + 1]; i++) {
            // don't send it to the originator
            if (rxBucket[i].type == getType(pEvent) && rxBucket[i].index == getIndex(pEvent)) {
                continue;
            }

            switch (rxBucket[i].type) {
                case SS_SECTOR:
                    trMessageSector(rxBucket[i].index, pEvent);
                    break;

                case SS_WALL:
                    trMessageWall(rxBucket[i].index, pEvent);
                    break;

                case SS_SPRITE:
                    Sprite pSprite = boardService.getSprite(rxBucket[i].index);
                    if ((pSprite.getHitag() & kAttrFree) == 0) {
                        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                        if (pXSprite != null && pXSprite.getRxID() != 0) {
                            trMessageSprite(rxBucket[i].index, pEvent);
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    }

    public static void checkEventList(int nIndex, int nType) {
        eventQ.checkList(nIndex, nType);
    }

    public static void checkEventList(int nIndex, int nType, int funcId) {
        eventQ.checkList(nIndex, nType, funcId);
    }

    public static void evPost(int index, int type, /* unsigned */ long time, int command) {
        if (command == kCommandCallback) {
            throw new AssertException("command != kCommandCallback");
        }

        if (command == kCommandState) {
            command = evGetSourceState(type, index) ? kCommandOn : kCommandOff;
        } else if (command == kCommandNotState) {
            command = evGetSourceState(type, index) ? kCommandOff : kCommandOn;
        }

        int pEvent = getEvent(index, type, command, 0);
        eventQ.Insert(gFrameClock + time, pEvent);
    }

    public static void evPostCallback(int index, int type, long time, int funcId) {
        int pEvent = getEvent(index, type, kCommandCallback, funcId);
        eventQ.Insert(gFrameClock + time, pEvent);
    }

    public static void evProcess(long time) {
        // while there are events to be processed
        while (eventQ.Check(time)) {
            int event = eventQ.Remove();
            if (event == 0) {
                continue;
            }

            // is it a special callback event?
            if (getCommand(event) == kCommandCallback) {
                int nFunc = getFuncID(event);
                if (nFunc >= kCallbackMax) {
                    throw new AssertException("event.funcID < kCallbackMax");
                }
                if (gCallback[nFunc] == null) {
                    throw new AssertException("gCallback[event.funcID] != null");
                }
                gCallback[nFunc].run(getIndex(event));
                continue;
            }

            switch (getType(event)) {
                case SS_SECTOR:
                    trMessageSector(getIndex(event), event);
                    break;

                case SS_WALL:
                    trMessageWall(getIndex(event), event);
                    break;

                case SS_SPRITE:
                    trMessageSprite(getIndex(event), event);
                    break;
            }
        }
    }

    private static void qsort(int n) {
        int base = 0, sp = 0;

        int[] base_stack = new int[4 * 8];
        int[] n_stack = new int[4 * 8];

        while (true) {
            while (n > 1) {
                if (n < 16) {
                    for (int shell = 3; shell > 0; shell -= 2) {
                        for (int p1 = base + shell; p1 < base + n; p1 += shell) {
                            for (int p2 = p1; p2 > base && CompareChannels(rxBucket[p2 - shell], rxBucket[p2]) > 0; p2 -= shell) {
                                swap(p2, p2 - shell);
                            }
                        }
                    }
                    break;
                } else {
                    int mid = base + (n >> 1);
                    if (n > 29) {
                        int p1 = base;
                        int p2 = base + (n - 1);
                        if (n > 42) {
                            int s = (n >> 3);
                            p1 = med3(p1, p1 + s, p1 + (s << 1));
                            mid = med3(mid - s, mid, mid + s);
                            p2 = med3(p2 - (s << 1), p2 - s, p2);
                        }
                        mid = med3(p1, mid, p2);
                    }

//                  int pv = base;
//    				swap( pv, mid );

                    RXBUCKET pv = rxBucket[mid];

                    int pa, pb, pc, comparison;
                    pa = pb = base;
                    int pd = base + (n - 1);

                    for (pc = base + (n - 1); ; pc--) {
                        while (pb <= pc && (comparison = CompareChannels(rxBucket[pb], pv)) <= 0) {
                            if (comparison == 0) {
                                swap(pa, pb);
                                pa++;
                            }
                            pb++;
                        }
                        while (pb <= pc && (comparison = CompareChannels(rxBucket[pc], pv)) >= 0) {
                            if (comparison == 0) {
                                swap(pc, pd);
                                pd--;
                            }
                            pc--;
                        }
                        if (pb > pc) {
                            break;
                        }
                        swap(pb, pc);
                        pb++;
                    }
                    int pn = base + n;
                    int s = Math.min(pa - base, pb - pa);
                    if (s > 0) {
                        for (int i = 0; i < s; i++) {
                            swap(base + i, pb - s + i);
                        }
                    }
                    s = Math.min(pd - pc, pn - pd - 1);
                    if (s > 0) {
                        for (int i = 0; i < s; i++) {
                            swap(pb + i, pn - s + i);
                        }
                    }

                    int r = pb - pa;
                    s = pd - pc;
                    if (s >= r) {
                        base_stack[sp] = pn - s;
                        n_stack[sp] = s;
                        n = r;
                    } else {
                        if (r <= 1) {
                            break;
                        }
                        base_stack[sp] = base;
                        n_stack[sp] = r;
                        base = pn - s;
                        n = s;
                    }
                    ++sp;
                }
            }
            if (sp-- == 0) {
                break;
            }
            base = base_stack[sp];
            n = n_stack[sp];
        }
    }

    private static void swap(int i, int j) {
        RXBUCKET temp = rxBucket[i];
        rxBucket[i] = rxBucket[j];
        rxBucket[j] = temp;
    }

    private static int med3(int a, int b, int c) {
//		return CompareChannels(rxBucket[a], rxBucket[b]) < 0 ?
//		       (CompareChannels(rxBucket[b], rxBucket[c]) < 0 ? b : (CompareChannels(rxBucket[a], rxBucket[c]) < 0 ? c : a ))
//	              :(CompareChannels(rxBucket[b], rxBucket[c]) > 0 ? b : (CompareChannels(rxBucket[a], rxBucket[c]) < 0 ? a : c ));

        if (CompareChannels(rxBucket[a], rxBucket[b]) > 0) {
            if (CompareChannels(rxBucket[a], rxBucket[c]) > 0) {
                if (CompareChannels(rxBucket[b], rxBucket[c]) > 0) {
                    return (b);
                }
                return (c);
            }
            return (a);
        }

        if (CompareChannels(rxBucket[a], rxBucket[c]) >= 0) {
            return (a);
        }

        if (CompareChannels(rxBucket[b], rxBucket[c]) > 0) {
            return (c);
        }

        return (b);
    }
}

class RXBUCKET implements Comparable<RXBUCKET> {
    int index = 0;    //: 13; 	// object array index (sprite[], sector[], wall[])
    int type = 0;    //: 3;	// 0=sprite, 1=sector, 2=wall

    public RXBUCKET() {
    }

    public RXBUCKET(int index, int type) { //for debug
        this.index = index;
        this.type = type;
    }

    public void flush() {
        index = 0;
        type = 0;
    }

    @Override
    public int compareTo(RXBUCKET refl) {
        return EVENT.CompareChannels(this, refl);
    }
}


package ru.m210projects.Blood.Factory;

import ru.m210projects.Build.Board;
import ru.m210projects.Build.Types.BuildPos;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;

import java.util.List;

public class BloodBoard extends Board {

    private int skyBits;
    private int visibility;
    private int songId;
    private int parallaxType;
    private int mapRev;
    private int secrets;
    private long mapCRC;
    private short[] skyOff;

    public BloodBoard(BuildPos pos, Sector[] sectors, Wall[] walls, List<Sprite> sprites) {
        super(pos, sectors, walls, sprites);
    }

    @Override
    public BloodSprite getSprite(int index) {
        return (BloodSprite) super.getSprite(index);
    }

    public short[] getSkyOffset() {
        return skyOff;
    }

    public void setSkyOffset(short[] pskyoff) {
        this.skyOff = pskyoff;
    }

    public int getSkyBits() {
        return skyBits;
    }

    public void setSkyBits(int skyBits) {
        this.skyBits = skyBits;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public int getSongId() {
        return songId;
    }

    public void setSongId(int songId) {
        this.songId = songId;
    }

    public int getParallaxType() {
        return parallaxType;
    }

    public void setParallaxType(int parallaxType) {
        this.parallaxType = parallaxType;
    }

    public int getMapRev() {
        return mapRev;
    }

    public void setMapRev(int mapRev) {
        this.mapRev = mapRev;
    }

    public int getSecrets() {
        return secrets;
    }

    public void setSecrets(int secrets) {
        this.secrets = secrets;
    }

    public long getMapCRC() {
        return mapCRC;
    }

    public void setMapCRC(long mapCRC) {
        this.mapCRC = mapCRC;
    }
}

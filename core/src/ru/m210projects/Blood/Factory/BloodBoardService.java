package ru.m210projects.Blood.Factory;

import ru.m210projects.Blood.DB;
import ru.m210projects.Blood.LEVELS;
import ru.m210projects.Blood.Types.BloodSpriteMap;
import ru.m210projects.Blood.Types.XSECTOR;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Blood.Types.XWALL;
import ru.m210projects.Blood.filehandlers.ChecksumInputStream;
import ru.m210projects.Blood.filehandlers.CryptedInputStream;
import ru.m210projects.Build.Board;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.kChannelSecret;
import static ru.m210projects.Blood.EVENT.kCommandNumbered;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Mirror.*;
import static ru.m210projects.Build.Engine.*;

public class BloodBoardService extends BoardService {

    public static final String kBloodMapExt = "MAP";
    public static final String kBloodMapSig = "BLM\u001A";
    public static final int kBloodMapVersion6 = 0x0600;
    public static final int kBloodMapVersion7 = 0x0700;
    public static final int kMajorVersionMask = 0xFF00;
    public static final int kMinorVersionMask = 0x00FF;
    private static final byte[] crypt = {'M', 'a', 't', 't'};
    private static final int key = LittleEndian.getInt(crypt);

    private BloodXSpriteMap xSpriteMap = new BloodXSpriteMap(new ArrayList<>());

    @Override
    protected Board loadBoard(Entry entry) throws IOException {
        ChecksumInputStream is = new ChecksumInputStream(entry.getInputStream());
        dbInit();

        String signature = StreamUtils.readString(is, 4);
        if (!signature.equals(kBloodMapSig)) {
            throw new WarningException("Wrong signature! Perhaps not a Blood map or map file corrupted. Map signature: \"" + signature + "\" != " + kBloodMapSig);
        }

        boolean crypted = false;
        int version = StreamUtils.readShort(is);
        if ((version & kMajorVersionMask) != kBloodMapVersion6) {
            if ((version & kMajorVersionMask) == kBloodMapVersion7) {
                crypted = true;
            } else {
                throw new WarningException("Map file is wrong version 0x" + Integer.toHexString(version & kMajorVersionMask));
            }
        }

        CryptedInputStream cis = new CryptedInputStream(is, key, 37);
        BuildPos startPos = new BuildPos(StreamUtils.readInt(cis),
                StreamUtils.readInt(cis),
                StreamUtils.readInt(cis),
                StreamUtils.readShort(cis),
                StreamUtils.readShort(cis));

        int pskybits = StreamUtils.readShort(cis);
        int gVisibility = StreamUtils.readInt(cis);
        int gSongId = StreamUtils.readInt(cis);
        int parallaxtype = StreamUtils.readByte(cis);
        int gMapRev = StreamUtils.readInt(cis);
        int numsectors = StreamUtils.readShort(cis);
        int numwalls = StreamUtils.readShort(cis);
        int numSprites = StreamUtils.readShort(cis);
        int secrets = 0;

        if (numsectors >= MAXSECTORSV7 || numwalls >= MAXWALLSV7  || numSprites >= MAXSPRITESV7) {
            Console.out.println("Map limits of sectors, walls or sprites are overflow. Map version is wrong!", OsdColor.RED);
        }

        if (numsectors == 0 || numwalls == 0) {
            throw new WarningException("Empty map! (numsectors == 0 || numwalls == 0)\r\nor map file is corrupt");
        }

        int spr = 44;
        int xspr = 56, xwal = 24, xsec = 60;
        if (crypted) {
            cis = new CryptedInputStream(is, numwalls, 128);
            String copyright = StreamUtils.readString(cis, 64);
            xspr = StreamUtils.readInt(cis);
            xwal = StreamUtils.readInt(cis);
            xsec = StreamUtils.readInt(cis);
            StreamUtils.skip(cis, 52);
        } else {
            StreamUtils.skip(is, 128); //FIXME: check it
        }

        int gSkyCount = 1 << pskybits;
        short[] pskyoff = new short[gSkyCount];
        int length = pskyoff.length * 2;
        ByteBuffer.wrap(StreamUtils.readBytes(new CryptedInputStream(is, length, length), length)).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(pskyoff);

        long dec = (long) gMapRev * Sector.sizeof;
        MirrorSector = numsectors;
        Sector[] sectors = new Sector[numsectors + 1];
        for (int i = 0; i < sectors.length; i++) {
            Sector sec = new Sector();

            if (i < numsectors) {
                cis = new CryptedInputStream(is, dec, Sector.sizeof);
                sec.setWallptr(StreamUtils.readShort(cis));
                sec.setWallnum(StreamUtils.readShort(cis));
                sec.setCeilingz(StreamUtils.readInt(cis));
                sec.setFloorz(StreamUtils.readInt(cis));
                sec.setCeilingstat(StreamUtils.readShort(cis));
                sec.setFloorstat(StreamUtils.readShort(cis));
                sec.setCeilingpicnum(StreamUtils.readShort(cis));
                sec.setCeilingheinum(StreamUtils.readShort(cis));
                sec.setCeilingshade(StreamUtils.readByte(cis));
                sec.setCeilingpal(StreamUtils.readByte(cis));
                sec.setCeilingxpanning(StreamUtils.readByte(cis));
                sec.setCeilingypanning(StreamUtils.readByte(cis));
                sec.setFloorpicnum(StreamUtils.readShort(cis));
                sec.setFloorheinum(StreamUtils.readShort(cis));
                sec.setFloorshade(StreamUtils.readByte(cis));
                sec.setFloorpal(StreamUtils.readByte(cis));
                sec.setFloorxpanning(StreamUtils.readByte(cis));
                sec.setFloorypanning(StreamUtils.readByte(cis));
                sec.setVisibility(StreamUtils.readByte(cis));
                sec.setFiller(StreamUtils.readByte(cis));
                sec.setLotag(StreamUtils.readShort(cis));
                sec.setHitag(StreamUtils.readShort(cis));
                sec.setExtra(StreamUtils.readShort(cis));

                if (sec.getExtra() > 0) {
                    XSECTOR pXSector = xsector[dbInsertXSector(sec, i)]; // XXX
                    pXSector.init(StreamUtils.readBytes(cis, xsec));
                    pXSector.reference = i;
                    pXSector.busy = pXSector.state << 16;

                    if (pXSector.txID == kChannelSecret && pXSector.command == kCommandNumbered) {
                        secrets++;
                    }
                }
            } else { // Mirror sector
                sec.setCeilingpicnum(MIRROR);
                sec.setFloorpicnum(MIRROR);
                sec.setWallnum(4);
                sec.setWallptr(numwalls);
            }

            sectors[i] = sec;
        }

        dec |= key;
        Wall[] walls = new Wall[numwalls + 4];
        for (int i = 0; i < walls.length; i++) {
            Wall wal = new Wall();

            if (i < numwalls) {
                cis = new CryptedInputStream(is, dec, 32);
                wal.setX(StreamUtils.readInt(cis));
                wal.setY(StreamUtils.readInt(cis));
                wal.setPoint2(StreamUtils.readShort(cis));
                wal.setNextwall(StreamUtils.readShort(cis));
                wal.setNextsector(StreamUtils.readShort(cis));
                wal.setCstat(StreamUtils.readShort(cis));
                wal.setPicnum(StreamUtils.readShort(cis));
                wal.setOverpicnum(StreamUtils.readShort(cis));
                wal.setShade(StreamUtils.readByte(cis));
                wal.setPal(StreamUtils.readByte(cis));
                wal.setXrepeat(StreamUtils.readByte(cis));
                wal.setYrepeat(StreamUtils.readByte(cis));
                wal.setXpanning(StreamUtils.readByte(cis));
                wal.setYpanning(StreamUtils.readByte(cis));
                wal.setLotag(StreamUtils.readShort(cis));
                wal.setHitag(StreamUtils.readShort(cis));
                wal.setExtra(StreamUtils.readShort(cis));

                if (wal.getExtra() > 0) {
                    XWALL pXWall = xwall[dbInsertXWall(wal, i)]; // XXX
                    pXWall.init(StreamUtils.readBytes(cis, xwal));
                    pXWall.reference = i;
                    pXWall.busy = pXWall.state << 16;

                    if (pXWall.txID == kChannelSecret && pXWall.command == kCommandNumbered) {
                        secrets++;
                    }
                }
            } else { // Mirror walls
                MirrorWall[i - numwalls] = i;
                wal.setPicnum(MIRROR);
                wal.setOverpicnum(MIRROR);
                wal.setCstat(0);
                wal.setNextsector(-1);
                wal.setNextwall(-1);
                if (i < numwalls + 3) {
                    wal.setPoint2(i + 1);
                } else {
                    wal.setPoint2(numwalls);
                }
            }

            walls[i] = wal;
        }

        dec = (long) gMapRev * spr | key;
        List<Sprite> sprites = new ArrayList<>(numSprites * 2);
        for (int i = 0; i < numSprites; i++) {
            cis = new CryptedInputStream(is, dec, spr);

            BloodSprite s = new BloodSprite();
            s.setX(StreamUtils.readInt(cis));
            s.setY(StreamUtils.readInt(cis));
            s.setZ(StreamUtils.readInt(cis));
            s.setCstat(StreamUtils.readShort(cis));
            s.setPicnum(StreamUtils.readShort(cis));
            s.setShade(StreamUtils.readByte(cis));
            s.setPal(StreamUtils.readByte(cis));
            s.setClipdist(StreamUtils.readByte(cis));
            s.setDetail(StreamUtils.readByte(cis));
            s.setXrepeat(StreamUtils.readByte(cis));
            s.setYrepeat(StreamUtils.readByte(cis));
            s.setXoffset(StreamUtils.readByte(cis));
            s.setYoffset(StreamUtils.readByte(cis));
            s.setSectnum(StreamUtils.readShort(cis));
            s.setStatnum(StreamUtils.readShort(cis));
            s.setAng(StreamUtils.readShort(cis));
            s.setOwner(StreamUtils.readShort(cis));
            s.setXvel(StreamUtils.readShort(cis));
            s.setYvel(StreamUtils.readShort(cis));
            s.setZvel(StreamUtils.readShort(cis));
            s.setLotag(StreamUtils.readShort(cis));
            s.setHitag(StreamUtils.readShort(cis));
            s.setExtra(StreamUtils.readShort(cis));

            if (s.getExtra() > 0) {
                XSPRITE pXSprite = getXSprite(insertXSprite(s)); // XXX
                pXSprite.init(StreamUtils.readBytes(cis, xspr));
                pXSprite.setReference(i);
                pXSprite.setBusy(pXSprite.getState() << 16);

                if (!crypted) {
//			        v47 = (4 * *&getXSprite(v42).dropMsg) >> 31;
//			        getXSprite(v42).restState_interruptable_difficulty &= 0x7Fu;
//			        *&getXSprite(v42).busyTime |= v47 << 31;
                }

                if (pXSprite.getTxID() == kChannelSecret && pXSprite.getCommand() == kCommandNumbered) {
                    if (((1 << pGameInfo.nEnemyQuantity) & pXSprite.getLSkill()) == 0
                            && (!pXSprite.islS() && pGameInfo.nGameType == 0
                            || !pXSprite.islB() && pGameInfo.nGameType == 2
                            || !pXSprite.islT() && pGameInfo.nGameType == 3
                            || !pXSprite.islC() && pGameInfo.nGameType == 1)) {
                        secrets++;
                    }
                }

                s.setXSprite(pXSprite);
            }

            s.setXvel(i);
            if ((s.getCstat() & kSpriteRMask) == kSpriteRMask) {
                Console.out.println("ERROR: Sprite " + i + " has an invalid rotate", OsdColor.YELLOW);
                s.setCstat(s.getCstat() & ~kSpriteRMask);
            }

            sprites.add(s);
        }

        long uMapCRC = is.getChecksum();
        if (uMapCRC != (StreamUtils.readInt(is) & 0xFFFFFFFFL)) {
            throw new WarningException("File does not match CRC");
        }

        BloodBoard board = new BloodBoard(startPos, sectors, walls, sprites);
        board.setSecrets(secrets);
        board.setVisibility(gVisibility);
        board.setSkyBits(pskybits);
        board.setSongId(gSongId);
        board.setMapCRC(uMapCRC);
        board.setMapRev(gMapRev);
        board.setParallaxType(parallaxtype);
        board.setSkyOffset(pskyoff);

        return board;
    }

    @Override
    public void setBoard(Board board) {
        this.board = board;
        initSpriteLists(board);

        List<Sprite> sprites = board.getSprites();
        for (int i = 0; i < sprites.size(); i++) {
            Sprite spr = sprites.get(i);
            spriteStatMap.set(i, spr.getStatnum());
            spriteSectMap.set(i, spr.getSectnum());
        }
    }

    @Override
    public BloodBoard prepareBoard(Board b) {
        BloodBoard board = (BloodBoard) b;
        gSkyCount = 1 << board.getSkyBits();
        Arrays.fill(zeropskyoff, (short) 0);
        short[] boardSkyOff = board.getSkyOffset();
        System.arraycopy(boardSkyOff, 0, zeropskyoff, 0, Math.min(boardSkyOff.length, zeropskyoff.length));
        System.arraycopy(boardSkyOff, 0, pskyoff, 0, Math.min(boardSkyOff.length, pskyoff.length));

        pGameInfo.uMapCRC = board.getMapCRC();
        LEVELS.autoTotalSecrets = board.getSecrets();
        Engine.visibility = board.getVisibility();
        DB.gVisibility = Engine.visibility;
        Engine.pskybits = (short) board.getSkyBits();
        Engine.parallaxtype = (byte) board.getParallaxType();
        this.board = board;

        initSpriteLists(board);
        List<Sprite> sprites = board.getSprites();
        for (int i = 0; i < sprites.size(); i++) {
            Sprite spr = sprites.get(i);

            InsertSpriteSect(i, spr.getSectnum());
            InsertSpriteStat(i, spr.getStatnum());
        }

        PropagateMarkerReferences(board);

        return board;
    }

    public ListNode<XSPRITE> getXSpriteNode() {
        return xSpriteMap.getFirst(0);
    }

    private void PropagateMarkerReferences(Board board) {
        int nXSector;
        Sector pSector;

        for (ListNode<Sprite> node = spriteStatMap.getFirst(kStatMarker); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            Sprite pSprite = node.get();

            switch (pSprite.getLotag()) {
                case kMarkerWarpDest:
                case kMarkerAxis:
                case kMarkerOff:
                    pSector = board.getSector(pSprite.getOwner());
                    if (pSector == null) {
                        break;
                    }

                    nXSector = pSector.getExtra();
                    if (nXSector <= 0 || nXSector >= kMaxXSectors)
                        break;

                    xsector[nXSector].marker0 = nSprite;
                    continue;
                case kMarkerOn:
                    pSector = board.getSector(pSprite.getOwner());
                    if (pSector == null) {
                        break;
                    }

                    nXSector = pSector.getExtra();
                    if (nXSector <= 0 || nXSector >= kMaxXSectors)
                        break;

                    xsector[nXSector].marker1 = nSprite;
                    continue;
            }

            System.out.println("Deleting invalid marker sprite");
            deletesprite(nSprite);
        }
    }

    @Override
    public int getSectorCount() {
        return board.getSectorCount() - 1;
    }

    @Override
    public int getWallCount() {
        return board.getWallCount() - 4;
    }

    public int getXSpriteCount() {
        return xSpriteMap.getSize();
    }

    @Override
    public boolean deletesprite(int nSprite) {
        Sprite pSprite = board.getSprite(nSprite);
        if (pSprite == null || pSprite.getStatnum() == kStatFree && pSprite.getSectnum() == -1) {
            return false; //already deleted
        }

        if (pSprite.getExtra() >= 0) {
            deleteXSprite(pSprite);
        }

        if (!(pSprite.getStatnum() >= 0 && pSprite.getStatnum() < kMaxStatus)) {
            Console.out.println("deletesprite() statnum throw new AssertException: " + pSprite, OsdColor.RED);
            throw new AssertException("pSprite.statnum >= 0 && pSprite.statnum < kMaxStatus");
        }
//        RemoveSpriteStat(nSprite);
//
//        if (!isValidSector(pSprite.getSectnum()))
//            throw new AssertException("isValidSector(pSprite.getSectnum())");
//
//        RemoveSpriteSect(nSprite);

        InsertSpriteSect(nSprite, -1);
        InsertSpriteStat(nSprite, kStatFree);
        pSprite.setSectnum(-1);

        return true;
    }

    @Override
    public int insertsprite(int nSector, int nStatus) {
        int nSprite = spriteStatMap.insert(nStatus);
        if (nSprite >= 0) {
            BloodSprite pSprite = getSprite(nSprite);
            pSprite.reset((byte) 0);
            InsertSpriteStat(nSprite, nStatus);
            InsertSpriteSect(nSprite, nSector);

            pSprite.setCstat(kSpriteOriginAlign);
            pSprite.setClipdist(32);
            pSprite.setXrepeat(64);
            pSprite.setYrepeat(64);
            pSprite.setOwner(-1);
            pSprite.setExtra(-1);
            pSprite.setXSprite(null);
            pSprite.setXvel((short) nSprite);
            pSprite.setVelocity(0, 0, 0);
        }

        return nSprite;
    }

    @Override
    public boolean setSprite(int spritenum, int newx, int newy, int newz, boolean checkZ) {
        Sprite pSprite = board.getSprite(spritenum);
        if (pSprite == null) {
            return false;
        }

        pSprite.setX(newx);
        pSprite.setY(newy);
        pSprite.setZ(newz);

        int tempsectnum = updatesector(newx, newy, pSprite.getSectnum());
        if (tempsectnum < 0) {
            return false;
        }

        if (tempsectnum != pSprite.getSectnum()) {
            changespritesect(spritenum, tempsectnum);
        }

        return true;
    }

    @Override
    public boolean changespritestat(int nSprite, int nStatus) {
        if (!isValidSprite(nSprite)) throw new AssertException("isValidSprite(nSprite)");
        if (!(nStatus >= 0 && nStatus <= kMaxStatus)) throw new AssertException("nStatus >= 0 && nStatus <= kMaxStatus");

        Sprite pSprite = board.getSprite(nSprite);
        if (pSprite == null) {
            return false;
        }

        if (!(pSprite.getStatnum() >= 0 && pSprite.getStatnum() < kMaxStatus))
            throw new AssertException("boardService.getSprite(nSprite).statnum >= 0 && boardService.getSprite(nSprite).statnum < kMaxStatus");
        if (!isValidSector(pSprite.getSectnum()))
            throw new AssertException("isValidSector(pSprite.getSectnum())");

        // changing to same status is a valid operation, and will put sprite at tail of list
//        RemoveSpriteStat(nSprite);
        InsertSpriteStat(nSprite, nStatus);
        return true;
    }


    private void RemoveSpriteSect(int nSprite) {
        if (!isValidSprite(nSprite)) throw new AssertException("isValidSprite(nSprite)");
        spriteSectMap.remove(nSprite);
    }

    private void RemoveSpriteStat(int nSprite) {
        if (!isValidSprite(nSprite)) throw new AssertException("isValidSprite(nSprite)");
//        int nStat = board.getSprite(nSprite).getStatnum();
//        if (!(nStat >= 0 && nStat <= kMaxStatus)) throw new AssertException("nStat >= 0 && nStat <= kMaxStatus");

        spriteStatMap.remove(nSprite);
//        nStatSize[nStat]--;
    }

    private void InsertSpriteSect(int nSprite, int nSector) {
        spriteSectMap.set(nSprite, nSector);
    }

    private void InsertSpriteStat(int nSprite, int nStat) {
        spriteStatMap.set(nSprite, nStat);
//        nStatSize[nStat]++;
    }

    public void dbInit() {
        xSpriteMap = new BloodXSpriteMap(new ArrayList<>());

        for (int i = 0; i < kMaxXWalls; i++) {
            if (xwall[i] == null) xwall[i] = new XWALL();
            else xwall[i].free();
        }

        for (int i = 0; i < kMaxXSectors; i++) {
            if (xsector[i] == null) xsector[i] = new XSECTOR();
            else xsector[i].free();
        }

        InitFreeList(nextXWall, kMaxXWalls);
        for (int i = 1; i < kMaxXWalls; i++)
            xwall[i].reference = -1;

        InitFreeList(nextXSector, kMaxXSectors);
        for (int i = 1; i < kMaxXSectors; i++)
            xsector[i].reference = -1;
    }

    @Override
    protected void initSpriteLists(Board board) {
        List<Sprite> sprites = board.getSprites();
        this.spriteStatMap = new BloodSpriteMap(kMaxStatus, sprites, Sprite::setStatnum) {
            @Override
            protected void setValue(ListNode<Sprite> node, int value) {
                super.setValue(node, (value == -1) ? poolIndex : value);
            }
        };
        this.spriteSectMap = new BloodSpriteMap(MAXSECTORS, sprites, Sprite::setSectnum);
    }

    private int dbInsertXWall(Wall wall, int nWall) {
        int nXWall = RemoveFree(nextXWall);
        if (nXWall == 0) System.err.println("Out of free XWalls");

        xwall[nXWall].free();
        wall.setExtra((short) nXWall);
        xwall[nXWall].reference = nWall;

        return nXWall;
    }

    private void dbDeleteXWall(int nXWall) {
        if (xwall[nXWall].reference < 0) throw new AssertException("xwall[nXWall].reference >= 0");
        InsertFree(nextXWall, nXWall);

        // clear the references
        getWall(xwall[nXWall].reference).setExtra(-1);
        xwall[nXWall].reference = -1;
    }

    public int getStatSize(int nStat) {
        return spriteStatMap.get(nStat).getSize();
    }

    /**
     * Initializes {@link XSPRITE} structure in xsprite array and links
     * it to a {@link Sprite} with index {@code nSprite} in
     *
     * @return index of {@link XSPRITE} freshly allocated in xsprite
     */
    public int insertXSprite(Sprite pSprite) {
        return xSpriteMap.insert(pSprite);
    }

    private void deleteXSprite(Sprite pSprite) {
        int nXSprite = pSprite.getExtra();
        if (getXSprite(nXSprite).getReference() < 0) {
            throw new AssertException("getXSprite(nXSprite).reference >= 0");
        }
        xSpriteMap.remove(nXSprite);
    }

    /**
     * @param nXSprite index that should be inserted
     * @return {@link XSPRITE} freshly allocated in xsprite
     */
    public XSPRITE setXSprite(int nXSprite) {
        xSpriteMap.insert(nXSprite);
        XSPRITE pXSprite = getXSprite(nXSprite);
        pXSprite.free();
        return pXSprite;
    }

    /**
     * Array of all loaded XSPRITES. {@link Sprite} can access its
     * {@link XSPRITE} by using {@link Sprite#getExtra()} as an index to this array;
     */
    public XSPRITE getXSprite(int nXSprite) {
        if (nXSprite == -1) {
            return null;
        }

        return xSpriteMap.getXSprite(nXSprite);
    }

    @Override
    public BloodSprite getSprite(int index) {
        return (BloodSprite) super.getSprite(index);
    }
}

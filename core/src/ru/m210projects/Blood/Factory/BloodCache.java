package ru.m210projects.Blood.Factory;

import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.*;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.rff.RffFile;

import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.*;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;

public class BloodCache extends Cache {

    public BloodCache(Directory gameDirectory) {
        super(gameDirectory);
    }

    @Override
    public boolean addGroup(Entry groupEntry, CacheResourceMap.CachePriority priority) {
        Group group = newGroup(groupEntry);
        boolean result = addGroup(group, priority);
        if (!result && groupEntry.getName().equalsIgnoreCase("blood.rff")) {
            // Handle the Blood Alpha version error
            try {
                result = addGroup(new RffFile(groupEntry.getName(), groupEntry::getInputStream), priority);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage());
            }
        }
        return result;
    }

    public Entry getEntry(int fileid, String type) {
        for (int i = HIGHEST.getLevel(); i >= NORMAL.getLevel(); i--) {
            for (ListNode<Group> node = cacheResourceMap.getFirst(i); node != null; node = node.getNext()) {
                Group group = node.get();
                if (group instanceof RffFile) {
                    Entry entry = ((RffFile) group).getEntry(fileid, type);
                    if (entry.exists()) {
                        return entry;
                    }
                }
            }
        }
        return DUMMY_ENTRY;
    }

    public boolean contains(int fileid, String type) {
        return getEntry(fileid, type).exists();
    }

    public boolean contains(String name, boolean searchFirst) {
        return getEntry(name, searchFirst).exists();
    }
}

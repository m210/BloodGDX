package ru.m210projects.Blood.Factory;

import ru.m210projects.Build.Render.DummyRenderer;

public class BloodDummyRenderer extends DummyRenderer implements BloodRenderer {

    @Override
    public void setaspect(int daxrange, int daaspect) {
    }

    @Override
    public void setdrunk(float intensive) {
    }

    @Override
    public void setRestorePalette() {

    }

    @Override
    public boolean isRestorePalette() {
        return false;
    }

    @Override
    public void scrSetDac() {

    }

    @Override
    public int getCurrentShade() {
        return 0;
    }
}

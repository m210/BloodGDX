// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.ClipMover;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Timer;
import ru.m210projects.Build.Types.TileManager;
import ru.m210projects.Build.osd.CommandResponse;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.commands.OsdCommand;

import static ru.m210projects.Blood.Globals.kFrameTicks;
import static ru.m210projects.Blood.Globals.kTimerRate;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;

public class BloodEngine extends Engine {

    public BloodEngine(BuildGame game) throws Exception {
        super(game);

        if (game.getCache().getGroup("blood.rff").isEmpty() || game.getCache().getGroup("sounds.rff").isEmpty())
            throw new Exception("Looks like you have tried to launch unsupported version of Blood");

        clipmove = new ClipMover(this);
        inittimer(game.pCfg.isLegacyTimer(), kTimerRate, kFrameTicks);

        Console.out.registerCommand(new OsdCommand("fastdemo", "fastdemo \"demo speed\"") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (argv.length != 1) {
                    return CommandResponse.DESCRIPTION_RESPONSE;
                }

                try {
                    getTimer().setSkipTicks(Math.max(0, Integer.parseInt(argv[0])));
                } catch (Exception e) {
                    return CommandResponse.BAD_ARGUMENT_RESPONSE;
                }
                return CommandResponse.OK_RESPONSE;
            }
        });
    }

    @Override
    protected TileManager loadTileManager() {
        return new BloodTileManager();
    }

    @Override
    protected BoardService createBoardService() {
        return new BloodBoardService();
    }

    @Override
    public BloodPaletteManager loadpalette() throws Exception {
        return new BloodPaletteManager(this, false);
    }

    @Override
    public BloodPaletteManager getPaletteManager() {
        return (BloodPaletteManager) paletteManager;
    }

    // Forcing original Blood bug
    @Override
    public int clipmove(int x, int y, int z, int sectnum, long xvect, long yvect, int walldist, int ceildist,
                        int flordist, int cliptype) {

        clipmove.setTraceNum(clipmoveboxtracenum);
        clipmove.setNeedUpdateSector(!IsOriginalDemo());
        return super.clipmove(x, y, z, sectnum, xvect, yvect, walldist, ceildist, flordist, cliptype);
    }
}

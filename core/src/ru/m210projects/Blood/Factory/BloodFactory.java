// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Factory;

import ru.m210projects.Blood.Fonts.BloodFont;
import ru.m210projects.Blood.Fonts.QFNFont;
import ru.m210projects.Blood.Main;
import ru.m210projects.Blood.filehandlers.scripts.BloodDef;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.FontHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.SliderDrawable;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Renderer.RenderType;
import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.OsdFunc;

import static ru.m210projects.Blood.Main.game;
import static ru.m210projects.Blood.VERSION.hasQFN;
import static ru.m210projects.Blood.VERSION.versionInit;

public class BloodFactory extends BuildFactory {

    private final Main app;

    public BloodFactory(Main app) {
        super("BLOOD.RFF", "SOUNDS.RFF");
        this.app = app;

        OsdColor.DEFAULT.setPal(0);
    }

    @Override
    public void drawInitScreen() {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(0, 0, 65536, 0, 2046, -128, 0, 10 | 16);
    }

    @Override
    public Engine engine() throws Exception {
        versionInit();
        return Main.engine = new BloodEngine(app);
    }

    @Override
    public Renderer renderer(RenderType type) {
        if (type == RenderType.Software)
            return new BloodSoftware(app.pCfg);
        if (type == RenderType.PolyGDX)
            return new BloodPolygdx(app.pCfg);
        else
            return new BloodPolymost(app.pCfg);
    }

    @Override
    public DefScript getBaseDef(Engine engine) {
        return new BloodDef(engine);
    }

    @Override
    public OsdFunc getOsdFunc() {
        return new BloodOsdFunc();
    }

    @Override
    public MenuHandler menus() {
        return app.menu = new BloodMenuHandler(app);
    }

    @Override
    public FontHandler fonts() {
        return new FontHandler(6) {
            @Override
            protected Font init(int i) {
                if (i == 0)
                    return hasQFN ? new QFNFont(4096, 0) : new BloodFont(4096, 0);
                else if (i == 1)
                    return hasQFN ? new QFNFont(4192, 1) : new BloodFont(4192, 1);
                else if (i == 2)
                    return hasQFN ? new QFNFont(4288, 2) : new BloodFont(4288, 1);
                else if (i == 3)
                    return hasQFN ? new QFNFont(4384, 3) : new BloodFont(4384, 1);
                else if (i == 4)
                    return hasQFN ? new QFNFont(4480, 4) : new BloodFont(4480, 1);
                else if (i == 5) {
                    Font console = hasQFN ? new QFNFont(4384, 3) : new BloodFont(4384, 1);
                    console.setVerticalScaled(false);
                    return console;
                }
                return EngineUtils.getLargeFont();
            }
        };
    }

    @Override
    public BuildNet net() {
        return new BloodNetwork(app);
    }

    @Override
    public SliderDrawable slider() {
        return new BLSliderDrawable();
    }
}

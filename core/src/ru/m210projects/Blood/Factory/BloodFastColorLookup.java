package ru.m210projects.Blood.Factory;

import ru.m210projects.Build.Types.FastColorLookup;
import ru.m210projects.Build.osd.Console;

import java.util.Arrays;

public class BloodFastColorLookup implements FastColorLookup {

    // Weight coefficients for color matching
    private final int kWeightR;
    private final int kWeightG;
    private final int kWeightB;
    protected Byte[] palcache = new Byte[0x40000]; // buffer 256kb

    private byte[] gStdColor = new byte[32]; // standard 32 colors
    private int[][] StdPal = {{0, 0, 0}, {0, 0, 170}, {0, 170, 0}, {0, 170, 170}, {170, 0, 0}, {170, 0, 170}, {170, 85, 0}, {170, 170, 170}, {85, 85, 85}, {85, 85, 255}, {85, 255, 85}, {85, 255, 255}, {255, 85, 85}, {255, 85, 255}, {255, 255, 85}, {255, 255, 255}, {241, 241, 241}, {226, 226, 226}, {211, 211, 211}, {196, 196, 196}, {181, 181, 181}, {166, 166, 166}, {151, 151, 151}, {136, 136, 136}, {120, 120, 120}, {105, 105, 105}, {90, 90, 90}, {75, 75, 75}, {60, 60, 60}, {45, 45, 45}, {30, 30, 30}, {15, 15, 15},};

    public BloodFastColorLookup(BloodPaletteManager pm, int rscale, int gscale, int bscale) {
        this.kWeightR = rscale;
        this.kWeightG = gscale;
        this.kWeightB = bscale;

        Console.out.println("Creating standard color lookups");
        for (int i = 0; i < 32; i++) {
            gStdColor[i] = findClosestColor(pm.getBasePalette(), StdPal[i][0], StdPal[i][1], StdPal[i][2]);
        }
    }

    @Override
    public void invalidate() {
        Arrays.fill(palcache, null);
    }

    @Override
    public byte getClosestColorIndex(byte[] palette, int r, int g, int b) {
        int rgb = ((r << 12) | (g << 6) | b);
        Byte out = palcache[rgb & (palcache.length - 1)];
        if (out != null) return out;

        byte col = findClosestColor(palette, r << 2, g << 2, b << 2);
        palcache[rgb & (palcache.length - 1)] = col;
        return col;
    }

    private byte findClosestColor(byte[] palette, int r, int g, int b) {
        int matchDist = 0x7FFFFFFF;
        int match = 0;
        for (int i = 0; i < 256; i++) {
            int dist = 0;

            int dg = (palette[3 * i + 1] & 0xFF) - g;
            dist += kWeightG * dg * dg;
            if (dist >= matchDist) {
                continue;
            }

            int dr = (palette[3 * i] & 0xFF) - r;
            dist += kWeightR * dr * dr;
            if (dist >= matchDist) {
                continue;
            }

            int db = (palette[3 * i + 2] & 0xFF) - b;
            dist += kWeightB * db * db;
            if (dist >= matchDist) {
                continue;
            }

            matchDist = dist;
            match = i;

            if (dist == 0) {
                break;
            }
        }

        return (byte) match;
    }
}

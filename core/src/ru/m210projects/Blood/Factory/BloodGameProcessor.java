package ru.m210projects.Blood.Factory;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Blood.Config;
import ru.m210projects.Blood.Main;
import ru.m210projects.Blood.Types.INPUT;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;

import static ru.m210projects.Blood.Gameutils.ClipHigh;
import static ru.m210projects.Blood.Gameutils.ClipRange;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.cfg;
import static ru.m210projects.Blood.PLAYER.kMoveSwim;
import static ru.m210projects.Blood.Screen.scrSetView;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.mulscale;

public class BloodGameProcessor extends GameProcessor {

    private final BloodPrompt bloodMessage;

    private int turnAccel = 0;

    public BloodGameProcessor(Main game) {
        super(game);
        this.bloodMessage = new BloodPrompt();
    }

    public BloodPrompt getBloodMessage() {
        return bloodMessage;
    }

    @Override
    public void fillInput(BuildNet.NetInput input) {
        INPUT gInput = (INPUT) input;

        gInput.reset();

        if (Console.out.isShowing() || game.pMenu.gShowMenu) {
            return;
        }

        if(pGameInfo.nGameType > 0)
        {
            if(isKeyPressed(Input.Keys.ALT_LEFT) || isKeyPressed(Input.Keys.SHIFT_LEFT))
            {
                int fkey = -1;
                for(int i = 0; i < 10; i++)
                    if(isKeyPressed(i + Input.Keys.NUM_1))
                    {
                        fkey = i;
                        break;
                    }

                if(isKeyPressed(Input.Keys.ALT_LEFT) && fkey != -1)
                    Main.game.net.TauntSound(fkey);
                if(isKeyPressed(Input.Keys.SHIFT_LEFT) && fkey != -1)
                    Main.game.net.TauntMessage(fkey);
                return;
            }
        }

        if(isGameKeyJustPressed(Config.BloodKeys.AutoRun)) {
            if(!cfg.gAutoRun) {
                cfg.gAutoRun = true;
                viewSetMessage("Auto run On", gPlayer[gViewIndex].nPlayer);
            } else {
                cfg.gAutoRun = false;
                viewSetMessage("Auto run Off", gPlayer[gViewIndex].nPlayer);
            }
        }

        if(gViewMode == kView2DIcon || gViewMode == kView2D) {
            if(isGameKeyJustPressed(Config.BloodKeys.Map_Follow_Mode))
            {
                gMapScrollMode = !gMapScrollMode;
            }
        }

        if(isGameKeyJustPressed(GameKeys.Map_Toggle))
            scrSetView(gViewMode);
        if(isGameKeyJustPressed(GameKeys.Next_Weapon))
            gInput.NextWeapon = true;
        if(isGameKeyJustPressed(GameKeys.Previous_Weapon))
            gInput.PrevWeapon = true;

        gInput.Jump = isGameKeyPressed(GameKeys.Jump);
        gInput.Crouch = isGameKeyPressed(GameKeys.Crouch);
        gInput.Shoot = isGameKeyPressed(GameKeys.Weapon_Fire);
        gInput.AltShoot = isGameKeyPressed(Config.BloodKeys.Weapon_Special_Fire);
        gInput.Run = isGameKeyPressed(GameKeys.Run);
        gInput.TurnAround = isGameKeyJustPressed(GameKeys.Turn_Around);
        gInput.Use = isGameKeyJustPressed(GameKeys.Open);
        gInput.InventoryLeft = isGameKeyJustPressed(Config.BloodKeys.Inventory_Left);
        gInput.InventoryRight = isGameKeyJustPressed(Config.BloodKeys.Inventory_Right);
        gInput.InventoryUse = isGameKeyJustPressed(Config.BloodKeys.Inventory_Use);
        gInput.HolsterWeapon = isGameKeyJustPressed(Config.BloodKeys.Holster_Weapon);
        if (gMe.moveState != kMoveSwim) {
            gInput.CrouchMode = isGameKeyJustPressed(Config.BloodKeys.Crouch_toggle);
        } else {
            gInput.CrouchMode = isGameKeyPressed(Config.BloodKeys.Crouch_toggle);
        }
        gInput.LastWeapon = isGameKeyJustPressed(Config.BloodKeys.Last_Used_Weapon);

        for(int i = 0; i < 10; i++) {
            GameKey gameKey = cfg.getKeymap()[i + cfg.weaponIndex];
            if (isGameKeyJustPressed(gameKey)) {
                gInput.newWeapon = i + 1;
            }
        }

        if(isGameKeyJustPressed(Config.BloodKeys.See_Chase_View))
            gViewPos ^= 1;

        if(isGameKeyJustPressed(Config.BloodKeys.Show_Opponents_Weapon))
            cfg.gShowWeapon = !cfg.gShowWeapon;

        if(isGameKeyJustPressed(Config.BloodKeys.See_Coop_View))
        {
            if(numplayers > 1 && (pGameInfo.nGameType == kNetModeCoop || pGameInfo.nGameType == kNetModeTeams) && !kFakeMultiplayer)
            {
                if(pGameInfo.nGameType == kNetModeCoop) {
                    gViewIndex = connectpoint2[gViewIndex];
                    if ( gViewIndex == -1 )
                        gViewIndex = connecthead;
                }
                else
                {
                    int index = gViewIndex;
                    do
                    {
                        gViewIndex = connectpoint2[gViewIndex];
                        if ( gViewIndex == -1 )
                            gViewIndex = connecthead ;
                        if ( index == gViewIndex )
                            break;
                        if ( gMe.teamID == gPlayer[gViewIndex].teamID )
                            break;
                    }
                    while ( index != gViewIndex );
                }

//				do
//				{
//					gViewIndex++;
//					if (gViewIndex >= gNetPlayers) 
//						gViewIndex = 0;
//					if(pGameInfo.nGameType == kNetModeCoop 
//							|| gPlayer[gViewIndex].teamID == gMe.teamID)
//						break;
//				}
//				while(true);
            }
        }

        gInput.UseBeastVision = isGameKeyJustPressed(Config.BloodKeys.BeastVision);
        gInput.UseCrystalBall = isGameKeyJustPressed(Config.BloodKeys.CrystalBall);
        gInput.UseJumpBoots = isGameKeyJustPressed(Config.BloodKeys.JumpBoots);
        gInput.UseMedKit = isGameKeyJustPressed(Config.BloodKeys.MedKit);

        if(isGameKeyJustPressed(Config.BloodKeys.ProximityBombs))
            gInput.newWeapon = 11;
        if(isGameKeyJustPressed(Config.BloodKeys.RemoteBombs))
            gInput.newWeapon = 12;

        boolean up = isGameKeyPressed(GameKeys.Move_Forward);
        boolean down = isGameKeyPressed(GameKeys.Move_Backward);
        int run = ((!cfg.gAutoRun && gInput.Run) || (!gInput.Run && cfg.gAutoRun)) ? 1:0;

        if ( up )
            gInput.Forward += kFrameTicks * (run + 1);
        if ( down )
            gInput.Forward -= kFrameTicks * (run + 1);

        boolean strafe = isGameKeyPressed(GameKeys.Strafe);
        boolean sleft = isGameKeyPressed(GameKeys.Strafe_Left);
        boolean sright = isGameKeyPressed(GameKeys.Strafe_Right);

        if ( sleft )
            gInput.Strafe += kFrameTicks * (run + 1);
        if ( sright )
            gInput.Strafe -= kFrameTicks * (run + 1);

        boolean left = isGameKeyPressed(GameKeys.Turn_Left);
        boolean right = isGameKeyPressed(GameKeys.Turn_Right);

        if(left || right)
            turnAccel += kFrameTicks;
        else turnAccel = 0;

        if(!strafe) {
            if ( left )
                gInput.Turn -= ClipHigh(turnAccel * 12, cfg.gTurnSpeed);
            if ( right )
                gInput.Turn += ClipHigh(turnAccel * 12, cfg.gTurnSpeed);
            gInput.Turn = BClipRange(gInput.Turn + ctrlGetMouseTurn(), -1024, 1024);
        }
        else
        {
            if ( left )
                gInput.Strafe += kFrameTicks * (run + 1);
            if ( right )
                gInput.Strafe -= kFrameTicks * (run + 1);
            gInput.Strafe = BClipRange(gInput.Strafe - (int) ctrlGetMouseStrafe(), -kFrameTicks * (run + 1), kFrameTicks * (run + 1));
        }

        if ( (gInput.Run || cfg.gAutoRun) && turnAccel > 24 )
            gInput.Turn *= 2;

        if(isGameKeyJustPressed(GameKeys.Mouse_Aiming))
        {
            if(!cfg.isgMouseAim()) {
                cfg.setgMouseAim(true);
                viewSetMessage("Mouse aiming on", gPlayer[gViewIndex].nPlayer);
            } else
            {
                cfg.setgMouseAim(false);
                gInput.LookCenter = true;
                viewSetMessage("Mouse aiming off", gPlayer[gViewIndex].nPlayer);
            }
        }

        if(isGameKeyJustPressed(Config.BloodKeys.Toggle_Crosshair))
        {
            if(!cfg.gCrosshair) {
                cfg.gCrosshair = true;
                viewSetMessage("Crosshair on", gPlayer[gViewIndex].nPlayer);
            } else
            {
                cfg.gCrosshair = false;
                viewSetMessage("Crosshair off", gPlayer[gViewIndex].nPlayer);
            }
        }

        gInput.LookLeft = isGameKeyPressed(Config.BloodKeys.Tilt_Left);
        gInput.LookRight = isGameKeyPressed(Config.BloodKeys.Tilt_Right);

        gInput.Lookup = false;
        gInput.Lookdown = false;
        if(isGameKeyPressed(Config.BloodKeys.Aim_Up))
            gInput.Lookup = true;
        if(isGameKeyPressed(Config.BloodKeys.Aim_Down))
            gInput.Lookdown = true;
        if(isGameKeyPressed(GameKeys.Look_Up))
        {
            gInput.Lookup = true;
            gInput.LookCenter = true;
        }

        if(isGameKeyPressed(GameKeys.Look_Down))
        {
            gInput.Lookdown = true;
            gInput.LookCenter = true;
        }
        if(isGameKeyJustPressed(Config.BloodKeys.Aim_Center))
            gInput.LookCenter = true;

        if(cfg.isgMouseAim() && gViewMode != kView2DIcon) {
            gInput.mlook = BClipRange(ctrlGetMouseLook(!cfg.isgInvertmouse()) / 16.0f, -177, 178);
        } else
            gInput.Forward =  ClipRange(gInput.Forward - (int)ctrlGetMouseMove(), -kFrameTicks * (run + 1), kFrameTicks * (run + 1));


        Vector2 stick1 = ctrlGetStick(JoyStick.LOOKING);
        Vector2 stick2 = ctrlGetStick(JoyStick.MOVING);

        float lookx = stick1.x;
        float looky = stick1.y;

        if (looky != 0) {
            float k = 1.5f;
            gInput.mlook = BClipRange(gInput.mlook - k * looky * cfg.getJoyLookSpeed(), -177, 178);
        }

        if (lookx != 0) {
            float k = 64;
            gInput.Turn = BClipRange(gInput.Turn + k * lookx * cfg.getJoyTurnSpeed(), -1024, 1024);
        }

        if (stick2.y != 0) {
            gInput.Forward = (short) BClipRange(gInput.Forward - (20.0f * stick2.y), -kFrameTicks * (run + 1), kFrameTicks * (run + 1));
        }
        if (stick2.x != 0) {
            gInput.Strafe = (short) BClipRange(gInput.Strafe - (20.0f * stick2.x), -kFrameTicks * (run + 1), kFrameTicks * (run + 1));
        }

        if(isKeyJustPressed(Input.Keys.PAUSE)) {
            gInput.Pause = !gInput.Pause;
        }

        if(isGameKeyPressed(GameKeys.Send_Message)) {
            bloodMessage.setCaptureInput(true);
        }

        if( ( gViewMode == kView2DIcon || gViewMode == kView2D ) && gMapScrollMode) {
            scrollOX = scrollX;
            scrollOY = scrollY;
            scrollOAng = scrollAng;

            if( gInput.Forward != 0 ) {
                scrollX += mulscale(Cos(scrollAng), gInput.Forward, 24);
                scrollY += mulscale(Sin(scrollAng), gInput.Forward, 24);
                gInput.Forward = 0;
            }
            if( gInput.Strafe != 0 ) {
                scrollX += mulscale(Sin(scrollAng), gInput.Strafe, 24);
                scrollY -= mulscale(Cos(scrollAng), gInput.Strafe, 24);
                gInput.Strafe = 0;
            }
            if ( gInput.Turn != 0 ) {
                scrollAng = (short) ((scrollAng + (kFrameTicks * (int) gInput.Turn >> 4)) & kAngleMask);
                gInput.Turn = 0;
            }
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        if (bloodMessage.isCaptured()) {
            if (keycode == Input.Keys.ESCAPE) {
                bloodMessage.setCaptureInput(false);
                bloodMessage.clear();
            } else {
                bloodMessage.keyDown(keycode);
            }
            return true;
        }
        return super.keyDown(keycode);
    }

    @Override
    public boolean keyUp(int i) {
        bloodMessage.keyUp(i);
        return super.keyUp(i);
    }

    @Override
    public boolean keyRepeat(int keycode) {
        if (bloodMessage.isCaptured()) {
            bloodMessage.keyRepeat(keycode);
            return true;
        }
        return super.keyRepeat(keycode);
    }

    @Override
    public boolean keyTyped(char i) {
        if (bloodMessage.isCaptured()) {
            bloodMessage.keyTyped(i);
            return true;
        }
        return super.keyTyped(i);
    }
}

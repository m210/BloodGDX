package ru.m210projects.Blood.Factory;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Pattern.ScreenAdapters.InitScreen;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.DefaultOsdFunc;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Blood.Cheats.IsCheatCode;
import static ru.m210projects.Blood.Cheats.cheatCode;
import static ru.m210projects.Blood.Globals.kNetModeOff;
import static ru.m210projects.Blood.Globals.pGameInfo;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Build.Pragmas.mulscale;

public class BloodOsdFunc extends DefaultOsdFunc {

    public BloodOsdFunc() {
        super(game.getRenderer());
        final int GREEN_PAL = 100;
        final int RED_PAL = 101; //7

        this.BGTILE = 2051;
        this.BORDTILE = 2051;
        this.PALETTE = 5;

        OsdColor.RED.setPal(RED_PAL);
        OsdColor.BLUE.setPal(10);
        OsdColor.YELLOW.setPal(9);
        OsdColor.BROWN.setPal(2);
        OsdColor.GREEN.setPal(GREEN_PAL);

        PaletteManager paletteManager = engine.getPaletteManager();
        byte[] remapbuf = new byte[256];
        remapbuf[24] = (byte) 143;
        paletteManager.makePalookup(RED_PAL, remapbuf, 0, 0, 0, 1);
        remapbuf[24] = (byte) 188;
        paletteManager.makePalookup(GREEN_PAL, remapbuf, 0, 0, 0, 1);
    }

    @Override
    protected Font getFont() {
        return game.getFont(5);
    }

    @Override
    public void showOsd(boolean captured) {
        super.showOsd(captured);
        if (!(game.getScreen() instanceof InitScreen) && !game.pMenu.gShowMenu) {
            Gdx.input.setCursorCatched(!captured);
        }
    }

    @Override
    public int getcolumnwidth(int osdtextscale) {
        double columns = renderer.getWidth() * (osdtextscale / 65536.0f) / 5.93f;
        return (int) columns;
//        return (int) (divscale(renderer.getWidth(), osdtextscale, 16) / 5.9f);
    }

    @Override
    protected int calcStartX(int x, int scale) {
        return mulscale(6L * x, scale, 16) + 3;
    }

    @Override
    public boolean textHandler(String message) {
        if (pGameInfo.nGameType != kNetModeOff) {
            return false;
        }

        char[] lockeybuf = message.toCharArray();
        int i = 0;
        while (i < lockeybuf.length && lockeybuf[i] != 0) {
            lockeybuf[i++] += 1;
        }
        String cheat = new String(lockeybuf).toUpperCase();

        int ep = -1, lvl = -1;
        boolean wrap1 = false;
        boolean wrap2 = false;
        boolean isMario = false;

        if (cheat.startsWith(cheatCode[17]) || cheat.startsWith(cheatCode[18]) || cheat.startsWith(cheatCode[35])) {

            isMario = true;

            i = 0;
            while (i < message.length() && message.charAt(i) != 0 && message.charAt(i) != ' ') {
                i++;
            }
            cheat = cheat.substring(0, i);
            message = message.replaceAll("[\\s]{2,}", " ");
            int startpos = ++i;
            while (i < message.length() && message.charAt(i) != 0 && message.charAt(i) != ' ') {
                i++;
            }

            if (i <= message.length()) {
                String nEpisode = message.substring(startpos, i);
                nEpisode = nEpisode.replaceAll("[^0-9]", "");
                if (!nEpisode.isEmpty()) {
                    try {
                        ep = Integer.parseInt(nEpisode);
                        wrap1 = true;
                        startpos = ++i;
                        while (i < message.length() && message.charAt(i) != 0 && message.charAt(i) != ' ') {
                            i++;
                        }
                        if (i <= message.length()) {
                            String nLevel = message.substring(startpos, i);
                            nLevel = nLevel.replaceAll("[^0-9]", "");
                            if (!nLevel.isEmpty()) {
                                lvl = Integer.parseInt(nLevel);
                                wrap2 = true;
                            }
                        }
                    } catch (Exception ignored) {
                    }
                }
            }
        }

        boolean isCheat = false;
        for (String s : cheatCode) {
            if (cheat.equalsIgnoreCase(s)) {
                isCheat = true;
                break;
            }
        }

        if (!game.isCurrentScreen(gGameScreen) && isCheat) {
            Console.out.println(message + ": not in a game");
            return true;
        }

        if (wrap1) {
            if (wrap2) {
                return IsCheatCode(cheat, ep, lvl);
            } else {
                return IsCheatCode(cheat, ep);
            }
        } else {
            if (isMario) {
                Console.out.println("mario <level> or <episode> <level>");
                return true;
            }

            if (cheat.equalsIgnoreCase(cheatCode[36]) || cheat.equalsIgnoreCase(cheatCode[17])) {
                Console.out.println(message + ": level will end");
                IsCheatCode(cheat);
                return true;
            } else {
                return IsCheatCode(cheat);
            }
        }
    }
}

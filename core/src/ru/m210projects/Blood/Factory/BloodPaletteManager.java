package ru.m210projects.Blood.Factory;

import ru.m210projects.Blood.Types.LOADITEM;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Types.Color;
import ru.m210projects.Build.Render.listeners.PaletteListener;
import ru.m210projects.Build.Script.TextureHDInfo;
import ru.m210projects.Build.Types.FastColorLookup;
import ru.m210projects.Build.Types.Palette;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.game;
import static ru.m210projects.Blood.Strings.pal;
import static ru.m210projects.Blood.Strings.plu;
import static ru.m210projects.Build.Engine.MAXPALOOKUPS;
import static ru.m210projects.Build.Engine.parallaxvisibility;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Build.Pragmas.mulscale;

public class BloodPaletteManager implements PaletteManager {

    protected static final Color DEFAULT_COLOR = new Color(0, 0, 0, 0);

    public byte[][] palTable = new byte[kMaxPalettes][];
    public int curPaletteIndex;
    public int gGammaLevels;
    public byte[][] gammaTable;
    private boolean gFogMode;
    protected PaletteListener listener = PaletteListener.DUMMY_PALETTE_CHANGE_LISTENER;

    private final Palette currentPalette;
    private final FastColorLookup fastColorLookup;
    private final byte[] basePalette;
    private final byte[][] palookup;
    private final byte[] transluc;
    private final int shadesCount;
    private final Color[] palookupfog;
    private final Engine engine;

    public BloodPaletteManager(BloodEngine engine, boolean gFogMode) throws Exception {
        this.gFogMode = gFogMode;
        this.curPaletteIndex = kPalNormal;
        this.basePalette = new byte[768];
        this.palookup = new byte[MAXPALOOKUPS][];
        this.engine = engine;
        this.gammaTable = new byte[16][256];
        this.palookupfog = new Color[MAXPALOOKUPS];
        this.currentPalette = new Palette();
        this.currentPalette.update(basePalette);
        this.fastColorLookup = new BloodFastColorLookup(this, 1, 1, 1);

        Console.out.println("Loading palettes");
        for (LOADITEM loaditem : PAL) {
            Entry entry = game.getCache().getEntry(loaditem.name + "." + pal, true);
            if (!entry.exists()) {
                Console.out.println(loaditem.name + ".PAL not found", OsdColor.RED);
                continue;
            }

            palTable[loaditem.index] = entry.getBytes();
        }

        // copy the default palette into ken's variable so Std color stuff still works
        System.arraycopy(palTable[kPalNormal], 0, basePalette, 0, 768);

        shadesCount = kPalLookups;

        loadPLUs(gFogMode);

        Console.out.println("Loading translucency table");

        Entry entry = game.getCache().getEntry("trans.tlu", true);
        if (!entry.exists()) {
            throw new Exception("TRANS.TLU not found");
        }
        transluc = entry.getBytes();

        Console.out.println("Loading gamma correction table");
        Entry gamma = game.getCache().getEntry("gamma.dat", true);
        if (!gamma.exists()) {
            throw new Exception("Gamma table not found");
        }

        byte[] buf = gamma.getBytes();
        gGammaLevels = buf.length / 256;
        for (int i = 0; i < 16; i++) {
            System.arraycopy(buf, i * 256, gammaTable[i], 0, 256);
        }
    }

    public int getGammaLevels() {
        return gGammaLevels;
    }

    public boolean isFogMode() {
        return gFogMode;
    }

    public void loadPLUs(boolean gFogMode) {
        this.gFogMode = gFogMode;
        if (gFogMode) {
            Entry entry = game.getCache().getEntry("FOG.FLU", true);
            if (!entry.exists()) {
                Console.out.println("FOG.FLU not found", OsdColor.RED);
                return;
            }

            byte[] buf = entry.getBytes();
            for (int i = 0; i < kMaxPLU; i++) {
                System.arraycopy(buf, 768 * i, palookup[PLU[i].index], 0, 768);
            }

            parallaxvisibility = 3072;
        } else {
            for (int i = 0; i < kMaxPLU; i++) {
                Entry entry = game.getCache().getEntry(PLU[i].name + "." + plu, true);
                if (!entry.exists()) {
                    Console.out.println(PLU[i].name + ".PLU not found", OsdColor.RED);
                    return;
                }

                byte[] buf = entry.getBytes();
                if (buf.length / 256 != kPalLookups) {
                    Console.out.println("Incorrect PLU size", OsdColor.RED);
                    return;
                }

                //[index][color + 256 * shade]
                palookup[PLU[i].index] = buf;
                int colnum = 0;
                int colindex = palookup[PLU[i].index][colnum + 256 * 63] & 0xFF;

                palookupfog[PLU[i].index] = new Color(basePalette[colindex], basePalette[colindex + 1], basePalette[colindex + 2], 0);
            }
        }
    }

    public void invalidate() {
        curPaletteIndex = -1;
    }

    public void setPalette(int nPalette) {
        if (curPaletteIndex == nPalette) {
            return;
        }

        curPaletteIndex = nPalette;
        engine.setbrightness(getPaletteGamma(), palTable[curPaletteIndex]); // reset baseline colors

        if (game.currentDef != null) {
            TextureHDInfo hdInfo = game.currentDef.texInfo;
            if (nPalette == kPalWater) {
                hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 100, 160, 255, 0);
            } else if (nPalette == kPalBeast) {
                hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 255, 120, 120, 0);
            } else if (nPalette == kPalSewer) {
                hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 180, 200, 50, 0);
            } else {
                hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 255, 255, 255, 0);
            }
        }
    }

    @Override
    public Color getFogColor(int pal) {
        if (palookupfog[pal] == null) {
            return DEFAULT_COLOR;
        }

        return palookupfog[pal];
    }

    @Override
    public boolean isValidPalette(int paletteIndex) {
        if (paletteIndex < 0 || paletteIndex >= palookup.length) {
            return false;
        }
        return palookup[paletteIndex] != null;
    }

    @Override
    public boolean changePalette(final byte[] palette) {
        Renderer renderer = game.getRenderer();
        if (!currentPalette.update(palette)) {
            return false;
        }
        fastColorLookup.invalidate();

        renderer.changepalette(palette);
        return true;
    }

    @Override
    public byte[] makePalookup(final int palnum, byte[] remapbuf, int r, int g, int b, int dastat) { // jfBuild
        if (!isValidPalette(palnum)) {
            palookup[palnum] = new byte[shadesCount << 8];
        }

        if (dastat == 0 || (r | g | b | 63) != 63) {
            return palookup[palnum];
        }

        if (dastat == 2) {
            int len = Math.min(palookup[palnum].length, remapbuf.length);
            System.arraycopy(remapbuf, 0, palookup[palnum], 0, len);
        } else {
            if ((r | g | b) == 0) {
                for (int i = 0; i < 256; i++) {
                    for (int j = 0; j < shadesCount; j++) {
                        palookup[palnum][i + j * 256] = palookup[0][(remapbuf[i] & 0xFF) + j * 256];
                    }
                }
            } else {
                byte[] palette = currentPalette.getBytes();
                for (int i = 0; i < shadesCount; i++) {
                    int palscale = divscale(i, shadesCount, 16);
                    for (int j = 0; j < 256; j++) {
                        int rptr = palette[3 * (remapbuf[j] & 0xFF)] & 0xFF;
                        int gptr = palette[3 * (remapbuf[j] & 0xFF) + 1] & 0xFF;
                        int bptr = palette[3 * (remapbuf[j] & 0xFF) + 2] & 0xFF;

                        palookup[palnum][j + i * 256] = fastColorLookup.getClosestColorIndex(palette, rptr + mulscale(r - rptr, palscale, 16), gptr + mulscale(g - gptr, palscale, 16), bptr + mulscale(b - bptr, palscale, 16));
                    }
                }
            }
            palookupfog[palnum] = new Color(r, g, b, 0);
        }

        listener.onPalookupChanged(palnum);
        return palookup[palnum];
    }

    @Override
    public int getPaletteGamma() {
        return engine.getConfig().getPaletteGamma();
    }

    @Override
    public byte[] getBasePalette() {
        return basePalette;
    }

    @Override
    public Palette getCurrentPalette() {
        return currentPalette;
    }

    @Override
    public FastColorLookup getFastColorLookup() {
        return fastColorLookup;
    }

    @Override
    public int getColorIndex(int pal, int colorIndex) {
        if (colorIndex >= palookup[pal].length) {
            return 0;
        }

        return palookup[pal][colorIndex] & 0xFF;
    }

    @Override
    public int getColorIndex(int pal, int color, int shade) {
        if (!isValidPalette(pal)) {
            pal = 0;
        }

        if (color >= palookup[pal].length) {
            return 0;
        }

        shade = (min(max(shade, 0), shadesCount - 1));
        return palookup[pal][color + (shade << 8)] & 0xFF;
    }

    @Override
    public byte[][] getPalookupBuffer() {
        return palookup;
    }

    @Override
    public byte[] getTranslucBuffer() {
        return transluc;
    }

    @Override
    public int getShadeCount() {
        return shadesCount;
    }

    @Override
    public byte[][] getBritableBuffer() {
        return gammaTable;
    }

    @Override
    public void setListener(PaletteListener listener) {
        this.listener = listener;
    }

    @Override
    public void setbrightness(int nGamma, byte[] dapal) {
        int curbrightness = BClipRange(nGamma, 0, gGammaLevels);
        byte[] temppal = new byte[768];
        if (curbrightness != 0) {
            for (int i = 0; i < dapal.length; i++) {
                temppal[i] = gammaTable[nGamma][dapal[i] & 0xFF];
            }
        } else {
            System.arraycopy(dapal, 0, temppal, 0, dapal.length);
        }
        int r = gammaTable[curbrightness][1] >> 2;
        int g = gammaTable[curbrightness][1] >> 2;
        int b = gammaTable[curbrightness][1] >> 2;
        setFogColor(0, new Color(r, g, b, 0));
        changePalette(temppal);
    }

    protected void setFogColor(int palnum, Color fogColor) {
        if (!fogColor.equals(palookupfog[palnum])) {
            palookupfog[palnum] = fogColor;
        }
    }
}

package ru.m210projects.Blood.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.GdxRender.GDXOrtho;
import ru.m210projects.Build.Render.GdxRender.GDXRenderer;
import ru.m210projects.Build.settings.GameConfig;

import static ru.m210projects.Blood.Mirror.MIRROR;
import static ru.m210projects.Blood.Mirror.MIRRORLABEL;

public class BloodPolygdx extends GDXRenderer implements BloodRenderer {

    private boolean restorepalette;

    public BloodPolygdx(GameConfig config) {
        super(config);
    }

    @Override
    public void init(Engine engine) {
        super.init(engine);
        TRANSLUSCENT1 = 0.33f;
        TRANSLUSCENT2 = 0.66f;
    }

    @Override
    protected GDXOrtho allocOrphoRenderer(Engine engine) {
        return new GDXOrtho(this, new BloodMapSettings(engine.getBoardService()));
    }

    @Override
    public int getCurrentShade() {
        return globalshade;
    }

    @Override
    protected int[] getMirrorTextures() {
        return new int[]{MIRROR, MIRRORLABEL, MIRRORLABEL + 1, MIRRORLABEL + 2, MIRRORLABEL + 3, MIRRORLABEL + 4,
                MIRRORLABEL + 5, MIRRORLABEL + 6, MIRRORLABEL + 7, MIRRORLABEL + 8, MIRRORLABEL + 9, MIRRORLABEL + 10,
                MIRRORLABEL + 11, MIRRORLABEL + 12, MIRRORLABEL + 13, MIRRORLABEL + 14, MIRRORLABEL + 15,};
    }

    @Override
    public void setaspect(int daxrange, int daaspect) {
        super.setaspect(daxrange, daaspect);
    }

    @Override
    public void setdrunk(float intensive) {
        //TODO
    }

    @Override
    public void setRestorePalette() {
        restorepalette = true;
    }

    @Override
    public boolean isRestorePalette() {
        if (restorepalette) {
            restorepalette = false;
            return true;
        }
        return false;
    }
}

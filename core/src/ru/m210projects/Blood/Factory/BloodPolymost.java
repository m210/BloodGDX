// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Factory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.GLFog;
import ru.m210projects.Build.Render.GLInfo;
import ru.m210projects.Build.Render.Polymost.Polymost;
import ru.m210projects.Build.Render.Polymost.Polymost2D;
import ru.m210projects.Build.Render.RenderingType;
import ru.m210projects.Build.Render.TexFilter;
import ru.m210projects.Build.Render.TextureHandle.DummyTileData;
import ru.m210projects.Build.Render.TextureHandle.GLTile;
import ru.m210projects.Build.Render.TextureHandle.TextureManager;
import ru.m210projects.Build.Render.TextureHandle.TileData.PixelFormat;
import ru.m210projects.Build.Render.Types.Color;
import ru.m210projects.Build.Render.Types.GL10;
import ru.m210projects.Build.Render.Types.ScreenFade;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.settings.GameConfig;

import static java.lang.Math.*;
import static ru.m210projects.Blood.Main.game;
import static ru.m210projects.Blood.Types.ScreenEffect.SCREEN_DAC_ARRAY;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.Render.Types.GL10.GL_TEXTURE0;
import static ru.m210projects.Build.Render.Types.GL10.*;

public class BloodPolymost extends Polymost implements BloodRenderer {

    private final float[] pal1_color = {1.0f, 1.0f, 1.0f, 1.0f};
    protected int framew;
    protected int frameh;
    protected GLTile frameTexture;
    private GLTile dummy;
    private boolean drunk;
    private float drunkIntensive = 1.0f;

    private boolean restorepalette;

    public BloodPolymost(GameConfig config) {
        super(config);
        globalfog = new GLFog() {
            @Override
            public void calc() {
                int numshades = paletteManager.getShadeCount();
                if (combvis == 0) {
                    start = FULLVIS_BEGIN;
                    end = FULLVIS_END;
                } else if (shade >= numshades - 1) {
                    start = -1;
                    end = 0.001f;
                } else {
                    start = (shade > 0) ? 0 : -(FOGDISTCONST * shade) / combvis;
                    end = (FOGDISTCONST * (numshades - 1 - shade)) / combvis;
                }

                Color fog = paletteManager.getFogColor(pal);
                color[0] = (fog.r / 63.f);
                color[1] = (fog.g / 63.f);
                color[2] = (fog.b / 63.f);
                color[3] = 1;

                if (pal == 1 && (GLInfo.multisample == 0 || !game.pCfg.isPaletteEmulation())) { // Blood's pal 1
                    start = 0;
                    if (end > 2) {
                        end = 2;
                    }
                }

//				if (manager.getShader() != null)
//					manager.getShader().setFogParams(true, start, end, color);
                gl.glFogfv(GL_FOG_COLOR, color, 0);
                gl.glFogf(GL_FOG_START, start);
                gl.glFogf(GL_FOG_END, end);
            }
        };
        globalfog.setFogScale(64);
    }

    @Override
    public void init(Engine engine) {
        super.init(engine);
        TRANSLUSCENT1 = 0.33f;
        TRANSLUSCENT2 = 0.66f;
    }

    @Override
    public int getCurrentShade() {
        return globalshade;
    }

    @Override
    public void drawmasks() {
        super.drawmasks();
        if (drunk) {
            Gdx.gl.glActiveTexture(GL_TEXTURE0);
            boolean hasShader = texshader != null && texshader.isBinded();
            if (hasShader) {
                texshader.unbind();
            }

            if (frameTexture == null || framew != xdim || frameh != ydim) {
                int size;
                for (size = 1; size < Math.max(xdim, ydim); size <<= 1) {
                }

                if (frameTexture != null) {
                    frameTexture.dispose();
                } else {
                    frameTexture = textureCache.newTile(PixelFormat.Rgb, size, size);
                }

                frameTexture.bind();

                gl.glTexImage2D(GL_TEXTURE_2D, 0, GL10.GL_RGB, frameTexture.getWidth(), frameTexture.getHeight(), 0,
                        GL10.GL_RGB, GL_UNSIGNED_BYTE, null);
                frameTexture.unsafeSetFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
                framew = xdim;
                frameh = ydim;
            }

            textureCache.bind(frameTexture);
            gl.glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, frameTexture.getWidth(), frameTexture.getHeight());

            gl.glDisable(GL_DEPTH_TEST);
            gl.glDisable(GL_ALPHA_TEST);
            gl.glEnable(GL_TEXTURE_2D);

            gl.glMatrixMode(GL_PROJECTION);
            gl.glPushMatrix();
            gl.glLoadIdentity();

            float tiltang = (drunkIntensive * 360) / 2048f;
            float tilt = min(max(tiltang, -MAXDRUNKANGLE), MAXDRUNKANGLE);

            gl.glScalef(1.05f, 1.05f, 1);
            gl.glRotatef(tilt, 0, 0, 1.0f);

            gl.glMatrixMode(GL_MODELVIEW);
            gl.glPushMatrix();
            gl.glLoadIdentity();

            float u = (float) xdim / frameTexture.getWidth();
            float v = (float) ydim / frameTexture.getHeight();

            gl.glColor4f(1, 1, 1, abs(tilt) / (2 * MAXDRUNKANGLE));
            gl.glBegin(GL10.GL_TRIANGLE_FAN);
            gl.glTexCoord2f(0, 0);
            gl.glVertex2f(-1f, -1f);

            gl.glTexCoord2f(0, v);
            gl.glVertex2f(-1f, 1f);

            gl.glTexCoord2f(u, v);
            gl.glVertex2f(1f, 1f);

            gl.glTexCoord2f(u, 0);
            gl.glVertex2f(1f, -1f);
            gl.glEnd();

            gl.glMatrixMode(GL_MODELVIEW);
            gl.glPopMatrix();
            gl.glMatrixMode(GL_PROJECTION);
            gl.glPopMatrix();

            gl.glEnable(GL_DEPTH_TEST);
            gl.glEnable(GL_ALPHA_TEST);
            gl.glDisable(GL_TEXTURE_2D);

            if (hasShader) {
                texshader.bind();
            }
        }
    }

    @Override
    public void setdrunk(float intensive) {
        if (intensive == 0) {
            drunk = false;
            drunkIntensive = 0;
        } else {
            drunk = true;
            drunkIntensive = intensive;
        }
    }

    @Override
    public void setRestorePalette() {
        restorepalette = true;
    }

    @Override
    public boolean isRestorePalette() {
        if (restorepalette) {
            restorepalette = false;
            return true;
        }
        return false;
    }

    @Override
    public void setaspect(int daxrange, int daaspect) {
        super.setaspect(daxrange, daaspect);
    }

    @Override
    protected Polymost2D allocOrphoRenderer(Engine engine) {
        return new Polymost2D(this, new BloodMapSettings(engine.getBoardService()));
    }

    @Override
    protected void calc_and_apply_fog(int shade, int vis, int pal) {
        PixelFormat fmt = textureCache.getFmt(globalpicnum);
        if (fmt != PixelFormat.Pal8) {
            if (renderingType == RenderingType.Sprite && globalpal == 5 && globalshade == 127) {
                shade = 0; // Blood's shadows (for pal 1)
            }

            if (globalpal == 1 || pal == 1) {
                if (renderingType == RenderingType.Model) {
                    shade = tspriteptr[RenderingType.Model.getIndex()].getShade();
                    if (shade > 0) {
                        shade = BClipRange((int) (2.8f * shade), 32, 52);
                    }
                } else {
                    shade = 0;
                }
            }
        }

        super.calc_and_apply_fog(shade, vis, pal);
    }

    @Override
    public void uninit() {
        if (dummy != null) {
            dummy.delete();
        }
        dummy = null;
        super.uninit();
    }

    @Override
    public com.badlogic.gdx.graphics.Color getshadefactor(int shade, int method) {
        if (RenderingType.Skybox.getIndex() != 0 && globalpal == 1 && GLInfo.multisample != 0) {
            bindBloodPalette(shade);
            return super.getshadefactor(0, 0);
        }

        com.badlogic.gdx.graphics.Color c = super.getshadefactor(shade, method);
        if (globalpal == 1) {
            c.r = c.g = c.b = 1; // Blood's pal 1
        }

        return c;
    }

    protected void bindDummyTexture() {
        if (dummy == null) {
            dummy = new GLTile(new DummyTileData(PixelFormat.Rgba, 1, 1), 1, TexFilter.NONE);
        }
        bind(dummy);
    }

    public void bindBloodPalette(int shade) {
        textureCache.activateEffect();
        bindDummyTexture();

        int numshades = paletteManager.getShadeCount();
        pal1_color[3] = ((numshades - shade) / (float) numshades) + 0.1f;
        gl.glTexEnvfv(GL_TEXTURE_ENV, 8705, pal1_color, 0); // GL_TEXTURE_ENV_COLOR

        gl.glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
        gl.glTexEnvf(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_INTERPOLATE_ARB);

        gl.glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_PREVIOUS_ARB);
        gl.glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND0_RGB_ARB, GL_SRC_COLOR);

        gl.glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE1_RGB_ARB, 34166); // GL_CONSTANT
        gl.glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND1_RGB_ARB, GL_SRC_COLOR);

        gl.glTexEnvf(GL_TEXTURE_ENV, GL_SOURCE2_RGB_ARB, 34166); // GL_CONSTANT
        gl.glTexEnvf(GL_TEXTURE_ENV, GL_OPERAND2_RGB_ARB, GL_SRC_ALPHA);
    }

    @Override
    public TextureManager newTextureManager(Engine engine) {
        return new BloodTextureManager(engine);
    }

    @Override
    public void setTextureParameters(GLTile tile, ArtEntry artEntry, int pal, int shade, int skybox, int method) {
        if (tile.getPixelFormat() == PixelFormat.Pal8) {
            if (!texshader.isBinded()) {
                gl.glActiveTexture(GL_TEXTURE0);
                texshader.bind();
            }
            texshader.setTextureParams(pal, shade);

            float alpha = 1.0f;
            switch (method & 3) {
                case 2:
                    alpha = TRANSLUSCENT1;
                    break;
                case 3:
                    alpha = TRANSLUSCENT2;
                    break;
            }

            if (!artEntry.exists()) {
                alpha = 0.01f; // Hack to update Z-buffer for invalid mirror textures
            }

            texshader.setTextureSize(tile.getWidth(), tile.getHeight());
            texshader.setPaletteFiltered(config.getPaletteFiltered());
            texshader.setSoftShading(config.getSoftShading());
            texshader.setDrawLastIndex((method & 3) == 0 || !textureCache.alphaMode(method));
            texshader.setTransparent(alpha);
        } else {
            // texture scale by parkar request
            if (tile.isHighTile() && ((tile.getHiresXScale() != 1.0f) || (tile.getHiresYScale() != 1.0f))
                    && RenderingType.Skybox.getIndex() == 0) {
                gl.glMatrixMode(GL_TEXTURE);
                gl.glLoadIdentity();
                gl.glScalef(tile.getHiresXScale(), tile.getHiresYScale(), 1.0f);
                gl.glMatrixMode(GL_MODELVIEW);
            }

            if (GLInfo.multisample != 0 && config.isUseHighTiles() && RenderingType.Skybox.getIndex() == 0) {
                if (config.isDetailMapping()) {
                    GLTile detail = textureCache.get(tile.getPixelFormat(), artEntry, DETAILPAL, 0, method);
                    if (detail != null) {
                        bind(detail);
                        setupTextureDetail(detail);

                        gl.glMatrixMode(GL_TEXTURE);
                        gl.glLoadIdentity();
                        if (detail.isHighTile() && (detail.getHiresXScale() != 1.0f)
                                || (detail.getHiresYScale() != 1.0f)) {
                            gl.glScalef(detail.getHiresXScale(), detail.getHiresYScale(), 1.0f);
                        }
                        gl.glMatrixMode(GL_MODELVIEW);
                    }
                }

                if (config.isGlowMapping()) {
                    GLTile glow = textureCache.get(tile.getPixelFormat(), artEntry, GLOWPAL, 0, method);
                    if (glow != null) {
                        bind(glow);
                        setupTextureGlow(glow);
                    }
                }
            }

            com.badlogic.gdx.graphics.Color c = getshadefactor(shade, method);
            if (tile.isHighTile() && defs.texInfo != null) {
                if (tile.getPal() != pal) {
                    // apply tinting for replaced textures

                    Color p = defs.texInfo.getTints(pal);
                    c.r *= p.r / 255.0f;
                    c.g *= p.g / 255.0f;
                    c.b *= p.b / 255.0f;
                }

                Color pdetail = defs.texInfo.getTints(MAXPALOOKUPS - 1);
                if (pdetail.r != 255 || pdetail.g != 255 || pdetail.b != 255) {
                    c.r *= pdetail.r / 255.0f;
                    c.g *= pdetail.g / 255.0f;
                    c.b *= pdetail.b / 255.0f;
                }
            }

            if (!artEntry.exists()) {
                c.a = 0.01f; // Hack to update Z-buffer for invalid mirror textures
            }
            gl.glColor4f(c.r, c.g, c.b, c.a);
        }
    }


    @Override
    public void scrSetDac() {
        if (config.isPaletteEmulation()) {
            BloodRenderer.super.scrSetDac();
            return;
        }

//        if (game.menu.gShowMenu) {
//            return;
//        }

        boolean hasShader = beginShowFade();
        for (ScreenFade fade : SCREEN_DAC_ARRAY) {
            showScreenFade(fade);
        }
        endShowFade(hasShader);
    }

    private void renderScreenFade() {
        gl.glBegin(GL_TRIANGLES);
        for (int i = 0; i < 6; i += 2) {
            gl.glVertex2f(vertices[i], vertices[i + 1]);
        }
        gl.glEnd();
    }

    @Override
    public void showScreenFade(ScreenFade screenFade) {
        int intensive = screenFade.getIntensive();
        if (intensive == 0) {
            return;
        }

        int r;
        int g = 0;
        int b = 0;
        int a;
        switch (screenFade.getName()) {
            case "Pickup":
                g = r = Math.min(4 * intensive, 255);
                a = Math.min(intensive + 32, 255);

                gl.glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, b, a);
                renderScreenFade();

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(intensive, intensive, 0, 0);
                renderScreenFade();
                break;
            case "Hit":
                intensive *= 3;
                r = Math.min(3 * (intensive + 32), 255);
                a = Math.min(2 * (intensive + 32), 255);

                gl.glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, b, a);
                renderScreenFade();

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(intensive, 0, 0, 0);
                renderScreenFade();
                break;
            case "Drown": {
                intensive >>= 5;
                if (intensive < 128) {
                    r = b = Math.min(255, 3 * intensive);
                    a = 0;
                } else {
                    r = b = 255;
                    a = Math.min(255, 2 * (intensive - 128));
                }

                gl.glBlendFunc(GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(r, g, b, a);
                renderScreenFade();

                int kintensive = intensive >> 6;
                int attenuation = 128 - kintensive;
                if (attenuation < 0) {
                    attenuation = 0;
                }

                gl.glBlendFunc(GL_DST_COLOR, GL_DST_COLOR);
                gl.glColor4ub(attenuation, attenuation, attenuation, 0);
                renderScreenFade();
                if (intensive > 2000) {
                    renderScreenFade();
                }

                int multiple = intensive / 10;
                if (multiple > 160) {
                    multiple = 160;
                }

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(0, 0, 0, multiple);
                renderScreenFade();
                break;
            }
            case "Blind":
                int attenuation = Math.max(0, 128 - intensive);
                gl.glBlendFunc(GL_DST_COLOR, GL_DST_COLOR);
                gl.glColor4ub(attenuation, attenuation, attenuation, 0);
                renderScreenFade();
                renderScreenFade();

                gl.glBlendFunc(GL_ONE_MINUS_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                gl.glColor4ub(0, 0, 0, intensive);
                renderScreenFade();
                break;
        }
    }
}

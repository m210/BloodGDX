package ru.m210projects.Blood.Factory;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.OsdCommandPrompt;

import static ru.m210projects.Blood.Cheats.IsCheatCode;
import static ru.m210projects.Blood.Cheats.cheatCode;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.game;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Build.Pragmas.mulscale;

public class BloodPrompt extends OsdCommandPrompt implements OsdCommandPrompt.ActionListener {

    public BloodPrompt() {
        super(512, 32);
        setActionListener(this);
    }

    public void draw() {
        Renderer renderer = game.getRenderer();
        int nShade = 8;
        int x = mulscale(gViewX0 + 10, gViewX1Scaled, 16);
        int y = mulscale(gViewY0 + 10, gViewY1Scaled, 16);
        y += nextY + numQuotes * yOffset;
        if (pGameInfo.nGameType != kNetModeOff && pGameInfo.nGameType != kNetModeTeams) {
            int row = (numplayers - 1) / 4;
            y += (row + 1) * 9;
        }
        if (pGameInfo.nGameType == kNetModeTeams) y += 22;

        final String text = getTextInput();
        final int cursorPos = getCursorPosition();
        final Font font = game.getFont(0);
        int pos = x + 1;
        int curX = pos;
        for (int i = 0; i < text.length(); i++) {
            pos += font.drawCharScaled(renderer, pos, y, text.charAt(i), 1.0f, nShade, 0, Transparent.None, ConvertType.AlignLeft, false);
            if (i == cursorPos - 1) {
                curX = pos;
            }
        }

        if ((System.currentTimeMillis() & 0x200) == 0) {
            char ch = '_';
            if (isOsdOverType()) {
                ch = '#';
            }
            font.drawCharScaled(renderer, curX, y, ch, 1.0f, nShade, 0, Transparent.None, ConvertType.AlignLeft, false);
        }
    }

    @Override
    public void onEnter(String message) {
        setCaptureInput(false);

        byte[] lockeybuf = message.getBytes();
        int i = 0;
        while (i < lockeybuf.length)
            lockeybuf[i++] += 1;
        String cheat = new String(lockeybuf).toUpperCase();

        int ep = -1, lvl = -1;
        boolean wrap1 = false;
        boolean wrap2 = false;

        if (pGameInfo.nGameType == kNetModeOff) {
            boolean isMario = false;
            if (cheat.startsWith(cheatCode[17]) || cheat.startsWith(cheatCode[18]) || cheat.startsWith(cheatCode[35])) {
                isMario = true;

                i = 0;
                while (i < message.length() && message.charAt(i) != 0 && message.charAt(i) != ' ') i++;
                cheat = cheat.substring(0, i);
                message = message.replaceAll("[\\s]{2,}", " ");
                int startpos = ++i;
                while (i < message.length() && message.charAt(i) != 0 && message.charAt(i) != ' ')
                    i++;
                if (i <= message.length()) {
                    String nEpisode = message.substring(startpos, i);
                    nEpisode = nEpisode.replaceAll("[^0-9]", "");
                    if (!nEpisode.isEmpty()) {
                        try {
                            ep = Integer.parseInt(nEpisode);
                            wrap1 = true;
                            startpos = ++i;
                            while (i < message.length() && message.charAt(i) != 0 && message.charAt(i) != ' ')
                                i++;
                            if (i <= message.length()) {
                                String nLevel = message.substring(startpos, i);
                                nLevel = nLevel.replaceAll("[^0-9]", "");
                                if (!nLevel.isEmpty()) {
                                    lvl = Integer.parseInt(nLevel);
                                    wrap2 = true;
                                }
                            }
                        } catch (Exception ignored) {
                        }
                    }
                }
            }

            if (wrap1) {
                if (wrap2) {
                    if (!IsCheatCode(cheat, ep, lvl))
                        viewSetMessage(message, gPlayer[gViewIndex].nPlayer);
                } else {
                    if (!IsCheatCode(cheat, ep))
                        viewSetMessage(message, gPlayer[gViewIndex].nPlayer);
                }
            } else {
                if (isMario) {
                    viewSetMessage("mario <level> or <episode> <level>", gPlayer[gViewIndex].nPlayer);
                } else if (!IsCheatCode(cheat))
                    viewSetMessage(message, gPlayer[gViewIndex].nPlayer);
            }
        } else {
            game.net.SendNetMessage(message);
            viewSetMessage(message, -1);
        }
    }
}

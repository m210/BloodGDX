package ru.m210projects.Blood.Factory;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Types.ScreenFade;
import ru.m210projects.Build.Types.Palette;

import static ru.m210projects.Blood.Gameutils.ClipRange;
import static ru.m210projects.Blood.Types.ScreenEffect.SCREEN_DAC_ARRAY;

public interface BloodRenderer extends Renderer {

    byte[] tmpDAC = new byte[768];

    void setaspect(int daxrange, int daaspect);

    void setdrunk(float intensive);

    void setRestorePalette();

    boolean isRestorePalette();

    default void scrSetDac() {
        int scrR = 0;
        int scrG = 0;
        int scrB = 0;

        for (ScreenFade fade : SCREEN_DAC_ARRAY) {
            scrR += fade.getRed();
            scrG += fade.getGreen();
            scrB += fade.getBlue();
        }

        boolean update = false;
        if ((scrR | scrG | scrB) != 0) {
            update = true;
            setRestorePalette();
        } else if (isRestorePalette()) {
            update = true;
        }

        if (!update) {
            return;
        }

        Palette curpalette = getPaletteManager().getCurrentPalette();
        for (int i = 0; i < 256; i++) {
            tmpDAC[3 * i] = (byte) ClipRange(curpalette.getRed(i) + scrR, 0, 255);
            tmpDAC[3 * i + 1] = (byte) ClipRange(curpalette.getGreen(i) + scrG, 0, 255);
            tmpDAC[3 * i + 2] = (byte) ClipRange(curpalette.getBlue(i) - scrB, 0, 255);
        }

        changepalette(tmpDAC);
    }

    int getCurrentShade();

}

package ru.m210projects.Blood.Factory;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.Software.Software;
import ru.m210projects.Build.Render.Software.SoftwareOrpho;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.settings.GameConfig;

import static ru.m210projects.Blood.Globals.TILTBUFFER;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Build.Pragmas.dmulscale;

public class BloodSoftware extends Software implements BloodRenderer {

    private boolean restorepalette;

    public BloodSoftware(GameConfig config) {
        super(config);
    }

    @Override
    protected SoftwareOrpho allocOrphoRenderer(Engine engine) {
        return new SoftwareOrpho(this, new BloodMapSettings(engine.getBoardService()));
    }

    @Override
    public void setaspect(int daxrange, int daaspect) {
        super.setaspect(daxrange, daaspect);
    }

    @Override
    public void setdrunk(float deliriumTilt) {
        if (deliriumTilt != 0) {
            ArtEntry pic = engine.getTile(TILTBUFFER);
            if (!(pic instanceof DynamicArtEntry) || !pic.exists() || pic.getWidth() == 0 || pic.getHeight() == 0)
                engine.allocatepermanenttile(TILTBUFFER, 320, 320);
            setviewtotile(engine.getTileManager().getDynamicTile(TILTBUFFER));

            int tilt = ((int) deliriumTilt & 511);
            if (tilt > 256) tilt = 512 - tilt;
            setaspect(dmulscale(256000, Cos(tilt), 160000, Sin(tilt), 32), yxaspect);
        }
    }

    @Override
    public void setRestorePalette() {
        restorepalette = true;
    }

    @Override
    public boolean isRestorePalette() {
        if (restorepalette) {
            restorepalette = false;
            return true;
        }
        return false;
    }

    @Override
    public int getCurrentShade() {
        return globalshade;
    }
}

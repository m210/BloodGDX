package ru.m210projects.Blood.Factory;

import com.badlogic.gdx.math.Vector3;
import org.jetbrains.annotations.Nullable;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BloodSprite extends Sprite {

    private XSPRITE xSprite;
    Vector3 ksprite = new Vector3();
    private long velocityX, velocityY, velocityZ;

    @Nullable
    public XSPRITE getXSprite() {
        return xSprite;
    }

    public void setXSprite(XSPRITE xSprite) {
        this.xSprite = xSprite;
    }

    public Vector3 getKSprite() {
        return ksprite;
    }

    @Override
    public BloodSprite readObject(InputStream is) throws IOException {
        super.readObject(is);
        ksprite.x = StreamUtils.readInt(is);
        ksprite.y = StreamUtils.readInt(is);
        ksprite.z = StreamUtils.readInt(is);

        velocityX = StreamUtils.readInt(is);
        velocityY = StreamUtils.readInt(is);
        velocityZ = StreamUtils.readInt(is);

        return this;
    }

    @Override
    public BloodSprite writeObject(OutputStream os) throws IOException {
        super.writeObject(os);

        StreamUtils.writeInt(os, (int) ksprite.x);
        StreamUtils.writeInt(os, (int) ksprite.y);
        StreamUtils.writeInt(os, (int) ksprite.z);

        StreamUtils.writeInt(os, (int) velocityX);
        StreamUtils.writeInt(os, (int) velocityY);
        StreamUtils.writeInt(os, (int) velocityZ);

        return this;
    }

    public void setVelocity(long x, long y, long z) {
        this.velocityX = x;
        this.velocityY = y;
        this.velocityZ = z;
    }

    public void addVelocity(long x, long y, long z) {
        this.velocityX += x;
        this.velocityY += y;
        this.velocityZ += z;
    }

    public long getVelocityX() {
        return velocityX;
    }

    public void addVelocityX(long x) {
        this.velocityX += x;
    }

    public void setVelocityX(long velocityX) {
        this.velocityX = velocityX;
    }

    public long getVelocityY() {
        return velocityY;
    }

    public void setVelocityY(long velocityY) {
        this.velocityY = velocityY;
    }

    public void addVelocityY(long velocityY) {
        this.velocityY += velocityY;
    }

    public long getVelocityZ() {
        return velocityZ;
    }

    public void setVelocityZ(long velocityZ) {
        this.velocityZ = velocityZ;
    }

    public void addVelocityZ(long velocityZ) {
        this.velocityZ += velocityZ;
    }
}

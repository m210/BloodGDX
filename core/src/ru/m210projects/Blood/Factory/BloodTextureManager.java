// This file is part of BloodGDX.
// Copyright (C) 2017-2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Factory;

import com.badlogic.gdx.graphics.Pixmap;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Render.TexFilter;
import ru.m210projects.Build.Render.TextureHandle.*;
import ru.m210projects.Build.Render.TextureHandle.TileData.PixelFormat;
import ru.m210projects.Build.Types.Palette;
import ru.m210projects.Build.Types.PaletteManager;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static ru.m210projects.Blood.Main.game;

public class BloodTextureManager extends TextureManager {

    public BloodTextureManager(Engine engine) {
        super(engine, ExpandTexture.Both);
    }

    @Override
    protected TileData loadPic(PixelFormat fmt, Hicreplctyp hicr, ArtEntry artEntry, int dapalnum, boolean clamping, boolean alpha, int skybox, TexFilter textureFilter) {
        if (hicr != null) {
            Entry entry = checkResource(hicr, artEntry.getNum(), skybox);
            if (entry != null && entry.exists()) {
                try {
                    byte[] data = entry.getBytes();
                    return new PixmapTileData(new Pixmap(data, 0, data.length), clamping, expand.get());
                } catch (Throwable t) {
                    t.printStackTrace();
                    if (skybox != 0) {
                        return null;
                    }
                }
            }
        }

        if (fmt == PixelFormat.Pal8) {
            return new IndexedTileData(artEntry, clamping, alpha, expand.get());
        }
        return new RGBTileData(engine.getPaletteManager(), artEntry, dapalnum, clamping, alpha, expand.get(), textureFilter != TexFilter.NONE) {
            @Override
            protected int getColor(PaletteManager paletteManager, int dacol, int dapal, boolean alphaMode) {
                dacol &= 0xFF;

                BloodRenderer renderer = game.getRenderer();
                byte[][] palookup = paletteManager.getPalookupBuffer();
                Palette curpalette = paletteManager.getCurrentPalette();
                if (alphaMode && dacol == 255)
                    return curpalette.getRGBA(0, (byte) 0);

                if (dacol >= palookup[dapal].length) return 0;

                if (dapal == 1) {
                    int shade = (min(max(renderer.getCurrentShade(), 0), paletteManager.getShadeCount() - 1));
                    dacol = palookup[dapal][dacol + (shade << 8)] & 0xFF;
                } else dacol = palookup[dapal][dacol] & 0xFF;

                return curpalette.getRGBA(dacol, (byte) 0xFF);
            }
        };
    }
}

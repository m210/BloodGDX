package ru.m210projects.Blood.Factory;

import ru.m210projects.Blood.filehandlers.art.BloodArtFile;
import ru.m210projects.Build.Types.TileManager;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.ArtFile;

import static ru.m210projects.Blood.VERSION.SHAREWARE;

public class BloodTileManager extends TileManager {

    public BloodTileManager() {
        super();
        if (SHAREWARE) {
            this.tilesPath = "shareXXX.art";
        }
    }

    @Override
    public boolean loadpic(Entry artFile) { // gdxBuild
        if (artFile.exists()) {
            ArtFile art = new BloodArtFile(artFile.getName(), artFile::getInputStream);
            for (Entry artEntry : art.getEntries()) {
                ArtEntry tile = ((ArtEntry) artEntry);
                tiles[tile.getNum()] = tile;
            }
            return true;
        }

        return false;
    }
}

package ru.m210projects.Blood.Factory;

import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.LinkedMap;
import ru.m210projects.Build.Types.collections.LinkedList;
import ru.m210projects.Build.Types.collections.ListNode;

import java.util.List;

import static ru.m210projects.Blood.Main.boardService;

/**
 * XSprite pool. Actually, it's a list (map of two lists). Has two keys:
 * 0 - allocated XSprites
 * 1 - free XSprite
 * When allocating new xsprite, the map removing the object from list of free objects and inserting it to allocated one.
 */
public class BloodXSpriteMap extends LinkedMap<XSPRITE> {

    /**
     * Maximum number of {@link XSPRITE}.
     */
    private static final int kMaxXSprites = 2048;

    public BloodXSpriteMap(List<XSPRITE> list) {
        super(1, list, kMaxXSprites, XSPRITE::setReference);

        final LinkedList<XSPRITE> oldList = basket[poolIndex];
        LinkedList<XSPRITE> newList = new LinkedList<>();
        while (oldList.getSize() != 0) {
            newList.addFirst(oldList.removeFirst());
        }
        basket[poolIndex] = newList;
    }

    public XSPRITE getXSprite(int nXSprite) {
        if (nXSprite < 0) {
            return null;
        }

        if (nXSprite >= nodeMap.length) {
            increase(nXSprite);
        }

        return list.get(nXSprite);
    }

    @Override
    protected int insert(ListNode<XSPRITE> node, int nSprite) {
        final LinkedList<XSPRITE> list = basket[0];
        list.addLast(node);
        setValue(node, nSprite);
        return node.getIndex();
    }

    public int insert(Sprite pSprite) {
        ListNode<XSPRITE> node = obtain();
        int nXSprite = node.getIndex();
        XSPRITE pXSprite = list.get(nXSprite);
        pXSprite.free();

        insert(node, pSprite.getXvel());

        pSprite.setExtra(nXSprite);
        ((BloodSprite) pSprite).setXSprite(pXSprite);

        return nXSprite;
    }

    @Override
    public boolean remove(int nXSprite) {
        if (nXSprite < 0) {
            return false;
        }

        if (nXSprite >= nodeMap.length) {
            increase(nXSprite);
        }

        XSPRITE pXSprite = list.get(nXSprite);
        final int nSprite = pXSprite.getReference();
        if (super.remove(nXSprite)) {
            BloodSprite pSprite = boardService.getSprite(nSprite);
            if (pSprite != null) {
                pSprite.setXSprite(null);
                pSprite.setExtra(-1);
                return true;
            }
        }
        return false;
    }

    @Override
    protected XSPRITE getInstance() {
        return new XSPRITE();
    }

    /**
     *
     * @param nXSprite Sets index of {@link XSPRITE} to non-free basket
     * @return nXSprite from param
     */
    @Override
    public int insert(int nXSprite) {
        if (getXSprite(nXSprite).getReference() != -1) {
            throw new RuntimeException("The XSprite already allocated!");
        }
        set(nXSprite, 0); // 0 - allocated basket
        return nXSprite;
    }

    public int getSize() {
        return basket[0].getSize();
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Fonts;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import static ru.m210projects.Blood.Main.game;

public class BloodFont extends Font {

    public BloodFont(int nTile, int charSpace) {
        ArtEntry pic;
        Renderer renderer = game.getRenderer();
        for (int i = 0; i < 96; i++) {
            pic = renderer.getTile(nTile + i);
            if (pic.getHeight() > this.size) {
                this.size = pic.getHeight();
            }
        }

        this.addCharInfo(' ', new CharInfo(this, -1, 1.0f, renderer.getTile(nTile++).getWidth() + charSpace));
        for (int i = 0; i < 95; i++) {
            pic = renderer.getTile(nTile + i);
            if (pic.getWidth() != 0) {
                this.addCharInfo((char) (i + '!'), new CharInfo(this, nTile + i, 1.0f, pic.getWidth() + charSpace, pic.getWidth(), 0, 0));
            }
        }
    }
}

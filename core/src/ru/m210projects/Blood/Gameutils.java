// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Blood.Types.XWALL;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.ArtEntry;

import java.io.InputStream;
import java.util.Arrays;

import static ru.m210projects.Blood.Actor.kAttrAiming;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Warp.gLowerLink;
import static ru.m210projects.Blood.Warp.gUpperLink;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Build.Strhandler.buildString;

public class Gameutils {

    private static final short[] handled = new short[(MAXSECTORS + 7) >> 3];
    private static final char[] buf = new char[80];
    public static int extents_zTop, extents_zBot;
    public static Vector2 normal = new Vector2();
    /*******************************************************************************
     FUNCTION:		FindSector()

     DESCRIPTION:	This function works like Build's updatesector() function
     except it takes into account z position, which makes it
     give correct values in areas where sectors overlap.

     PARAMETERS:		You should supplies a starting search sector in the nSector
     variable.

     RETURNS:		TRUE if point found and updates nSector, FALSE otherwise.
     *******************************************************************************/
    public static int foundSector;
    public static int clipm_px, clipm_py, clipm_pz, clipm_pnsectnum;
    public static boolean clipmove_error;
    /*******************************************************************************
     FUNCTION:		GetZRange()

     DESCRIPTION:	Cover function for Ken's getzrange

     PARAMETERS:

     RETURNS:

     NOTES:
     *******************************************************************************/
    public static int gz_ceilZ, gz_ceilHit, gz_floorZ, gz_floorHit;
    public static int bseed;
    public static int vseed = 1;

    public static int M2Z(double n) {
        return (int) ((n) * kZUnitsPerMeter);
    }

    public static int M2X(double n) {
        return (int) ((n) * kXUnitsPerMeter);
    }

    public static void GetSpriteExtents(Sprite pSprite) {
        extents_zTop = extents_zBot = pSprite.getZ();
        int nTile = pSprite.getPicnum();

        if (nTile == -1 || (pSprite.getCstat() & kSpriteRMask) == kSpriteFloor) {
            return;
        }

        ArtEntry pic = engine.getTile(nTile);
        extents_zTop -= (pic.getOffsetY() + pic.getHeight() / 2) * (pSprite.getYrepeat() << 2);
        extents_zBot += (pic.getHeight() - (pic.getHeight() / 2 + pic.getOffsetY())) * (pSprite.getYrepeat() << 2);
    }

    private static final Point TMP_POINT = new Point();

    public static Point RotatePoint(int x, int y, int daang, int xpivot, int ypivot) {
        x -= xpivot;
        y -= ypivot;

        TMP_POINT.set(dmulscaler(x, Cos(daang), -y, Sin(daang), 30) + xpivot,
                dmulscaler(y, Cos(daang), x, Sin(daang), 30) + ypivot, 0);
        return TMP_POINT;
    }

    public static boolean IsPlayerSprite(Sprite pSprite) {
        return pSprite != null && pSprite.getLotag() >= kDudePlayer1 && pSprite.getLotag() <= kDudePlayer8;
    }

    public static int ClipRange(int value, int min, int max) {
        if (value < min) {
            value = min;
        }
        if (value > max) {
            value = max;
        }

        return value;
    }

    public static int ClipLow(int value, int min) {
        if (value < min) {
            value = min;
        }

        return value;
    }

    public static int ClipHigh(int value, int max) {
        if (value > max) {
            value = max;
        }

        return value;
    }

    public static float ClipLow(float value, int min) {
        if (value < min) {
            value = min;
        }

        return value;
    }

    public static float ClipHigh(float value, int max) {
        if (value > max) {
            value = max;
        }

        return value;
    }

    /*******************************************************************************
     FUNCTION:		GetWallAngle()

     DESCRIPTION:	Get the angle for a wall vector

     RETURNS:		Angle in the range 0 - kMax360

     NOTES:			Add kAngle90 to get the wall Normal
     *******************************************************************************/
    public static int GetWallAngle(int nWall) {
        int dx = boardService.getWall(boardService.getWall(nWall).getPoint2()).getX() - boardService.getWall(nWall).getX();
        int dy = boardService.getWall(boardService.getWall(nWall).getPoint2()).getY() - boardService.getWall(nWall).getY();
        return EngineUtils.getAngle(dx, dy);
    }

    public static void GetWallNormal(int nWall) {
        if (!boardService.isValidWall(nWall)) {
            throw new AssertException("boardService.isValidWall(nWall)");
        }

        int nx = -(boardService.getWall(boardService.getWall(nWall).getPoint2()).getY() - boardService.getWall(nWall).getY()) >> 4;
        int ny = (boardService.getWall(boardService.getWall(nWall).getPoint2()).getX() - boardService.getWall(nWall).getX()) >> 4;

        int length = EngineUtils.sqrt(nx * nx + ny * ny);
        if (length <= 0) {
            length = 1;
        }

        normal.set(divscale(nx, length, 16), divscale(ny, length, 16));
    }

    private static boolean checkDistance(int nPoint2, int x, int y, int distance) {
        int x1 = boardService.getWall(nPoint2).getX();
        int y1 = boardService.getWall(nPoint2).getY();

        nPoint2 = boardService.getWall(nPoint2).getPoint2();
        int x2 = boardService.getWall(nPoint2).getX();
        int y2 = boardService.getWall(nPoint2).getY();

        distance *= 16;
        int dist2 = distance * distance;
        int dwx = x2 - x1;
        int dwy = y2 - y1;
        int dsx1 = x - x1;
        int dsx2 = x - x2;
        int dsy1 = y - y1;
        int dsy2 = y - y2;

        if (x1 >= x2) {
            if (x <= x2 - distance || x >= x1 + distance) {
                return false;
            }

            if (x1 == x2) {
                if (y1 >= y2) {
                    if (y <= y2 - distance || y >= y1 + distance) {
                        return false;
                    }
                    if (y < y2) {
                        return dsy2 * dsy2 + dsx2 * dsx2 < dist2;
                    }
                    if (y > y1) {
                        return dsy1 * dsy1 + dsx1 * dsx1 < dist2;
                    }
                } else {
                    if (y <= y1 - distance || y >= y2 + distance) {
                        return false;
                    }
                    if (y < y1) {
                        return dsy1 * dsy1 + dsx1 * dsx1 < dist2;
                    }
                    if (y > y2) {
                        return dsy2 * dsy2 + dsx2 * dsx2 < dist2;
                    }
                }
                return true;
            }
        } else if (x <= x1 - distance || x >= x2 + distance) {
            return false;
        }

        if (y2 <= y1) {
            if (y <= y2 - distance || y >= y1 + distance) {
                return false;
            }

            if (y1 == y2) {
                if (x1 >= x2) {
                    if (x <= x2 - distance || x >= x1 + distance) {
                        return false;
                    }
                    if (x < x2) {
                        return dsy2 * dsy2 + dsx2 * dsx2 < dist2;
                    }
                    if (x > x1) {
                        return dsy2 * dsy2 + dsx1 * dsx1 < dist2;
                    }
                } else {
                    if (x <= x1 - distance || x >= x2 + distance) {
                        return false;
                    }
                    if (x < x1) {
                        return dsy2 * dsy2 + dsx1 * dsx1 < dist2;
                    }
                    if (x > x2) {
                        return dsy2 * dsy2 + dsx2 * dsx2 < dist2;
                    }
                }
            }
        } else if (y <= y1 - distance || y >= y2 + distance) {
            return false;
        }

        if (dsy2 * dwy + dwx * dsx2 >= 0) {
            return dsy2 * dsy2 + dsx2 * dsx2 < dist2;
        }
        if (dsy1 * dwy + dwx * dsx1 <= 0) {
            return dsy1 * dsy1 + dsx1 * dsx1 < dist2;
        }

        return ((dwy * dsx1 - dsy1 * dwx) * (dwy * dsx1 - dsy1 * dwx)) < (dist2 * (dwx * dwx + dwy * dwy));
    }

    public static void NearSectors(int nSector, int x, int y, int distance, int[] pSectors, byte[] pSprites, int[] pWalls) {
        if (pSectors == null) {
            throw new AssertException("pSectors != null");
        }

        int sectorcount = 1;
        int nextsectnum = 1;
        pSectors[0] = nSector;

        Arrays.fill(handled, (short) 0);
        handled[nSector >> 3] |= 1 << (nSector & 7);

        int secIndex = 0;
        int xwallcount = 0;

        if (pSprites != null) {
            Arrays.fill(pSprites, (byte) 0);
            pSprites[nSector >> 3] |= 1 << (nSector & 7);
        }

        while (secIndex < sectorcount) {
            int cursect = pSectors[secIndex];
            if (boardService.getSector(cursect) == null) {
                secIndex++;
                continue;
            }
            int startwall = boardService.getSector(cursect).getWallptr();
            int endwall = startwall + boardService.getSector(cursect).getWallnum();
            if (startwall < 0 || endwall < 0) {
                secIndex++;
                continue;
            }
            Wall pWall = boardService.getWall(startwall);
            while (startwall < endwall) {
                if (pWall == null) {
                    startwall++;
                    continue;
                }
                int nextsector = pWall.getNextsector();
                if (nextsector >= 0) {
                    if ((handled[nextsector >> 3] & (1 << (nextsector & 7))) == 0) {
                        handled[nextsector >> 3] |= 1 << (nextsector & 7);
                        int nPoint2 = boardService.getWall(startwall).getPoint2();
                        if (checkDistance(nPoint2, x, y, distance)) {
                            if (pSprites != null) {
                                pSprites[nextsector >> 3] |= 1 << (nextsector & 7);
                            }
                            pSectors[sectorcount++] = nextsector;
                            nextsectnum++;
                            if (pWalls != null) {
                                int nXWall = pWall.getExtra();
                                if (nXWall > 0) {
                                    XWALL pXWall = xwall[nXWall];
                                    if (pXWall.triggerVector) {
                                        if (pXWall.state == 0 && !pXWall.isTriggered) {
                                            pWalls[xwallcount++] = startwall;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                startwall++;
                pWall = boardService.getWall(startwall);
            }
            secIndex++;
        }
        pSectors[nextsectnum] = -1;
        if (pWalls != null) {
            pWalls[xwallcount] = -1;
        }
    }

    /*******************************************************************************
     FUNCTION:		HitScan()

     DESCRIPTION:	Returns the object hit, prioritized in sprite/wall/object
     order.

     PARAMETERS:

     RETURNS:		SS_SPRITE, SS_WALL, SS_FLOOR, SS_CEILING, or -1

     NOTES:			To simplify return object handling: SS_SPRITE is returned
     for flat and floor sprites. SS_WALL is returned for masked
     and one-way walls. In addition to the standard return
     value, HitScan fills the specified hitInfo structure with
     relevant hit data.
     *******************************************************************************/
    public static int HitScan(Sprite pSprite, int z, int dx, int dy, int dz, Hitscan hitInfo, int clipmask, int dist) {
        if (pSprite == null) {
            return -1;
        }

        hitInfo.hitsect = -1;
        hitInfo.hitwall = -1;
        hitInfo.hitsprite = -1;

        // offset the starting point for the vector so we don't hit the source!
        int x = pSprite.getX(); // + mulscale(16, Cos(pSprite.ang), 30);
        int y = pSprite.getY(); // + mulscale(16, Sin(pSprite.ang), 30);
        short nSector = pSprite.getSectnum();

        short oldcstat = pSprite.getCstat();
        pSprite.setCstat(pSprite.getCstat() & ~0x0100);

        if (dist != 0) {
            hitscangoalx = x + mulscale((long) dist << 4, Cos(pSprite.getAng()), 30);
            hitscangoaly = y + mulscale((long) dist << 4, Sin(pSprite.getAng()), 30);
        } else {
            hitscangoaly = 0x1FFFFFFF;
            hitscangoalx = 0x1FFFFFFF;
        }

        engine.hitscan(x, y, z, nSector, dx, dy, dz << 4, hitInfo, clipmask);

        hitscangoaly = 0x1FFFFFFF;
        hitscangoalx = 0x1FFFFFFF;

        pSprite.setCstat(oldcstat);

        if (hitInfo.hitsprite != -1) {
            return SS_SPRITE;
        }

        if (hitInfo.hitwall != -1) {
            short nNextSector = boardService.getWall(hitInfo.hitwall).getNextsector();

            if (nNextSector == -1)    // single-sided wall
            {
                return SS_WALL;
            } else                        // double-sided wall, check if we hit a masked wall
            {
                engine.getzsofslope(nNextSector, hitInfo.hitx, hitInfo.hity, floorz, ceilz);

                if (hitInfo.hitz <= ceilz.get() || hitInfo.hitz >= floorz.get()) {
                    return SS_WALL;
                }

                return SS_MASKED;
            }
        }

        if (hitInfo.hitsect != -1) {
            return (hitInfo.hitz > z) ? SS_FLOOR : SS_CEILING;
        }

        return -1;
    }

    public static int VectorScan(Sprite pActor, int xoffset, int zoffset, int dx, int dy, int dz, int dist, int flags) {
        Sector hitsect = null;
        Sector nextsect = null;
        if (pActor == null) {
            throw new AssertException("pSprite != null");
        }

        pHitInfo.hitsect = -1;
        pHitInfo.hitwall = -1;
        pHitInfo.hitsprite = -1;

        // offset the starting point for the vector so we don't hit the source!
        int x = pActor.getX() + mulscale(xoffset, Cos(pActor.getAng() + kAngle90), 30);
        int y = pActor.getY() + mulscale(xoffset, Sin(pActor.getAng() + kAngle90), 30);
        int z = pActor.getZ() + zoffset;

        short oldcstat = pActor.getCstat();
        pActor.setCstat(pActor.getCstat() & ~kSpriteHitscan);

        if (dist != 0) {
            hitscangoalx = x + mulscale((long) dist << 4, Cos(pActor.getAng()), 30);
            hitscangoaly = y + mulscale((long) dist << 4, Sin(pActor.getAng()), 30);
        } else {
            hitscangoaly = 0x1FFFFFFF;
            hitscangoalx = 0x1FFFFFFF;
        }

        engine.hitscan(x, y, z, pActor.getSectnum(), dx, dy, dz << 4, pHitInfo, 0x1000040);
        pActor.setCstat(oldcstat);

        int hitcount = 256;
        while (hitcount != -1) {
            hitcount--;

            if ((pHitInfo.hitsprite != -1 && !boardService.isValidSprite(pHitInfo.hitsprite))
                    || pHitInfo.hitwall >= boardService.getWallCount()
                    || pHitInfo.hitsect >= boardService.getSectorCount()) {
                return -1;
            }

            if (dist != 0) {
                int hx = pHitInfo.hity - pActor.getY();
                int hy = pHitInfo.hitx - pActor.getX();

                if (EngineUtils.qdist(hx, hy) > dist) {
                    return -1;
                }
            }

            if (pHitInfo.hitsprite != -1) {
                Sprite pSprite = boardService.getSprite(pHitInfo.hitsprite);

                if ((pSprite.getHitag() & kAttrAiming) != 0 && (flags & 1) == 0) {
                    return SS_SPRITE;
                }

                if ((pSprite.getCstat() & kSpriteRMask) == kSpriteFace) {
                    int nTile = pSprite.getPicnum();
                    ArtEntry pic = engine.getTile(nTile);

                    if (pic.getWidth() == 0 || pic.getHeight() == 0) {
                        return SS_SPRITE;
                    }

                    int height = (pic.getHeight() * pSprite.getYrepeat() << 2);
                    int zBot = pSprite.getZ();
                    if ((pSprite.getCstat() & kSpriteOriginAlign) != 0) {
                        zBot += height / 2;
                    }

                    if (pic.getOffsetY() != 0) {
                        zBot -= pic.getOffsetY() * pSprite.getYrepeat() << 2;
                    }

                    if (height <= 0) {
                        throw new AssertException("height > 0");
                    }

                    int texy = muldiv(zBot - pHitInfo.hitz, pic.getHeight(), height);
                    if ((pSprite.getCstat() & kSpriteFlipY) == 0) {
                        texy = pic.getHeight() - texy;
                    }

                    if (texy >= 0 && texy < pic.getHeight()) {

                        int width = pSprite.getXrepeat() * pic.getWidth() >> 2;

                        width = width * 3 / 4;    // aspect ratio correction?

                        int top = dx * (y - pSprite.getY()) - dy * (x - pSprite.getX());
                        int bot = EngineUtils.sqrt(dx * dx + dy * dy);
                        if (width <= 0) {
                            throw new AssertException("width > 0");
                        }

                        int texx = muldiv(top / bot, pic.getWidth(), width);
                        texx += pic.getWidth() / 2 + pic.getOffsetX();

                        if (texx >= 0 && texx < pic.getWidth()) {
                            int texel = getTexelCol(nTile, texx * pic.getHeight() + texy);
                            if (texel != 255) {
                                return SS_SPRITE;
                            }
                        }
                    }

                    // clear hitscan bits on sprite
                    oldcstat = pSprite.getCstat();
                    pSprite.setCstat(pSprite.getCstat() & ~kSpriteHitscan);

                    pHitInfo.hitsect = -1;
                    pHitInfo.hitwall = -1;
                    pHitInfo.hitsprite = -1;

                    x = pHitInfo.hitx;
                    y = pHitInfo.hity;
                    z = pHitInfo.hitz;

                    engine.hitscan(x, y, z, pSprite.getSectnum(), dx, dy, dz << 4, pHitInfo, 0x1000040);

                    // restore the hitscan bits
                    pSprite.setCstat(oldcstat);

                    continue;
                }
                return SS_SPRITE;
            }

            if (pHitInfo.hitwall != -1) {
                Wall pWall = boardService.getWall(pHitInfo.hitwall);
                int nextSector = pWall.getNextsector();
                if (nextSector == -1) {
                    return SS_WALL;
                } else {
                    hitsect = boardService.getSector(pHitInfo.hitsect);
                    nextsect = boardService.getSector(nextSector);
                    engine.getzsofslope((short) nextSector, pHitInfo.hitx, pHitInfo.hity, floorz, ceilz);
                    if (pHitInfo.hitz <= ceilz.get()) {
                        return SS_WALL;
                    }
                    if (pHitInfo.hitz >= floorz.get()) {
                        if ((hitsect != null && (hitsect.getFloorstat() & 1) == 0) || (nextsect != null && (nextsect.getFloorstat() & 1) == 0)) {
                            return SS_WALL;
                        }
                        return SS_FLOOR;
                    }
                    if ((pWall.getCstat() & (kWallOneWay | kWallMasked)) == 0) {
                        return SS_WALL;
                    }

                    // must be a masked wall
                    int zOrg;
                    if ((pWall.getCstat() & kWallBottomOrg) != 0) {
                        zOrg = ClipHigh(hitsect.getFloorz(), nextsect.getFloorz());
                    } else {
                        zOrg = ClipLow(hitsect.getCeilingz(), nextsect.getCeilingz());
                    }

                    int zOff = (pHitInfo.hitz - zOrg) >> 8;
                    if ((pWall.getCstat() & kWallFlipY) != 0) {
                        zOff = -zOff;
                    }

                    int nTile = pWall.getOverpicnum();
                    ArtEntry pic = engine.getTile(nTile);

                    int tsizx = pic.getWidth();
                    int tsizy = pic.getHeight();
                    boolean xnice = (1 << pic.getSizex()) == tsizx;
                    boolean ynice = (1 << pic.getSizey()) == tsizy;
                    if (tsizx == 0 || tsizy == 0) {
                        return SS_WALL;
                    }

                    // calculate y texel coord
                    int texy = zOff * pWall.getYrepeat() / 8 + pWall.getYpanning() * tsizy / 256;

                    int len = EngineUtils.qdist(pWall.getX() - boardService.getWall(pWall.getPoint2()).getX(), pWall.getY() - boardService.getWall(pWall.getPoint2()).getY());
                    int distance, texel;

                    if ((pWall.getCstat() & kWallFlipX) != 0) {
                        distance = EngineUtils.qdist(pHitInfo.hitx - boardService.getWall(pWall.getPoint2()).getX(), pHitInfo.hity - boardService.getWall(pWall.getPoint2()).getY());
                    } else {
                        distance = EngineUtils.qdist(pHitInfo.hitx - pWall.getX(), pHitInfo.hity - pWall.getY());
                    }

                    // calculate x texel coord
                    int texx = distance * pWall.getXrepeat() * 8 / len + pWall.getXpanning();

                    if (xnice) {
                        texx &= (tsizx - 1);
                    } else {
                        texx %= tsizx;
                    }

                    if (ynice) {
                        texy &= (tsizy - 1);
                    } else {
                        texy %= tsizy;
                    }

                    if (ynice) {
                        texel = getTexelCol(nTile, (texx << pic.getSizey()) + texy);
                    } else {
                        if ((texx * pic.getHeight() + texy) >= 0) {
                            texel = getTexelCol(nTile, texx * pic.getHeight() + texy);
                        } else {
                            texel = 0;
                        }
                    }

                    if (texel == 255) {
                        // clear hitscan bits on both sides of the wall
                        short oldcstat1 = pWall.getCstat();
                        pWall.setCstat(pWall.getCstat() & ~kWallHitscan);
                        short oldcstat2 = boardService.getWall(pWall.getNextwall()).getCstat();
                        boardService.getWall(pWall.getNextwall()).setCstat(boardService.getWall(pWall.getNextwall()).getCstat() & ~kWallHitscan);

                        pHitInfo.hitsect = -1;
                        pHitInfo.hitwall = -1;
                        pHitInfo.hitsprite = -1;

                        x = pHitInfo.hitx;
                        y = pHitInfo.hity;
                        z = pHitInfo.hitz;

                        engine.hitscan(x, y, z, pWall.getNextsector(), dx, dy, dz << 4, pHitInfo, 0x1000040);

                        // restore the hitscan bits
                        pWall.setCstat(oldcstat1);
                        boardService.getWall(pWall.getNextwall()).setCstat(oldcstat2);

                        continue;
                    }
                    return SS_MASKED;
                }
            }

            if (pHitInfo.hitsect != -1) {
                if (dz <= 0) {
                    int nLower = gLowerLink[pHitInfo.hitsect];
                    if (nLower < 0) {
                        return SS_CEILING;
                    }
                    int nUpper = boardService.getSprite(nLower).getOwner();
                    if (nUpper < 0) {
                        return SS_CEILING;
                    }
                    pHitInfo.hitsect = -1;
                    pHitInfo.hitwall = -1;
                    pHitInfo.hitsprite = -1;

                    x = pHitInfo.hitx + boardService.getSprite(nUpper).getX() - boardService.getSprite(nLower).getX();
                    y = pHitInfo.hity + boardService.getSprite(nUpper).getY() - boardService.getSprite(nLower).getY();
                    z = pHitInfo.hitz + boardService.getSprite(nUpper).getZ() - boardService.getSprite(nLower).getZ();

                    engine.hitscan(x, y, z, boardService.getSprite(nUpper).getSectnum(), dx, dy, dz << 4, pHitInfo, 0x1000040);
                    continue;
                } else {
                    int nUpper = gUpperLink[pHitInfo.hitsect];
                    if (nUpper < 0) {
                        return SS_FLOOR;
                    }
                    int nLower = boardService.getSprite(nUpper).getOwner();
                    if (nLower < 0) {
                        return SS_FLOOR;
                    }
                    pHitInfo.hitsect = -1;
                    pHitInfo.hitwall = -1;
                    pHitInfo.hitsprite = -1;

                    x = pHitInfo.hitx + boardService.getSprite(nLower).getX() - boardService.getSprite(nUpper).getX();
                    y = pHitInfo.hity + boardService.getSprite(nLower).getY() - boardService.getSprite(nUpper).getY();
                    z = pHitInfo.hitz + boardService.getSprite(nLower).getZ() - boardService.getSprite(nUpper).getZ();

                    engine.hitscan(x, y, z, boardService.getSprite(nLower).getSectnum(), dx, dy, dz << 4, pHitInfo, 0x1000040);
                    continue;
                }
            }
            return -1;
        }
        return -1;
    }

    private static int getTexelCol(int nTile, int pos) {
        ArtEntry art = engine.getTile(nTile);
        try(InputStream is = art.getInputStream()) {
            StreamUtils.skip(is, pos);
            return is.read();
        } catch (Exception e) {
            return 255;
        }
    }

    public static boolean FindSector(int x, int y, int z, int nSector) {
        foundSector = nSector;
        if (engine.inside(x, y, nSector) != 0) {
            engine.getzsofslope(nSector, x, y, floorz, ceilz);
            if (z >= ceilz.get() && z <= floorz.get()) {
                return true;
            }
        }

        int wallid = boardService.getSector(nSector).getWallptr();
        for (int i = boardService.getSector(nSector).getWallnum(); i > 0; i--, wallid++) {
            short j = boardService.getWall(wallid).getNextsector();
            if (j >= 0 && engine.inside(x, y, j) != 0) {
                engine.getzsofslope(j, x, y, floorz, ceilz);
                if (z >= ceilz.get() && z <= floorz.get()) {
                    nSector = j;
                    foundSector = nSector;
                    return true;
                }
            }
        }

        for (int i = 0; i < boardService.getSectorCount(); i++) {
            if (engine.inside(x, y, (short) i) != 0) {
                engine.getzsofslope((short) i, x, y, floorz, ceilz);
                if (z >= ceilz.get() && z <= floorz.get()) {
                    nSector = (short) i;
                    foundSector = nSector;
                    return true;
                }
            }
        }

        return false;
    }

    public static int ClipMove(int px, int py, int pz, int pnSector, long dx, long dy, int wallDist, int ceilDist, int floorDist, int clipType) {
        clipmove_error = false;
        int ccode = engine.clipmove(px, py, pz, pnSector, dx << 14, dy << 14, wallDist, ceilDist, floorDist, clipType);

        clipm_px = clipmove_x;
        clipm_py = clipmove_y;
        clipm_pz = clipmove_z;
        clipm_pnsectnum = clipmove_sectnum;

        // force temporary fix to ken's inside() bug
        if (clipmove_sectnum == -1) {
            // return to last known good location
            clipmove_error = true;
            clipm_px = px;
            clipm_py = py;
            clipm_pz = pz;
            clipm_pnsectnum = pnSector;
        }
        return ccode;
    }

    public static void GetZRange(Sprite pSprite, int clipdist, int cliptype) {
        if (pSprite == null) {
            throw new AssertException("pSprite != null");
        }

        short oldcstat = pSprite.getCstat();
        pSprite.setCstat(pSprite.getCstat() & (~kSpriteBlocking & ~kSpriteHitscan));

        engine.getzrange(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), clipdist, cliptype);

        gz_ceilZ = zr_ceilz;
        gz_ceilHit = zr_ceilhit;
        gz_floorZ = zr_florz;
        gz_floorHit = zr_florhit;

        if ((gz_floorHit & kHitTypeMask) == kHitSector) {
            int sectnum = gz_floorHit & kHitIndexMask;
            if ((cliptype & 0x2000) == 0 && (boardService.getSector(sectnum).getFloorstat() & 1) != 0) {
                gz_floorZ = 0x7FFFFFFF;
            }

            int nXSector = boardService.getSector(sectnum).getExtra();
            if (nXSector > 0) {
                gz_floorZ += xsector[nXSector].Depth << 10;
            }

            int nUpper = gUpperLink[sectnum];
            if (nUpper >= 0) {
                int nLower = boardService.getSprite(nUpper).getOwner();
                if (nLower >= 0) {
                    engine.getzrange(pSprite.getX() + boardService.getSprite(nLower).getX() - boardService.getSprite(nUpper).getX(), pSprite.getY() + boardService.getSprite(nLower).getY() - boardService.getSprite(nUpper).getY(), pSprite.getZ() + boardService.getSprite(nLower).getZ() - boardService.getSprite(nUpper).getZ(), boardService.getSprite(nLower).getSectnum(), clipdist, cliptype);
                    gz_floorZ = zr_florz;
                    gz_floorHit = zr_florhit;
                    gz_floorZ -= (boardService.getSprite(nLower).getZ() - boardService.getSprite(nUpper).getZ());
                }
            }
        }

        if ((gz_ceilHit & kHitTypeMask) == kHitSector) {
            int sectnum = gz_ceilHit & kHitIndexMask;
            if ((cliptype & 0x1000) == 0 && (boardService.getSector(sectnum).getCeilingstat() & 1) != 0) {
                gz_ceilZ = 0x80000000;
            }
            int nLower = gLowerLink[sectnum];
            if (nLower >= 0) {
                int nUpper = boardService.getSprite(nLower).getOwner();
                if (nUpper >= 0) {
                    engine.getzrange(pSprite.getX() + boardService.getSprite(nUpper).getX() - boardService.getSprite(nLower).getX(), pSprite.getY() + boardService.getSprite(nUpper).getY() - boardService.getSprite(nLower).getY(), pSprite.getZ() + boardService.getSprite(nUpper).getZ() - boardService.getSprite(nLower).getZ(), boardService.getSprite(nUpper).getSectnum(), clipdist, cliptype);
                    gz_ceilZ = zr_ceilz;
                    gz_ceilHit = zr_ceilhit;
                    gz_ceilZ -= (boardService.getSprite(nUpper).getZ() - boardService.getSprite(nLower).getZ());
                }
            }
        }
        pSprite.setCstat(oldcstat);
    }

    public static void GetZRange(int x, int y, int z, int nSector, int clipdist, int cliptype) {
        engine.getzrange(x, y, z, nSector, clipdist, cliptype);

        gz_ceilZ = zr_ceilz;
        gz_ceilHit = zr_ceilhit;
        gz_floorZ = zr_florz;
        gz_floorHit = zr_florhit;

        if ((gz_floorHit & kHitTypeMask) == kHitSector) {
            int sectnum = gz_floorHit & HIT_INDEX_MASK;
            if ((cliptype & 0x2000) == 0 && (boardService.getSector(sectnum).getFloorstat() & 1) != 0) {
                gz_floorZ = 0x7FFFFFFF;
            }
            int nXSector = boardService.getSector(sectnum).getExtra();
            if (nXSector > 0) {
                gz_floorZ += xsector[nXSector].Depth << 10;
            }
            int nUpper = gUpperLink[sectnum];
            int nLower;
            if (nUpper >= 0) {
                nLower = boardService.getSprite(nUpper).getOwner();
                if (nLower >= 0) {
                    engine.getzrange(x + boardService.getSprite(nLower).getX() - boardService.getSprite(nUpper).getX(), y + boardService.getSprite(nLower).getY() - boardService.getSprite(nUpper).getY(), z + boardService.getSprite(nLower).getZ() - boardService.getSprite(nUpper).getZ(), boardService.getSprite(nLower).getSectnum(), clipdist, cliptype);

                    gz_floorZ = zr_florz;
                    gz_floorHit = zr_florhit;
                    gz_floorZ -= (boardService.getSprite(nLower).getZ() - boardService.getSprite(nUpper).getZ());
                }
            }
        }

        if ((gz_ceilHit & kHitTypeMask) == kHitSector) {
            int sectnum = gz_ceilHit & HIT_INDEX_MASK;
            if ((cliptype & 0x1000) == 0 && (boardService.getSector(sectnum).getCeilingstat() & 1) != 0) {
                gz_ceilZ = 0x80000000;
            }
            int nLower = gLowerLink[sectnum];
            int nUpper;
            if (nLower >= 0) {
                nUpper = boardService.getSprite(nLower).getOwner();
                if (nUpper >= 0) {
                    engine.getzrange(x + boardService.getSprite(nUpper).getX() - boardService.getSprite(nLower).getX(), y + boardService.getSprite(nUpper).getY() - boardService.getSprite(nLower).getY(), z + boardService.getSprite(nUpper).getZ() - boardService.getSprite(nLower).getZ(), boardService.getSprite(nUpper).getSectnum(), clipdist, cliptype);
                    gz_ceilZ = zr_ceilz;
                    gz_ceilHit = zr_ceilhit;
                    gz_ceilZ -= (boardService.getSprite(nUpper).getZ() - boardService.getSprite(nLower).getZ());
                }
            }
        }
    }

    public static boolean CheckProximity(Sprite pSprite, int x, int y, int z, int nSector, int dist) {
        if (pSprite == null) {
            throw new AssertException("pSprite != null");
        }

        long dx = klabs(x - pSprite.getX()) >> 4;
        if (dx < dist) {
            long dy = klabs(y - pSprite.getY()) >> 4;
            if (dy < dist) {
                long dz = klabs(z - pSprite.getZ()) >> 8;
                if (dz < dist && EngineUtils.qdist(dx, dy) < dist) {
                    GetSpriteExtents(pSprite);
                    if (engine.cansee(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), x, y, z, (short) nSector)) {
                        return true;
                    }
                    if (engine.cansee(pSprite.getX(), pSprite.getY(), extents_zTop, pSprite.getSectnum(), x, y, z, (short) nSector)) {
                        return true;
                    }
                    return engine.cansee(pSprite.getX(), pSprite.getY(), extents_zBot, pSprite.getSectnum(), x, y, z, (short) nSector);
                }
            }
        }
        return false;
    }

    public static int Dist3d(int dx, int dy, int dz) {
        // Euclidean 3D distance
        dx >>= 4;
        dy >>= 4;
        dz >>= 8;
        return EngineUtils.sqrt(dx * dx + dy * dy + dz * dz);

    }

    public static void sRandom(long set) {
        bseed = (int) set;
    }

    public static int bRandom() {
        bseed = (bseed * 1103515245) + 12345;
        return (bseed >> 16) & 0x7FFF;
    }

    public static int vRandom() {
        int var = 2 * vseed;
        if ((byte) vseed > ((byte) vseed + (byte) vseed)) {
            var = var ^ 0x20000004 | 1;
        }
        vseed = var;
        return var & 0x7FFF;
    }

    public static int ViRandom(int var) {
        return mulscale(var, vRandom(), 14) - var;
    }

    public static boolean Chance(int var) {
        return bRandom() < var;
    }

    public static int BiRandom(int var) {
        return mulscale(var, bRandom(), 14) - var;
    }

    public static int BiRandom2(int var) {
        return mulscale(var, bRandom() + bRandom(), 15) - var;
    }

    public static int Random(int var) {
        return mulscale(var, bRandom(), 15);
    }

    public static char[] toCharArray(String... text) {
        buildString(buf, 0, text);
        return buf;
    }
}

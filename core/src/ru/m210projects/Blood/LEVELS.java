// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import ru.m210projects.Blood.Types.EpisodeInfo;
import ru.m210projects.Blood.Types.MapInfo;
import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Blood.filehandlers.EpisodeEntry;
import ru.m210projects.Blood.filehandlers.scripts.BloodDef;
import ru.m210projects.Build.Pattern.Tools.IniFile;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;

import java.nio.file.Path;

import static ru.m210projects.Blood.Actor.IsDudeSprite;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.Random;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.playerReset;
import static ru.m210projects.Blood.PLAYER.resetInventory;
import static ru.m210projects.Blood.ResourceHandler.episodeManager;
import static ru.m210projects.Blood.Types.GAMEINFO.*;
import static ru.m210projects.Blood.View.gViewIndex;
import static ru.m210projects.Blood.View.viewSetMessage;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;

public class LEVELS {

    public static final int kMaxEpisode = 6;
    public static final int kMinMap = 1;
    public static final int kMaxMap = 16;
    public static final int kMaxMessages = 128;

    //Statistics
    public static int foundSecret = 0;
    public static int totalSecrets = 0;
    public static int autoTotalSecrets = 0;
    public static int superSecrets = 0;
    public static int totalKills = 0;
    public static int kills = 0;

    public static int gNextMap;
    public static int nEpisodeCount;

    public static EpisodeInfo currentEpisode;
    public static EpisodeInfo[] gEpisodeInfo = new EpisodeInfo[kMaxEpisode];
    public static EpisodeInfo[] gUserEpisodeInfo = new EpisodeInfo[kMaxEpisode];

    public static MapInfo gUserMapInfo = new MapInfo();

    public static void levelCalcKills() {
        totalKills = 0;
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            Sprite pSprite = node.get();
            if (!IsDudeSprite(pSprite)) {
                Console.out.println("Warning: pDude.type >= kDudeBase && pDude.type < kDudeMax : " + pSprite.getLotag());
                continue;
            }

            if (pSprite.getStatnum() == kStatDude) {
                if (pSprite.getLotag() != kDudeBat && pSprite.getLotag() != kDudeRat && pSprite.getLotag() != kDudeInnocent && pSprite.getLotag() != kDudeBurning) {
                    ++totalKills;
                }
            }
        }
    }

    /**
     * Get map information (title, name, soundtrack)
     *
     * @param mLevelEntry map file entry
     */
    public static MapInfo getUserMapInfo(FileEntry mLevelEntry) {
        String mLevelName = mLevelEntry.getName();
        gUserMapInfo.MapName = mLevelEntry;
        gUserMapInfo.Title = mLevelName;
        gUserMapInfo.Song = null;
        gUserMapInfo.Track = 0;

        FileEntry defFile = mLevelEntry.getParent().getEntry(mLevelName.substring(0, mLevelName.lastIndexOf('.')) + ".def");
        if (defFile.exists()) {
            // the script with map title and soundtrack infos
            BloodIniFile defScript = new BloodIniFile(new EpisodeEntry.File(defFile));
            defScript.initDef();

            String title = defScript.getKeyString("Title");
            if (title != null && !title.isEmpty()) {
                gUserMapInfo.Title = title;
            }

            String song = defScript.getKeyString("Song");
            if (song != null) {
                Path currPath = defFile.getRelativePath();
                song = currPath.resolveSibling(song).toString();
            }
            gUserMapInfo.Song = song;
            gUserMapInfo.Track = defScript.getKeyInt("Track");
        }

        Entry def = mLevelEntry.getParent().getEntry(appdef);
        if (def.exists()) {
            BloodDef addonScript = new BloodDef(game.getBaseDef(), def);
            addonScript.loadScript(def);
            game.setDefs(addonScript);
        }

        return gUserMapInfo;
    }

    public static void levelSetupSecret(int command) {
        if (cfg.useautosecretcount) {
            totalSecrets = autoTotalSecrets;
        } else {
            totalSecrets = command;
        }
    }

    public static void initEpisodeInfo(BloodIniFile mainIni) { //Main.java only
        for (int i = 0; i < kMaxEpisode; i++) {
            gEpisodeInfo[i] = new EpisodeInfo();
            gEpisodeInfo[i].iniFile = mainIni;
            gUserEpisodeInfo[i] = new EpisodeInfo();
        }
        nEpisodeCount = kMaxEpisode;

        episodeManager.putEpisode(mainIni);
    }

    private static String handleCutscenePath(String path) {
        if (path != null) {
            return FileUtils.getPath(path).getFileName().toString();

//            if (out.startsWith(".")) { // "./dir/file"
//                out = out.substring(1);
//            }
//
//            out = FileUtils.getCorrectPath(out);
//            if (out.startsWith(File.separator)) {
//                out = out.substring(1);
//            }
        }

        return path;
    }

    public static int getEpisodeInfo(EpisodeInfo[] pEpisodes, BloodIniFile INI) {
        int nEpisodeCount = 0;
        for (int i = 0; i < kMaxEpisode; i++) {
            EpisodeInfo pEpisode = pEpisodes[i];
            pEpisode.clear();
            if (INI.set("Episode" + (i + 1))) {
                pEpisode.iniFile = INI;
                String title = INI.getKeyString("Title");
                if (title == null || title.isEmpty()) {
                    title = "Episode" + (i + 1);
                }
                pEpisode.Title = title;
                pEpisode.CutSceneA = handleCutscenePath(INI.getKeyString("CutSceneA"));
                pEpisode.CutWavA = handleCutscenePath(INI.getKeyString("CutWavA"));
                pEpisode.CutSceneB = handleCutscenePath(INI.getKeyString("CutSceneB"));
                pEpisode.CutWavB = handleCutscenePath(INI.getKeyString("CutWavB"));

                String BloodBatchOnly = INI.getKeyString("BloodBathOnly");
                if (!BloodBatchOnly.isEmpty() && BloodBatchOnly.charAt(0) == '1') {
                    pEpisode.BBOnly = 1;
                } else {
                    pEpisode.BBOnly = 0;
                }

                pEpisode.CutSceneALevel = 0;
                int CutSceneALevel = INI.getKeyInt("CutSceneALevel");
                if (CutSceneALevel != -1) {
                    pEpisode.CutSceneALevel = CutSceneALevel - 1;
                }

                int nMaps = 0;
                for (int j = kMinMap; j < kMaxMap; j++) {
                    INI.set("Episode" + (i + 1));
                    String mapName = INI.getKeyString("Map" + j);
                    if (!mapName.isEmpty()) {
                        if (pEpisode.gMapInfo[j - 1] == null) {
                            pEpisode.gMapInfo[j - 1] = new MapInfo();
                        }
                        MapInfo pMap = pEpisode.gMapInfo[j - 1];
                        INI.set(mapName);
                        mapName = String.format("%s.map", mapName);
                        Entry mapEntry = game.getCache().getEntry(mapName, true);
                        if (mapEntry.exists()) {
                            pMap.MapName = mapEntry;
                        } else {
                            pMap.MapName = new FileEntry(FileUtils.getPath(mapName), mapName, 0);
                        }
                        levelLoadDef(pMap, INI);
                        nMaps++;
                    }
                }

                if (nMaps == 0) {
                    pEpisode.clear();
                } else {
                    pEpisode.nMaps = nMaps;
                    nEpisodeCount++;
                }
            }
        }
        return nEpisodeCount;
    }

    public static void levelCountSecret(int command) {
        if (command < 0) {
            System.err.println("Invalid secret type " + command + " triggered");
            return;
        }
        if (command != 0) {
            superSecrets++;
        } else {
            foundSecret++;
        }

        if (pGameInfo.nGameType == 0) {
            if (Random(2) == 1) {
                viewSetMessage("You found a secret.", gPlayer[gViewIndex].nPlayer, 9);
            } else {
                viewSetMessage("A secret is revealed.", gPlayer[gViewIndex].nPlayer, 9);
            }
        }
    }

    public static void levelResetKills() {
        kills = totalKills = 0;
    }

    public static void levelResetSecrets() {
        foundSecret = totalSecrets = superSecrets = 0;
    }

    public static void levelAddKills(Sprite pSprite) {
        if (pSprite.getStatnum() == kStatDude) {
            if (pSprite.getLotag() != kDudeBat && pSprite.getLotag() != kDudeRat && pSprite.getLotag() != kDudeInnocent && pSprite.getLotag() != kDudeBurning) {
                ++kills;
            }
        }
    }

    public static void loadMapInfo(int nEp, int nMap) {
        pGameInfo.nEpisode = nEp;
        pGameInfo.nLevel = nMap;

        if (mUserFlag != UserFlag.Addon) {
            currentEpisode = gEpisodeInfo[pGameInfo.nEpisode];
        } else {
            currentEpisode = gUserEpisodeInfo[pGameInfo.nEpisode];
        }

        MapInfo mapInfo = currentEpisode.getMapInfo(nMap);
        if (mapInfo != null) {
            pGameInfo.zLevelSong = mapInfo.Song;
            pGameInfo.zLevelName = mapInfo.MapName;
            pGameInfo.nTrackNumber = mapInfo.Track;
        } else if (mUserFlag != UserFlag.UserMap) {
            pGameInfo.zLevelName = DUMMY_ENTRY;
            pGameInfo.nLevel = 0;
        } // else zLevelName and nLevel already loaded
    }

    public static void levelLoadDef(MapInfo pMap, IniFile INI) {
        pMap.Title = INI.getKeyString("Title");
        if (pMap.Title.isEmpty()) {
            pMap.Title = pMap.MapName.getName();
        }
        pMap.Author = INI.getKeyString("Author");
        pMap.Song = INI.getKeyString("Song");
        String Song = INI.getKeyString("Song");
//		if(Song != null && midPath != null) {
//			int hResource = kOpen(Song + "." + mid, 0);
//			if(hResource == -1) {
//				Song = midPath + File.separator + Song;
//			} else
//				kClose(hResource);
//		}
        pMap.Song = Song;
        pMap.Track = INI.getKeyInt("Track");
        pMap.EndingA = INI.getKeyInt("EndingA");
        pMap.EndingB = INI.getKeyInt("EndingB");
        pMap.Fog = INI.getKeyInt("Fog") == 1;
        pMap.Weather = INI.getKeyInt("Weather") == 1;

        for (int i = 1; i < kMaxMessages; i++) {
            String message = INI.getKeyString("Message" + i);
            if (!message.isEmpty()) {
                pMap.gMessage[i - 1] = message;
            }
        }
    }

    public static void levelEndLevel(int levType) {

        if (numplayers > 1 && game.pNet.bufferJitter > 1 && myconnectindex == connecthead) {
            for (int i = 0; i < game.pNet.bufferJitter; i++) {
                game.pNet.GetNetworkInput(); //wait for other player before level end
            }
        }

        if (!game.pNet.WaitForAllPlayers(5000)) {
            game.pNet.NetDisconnect(myconnectindex);
            return;
        }

        if (pGameInfo.nGameType == kNetModeCoop && pGameInfo.nReviveMode) {
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                if (gPlayer[i].deathTime > 0) {
                    resetInventory(gPlayer[i]);
                    playerReset(i);
                }
            }
        }

        pGameInfo.uGameFlags |= EndOfLevel;
        MapInfo pMap = null;

        if (currentEpisode == null || (pMap = currentEpisode.gMapInfo[pGameInfo.nLevel]) == null) //usermap
        {
            pGameInfo.uGameFlags |= EndOfGame;
            pGameInfo.nLevel = 0;
            return;
        }

        int pnEndingA = pMap.EndingA;
        if (pnEndingA >= 0) {
            pnEndingA--;
        }
        int pnEndingB = pMap.EndingB;
        if (pnEndingB >= 0) {
            pnEndingB--;
        }

        if (levType == 1) {
            if (pnEndingB == -1) {
                if (pGameInfo.nEpisode + 1 < nEpisodeCount) {
                    if (currentEpisode.CutSceneB != null) {
                        pGameInfo.uGameFlags |= CutsceneB;
                    }
                    pGameInfo.nLevel = 0;
                    pGameInfo.uGameFlags |= EndOfGame;
                    return;
                }

                pGameInfo.uGameFlags |= EndOfLevel;
                pGameInfo.nLevel = 0;
                return;
            }
            gNextMap = pnEndingB;
            return;
        }

        if (pnEndingA != -1) {
            gNextMap = pnEndingA;
            return;
        }

        if (currentEpisode.CutSceneB != null) {
            pGameInfo.uGameFlags |= CutsceneB;
        }
        pGameInfo.uGameFlags |= EndOfGame;
        pGameInfo.nLevel = 0;
    }

    /* - This method can be called via sending kCommandNumbered to TX kGDXChannelEndLevel - */
    /* - kCommandNumbered is a level number  -*/
    public static void levelEndLevelCustom(int nLevel) {

        if (!game.pNet.WaitForAllPlayers(5000)) {
            game.pNet.NetDisconnect(myconnectindex);
            return;
        }

        if (pGameInfo.nGameType == kNetModeCoop && pGameInfo.nReviveMode) {
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                if (gPlayer[i].deathTime > 0) {
                    resetInventory(gPlayer[i]);
                    playerReset(i);
                }
            }
        }

        pGameInfo.uGameFlags |= EndOfLevel;
        if (mUserFlag == UserFlag.UserMap || nLevel >= kMaxMap || nLevel < 0) {

            pGameInfo.uGameFlags |= EndOfGame;
            pGameInfo.nLevel = 0;
            return;
        }

        gNextMap = nLevel;
    }
}

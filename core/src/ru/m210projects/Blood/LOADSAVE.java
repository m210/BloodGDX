// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import ru.m210projects.Blood.Factory.BloodBoard;
import ru.m210projects.Blood.Factory.BloodNetwork;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.Menus.MenuCorruptGame;
import ru.m210projects.Blood.PriorityQueue.PriorityItem;
import ru.m210projects.Blood.Types.*;
import ru.m210projects.Blood.Types.Seq.*;
import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Blood.filehandlers.BloodSaveHeader;
import ru.m210projects.Blood.filehandlers.EpisodeEntry;
import ru.m210projects.Build.Board;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.SaveReader;
import ru.m210projects.Build.Pattern.Tools.SaveManager;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.kMaxChannels;
import static ru.m210projects.Blood.EVENT.*;
import static ru.m210projects.Blood.Factory.BloodMenuHandler.CORRUPTLOAD;
import static ru.m210projects.Blood.Gameutils.bseed;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.Mirror.*;
import static ru.m210projects.Blood.PLAYER.playerSetRace;
import static ru.m210projects.Blood.PLAYER.powerupCheck;
import static ru.m210projects.Blood.ResourceHandler.checkEpisodeResources;
import static ru.m210projects.Blood.ResourceHandler.resetEpisodeResources;
import static ru.m210projects.Blood.SECTORFX.InitSectorFX;
import static ru.m210projects.Blood.SOUND.*;
import static ru.m210projects.Blood.Screen.scrReset;
import static ru.m210projects.Blood.Trigger.*;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.*;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Blood.Warp.*;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;

public class LOADSAVE {

    public static final String savsign = "BLUD";
    public static final int gdxSave = 302; // v1.16
    public static final int currentGdxSave = 304;
    public static final int SAVETIME = 8;
    public static final int SAVENAME = 16;
    public static final int SAVELEVELINFO = 9;
    public static final int SAVESCREENSHOTSIZE = 320 * 200;
    public static final int SAVEGDXDATA = 128;
    public static final char[] filenum = new char[4];
    private static final SafeLoader loader = new SafeLoader();
    public static boolean gQuickSaving;
    public static boolean gAutosaveRequest;
    public static LSInfo lsInf = new LSInfo();
    public static int quickslot = 0;
    public static FileEntry lastload;

    SaveReader saveReader = new SaveReader(engine, savsign, currentGdxSave) {
        @Override
        protected SaveHeader getNewHeader(String saveName) {
            return new BloodSaveHeader();
        }
    };

    public static void FindSaves(Directory dir) {
        for (Entry file : dir) {
            if (file.isExtension("sav") && file instanceof FileEntry) {
                try (InputStream is = file.getInputStream()) {
                    String signature = StreamUtils.readString(is, 4);
                    if (signature.isEmpty()) {
                        continue;
                    }

                    if (signature.equals(savsign)) {
                        int nVersion = StreamUtils.readShort(is);
                        if (nVersion >= gdxSave) {
                            StreamUtils.skip(is, 4); // nBuild

                            long time = StreamUtils.readLong(is);
                            String savname = StreamUtils.readString(is, SAVENAME);
                            game.pSavemgr.add(savname, time, (FileEntry) file);
                        }
                    }
                } catch (Exception ignored) {
                }
            }
        }
        game.pSavemgr.sort();
    }

    public static String makeNum(int num) {
        filenum[3] = (char) ((num % 10) + 48);
        filenum[2] = (char) (((num / 10) % 10) + 48);
        filenum[1] = (char) (((num / 100) % 10) + 48);
        filenum[0] = (char) (((num / 1000) % 10) + 48);

        return new String(filenum);
    }

    public static void savegame(Directory dir, String savename, String filename) {
        FileEntry file = dir.getEntry(filename);
        if (file.exists()) {
            if (!file.delete()) {
                viewSetMessage("Game not saved. Access denied!", -1, 7);
                return;
            }
        }

        Path path = dir.getPath().resolve(filename);
        try (OutputStream os = new BufferedOutputStream(Files.newOutputStream(path))) {
            long time = game.date.getCurrentDate();
            save(os, savename, time);

            file = dir.addEntry(path);
            if (file.exists()) {
                game.pSavemgr.add(savename, time, file);
                lastload = file;
                viewSetMessage("Game saved", -1, 10);
            } else {
                throw new FileNotFoundException(filename);
            }
        } catch (Exception e) {
            viewSetMessage("Game not saved! " + e, -1, 7);
        }

    }

    private static void save(OutputStream os, String savename, long time) throws Exception {
        SaveVersion(os, currentGdxSave);
        SaveInfo(os, savename, time);
        SaveGDXBlock(os);

        // Original save
        MySave110(os);
        StreamUtils.writeByte(os, cheatsOn ? 1 : 0);
        WarpSave(os);
        MirrorSave(os);
        SeqSave(os);
        EventSave(os);
        TriggersSave(os);
        PlayersSave(os, currentGdxSave);
        ActorsSave(os);
        StreamUtils.writeInt(os, gNextMap);
        StatsSave(os);
        ScreenSave(os);

        os.flush();
    }

    public static void SaveInfo(OutputStream os, String savename, long time) throws IOException {
        StreamUtils.writeLong(os, time);
        StreamUtils.writeString(os, savename, SAVENAME);
        StreamUtils.writeByte(os, pGameInfo.nDifficulty);
        StreamUtils.writeInt(os, pGameInfo.nEpisode);
        StreamUtils.writeInt(os, pGameInfo.nLevel);
        SaveScreenshot(os);
        SaveUserEpisodeInfo(os);
    }

    public static void SaveGDXBlock(OutputStream os) throws IOException {
        ByteBuffer gdxDataBuffer = ByteBuffer.allocate(SAVEGDXDATA);

        gdxDataBuffer.put((byte) pGameInfo.nEnemyDamage);
        gdxDataBuffer.put((byte) pGameInfo.nEnemyQuantity);
        gdxDataBuffer.put((byte) pGameInfo.nDifficulty); // XXX
        gdxDataBuffer.put((byte) (pGameInfo.nPitchforkOnly ? 1 : 0));
        gdxDataBuffer.put((byte) (gInfiniteAmmo ? 1 : 0));

        StreamUtils.writeBytes(os, gdxDataBuffer.array());
    }

    public static void SaveUserEpisodeInfo(OutputStream os) throws IOException {
        StreamUtils.writeBoolean(os, mUserFlag == UserFlag.Addon);
        if (mUserFlag == UserFlag.Addon) {
            if (currentEpisode != null && currentEpisode.iniFile != null) {
                EpisodeEntry episodeEntry = currentEpisode.iniFile.getEpisodeEntry();
                boolean isPacked = episodeEntry.isPackageEpisode();
                StreamUtils.writeBoolean(os, isPacked);
                StreamUtils.writeDataString(os, episodeEntry.getFileEntry().getRelativePath().toString());

                if (isPacked) {
                    StreamUtils.writeDataString(os, episodeEntry.getIniFile().getName());
                }
            } else {
                StreamUtils.writeBoolean(os, false); // packed
                StreamUtils.writeInt(os, 0); // name length
            }
        }
    }

    public static void SaveScreenshot(OutputStream os) throws IOException {
        StreamUtils.writeBytes(os, gGameScreen.captBuffer);
        gGameScreen.captBuffer = null;
    }

    public static void SaveVersion(OutputStream os, int nVersion) throws IOException {
        StreamUtils.writeString(os, savsign);
        StreamUtils.writeShort(os, nVersion);
        StreamUtils.writeInt(os, 4); // nBuild 4 - plasma
    }

    public static void MySave110(OutputStream os) throws IOException {
        Renderer renderer = game.getRenderer();
        GameInfoSave(os);

        Board board = boardService.getBoard();
        int numsectors = boardService.getSectorCount();
        int numwalls = boardService.getWallCount();
        int numsprites = boardService.getSpriteCount();

        StreamUtils.writeInt(os, numsectors);
        StreamUtils.writeInt(os, numwalls);
        StreamUtils.writeInt(os, numsprites);

        for (int i = 0; i < numsectors + 1; i++) { // and mirror sector
            boardService.getSector(i).writeObject(os);
        }

        for (int i = 0; i < numwalls + 4; i++) { // and mirror walls
            boardService.getWall(i).writeObject(os);
        }

        List<Sprite> sprites = board.getSprites();
        for (Sprite spr : sprites) {
            spr.writeObject(os);

            // sprite extra
            if (spr.getStatnum() < kStatFree && spr.getExtra() >= 0) {
                XSPRITE pXSprite = boardService.getXSprite(spr.getExtra());

                pXSprite.writeObject(os);
                pXSprite.getSpriteHit().writeObject(os);
                pXSprite.getSeqInst().writeObject(os);
                pXSprite.getDudeExtra().writeObject(os);
            }
        }

        StreamUtils.writeInt(os, bseed);
        StreamUtils.writeByte(os, 0); // parallaxtype
        StreamUtils.writeByte(os, 0); // showinvisibility ? 1 : 0
        StreamUtils.writeInt(os, 0); // parallaxyoffs
        StreamUtils.writeInt(os, renderer.getParallaxScale());
        StreamUtils.writeInt(os, visibility);
        StreamUtils.writeInt(os, parallaxvisibility);

        for (int i = 0; i < MAXPSKYTILES; i++) {
            StreamUtils.writeShort(os, pskyoff[i]);
        }

        StreamUtils.writeShort(os, pskybits);
        show2dsector.writeObject(os);
        show2dwall.writeObject(os);
        show2dsprite.writeObject(os);
        StreamUtils.writeByte(os, automapping);
        StreamUtils.writeInt(os, gFrameClock);
        StreamUtils.writeInt(os, gTicks);
        StreamUtils.writeInt(os, gFrame);
        StreamUtils.writeInt(os, engine.getTotalClock());
        StreamUtils.writeByte(os, game.gPaused ? 1 : 0);

        for (int i = 0; i < numwalls; i++) {
            StreamUtils.writeInt(os, (int) kwall[i].x);
            StreamUtils.writeInt(os, (int) kwall[i].y);
        }

        for (int i = 0; i < numsectors; i++) {
            StreamUtils.writeInt(os, secFloorZ[i]);
        }
        for (int i = 0; i < numsectors; i++) {
            StreamUtils.writeInt(os, secCeilZ[i]);
        }
        for (int i = 0; i < numsectors; i++) {
            StreamUtils.writeInt(os, (int) floorVel[i]);
        }
        for (int i = 0; i < numsectors; i++) {
            StreamUtils.writeInt(os, (int) ceilingVel[i]);
        }

        StreamUtils.writeShort(os, pHitInfo.hitsect);
        StreamUtils.writeShort(os, pHitInfo.hitwall);
        StreamUtils.writeShort(os, pHitInfo.hitsprite);
        StreamUtils.writeInt(os, pHitInfo.hitx);
        StreamUtils.writeInt(os, pHitInfo.hity);
        StreamUtils.writeInt(os, pHitInfo.hitz);
        StreamUtils.writeByte(os, mUserFlag == UserFlag.UserMap ? 1 : 0);
        StreamUtils.writeString(os, "Copyright 1997 Monolith Productions.", 128);

        for (int i = 0; i < kMaxXWalls; i++) {
            StreamUtils.writeShort(os, (short) nextXWall[i]);
        }

        for (int i = 0; i < kMaxXSectors; i++) {
            StreamUtils.writeShort(os, (short) nextXSector[i]);
        }

        for (int i = 0; i < numwalls; i++) {
            if (boardService.getWall(i).getExtra() > 0) {
                xwall[boardService.getWall(i).getExtra()].writeObject(os);
            }
        }

        for (int i = 0; i < numsectors; i++) {
            if (boardService.getSector(i).getExtra() > 0) {
                xsector[boardService.getSector(i).getExtra()].writeObject(os);
            }
        }

        StreamUtils.writeInt(os, gSkyCount);
        StreamUtils.writeByte(os, engine.getPaletteManager().isFogMode() ? 1 : 0);
    }

    public static void WarpSave(OutputStream os) throws IOException {
        for (int i = 0; i < kMaxPlayers; i++) {
            StreamUtils.writeInt(os, gStartZone[i].x);
            StreamUtils.writeInt(os, gStartZone[i].y);
            StreamUtils.writeInt(os, gStartZone[i].z);
            StreamUtils.writeShort(os, (short) gStartZone[i].sector);
            StreamUtils.writeShort(os, (short) gStartZone[i].angle);
        }

        for (int i = 0; i < boardService.getSectorCount(); i++) {
            StreamUtils.writeInt(os, (short) gUpperLink[i]);
            StreamUtils.writeInt(os, (short) gLowerLink[i]);
        }
    }

    public static void MirrorSave(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, mirrorcnt);
        StreamUtils.writeInt(os, MirrorSector);

        for (int i = 0; i < MAXMIRRORS; i++) {
            StreamUtils.writeShort(os, (short) MirrorType[i]);
            StreamUtils.writeInt(os, MirrorLower[i]);
            StreamUtils.writeInt(os, MirrorX[i]);
            StreamUtils.writeInt(os, MirrorY[i]);
            StreamUtils.writeInt(os, MirrorZ[i]);
            StreamUtils.writeInt(os, MirrorUpper[i]);
        }

        for (int i = 0; i < 4; i++) {
            StreamUtils.writeInt(os, MirrorWall[i]);
        }
    }

    public static void SeqSave(OutputStream os) throws IOException {
        for (int i = 0; i < kMaxXWalls; i++) {
            siWall[i].writeObject(os);
        }
        for (int i = 0; i < kMaxXWalls; i++) {
            siMasked[i].writeObject(os);
        }
        for (int i = 0; i < kMaxXSectors; i++) {
            siCeiling[i].writeObject(os);
        }
        for (int i = 0; i < kMaxXSectors; i++) {
            siFloor[i].writeObject(os);
        }

        StreamUtils.writeInt(os, activeList.getSize());
        for (int i = 0; i < activeList.getSize(); i++) {
            SeqInst pInst = activeList.getInst(i);
            if (pInst instanceof WallInst) {
                StreamUtils.writeByte(os, SS_WALL);
            } else if (pInst instanceof MaskedWallInst) {
                StreamUtils.writeByte(os, SS_MASKED);
            } else if (pInst instanceof FloorInst) {
                StreamUtils.writeByte(os, SS_FLOOR);
            } else if (pInst instanceof CeilingInst) {
                StreamUtils.writeByte(os, SS_CEILING);
            } else if (pInst instanceof SpriteInst) {
                StreamUtils.writeByte(os, SS_SPRITE);
            } else {
                StreamUtils.writeByte(os, (byte) -1);
            }
            StreamUtils.writeShort(os, activeList.getIndex(i));
        }
    }

    public static void EventSave(OutputStream os) throws IOException {
        eventQ.writeObject(os);

        for (int i = 0; i < kMaxChannels; i++) {
            StreamUtils.writeInt(os, getEvent(rxBucket[i].index, rxBucket[i].type, 0, 0));
        }

        for (int i = 0; i <= kMaxID; i++) {
            StreamUtils.writeShort(os, bucketHead[i]);
        }
    }

    public static void TriggersSave(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, gBusyCount);

        for (int i = 0; i < kMaxBusyArray; i++) {
            StreamUtils.writeInt(os, gBusy[i].nIndex);
            StreamUtils.writeInt(os, gBusy[i].nDelta);
            StreamUtils.writeInt(os, gBusy[i].nBusy);
            StreamUtils.writeByte(os, (byte) gBusy[i].busyProc);
        }

        for (int i = 0; i < boardService.getSectorCount(); i++) {
            StreamUtils.writeInt(os, secPath[i]);
        }
    }

    public static void PlayersSave(OutputStream os, int nVersion) throws IOException {
        for (int i = 0; i < kMaxPlayers; i++) {
            StreamUtils.writeInt(os, nTeamCount[i]);
        }
        StreamUtils.writeInt(os, numplayers);

        for (int i = 0; i < kMaxPlayers; i++) {
            StreamUtils.writeByte(os, game.net.gProfile[i].autoaim ? 1 : 0);
            if (nVersion >= gdxSave + 2) {
                StreamUtils.writeByte(os, game.net.gProfile[i].slopetilt ? 1 : 0);
            }
            StreamUtils.writeByte(os, game.net.gProfile[i].skill);
            StreamUtils.writeString(os, game.net.gProfile[i].name, 15);
        }

        for (int i = 0; i < kMaxPlayers; i++) {
            gPlayer[i].pInput.writeObject(os, nVersion);
            gPlayer[i].setVersion(nVersion).writeObject(os);
        }
    }

    public static void ActorsSave(OutputStream os) throws IOException {
        for (int i = 0; i < boardService.getSectorCount(); i++) {
            StreamUtils.writeShort(os, (short) gSectorExp[i]);
        }

        for (int i = 0; i < kMaxXWalls; i++) {
            StreamUtils.writeShort(os, (short) gWallExp[i]);
        }

        StreamUtils.writeInt(os, gPost.getSize());
        for (int i = 0; i < gPost.getSize(); i++) {
            StreamUtils.writeInt(os, gPost.get(i).nSprite);
            StreamUtils.writeInt(os, gPost.get(i).nStatus);
        }
    }

    public static void GameInfoSave(OutputStream os) throws IOException {
        StreamUtils.writeByte(os, pGameInfo.nGameType);
        StreamUtils.writeByte(os, pGameInfo.nDifficulty);
        StreamUtils.writeInt(os, pGameInfo.nEpisode);
        StreamUtils.writeInt(os, pGameInfo.nLevel);
        if (pGameInfo.zLevelName instanceof FileEntry) {
            StreamUtils.writeString(os, ((FileEntry) pGameInfo.zLevelName).getRelativePath().toString(), 144); // e1m1.map
        } else {
            StreamUtils.writeString(os, pGameInfo.zLevelName.getName(), 144); // e1m1.map
        }
        StreamUtils.writeString(os, pGameInfo.zLevelSong, 144);
        StreamUtils.writeInt(os, pGameInfo.nTrackNumber);

        StreamUtils.writeInt(os, (int) pGameInfo.uMapCRC);
        StreamUtils.writeByte(os, pGameInfo.nMonsterSettings);
        StreamUtils.writeInt(os, pGameInfo.uGameFlags);
        StreamUtils.writeInt(os, pGameInfo.uNetGameFlags);
        StreamUtils.writeByte(os, pGameInfo.nWeaponSettings);
        StreamUtils.writeByte(os, pGameInfo.nItemSettings);
        StreamUtils.writeByte(os, pGameInfo.nRespawnSettings);
        StreamUtils.writeByte(os, pGameInfo.nTeamSettings);
        StreamUtils.writeInt(os, pGameInfo.nMonsterRespawnTime);
        StreamUtils.writeInt(os, pGameInfo.nWeaponRespawnTime);
        StreamUtils.writeInt(os, pGameInfo.nItemRespawnTime);
        StreamUtils.writeInt(os, pGameInfo.nSpecialRespawnTime);
    }

    public static void StatsSave(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, totalSecrets);
        StreamUtils.writeInt(os, foundSecret);
        StreamUtils.writeInt(os, superSecrets);
        StreamUtils.writeInt(os, totalKills);
        StreamUtils.writeInt(os, kills);
    }

    public static void ScreenSave(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, deliriumTilt);
        StreamUtils.writeInt(os, deliriumTurn);
        StreamUtils.writeInt(os, deliriumPitch);
    }

    public static void quicksave() {
        if (numplayers > 1 || kFakeMultiplayer) {
            return;
        }
        if (gMe.pXsprite.getHealth() != 0) {
            gQuickSaving = true;
        }
    }

    // LOAD GAME

    public static boolean checkfile(InputStream is) throws IOException {
        /*
         * 266 - v1.10 267 - v1.11 277 - v1.21
         */
        int saveHeader = checkSave(is);
        int nVersion = (saveHeader & 0xFFFF);

        // !277 - Incompatible version of saved game found!
        // int nBuild = saveHeader >> 16;
        // !4 - Saved game is from another release of Blood

        if (nVersion < gdxSave) {
            Console.out.println("Dos saved game found, version: " + nVersion);
        }

        if (nVersion != currentGdxSave) { // && nVersion != 0x115 && nVersion != 0x100) {
            return false;
        }

        return loader.load(is);
    }

    public static boolean canLoad(FileEntry fil) {
        if (fil.exists()) {
            try (InputStream is = fil.getInputStream()) {
                int nVersion = checkSave(is) & 0xFFFF;

                if (nVersion != currentGdxSave) {
                    if (nVersion >= gdxSave) {
                        final BloodIniFile addon = loader.LoadGDXHeader(is);

                        if (loader.safeGameInfo.nLevel <= kMaxMap && loader.safeGameInfo.nEpisode < kMaxEpisode && loader.safeGameInfo.nDifficulty >= 0 && loader.safeGameInfo.nDifficulty < 5 && !loader.gForceMap) {
                            MenuCorruptGame menu = (MenuCorruptGame) game.menu.mMenus[CORRUPTLOAD];
                            menu.setRunnable(() -> {
                                int nEpisode = loader.safeGameInfo.nEpisode;
                                int nLevel = loader.safeGameInfo.nLevel;
                                int nSkill = loader.safeGameInfo.nDifficulty;
                                gGameScreen.newgame(false, addon, nEpisode, nLevel, nSkill, nSkill, nSkill, false);
                            });
                            game.menu.mOpen(menu, -1);
                        }
                    }
                }
                return nVersion == currentGdxSave;
            } catch (Exception ignored) {
            }
        }
        return false;
    }

    public static void quickload() {
        if (numplayers > 1) {
            return;
        }
        final FileEntry loadFile = game.pSavemgr.getLast();

        if (canLoad(loadFile)) {
            game.changeScreen(gLoadingScreen.setTitle(loadFile.getName()));
            gLoadingScreen.init(() -> {
                if (!loadgame(loadFile)) {
                    game.setPrevScreen();
                    game.pNet.ready2send = true;
                }
            });
        }
    }

    public static int lsReadLoadData(FileEntry file) {
        if (file.exists()) {
            ArtEntry pic = engine.getTile(SaveManager.Screenshot);
            if (!(pic instanceof DynamicArtEntry) || !pic.exists()) {
                pic = engine.allocatepermanenttile(SaveManager.Screenshot, 320, 200);
            }

            try (InputStream is = file.getInputStream()) {
                int nVersion = checkSave(is) & 0xFFFF;
                lsInf.clear();

                if (nVersion == currentGdxSave) {
                    lsInf.date = game.date.getDate(StreamUtils.readLong(is));
                    StreamUtils.skip(is, SAVENAME);
                    lsInf.read(is);
                    if (is.available() <= SAVESCREENSHOTSIZE) {
                        return -1;
                    }

                    StreamUtils.readBytes(is, pic.getBytes(), SAVESCREENSHOTSIZE);
                    ((DynamicArtEntry) pic).invalidate();

                    boolean gUserEpisode = StreamUtils.readBoolean(is);
                    if (gUserEpisode) {
                        boolean isPacked = StreamUtils.readBoolean(is);
                        String fullname = StreamUtils.readDataString(is);
                        String ininame;
                        if (isPacked) {
                            String ext = FileUtils.getExtension(fullname);
                            ininame = ext + ":" + game.getFilename(StreamUtils.readDataString(is));
                        } else {
                            ininame = game.getFilename(fullname);
                        }

                        if (!ininame.isEmpty()) {
                            lsInf.iniName = "File: " + ininame;
                        }
                    }

                    int nEnemyDamage = StreamUtils.readUnsignedByte(is) + 1;
                    int nEnemyQuantity = StreamUtils.readUnsignedByte(is) + 1;
                    int nDifficulty = StreamUtils.readUnsignedByte(is) + 1;
                    boolean nPitchforkOnly = StreamUtils.readBoolean(is);

                    if (lsInf.skill != nEnemyDamage || lsInf.skill != nEnemyQuantity || lsInf.skill != nDifficulty || nPitchforkOnly) {
                        lsInf.skill = 6;
                    }

                    lsInf.update();
                    return 1;
                } else {
                    lsInf.info = "Incompatible ver. " + nVersion + " != " + currentGdxSave;
                    return -1;
                }
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }

        lsInf.clear();
        return -1;
    }

    public static void LoadGameInfo() {
        pGameInfo.copy(loader.safeGameInfo);
    }

    public static int checkSave(InputStream is) throws IOException {
        String signature = StreamUtils.readString(is, 4);
        if (!signature.equals(savsign)) {
            return 0;
        }

        int nVersion = StreamUtils.readShort(is);
        int nBuild = StreamUtils.readInt(is);
        return nVersion | nBuild << 16;
    }

    public static boolean loadgame(FileEntry fil) {
        if (fil.exists()) {
            try (InputStream is = fil.getInputStream()) {
                Console.out.println("debug: start loadgame()", OsdColor.BLUE);

                boolean status = checkfile(is);
                if (status) {
                    load();
                    if (lastload == null || !lastload.exists()) {
                        lastload = fil;
                    }

                    if (loader.getMessage() != null) {
                        viewSetMessage(loader.getMessage(), -1, 7);
                    }
                    return true;
                }

                viewSetMessage("Incompatible version of saved game found!", -1, 7);
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                Console.out.println(e.toString(), OsdColor.RED);
            }
        }

        viewSetMessage("Can't access to file or file not found!", -1, 7);
        return false;
    }

    public static boolean load() {
        boardService.dbInit();
        sndStopAllSamples();
        sfxKillAll3DSounds();
        ambStopAll();
        seqKillAll();

        resetQuotes();
        trInitStructs();

        LoadGDXBlock();
        MyLoad();
        WarpLoad();
        MirrorLoad();
        SeqLoad();
        EventLoad();
        TriggersLoad();
        PlayersLoad();
        ActorsLoad(); // throw
        GameInfoLoad();
        StatsLoad();
        ScreenLoad();
        LoadUserEpisodeInfo(); // throw

        cheatsOn = loader.cheatsOn;
        gInfiniteAmmo = loader.gInfiniteAmmo;

        InitMirrorTiles();

        loadMapInfo(pGameInfo.nEpisode, pGameInfo.nLevel);

        if (mUserFlag == UserFlag.UserMap || currentEpisode.gMapInfo[pGameInfo.nLevel] == null || currentEpisode.gMapInfo[pGameInfo.nLevel].Title == null) {
            boardfilename = gUserMapInfo.Title = pGameInfo.zLevelName.getName();
            gUserMapInfo.MapName = pGameInfo.zLevelName;
            gUserMapInfo.Song = null;
            gUserMapInfo.Track = 0;
        } else {
            boardfilename = currentEpisode.gMapInfo[pGameInfo.nLevel].Title;
        }

        if (!game.isCurrentScreen(gGameScreen) && !game.isCurrentScreen(gDemoScreen)) {
            engine.getPaletteManager().loadPLUs(loader.gFogMode);
        }

        InitSectorFX();
        sfxResetListener();
        ((BloodNetwork) game.pNet).PredictReset();

        game.doPrecache(() -> {
            ambPrepare();

            sndPlayMusic();

            viewSetMessage("Game loaded", -1, 10);
            gTicks = 0;
            gFrame = 0;

            scrReset();
            gViewPos = 0;
            gViewMode = kView3D;

            engine.getTimer().reset();
            game.gPaused = false;

            game.changeScreen(gGameScreen);
            game.pNet.ResetTimers();
            game.pNet.WaitForAllPlayers(0);
            game.pNet.ready2send = true;

            game.nNetMode = NetMode.Single;

            SOUND.setReverb(false, 0);

            PaletteView = kPalNormal;
            engine.getPaletteManager().setPalette(PaletteView);
            if (powerupCheck(gMe, kItemDivingSuit - kItemBase) != 0) {
                SOUND.setReverb(true, 0.2f);
            }

            if (powerupCheck(gMe, kItemReflectiveShots - kItemBase) != 0) {
                SOUND.setReverb(true, 0.4f);
            }

            Console.out.println("debug: end loadgame()", OsdColor.BLUE);
        });

        return true;
    }

    public static void LoadGDXBlock() {
        pGameInfo.nEnemyDamage = loader.safeGameInfo.nEnemyDamage;
        pGameInfo.nEnemyQuantity = loader.safeGameInfo.nEnemyQuantity;
        pGameInfo.nDifficulty = loader.safeGameInfo.nDifficulty;
        pGameInfo.nPitchforkOnly = loader.safeGameInfo.nPitchforkOnly;
        gInfiniteAmmo = loader.gInfiniteAmmo;
    }

    public static void MyLoad() {
        LoadGameInfo();

//        boardfilename = loader.boardfilename;

        BloodBoard board = new BloodBoard(null, Arrays.copyOf(loader.sector.toArray(new Sector[0]), loader.numsectors + 1), Arrays.copyOf(loader.wall.toArray(new Wall[0]), loader.numwalls + 4), loader.sprite);
        board.setSecrets(loader.totalSecrets);
        board.setVisibility(loader.visibility);
        board.setSkyBits(loader.pskybits);
//        board.setSongId(gSongId);
//        board.setMapCRC(uMapCRC);
//        board.setMapRev(gMapRev);
        board.setParallaxType(parallaxtype);
        board.setSkyOffset(pskyoff);
        boardService.setBoard(board);
        LEVELS.autoTotalSecrets = board.getSecrets();

        bseed = loader.randomseed;
//		showinvisibility = loader.showinvisibility;

        Renderer renderer = game.getRenderer();
        renderer.setParallaxScale(loader.parallaxyscale);

        visibility = loader.visibility;
        parallaxvisibility = loader.parallaxvisibility;
        DB.gVisibility = Engine.visibility;


        System.arraycopy(loader.pskyoff, 0, pskyoff, 0, MAXPSKYTILES);
        pskybits = loader.pskybits;

        Arrays.fill(zeropskyoff, (short) 0);
        System.arraycopy(pskyoff, 0, zeropskyoff, 0, MAXPSKYTILES);
        show2dsector.copy(loader.show2dsector);
        show2dwall.copy(loader.show2dwall);
        show2dsprite.copy(loader.show2dsprite);
        automapping = loader.automapping;

        gFrameClock = loader.gFrameClock;
        gTicks = loader.gTicks;
        gFrame = loader.gFrame;
        game.pEngine.getTimer().setTotalClock(loader.gGameClock);

        game.gPaused = loader.gPaused;

        for (int i = 0; i < loader.kwall.size; i++) {
            kwall[i].set(loader.kwall.items[i]);
        }

        int numsectors = loader.numsectors;
        System.arraycopy(loader.secFloorZ.items, 0, secFloorZ, 0, numsectors);
        System.arraycopy(loader.secCeilZ.items, 0, secCeilZ, 0, numsectors);
        System.arraycopy(loader.floorVel.items, 0, floorVel, 0, numsectors);
        System.arraycopy(loader.ceilingVel.items, 0, ceilingVel, 0, numsectors);

        pHitInfo.hitsect = loader.safeHitInfo.hitsect;
        pHitInfo.hitwall = loader.safeHitInfo.hitwall;
        pHitInfo.hitsprite = loader.safeHitInfo.hitsprite;
        pHitInfo.hitx = loader.safeHitInfo.hitx;
        pHitInfo.hity = loader.safeHitInfo.hity;
        pHitInfo.hitz = loader.safeHitInfo.hitz;

        mUserFlag = UserFlag.None;
        if (loader.gForceMap) {
            mUserFlag = UserFlag.UserMap;
        }

        System.arraycopy(loader.nextXWall, 0, nextXWall, 0, kMaxXWalls);
        System.arraycopy(loader.nextXSector, 0, nextXSector, 0, kMaxXSectors);

        for (int nSprite = 0; nSprite < boardService.getSpriteCount(); nSprite++) {
            BloodSprite spr = boardService.getSprite(nSprite);
            if (spr == null) {
                continue;
            }

            int nXSprite = spr.getExtra();
            if (spr.getStatnum() < kStatFree && spr.getExtra() >= 0) {
                XSPRITE pXSprite = boardService.setXSprite(nXSprite);
                pXSprite.copy(loader.xspriteMap.get(nXSprite));
                spr.setXSprite(pXSprite);
                pXSprite.getDudeExtra().setCumulDamage(0);
            }
        }

        for (int i = 0; i < kMaxXWalls; i++) {
            xwall[i].copy(loader.xwall[i]);
        }
        for (int i = 0; i < kMaxXSectors; i++) {
            xsector[i].copy(loader.xsector[i]);
        }

        gSkyCount = loader.gSkyCount;

        gNoClip = loader.gNoClip;
        gFullMap = loader.gFullMap;
    }

    public static void WarpLoad() {
        for (int i = 0; i < kMaxPlayers; i++) {
            gStartZone[i] = new ZONE();
            gStartZone[i].x = loader.gStartZone[i].x;
            gStartZone[i].y = loader.gStartZone[i].y;
            gStartZone[i].z = loader.gStartZone[i].z;
            gStartZone[i].sector = loader.gStartZone[i].sector;
            gStartZone[i].angle = loader.gStartZone[i].angle;
        }
        System.arraycopy(loader.gUpperLink.items, 0, gUpperLink, 0, loader.numsectors);
        System.arraycopy(loader.gLowerLink.items, 0, gLowerLink, 0, loader.numsectors);
    }

    public static void MirrorLoad() {
        mirrorcnt = loader.mirrorcnt;
        MirrorSector = loader.MirrorSector;
        for (int i = 0; i < MAXMIRRORS; i++) {
            MirrorType[i] = loader.MirrorType[i];
            MirrorLower[i] = loader.MirrorLower[i];
            MirrorX[i] = loader.MirrorX[i];
            MirrorY[i] = loader.MirrorY[i];
            MirrorZ[i] = loader.MirrorZ[i];
            MirrorUpper[i] = loader.MirrorUpper[i];
        }
        System.arraycopy(loader.MirrorWall, 0, MirrorWall, 0, 4);
    }

    public static void SeqLoad() {
        for (int i = 0; i < kMaxXWalls; i++) {
            siWall[i].copy(loader.siWall[i]);
        }
        for (int i = 0; i < kMaxXWalls; i++) {
            siMasked[i].copy(loader.siMasked[i]);
        }
        for (int i = 0; i < kMaxXSectors; i++) {
            siCeiling[i].copy(loader.siCeiling[i]);
        }
        for (int i = 0; i < kMaxXSectors; i++) {
            siFloor[i].copy(loader.siFloor[i]);
        }
        activeList.set(loader.actListType, loader.actListIndex, loader.activeCount);
    }

    public static void EventLoad() {
        eventQueryInit(IsOriginalDemo());

        for (int i = 0; i < kMaxChannels; i++) {
            if (rxBucket[i] == null) {
                rxBucket[i] = new RXBUCKET();
            } else {
                rxBucket[i].flush();
            }
        }

        eventQ.flush();
        for (int i = 0; i < loader.fNodeCount; i++) {
            PriorityItem item = loader.qEventItems[i];
            eventQ.Insert(item.priority, item.event);
        }

        for (int i = 0; i < kMaxChannels; i++) {
            rxBucket[i].index = loader.rxBucketIndex[i];
            rxBucket[i].type = loader.rxBucketType[i];
        }

        System.arraycopy(loader.bucketHead, 0, bucketHead, 0, kMaxID + 1);
    }

    public static void TriggersLoad() {
        gBusyCount = loader.gBusyCount;
        for (int i = 0; i < kMaxBusyArray; i++) {
            gBusy[i].nIndex = loader.gBusy[i].nIndex;
            gBusy[i].nDelta = loader.gBusy[i].nDelta;
            gBusy[i].nBusy = loader.gBusy[i].nBusy;
            gBusy[i].busyProc = loader.gBusy[i].busyProc;
        }

        System.arraycopy(loader.secPath.items, 0, secPath, 0, loader.numsectors);
    }

    public static void PlayersLoad() {
        System.arraycopy(loader.nTeamCount, 0, nTeamCount, 0, kMaxPlayers);
        numplayers = (short) loader.gNetPlayers;

        if (numplayers >= 0) {
            System.arraycopy(loader.connectpoint2, 0, connectpoint2, 0, numplayers);
        }

        for (int i = 0; i < kMaxPlayers; i++) {
            if (loader.autoaim[i] != -1) {
                game.net.gProfile[i].autoaim = loader.autoaim[i] == 1;
            }
            if (loader.slopetilt[i] != -1) {
                game.net.gProfile[i].slopetilt = loader.slopetilt[i] == 1;
            }
            if (loader.skill[i] != -1) {
                game.net.gProfile[i].skill = loader.skill[i];
            }
            if (loader.name[i] != null) {
                game.net.gProfile[i].name = loader.name[i];
            }
        }

        for (int i = 0; i < kMaxPlayers; i++) {
            loader.safePlayer[i].pInput.Copy(gPlayer[i].pInput);
            gPlayer[i].copy(loader.safePlayer[i]);
        }

        for (int i = 0; i < numplayers; i++) {
            gPlayer[i].pSprite = boardService.getSprite(gPlayer[i].nSprite);
            gPlayer[i].pXsprite = boardService.getXSprite(gPlayer[i].pSprite.getExtra());
            gPlayer[i].pDudeInfo = dudeInfo[gPlayer[i].pSprite.getLotag() - kDudeBase];
        }
        gViewIndex = myconnectindex;
    }

    public static void ActorsLoad() throws WarningException {
        System.arraycopy(loader.gSectorExp.items, 0, gSectorExp, 0, loader.numsectors);
        System.arraycopy(loader.gWallExp.items, 0, gWallExp, 0, kMaxXWalls);

        for (int i = 0; i < loader.gPostCount; i++) {
            POSTPONE pPost = gPost.obtain();
            pPost.nSprite = loader.gPost.items[i].nSprite;
            pPost.nStatus = loader.gPost.items[i].nStatus;
        }

        actInit(true, IsOriginalDemo());
        for (int p = 0; p < numplayers; p++) {
            playerSetRace(gPlayer[p], gPlayer[p].nLifeMode);
            gPlayer[p].ang = gPlayer[p].pSprite.getAng();
        }
    }

    public static void GameInfoLoad() {
        gNextMap = loader.gNextMap;
        LoadGameInfo();
    }

    public static void StatsLoad() {
        totalSecrets = loader.totalSecrets;
        foundSecret = loader.foundSecret;
        superSecrets = loader.superSecrets;
        totalKills = loader.totalKills;
        kills = loader.kills;
    }

    public static void ScreenLoad() {
        deliriumTilt = loader.deliriumTilt;
        deliriumTurn = loader.deliriumTurn;
        deliriumPitch = loader.deliriumPitch;
    }

    public static void LoadUserEpisodeInfo() throws WarningException {
        if (loader.gUserEpisode) {
            mUserFlag = UserFlag.Addon;
        }

        if (mUserFlag == UserFlag.Addon) {
            BloodIniFile ini = loader.addon;
            checkEpisodeResources(ini);
            getEpisodeInfo(gUserEpisodeInfo, ini);
        } else {
            resetEpisodeResources();
        }
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import com.badlogic.gdx.utils.ByteArray;
import org.jetbrains.annotations.NotNull;
import ru.m210projects.Blood.Factory.*;
import ru.m210projects.Blood.Menus.*;
import ru.m210projects.Blood.Screens.*;
import ru.m210projects.Blood.Types.EpisodeInfo;
import ru.m210projects.Blood.Types.PLOCATION;
import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Blood.filehandlers.EpisodeEntry;
import ru.m210projects.Blood.filehandlers.scripts.BloodDef;
import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Pattern.BuildFactory;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.LogSender;
import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Script.Maphack;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.*;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.filehandle.grp.GrpFile;
import ru.m210projects.Build.filehandle.rff.RffFile;
import ru.m210projects.Build.osd.CommandResponse;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.commands.OsdCallback;
import ru.m210projects.Build.osd.commands.OsdCommand;
import ru.m210projects.Build.settings.GameConfig;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static ru.m210projects.Blood.AI.Ai.aiInit;
import static ru.m210projects.Blood.Actor.actSpawnDude;
import static ru.m210projects.Blood.Actor.gNoEnemies;
import static ru.m210projects.Blood.DB.kDudeBase;
import static ru.m210projects.Blood.DB.kDudeMax;
import static ru.m210projects.Blood.Factory.BloodMenuHandler.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.*;
import static ru.m210projects.Blood.LOADSAVE.*;
import static ru.m210projects.Blood.PLAYER.gPrevView;
import static ru.m210projects.Blood.ResourceHandler.*;
import static ru.m210projects.Blood.SOUND.sndInit;
import static ru.m210projects.Blood.Tile.tileInit;
import static ru.m210projects.Blood.Trig.trigInit;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DemoUtils.checkDemoEntry;
import static ru.m210projects.Blood.Types.GAMEINFO.EndOfGame;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.SeqInit;
import static ru.m210projects.Blood.VERSION.SHAREWARE;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Blood.Weapon.WeaponInit;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.NORMAL;

public class Main extends BuildGame {
    public static final String appdef = "bloodgdx.def";

    /*
     * TODO:
     * Not from original:
     * 1. это тип 456. это электрический спрайт, который можно вкл и выкл, но дело в том, что когда его выключаешь, он становится блочным, даже если до этого не был таковым
     * 2. сочитание клавиш для рук зверя
     * 3. плескания лавы у цербера
     * 4. церберу давку как у чернобога
     * 5. культистам присядать если потолок низкий
     * 6. не забудь добавить Dudelockout для этого флага
     * 7. в порте динамит основной огонь - он тоже должен взрываться в зависимости от того, сколько осталось времени
     * 8. headspritestat[kStatMissile] вернуть hitscan fix
     * 9. Ракетница под водой жарит
     * 10. Кровь и пули на поворотных секторах
     * 11. Не стреляет томмиган из воды отключить
     * 12. nearsectors искать проекцию на стену сектора
     * 13. Квоты для итемов
     *
     * БАГИ ОРИГИНАЛЬНОЙ ИГРЫ: БАГИ КАРТ (вряд ли можно пофиксить без редактирования
     * карты): In E2M4: The Overlooked Hotel, the player can pass through the gate
     * before the end of the level, by crawling, without the moon key. In E2M5: The
     * Haunting, there is a fireplace that contains a secret. Each time you visit
     * the secret, it credits you, artificially inflating the total at the end of
     * the level. In E5M2: Old Opera House, there is a secret which will not
     * increase the secrets count, possibly due to a bug, so your best possible
     * score will be a total of 3/4 secrets at the end of the level.
     */

    public static MenuScreen gMenuScreen;
    public static GameScreen gGameScreen;
    public static DemoScreen gDemoScreen;
    public static LoadingScreen gLoadingScreen;
    public static LogoScreen gLogoScreen;
    public static CutsceneScreen gCutsceneScreen;
    public static NetScreen gNetScreen;
    public static StatisticScreen gStatisticScreen;
    public static DisconnectScreen gDisconnectScreen;
    public static UserFlag mUserFlag = UserFlag.None;
    public static Main game;
    public static BloodEngine engine;
    public static BloodBoardService boardService;
    public static Config cfg;
    public String sversion;
    public BloodMenuHandler menu;
    public final Runnable rMenu = new Runnable() {
        @Override
        public void run() {
            resetState();
            Directory gameDir = cache.getGameDirectory();
            if (cfg.gDemoSeq == 0 || !gDemoScreen.showDemo(gameDir)) {
                changeScreen(gMenuScreen);
            }
        }
    };
    private final Runnable nextLogo = () -> {
        if (cfg.showCutscenes && gCutsceneScreen.init("gti.smk", "gti.wav")) {
            changeScreen(gCutsceneScreen.setCallback(rMenu));
        } else {
            changeScreen(gLogoScreen.setTile(2052).setCallback(rMenu));
        }
    };
    public BloodNetwork net;

    public Main(List<String> args, GameConfig bcfg, String appname, String sversion, boolean isDemo, boolean isRelease) throws IOException {
        super(args, bcfg, appname, sversion, isRelease);
        game = this;
        this.sversion = sversion;
        cfg = (Config) bcfg;
        SHAREWARE = isDemo;
        MAXWALLS = MAXWALLSV8;
        MAXSECTORS = MAXSECTORSV8;
    }

    public Main(List<String> args, GameConfig bcfg, String appname, String sversion, boolean isDemo) throws IOException {
        this(args, bcfg, appname, sversion, isDemo, true);
    }

    @Override
    public BuildFactory getFactory() {
        return new BloodFactory(this);
    }

    @Override
    public Cache createFileCache(Directory gameDirectory) {
        return new BloodCache(gameDirectory);
    }

    @Override
    public BloodCache getCache() {
        return (BloodCache) cache;
    }

    @Override
    public boolean init() throws Exception {
        net = (BloodNetwork) pNet;
        boardService = (BloodBoardService) engine.getBoardService();

        ConsoleInit();
        Console.out.println("Initializing sound system");
        sndInit();

        Console.out.println("Loading INI file");
        Directory gameDir = cache.getGameDirectory();
        MainINI = new BloodIniFile(new EpisodeEntry.File(gameDir.getEntry("blood.ini")));
        pGameInfo.copy(defGameInfo);

        tileInit();

        fire = new Fire();
        Console.out.println("Loading cosine table");
        trigInit();
        Console.out.println("Initializing view subsystem");
        viewInit();

        checkDemoEntry(gameDir);

        net.ResetNetwork();

        for (int i = 0; i < kMaxPlayers; i++) {
            gPlayer[i] = new PLAYER();
            gPrevView[i] = new PLOCATION();
        }

        gMe = gPlayer[myconnectindex];
        gViewIndex = myconnectindex;

        SeqInit();

        initEpisodeInfo(MainINI);
        getEpisodeInfo(gEpisodeInfo, MainINI);
        InitCutscenes();

        Console.out.println("Initializing def-scripts...");
        cache.loadGdxDef(baseDef, appdef, "bloodgdx.dat");

        if (cfg.isAutoloadFolder()) {
            Console.out.println("Initializing autoload folder");
            for (Entry file : gameDir.getDirectory(gameDir.getEntry("autoload"))) {
                FileEntry fileEntry = (FileEntry) file;
                switch (file.getExtension()) {
                    case "PK3":
                    case "ZIP": {
                        Group group = cache.newGroup(file);
                        Entry def = group.getEntry(appdef);
                        if (def.exists()) {
                            cache.addGroup(group, NORMAL); // HIGH?
                            baseDef.loadScript(file.getName(), def);
                        } else if (group.getSize() > 0) { // load it as a game resource
                            RffFile holder = new RffFile(group.getName());
                            searchEpisodeResources(group, holder);
                            cache.addGroup(holder, NORMAL);
                            InitGroupResources(getBaseDef(), group.getEntries());
                        }
                    }
                    break;
                    case "RFF":
                    case "GRP": {
                        if (cache.addGroup(file, NORMAL)) {
                            Group group = cache.getGroup(file.getName());
                            InitGroupResources(getBaseDef(), group.getEntries());
                        }
                        break;
                    }
                    case "DEF":
                        baseDef.loadScript(fileEntry.getRelativePath().toString(), fileEntry);
                        break;
                }
            }
        }

        FileEntry filgdx = gameDir.getEntry(appdef);
        if (filgdx.exists()) {
            baseDef.loadScript(filgdx);
        }
        this.setDefs(baseDef);

        viewHandInit();
        Console.out.println("Initializing weapon animations");
        WeaponInit();

        FindSaves(getUserDirectory());

        menu.mMenus[MAIN] = new MainMenu(this);
        menu.mMenus[GAME] = new MenuGame(this);
        menu.mMenus[DIFFICULTY] = new MenuDifficulty(this);
        menu.mMenus[NEWADDON] = new MenuNewAddon(this);
        menu.mMenus[MULTIPLAYER] = new BLMenuMultiplayer(this);
        menu.mMenus[NETWORKGAME] = new MenuNetwork(this);
        menu.mMenus[CORRUPTLOAD] = new MenuCorruptGame(this);

        gCutsceneScreen = new CutsceneScreen(this);
        gLoadingScreen = new LoadingScreen(this);
        gLogoScreen = new LogoScreen(this, 2.0f);
        gMenuScreen = new MenuScreen(this);
        gGameScreen = new GameScreen(this);
        gDemoScreen = new DemoScreen(this);
        gNetScreen = new NetScreen(this);
        gStatisticScreen = new StatisticScreen(this, 1.0f);
        gDisconnectScreen = new DisconnectScreen(this, 1.0f);

        return true;
    }

    public BloodDef getBaseDef() {
        return (BloodDef) baseDef;
    }

    public BloodDef getCurrentDef() {
        return (BloodDef) currentDef;
    }

    @Override
    protected MessageScreen createMessage(String header, String text, MessageType type) {
        return new BloodMessageScreen(this, header, text, type);
    }

    @Override
    public void onDropEntry(FileEntry entry) {
        if (!entry.isExtension("map")) {
            return;
        }

        Console.out.println("Start dropped map: " + entry.getName());
        gGameScreen.newgame(false, entry, pGameInfo.nEpisode, pGameInfo.nLevel, pGameInfo.nDifficulty, pGameInfo.nDifficulty, pGameInfo.nDifficulty, false);
    }

    @Override
    @NotNull
    public BloodRenderer getRenderer() {
        Renderer renderer = super.getRenderer();
        if (renderer instanceof BloodRenderer) {
            return (BloodRenderer) renderer;
        }

        throw new RuntimeException("Dummy renderer!");
//        return new BloodDummyRenderer();
    }

    @Override
    public void initRenderer() {
        super.initRenderer();
        PaletteView = kPalNormal;
        engine.getPaletteManager().setPalette(PaletteView);
    }

    @Override
    public BloodGameProcessor createGameProcessor() {
        return new BloodGameProcessor(this);
    }

    @Override
    public BloodGameProcessor getProcessor() {
        return (BloodGameProcessor) super.getProcessor();
    }

    private void ConsoleInit() {
        Console.out.println("Initializing on-screen display system");
        Console.out.setValue("osdtextpal", 12);
        Console.out.setValue("osdtextshade", 2);
        Console.out.setValue("osdeditpal", 12);
        Console.out.getPrompt().setVersion(getTitle(), OsdColor.YELLOW, 10);

        Console.out.registerCommand(new OsdCommand("changeteam", "changeteam to 0 or 1") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (argv.length != 1) {
                    Console.out.println("changeteam: " + net.gProfile[myconnectindex].team + " ( 0 - auto, 1 - blue, 2 - red )");
                    return CommandResponse.SILENT_RESPONSE;
                }

                int team = Integer.parseInt(argv[0]);
                if (team >= 0 && team < 3) {
                    if (net.gProfile[myconnectindex].team == team) {
                        Console.out.println("Already changed");
                        return CommandResponse.SILENT_RESPONSE;
                    }

                    net.ChangeTeam(myconnectindex, team);
                    String message = "Your team will change to ";

                    if (team == 0) {
                        team = myconnectindex & 1;
                    } else {
                        team = (team - 1);
                    }

                    switch (team) {
                        case 0:
                            message += "blue after respawn";
                            break;
                        case 1:
                            message += "red after respawn";
                            break;
                    }
                    Console.out.println(message);
                } else {
                    Console.out.println("changeteam: out of range");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("noenemies", "show enemies") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (isCurrentScreen(gDemoScreen) || pGameInfo.nGameType != kNetModeOff || numplayers > 1) {
                    Console.out.println("noenemies: Single player only");
                    return CommandResponse.SILENT_RESPONSE;
                }

                if (argv.length != 1) {
                    Console.out.println("noenemies: " + gNoEnemies + " ( 0 - disabled, 1 - ai disable, 2 - enabled )");
                    return CommandResponse.SILENT_RESPONSE;
                }

                try {
                    int type = Integer.parseInt(argv[0]);
                    if (type >= 0 && type < 3) {
                        gNoEnemies = type;
                        switch (type) {
                            case 0:
                            case 1:
                                if (type == 0) {
                                    Console.out.println("noenemies disabled");
                                } else {
                                    Console.out.println("noenemies: ai disable");
                                }

                                for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
                                    if (!IsOriginalDemo() && pGameInfo.nGameType == kNetModeOff && numplayers < 2) {
                                        Sprite pSprite = node.get();
                                        pSprite.setCstat(pSprite.getCstat() & ~kSpriteInvisible);
                                        pSprite.setCstat(pSprite.getCstat() | (kSpriteBlocking | kSpriteHitscan | kSpritePushable));
                                        aiInit(IsOriginalDemo());
                                    }
                                }
                                break;
                            case 2:
                                Console.out.println("noenemies enabled");
                                break;
                        }

                    } else {
                        Console.out.println("noenemies: out of range");
                    }
                } catch (Exception e) {
                    Console.out.println("noenemies: out of range");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("playback", "playback \"filename.dem\"") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (pGameInfo.nGameType != kNetModeOff || numplayers > 1) {
                    Console.out.println("playback: Single player only");
                    return CommandResponse.SILENT_RESPONSE;
                }

                if (argv.length < 1) {
                    Console.out.println("playback \"demoname.dem\" [\"addon.ini\" or \"zipfile.zip:addon.ini\"]");
                    return CommandResponse.SILENT_RESPONSE;
                }

                final String name = argv[0];
                Entry entry = cache.getEntry(name, true);
                if (entry.exists()) {
                    BloodIniFile ini = null;
                    if (argv.length == 2) {
                        ini = levelGetEpisode(entry);
                    }
                    gDemoScreen.showDemo(entry, ini);
                } else {
                    Console.out.println("\"" + name + "\" " + "not found!");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("quicksave", "quicksave: performs a quick save") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (pGameInfo.nGameType != kNetModeOff || numplayers > 1) {
                    Console.out.println("quicksave: Single player only");
                    return CommandResponse.SILENT_RESPONSE;
                }

                if (isCurrentScreen(gGameScreen)) {
                    quicksave();
                } else {
                    Console.out.println("quicksave: not in a game");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("quickload", "quickload: performs a quick load") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (isCurrentScreen(gGameScreen)) {
                    quickload();
                } else {
                    Console.out.println("quickload: not in a game");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("spawndude", "") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (pGameInfo.nGameType != kNetModeOff || numplayers > 1) {
                    Console.out.println("spawndude: Single player only");
                    return CommandResponse.SILENT_RESPONSE;
                }

                if (argv.length != 1) {
                    Console.out.println("spawndude \"enemy type number\"");
                    return CommandResponse.SILENT_RESPONSE;
                }

                if (isCurrentScreen(gGameScreen)) {
                    try {
                        int type = Integer.parseInt(argv[0]);
                        if (type > kDudeBase && type < kDudeMax) {
                            actSpawnDude(gMe.pSprite, type, -1);
                        } else {
                            Console.out.println("spawndude: type out of range");
                        }
                    } catch (Exception e) {
                        Console.out.println("spawndude: type out of range");
                    }
                } else {
                    Console.out.println("spawndude: not in a game");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCallback("showcoords", "", a -> {
            Sprite spr = gMe.pSprite;
            if (spr != null) {
                Console.out.println("Player x: " + spr.getX());
                Console.out.println("Player y: " + spr.getY());
                Console.out.println("Player z: " + spr.getZ());
                Console.out.println("Player sect: " + spr.getSectnum());
            } else {
                Console.out.println("not in a game");
            }
            return CommandResponse.SILENT_RESPONSE;
        }));

        Console.out.registerCommand(new OsdCommand("maphack_angoff", "") {
            @Override
            public CommandResponse execute(String[] argv) {

                if (argv.length != 1) {
                    Console.out.println("maphack_angoff value");
                    return CommandResponse.SILENT_RESPONSE;
                }

                try {
                    int angoff = Integer.parseInt(argv[0]);
                    int nSprite = getSprite();
                    if (nSprite != -1) {
                        if (!currentDef.mapInfo.isLoaded()) {
                            currentDef.mapInfo.load(new Maphack());
                        }
                        currentDef.mapInfo.getSpriteInfo(nSprite).angoff = (short) angoff;
                        Console.out.println("sprite " + nSprite + " angoff " + angoff);
                    }
                } catch (Exception e) {
                    Console.out.println("type out of range");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("maphack_notmd", "") {
            @Override
            public CommandResponse execute(String[] argv) {

                if (argv.length != 1) {
                    Console.out.println("maphack_notmd value");
                    return CommandResponse.SILENT_RESPONSE;
                }

                try {
                    int value = Integer.parseInt(argv[0]);
                    int nSprite = getSprite();
                    if (nSprite != -1) {
                        if (!currentDef.mapInfo.isLoaded()) {
                            currentDef.mapInfo.load(new Maphack());
                        }

                        if (value == 1) {
                            currentDef.mapInfo.getSpriteInfo(nSprite).flags |= 1;
                        } else {
                            currentDef.mapInfo.getSpriteInfo(nSprite).flags &= ~1;
                        }

                        Console.out.println("sprite " + nSprite + (value == 1 ? " notmd" : " notmd removed"));
                    }
                } catch (Exception e) {
                    Console.out.println("type out of range");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("maphack_mdposxoff", "") {
            @Override
            public CommandResponse execute(String[] argv) {

                if (argv.length != 1) {
                    Console.out.println("maphack_mdposxoff value");
                    return CommandResponse.SILENT_RESPONSE;
                }

                try {
                    int angoff = Integer.parseInt(argv[0]);
                    int nSprite = getSprite();
                    if (nSprite != -1) {
                        if (!currentDef.mapInfo.isLoaded()) {
                            currentDef.mapInfo.load(new Maphack());
                        }

                        currentDef.mapInfo.getSpriteInfo(nSprite).xoff = angoff;
                        Console.out.println("sprite " + nSprite + " mdposxoff " + angoff);
                    }
                } catch (Exception e) {
                    Console.out.println("type out of range");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("maphack_mdposyoff", "") {
            @Override
            public CommandResponse execute(String[] argv) {

                if (argv.length != 1) {
                    Console.out.println("maphack_mdposyoff value");
                    return CommandResponse.SILENT_RESPONSE;
                }

                try {
                    int angoff = Integer.parseInt(argv[0]);
                    int nSprite = getSprite();
                    if (nSprite != -1) {
                        if (!currentDef.mapInfo.isLoaded()) {
                            currentDef.mapInfo.load(new Maphack());
                        }

                        currentDef.mapInfo.getSpriteInfo(nSprite).yoff = angoff;
                        Console.out.println("sprite " + nSprite + " mdposyoff " + angoff);
                    }
                } catch (Exception e) {
                    Console.out.println("type out of range");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("maphack_mdposzoff", "") {
            @Override
            public CommandResponse execute(String[] argv) {

                if (argv.length != 1) {
                    Console.out.println("maphack_mdposzoff value");
                    return CommandResponse.SILENT_RESPONSE;
                }

                try {
                    int angoff = Integer.parseInt(argv[0]);
                    int nSprite = getSprite();
                    if (nSprite != -1) {
                        if (!currentDef.mapInfo.isLoaded()) {
                            currentDef.mapInfo.load(new Maphack());
                        }
                        currentDef.mapInfo.getSpriteInfo(nSprite).zoff = angoff;
                        Console.out.println("sprite " + nSprite + " mdposzoff " + angoff);
                    }
                } catch (Exception e) {
                    Console.out.println("type out of range");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("maphack_save", "") {
            @Override
            public CommandResponse execute(String[] argv) {

                if (!currentDef.mapInfo.isLoaded()) {
                    Console.out.println("These is nothing to save");
                    return CommandResponse.SILENT_RESPONSE;
                }

                String name = "unknown";
                if (mUserFlag == UserFlag.UserMap && gUserMapInfo != null) {
                    name = gUserMapInfo.MapName.getName(); //getFilename(gUserMapInfo.MapName);
                }
                if (mUserFlag != UserFlag.UserMap && currentEpisode != null) {
                    name = "e" + (pGameInfo.nEpisode + 1) + "m" + (pGameInfo.nLevel + 1);
                }

                String filename = name + ".mhk";
                Path file = getUserDirectory().getPath().resolve(filename);
                try (OutputStream os = Files.newOutputStream(file)) {
                    String checksum = "crc32 " + pGameInfo.uMapCRC + "\r\n";
                    StreamUtils.writeString(os, checksum);
                    for (int i = 0; i < boardService.getSpriteCount(); i++) {
                        if (currentDef.mapInfo.getSpriteInfo(i).angoff != 0) {
                            String msg = "sprite " + i + " angoff " + currentDef.mapInfo.getSpriteInfo(i).angoff + "\r\n";
                            StreamUtils.writeString(os, msg);
                        }

                        if (currentDef.mapInfo.getSpriteInfo(i).xoff != 0) {
                            String msg = "sprite " + i + " mdposxoff " + currentDef.mapInfo.getSpriteInfo(i).xoff + "\r\n";
                            StreamUtils.writeString(os, msg);
                        }

                        if (currentDef.mapInfo.getSpriteInfo(i).yoff != 0) {
                            String msg = "sprite " + i + " mdposyoff " + currentDef.mapInfo.getSpriteInfo(i).yoff + "\r\n";
                            StreamUtils.writeString(os, msg);
                        }

                        if (currentDef.mapInfo.getSpriteInfo(i).zoff != 0) {
                            String msg = "sprite " + i + " mdposzoff " + currentDef.mapInfo.getSpriteInfo(i).zoff + "\r\n";
                            StreamUtils.writeString(os, msg);
                        }
                        if ((currentDef.mapInfo.getSpriteInfo(i).flags & 1) != 0) {
                            String msg = "sprite " + i + " notmd\r\n";
                            StreamUtils.writeString(os, msg);
                        }
                    }
                    Console.out.println("Saved to " + file);
                } catch (IOException e) {
                    Console.out.println(filename + " is not saved. " + e, OsdColor.RED);
                }

                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCommand("maphack_load", "") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (argv.length != 1) {
                    Console.out.println("maphack_load filename");
                    return CommandResponse.SILENT_RESPONSE;
                }
                String filename = argv[0];
                Entry entry = cache.getEntry(filename, true);
                if (entry.exists()) {
                    Maphack map = new Maphack(entry);
                    if (map.getMapCRC() == pGameInfo.uMapCRC) {
                        currentDef.mapInfo.load(map);
                        Console.out.println("Maphack is loaded");
                    } else {
                        Console.out.println("Maphack isn't loaded. Wrong checksum");
                    }
                } else {
                    Console.out.println("maphack_load: file not found");
                }
                return CommandResponse.SILENT_RESPONSE;
            }
        });

        Console.out.registerCommand(new OsdCallback("maphack_highlight", "", a -> {
            maphack_highlight = !maphack_highlight;
            Console.out.println("Maphack highlight is " + (maphack_highlight ? "on" : "off"));
            return CommandResponse.SILENT_RESPONSE;
        }));
    }

    private void InitCutscenes() {
        Console.out.println("Initializing cutscenes");
        GrpFile group = new GrpFile("Cutscenes");

        String[] smknames = new String[]{"CutSceneA", "CutWavA", "CutSceneB", "CutWavB"};
        for (int i = 0; i < kMaxEpisode; i++) {
            if (MainINI.set("Episode" + (i + 1))) {
                for (String smkname : smknames) {
                    String smkPath = MainINI.getKeyString(smkname);
                    if (smkPath.isEmpty()) {
                        continue;
                    }

                    try {
                        int index = smkPath.indexOf(":" + File.separator);
                        if (index != -1) { // perhaps it's a CDROM path (D:\Movie\CS1.SMK)
                            smkPath = smkPath.substring(index + 2);
                        }

                        Entry entry = cache.getEntry(FileUtils.getPath(smkPath), true);
                        if (entry.exists()) {
                            group.addEntry(entry);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        Set<Group> groupList = new LinkedHashSet<>();
        if (!group.isEmpty()) {
            // search in place, where the others was found
            for (Entry fil : group.getEntries()) {
                groupList.add(fil.getParent());
            }
        }
        groupList.add(cache.getGameDirectory());

        smknames = new String[]{"logo.smk", "logo811m.wav", "gti.smk", "gti.wav"};
        for (String smkname : smknames) {
            for (Group gr : groupList) {
                Entry entry = gr.getEntry(smkname);
                if (entry.exists()) {
                    group.addEntry(entry);
                    break;
                }
            }
        }

        if (!group.isEmpty()) {
            cache.addGroup(group, NORMAL);
        } else {
            Console.out.println("Cutscenes were not found", OsdColor.YELLOW);
        }
    }

    public int getSprite() {
        int dx = Trig.Cos(gMe.pSprite.getAng()) >> 16;
        int dy = Trig.Sin(gMe.pSprite.getAng()) >> 16;
        int dz = (int) gMe.horizOff;

        Gameutils.HitScan(gMe.pSprite, gMe.viewOffZ, dx, dy, dz, pHitInfo, 0xFFFF0030, 0);

        return pHitInfo.hitsprite;
    }

    public void resetState() {
        mUserFlag = UserFlag.None;
        currentEpisode = null;
        lastload = null;
        kFakeMultiplayer = false;
        if (usecustomarts || usecustomqavs) {
            resetEpisodeResources();
        }

        menu.mClose();
        menu.mOpen(menu.mMenus[MAIN], -1);
    }

    public void EndGame() {
        changeScreen(gLogoScreen.setTile(2050).setCallback(rMenu));
    }

    public void Disconnect() {
        Actor.gPost.reset(); // #GDX 31.12.2024
        changeScreen(gDisconnectScreen.setSkipping(rMenu));
    }

    @Override
    public void GameMessage(String errorText) {
        pGameInfo.uGameFlags = EndOfGame;
        pGameInfo.nLevel = 0;
        super.GameMessage(errorText);
    }

    @Override
    public void show() {
        if (!args.isEmpty()) {
            // Handle arguments at the first launch. args should be clear after handle
            parseArgumentsCommon();
            String netmode = args.getOrDefault("-netmode", "");
            String players = args.getOrDefault("-players", "");
            args.clear();

//            if (!file.isEmpty() && netmode.isEmpty()) {
//                onFilesDropped(new String[] {file});
//                return;
//            }

            if (!netmode.isEmpty()) {
                Console.out.println("Starting multiplayer as " + netmode, OsdColor.YELLOW);
                if (netmode.equalsIgnoreCase("master")) {
                    String[] param = { "-n0:" + (players.isEmpty() ? 2 : players), "-p " + cfg.getPort() };
                    ((BLMenuMultiplayer) menu.mMenus[MULTIPLAYER]).getMenuCreate(this).createGame(0, false, param);
                    return;
                } else if (netmode.equalsIgnoreCase("slave")) {
                    String[] param = new String[]{ "-n0", cfg.getmAddress(), "-p " + cfg.getPort() };
                    ((BLMenuMultiplayer) menu.mMenus[MULTIPLAYER]).getMenuJoin(this).joinGame(param);
                    return;
                }
            }
        }

        gPaused = false;
        gLogoScreen.setTile(2050).setCallback(nextLogo).setSkipping(rMenu);
        gCutsceneScreen.setCallback(nextLogo).setSkipping(rMenu).escSkipping(false);

        if (cfg.showCutscenes && gCutsceneScreen.init("logo.smk", "logo811m.wav")) {
            changeScreen(gCutsceneScreen);
        } else {
            changeScreen(gLogoScreen);
        }
    }

    @Override
    public DefaultPrecacheScreen getPrecacheScreen(Runnable readyCallback, PrecacheListener listener) {
        return new PrecacheScreen(readyCallback, listener);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewResizeView(cfg.gViewSize);
    }

    @Override
    public LogSender getLogSender() {
        return new LogSender(this) {
            @Override
            public byte[] reportData() {
                byte[] out;

                String text = "Mapname: " + boardfilename + "\r\n";
                text += "MapEntry: " + pGameInfo.getMapName() + "\r\n";
                text += "UserFlag: " + mUserFlag + "\r\n";

                if (mUserFlag != UserFlag.UserMap && LEVELS.currentEpisode != null) {
                    EpisodeInfo currentEpisode = LEVELS.currentEpisode;

                    text += "Episode filename: " + currentEpisode.iniFile;
                    text += "\r\n";
                    text += "Episode title: " + currentEpisode.Title;
                    text += "\r\n";
                }

                text += "nDifficulty: " + pGameInfo.nDifficulty;
                text += "\r\n";

                if (gMe != null && gMe.pSprite != null) {
                    text += "PlayerX: " + gMe.pSprite.getX();
                    text += "\r\n";
                    text += "PlayerY: " + gMe.pSprite.getY();
                    text += "\r\n";
                    text += "PlayerZ: " + gMe.pSprite.getZ();
                    text += "\r\n";
                    text += "PlayerAng: " + gMe.pSprite.getAng();
                    text += "\r\n";
                    text += "PlayerHoriz: " + gMe.horiz;
                    text += "\r\n";
                    text += "PlayerSect: " + gMe.pSprite.getSectnum();
                    text += "\r\n";
                }

                if (mUserFlag == UserFlag.UserMap && gUserMapInfo != null && gUserMapInfo.MapName != null) {
                    ByteArray array = new ByteArray();
                    byte[] data = gUserMapInfo.MapName.getBytes();

                    text += "\r\n<------Start Map data------->\r\n";
                    array.addAll(text.getBytes());
                    array.addAll(data);
                    array.addAll("\r\n<------End Map data------->\r\n".getBytes());

                    out = Arrays.copyOf(array.items, array.size);
                } else {
                    out = text.getBytes();
                }

                return out;
            }
        };
    }

    public String getFilename(String path) {
        if (path != null) {
            path = FileUtils.getPath(path).getFileName().toString();
        }

        return path;
    }

    public enum UserFlag {
        None, UserMap, Addon
    }

}

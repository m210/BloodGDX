// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Menus;

import ru.m210projects.Blood.Factory.BloodMenuHandler;
import ru.m210projects.Blood.Main;
import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Blood.filehandlers.EpisodeEntry;
import ru.m210projects.Build.Pattern.MenuItems.BuildMenu;
import ru.m210projects.Build.Pattern.MenuItems.MenuFileBrowser;
import ru.m210projects.Build.Pattern.MenuItems.MenuTitle;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.filehandle.fs.FileEntry;

import java.util.List;

import static ru.m210projects.Blood.Factory.BloodMenuHandler.*;
import static ru.m210projects.Blood.Globals.MainINI;
import static ru.m210projects.Blood.Main.gDemoScreen;
import static ru.m210projects.Blood.Main.game;
import static ru.m210projects.Blood.ResourceHandler.episodeManager;
import static ru.m210projects.Blood.ResourceHandler.levelGetEpisode;
import static ru.m210projects.Blood.Types.DemoUtils.checkDemoEntry;
import static ru.m210projects.Blood.Types.DemoUtils.nDemonum;

public class BLUserContent extends BuildMenu {

    private final Main app;
    private final MenuFileBrowser list;
    public boolean showmain;

    public BLUserContent(final Main app) {
        super(app.menu);
        this.app = app;
        MenuTitle title = new MenuTitle(app.pEngine, "User content", app.getFont(1), 160, 20, 2038);

        int width = 240;
        list = new MenuFileBrowser(app, app.getFont(3), app.getFont(1), app.getFont(3), 40, 40, width, 1, 10, 2046) {

            @Override
            public void init() {
                registerExtension("map", 0, 0);
                registerExtension("rff", 7, 1);
                registerExtension("grp", 7, 1);
                registerExtension("zip", 7, 1);
                registerExtension("pk3", 7, 1);
                registerExtension("ini", 7, 1);
                registerExtension("dem", 10, -1);
            }

            @Override
            public void handleFile(FileEntry fil) {
                switch (fil.getExtension()) {
                    case "INI":
                        boolean skipMain = game.getCache().isGameDirectory(fil.getParent());
                        if (showmain) {
                            skipMain = false;
                        }
                        if (skipMain && fil.getName().equalsIgnoreCase(MainINI.getName())) {
                            break;
                        }
                    case "GRP":
                    case "ZIP":
                    case "RFF":
                    case "PK3":
                        for (EpisodeEntry entry : episodeManager.getEpisodeEntries(fil)) {
                            addFile((FileEntry) entry);
                        }
                        break;
                    case "DEM":
                        if (showmain) {
                            break; // multiplayer menu
                        }

                        if (checkDemoEntry(fil, false))
                            addFile(fil);
                        break;
                    default:
                        addFile(fil);
                        break;
                }
            }

            @Override
            public void invoke(FileEntry fil) {
                switch (fil.getExtension()) {
                    case "MAP":
                        launchMap(fil);
                        break;
                    case "INI":
                    case "GRP":
                    case "ZIP":
                    case "RFF":
                    case "PK3:":
                        if (fil instanceof EpisodeEntry) {
                            launchEpisode(episodeManager.getEpisode((EpisodeEntry) fil));
                        }
                        break;
                    case "DEM":
                        BloodIniFile episode = null;
                        Directory parent = fil.getParent();

                        List<Entry> demoList = checkDemoEntry(parent);
                        if (!game.getCache().isGameDirectory(parent)) {
                            for (Entry entry : parent.getEntries()) {
                                if (entry.isExtension("ini")) {
                                    episode = levelGetEpisode(entry);
                                    if (episode != null) {
                                        break;
                                    }
                                }
                            }
                        }

                        nDemonum = demoList.indexOf(fil);
                        gDemoScreen.showDemo(fil, episode);
                        app.pMenu.mClose();
                        break;
                }
            }

            @Override
            public void handleDirectory(Directory dir) {
                /*nothing*/
            }

            @Override
            public void drawHeader(Renderer renderer, int x1, int x2, int y) {
                /*directories*/
                topFont.drawTextScaled(renderer, x1 + 11, y + 5, dirs, 0.5f, -32, topPal, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                /*files*/
                topFont.drawTextScaled(renderer, x2 + 10, y + 5, ffs, 0.5f, -32, topPal, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
            }

            @Override
            public void drawPath(Renderer renderer, int x, int y, String path) {
                super.drawPath(renderer, x, y, this.path);
            }
        };

        list.transparent = 33;
        list.topPal = 8;
        list.pathPal = 8;
        addItem(title, false);
        addItem(list, true);
        addItem(((BloodMenuHandler) app.pMenu).addMenuBlood(), false);
    }

    public boolean mFromNetworkMenu() {
        return app.menu.getLastMenu() == app.menu.mMenus[NETWORKGAME];
    }

    public void setShowMain(boolean show) {
        this.showmain = show;
        if (game.getCache().isGameDirectory(list.getDirectory()))
            list.refreshList();
    }

    private void launchEpisode(BloodIniFile ini) {
        if (ini == null) return;

        if (mFromNetworkMenu()) {
            MenuNetwork network = (MenuNetwork) app.menu.mMenus[NETWORKGAME];
            network.setEpisode(ini);
            app.menu.mMenuBack();
            return;
        }

        MenuNewAddon next = (MenuNewAddon) app.menu.mMenus[NEWADDON];
        next.setEpisode(ini);
        app.menu.mOpen(next, -1);
    }

    private void launchMap(FileEntry file) {
        if (file == null) return;

        if (mFromNetworkMenu()) {
            MenuNetwork network = (MenuNetwork) app.menu.mMenus[NETWORKGAME];
            network.setMap(file);
            app.menu.mMenuBack();
            return;
        }

        MenuDifficulty next = (MenuDifficulty) app.menu.mMenus[DIFFICULTY];
        next.setMap(file);
        app.menu.mOpen(next, -1);
    }
}

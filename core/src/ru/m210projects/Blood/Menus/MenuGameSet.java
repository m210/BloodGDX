// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Menus;

import ru.m210projects.Blood.Main;
import ru.m210projects.Build.Pattern.MenuItems.*;

import static ru.m210projects.Blood.Globals.kFrameTicks;
import static ru.m210projects.Blood.Globals.kTimerRate;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.net.Mmulti.numplayers;

public class MenuGameSet extends BuildMenu {

    public MenuGameSet(Main app) {
        super(app.menu);
        MenuTitle mTitle = new MenuTitle(app.pEngine, "Game Setup", app.getFont(1), 160, 20, 2038);
        int pos = 40;

        MenuSwitch sAutoload = new MenuSwitch("Autoload folder", app.getFont(3), 46, pos += 10, 240, cfg.isAutoloadFolder(), (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.setAutoloadFolder(sw.value);
        }, "Enabled", "Disabled");

        MenuSwitch sViewBobbing = new MenuSwitch("VIEW BOBBING:", app.getFont(3), 46, pos += 10, 240, cfg.gBobWidth, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.gBobWidth = sw.value;
        }, null, null);
        MenuSwitch sViewSwaying = new MenuSwitch("VIEW SWAYING:", app.getFont(3), 46, pos += 10, 240, cfg.gBobHeight,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.gBobHeight = sw.value;
                }, null, null);

        MenuSwitch sSlopeTilt = new MenuSwitch("SLOPE TILTING:", app.getFont(3), 46, pos += 10, 240, cfg.gSlopeTilt, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.gSlopeTilt = sw.value;
            game.net.gProfile[myconnectindex].slopetilt = cfg.gSlopeTilt;
            if (numplayers > 1)
                game.net.InitProfile(myconnectindex);
        }, null, null) {

            @Override
            public void open() {
                mCheckEnableItem(numplayers < 2 || !game.isCurrentScreen(gGameScreen));
            }
        };

        MenuSwitch sAutoAim = new MenuSwitch("AutoAim:", app.getFont(3), 46, pos += 10, 240, cfg.gAutoAim, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.gAutoAim = sw.value;
            game.net.gProfile[myconnectindex].autoaim = cfg.gAutoAim;
            if (numplayers > 1)
                game.net.InitProfile(myconnectindex);
        }, null, null) {

            @Override
            public void open() {
                mCheckEnableItem(numplayers < 2 || !game.isCurrentScreen(gGameScreen));
            }
        };

        MenuSwitch sSecretCount = new MenuSwitch("Auto secrets counter:", app.getFont(3), 46, pos += 10, 240, cfg.useautosecretcount,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.useautosecretcount = sw.value;
                }, null, null);

        MenuSwitch sShowCutscenes = new MenuSwitch("Cutscenes:", app.getFont(3), 46, pos += 10, 240, cfg.showCutscenes,
                (handler, pItem) -> {
                    MenuSwitch sw = (MenuSwitch) pItem;
                    cfg.showCutscenes = sw.value;
                }, null, null);

        MenuConteiner mPlayingDemo = new MenuConteiner("Demos playback:", app.getFont(3), 46, pos += 10, 240, null, 0,
                (handler, pItem) -> {
                    MenuConteiner item = (MenuConteiner) pItem;
                    cfg.gDemoSeq = item.num;
                }) {
            @Override
            public void open() {
                if (this.list == null) {
                    this.list = new char[3][];
                    this.list[0] = "Off".toCharArray();
                    this.list[1] = "In order".toCharArray();
                    this.list[2] = "Randomly".toCharArray();
                }
                num = cfg.gDemoSeq;
            }
        };

        MenuConteiner sOverlay = new MenuConteiner("Overlay map:", app.getFont(3), 46, pos += 10, 240, null, 0, (handler, pItem) -> {
            MenuConteiner item = (MenuConteiner) pItem;
            cfg.gOverlayMap = item.num;
        }) {

            @Override
            public void open() {
                if (this.list == null) {
                    this.list = new char[3][];
                    this.list[0] = "Full only".toCharArray();
                    this.list[1] = "Overlay only".toCharArray();
                    this.list[2] = "Full and overlay".toCharArray();
                }
                num = cfg.gOverlayMap;
            }
        };

        MenuSwitch mVanilla = new MenuSwitch("Vanilla mode:", app.getFont(3), 46, pos += 10, 240, cfg.gVanilla, (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.gVanilla = sw.value;
        }, null, null);

        MenuSwitch mTimer = new MenuSwitch("Game loop timer:", app.getFont(3), 46, pos += 10, 240, cfg.isLegacyTimer(), (handler, pItem) -> {
            MenuSwitch sw = (MenuSwitch) pItem;
            cfg.setLegacyTimer(sw.value);
            engine.inittimer(cfg.isLegacyTimer(), kTimerRate, kFrameTicks);
        }, "Legacy", "Gdx");

        addItem(mTitle, false);
        addItem(sAutoload, true);
        addItem(sViewBobbing, false);
        addItem(sViewSwaying, false);
        addItem(sSlopeTilt, false);
        addItem(sAutoAim, false);
        addItem(sSecretCount, false);
//        addItem(sStartup, false);
        addItem(sShowCutscenes, false);
        addItem(mPlayingDemo, false);
        addItem(sOverlay, false);
        addItem(mVanilla, false);
        addItem(mTimer, false);

        addItem(app.menu.addMenuBlood(), false);
    }
}

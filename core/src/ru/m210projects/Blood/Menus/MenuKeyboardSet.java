// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Menus;

import ru.m210projects.Blood.Main;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.settings.GameConfig;

import static ru.m210projects.Blood.Main.cfg;

public class MenuKeyboardSet extends BuildMenu {

    public MenuKeyboardSet(Main app) {
        super(app.menu);
        MenuTitle mTitle = new MenuTitle(app.pEngine, "Keyboard Setup", app.getFont(1), 160, 20, 2038);

        addItem(mTitle, false);
        int pos = 30;

        MenuSlider keyTurn = new MenuSlider(app.pSlider, "Key turn speed:", app.getFont(1), 60, pos += 20, 200, cfg.gTurnSpeed, 64, 128, 4,
                (handler, pItem) -> {
                    MenuSlider slider = (MenuSlider) pItem;
                    cfg.gTurnSpeed = slider.value;
                }, false);
        keyTurn.fontShadow = true;

        MenuButton mKeySet = new MenuButton("Configure Keys", app.getFont(1), 0, pos += 30, 320, 1, 0, new BLMenuMenuKeyboard(app), -1, null, 0);
        mKeySet.fontShadow = true;
        MenuButton mKeyReset = new MenuButton("Reset Keys to default", app.getFont(1), 0, pos += 20, 320, 1, 0, getResetDefaultMenu(app), -1, null, 0);
        mKeyReset.fontShadow = true;
        MenuButton mKeyReset2 = new MenuButton("Reset Keys to classic", app.getFont(1), 0, pos += 20, 320, 1, 0, getResetClassicMenu(app), -1, null, 0);
        mKeyReset2.fontShadow = true;

        addItem(keyTurn, true);
        addItem(mKeySet, false);
        addItem(mKeyReset, false);
        addItem(mKeyReset2, false);

        addItem(app.menu.addMenuBlood(), false);
    }

    private void mResetDefault(GameConfig cfg, MenuHandler menu) {
        cfg.resetInput(false);
        menu.mMenuBack();
    }

    private void mResetClassic(GameConfig cfg, MenuHandler menu) {
        cfg.resetInput(true);
        menu.mMenuBack();
    }

    private BuildMenu getResetDefaultMenu(final Main app) {
        BuildMenu menu = new BuildMenu(app.menu);

        int pos = 90;
        MenuText QuitQuestion = new MenuText("Do you really", app.getFont(3), 160, pos, 1);
        MenuText QuitQuestion2 = new MenuText("want to reset keys?", app.getFont(3), 160, pos += 10, 1);
        MenuVariants QuitVariants = new MenuVariants(app.pEngine, "[Y/N]", app.getFont(3), 160, pos += 20) {
            @Override
            public void positive(MenuHandler menu) {
                mResetDefault(app.pCfg, menu);
            }
        };

        menu.addItem(QuitQuestion, false);
        menu.addItem(QuitQuestion2, false);
        menu.addItem(QuitVariants, true);

        menu.addItem(app.menu.addMenuBlood(), false);

        return menu;
    }

    private BuildMenu getResetClassicMenu(final Main app) {
        BuildMenu menu = new BuildMenu(app.menu);

        int pos = 90;
        MenuText QuitQuestion = new MenuText("Do you really want", app.getFont(3), 160, pos, 1);
        MenuText QuitQuestion2 = new MenuText("reset to classic?", app.getFont(3), 160, pos += 10, 1);
        MenuVariants QuitVariants = new MenuVariants(app.pEngine, "[Y/N]", app.getFont(3), 160, pos += 20) {
            @Override
            public void positive(MenuHandler menu) {
                mResetClassic(app.pCfg, menu);
            }
        };

        menu.addItem(QuitQuestion, false);
        menu.addItem(QuitQuestion2, false);
        menu.addItem(QuitVariants, true);

        menu.addItem(app.menu.addMenuBlood(), false);

        return menu;
    }
}

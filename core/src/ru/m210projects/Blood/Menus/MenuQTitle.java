// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Menus;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Blood.Main;
import ru.m210projects.Blood.Screens.DemoScreen;
import ru.m210projects.Build.Pattern.MenuItems.*;

import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.net.Mmulti.numplayers;

public class MenuQTitle extends BuildMenu {

    public MenuQTitle(final Main game) {
        super(game.menu);
        MenuTitle QuitTitle = new MenuTitle(game.pEngine, "Quit to title", game.getFont(1), 160, 20, 2038);

        MenuText QuitQuestion = new MenuText("Quit to title?", game.getFont(0), 160, 100, 1);
        MenuVariants QuitVariants = new MenuVariants(game.pEngine, "[Y/N]", game.getFont(0), 160, 115) {
            @Override
            public void positive(MenuHandler menu) {

                if (!DemoScreen.isDemoPlaying() && pGameInfo.nGameType != kNetModeOff && (numplayers > 1 || kFakeMultiplayer)) {
                    Gdx.app.postRunnable(() -> game.net.NetDisconnect(myconnectindex));
                } else {
                    game.EndGame();
                }

                menu.mClose();
            }
        };

        addItem(QuitTitle, false);
        addItem(QuitQuestion, false);
        addItem(QuitVariants, true);
        addItem(game.menu.addMenuBlood(), false);
    }
}

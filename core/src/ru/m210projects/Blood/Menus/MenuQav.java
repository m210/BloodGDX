// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Menus;

import ru.m210projects.Blood.QAV;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler;
import ru.m210projects.Build.Pattern.MenuItems.MenuHandler.MenuOpt;
import ru.m210projects.Build.Pattern.MenuItems.MenuItem;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.filehandle.Entry;

import java.io.InputStream;

import static ru.m210projects.Blood.Globals.gFrameClock;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.Main.game;
import static ru.m210projects.Blood.QAV.kQFrameScale;
import static ru.m210projects.Blood.QAV.kQFrameUnclipped;

public class MenuQav extends MenuItem {
    public String qFileName;
    public int qFileId;
    public QAV pQAV;
    public int duration;
    public int qGameClock;
    public boolean clearScreen;

    public MenuQav(int x, int y, int qFileId) {
        super(null, null);
        this.flags = 1;
        this.m_pMenu = null;
        this.width = 0;
        this.x = x;
        this.y = y;
        this.qFileId = qFileId;
        this.qFileName = null;
        this.clearScreen = false;
    }

    public MenuQav(int x, int y, String qFileName) {
        super(null, null);
        this.flags = 1;
        this.m_pMenu = null;
        this.width = 0;
        this.x = x;
        this.y = y;
        this.qFileName = qFileName;
        this.qFileId = -1;
        this.clearScreen = false;
    }

    @Override
    public void draw(MenuHandler handler) {
        if (pQAV != null) {
            Renderer renderer = game.getRenderer();
            if (clearScreen) {
                renderer.clearview(0);
            }
            int oldFrameClock = gFrameClock;
            gFrameClock = engine.getTotalClock();
            int ticks = engine.getTotalClock() - qGameClock;
            qGameClock = engine.getTotalClock();
            duration -= ticks;
            if (duration <= 0 || duration > pQAV.duration)
                duration = pQAV.duration;

            int t = pQAV.duration - duration;
            pQAV.Play(t - ticks, t, -1, null);

//            int oldwx1 = windowx1;
//            int oldwy1 = windowy1;
//            int oldwx2 = windowx2;
//            int oldwy2 = windowy2;
//            windowx1 = 0;
//            windowy1 = 0;
//            windowx2 = xdim - 1;
//            windowy2 = ydim - 1;
            pQAV.Draw(t, 0, (kQFrameUnclipped | kQFrameScale), 0, 65536);
//            windowx1 = oldwx1;
//            windowy1 = oldwy1;
//            windowx2 = oldwx2;
//            windowy2 = oldwy2;
            gFrameClock = oldFrameClock;
        }
    }

    @Override
    public boolean callback(MenuHandler handler, MenuOpt opt) {

        switch (opt) {
            case LEFT:
            case BSPACE:
                m_pMenu.mNavUp();
                return false;
            case RIGHT:
            case ENTER:
            case SPACE:
                m_pMenu.mNavDown();
                return false;
            case UP:
            case DW:
            case ESC:
                return m_pMenu.mNavigation(opt);
            default:
                return false;
        }
    }

    @Override
    public void open() {
        if (qFileName == null && qFileId == -1)
            return;

        if (pQAV != null) {
            duration = pQAV.duration;
            qGameClock = engine.getTotalClock();
            return;
        }

        Entry hQAV;
        if (qFileName != null)
            hQAV = game.getCache().getEntry(qFileName, true);
        else
            hQAV = game.getCache().getEntry(qFileId, "qav");

        if (!hQAV.exists()) {
            System.err.println("Could not load QAV " + qFileName);
            return;
        }

        try (InputStream is = hQAV.getInputStream()) {
            pQAV = new QAV(is);
            pQAV.origin.x = x;
            pQAV.origin.y = y;
            duration = pQAV.duration;
            qGameClock = engine.getTotalClock();
        } catch (Exception e) {
            System.err.println("Could not load QAV " + qFileName + ": " + e);
        }
    }

    @Override
    public void close() {
    }

    @Override
    public boolean mouseAction(int x, int y) {
        return false;
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import ru.m210projects.Blood.filehandlers.art.BloodArtEntry;
import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Blood.Actor.kAttrGravity;
import static ru.m210projects.Blood.DB.xwall;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.Trig.*;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Blood.Warp.gUpperLink;
import static ru.m210projects.Build.Engine.pow2char;

public class Mirror {

    public static final int MIRROR = 504;
    public static final int MIRRORLABEL = 4080;
    //	public static final int MIRRORLABEL = 4095;
    public static final int MAXMIRRORS = 16;
    public static int mirrorcnt;
    public static boolean display_mirror;

    public static int[] MirrorType = new int[MAXMIRRORS];
    public static int[] MirrorX = new int[MAXMIRRORS];
    public static int[] MirrorY = new int[MAXMIRRORS];
    public static int[] MirrorZ = new int[MAXMIRRORS];
    public static int[] MirrorLower = new int[MAXMIRRORS];
    public static int[] MirrorUpper = new int[MAXMIRRORS];
    public static int MirrorSector;
    public static int[] MirrorWall = new int[4];

    public static void InitMirrors() {
        int i;

        //Scan wall tags
        mirrorcnt = 0;
        InitMirrorTiles();

        for (i = boardService.getWallCount() - 1; i >= 0 && mirrorcnt != MAXMIRRORS; i--) {
            Wall pWall = boardService.getWall(i);
            if (pWall.getOverpicnum() == MIRROR) {
                if (pWall.getExtra() > 0 && pWall.getLotag() == 501) {
                    Console.out.println("Initializing wall overpicnum mirror for");

                    pWall.setCstat(pWall.getCstat() | kWallOneWay);
                    pWall.setOverpicnum((short) (mirrorcnt + MIRRORLABEL));
                    MirrorType[mirrorcnt] = 0;
                    int nXWall = pWall.getExtra();
                    MirrorUpper[mirrorcnt] = i;
                    int data = xwall[nXWall].data;

                    int nWall = boardService.getWallCount();
                    while (--nWall >= 0) {
                        if (nWall != i) {
                            int nXWall2 = boardService.getWall(nWall).getExtra();
                            if (nXWall2 > 0 && boardService.getWall(nWall).getLotag() == 501 && xwall[nXWall2].data == data) {
                                pWall.setHitag((short) nWall);
                                boardService.getWall(nWall).setHitag((short) i);
                                MirrorLower[mirrorcnt] = nWall;
                                break;
                            }
                        }
                    }

                    if (nWall < 0) {
                        throw new AssertException("wall[" + i + "] has no matching wall link! (data=" + xwall[nXWall].data + ")");
                    }
                    ++mirrorcnt;
                }
            } else if (pWall.getPicnum() == MIRROR) {
//				Console.out.println("Initializing wall picnum mirror for wal");
                pWall.setCstat(pWall.getCstat() | kWallOneWay);
                pWall.setPicnum((short) (mirrorcnt + MIRRORLABEL));
                MirrorType[mirrorcnt] = 0;
                MirrorLower[mirrorcnt] = i;
                MirrorUpper[mirrorcnt] = i;
                mirrorcnt++;
            }
        }

        for (i = boardService.getSectorCount() - 1; i >= 0 && mirrorcnt < (MAXMIRRORS - 1); i--) {
            Sector pSector = boardService.getSector(i);
            if (pSector.getFloorpicnum() == MIRROR) {
                int nUpper = gUpperLink[i];
                if (nUpper >= 0) {
                    int nLower = boardService.getSprite(nUpper).getOwner();
                    if (nLower == -1) {
                        continue;
                    }
                    if (boardService.getSector(boardService.getSprite(nLower).getSectnum()).getCeilingpicnum() != MIRROR) {
                        Console.out.println("Lower link sector " + i + " doesn't have mirror picnum!", OsdColor.RED);
                    }
//					Console.out.println("Initializing floor mirror for sector " + i);
                    MirrorType[mirrorcnt] = 2;
                    MirrorX[mirrorcnt] = boardService.getSprite(nLower).getX() - boardService.getSprite(nUpper).getX();
                    MirrorY[mirrorcnt] = boardService.getSprite(nLower).getY() - boardService.getSprite(nUpper).getY();
                    MirrorZ[mirrorcnt] = boardService.getSprite(nLower).getZ() - boardService.getSprite(nUpper).getZ();
                    MirrorLower[mirrorcnt] = boardService.getSprite(nLower).getSectnum();
                    MirrorUpper[mirrorcnt] = i;
                    pSector.setFloorpicnum((short) (MIRRORLABEL + mirrorcnt++));
//					Console.out.println("Initializing ceiling mirror for sector " + i);
                    MirrorType[mirrorcnt] = 1;
                    MirrorX[mirrorcnt] = boardService.getSprite(nUpper).getX() - boardService.getSprite(nLower).getX();
                    MirrorY[mirrorcnt] = boardService.getSprite(nUpper).getY() - boardService.getSprite(nLower).getY();
                    MirrorZ[mirrorcnt] = boardService.getSprite(nUpper).getZ() - boardService.getSprite(nLower).getZ();
                    MirrorLower[mirrorcnt] = i;
                    MirrorUpper[mirrorcnt] = boardService.getSprite(nLower).getSectnum();
                    boardService.getSector(boardService.getSprite(nLower).getSectnum()).setCeilingpicnum((short) (MIRRORLABEL + mirrorcnt++));
                }
            }
        }

//		Console.out.println(mirrorcnt + " mirrors initialized");
    }

    public static void InitMirrorTiles() {
        engine.allocatepermanenttile(MIRROR, 0, 0);
        for (int i = 0; i < MAXMIRRORS; i++) {
            engine.allocatepermanenttile(i + MIRRORLABEL, 0, 0);
        }
    }

    public static void processMirror(long nViewX, long nViewY) {
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();
        for (int i = renderedSpriteList.getSize() - 1; i >= 0; --i) {
            TSprite tSprite = renderedSpriteList.get(i);
            tSprite.setXrepeat(0);
            tSprite.setYrepeat(0);
        }

        for (int i = mirrorcnt - 1; i >= 0; i--) {
            int nMirror = i + MIRRORLABEL;

            if (renderer.gotPic(nMirror)) {
                int type = MirrorType[i];
                if (type == 1 || type == 2) {
                    for (ListNode<Sprite> node = boardService.getSectNode(MirrorLower[i]); node != null; node = node.getNext()) {
                        int j = node.getIndex();
                        Sprite pSprite = boardService.getSprite(j);
                        if (pSprite != gPlayer[gViewIndex].pSprite) {
                            GetSpriteExtents(pSprite);
                            engine.getzsofslope((short) MirrorLower[i], pSprite.getX(), pSprite.getY(), floorz, ceilz);
                            if (pSprite.getLotag() == kStatDude) {
                                if (extents_zTop < ceilz.get() || extents_zBot > floorz.get()) {
                                    int cnt;
                                    if (type == 2) {
                                        cnt = i + 1;
                                    } else {
                                        cnt = i - 1;
                                    }
                                    int mirX = MirrorX[cnt];
                                    int mirY = MirrorY[cnt];
                                    int mirZ = MirrorZ[cnt];

                                    TSprite tSprite = viewInsertTSprite(MirrorUpper[i], 0, null);
                                    tSprite.reset((byte) 0);

                                    tSprite.setLotag(pSprite.getLotag());
                                    tSprite.setXvel(pSprite.getXvel());
                                    tSprite.update(mirX + pSprite.getX(), mirY + pSprite.getY(), mirZ + pSprite.getZ(), MirrorUpper[i]);

                                    tSprite.setAng(pSprite.getAng());
                                    tSprite.setPicnum(pSprite.getPicnum());
                                    tSprite.setShade(pSprite.getShade());
                                    tSprite.setPal(pSprite.getPal());
                                    tSprite.setXrepeat(pSprite.getXrepeat());
                                    tSprite.setYrepeat(pSprite.getYrepeat());
                                    tSprite.setXoffset(pSprite.getXoffset());
                                    tSprite.setYoffset(pSprite.getYoffset());
                                    tSprite.setStatnum(0);
                                    tSprite.setCstat(pSprite.getCstat());
                                    tSprite.setOwner(pSprite.getXvel());
                                    tSprite.setExtra(pSprite.getExtra());
                                    tSprite.setHitag((short) (pSprite.getHitag() | kAttrGravity));

                                    viewBackupSpriteLoc(pSprite.getXvel(), pSprite);
                                }
                            }
                        }
                    }
                }
            }
        }

        for (int nTSprite = renderedSpriteList.getSize() - 1; nTSprite >= 0; --nTSprite) {
            Sprite pTSprite = renderedSpriteList.get(nTSprite);
            int nFrames = 0, dx, dy;

            ArtEntry pic = engine.getTile(pTSprite.getPicnum());
            if (pic instanceof BloodArtEntry) {
                switch (((BloodArtEntry) pic).getView()) {
                    case kSpriteView5Full:
                        dx = (int) (nViewX - pTSprite.getX());
                        dy = (int) (nViewY - pTSprite.getY());

                        RotateVector(dx, dy, -pTSprite.getAng() + kAngle45 / 2);
                        nFrames = GetOctant((int) rotated.x, (int) rotated.y);

                        if (nFrames > 4) {
                            nFrames = 8 - nFrames;
                            pTSprite.setCstat(pTSprite.getCstat() | kSpriteFlipX);
                        } else {
                            pTSprite.setCstat(pTSprite.getCstat() & ~kSpriteFlipX);
                        }
                        break;
                    case kSpriteView8Full:
                        // Calculate which of the 8 angles of the sprite to draw (0-7)
                        dx = (int) (nViewX - pTSprite.getX());
                        dy = (int) (nViewY - pTSprite.getY());

                        RotateVector(dx, dy, -pTSprite.getAng() + kAngle45 / 2);
                        nFrames = GetOctant((int) rotated.x, (int) rotated.y);
                        break;
                    default:
                        break;
                }
                while (nFrames > 0) {
                    --nFrames;
                    pTSprite.setPicnum(pTSprite.getPicnum() + pic.getAnimFrames() + 1);
                }
            }
        }

    }

    public static void setMirrorParalax(boolean mirror) {
        Renderer renderer = game.getRenderer();
        for (int i = mirrorcnt - 1; i >= 0; i--) {
            int nMirror = i + MIRRORLABEL;
            if (renderer.gotPic(nMirror)) {
                int type = MirrorType[i];
                int nSector = MirrorUpper[i];
                if (type == 1) {
                    if (mirror) {
                        boardService.getSector(nSector).setCeilingstat(boardService.getSector(nSector).getCeilingstat() | kSectorParallax);
                    } else {
                        boardService.getSector(nSector).setCeilingstat(boardService.getSector(nSector).getCeilingstat() & ~kSectorParallax);
                    }
                } else if (type == 2) {
                    if (mirror) {
                        boardService.getSector(nSector).setFloorstat(boardService.getSector(nSector).getFloorstat() | kSectorParallax);
                    } else {
                        boardService.getSector(nSector).setFloorstat(boardService.getSector(nSector).getFloorstat() & ~kSectorParallax);
                    }
                }
            }
        }
    }

    public static void DrawMirrors(long x, long y, long z, float ang, float horiz) {
        Renderer renderer = game.getRenderer();
        for (int i = mirrorcnt - 1; i >= 0; i--) {
            int nMirror = i + MIRRORLABEL;

            if (renderer.gotPic(nMirror)) {
                int type = MirrorType[i];
                if (type == 0)  //wall mirror
                {
                    int nWall = MirrorLower[i];
                    Wall pWall = boardService.getWall(nWall);
                    int nSector = engine.sectorofwall((short) nWall);
                    short oldNextwall = pWall.getNextwall();
                    short oldNextsector = pWall.getNextsector();
                    pWall.setNextwall((short) MirrorWall[0]);
                    pWall.setNextsector((short) MirrorSector);
                    boardService.getWall(MirrorWall[0]).setNextwall((short) nWall);
                    boardService.getWall(MirrorWall[0]).setNextsector((short) nSector);
                    boardService.getWall(MirrorWall[0]).setX(boardService.getWall(pWall.getPoint2()).getX());
                    boardService.getWall(MirrorWall[0]).setY(boardService.getWall(pWall.getPoint2()).getY());
                    boardService.getWall(MirrorWall[1]).setX(pWall.getX());
                    boardService.getWall(MirrorWall[1]).setY(pWall.getY());
                    boardService.getWall(MirrorWall[2]).setX(16 * (boardService.getWall(MirrorWall[1]).getX() - boardService.getWall(MirrorWall[0]).getX()) + boardService.getWall(MirrorWall[1]).getX());
                    boardService.getWall(MirrorWall[2]).setY(16 * (boardService.getWall(MirrorWall[1]).getY() - boardService.getWall(MirrorWall[0]).getY()) + boardService.getWall(MirrorWall[1]).getY());
                    boardService.getWall(MirrorWall[3]).setX(16 * (boardService.getWall(MirrorWall[0]).getX() - boardService.getWall(MirrorWall[1]).getX()) + boardService.getWall(MirrorWall[0]).getX());
                    boardService.getWall(MirrorWall[3]).setY(16 * (boardService.getWall(MirrorWall[0]).getY() - boardService.getWall(MirrorWall[1]).getY()) + boardService.getWall(MirrorWall[0]).getY());
                    boardService.getSector(MirrorSector).setFloorz(boardService.getSector(nSector).getFloorz());
                    boardService.getSector(MirrorSector).setCeilingz(boardService.getSector(nSector).getCeilingz());
                    int mirX = 0, mirY = 0;
                    float mirAng = 0;
                    if (pWall.getLotag() == 501) {
                        mirX = (int) (x - (boardService.getWall(pWall.getHitag()).getX() - boardService.getWall(pWall.getPoint2()).getX()));
                        mirY = (int) (y - (boardService.getWall(pWall.getHitag()).getY() - boardService.getWall(pWall.getPoint2()).getY()));
                        mirAng = ang;
                    } else {
                        ru.m210projects.Build.Render.Mirror mirror = renderer.preparemirror((int) x, (int) y, (int) z, ang, horiz, nWall, MirrorSector);
                        mirX = (int) mirror.getX();
                        mirY = (int) mirror.getY();
                        mirAng = mirror.getAngle();
                    }

                    renderer.drawrooms(mirX, mirY, z, mirAng, horiz, (short) (MirrorSector + boardService.getSectorCount()));
                    display_mirror = true;
                    viewProcessSprites(mirX, mirY, z);
                    display_mirror = false;
                    renderer.drawmasks();
                    if (pWall.getLotag() != 501) {
                        renderer.completemirror();
                    }
                    pWall.setNextwall(oldNextwall);
                    pWall.setNextsector(oldNextsector);
                } else if (type == 1) //ceiling mirror
                {
                    int nSector = MirrorLower[i];
                    renderer.drawrooms(x + MirrorX[i], MirrorY[i] + y, z + MirrorZ[i], ang, horiz, (short) (nSector + boardService.getSectorCount()));
                    viewProcessSprites(x + MirrorX[i], MirrorY[i] + y, z + MirrorZ[i]);
                    short oldstat = boardService.getSector(nSector).getFloorstat();
                    boardService.getSector(nSector).setFloorstat(boardService.getSector(nSector).getFloorstat() | kSectorParallax);
                    renderer.drawmasks();
                    boardService.getSector(nSector).setFloorstat(oldstat);
                } else if (type == 2) //floor mirror
                {
                    int nSector = MirrorLower[i];
                    renderer.drawrooms(x + MirrorX[i], MirrorY[i] + y, z + MirrorZ[i], ang, horiz, (short) (nSector + boardService.getSectorCount()));
                    viewProcessSprites(x + MirrorX[i], MirrorY[i] + y, z + MirrorZ[i]);
                    short oldstat = boardService.getSector(nSector).getCeilingstat();
                    boardService.getSector(nSector).setCeilingstat(boardService.getSector(nSector).getCeilingstat() | kSectorParallax);
                    renderer.drawmasks();
                    boardService.getSector(nSector).setCeilingstat(oldstat);
                }

                renderer.getRenderedPics()[(nMirror) >> 3] &= (byte) ~pow2char[nMirror & 7];
                return;
            }
        }
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.Factory.BloodNetwork;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.Types.*;
import ru.m210projects.Blood.Types.Seq.SeqHandling;
import ru.m210projects.Blood.Types.Seq.SeqInst;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Gib.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LOADSAVE.lastload;
import static ru.m210projects.Blood.LOADSAVE.loadgame;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.SOUND.*;
import static ru.m210projects.Blood.Strings.seq;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Trigger.*;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.DudeInfo.gPlayerTemplate;
import static ru.m210projects.Blood.Types.ScreenEffect.resetDacEffects;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.*;
import static ru.m210projects.Blood.VERSION.*;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Blood.Warp.*;
import static ru.m210projects.Blood.Weapon.*;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.*;

public class PLAYER implements Serializable<PLAYER> {

    public static final String deathMessage = "Press \"USE\" to load last saved game or press \"ENTER\" to restart level";
    public static final int kInventoryDoctorBag = 0;
    public static final int kInventoryDivingSuit = 1;
    public static final int kInventoryCrystalBall = 2;
    public static final int kInventoryBeastVision = 3;
    public static final int kInventoryJumpBoots = 4;
    public static final int kInventoryJetpack = 5;
    public static final int BLUEARMOR = 0;
    public static final int REDARMOR = 1;
    public static final int GREENARMOR = 2;
    public static final int kOrderMax = 2;
    public static final int kWeaponNone = 0;
    public static final int kWeaponPitchfork = 1;
    public static final int kWeaponFlare = 2;
    public static final int kWeaponShotgun = 3;
    public static final int kWeaponTommy = 4;
    public static final int kWeaponNapalm = 5;
    public static final int kWeaponTNT = 6;
    public static final int kWeaponSprayCan = 7;
    public static final int kWeaponTesla = 8;
    public static final int kWeaponVoodoo = 10;
    public static final int kWeaponLifeLeach = 9;
    public static final int kWeaponProxyTNT = 11;
    public static final int kWeaponRemoteTNT = 12;
    public static final int kWeaponBeast = 13;
    public static final int kWeaponMax = 14;
    public static final int kAmmoNone = -1;
    public static final int kLookMax = 60;
    public static final int kHorizUpMax = 120;
    public static final int kHorizDownMax = 180;
    public static final int kHorizDefault = 90;
    public static final int newHorizDefault = 100;
    public static final int kMoveWalk = 0;
    public static final int kMoveSwim = 1;
    public static final int kMoveCrouch = 2;
    public static final int kMoveFly = 3;
    public static final int kModeHuman = 0;
    public static final int kModeBeast = 1;
    public static final int kModeHumanShrink = 2;
    public static final int kModeHumanGrown = 3;
    // distance in pixels for picking up items and pushing things
    public static final int kTouchXYDist = 48;
    public static final int kTouchZDist = 32;
    public static final int kPushXYDist = 64;
    public static final int[] gLaughs = {734, 735, 736, 737, 738, 739, 740, 741, 3038, 3049};
    public static final int kInventoryMax = 6;
    // this constant determine when the effect starts to wear off
    public static final int kWaneTime = 512;
    public static final FragInfo[] deathAphorisms1 = {new FragInfo(4202, " is excrement", null), new FragInfo(4203, " is hamburger", null), new FragInfo(4204, " suffered scrotum separation", null), new FragInfo(4206, " volunteered for population control", null), new FragInfo(4207, " has suicided", null),};
    public static final FragInfo[] deathAphorisms2 = {new FragInfo(4100, " boned ", " like a fish"), new FragInfo(4101, " castrated ", null), new FragInfo(4102, " creamed ", null), new FragInfo(4103, " destroyed ", null), new FragInfo(4104, " diced ", null), new FragInfo(4105, " disemboweled ", null), new FragInfo(4106, " flattened ", null), new FragInfo(4107, " gave ", " Anal Justice"), new FragInfo(4108, " gave AnAl MaDnEsS to ", null), new FragInfo(4109, " hurt ", " real bad"), new FragInfo(4110, " killed ", null), new FragInfo(4111, " made mincemeat out of ", null), new FragInfo(4112, " massacred ", null), new FragInfo(4113, " mutilated ", null), new FragInfo(4114, " reamed ", null), new FragInfo(4115, " ripped ", " a new orifice"), new FragInfo(4116, " slaughtered ", null), new FragInfo(4117, " sliced ", null), new FragInfo(4118, " smashed ", null), new FragInfo(4119, " sodomized ", null), new FragInfo(4120, " splattered ", null), new FragInfo(4121, " squashed ", null), new FragInfo(4122, " throttled ", null), new FragInfo(4123, " wasted ", null), new FragInfo(4124, " body bagged ", null),};
    private static final boolean gInventorySwap = true;

    //Player struct
    public static PLOCATION[] gPrevView = new PLOCATION[kMaxPlayers];
    public static boolean newHoriz = true;
    public static QAV[] weaponQAVs = new QAV[kQAVEnd];
    public static AMMOINFO[] gAmmoInfo = {new AMMOINFO(0, 0, 255), new AMMOINFO(100, 0, 255), new AMMOINFO(100, 0, 4), new AMMOINFO(500, 0, 5), new AMMOINFO(100, 0, 255), new AMMOINFO(50, 0, 255), new AMMOINFO(2880, 0, 255), new AMMOINFO(250, 0, 255), new AMMOINFO(100, 0, 255), new AMMOINFO(100, 0, 255), new AMMOINFO(50, 0, 255), new AMMOINFO(50, 0, 255)};
    public static DAMAGEINFO[] gDamageInfo = {new DAMAGEINFO(-1, new int[]{731, 732, 733, 710, 710, 710}), new DAMAGEINFO(1, new int[]{742, 743, 744, 711, 711, 711}), new DAMAGEINFO(0, new int[]{731, 732, 733, 712, 712, 712}), new DAMAGEINFO(1, new int[]{731, 732, 733, 713, 713, 713}), new DAMAGEINFO(-1, new int[]{724, 724, 724, 714, 714, 714}), new DAMAGEINFO(2, new int[]{731, 732, 733, 715, 715, 715}), new DAMAGEINFO(0, new int[]{0, 0, 0, 0, 0, 0})};
    public static int timer = 0;
    private static int Action_nIndex = 0;
    private static int Action_nXIndex = -1;
    public BloodSprite pSprite;
    public XSPRITE pXsprite;
    public DudeInfo pDudeInfo;
    public INPUT pInput;
    public int NPSTART;
    public int pWeaponQAV;
    public int weaponCallback;
    public boolean Run;
    public int moveState;
    public int moveDist;
    public int bobPhase;
    public int bobAmp;
    public int bobHeight;
    public int bobWidth;
    public int swayPhase;
    public int swayAmp;
    public int swayHeight;
    public int swayWidth;
    public int nPlayer;
    public int nSprite;
    public int nLifeMode;
    public int bloodlust;
    public int weapOffdZ;
    public int viewOffdZ;
    public int weaponAboveZ;
    public int viewOffZ;
    public float look;
    public float horiz;
    public int slope;
    public float horizOff;
    public boolean Underwater;
    public boolean[] hasKey = new boolean[8];
    public int hasFlag;
    public short nBlueTeam;
    public short nRedTeam;
    //field_95 12bytes
    public int[] damageShift = new int[7];
    public int CrouchMode; //GDX function
    public int LastWeapon; //GDX function
    public int currentWeapon;
    public int updateWeapon;
    public int weaponTimer;
    public int weaponState;
    public int weaponAmmo;
    public boolean[] hasWeapon = new boolean[kWeaponMax];
    public int[] weaponMode = new int[kWeaponMax];
    public int[][] weaponOrder = new int[kOrderMax][kWeaponMax];
    public int[] ammoCount = new int[12];
    public boolean fLoopQAV;
    public int fuseTime;
    public int fireClock;
    public int throwTime;
    public Vector3 aim;
    public Vector3 relAim;
    public int nAimSprite;
    public int aimCount;
    public int[] aimSprites = new int[16];
    public int deathTime;
    public int[] powerUpTimer = new int[kMaxPowerUps];
    public int fragCount;
    public int[] fragInfo = new int[kMaxPlayers];
    public int teamID;
    public int fraggerID;
    public int airTime; // set when first going underwater, decremented while underwater then takes damage
    public int bloodTime;    // set when player walks through blood, decremented when on normal surface
    public int gooTime;    // set when player walks through sewage, decremented when on normal surface
    public int wetTime;    // set when player walks through water, decremented when on normal surface
    public int bubbleTime; // set when first going underwater, decremented while underwater then takes damage
    public int kickTime;
    public int stayTime;
    public int pLaughsCount;
    public int TurnAround;
    public boolean godMode;
    public boolean fScreamed;
    public boolean pJump;
    public int showInventory;
    public int choosedInven;
    public INVITEM[] Inventory = new INVITEM[kInventoryMax];
    public int[] ArmorAmount = new int[3];
    public int explosion;
    public int tilt;
    public int visibility;
    public int fireEffect;
    public int hitEffect;
    public int blindEffect;
    public int drownEffect;
    public int handCount;
    public boolean handDamage;
    public int pickupEffect;
    public int quakeTime;
    public int lookang;
    public int rotscrnang;
    public float ang;
    int voodooTarget;
    int voodooCount;
    int voodooAng;
    int voodooUnk;

    private int nVersion = 277;

    public PLAYER() {
        aim = new Vector3();
        relAim = new Vector3();
        pInput = new INPUT();
        for (int i = 0; i < kInventoryMax; i++) {
            Inventory[i] = new INVITEM();
        }
    }

    public PLAYER setVersion(int nVersion) {
        this.nVersion = nVersion;
        return this;
    }

    public static boolean checkPlayerSeq(PLAYER pPlayer, int nSeqID) {
        final int nXSprite = pPlayer.pSprite.getExtra(); // XXX
        SeqInst pInst = SeqHandling.GetInstance(SS_SPRITE, nXSprite);
        return pInst.getSeqIndex() == (pPlayer.pDudeInfo.seqStartID + nSeqID) && seqFrame(SS_SPRITE, nXSprite) >= 0;
    }

    public static void resetInventory(PLAYER pPlayer) {
        if (pPlayer == null) {
            throw new AssertException("pPlayer != NULL");
        }
        for (int i = 0; i < 14; i++) {
            pPlayer.hasWeapon[i] = gInfiniteAmmo;
            pPlayer.weaponMode[i] = 0;
        }
        pPlayer.hasWeapon[1] = true;
        pPlayer.LastWeapon = 0;
        pPlayer.currentWeapon = 0;
        pPlayer.weaponCallback = -1;
        pPlayer.pInput.newWeapon = 1;

        for (int i = 0; i < 14; i++) {
            pPlayer.weaponOrder[0][i] = defaultOrder[i]; //kOrderAboveWater
            pPlayer.weaponOrder[1][i] = defaultOrder[i]; //kOrderBelowWater
        }

        for (int i = 0; i < 12; i++) {
            pPlayer.ammoCount[i] = gInfiniteAmmo ? gAmmoInfo[i].max : 0;
        }
        for (int i = 0; i < 3; i++) {
            pPlayer.ArmorAmount[i] = 0;
        }
        pPlayer.weaponTimer = 0;
        pPlayer.weaponState = 0;
        pPlayer.pWeaponQAV = -1;
        pPlayer.fLoopQAV = false;
        pPlayer.choosedInven = -1;
        for (int i = 0; i < kInventoryMax; i++) {
            pPlayer.Inventory[i].activated = false;
            pPlayer.Inventory[i].amount = 0;
        }
        gInfiniteAmmo = false;
    }

    public static void playerInit(int nPlayer, boolean nInited) {
        PLAYER pPlayer = gPlayer[nPlayer];

        if (!nInited) {
            System.out.println("Initializing player " + nPlayer);
            pPlayer.reset();
        }

        pPlayer.nPlayer = nPlayer;
        pPlayer.teamID = nPlayer;
        pPlayer.CrouchMode = 0;
        if (pGameInfo.nGameType == kNetModeTeams) {
            if (game.net.gProfile[nPlayer].team == 0) {
                pPlayer.teamID = nPlayer & 1;
            } else {
                pPlayer.teamID = (game.net.gProfile[nPlayer].team - 1);
            }
        }
        pPlayer.fragCount = 0;
        Arrays.fill(nTeamCount, 0);
        Arrays.fill(nTeamClock, 0);
        Arrays.fill(pPlayer.fragInfo, 0);
        if (!nInited) {
            resetInventory(pPlayer);
        }
    }

    public static void playerGodMode(PLAYER pPlayer, int mode) {
        if (mode == 1) {
            for (int i = 0; i < 7; i++) {
                pPlayer.damageShift[i] += 1;
            }
        } else {
            for (int i = 0; i < 7; i++) {
                pPlayer.damageShift[i] -= 1;
            }
        }
        pPlayer.godMode = mode == 1;
    }

    public static void playerReset(int nPlayer) throws WarningException {
        PLAYER pPlayer = gPlayer[nPlayer];

        // get the normal player starting position, else if in bloodbath mode,
        // randomly pick one of kMaxPlayers starting positions
        ZONE pZone = null;
        if (pGameInfo.nGameType == kNetModeOff || pGameInfo.nGameType == kNetModeCoop) {
            pZone = gStartZone[nPlayer];
        }

        // let's check if there is positions of teams is specified
        // if no, pick position randomly, just like it works in vanilla.
        else if (pGameInfo.nGameType == kNetModeTeams && gTeamsSpawnUsed) {
            int maxRetries = 5;
            while (maxRetries-- > 0) {
                //System.err.println("-> TRY #"+maxRetries+" SEARCHING START POINT FOR PLAYER: "+nPlayer+", TEAM: "+pPlayer.teamID);
                if (pPlayer.teamID == 0) {
                    pZone = gStartZoneTeam1[Random(kMaxPlayers / 2)];
                } else {
                    pZone = gStartZoneTeam2[Random(kMaxPlayers / 2)];
                }

                //System.err.println(pZone.x);
                //System.err.println(pZone.y);
                //System.err.println(pZone.z);
                //System.err.println(pZone.sector);

                if (maxRetries != 0) {
                    // check if there is no spawned player in selected zone
                    for (ListNode<Sprite> node = boardService.getSectNode(pZone.sector); node != null; node = node.getNext()) {
                        Sprite pSprite = node.get();
                        if (pZone.x == pSprite.getX() && pZone.y == pSprite.getY() && IsPlayerSprite(pSprite)) {
                            //System.err.println("FOUND PLAYER SPRITE ON START ZONE");
                            pZone = null;
                            break;
                        }
                    }
                }

                if (pZone != null) {
                    break;
                }
            }
        } else {
            pZone = gStartZone[Random(kMaxPlayers)];
        }

        if (pZone.sector == -1) {
            throw new WarningException("The player should be inside a map sector");
        }

        int nSprite = actSpawnSprite(pZone.sector, pZone.x, pZone.y, pZone.z, kStatDude, true);
        if (nSprite == -1) {
            throw new AssertException("The player should be inside a map");
        }

        BloodSprite pSprite = boardService.getSprite(nSprite);
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        pPlayer.pSprite = pSprite;
        pPlayer.pXsprite = pXSprite;
        pPlayer.nSprite = nSprite;
        pPlayer.pDudeInfo = dudeInfo[nPlayer + 31];

        playerSetRace(pPlayer, kModeHuman);
        seqSpawn(pPlayer.pDudeInfo.seqStartID + kSeqDudeIdle, SS_SPRITE, pSprite.getExtra(), null);

        if (pPlayer == gMe) {
            show2dsprite.setBit(pSprite.getXvel());
        }

        GetSpriteExtents(pSprite);

        pPlayer.CrouchMode = 0;
        pPlayer.LastWeapon = 0;

        if (pGameInfo.nGameType == kNetModeTeams) {
            if (game.net.gProfile[nPlayer].team == 0) {
                pPlayer.teamID = nPlayer & 1;
            } else {
                pPlayer.teamID = (game.net.gProfile[nPlayer].team - 1);
            }
        }

        pPlayer.pXsprite.setData1(0); // player SEQ size scale

        pSprite.setZ(pSprite.getZ() - (extents_zBot - pSprite.getZ()));
        pSprite.setPal((byte) ((pPlayer.teamID & 3) + 11));

        pSprite.setAng((short) (pZone.angle & kAngleMask));
        pPlayer.ang = pSprite.getAng();
        pSprite.setLotag((short) (nPlayer + kDudePlayer1));
        pSprite.setClipdist(pPlayer.pDudeInfo.clipdist);
        pSprite.setHitag((kAttrMove | kAttrGravity | kAttrFalling | kAttrAiming));
        pXSprite.setBurnTime(0);
        pXSprite.setBurnSource(-1);
        pXSprite.setHealth(pPlayer.pDudeInfo.startHealth << 4);
        pPlayer.pSprite.setCstat(pPlayer.pSprite.getCstat() & 0x7FFF);
        pPlayer.bloodlust = 0;
        pPlayer.horiz = 0;
        pPlayer.slope = 0;
        pPlayer.look = 0;
        pPlayer.horizOff = 0;
        pPlayer.fraggerID = -1;
        pPlayer.airTime = 1200;
        pPlayer.bloodTime = 0;
        pPlayer.gooTime = 0;
        pPlayer.wetTime = 0;
        pPlayer.bubbleTime = 0;
//		pPlayer.unkTime = 0;
        pPlayer.stayTime = 0;
        pPlayer.kickTime = 0;
        pPlayer.pLaughsCount = 0;
        pPlayer.TurnAround = 0;
        pPlayer.moveState = kMoveWalk;
        pPlayer.voodooTarget = -1;
        pPlayer.voodooCount = 0;
        pPlayer.voodooAng = 0;
        pPlayer.voodooUnk = 0;
        viewUpdatePlayerLoc(pPlayer);
        pPlayer.weapOffdZ = 0;
        pPlayer.relAim.x = 1 << 14;
        pPlayer.relAim.y = 0;
        pPlayer.relAim.z = 0;
        pPlayer.nAimSprite = -1;
        pPlayer.viewOffdZ = pPlayer.weapOffdZ;

        if (pGameInfo.nGameType != kNetModeCoop) {
            for (int i = 0; i < 8; i++) {
                pPlayer.hasKey[i] = (pGameInfo.nGameType >= 2);
            }
        }

        pPlayer.hasFlag = 0;
        pPlayer.nBlueTeam = 0;
        pPlayer.nRedTeam = 0;
        for (int i = 0; i < 7; i++) {
            pPlayer.damageShift[i] = 0;
        }
        if (pPlayer.godMode) {
            playerGodMode(pPlayer, 1);
        }

        for (int i = 0; i < kInventoryMax; i++) {
            pPlayer.Inventory[i].activated = false;
        }

        gFullMap = false;
        pPlayer.throwTime = 0;
        pPlayer.deathTime = 0;
        pPlayer.updateWeapon = 0;

        pSprite.setVelocity(0, 0, 0);

        pPlayer.pInput.Turn = 0;
        pPlayer.pInput.Jump = false;
        pPlayer.pInput.Crouch = false;
        pPlayer.pInput.Shoot = false;
        pPlayer.pInput.AltShoot = false;
        pPlayer.pInput.Lookup = false;
        pPlayer.pInput.Lookdown = false;
        pPlayer.pInput.TurnAround = false;
        pPlayer.pInput.Use = false;
        pPlayer.pInput.InventoryLeft = false;
        pPlayer.pInput.InventoryRight = false;
        pPlayer.pInput.InventoryUse = false;
        pPlayer.pInput.PrevWeapon = false;
        pPlayer.pInput.NextWeapon = false;
        pPlayer.pInput.HolsterWeapon = false;
        pPlayer.pInput.LookCenter = false;
        pPlayer.pInput.Pause = false;
        pPlayer.pInput.Quit = false;
        pPlayer.pInput.Restart = false;
        pPlayer.pInput.Forward = 0;
        pPlayer.pInput.Strafe = 0;
        pPlayer.pInput.mlook = 0;
        pPlayer.explosion = 0;
        pPlayer.quakeTime = 0;
        pPlayer.tilt = 0;
        pPlayer.visibility = 0;
        pPlayer.hitEffect = 0;
        pPlayer.blindEffect = 0;
        pPlayer.drownEffect = 0;
        pPlayer.handCount = 0;
        pPlayer.weaponTimer = 0;
        pPlayer.weaponState = 0;
        pPlayer.pWeaponQAV = -1;
        pPlayer.handDamage = false;
        resetDacEffects();

        pPlayer.lookang = 0;
        pPlayer.rotscrnang = 0;

        Arrays.fill(pPlayer.powerUpTimer, 0);
        resetPlayerSize(pPlayer);

        if (pPlayer == gMe) {
            ((BloodNetwork) game.pNet).PredictReset();
        }

        if (IsUnderwaterSector(pSprite.getSectnum())) {
            pPlayer.moveState = kMoveSwim;
            pPlayer.pXsprite.setPalette(1);
        }
    }

    public static void playerSetRace(PLAYER pPlayer, int nLifeMode) {
        if (!(nLifeMode >= kModeHuman && nLifeMode <= kModeHumanGrown)) {
            throw new AssertException("nLifeMode >= kModeHuman && nLifeMode <= kModeHumanGrown");
        }
        pPlayer.nLifeMode = nLifeMode;
        pPlayer.pDudeInfo.copy(gPlayerTemplate[nLifeMode]);
        for (int i = 0; i < 7; i++) {
            pPlayer.pDudeInfo.damageShift[i] = mulscale(pPlayer.pDudeInfo.startDamage[i], pPlayerShift[pGameInfo.nDifficulty], 8);
        }
    }

    public static boolean PickupLeech(PLAYER pPlayer, Sprite pSprite) {
        for (ListNode<Sprite> node = boardService.getStatNode(kStatThing); node != null; node = node.getNext()) {
            if (pSprite == null || pSprite.getXvel() != node.getIndex()) {
                Sprite pThing = node.get();
                if (pThing.getLotag() == kThingLifeLeech && actGetBurnSource(pThing.getOwner()) == pPlayer.nSprite) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int ActionScan(PLAYER pPlayer) {
        final Sprite pPlayerSprite = pPlayer.pSprite;

        Action_nIndex = 0;
        Action_nXIndex = -1;

        int dx = Cos(pPlayerSprite.getAng()) >> 16;
        int dy = Sin(pPlayerSprite.getAng()) >> 16;
        int dz = (int) pPlayer.horizOff;

        int hitType = HitScan(pPlayerSprite, pPlayer.viewOffZ, dx, dy, dz, pHitInfo, 0x10000040, 128);
        int hitDist = EngineUtils.qdist(pPlayerSprite.getX() - pHitInfo.hitx, pPlayerSprite.getY() - pHitInfo.hity) >> 4;

        if (hitDist < kPushXYDist) {
            switch (hitType) {
                case SS_SPRITE:
                    Action_nIndex = pHitInfo.hitsprite;
                    final BloodSprite pSprite = boardService.getSprite(Action_nIndex);
                    Action_nXIndex = pSprite.getExtra();
                    XSPRITE pXSprite = boardService.getXSprite(Action_nXIndex);

                    if (pXSprite != null && pSprite.getStatnum() == 4 && pSprite.getLotag() == kThingLifeLeech) {
                        if (pGameInfo.nGameType > 1 && PickupLeech(pPlayer, pSprite)) {
                            return -1;
                        }

                        pXSprite.setData4(pPlayer.nPlayer);
                        pXSprite.setTriggered(false);
                    }


                    if (pXSprite != null && pXSprite.isPush()) {
                        return SS_SPRITE;
                    }

                    if (pSprite.getStatnum() == kStatDude) {
                        if (pGameInfo.nReviveMode && pGameInfo.nGameType == kNetModeCoop && IsPlayerSprite(pSprite)) {
                            PLAYER pReviving = gPlayer[pSprite.getLotag() - kDudePlayer1];
                            if (pReviving.deathTime > 0) {
                                seqSpawn(pReviving.pDudeInfo.seqStartID + getPlayerSeq(kPlayerFatalityDead), SS_SPRITE, pReviving.pSprite.getExtra(), callbacks[ReviveCallback]);
                                break;
                            }
                        }

//						dprintf("DUDE %d: ang=%d, goalAng=%d\n", Action_nIndex, pDude.ang, pXDude.goalAng);
                        int mass = dudeInfo[pSprite.getLotag() - kDudeBase].mass;
                        if (mass > 0) {
                            int impulse = 0xCCCCC00 / mass;
                            pSprite.addVelocity(
                            mulscale(impulse, dx, 16),
                            mulscale(impulse, dy, 16),
                            mulscale(impulse, dz, 16));
                        }
                        if (pXSprite != null && pXSprite.isPush() && pXSprite.getState() == 0 && !pXSprite.isTriggered()) {
                            trTriggerSprite(Action_nIndex, pXSprite, kCommandSpritePush);
                        }
                    }
                    break;

                case SS_MASKED:
                case SS_WALL:
                    Action_nIndex = pHitInfo.hitwall;
                    Wall pWall = boardService.getWall(Action_nIndex);
                    Action_nXIndex = pWall.getExtra();
                    if (Action_nXIndex >= 0 && xwall[Action_nXIndex].triggerPush) {
                        return SS_WALL;
                    }

                    if (pWall.getNextsector() >= 0) {
                        Action_nIndex = pWall.getNextsector();
                        Action_nXIndex = boardService.getSector(Action_nIndex).getExtra();
                        if (Action_nXIndex >= 0 && xsector[Action_nXIndex].Wallpush) {
                            return SS_SECTOR;
                        }
                    }
                    break;

                case SS_FLOOR:
                case SS_CEILING:
                    Action_nIndex = pHitInfo.hitsect;
                    Action_nXIndex = boardService.getSector(Action_nIndex).getExtra();
                    if (Action_nXIndex >= 0 && xsector[Action_nXIndex].Push) {
                        return SS_SECTOR;
                    }
                    break;
            }
        }

        Action_nIndex = pPlayerSprite.getSectnum();
        Action_nXIndex = boardService.getSector(Action_nIndex).getExtra();
        if (Action_nXIndex >= 0 && xsector[Action_nXIndex].Push) {
            return SS_SECTOR;
        }

        return -1;
    }

    public static void ProcessInput(PLAYER pPlayer) {
        long vel;
        final BloodSprite pSprite = pPlayer.pSprite;
        POSTURE cp = gPosture[pPlayer.nLifeMode][pPlayer.moveState];
        final int nXSprite = pPlayer.pSprite.getExtra(); // XXX

        if (nXSprite == -1) {
            return; //disconnected?
        }

        XSPRITE pXSprite = pPlayer.pXsprite;
        pPlayer.Run = pPlayer.pInput.Run;


        if (!gNoClip && !IsOriginalDemo()) {
            short nSector = pSprite.getSectnum();
            GetSpriteExtents(pSprite);
            int floorDist = (extents_zBot - pSprite.getZ()) / 4;
            int ceilDist = (pSprite.getZ() - extents_zTop) / 4;
            int clipDist = pSprite.getClipdist() << 2;

            int push = engine.pushmove(pSprite.getX(), pSprite.getY(), pSprite.getZ(), nSector, clipDist, ceilDist, floorDist, CLIPMASK0);
            pSprite.setX(pushmove_x);
            pSprite.setY(pushmove_y);
            pSprite.setZ(pushmove_z);
            nSector = pushmove_sectnum;

            if (push == -1) {
                actDamageSprite(pPlayer.nSprite, pSprite, kDamageFall, 8000);
            }

            if (nSector != pSprite.getSectnum()) {
                if (nSector == -1) {
                    nSector = pSprite.getSectnum();
                    actDamageSprite(pSprite.getXvel(), pSprite, kDamageFall, 8000);
                }
                if (!boardService.isValidSector(nSector)) {
                    throw new AssertException("boardService.isValidSector(nSector)");
                }
                engine.changespritesect((short) pPlayer.nSprite, nSector);
            }
        }

        if (pPlayer.pInput.Forward != 0 || pPlayer.pInput.Turn != 0 || pPlayer.pInput.Strafe != 0 || pPlayer.pInput.Jump || pPlayer.pInput.Crouch || pPlayer.pInput.Lookup || pPlayer.pInput.Lookdown || pPlayer.pInput.AltShoot || pPlayer.pInput.Shoot) {
            pPlayer.stayTime = 0;
        } else if (pPlayer.stayTime >= 0) {
            pPlayer.stayTime += kFrameTicks;
        }

        WeaponProcess(pPlayer);

        // quick hack for death
        if (pXSprite.getHealth() == 0) {
            boolean noLastLoad = false;
            if (pGameInfo.nGameType == 0 && game.isCurrentScreen(gGameScreen) && lastload != null && lastload.exists()) {
                if (pPlayer.deathTime == 0) {
                    QUOTE quote = viewSetMessage(deathMessage, -1, 7);
                    if (quote != null) {
                        quote.messageTime = kTimerRate * 65536 + gFrameClock;
                    }
                } else {
                    if (numQuotes == 0) {
                        QUOTE quote = viewSetMessage(deathMessage, -1, 7);
                        if (quote != null) {
                            quote.messageTime = kTimerRate * 65536 + gFrameClock;
                        }
                    }
                }

                if (pPlayer.pInput.Use) {
                    game.changeScreen(gLoadingScreen.setTitle(lastload.getName()));
                    gLoadingScreen.init(() -> {
                        if (!loadgame(lastload)) {
                            game.GameMessage("Can't load game!");
                        }
                    });
                    pPlayer.pInput.Use = false;
                    return;
                }

                if (game.getProcessor().isKeyPressed(Input.Keys.ENTER)) {
                    noLastLoad = true;
                }
            }

            boolean deathType = checkPlayerSeq(pPlayer, getPlayerSeq(kPlayerFatality));
            if (pPlayer.fraggerID != -1 && pPlayer.fraggerID != pPlayer.nSprite) {
                pSprite.setAng(EngineUtils.getAngle(boardService.getSprite(pPlayer.fraggerID).getX() - pSprite.getX(), boardService.getSprite(pPlayer.fraggerID).getY() - pSprite.getY()));
                pPlayer.ang = pSprite.getAng();
            }
            pPlayer.deathTime += kFrameTicks;

            if (!deathType && (pGameInfo.nGameType != kNetModeCoop || !pGameInfo.nReviveMode)) {
                pPlayer.horiz = mulscale((1 << 15) - (Cos(ClipHigh(pPlayer.deathTime << 3, kAngle180)) >> 15), kHorizUpMax, 16);
            }

            SOUND.setReverb(false, 0.0f);
            pPlayer.LastWeapon = 0;
            pPlayer.CrouchMode = 0;
            if (pPlayer.currentWeapon != 0) {
                pPlayer.pInput.newWeapon = pPlayer.currentWeapon;    // force weapon down
            }

            if (pPlayer.pInput.Use || noLastLoad) {
                if (deathType) {
                    if (pGameInfo.nGameType != kNetModeCoop) {
                        if (pPlayer.deathTime > 360) {
                            seqSpawn(pPlayer.pDudeInfo.seqStartID + getPlayerSeq(kPlayerFatalityDead), SS_SPRITE, nXSprite, callbacks[ReviveCallback]);
                            pPlayer.pInput.Use = false;
                            return;
                        }
                    }
                }

                if (!game.isCurrentScreen(gDemoScreen) && (seqFrame(SS_SPRITE, nXSprite) < 0 || (pGameInfo.nGameType == kNetModeCoop && pGameInfo.nReviveMode && pPlayer.deathTime > 100))) {
                    sfxKillAll3DSounds();
                    if (pPlayer.pSprite != null) {
                        pPlayer.pSprite.setLotag(426);
                    }
                    actPostSprite(pPlayer.nSprite, 4);
                    seqSpawn(pPlayer.pDudeInfo.seqStartID + getPlayerSeq(kPlayerDead), 3, nXSprite, null);
                    resetInventory(pPlayer);
                    if (pGameInfo.nGameType == 0 && numplayers == 1) {
//			        	if ( Demo.byte0 )
//			            	recordDemoFunc(Demo);

                        pPlayer.pInput.Restart = true;
                        pPlayer.pInput.Use = false;
                        pPlayer.hitEffect = 0;

                        return;
                    }

                    playerReset(pPlayer.nPlayer);
                }

                pPlayer.pInput.Use = false;
            }

            return; // Don't allow the player to do anything else if dead
        }

        if (pPlayer.moveState == 1) {
            if (pPlayer.pInput.Forward != 0) {
                pSprite.addVelocityX(mulscale(Cos(pSprite.getAng()), (long) pPlayer.pInput.Forward * cp.frontAccel, 30)); //4608
                pSprite.addVelocityY(mulscale(Sin(pSprite.getAng()), (long) pPlayer.pInput.Forward * cp.frontAccel, 30)); //4608
            }

            if (pPlayer.pInput.Strafe != 0) {
                pSprite.addVelocityX(mulscale(Sin(pSprite.getAng()), (long) pPlayer.pInput.Strafe * cp.sideAccel, 30)); //4608
                pSprite.addVelocityY(-mulscale(Cos(pSprite.getAng()), (long) pPlayer.pInput.Strafe * cp.sideAccel, 30)); //4608
            }
        } else {
            if (pXSprite.getHeight() < 256) {
                int zvel = 65536;
                if (pXSprite.getHeight() != 0) {
                    zvel -= (pXSprite.getHeight() << 16) / 256;
                }

                if (pPlayer.pInput.Forward != 0) {
                    if (pPlayer.pInput.Forward > 0) {
                        vel = (long) pPlayer.pInput.Forward * cp.frontAccel;
                    } else {
                        vel = (long) pPlayer.pInput.Forward * cp.backAccel;
                    }

                    if (pXSprite.getHeight() != 0) {
                        vel = mulscale(zvel, vel, 16);
                    }

                    pSprite.addVelocityX(mulscale(Cos(pSprite.getAng()), vel, 30));
                    pSprite.addVelocityY(mulscale(Sin(pSprite.getAng()), vel, 30));
                }

                if (pPlayer.pInput.Strafe != 0) {
                    vel = (long) pPlayer.pInput.Strafe * cp.sideAccel;
                    if (pXSprite.getHeight() != 0) {
                        vel = mulscale(zvel, vel, 16);
                    }

                    pSprite.addVelocityX(mulscale(Sin(pSprite.getAng()), vel, 30));
                    pSprite.addVelocityY(-mulscale(Cos(pSprite.getAng()), vel, 30));
                }
            }
        }

        // turn player


        if (IsOriginalDemo()) {
            if (pPlayer.pInput.Turn != 0) {
                pSprite.setAng((short) ((pSprite.getAng() + (kFrameTicks * (int) pPlayer.pInput.Turn >> 4)) & kAngleMask));
                pPlayer.ang = pSprite.getAng();
            }
        } else {
            if (pPlayer.pInput.Turn != 0) {
                pPlayer.ang = BClampAngle(pPlayer.ang + (kFrameTicks * pPlayer.pInput.Turn / 16.0f));
            }
            pSprite.setAng((short) pPlayer.ang);
        }

        if (pPlayer.pInput.TurnAround) {
            if (pPlayer.TurnAround == 0) {
                pPlayer.TurnAround = -1024;
            }
            pPlayer.pInput.TurnAround = false;
        }

        if (pPlayer.TurnAround < 0) {
            int angSpeed;
            if (pPlayer.moveState == kMoveSwim) {
                angSpeed = 64;
            } else {
                angSpeed = 128;
            }

            pPlayer.TurnAround = ClipHigh(pPlayer.TurnAround + angSpeed, 0);
            pPlayer.ang = BClampAngle(pPlayer.ang + angSpeed);
            if (IsOriginalDemo()) {
                pSprite.setAng((short) pPlayer.ang);
            }
        }

        pPlayer.lookang -= (pPlayer.lookang >> 2);
        if (pPlayer.lookang != 0 && (pPlayer.lookang >> 2) == 0) {
            pPlayer.lookang -= ksgn(pPlayer.lookang);
        }

        if (pPlayer.pInput.LookLeft) {
            pPlayer.lookang -= 152;
//			pPlayer.rotscrnang += 24;
        }

        if (pPlayer.pInput.LookRight) {
            pPlayer.lookang += 152;
//			pPlayer.rotscrnang -= 24;
        }

        if (!pPlayer.pInput.Jump) {
            pPlayer.pJump = false;
        } else {
            pPlayer.CrouchMode = 0;
        }

        if (pPlayer.pInput.Crouch) {
            pPlayer.CrouchMode = 0;
        }

        if (pPlayer.moveState == kMoveSwim) {
            pPlayer.CrouchMode = pPlayer.pInput.CrouchMode ? 1 : 0;
        } else if (pPlayer.pInput.CrouchMode) {
            pPlayer.CrouchMode ^= 1;
        }

        if (pPlayer.CrouchMode == 1) {
            pPlayer.pInput.Crouch = true;
        }

        if (pPlayer.pInput.LastWeapon) {
            pPlayer.pInput.newWeapon = pPlayer.LastWeapon;
        }

        switch (pPlayer.moveState) {
            case kMoveWalk:
                if (!pPlayer.pJump && pPlayer.pInput.Jump && pXSprite.getHeight() == 0) {
                    sfxStart3DSound(pPlayer.pSprite, 700, -1, 0); //kSfxPlayJump


                    if (inventoryCheck(pPlayer, kInventoryJumpBoots)) {
                        pSprite.setVelocityZ(-1529173);
                    } else {
                        pSprite.setVelocityZ(-764586);
                    }

//				if (isShrinked(pPlayer.pSprite)) sprZVel[nSprite]-= -200000;
//				else if (isGrown(pPlayer.pSprite)) sprZVel[nSprite]+= -250000;

                    pPlayer.pJump = true;
                }
                if (pPlayer.pInput.Crouch) {
                    pPlayer.moveState = kMoveCrouch;
                }

                break;
            case kMoveSwim:
                if (pPlayer.pInput.Jump) {
                    pSprite.addVelocityZ(-23301);
                }
                if (pPlayer.pInput.Crouch) {
                    pSprite.addVelocityZ(23301);
                }

                break;
            case kMoveFly:
                if (pPlayer.pInput.Jump) {
                    pSprite.setVelocityZ(-400000);
                } else if (pPlayer.pInput.Crouch) {
                    pSprite.setVelocityZ(400000);
                } else {
                    pSprite.setVelocityZ(0);
                }
                break;

            case kMoveCrouch:
                if (!pPlayer.pInput.Crouch) {
                    pPlayer.moveState = kMoveWalk;
                }
                break;
        }

        if (pPlayer.pInput.Use) {
            int keyId;
            switch (ActionScan(pPlayer)) {
                case SS_SECTOR: {
                    XSECTOR pXSector = xsector[Action_nXIndex];
                    keyId = pXSector.Key;

                    if (pXSector.locked != 0 && pPlayer == gMe) {
                        viewSetMessage("It's locked", pPlayer.nPlayer, 8);
                        sndStartSample(3062, -1, 1, false);
                    }

                    if (keyId == 0 || pPlayer.hasKey[keyId]) {
                        trTriggerSector(Action_nIndex, pXSector, kCommandSpritePush);
                    } else if (pPlayer == gMe) {
                        viewSetMessage("That requires a key.", pPlayer.nPlayer, 8);
                        sndStartSample(3063, -1, 1, false);
                    }
                    break;
                }

                case SS_WALL: {
                    XWALL pXWall = xwall[Action_nXIndex];
                    keyId = pXWall.key;

                    if (pXWall.locked != 0 && pPlayer == gMe) {
                        viewSetMessage("It's locked", pPlayer.nPlayer, 8);
                        sndStartSample(3062, -1, 1, false);
                    }

                    if (keyId == 0 || pPlayer.hasKey[keyId]) {
                        trTriggerWall(Action_nIndex, pXWall, kCommandWallPush);
                    } else if (pPlayer == gMe) {
                        viewSetMessage("That requires a key.", pPlayer.nPlayer, 8);
                        sndStartSample(3063, -1, 1, false);
                    }
                    break;
                }

                case SS_SPRITE: {
                    XSPRITE pXSpr = boardService.getXSprite(Action_nXIndex);
                    if (pXSpr == null) {
                        break;
                    }

                    keyId = pXSpr.getKey();
                    if (pXSpr.getLocked() != 0 && pPlayer == gMe && pXSpr.getLockMsg() != 0) {
//						viewSetMessage("It's locked");
                        trTextOver(pXSpr.getLockMsg());
                    }

                    if (keyId != 0 && !pPlayer.hasKey[keyId]) {
                        if (pPlayer == gMe) {
                            viewSetMessage("That requires a key.", pPlayer.nPlayer, 8);
                            sndStartSample(3063, -1, 1, false);
                        }
                    } else {
                        trTriggerSprite(Action_nIndex, pXSpr, kCommandSpritePush);
                    }


                    break;
                }
            }

            if (pPlayer.handCount > 0) {
                if (IsOriginalDemo()) {
                    pPlayer.handCount = ClipLow(pPlayer.handCount - kFrameTicks * (6 - pGameInfo.nDifficulty), 0);
                } else {
                    pPlayer.handCount = ClipLow(pPlayer.handCount - kFrameTicks * (7 - pGameInfo.nDifficulty), 0);
                }
            }

            if (pPlayer.handDamage && pPlayer.handCount == 0) {
                BloodSprite pHand = actSpawnDude(pSprite, kDudeHand, pSprite.getClipdist() << 1);
                pHand.setAng((short) ((pSprite.getAng() + kAngle180) & kAngleMask));
                pHand.setVelocity(pSprite.getVelocityX() + mulscale(Cos(pSprite.getAng()) >> 16, 0x155555, 14),
                 pSprite.getVelocityY() + mulscale(Sin(pSprite.getAng()) >> 16, 0x155555, 14),
                 pSprite.getVelocityZ());
                pPlayer.handDamage = false;
            }

            pPlayer.pInput.Use = false;
        }

        pPlayer.look = BClipRange(pPlayer.look + pPlayer.pInput.mlook, -kLookMax, kLookMax);

        if (pPlayer.pInput.InventoryLeft) {
            InventoryLeft(pPlayer);
            pPlayer.pInput.InventoryLeft = false;
        }

        if (pPlayer.pInput.InventoryRight) {
            InventoryRight(pPlayer);
            pPlayer.pInput.InventoryRight = false;
        }

        if (pPlayer.pInput.InventoryUse) {
            if (pPlayer.choosedInven != -1 && pPlayer.Inventory[pPlayer.choosedInven].amount > 0) {
                processInventory(pPlayer, pPlayer.choosedInven);
            }
            pPlayer.pInput.InventoryUse = false;

        }

        if (pPlayer.pInput.UseBeastVision) //1
        {
            if (pPlayer.Inventory[3].amount > 0) {
                processInventory(pPlayer, 3);
            }
            pPlayer.pInput.UseBeastVision = false;
        }
        if (pPlayer.pInput.UseCrystalBall) // 2
        {
            if (pPlayer.Inventory[2].amount > 0) {
                processInventory(pPlayer, 2);
            }
            pPlayer.pInput.UseCrystalBall = false;
        }
        if (pPlayer.pInput.UseJumpBoots) // 4
        {
            if (pPlayer.Inventory[4].amount > 0) {
                processInventory(pPlayer, 4);
            }
            pPlayer.pInput.UseJumpBoots = false;
        }
        if (pPlayer.pInput.UseMedKit) //8
        {
            if (pPlayer.Inventory[0].amount > 0) {
                processInventory(pPlayer, 0);
            }
            pPlayer.pInput.UseMedKit = false;
        }

        if (pPlayer.pInput.HolsterWeapon) {
            pPlayer.pInput.HolsterWeapon = false;
            if (pPlayer.currentWeapon != 0) {
                pPlayer.LastWeapon = pPlayer.currentWeapon;
                WeaponLower(pPlayer);
                viewSetMessage("Holstering weapon", pPlayer.nPlayer, 10);
            }
        }

        if (pPlayer.pInput.LookCenter && !pPlayer.pInput.Lookup && !pPlayer.pInput.Lookdown) {
            if (pPlayer.look < 0) {
                pPlayer.look = ClipHigh(pPlayer.look + kFrameTicks, 0);
            }

            if (pPlayer.look > 0) {
                pPlayer.look = ClipLow(pPlayer.look - kFrameTicks, 0);
            }

            if (pPlayer.look == 0) {
                pPlayer.pInput.LookCenter = false;
            }
        } else {
            if (pPlayer.pInput.Lookup) {
                pPlayer.look = ClipHigh(pPlayer.look + kFrameTicks, kLookMax);
            }

            if (pPlayer.pInput.Lookdown) {
                pPlayer.look = ClipLow(pPlayer.look - kFrameTicks, -kLookMax);
            }
        }

        if (IsOriginalDemo()) {
            if (pPlayer.look > 0) {
                pPlayer.horiz = mulscale(kHorizUpMax, Sin((int) (pPlayer.look * (kAngle90 / kLookMax))), 30);
            } else if (pPlayer.look < 0) {
                pPlayer.horiz = mulscale(kHorizDownMax, Sin((int) (pPlayer.look * (kAngle90 / kLookMax))), 30);
            } else {
                pPlayer.horiz = 0;
            }
        } else {
            if (pPlayer.look > 0) {
                pPlayer.horiz = (float) (BSinAngle(pPlayer.look * ((float) kAngle90 / kLookMax)) * kHorizUpMax / 16384.0f);
            } else if (pPlayer.look < 0) {
                pPlayer.horiz = (float) (BSinAngle(pPlayer.look * ((float) kAngle90 / kLookMax)) * kHorizDownMax / 16384.0f);
            } else {
                pPlayer.horiz = 0;
            }
        }

        int floorhit = pXSprite.getSpriteHit().floorHit & kHitTypeMask;
        if (pXSprite.getHeight() < 16 && (floorhit == kHitFloor || floorhit == 0) && (boardService.getSector(pSprite.getSectnum()).getFloorstat() & 2) != 0) {
            int oldslope = engine.getflorzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY());
            int dx = mulscale(64, Cos(pSprite.getAng()), 30) + pSprite.getX();
            int dy = mulscale(64, Sin(pSprite.getAng()), 30) + pSprite.getY();

            int nSector = engine.updatesector(dx, dy, pSprite.getSectnum());
            if (nSector == pSprite.getSectnum()) {
                int newslope = engine.getflorzofslope(nSector, dx, dy);

                int slope = (((oldslope - newslope) >> 3) - pPlayer.slope) << 14;
                pPlayer.slope += (slope >> 16);
            }
        } else {
            int slope = pPlayer.slope;
            int newslope = -slope << 14;
            newslope = slope + (newslope >> 16);
            pPlayer.slope = newslope;
            if (newslope < 0) {
                newslope = -(slope + ((-slope << 14) >> 16));
            }
            if (newslope < 4) {
                pPlayer.slope = 0;
            }
        }

        pPlayer.horizOff = -128 * pPlayer.horiz;

        PickUp(pPlayer);
    }

    public static int CheckTouchSprite(Sprite pSprite) {
        int dx, dy, dz;

        for (ListNode<Sprite> node = boardService.getStatNode(kStatItem); node != null; node = node.getNext()) {
            int i = node.getIndex();
            Sprite pItem = node.get();
            if ((pItem.getHitag() & kAttrFree) != 0) {
                continue;
            }

            dx = klabs(pSprite.getX() - pItem.getX()) >> 4;
            if (dx < kTouchXYDist) {
                dy = klabs(pSprite.getY() - pItem.getY()) >> 4;
                if (dy < kTouchXYDist) {
                    GetSpriteExtents(pSprite);
                    dz = 0;
                    if (boardService.getSprite(i).getZ() < extents_zTop) {
                        dz = (extents_zTop - pItem.getZ()) >> 8;
                    } else if (pItem.getZ() > extents_zBot) {
                        dz = (pItem.getZ() - extents_zBot) >> 8;
                    }
                    if (dz < kTouchZDist) {
                        if (EngineUtils.qdist(dx, dy) < kTouchXYDist) {

                            GetSpriteExtents(pItem);
                            if (
                                // center
                                    engine.cansee(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pItem.getX(), pItem.getY(), pItem.getZ(), pItem.getSectnum()) ||

                                            // top
                                            engine.cansee(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pItem.getX(), pItem.getY(), extents_zTop, pItem.getSectnum()) ||

                                            // bottom
                                            engine.cansee(pSprite.getX(), pSprite.getY(), pSprite.getZ(), pSprite.getSectnum(), pItem.getX(), pItem.getY(), extents_zBot, pItem.getSectnum())) {
                                return i;
                            }
                        }
                    }
                }
            }
        }
        return -1;
    }

    public static void PickUp(PLAYER pPlayer) {
        Sprite pSprite = pPlayer.pSprite;
        String buffer = "";

        int dx, dy, dz;

        for (ListNode<Sprite> node = boardService.getStatNode(kStatItem); node != null; node = node.getNext()) {
            int i = node.getIndex();
            Sprite pItem = node.get();
            if ((pItem.getHitag() & kAttrFree) != 0) {
                continue;
            }

            int x = pSprite.getX();
            int y = pSprite.getY();
            int z = pSprite.getZ();
            int sectnum = pSprite.getSectnum();
            int ix = pItem.getX();
            int iy = pItem.getY();
            int iz = pItem.getZ();
            int isectnum = pItem.getSectnum();

            dx = klabs(x - ix) >> 4;
            if (dx > kTouchXYDist) {
                continue;
            }

            dy = klabs(y - iy) >> 4;
            if (dy > kTouchXYDist) {
                continue;
            }

            GetSpriteExtents(pSprite);
            dz = 0;
            if (iz < extents_zTop) {
                dz = (extents_zTop - iz) >> 8;
            } else if (iz > extents_zBot) {
                dz = (iz - extents_zBot) >> 8;
            }

            if (dz > kTouchZDist) {
                continue;
            }

            if (EngineUtils.qdist(dx, dy) > kTouchXYDist) {
                continue;
            }

            GetSpriteExtents(pItem);
            if (engine.cansee(x, y, z, sectnum, ix, iy, iz, isectnum) || // center
                    engine.cansee(x, y, z, sectnum, ix, iy, extents_zTop, isectnum) || // top
                    engine.cansee(x, y, z, sectnum, ix, iy, extents_zBot, isectnum)) {  // bottom

                boolean bPickedUp = false;
                int customMsg = -1;
                int nType = pItem.getLotag();
                if (nType != 80 && nType != 40) { // No pickup for random item generators.
                    XSPRITE pXSprite = boardService.getXSprite(pItem.getExtra());
                    if (pXSprite != null && pXSprite.getTxID() != 3 && pXSprite.getLockMsg() > 0) {
                        customMsg = pXSprite.getLockMsg();
                    }

                    if (nType >= kItemBase && nType <= kItemMax) {
                        bPickedUp = PickupItem(pPlayer, i, nType);
                        if (bPickedUp && customMsg == -1) {
                            buffer = "Picked up " + gItemText[nType - kItemBase];
                        }
                    } else if (nType >= kAmmoItemBase && nType < kAmmoItemMax) {
                        bPickedUp = PickupAmmo(pPlayer, i, nType);
                        if (bPickedUp && customMsg == -1) {
                            buffer = "Picked up " + gAmmoText[nType - kAmmoItemBase];
                        }
                    } else if (nType >= kWeaponItemBase && nType < kWeaponItemMax) {
                        bPickedUp = PickupWeapon(pPlayer, i, nType);
                        if (bPickedUp && customMsg == -1) {
                            buffer = "Picked up " + gWeaponText[nType - kWeaponItemBase];
                        }
                    }
                }

                if (bPickedUp) {
                    XSPRITE pXItem = boardService.getXSprite(pItem.getExtra());
                    if (pXItem != null && pXItem.isPickup()) {
                        trTriggerSprite(i, pXItem, kCommandSpritePickup);
                    }

                    if (!actCheckRespawn(pItem)) {
                        actPostSprite(i, kStatFree);
                    }

                    pPlayer.pickupEffect = 30;
                    if (pPlayer == gMe) {
                        if (customMsg > 0) {
                            trTextOver(customMsg - 1);
                        } else {
                            viewSetMessage(buffer, pPlayer.nPlayer);
                        }
                    }
                }
            }
        }
    }

    public static boolean PickupItem(PLAYER pPlayer, int nSprite, int nItemType) {
        int soundId = 775;
        Sprite pSprite = pPlayer.pSprite;
        XSPRITE pXSprite = pPlayer.pXsprite;

        int nPowerUp = nItemType - kItemBase;
        switch (nItemType) {


//		case kItemLtdInvisibility:
//			if (isGrown(pPlayer.pSprite)) return false;
//			case kItemShroomShrink:
//			case kItemShroomGrow:
//				switch (nItemType) {
//					case kItemShroomShrink:
//						if (isShrinked(pSprite)) return false;
//						break;
//					case kItemShroomGrow:
//						if (isGrown(pSprite)) return false;
//						break;
//				}
//				powerupActivate(pPlayer, nPowerUp);
//				break;
            case kItemKey1:
            case kItemKey2:
            case kItemKey3:
            case kItemKey4:
            case kItemKey5:
            case kItemKey6:
            case kItemKey7:
                if (pPlayer.hasKey[nItemType - kItemKey1 + 1]) {
                    return false;
                }
                pPlayer.hasKey[nItemType - kItemKey1 + 1] = true;
                soundId = 781;
                break;

            case kItemPotion1:
            case kItemMedPouch:
            case kItemLifeEssence:
            case kItemLifeSeed:
                int addPower = gPowerUpInfo[nPowerUp].addPower;
                XSPRITE pXPower = boardService.getXSprite(boardService.getSprite(nSprite).getExtra());
                if (pXPower != null && pXPower.getData1() > 0 && !IsOriginalDemo()) {
                    addPower = pXPower.getData1();
                }

                return actHealDude(pXSprite, addPower, gPowerUpInfo[nPowerUp].maxPower);
            case kItemCrystalBall:
                if (pGameInfo.nGameType == 0 || !PickupInventryItem(pPlayer, gItemInfo[nPowerUp].nInventory)) {
                    return false;
                }
                break;
            case kItemDoctorBag:
            case kItemJumpBoots:
            case kItemDivingSuit:
            case kItemBeastVision:
                if (!PickupInventryItem(pPlayer, gItemInfo[nPowerUp].nInventory)) {
                    return false;
                }
                break;
            case kItemGunsAkimbo:
                if (!powerupActivate(pPlayer, nPowerUp)) {
                    return false;
                }
                break;
            case kItemBasicArmor:
            case kItemBodyArmor:
            case kItemFireArmor:
            case kItemSpiritArmor:
            case kItemSuperArmor:
                boolean ret = false;
                ARMORITEMDATA pArmorData = gArmorItemData[nItemType - kArmorItemBase];

                if (pPlayer.ArmorAmount[BLUEARMOR] < pArmorData.max[BLUEARMOR]) {
                    pPlayer.ArmorAmount[BLUEARMOR] = ClipHigh(pPlayer.ArmorAmount[BLUEARMOR] + pArmorData.count[BLUEARMOR], pArmorData.max[BLUEARMOR]);
                    ret = true;
                }
                if (pPlayer.ArmorAmount[REDARMOR] < pArmorData.max[REDARMOR]) {
                    pPlayer.ArmorAmount[REDARMOR] = ClipHigh(pPlayer.ArmorAmount[REDARMOR] + pArmorData.count[REDARMOR], pArmorData.max[REDARMOR]);
                    ret = true;
                }
                if (pPlayer.ArmorAmount[GREENARMOR] < pArmorData.max[GREENARMOR]) {
                    pPlayer.ArmorAmount[GREENARMOR] = ClipHigh(pPlayer.ArmorAmount[GREENARMOR] + pArmorData.count[GREENARMOR], pArmorData.max[GREENARMOR]);
                    ret = true;
                }
                if (ret) {
                    sfxCreate3DSound(pSprite.getX(), pSprite.getY(), pSprite.getZ(), 779, pSprite.getSectnum());
                }
                return ret;

            case kItemBlueTeamBase:
            case kItemRedTeamBase:
                if (pGameInfo.nGameType != 3) {
                    return false;
                }

                XSPRITE pXBase = boardService.getXSprite(boardService.getSprite(nSprite).getExtra());
                if (pXBase != null) {
                    if (nItemType == kItemBlueTeamBase) {
                        if (pPlayer.teamID == 1) {
                            if ((pPlayer.hasFlag & 1) == 0 && pXBase.getState() != 0) {
                                pPlayer.hasFlag |= 1;
                                pPlayer.nBlueTeam = boardService.getSprite(nSprite).getXvel();
                                trTriggerSprite(pSprite.getXvel(), pXBase, 0);
                                sndStartSample(8007, 255, 2, false);
                                viewSetMessage(game.net.gProfile[pPlayer.nPlayer].name + " stole Blue Flag", -1, 10);
                            }
                        } else {
                            if ((pPlayer.hasFlag & 1) != 0 && pXBase.getState() == 0) {
                                pPlayer.nBlueTeam = -1;
                                pPlayer.hasFlag &= ~1;
                                trTriggerSprite(pSprite.getXvel(), pXBase, 1);
                                sndStartSample(8003, 255, 2, false);
                                viewSetMessage(game.net.gProfile[pPlayer.nPlayer].name + " returned Blue Flag", -1, 10);
                            }

                            if ((pPlayer.hasFlag & 2) != 0 && pXBase.getState() != 0) {
                                pPlayer.nRedTeam = -1;
                                pPlayer.hasFlag &= ~2;

                                nTeamCount[pPlayer.teamID] += 10;
                                nTeamClock[pPlayer.teamID] += 240;

                                checkFragLimit();
                                evSend(0, 0, kChannelFlag1Captured, kCommandOn);
                                sndStartSample(8001, 255, 2, false);
                                viewSetMessage(game.net.gProfile[pPlayer.nPlayer].name + " captured Red Flag!", -1, 7);
                            }
                        }
                    } else //kItemRedTeamBase
                    {
                        if (pPlayer.teamID == 0) {
                            if ((pPlayer.hasFlag & 2) == 0 && pXBase.getState() != 0) {
                                pPlayer.hasFlag |= 2;
                                pPlayer.nRedTeam = boardService.getSprite(nSprite).getXvel();
                                trTriggerSprite(pSprite.getXvel(), pXBase, 0);
                                sndStartSample(8006, 255, 2, false);
                                viewSetMessage(game.net.gProfile[pPlayer.nPlayer].name + " stole Red Flag", -1, 7);
                            }
                        } else {
                            if ((pPlayer.hasFlag & 2) != 0 && pXBase.getState() == 0) {
                                pPlayer.nRedTeam = -1;
                                pPlayer.hasFlag &= ~2;
                                trTriggerSprite(pSprite.getXvel(), pXBase, 1);
                                sndStartSample(8002, 255, 2, false);
                                viewSetMessage(game.net.gProfile[pPlayer.nPlayer].name + " returned Red Flag", -1, 7);
                            }

                            if ((pPlayer.hasFlag & 1) != 0 && pXBase.getState() != 0) {
                                pPlayer.nBlueTeam = -1;
                                pPlayer.hasFlag &= ~1;
                                nTeamCount[pPlayer.teamID] += 10;
                                nTeamClock[pPlayer.teamID] += 240;

                                checkFragLimit();
                                evSend(0, 0, kChannelFlag0Captured, kCommandOn);
                                sndStartSample(8000, 255, 2, false);
                                viewSetMessage(game.net.gProfile[pPlayer.nPlayer].name + " captured Blue Flag!", -1, 10);
                            }
                        }
                    }
                    return false;
                }
                return false;
            case kItemBlueFlag:
                if (pGameInfo.nGameType != 3) {
                    return false;
                }
                pPlayer.hasFlag |= 1;
                pPlayer.nBlueTeam = boardService.getSprite(nSprite).getOwner();
                checkEventList(pSprite.getXvel(), SS_SPRITE, 17);
                break;
            case kItemRedFlag:
                if (pGameInfo.nGameType != 3) {
                    return false;
                }
                pPlayer.hasFlag |= 2;
                pPlayer.nRedTeam = boardService.getSprite(nSprite).getOwner();
                checkEventList(pSprite.getXvel(), SS_SPRITE, 17);
                break;
            default:
                if (!powerupActivate(pPlayer, nPowerUp)) {
                    return false;
                }
                break;
        }

        if (nItemType != kItemGunsAkimbo) {
            sfxCreate3DSound(pSprite.getX(), pSprite.getY(), pSprite.getZ(), soundId, pSprite.getSectnum());
        }
        return true;
    }

    public static void DeliriumProcess() {
        int nDelirium = powerupCheck(gPlayer[gViewIndex], kItemShroomDelirium - kItemBase);

        if (nDelirium != 0) {
            timer += kFrameTicks;

            int maxTilt = kAngle30;
            int maxTurn = kAngle30;
            int maxPitch = 20;

            if (nDelirium < kWaneTime) {
                int scale = (nDelirium << 16) / kWaneTime;
                maxTilt = mulscale(maxTilt, scale, 16);
                maxTurn = mulscale(maxTurn, scale, 16);
                maxPitch = mulscale(maxPitch, scale, 16);
            }

            deliriumTilt = mulscale((Sin(timer * 2) / 2) + (Sin(timer * 3) / 2), maxTilt, 30);
            deliriumTurn = mulscale((Sin(timer * 3) / 2) + (Sin(timer * 4) / 2), maxTurn, 30);
            deliriumPitch = mulscale((Sin(timer * 4) / 2) + (Sin(timer * 5) / 2), maxPitch, 30);
        } else {
            deliriumTilt = 0;
            deliriumTurn = 0;
            deliriumPitch = 0;
        }
    }

    public static boolean shrinkPlayerSize(PLAYER pPlayer, int divider) {
        //System.err.println("SHRINK FOR "+pPlayer.pSprite.lotag);
        pPlayer.pXsprite.setData1((short) -divider);
        playerSetRace(pPlayer, kModeHumanShrink);
        return true;
    }

//	public static void deactivateSizeShrooms(PLAYER pPlayer) {
//		powerupDeactivate(pPlayer,kItemShroomGrow - kItemBase);
//		pPlayer.powerUpTimer[kItemShroomGrow - kItemBase] = 0;
//
//		powerupDeactivate(pPlayer,kItemShroomShrink - kItemBase);
//		pPlayer.powerUpTimer[kItemShroomShrink - kItemBase] = 0;
//	}

    public static boolean growPlayerSize(PLAYER pPlayer, int multiplier) {
        //System.err.println("GROW FOR "+pPlayer.pSprite.lotag);
        pPlayer.pXsprite.setData1((short) multiplier);
        playerSetRace(pPlayer, kModeHumanGrown);
        return true;
    }

    public static boolean resetPlayerSize(PLAYER pPlayer) {
        //System.err.println("NORMAL FOR "+pPlayer.pSprite.lotag);
        playerSetRace(pPlayer, kModeHuman);
        pPlayer.pXsprite.setData1(0);
        return true;
    }

    public static boolean powerupActivate(PLAYER pPlayer, int nPowerUp) {
        // skip the power-up if it is unique and already activated

        if (powerupCheck(pPlayer, nPowerUp) > 0 && gPowerUpInfo[nPowerUp].isUnique) {
            return false;
        }

        int nInventory = getInventoryNum(nPowerUp + kItemBase);
        if (nInventory >= 0) {
            pPlayer.Inventory[nInventory].activated = true;
        }

        if ((!IsOriginalDemo() && nInventory == -1) || pPlayer.powerUpTimer[nPowerUp] == 0) {
            pPlayer.powerUpTimer[nPowerUp] = gPowerUpInfo[nPowerUp].addPower;
        }

        switch (nPowerUp + kItemBase) {    // switch on of actual type

            //case kGDXItemMapLevel:
            //gFullMap = true;
            //break;
//			case kItemShroomShrink:
//				if (isGrown(pPlayer.pSprite)) deactivateSizeShrooms(pPlayer);
//				else shrinkPlayerSize(pPlayer,2);
//				break;
//			case kItemShroomGrow:
//				if (isShrinked(pPlayer.pSprite)) deactivateSizeShrooms(pPlayer);
//				else {
//					growPlayerSize(pPlayer,2);
//					if (powerupCheck(gPlayer[pPlayer.pSprite.lotag - kDudePlayer1],kItemLtdInvisibility - kItemBase) > 0) {
//						powerupDeactivate(pPlayer, kItemLtdInvisibility - kItemBase);
//						pPlayer.powerUpTimer[kItemLtdInvisibility - kItemBase] = 0;
//					}
//
//					if (ru.m210projects.Blood.AI.AIUNICULT.ceilIsTooLow(pPlayer.pSprite))
//						actDamageSprite(pPlayer.pSprite.xvel, pPlayer.pSprite,3,65535);
//				}
//				break;
            case kItemGunsAkimbo:
                if (IsOriginalDemo() || pPlayer.currentWeapon == 2 || pPlayer.currentWeapon == 3 || pPlayer.currentWeapon == 4 || pPlayer.currentWeapon == 5 || pPlayer.currentWeapon == 8) {
                    pPlayer.pInput.newWeapon = pPlayer.currentWeapon;
                    WeaponRaise(pPlayer);
                }
                break;
            case kItemDivingSuit:
                if (!pPlayer.godMode) {
                    pPlayer.damageShift[kDamageDrown]++;
                }
                if (pPlayer == gMe) {
                    SOUND.setReverb(true, 0.2f);
                }
                break;
            case kItemGasMask:
                if (!pPlayer.godMode) {
                    pPlayer.damageShift[kDamageDrown]++;
                }
                break;
            case kItemReflectiveShots:
                if (pPlayer == gMe) {
                    SOUND.setReverb(true, 0.4f);
                }
                break;
            case kItemAsbestosArmor:
                if (!pPlayer.godMode) {
                    pPlayer.damageShift[kDamageBurn]++;
                }
                break;
            case kItemInvulnerability:
                if (!pPlayer.godMode) {
                    for (int i = 0; i < kDamageMax; i++) {
                        pPlayer.damageShift[i]++;
                    }
                }
                break;
            case kItemFeatherFall:
            case kItemJumpBoots:
                if (!pPlayer.godMode) {
                    pPlayer.damageShift[kDamageFall]++;
                }
                break;
            case kItemJetpack:
                pPlayer.moveState = kMoveFly;
                pPlayer.pSprite.setHitag(pPlayer.pSprite.getHitag() & ~kAttrGravity);
                break;
            default:
                break;
        }

        sfxStart3DSound(pPlayer.pSprite, 776, -1, 0);

        return true;
    }

    public static void powerupDeactivate(PLAYER pPlayer, int nPowerUp) {
        int nInventory = getInventoryNum(nPowerUp + kItemBase);

        if (nInventory >= 0) {
            pPlayer.Inventory[nInventory].activated = false;
        }

        switch (nPowerUp + kItemBase)    // switch off of actual type
        {
//			case kItemShroomShrink:
//				resetPlayerSize(pPlayer);
//				if (ru.m210projects.Blood.AI.AIUNICULT.ceilIsTooLow(pPlayer.pSprite))
//					actDamageSprite(pPlayer.pSprite.xvel, pPlayer.pSprite,3,65535);
//				break;
//			case kItemShroomGrow:
//				resetPlayerSize(pPlayer);
//				break;
            case kItemGunsAkimbo:
                if (IsOriginalDemo() || pPlayer.currentWeapon == 2 || pPlayer.currentWeapon == 3 || pPlayer.currentWeapon == 4 || pPlayer.currentWeapon == 5 || pPlayer.currentWeapon == 8) {
                    pPlayer.pInput.newWeapon = pPlayer.currentWeapon;
                    WeaponRaise(pPlayer);
                }
                break;
            case kItemDivingSuit:
                if (!pPlayer.godMode) {
                    pPlayer.damageShift[kDamageDrown]--;
                }
                if (pPlayer == gMe) {
                    SOUND.setReverb(false, 0);
                }
                break;
            case kItemGasMask:
                if (!pPlayer.godMode) {
                    pPlayer.damageShift[kDamageDrown]--;
                }
                break;
            case kItemReflectiveShots:
                if (pPlayer == gMe) {
                    SOUND.setReverb(false, 0);
                }
                break;
            case kItemAsbestosArmor:
                if (!pPlayer.godMode) {
                    pPlayer.damageShift[kDamageBurn]--;
                }
                break;
            case kItemInvulnerability:
                if (!pPlayer.godMode) {
                    for (int i = 0; i < kDamageMax; i++) {
                        pPlayer.damageShift[i]--;
                    }
                }
                break;
            case kItemFeatherFall:
            case kItemJumpBoots:
                if (!pPlayer.godMode) {
                    pPlayer.damageShift[kDamageFall]--;
                }
                break;
            case kItemJetpack:
                pPlayer.moveState = kMoveWalk;
                pPlayer.pSprite.setHitag(pPlayer.pSprite.getHitag() | kAttrGravity);
                pPlayer.pSprite.setVelocityZ(58254);
            default:
                break;
        }
    }

    public static void powerupProcess(PLAYER pPlayer) {
        pPlayer.showInventory = ClipLow(pPlayer.showInventory - kFrameTicks, 0);
        for (int i = 48; i >= 0; i--) {
            int nInventory = getInventoryNum(i + kItemBase);
            if (nInventory < 0) {
                if (pPlayer.powerUpTimer[i] > 0) {
                    pPlayer.powerUpTimer[i] = ClipLow(pPlayer.powerUpTimer[i] - kFrameTicks, 0);
                    if (pPlayer.powerUpTimer[i] == 0) {
                        powerupDeactivate(pPlayer, i);
                    }
                }
            } else if (pPlayer.Inventory[nInventory].activated) {
                pPlayer.powerUpTimer[i] = ClipLow(pPlayer.powerUpTimer[i] - kFrameTicks, 0);
                if (i == 47) {
                    continue; //Jetpack
                }

                if (pPlayer.powerUpTimer[i] > 0) {
                    pPlayer.Inventory[nInventory].amount = 100 * pPlayer.powerUpTimer[i] / gPowerUpInfo[i].addPower;
                } else {
                    powerupDeactivate(pPlayer, i);
                    if (nInventory == pPlayer.choosedInven) {
                        pPlayer.choosedInven = 0;
                    }
                }
            }
        }

        //System.err.println("GROW TIME: "+pPlayer.powerUpTimer[kItemShroomGrow - kItemBase]);
        //System.err.println("SHRINK TIME: "+pPlayer.powerUpTimer[kItemShroomShrink - kItemBase]);
        DeliriumProcess();
    }


//	public static boolean isGrown(SPRITE pSprite) {
//		return (powerupCheck(gPlayer[pSprite.lotag - kDudePlayer1],kItemShroomGrow - kItemBase) > 0);
//	}
//
//	public static boolean isShrinked(SPRITE pSprite) {
//		return (powerupCheck(gPlayer[pSprite.lotag - kDudePlayer1],kItemShroomShrink - kItemBase) > 0);
//	}

    public static void powerupClear(PLAYER pPlayer) {
        for (int i = 0; i < kMaxPowerUps; i++) {
            pPlayer.powerUpTimer[i] = 0;
        }
    }

    public static boolean PickupInventryItem(PLAYER pPlayer, int nInventory) {
        if (nInventory >= kInventoryMax) {
            System.err.println("Unhandled pack item " + nInventory);
            System.exit(1);
        }

        if (pPlayer.Inventory[nInventory].amount >= 100) {
            return false;
        }

        pPlayer.Inventory[nInventory].amount = 100;

        int type = -1;
        switch (nInventory) {
            case kInventoryDivingSuit:
                type = kItemDivingSuit;
                break;
            case kInventoryCrystalBall:
                type = kItemCrystalBall;
                break;
            case kInventoryBeastVision:
                type = kItemBeastVision;
                break;
            case kInventoryJumpBoots:
                type = kItemJumpBoots;
                break;
            case kInventoryDoctorBag:
                break;
            case kInventoryJetpack:
                type = kItemJetpack;
                break;
            default:
                System.err.println("Unhandled pack item " + nInventory);
                System.exit(1);
                break;
        }
        type -= kItemBase;
        if (type == 17) {
            powerupActivate(pPlayer, type);
        }
        if (type >= 0) {
            pPlayer.powerUpTimer[type] = gPowerUpInfo[type].addPower;
        }
        if (pPlayer.choosedInven == -1) {
            pPlayer.choosedInven = nInventory;
        }
        if (pPlayer.Inventory[pPlayer.choosedInven].amount == 0) {
            pPlayer.choosedInven = nInventory;
        }
        return true;
    }

    public static int powerupCheck(PLAYER pPlayer, int nPowerUp) {
        if (pPlayer == null) {
            throw new AssertException("pPlayer != null");
        }
        if (!(nPowerUp >= 0 && nPowerUp < kMaxPowerUps)) {
            throw new AssertException("nPowerUp >= 0 && nPowerUp < kMaxPowerUps");
        }

        int nInventory = getInventoryNum(nPowerUp + kItemBase);
        if (nInventory < 0 || inventoryCheck(pPlayer, nInventory)) {
            return pPlayer.powerUpTimer[nPowerUp];
        } else {
            return 0;
        }
    }

    public static void processInventory(PLAYER pPlayer, int nInventory) {
        int nPowerUp = -1;
        boolean activated = false;
        if (pPlayer.Inventory[nInventory].amount > 0) {
            switch (nInventory) {
                case kInventoryDoctorBag:
                    XSPRITE pXDude = pPlayer.pXsprite;
                    int health = pXDude.getHealth() >> 4;
                    if (health < 100) {
                        int healValue;
                        if ((100 - health) >= pPlayer.Inventory[kInventoryDoctorBag].amount) {
                            healValue = pPlayer.Inventory[kInventoryDoctorBag].amount;
                            actHealDude(pXDude, healValue, 100);
                        } else {
                            healValue = 100 - health;
                            actHealDude(pXDude, 100 - health, 100);
                        }
                        pPlayer.Inventory[kInventoryDoctorBag].amount -= healValue;
                    }
                    break;
                case kInventoryDivingSuit:
                    nPowerUp = kItemDivingSuit;
                    activated = true;
                    break;
                case kInventoryCrystalBall:
                    nPowerUp = kItemCrystalBall;
                    activated = true;
                    break;
                case kInventoryBeastVision:
                    nPowerUp = kItemBeastVision;
                    activated = true;
                    break;
                case kInventoryJumpBoots:
                    nPowerUp = kItemJumpBoots;
                    activated = true;
                    break;
                case kInventoryJetpack:
                    nPowerUp = kItemJetpack;
                    activated = true;
                    break;
                default:
                    System.err.println("Unhandled pack item " + nInventory);
                    System.exit(1);
                    break;
            }
        }

        nPowerUp -= kItemBase;
        pPlayer.showInventory = 0;
        if (activated) {
            if (pPlayer.Inventory[nInventory].activated) {
                powerupDeactivate(pPlayer, nPowerUp);
            } else {
                powerupActivate(pPlayer, nPowerUp);
            }
        }
    }

    public static int getInventoryNum(int item) {
        switch (item) {
            case kItemDivingSuit:
                return kInventoryDivingSuit;
            case kItemCrystalBall:
                return kInventoryCrystalBall;
            case kItemBeastVision:
                return kInventoryBeastVision;
            case kItemJumpBoots:
                return kInventoryJumpBoots;
            case kItemJetpack:
                return kInventoryJetpack;
        }
        return -1;
    }

    public static boolean inventoryCheck(PLAYER pPlayer, int nInventory) {
        return pPlayer.Inventory[nInventory].activated;
    }

    public static int getInventoryAmount(PLAYER pPlayer, int nInventory) {
        return pPlayer.Inventory[nInventory].amount;
    }

    public static void InventoryRight(PLAYER pPlayer) {
        if (pPlayer.showInventory > 0) {
            if (gInventorySwap) {
                int nextInv = pPlayer.choosedInven + 1;
                for (int i = 0; i < kInventoryMax; i++, nextInv++) {
                    if (nextInv == kInventoryMax) {
                        nextInv = 0;
                    }
                    if (pPlayer.Inventory[nextInv].amount != 0) {
                        pPlayer.choosedInven = nextInv;
                        break;
                    }
                }
            } else {
                for (int i = ClipHigh(pPlayer.choosedInven + 1, kInventoryMax); i < kInventoryMax; i++) {
                    if (pPlayer.Inventory[i].amount != 0) {
                        pPlayer.choosedInven = i;
                        break;
                    }
                }
            }
        }
        pPlayer.showInventory = 600;
    }

    public static void InventoryLeft(PLAYER pPlayer) {
        if (pPlayer.showInventory > 0) {
            if (gInventorySwap) {
                int prevInv = pPlayer.choosedInven - 1;
                for (int i = kInventoryMax; i >= 0; i--, prevInv--) {
                    if (prevInv <= -1) {
                        prevInv = kInventoryMax - 1;
                    }
                    if (pPlayer.Inventory[prevInv].amount != 0) {
                        pPlayer.choosedInven = prevInv;
                        break;
                    }
                }
            } else {
                for (int i = ClipLow(pPlayer.choosedInven - 1, 0); i >= 0; --i) {
                    if (pPlayer.Inventory[i].amount != 0) {
                        pPlayer.choosedInven = i;
                        break;
                    }
                }
            }
        }
        pPlayer.showInventory = 600;
    }

    public static boolean PickupAmmo(PLAYER pPlayer, int nSprite, int nAmmoItemType) {
        AMMOITEMDATA pAmmoData = gAmmoItemData[nAmmoItemType - kAmmoItemBase];
        BloodSprite pSprite = boardService.getSprite(nSprite);
        int nAmmoType = pAmmoData.ammoType;

        if (nAmmoType == 255) {
            return true;
        } else if (pPlayer.ammoCount[nAmmoType] >= gAmmoInfo[nAmmoType].max && !gInfiniteAmmo) {
            return false;
        }

        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null || pXSprite.getData1() <= 0) {
            pPlayer.ammoCount[nAmmoType] = ClipHigh(pPlayer.ammoCount[nAmmoType] + pAmmoData.count, gAmmoInfo[nAmmoType].max);
        } else {
            pPlayer.ammoCount[nAmmoType] = ClipHigh(pPlayer.ammoCount[nAmmoType] + pXSprite.getData1(), gAmmoInfo[nAmmoType].max);
        }

        // set the hasWeapon flags for weapons which are ammo
        if (pAmmoData.weaponType != kWeaponNone) {
            pPlayer.hasWeapon[pAmmoData.weaponType] = true;
        }

        sfxStart3DSound(pPlayer.pSprite, 782, -1, 0);
        return true;
    }

    public static boolean PickupWeapon(PLAYER pPlayer, int nSprite, int nWeaponItem) {
        WEAPONITEMDATA pWeaponData = gWeaponItemData[nWeaponItem - kWeaponItemBase];
        int nWeapon = pWeaponData.weaponType;
        int nAmmo = pWeaponData.ammoType;
        BloodSprite pSprite = boardService.getSprite(nSprite);
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        // add weapon to player inventory
        if (pPlayer.hasWeapon[nWeapon] && pGameInfo.nWeaponSettings != 2 && pGameInfo.nWeaponSettings != 3) {
            if (actGetRespawnTime(boardService.getSprite(nSprite)) != 0) {
                if (nAmmo == kAmmoNone || (!gInfiniteAmmo && pPlayer.ammoCount[nAmmo] >= gAmmoInfo[nAmmo].max)) {
                    return false;
                } else if (pXSprite == null || pXSprite.getData1() <= 0) {
                    pPlayer.ammoCount[nAmmo] = ClipHigh(pPlayer.ammoCount[nAmmo] + pWeaponData.count, gAmmoInfo[nAmmo].max);
                } else {
                    pPlayer.ammoCount[nAmmo] = ClipHigh(pPlayer.ammoCount[nAmmo] + pXSprite.getData1(), gAmmoInfo[nAmmo].max);
                }

                sfxStart3DSound(pPlayer.pSprite, 777, -1, 0);
                return true;
            }
            return false;
        } else //pickup life leech
            if (boardService.getSprite(nSprite).getLotag() != kWeaponLifeLeech || pGameInfo.nGameType <= kNetModeCoop || !PickupLeech(pPlayer, null)) {
                pPlayer.hasWeapon[nWeapon] = true;

                if (nAmmo != kAmmoNone) {
                    // add preloaded ammo
                    if (pXSprite == null || pXSprite.getData1() <= 0) {
                        pPlayer.ammoCount[nAmmo] = ClipHigh(pPlayer.ammoCount[nAmmo] + pWeaponData.count, gAmmoInfo[nAmmo].max);
                    } else {
                        pPlayer.ammoCount[nAmmo] = ClipHigh(pPlayer.ammoCount[nAmmo] + pXSprite.getData1(), gAmmoInfo[nAmmo].max);
                    }
                }

                int weap = WeaponUpgrade(pPlayer, nWeapon);
                if (weap != pPlayer.currentWeapon) {
                    pPlayer.weaponState = 0;
                    pPlayer.updateWeapon = weap;
                }
                sfxStart3DSound(pPlayer.pSprite, 777, -1, 0); //kSfxWeaponUp = 777

                return true;
            }

        return false;
    }

    public static void HandEffectProcess(PLAYER gPlayer) {
        if (gPlayer.pSprite.getStatnum() == kStatDude && gPlayer.handDamage) {
            if (mulscale(4, vRandom(), 15) != 0) {
                gPlayer.handCount = ClipHigh(gPlayer.handCount + kFrameTicks, 64);
                gPlayer.blindEffect = ClipHigh(gPlayer.blindEffect + 2 * kFrameTicks, 128);
            }
        }
//		original method
//		if(gPlayer.pSprite.statnum == kStatDude && gPlayer.handDamage) {
//			gPlayer.handCount = ClipHigh(gPlayer.handCount + pGameInfo.nDifficulty + 2, 64);
//			if(35 - 5 * pGameInfo.nDifficulty < gPlayer.handCount)
//				gPlayer.blindEffect = ClipHigh(gPlayer.blindEffect + 4 * pGameInfo.nDifficulty, 128);
//		}
    }

    public static void playerMove(PLAYER pPlayer) {
        final int nSprite = pPlayer.nSprite;
        final BloodSprite pSprite = pPlayer.pSprite;
        XSPRITE pXSprite = pPlayer.pXsprite;

        POSTURE cp = gPosture[pPlayer.nLifeMode][pPlayer.moveState];

        powerupProcess(pPlayer);
        HandEffectProcess(pPlayer);

        short nSector;

        GetSpriteExtents(pSprite);

        int clipDist = pSprite.getClipdist() << 2;
        int floorDist = (extents_zBot - pSprite.getZ()) / 4;
        int ceilDist = (pSprite.getZ() - extents_zTop) / 4;

        if (!gNoClip) {
            nSector = pSprite.getSectnum();

            int push = engine.pushmove(pSprite.getX(), pSprite.getY(), pSprite.getZ(), nSector, clipDist, ceilDist, floorDist, CLIPMASK0);

            pSprite.setX(pushmove_x);
            pSprite.setY(pushmove_y);
            pSprite.setZ(pushmove_z);
            nSector = pushmove_sectnum;

            if (push == -1) {
                actDamageSprite(nSprite, pSprite, kDamageFall, 8000);
            }

            if (nSector != pSprite.getSectnum()) {
                if (nSector == -1) {
                    nSector = pSprite.getSectnum();
                    actDamageSprite(pSprite.getXvel(), pSprite, kDamageFall, 8000);
                }
                if (!boardService.isValidSector(nSector)) {
                    throw new AssertException("boardService.isValidSector(nSector)");
                }
                engine.changespritesect((short) nSprite, nSector);
            }
        }

        ProcessInput(pPlayer);

        int moveDist = EngineUtils.qdist(pSprite.getVelocityX(), pSprite.getVelocityY()) >> 16;
        pPlayer.viewOffdZ += mulscale(28672, pSprite.getVelocityZ() - pPlayer.viewOffdZ, 16);
        int dZv = pSprite.getZ() - cp.viewSpeed - pPlayer.viewOffZ;
        if (dZv > 0) {
            pPlayer.viewOffdZ += mulscale(40960, (long) dZv << 8, 16);
        } else {
            pPlayer.viewOffdZ += mulscale(6144, (long) dZv << 8, 16);
        }
        pPlayer.viewOffZ += pPlayer.viewOffdZ >> 8;

        pPlayer.weapOffdZ += mulscale(20480, pSprite.getVelocityZ() - pPlayer.weapOffdZ, 16);
        int dZw = pSprite.getZ() - cp.weapSpeed - pPlayer.weaponAboveZ;
        if (dZw > 0) {
            pPlayer.weapOffdZ += mulscale(32768, (long) dZw << 8, 16);
        } else {
            pPlayer.weapOffdZ += mulscale(3072, (long) dZw << 8, 16);
        }
        pPlayer.weaponAboveZ += pPlayer.weapOffdZ >> 8;

        pPlayer.bobAmp = ClipLow(pPlayer.bobAmp - kFrameTicks, 0);

        if (pPlayer.moveState == kMoveSwim) {
            pPlayer.bobPhase = (pPlayer.bobPhase + kFrameTicks * kAngle360 / kTimerRate / 4) & kAngleMask;
            pPlayer.swayPhase = (pPlayer.swayPhase + kFrameTicks * kAngle360 / kTimerRate / 4) & kAngleMask;

            pPlayer.bobHeight = mulscale(cp.bobV * 10L, Sin(pPlayer.bobPhase * 2), 30);
            pPlayer.bobWidth = mulscale((long) cp.bobH * pPlayer.bobAmp, Sin(pPlayer.bobPhase - kAngle45), 30);
            pPlayer.swayHeight = mulscale((long) cp.swayV * pPlayer.bobAmp, Sin(pPlayer.swayPhase * 2), 30);
            pPlayer.swayWidth = mulscale((long) cp.swayH * pPlayer.bobAmp, Sin(pPlayer.swayPhase - kAngle60), 30);
        } else {
            if (pXSprite.getHeight() < 256) {

                boolean Run = false; //(!adapter.cfg.gAutoRun && pPlayer.Run) || (!pPlayer.Run && adapter.cfg.gAutoRun);

                pPlayer.bobPhase = (pPlayer.bobPhase + kFrameTicks * cp.pace[Run ? 1 : 0]) & kAngleMask;
                pPlayer.swayPhase = (pPlayer.swayPhase + kFrameTicks * cp.pace[Run ? 1 : 0] / 2) & kAngleMask;

                if (Run) {
                    if (pPlayer.bobAmp < 60) {
                        pPlayer.bobAmp = ClipHigh(pPlayer.bobAmp + moveDist, 60);
                    }
                } else {
                    if (pPlayer.bobAmp < 30) {
                        pPlayer.bobAmp = ClipHigh(pPlayer.bobAmp + moveDist, 30);
                    }
                }

                pPlayer.bobHeight = mulscale((long) cp.bobV * pPlayer.bobAmp, Sin(pPlayer.bobPhase * 2), 30);
                pPlayer.bobWidth = mulscale((long) cp.bobH * pPlayer.bobAmp, Sin(pPlayer.bobPhase - kAngle45), 30);
                pPlayer.swayHeight = mulscale((long) cp.swayV * pPlayer.bobAmp, Sin(pPlayer.swayPhase * 2), 30);
                pPlayer.swayWidth = mulscale((long) cp.swayH * pPlayer.bobAmp, Sin(pPlayer.swayPhase - kAngle60), 30);
            }
        }

        pPlayer.explosion = 0;
        pPlayer.quakeTime = ClipLow(pPlayer.quakeTime - kFrameTicks, 0);
        pPlayer.tilt = ClipLow(pPlayer.tilt - kFrameTicks, 0);
        pPlayer.visibility = ClipLow(pPlayer.visibility - kFrameTicks, 0);

        pPlayer.hitEffect = ClipLow(pPlayer.hitEffect - kFrameTicks, 0);
        pPlayer.blindEffect = ClipLow(pPlayer.blindEffect - kFrameTicks, 0);
        pPlayer.pickupEffect = ClipLow(pPlayer.pickupEffect - kFrameTicks, 0);

        if (pXSprite.getHealth() == 0) {
            if (pPlayer == gMe) {
                gMe.handDamage = false;
            }
            return;
        }

        pPlayer.Underwater = false;
        if (pPlayer.moveState == kMoveSwim) {
            pPlayer.Underwater = true;

            if (gLowerLink[pSprite.getSectnum()] > 0) {
                int type = boardService.getSprite(gLowerLink[pSprite.getSectnum()]).getLotag();
                if (type == 14 || type == 10) {
                    if (engine.getceilzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY()) > pPlayer.viewOffZ) {
                        pPlayer.Underwater = false;
                    }
                }
            }
        }

        if (!pPlayer.Underwater) {
            pPlayer.airTime = 1200;
            pPlayer.drownEffect = 0;
            if (inventoryCheck(pPlayer, kInventoryDivingSuit)) {
                processInventory(pPlayer, kInventoryDivingSuit);
            }
        }

        int dudeIndex = kDudePlayer1 - kDudeBase;
        int nXSprite = pSprite.getExtra(); // XXX

        if (nXSprite == -1) {
            return; //Player disconnected?
        }

        switch (pPlayer.moveState) {
            case kMoveWalk:
                if (moveDist == 0) {
                    seqSpawn(dudeInfo[dudeIndex].seqStartID + getPlayerSeq(kPlayerIdle), SS_SPRITE, nXSprite, null);
                } else {
                    seqSpawn(dudeInfo[dudeIndex].seqStartID + getPlayerSeq(kPlayerWalk), SS_SPRITE, nXSprite, null);    // hack to make player walk
                }
                break;
            case kMoveSwim:
                seqSpawn(dudeInfo[dudeIndex].seqStartID + getPlayerSeq(kPlayerSwim), SS_SPRITE, nXSprite, null);
                break;
            case kMoveCrouch:
            case kMoveFly:
                seqSpawn(dudeInfo[dudeIndex].seqStartID + getPlayerSeq(kPlayerCrouch), SS_SPRITE, nXSprite, null);
                break;
        }
    }

    public static Sprite playerFireThing(PLAYER pPlayer, int xoffset, int relSlope, int thingType, int velocity) {
        if (!(thingType >= kThingBase && thingType < kThingMax)) {
            throw new AssertException("thingType >= kThingBase && thingType < kThingMax");
        }

        int z = pPlayer.weaponAboveZ - pPlayer.pSprite.getZ();
        if (newHoriz && !IsOriginalDemo()) {
            z += 10 * pPlayer.horiz;
        }
        return actFireThing(pPlayer.nSprite, xoffset, z, (int) pPlayer.horizOff + relSlope, thingType, velocity);
    }

    public static Sprite playerFireMissile(PLAYER pPlayer, int xoffset, int dx, int dy, int dz, int missileType) {
        int z = pPlayer.weaponAboveZ - pPlayer.pSprite.getZ();
        if (newHoriz && !IsOriginalDemo()) {
            z -= (int) (20 * pPlayer.horiz); //z += 10 * pPlayer.horiz;
        }
        return actFireMissile(pPlayer.pSprite, xoffset, z, dx, dy, dz, missileType);
    }

    public static int playerCalcDamage(PLAYER pPlayer, int nDamageType, int nDamage) {
        int nArmorType = gDamageInfo[nDamageType].nArmorType;
        if (nArmorType >= 0) {
            if (pPlayer.ArmorAmount[nArmorType] != 0) {
                int tmp = nDamage / 4;
                int damage = tmp + muldiv(pPlayer.ArmorAmount[nArmorType], (7L * nDamage / 8) - tmp, 3200);

                nDamage -= damage;
                pPlayer.ArmorAmount[nArmorType] = ClipLow(pPlayer.ArmorAmount[nArmorType] - damage, 0);
            }
        }
        return nDamage;
    }

    public static void playerDamageSprite(PLAYER pPlayer, int nSource, int nDamageType, int nDamage) {
        if (!boardService.isValidSprite(nSource)) {
            throw new AssertException("isValidSprite(nSource)");
        }
        if (pPlayer == null) {
            throw new AssertException("pPlayer != NULL");
        }
        if (!(nDamageType >= 0 && nDamageType < kDamageMax)) {
            throw new AssertException("nDamageType >= 0 && nDamageType < kDamageMax");
        }

        if (pPlayer.damageShift[nDamageType] == 0) {
            int nDeathSeqID = -1;

            nDamage = playerCalcDamage(pPlayer, nDamageType, nDamage);
            pPlayer.hitEffect = ClipHigh(pPlayer.hitEffect + (nDamage >> 3), 600);
            final BloodSprite pSprite = pPlayer.pSprite;
            XSPRITE pXSprite = pPlayer.pXsprite;

            if (pSprite == null || !IsDudeSprite(pSprite)) {
                return;
            }

            DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];

            CALLPROC nCallback = null;
            if ((pXSprite.getHealth()) == 0) {
                if (checkPlayerSeq(pPlayer, getPlayerSeq(kPlayerFatality)))  //Fatality
                {
                    if (pGameInfo.nGameType == kNetModeCoop && pGameInfo.nReviveMode) {
                        return;
                    }

                    switch (nDamageType) {
                        case kDamageExplode:
                            actGenerateGibs(pSprite, 7, null, null);
                            actGenerateGibs(pSprite, 15, null, null);
                            pSprite.setCstat(pSprite.getCstat() | 0x8000);
                            nDeathSeqID = getPlayerSeq(kPlayerFDying);
                            break;
                        case kDamageSpirit:
                            nDeathSeqID = getPlayerSeq(kPlayerFSpirit);
                            sfxStart3DSound(pSprite, 716, 0, 0);
                            break;
                        default:
                            GetSpriteExtents(pSprite);
                            startPos.set(pSprite.getX(), pSprite.getY(), extents_zTop);
                            startVel.set(pSprite.getVelocityX() >> 1, pSprite.getVelocityY() >> 1, -838860);
                            actGenerateGibs(pSprite, 27, startPos, startVel);
                            actGenerateGibs(pSprite, 7, null, null);
                            actSpawnBlood(pSprite);
                            actSpawnBlood(pSprite);
                            nDeathSeqID = getPlayerSeq(kPlayerFDying);
                            break;
                    }
                }
            } else {
                int dHealth = pXSprite.getHealth() - nDamage;
                pXSprite.setHealth(ClipLow(dHealth, 0));
                if (pXSprite.getHealth() != 0 && pXSprite.getHealth() < 16) {
                    pXSprite.setHealth(0);
                    dHealth = -25;
                    nDamageType = kDamageBullet;
                }

                if (pXSprite.getHealth() == 0) {
                    if (pGameInfo.nGameType == kNetModeTeams) {
                        if (pPlayer.hasFlag != 0) {
                            if ((pPlayer.hasFlag & 1) != 0) {
                                pPlayer.hasFlag &= ~1;
                                Sprite nFlag = DropPickupObject(pPlayer.pSprite, kItemBlueFlag);
                                if (nFlag != null) {
                                    nFlag.setOwner(pPlayer.nBlueTeam);
                                }
                                sndStartSample(8005, 255, 2, false);
                                viewSetMessage(game.net.gProfile[pPlayer.nPlayer].name + " dropped Blue Flag", -1, 10);
                            }

                            if ((pPlayer.hasFlag & 2) != 0) {
                                pPlayer.hasFlag &= ~2;
                                Sprite nFlag = DropPickupObject(pPlayer.pSprite, kItemRedFlag);
                                if (nFlag != null) {
                                    nFlag.setOwner(pPlayer.nRedTeam);
                                }
                                sndStartSample(8004, 255, 2, false);
                                viewSetMessage(game.net.gProfile[pPlayer.nPlayer].name + " dropped Red Flag", -1, 7);
                            }
                        }
                    }

                    sfxKill3DSound(pPlayer.pSprite, -1, 441);
                    pPlayer.deathTime = 0;
                    if (pGameInfo.nGameType == kNetModeOff || ((pPlayer.currentWeapon != kWeaponTNT && pPlayer.currentWeapon != kWeaponSprayCan) || pPlayer.weaponTimer == 0)) {
                        pPlayer.fLoopQAV = false;
                        pPlayer.currentWeapon = 0;
                        pPlayer.LastWeapon = 0;
                    }

                    pPlayer.CrouchMode = 0;
                    pPlayer.fraggerID = nSource;
                    pPlayer.voodooCount = 0;
                    if (nDamageType == kDamageExplode && nDamage < 144) {
                        nDamageType = kDamageFall;
                    }

                    if (pGameInfo.nGameType == kNetModeCoop && pGameInfo.nReviveMode) //revive code
                    {
                        sfxStart3DSound(pSprite, gDamageInfo[nDamageType].nSoundID[3], 0, 2);
                        nDeathSeqID = getPlayerSeq(kPlayerFatality);
                        pXSprite.setTarget(nSource);
                        //evPostCallback(pSprite.xvel, 3, 15, 13);
                    } else {
                        switch (nDamageType) {
                            case kDamageBurn:
                                sfxStart3DSound(pSprite, 718, 0, 0);
                                nDeathSeqID = getPlayerSeq(kPlayerBurn);
                                break;
                            case kDamageExplode:
                                sfxStart3DSound(pSprite, 717, 0, 0);
                                actGenerateGibs(pSprite, 7, null, null);
                                actGenerateGibs(pSprite, 15, null, null);
                                nDeathSeqID = getPlayerSeq(kPlayerExplode);
                                pSprite.setCstat(pSprite.getCstat() | 0x8000);
                                break;
                            case kDamageDrown:
                                nDeathSeqID = getPlayerSeq(kPlayerDying);
                                break;
                            default:
                                if (dHealth < -20 && pGameInfo.nGameType >= kNetModeBloodBath && Chance(0x2000)) {
                                    nCallback = callbacks[FatalityDead];
                                    sfxStart3DSound(pSprite, gDamageInfo[nDamageType].nSoundID[3], 0, 2);
                                    nDeathSeqID = getPlayerSeq(kPlayerFatality);
                                    powerupActivate(pPlayer, kItemShroomDelirium - kItemBase);
                                    pXSprite.setTarget(nSource);
                                    evPostCallback(pSprite.getXvel(), 3, 15, 13);
                                } else {
                                    sfxStart3DSound(pSprite, 716, 0, 0);
                                    nDeathSeqID = getPlayerSeq(kPlayerDying);
                                }
                                break;
                        }
                    }
                } else {
                    DAMAGEINFO pDamageInfo = gDamageInfo[nDamageType];
                    int nSoundID;
                    if (nDamage < 160) {
                        nSoundID = pDamageInfo.nSoundID[Random(3)];
                    } else {
                        nSoundID = pDamageInfo.nSoundID[0];
                    }
                    if (nDamageType == kDamageDrown && (gPlayer[gViewIndex].pXsprite.getPalette() == 1) && !pPlayer.handDamage) {
                        nSoundID = 714;
                    }

                    sfxStart3DSound(pSprite, nSoundID, 0, 6);
                    return;
                }
            }

            if (nDeathSeqID >= 0) {
                if (nDeathSeqID != getPlayerSeq(kPlayerFatality) || (pGameInfo.nReviveMode && pGameInfo.nGameType == kNetModeCoop)) {
                    int nXSector = boardService.getSector(pSprite.getSectnum()).getExtra();
                    powerupClear(pPlayer);
                    if (nXSector > 0 && xsector[nXSector].Exit) {
                        trTriggerSector(pSprite.getSectnum(), xsector[nXSector], kCommandSectorExit);
                    }
                    pSprite.setHitag(pSprite.getHitag() | 7);
                    for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                        if (pSprite.getXvel() == gPlayer[i].fraggerID && gPlayer[i].deathTime > 0) {
                            gPlayer[i].fraggerID = -1;
                        }
                    }
                    playerFrag(pPlayer, nSource);
                    trTriggerSprite(pSprite.getXvel(), pXSprite, kCommandOff);
                }

                if (!game.getCache().getEntry(pDudeInfo.seqStartID + nDeathSeqID, seq).exists()) {
                    throw new AssertException("gSysRes.Lookup(pDudeInfo.seqStartID + nDeathSeqID, \"SEQ\") != null");
                }
                seqSpawn(pDudeInfo.seqStartID + nDeathSeqID, SS_SPRITE, pSprite.getExtra(), nCallback);
            }
        }
    }

    public static void playerFrag(PLAYER pKiller, PLAYER pVictim) {
        if (pKiller == null) {
            throw new AssertException("pKiller != NULL");
        }

        if (pVictim == null) {
            throw new AssertException("pVictim != NULL");
        }

        // get player indices for killer and victim
        int nKiller = pKiller.pSprite.getLotag() - kDudePlayer1;
        if (nKiller < 0 || nKiller >= kMaxPlayers) {
            throw new AssertException("nKiller >= 0 && nKiller < kMaxPlayers");
        }
        int nVictim = pVictim.pSprite.getLotag() - kDudePlayer1;
        if (nVictim < 0 || nVictim >= kMaxPlayers) {
            throw new AssertException("nVictim >= 0 && nVictim < kMaxPlayers");
        }

        if (myconnectindex == connecthead) {
//			sub_7AC28(); XXX
        }

        if (nKiller == nVictim) {
            pVictim.fraggerID = -1;    // can't target yourself
            pVictim.fragCount--;
            pVictim.fragInfo[nKiller]--;    // frags against self is negative

            if (pGameInfo.nGameType == 3) {
                nTeamCount[pVictim.teamID]--;
            }
            int n = Random(deathAphorisms1.length);
            if (pVictim.handCount <= 0) {
                String message;
                if (pVictim != gMe && pGameInfo.nGameType > kNetModeOff && deathAphorisms1[n].nSound >= 0) {
                    if (pGameInfo.nGameType != kNetModeCoop) {
                        sndStartSample(deathAphorisms1[n].nSound, 255, 2, false);
                    }
                    message = game.net.gProfile[nVictim].name + deathAphorisms1[n].text;
                } else {
                    message = "You killed yourself!";
                }

                viewSetMessage(message, -1, 7);
            }
        } else {
            pKiller.fragCount++;
            pKiller.fragInfo[nVictim]++;    // frags against others are positive

            if (pGameInfo.nGameType == kNetModeTeams) {
                if (pKiller.teamID == pVictim.teamID) {
                    nTeamCount[pKiller.teamID]--;
                } else {
                    nTeamCount[pKiller.teamID]++;
                    nTeamClock[pKiller.teamID] += 120;
                }
            }

            if (pGameInfo.nGameType > kNetModeOff) {
                int n = Random(deathAphorisms2.length);
                if (pGameInfo.nGameType > kNetModeOff && pKiller == gMe && deathAphorisms2[n].nSound >= 0) {
                    if (pGameInfo.nGameType != kNetModeCoop) {
                        sndStartSample(deathAphorisms2[n].nSound, 255, 2, false);
                    }
                }
                String message = game.net.gProfile[nKiller].name + deathAphorisms2[n].text + game.net.gProfile[nVictim].name;
                if (deathAphorisms2[n].text2 != null) {
                    message += deathAphorisms2[n].text2;
                }
                viewSetMessage(message, -1, 7);
            }
        }
    }

    public static void playerFrag(PLAYER pVictim, int nKiller) {
        if (nKiller >= 0) {
            Sprite pKiller = boardService.getSprite(nKiller);
            if (IsDudeSprite(pKiller)) {
                if (IsPlayerSprite(pKiller)) {
                    playerFrag(gPlayer[pKiller.getLotag() - kDudePlayer1], pVictim);
                    int nVictimTeam = pVictim.teamID & 1;
                    int nKillerTeam = gPlayer[pKiller.getLotag() - kDudePlayer1].teamID & 1;

                    if (nKillerTeam == 0 && nVictimTeam == 0) {
                        evSend(0, 0, kChannelTeamADeath, kCommandToggle);
                    } else {
                        evSend(0, 0, kChannelTeamBDeath, kCommandToggle);
                    }
                } else {
                    if (pGameInfo.nGameType > kNetModeOff) {
                        int n = Random(deathAphorisms2.length);
                        String message = kDudeName[pKiller.getLotag() - kDudeBase] + deathAphorisms2[n].text + game.net.gProfile[pVictim.nPlayer].name;
                        if (deathAphorisms2[n].text2 != null) {
                            message += deathAphorisms2[n].text2;
                        }
                        viewSetMessage(message, -1, 7);
                    }
                }
            }
        }

        checkFragLimit();
    }

    public static void checkFragLimit() {
        if (pGameInfo.nFragLimit != 0) {
            if (pGameInfo.nGameType == kNetModeBloodBath) {
                int nFrags = 0;
                for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                    nFrags = Math.max(nFrags, gPlayer[i].fragCount);
                }

                if (nFrags >= pGameInfo.nFragLimit) {
                    evPostCallback(gMe.nSprite, SS_SPRITE, 300, 22);
                }
            }

            if (pGameInfo.nGameType == kNetModeTeams) {
                int nFrags = 0;
                for (int i = 0; i < 2; i++) {
                    nFrags = Math.max(nFrags, nTeamCount[i]);
                }

                if (nFrags >= pGameInfo.nFragLimit) {
                    evPostCallback(gMe.nSprite, SS_SPRITE, 300, 22);
                }
            }
        }
    }

    public static void FReviveCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        Sprite pSprite = boardService.getSprite(pXSprite.getReference());
        if (pGameInfo.nReviveMode && pGameInfo.nGameType == kNetModeCoop) {
            actHealDude(pXSprite, 30, 30);
        } else {
            actHealDude(pXSprite, 1, 2);
        }

        if (pGameInfo.nGameType > kNetModeOff && numplayers > 1) {
            sfxStart3DSound(pSprite, 3009, 0, 0);
            if (IsPlayerSprite(pSprite)) {
                PLAYER pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];
                String message;
                if (pPlayer == gMe) {
                    message = "I LIVE...AGAIN!!";
                } else {
                    message = game.net.gProfile[pPlayer.nPlayer].name + " lives again!";
                }

                viewSetMessage(message, -1, 10);
                pPlayer.pInput.newWeapon = 1;
            }
        }
    }

    public static void FatalityDeadCallback(int nXIndex) {
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            if (boardService.getXSprite(nXIndex) == gPlayer[i].pXsprite) {
                playerDamageSprite(gPlayer[i], gPlayer[i].fraggerID, 5, 8000);
                break;
            }
        }
    }

    public void copy(PLAYER pPlayer) {

        pSprite = pPlayer.pSprite;
        pXsprite = pPlayer.pXsprite;
        pDudeInfo = pPlayer.pDudeInfo;

        pInput = pPlayer.pInput;
        ang = pPlayer.ang;

        pWeaponQAV = pPlayer.pWeaponQAV;
        weaponCallback = pPlayer.weaponCallback;
        Run = pPlayer.Run;
        moveState = pPlayer.moveState;

        bobPhase = pPlayer.bobPhase;
        bobAmp = pPlayer.bobAmp;
        bobHeight = pPlayer.bobHeight;
        bobWidth = pPlayer.bobWidth;
        swayPhase = pPlayer.swayPhase;
        swayAmp = pPlayer.swayAmp;
        swayHeight = pPlayer.swayHeight;
        swayWidth = pPlayer.swayWidth;
        nPlayer = pPlayer.nPlayer;
        nSprite = pPlayer.nSprite;
        nLifeMode = pPlayer.nLifeMode;
        bloodlust = pPlayer.bloodlust;

        weapOffdZ = pPlayer.weapOffdZ;
        viewOffdZ = pPlayer.viewOffdZ;
        weaponAboveZ = pPlayer.weaponAboveZ;
        viewOffZ = pPlayer.viewOffZ;

        look = pPlayer.look;
        horiz = pPlayer.horiz;
        slope = pPlayer.slope;
        horizOff = pPlayer.horizOff;
        Underwater = pPlayer.Underwater;

        hasFlag = pPlayer.hasFlag;
        nBlueTeam = pPlayer.nBlueTeam;
        nRedTeam = pPlayer.nRedTeam;

        System.arraycopy(pPlayer.hasKey, 0, hasKey, 0, 8);
        System.arraycopy(pPlayer.damageShift, 0, damageShift, 0, 7);

        CrouchMode = pPlayer.CrouchMode;
        LastWeapon = pPlayer.LastWeapon;
        currentWeapon = pPlayer.currentWeapon;
        updateWeapon = pPlayer.updateWeapon;
        weaponTimer = pPlayer.weaponTimer;
        weaponState = pPlayer.weaponState;
        weaponAmmo = pPlayer.weaponAmmo;
        for (int i = 0; i < 14; i++) {
            weaponMode[i] = pPlayer.weaponMode[i];
            hasWeapon[i] = pPlayer.hasWeapon[i];
            weaponOrder[0][i] = pPlayer.weaponOrder[0][i];
            weaponOrder[1][i] = pPlayer.weaponOrder[1][i];
        }
        System.arraycopy(pPlayer.ammoCount, 0, ammoCount, 0, 12);

        fLoopQAV = pPlayer.fLoopQAV;
        fuseTime = pPlayer.fuseTime;
        fireClock = pPlayer.fireClock;

        throwTime = pPlayer.throwTime;
        aim = pPlayer.aim;
        relAim = pPlayer.relAim;
        nAimSprite = pPlayer.nAimSprite;
        aimCount = pPlayer.aimCount;
        System.arraycopy(pPlayer.aimSprites, 0, aimSprites, 0, 16);

        deathTime = pPlayer.deathTime;
        System.arraycopy(pPlayer.powerUpTimer, 0, powerUpTimer, 0, kMaxPowerUps);
        fragCount = pPlayer.fragCount;
        System.arraycopy(pPlayer.fragInfo, 0, fragInfo, 0, kMaxPlayers);
        teamID = pPlayer.teamID;
        fraggerID = pPlayer.fraggerID;
        airTime = pPlayer.airTime; // set when first going underwater, decremented while underwater then takes damage
        bloodTime = pPlayer.bloodTime;    // set when player walks through blood, decremented when on normal surface
        gooTime = pPlayer.gooTime;    // set when player walks through sewage, decremented when on normal surface
        wetTime = pPlayer.wetTime;    // set when player walks through water, decremented when on normal surface
        bubbleTime = pPlayer.bubbleTime; // set when first going underwater, decremented while underwater then takes damage

        stayTime = pPlayer.stayTime;
        kickTime = pPlayer.kickTime;
        pLaughsCount = pPlayer.pLaughsCount;
        TurnAround = pPlayer.TurnAround;

        godMode = pPlayer.godMode;
        fScreamed = pPlayer.fScreamed;
        pJump = pPlayer.pJump;
        showInventory = pPlayer.showInventory;
        choosedInven = pPlayer.choosedInven;
        for (int i = 0; i < kInventoryMax; i++) {
            Inventory[i].activated = pPlayer.Inventory[i].activated;
            Inventory[i].amount = pPlayer.Inventory[i].amount;
        }
        System.arraycopy(pPlayer.ArmorAmount, 0, ArmorAmount, 0, 3);
        //voodoo
        voodooTarget = pPlayer.voodooTarget;
        voodooCount = pPlayer.voodooCount;
        voodooAng = pPlayer.voodooAng;
        voodooUnk = pPlayer.voodooUnk;

        explosion = pPlayer.explosion;
        tilt = pPlayer.tilt;
        visibility = pPlayer.visibility;
        fireEffect = pPlayer.fireEffect;
        hitEffect = pPlayer.hitEffect;
        blindEffect = pPlayer.blindEffect;
        drownEffect = pPlayer.drownEffect;
        handCount = pPlayer.handCount;
        handDamage = pPlayer.handDamage;
        pickupEffect = pPlayer.pickupEffect;
        quakeTime = pPlayer.quakeTime;

        lookang = pPlayer.lookang;
        rotscrnang = pPlayer.rotscrnang;
    }

    public void reset() {
        pSprite = null;
        pXsprite = null;
        pDudeInfo = null;

        pInput.reset();
        ang = 0;

        pWeaponQAV = 0;
        weaponCallback = 0;
        Run = false;
        moveState = 0;

        bobPhase = 0;
        bobAmp = 0;
        bobHeight = 0;
        bobWidth = 0;
        swayPhase = 0;
        swayAmp = 0;
        swayHeight = 0;
        swayWidth = 0;
        nPlayer = 0;
        nSprite = 0;
        nLifeMode = 0;
        bloodlust = 0;

        weapOffdZ = 0;
        viewOffdZ = 0;
        weaponAboveZ = 0;
        viewOffZ = 0;

        look = 0;
        horiz = 0;
        slope = 0;
        horizOff = 0;
        Underwater = false;
        for (int i = 0; i < 8; i++) {
            hasKey[i] = false;
        }
        for (int i = 0; i < 7; i++) {
            damageShift[i] = 0;
        }

        CrouchMode = 0;
        LastWeapon = 0;
        currentWeapon = 0;
        updateWeapon = 0;
        weaponTimer = 0;
        weaponState = 0;
        weaponAmmo = 0;
        for (int i = 0; i < 14; i++) {
            weaponMode[i] = 0;
            hasWeapon[i] = false;
            weaponOrder[0][i] = 0;
            weaponOrder[1][i] = 0;
        }
        for (int i = 0; i < 12; i++) {
            ammoCount[i] = 0;
        }

        fLoopQAV = false;
        fuseTime = 0;
        fireClock = 0;

        throwTime = 0;
        aim.set(0, 0, 0);
        relAim.set(0, 0, 0);
        nAimSprite = 0;
        aimCount = 0;
        for (int i = 0; i < 16; i++) {
            aimSprites[i] = 0;
        }

        deathTime = 0;
        for (int i = 0; i < kMaxPowerUps; i++) {
            powerUpTimer[i] = 0;
        }
        fragCount = 0;
        for (int i = 0; i < kMaxPlayers; i++) {
            fragInfo[i] = 0;
        }
        teamID = 0;
        fraggerID = 0;
        airTime = 0; // set when first going underwater, decremented while underwater then takes damage
        bloodTime = 0;    // set when player walks through blood, decremented when on normal surface
        gooTime = 0;    // set when player walks through sewage, decremented when on normal surface
        wetTime = 0;    // set when player walks through water, decremented when on normal surface
        bubbleTime = 0; // set when first going underwater, decremented while underwater then takes damage

        kickTime = 0;
        stayTime = 0;
        pLaughsCount = 0;
        TurnAround = 0;

        godMode = false;
        fScreamed = false;
        pJump = false;
        showInventory = 0;
        choosedInven = 0;
        for (int i = 0; i < kInventoryMax; i++) {
            Inventory[i].activated = false;
            Inventory[i].amount = 0;
        }
        for (int i = 0; i < 3; i++) {
            ArmorAmount[i] = 0;
        }

        voodooTarget = 0;
        voodooCount = 0;
        voodooAng = 0;
        voodooUnk = 0;

        explosion = 0;
        tilt = 0;
        visibility = 0;
        fireEffect = 0;
        hitEffect = 0;
        blindEffect = 0;
        drownEffect = 0;
        handCount = 0;
        handDamage = false;
        pickupEffect = 0;
        quakeTime = 0;

        lookang = 0;
        rotscrnang = 0;
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder("NPSTART " + NPSTART + " \r\n");
        out.append("pWeaponQAV ").append(pWeaponQAV).append(" \r\n");
        out.append("weaponCallback ").append(weaponCallback).append(" \r\n");
        out.append("Run ").append(Run).append(" \r\n");
        out.append("moveState ").append(moveState).append(" \r\n");
        out.append("moveDist ").append(moveDist).append(" \r\n");
        out.append("bobAmp ").append(bobAmp).append(" \r\n");
        out.append("bobPhase ").append(bobPhase).append(" \r\n");
        out.append("bobHeight ").append(bobHeight).append(" \r\n");
        out.append("bobWidth ").append(bobWidth).append(" \r\n");
        out.append("swayAmp ").append(swayAmp).append(" \r\n");
        out.append("swayPhase ").append(swayPhase).append(" \r\n");
        out.append("swayHeight ").append(swayHeight).append(" \r\n");
        out.append("swayWidth ").append(swayWidth).append(" \r\n");
        out.append("nPlayer ").append(nPlayer).append(" \r\n");
        out.append("nSprite ").append(nSprite).append(" \r\n");
        out.append("nLifeMode ").append(nLifeMode).append(" \r\n");
        out.append("bloodlust ").append(bloodlust).append(" \r\n");
        out.append("viewOffZ ").append(viewOffZ).append(" \r\n");
        out.append("viewOffdZ ").append(viewOffdZ).append(" \r\n");
        out.append("weaponAboveZ ").append(weaponAboveZ).append(" \r\n");
        out.append("weapOffdZ ").append(weapOffdZ).append(" \r\n");
        out.append("look ").append(look).append(" \r\n");
        out.append("horiz ").append(horiz).append(" \r\n");
        out.append("slope ").append(slope).append(" \r\n");
        out.append("horizOff ").append(horizOff).append(" \r\n");
        out.append("Underwater ").append(Underwater).append(" \r\n");
        for (int j = 0; j < 8; j++) {
            out.append("hasKey[").append(j).append("] ").append(hasKey[j]).append(" \r\n");
        }
        out.append("hasFlag ").append(hasFlag).append(" \r\n");
        out.append("nBlueTeam ").append(nBlueTeam).append(" \r\n");
        out.append("nRedTeam ").append(nRedTeam).append(" \r\n");
        for (int j = 0; j < 7; j++) {
            out.append("damageShift[").append(j).append("] ").append(damageShift[j]).append(" \r\n");
        }
        out.append("currentWeapon ").append(currentWeapon).append(" \r\n");
        out.append("updateWeapon ").append(updateWeapon).append(" \r\n");
        out.append("weaponTimer ").append(weaponTimer).append(" \r\n");
        out.append("weaponState ").append(weaponState).append(" \r\n");
        out.append("weaponAmmo ").append(weaponAmmo).append(" \r\n");
        for (int j = 0; j < 14; j++) {
            out.append("hasWeapon[").append(j).append("] ").append(hasWeapon[j]).append(" \r\n");
        }
        for (int j = 0; j < 14; j++) {
            out.append("weaponMode[").append(j).append("] ").append(weaponMode[j]).append(" \r\n");
        }
        for (int j = 0; j < 14; j++) {
            out.append("weaponOrder[0][").append(j).append("] ").append(weaponOrder[0][j]).append(" \r\n");
        }
        for (int j = 0; j < 14; j++) {
            out.append("weaponOrder[1][").append(j).append("] ").append(weaponOrder[1][j]).append(" \r\n");
        }
        for (int j = 0; j < 12; j++) {
            out.append("ammoCount[").append(j).append("] ").append(ammoCount[j]).append(" \r\n");
        }
        out.append("fLoopQAV ").append(fLoopQAV).append(" \r\n");
        out.append("fuseTime ").append(fuseTime).append(" \r\n");
        out.append("fireClock ").append(fireClock).append(" \r\n");
        out.append("throwTime ").append(throwTime).append(" \r\n");
        out.append("aim[").append(aim.x).append(", ").append(aim.y).append(", ").append(aim.z).append("] \r\n");
        out.append("relAim[").append(relAim.x).append(", ").append(relAim.y).append(", ").append(relAim.z).append("] \r\n");
        out.append("nAimSprite ").append(nAimSprite).append(" \r\n");
        out.append("aimCount ").append(aimCount).append(" \r\n");
        for (int j = 0; j < 16; j++) {
            out.append("aimSprites[").append(j).append("] ").append(aimSprites[j]).append(" \r\n");
        }
        out.append("deathTime ").append(deathTime).append(" \r\n");
        for (int j = 0; j < kMaxPowerUps; j++) {
            out.append("powerUpTimer[").append(j).append("] ").append(powerUpTimer[j]).append(" \r\n");
        }
        out.append("fragCount ").append(fragCount).append(" \r\n");
        for (int j = 0; j < kMaxPlayers; j++) {
            out.append("fragInfo[").append(j).append("] ").append(fragInfo[j]).append(" \r\n");
        }

        out.append("teamID ").append(teamID).append(" \r\n");
        out.append("fraggerID ").append(fraggerID).append(" \r\n");
        out.append("airTime ").append(airTime).append(" \r\n");
        out.append("bloodTime ").append(bloodTime).append(" \r\n");
        out.append("gooTime ").append(gooTime).append(" \r\n");
        out.append("wetTime ").append(wetTime).append(" \r\n");
        out.append("bubbleTime ").append(bubbleTime).append(" \r\n");
        out.append("stayTime ").append(stayTime).append(" \r\n");
        out.append("kickTime ").append(kickTime).append(" \r\n");
        out.append("pLaughsCount ").append(pLaughsCount).append(" \r\n");
        out.append("TurnAround ").append(TurnAround).append(" \r\n");

        out.append("godMode ").append(godMode).append(" \r\n");
        out.append("fScreamed ").append(fScreamed).append(" \r\n");
        out.append("pJump ").append(pJump).append(" \r\n");
        out.append("showInventory ").append(showInventory).append(" \r\n");
        out.append("choosedInven ").append(choosedInven).append(" \r\n");
        for (int j = 0; j < 5; j++) {
            out.append("Inventory[").append(j).append("].activated ").append(Inventory[j].activated).append(" \r\n");
            out.append("Inventory[").append(j).append("].amount ").append(Inventory[j].amount).append(" \r\n");
        }
        for (int j = 0; j < 3; j++) {
            out.append("ArmorAmount[").append(j).append("] ").append(ArmorAmount[j]).append(" \r\n");
        }
        out.append("voodooTarget ").append(voodooTarget).append(" \r\n");
        out.append("voodooCount ").append(voodooCount).append(" \r\n");
        out.append("voodooAng ").append(voodooAng).append(" \r\n");
        out.append("voodooUnk ").append(voodooUnk).append(" \r\n");
        out.append("explosion ").append(explosion).append(" \r\n");
        out.append("tilt ").append(tilt).append(" \r\n");
        out.append("visibility ").append(visibility).append(" \r\n");
        out.append("hitEffect ").append(hitEffect).append(" \r\n");
        out.append("blindEffect ").append(blindEffect).append(" \r\n");
        out.append("drownEffect ").append(drownEffect).append(" \r\n");
        out.append("handCount ").append(handCount).append(" \r\n");
        out.append("handDamage ").append(handDamage).append(" \r\n");
        out.append("pickupEffect ").append(pickupEffect).append(" \r\n");
        out.append("fireEffect ").append(fireEffect).append(" \r\n");
        out.append("quakeTime ").append(quakeTime).append(" \r\n");

        return out.toString();
    }


    @Override
    public PLAYER readObject(InputStream is) throws IOException {
        this.NPSTART = StreamUtils.readInt(is);
        this.pWeaponQAV = StreamUtils.readInt(is);
        this.weaponCallback = StreamUtils.readInt(is);
        this.Run = StreamUtils.readBoolean(is);
        this.moveState = StreamUtils.readInt(is);
        this.moveDist = StreamUtils.readInt(is);
        this.bobAmp = StreamUtils.readInt(is);
        this.bobPhase = StreamUtils.readInt(is);
        this.bobHeight = StreamUtils.readInt(is);
        this.bobWidth = StreamUtils.readInt(is);
        this.swayAmp = StreamUtils.readInt(is);
        this.swayPhase = StreamUtils.readInt(is);
        this.swayHeight = StreamUtils.readInt(is);
        this.swayWidth = StreamUtils.readInt(is);
        this.nPlayer = StreamUtils.readInt(is);
        this.nSprite = StreamUtils.readInt(is);
        this.nLifeMode = StreamUtils.readInt(is);
        this.bloodlust = StreamUtils.readInt(is);
        this.viewOffZ = StreamUtils.readInt(is);
        this.viewOffdZ = StreamUtils.readInt(is);
        this.weaponAboveZ = StreamUtils.readInt(is);
        this.weapOffdZ = StreamUtils.readInt(is);
        if (nVersion < 300) {
            this.look = StreamUtils.readInt(is);
            this.horiz = StreamUtils.readInt(is);
        } else {
            this.look = StreamUtils.readFloat(is);
            this.horiz = StreamUtils.readFloat(is);
        }
        this.slope = StreamUtils.readInt(is);
        if (nVersion < 300) {
            this.horizOff = StreamUtils.readInt(is);
        } else {
            this.horizOff = StreamUtils.readFloat(is);
        }

        this.Underwater = StreamUtils.readBoolean(is);
        for (int j = 0; j < 8; j++) {
            this.hasKey[j] = StreamUtils.readBoolean(is);
        }
        this.hasFlag = StreamUtils.readUnsignedByte(is);
        if (nVersion >= 277) {
            this.nBlueTeam = (short) StreamUtils.readShort(is);
            this.nRedTeam = (short) StreamUtils.readShort(is);
            for (int j = 0; j < 12; j++) {
                StreamUtils.readUnsignedByte(is);
            }
        }

        for (int j = 0; j < 7; j++) {
            this.damageShift[j] = StreamUtils.readInt(is);
        }
        this.currentWeapon = StreamUtils.readByte(is);
        this.updateWeapon = StreamUtils.readByte(is);
        this.weaponTimer = StreamUtils.readInt(is);
        this.weaponState = StreamUtils.readInt(is);
        this.weaponAmmo = StreamUtils.readInt(is);
        for (int j = 0; j < 14; j++) {
            this.hasWeapon[j] = StreamUtils.readBoolean(is);
        }
        for (int j = 0; j < 14; j++) {
            this.weaponMode[j] = StreamUtils.readInt(is);
        }
        for (int j = 0; j < 14; j++) {
            this.weaponOrder[0][j] = StreamUtils.readInt(is);
        }
        for (int j = 0; j < 14; j++) {
            this.weaponOrder[1][j] = StreamUtils.readInt(is);
        }
        for (int j = 0; j < 12; j++) {
            this.ammoCount[j] = StreamUtils.readInt(is);
        }

        this.fLoopQAV = StreamUtils.readBoolean(is);
        this.fuseTime = StreamUtils.readInt(is);
        this.fireClock = StreamUtils.readInt(is);
        this.throwTime = StreamUtils.readInt(is);

        this.aim.set(StreamUtils.readInt(is), StreamUtils.readInt(is), StreamUtils.readInt(is));
        this.relAim.set(StreamUtils.readInt(is), StreamUtils.readInt(is), StreamUtils.readInt(is));
        this.nAimSprite = StreamUtils.readInt(is);
        this.aimCount = StreamUtils.readInt(is);
        for (int j = 0; j < 16; j++) {
            this.aimSprites[j] = StreamUtils.readShort(is);
        }
        this.deathTime = StreamUtils.readInt(is);
        for (int j = 0; j < kMaxPowerUps; j++) {
            this.powerUpTimer[j] = StreamUtils.readInt(is);
        }
        this.fragCount = StreamUtils.readInt(is);
        for (int j = 0; j < kMaxPlayers; j++) {
            this.fragInfo[j] = StreamUtils.readInt(is);
        }
        this.teamID = StreamUtils.readInt(is);
        this.fraggerID = StreamUtils.readInt(is);
        this.airTime = StreamUtils.readInt(is); // set when first going underwater, decremented while underwater then takes damage
        this.bloodTime = StreamUtils.readInt(is);    // set when player walks through blood, decremented when on normal surface
        this.gooTime = StreamUtils.readInt(is);// set when player walks through sewage, decremented when on normal surface
        this.wetTime = StreamUtils.readInt(is);    // set when player walks through water, decremented when on normal surface
        this.bubbleTime = StreamUtils.readInt(is); // set when first going underwater, decremented while underwater then takes damage
        StreamUtils.readInt(is);
        this.stayTime = StreamUtils.readInt(is);
        this.kickTime = StreamUtils.readInt(is);
        this.pLaughsCount = StreamUtils.readInt(is);
        this.TurnAround = StreamUtils.readInt(is);

        this.godMode = StreamUtils.readBoolean(is);
        this.fScreamed = StreamUtils.readBoolean(is);
        this.pJump = StreamUtils.readBoolean(is);
        this.showInventory = StreamUtils.readInt(is);
        this.choosedInven = StreamUtils.readInt(is);
        for (int j = 0; j < 5; j++) {
            this.Inventory[j].activated = StreamUtils.readBoolean(is);
            this.Inventory[j].amount = StreamUtils.readInt(is);
        }
        for (int j = 0; j < 3; j++) {
            this.ArmorAmount[j] = StreamUtils.readInt(is);
        }

        this.voodooTarget = StreamUtils.readInt(is);
        this.voodooCount = StreamUtils.readInt(is);
        this.voodooAng = StreamUtils.readInt(is);
        this.voodooUnk = StreamUtils.readInt(is);

        this.explosion = StreamUtils.readInt(is);
        this.tilt = StreamUtils.readInt(is);
        this.visibility = StreamUtils.readInt(is);
        this.hitEffect = StreamUtils.readInt(is);
        this.blindEffect = StreamUtils.readInt(is);
        this.drownEffect = StreamUtils.readInt(is);
        this.handCount = StreamUtils.readInt(is);
        this.handDamage = StreamUtils.readBoolean(is);
        this.pickupEffect = StreamUtils.readInt(is);
        this.fireEffect = StreamUtils.readInt(is);
        this.quakeTime = StreamUtils.readInt(is);

        return this;
    }

    @Override
    public PLAYER writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, NPSTART);
        StreamUtils.writeInt(os, pWeaponQAV);
        StreamUtils.writeInt(os, weaponCallback);
        StreamUtils.writeByte(os, Run ? (byte) 1 : 0);
        StreamUtils.writeInt(os, moveState);
        StreamUtils.writeInt(os, moveDist);
        StreamUtils.writeInt(os, bobAmp);
        StreamUtils.writeInt(os, bobPhase);
        StreamUtils.writeInt(os, bobHeight);
        StreamUtils.writeInt(os, bobWidth);
        StreamUtils.writeInt(os, swayAmp);
        StreamUtils.writeInt(os, swayPhase);
        StreamUtils.writeInt(os, swayHeight);
        StreamUtils.writeInt(os, swayWidth);
        StreamUtils.writeInt(os, nPlayer);
        StreamUtils.writeInt(os, nSprite);
        StreamUtils.writeInt(os, nLifeMode);
        StreamUtils.writeInt(os, bloodlust);
        StreamUtils.writeInt(os, viewOffZ);
        StreamUtils.writeInt(os, viewOffdZ);
        StreamUtils.writeInt(os, weaponAboveZ);
        StreamUtils.writeInt(os, weapOffdZ);
        if (nVersion < 300) {
            StreamUtils.writeInt(os, (int) look);
            StreamUtils.writeInt(os, (int) horiz);
        } else {
            StreamUtils.writeFloat(os, look);
            StreamUtils.writeFloat(os, horiz);
        }
        StreamUtils.writeInt(os, slope);
        if (nVersion < 300) {
            StreamUtils.writeInt(os, (int) horizOff);
        } else {
            StreamUtils.writeFloat(os, horizOff);
        }

        StreamUtils.writeByte(os, Underwater ? (byte) 1 : 0);
        for (int j = 0; j < 8; j++) {
            StreamUtils.writeByte(os, hasKey[j] ? (byte) 1 : 0);
        }
        StreamUtils.writeByte(os, (byte) hasFlag);

        if (nVersion >= 277) {
            StreamUtils.writeShort(os, nBlueTeam);
            StreamUtils.writeShort(os, nRedTeam);
            for (int j = 0; j < 12; j++) {
                StreamUtils.writeByte(os, (byte) 0);
            }
        }
        for (int j = 0; j < 7; j++) {
            StreamUtils.writeInt(os, damageShift[j]);
        }
        StreamUtils.writeByte(os, (byte) currentWeapon);
        StreamUtils.writeByte(os, (byte) updateWeapon);
        StreamUtils.writeInt(os, weaponTimer);
        StreamUtils.writeInt(os, weaponState);
        StreamUtils.writeInt(os, weaponAmmo);
        for (int j = 0; j < 14; j++) {
            StreamUtils.writeByte(os, hasWeapon[j] ? (byte) 1 : 0);
        }
        for (int j = 0; j < 14; j++) {
            StreamUtils.writeInt(os, weaponMode[j]);
        }
        for (int j = 0; j < 14; j++) {
            StreamUtils.writeInt(os, weaponOrder[0][j]);
        }
        for (int j = 0; j < 14; j++) {
            StreamUtils.writeInt(os, weaponOrder[1][j]);
        }
        for (int j = 0; j < 12; j++) {
            StreamUtils.writeInt(os, ammoCount[j]);
        }
        StreamUtils.writeByte(os, fLoopQAV ? (byte) 1 : 0);
        StreamUtils.writeInt(os, fuseTime);
        StreamUtils.writeInt(os, fireClock);
        StreamUtils.writeInt(os, throwTime);
        StreamUtils.writeInt(os, (int) aim.x);
        StreamUtils.writeInt(os, (int) aim.y);
        StreamUtils.writeInt(os, (int) aim.z);
        StreamUtils.writeInt(os, (int) relAim.x);
        StreamUtils.writeInt(os, (int) relAim.y);
        StreamUtils.writeInt(os, (int) relAim.z);
        StreamUtils.writeInt(os, nAimSprite);
        StreamUtils.writeInt(os, aimCount);
        for (int j = 0; j < 16; j++) {
            StreamUtils.writeShort(os, (short) aimSprites[j]);
        }
        StreamUtils.writeInt(os, deathTime);
        for (int j = 0; j < kMaxPowerUps; j++) {
            StreamUtils.writeInt(os, powerUpTimer[j]);
        }
        StreamUtils.writeInt(os, fragCount);
        for (int j = 0; j < kMaxPlayers; j++) {
            StreamUtils.writeInt(os, fragInfo[j]);
        }
        StreamUtils.writeInt(os, teamID);
        StreamUtils.writeInt(os, fraggerID);
        StreamUtils.writeInt(os, airTime); // set when first going underwater, decremented while underwater then takes damage
        StreamUtils.writeInt(os, bloodTime);    // set when player walks through blood, decremented when on normal surface
        StreamUtils.writeInt(os, gooTime);// set when player walks through sewage, decremented when on normal surface
        StreamUtils.writeInt(os, wetTime);    // set when player walks through water, decremented when on normal surface
        StreamUtils.writeInt(os, bubbleTime); // set when first going underwater, decremented while underwater then takes damage
        StreamUtils.writeInt(os, 0);
        StreamUtils.writeInt(os, stayTime);
        StreamUtils.writeInt(os, kickTime);
        StreamUtils.writeInt(os, pLaughsCount);
        StreamUtils.writeInt(os, TurnAround);

        StreamUtils.writeByte(os, godMode ? (byte) 1 : 0);
        StreamUtils.writeByte(os, fScreamed ? (byte) 1 : 0);
        StreamUtils.writeByte(os, pJump ? (byte) 1 : 0);
        StreamUtils.writeInt(os, showInventory);
        StreamUtils.writeInt(os, choosedInven);
        for (int j = 0; j < 5; j++) {
            StreamUtils.writeByte(os, Inventory[j].activated ? (byte) 1 : 0);
            StreamUtils.writeInt(os, Inventory[j].amount);
        }
        for (int j = 0; j < 3; j++) {
            StreamUtils.writeInt(os, ArmorAmount[j]);
        }

        StreamUtils.writeInt(os, voodooTarget);
        StreamUtils.writeInt(os, voodooCount);
        StreamUtils.writeInt(os, voodooAng);
        StreamUtils.writeInt(os, voodooUnk);

        StreamUtils.writeInt(os, explosion);
        StreamUtils.writeInt(os, tilt);
        StreamUtils.writeInt(os, visibility);
        StreamUtils.writeInt(os, hitEffect);
        StreamUtils.writeInt(os, blindEffect);
        StreamUtils.writeInt(os, drownEffect);
        StreamUtils.writeInt(os, handCount);
        StreamUtils.writeByte(os, handDamage ? (byte) 1 : 0);
        StreamUtils.writeInt(os, pickupEffect);
        StreamUtils.writeInt(os, fireEffect);
        StreamUtils.writeInt(os, quakeTime);

        return this;
    }
}

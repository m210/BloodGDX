// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.PriorityQueue;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface IPriorityQueue {

    void flush();

    void Insert(long time, int event);

    int Remove();

    void checkList(int nIndex, int nType);

    void checkList(int nIndex, int nType, int funcId);

    boolean Check(long time);

    int getSize();

    PriorityItem getItem(int i);

    default void readObject(InputStream is) throws IOException {
        int fNodeCount = StreamUtils.readInt(is);
        for (int i = 0; i < fNodeCount; i++) {
            int qEventPriority = StreamUtils.readInt(is);
            int qEventEvent = StreamUtils.readInt(is);
            Insert(qEventPriority, qEventEvent);
        }
    }

    void writeObject(OutputStream os) throws IOException;

}

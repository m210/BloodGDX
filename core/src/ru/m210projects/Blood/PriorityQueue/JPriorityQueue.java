// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.PriorityQueue;

import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.OutputStream;

import static ru.m210projects.Blood.EVENT.*;

public class JPriorityQueue extends java.util.PriorityQueue<PriorityItem> implements IPriorityQueue {
    private static final long serialVersionUID = 1L;

    private final int kPQueueSize;
    private final PriorityItem[] qList;
    private final PriorityItem[] queueArray;

    public JPriorityQueue(int size) {
        super(size);
        this.kPQueueSize = size;
        qList = new PriorityItem[size];
        queueArray = new PriorityItem[size];
        for (int i = 0; i < size; i++)
            qList[i] = new PriorityItem(0, 0);
    }

    @Override
    public void flush() {
        clear();
        for (int i = 0; i < qList.length; i++)
            qList[i].isFree = true;
    }

    @Override
    public void Insert(long time, int event) {
        if (size() >= kPQueueSize) throw new AssertException("fNodeCount < kPQueueSize");

        int fNodeCount = 0;
        while (!qList[fNodeCount].isFree) fNodeCount++;

        qList[fNodeCount].priority = time;
        qList[fNodeCount].event = event;
        qList[fNodeCount].isFree = false;

        add(qList[fNodeCount]);
    }

    @Override
    public int Remove() {
        PriorityItem item = remove();
        item.isFree = true;
        return item.event;
    }

    @Override
    public void checkList(int nIndex, int nType) {
        int i = 0;
        PriorityItem[] priorityList = toArray(queueArray);
        while (i < size()) {
            PriorityItem item = priorityList[i];
            if (nIndex == getIndex(item.event) && nType == getType(item.event)) {
                item.isFree = true;
                remove(item);
                priorityList = toArray(queueArray);
            } else ++i;
        }
    }

    @Override
    public void checkList(int nIndex, int nType, int funcId) {
        int i = 0;
        PriorityItem[] priorityList = toArray(queueArray);
        while (i < size()) {
            PriorityItem item = priorityList[i];
            if (nIndex == getIndex(item.event) && nType == getType(item.event) && funcId == getFuncID(item.event)) {
                item.isFree = true;
                remove(item);
                priorityList = toArray(queueArray);
            } else ++i;
        }
    }

    @Override
    public boolean Check(long time) {
        return size() > 0 && time >= element().priority;
    }

    @Override
    public PriorityItem getItem(int i) {
        return toArray(queueArray)[i];
    }

    @Override
    public void writeObject(OutputStream os) throws IOException {
        final int size = getSize();
        PriorityItem[] priorityList = toArray(queueArray);

        StreamUtils.writeInt(os, size + 1);
        StreamUtils.writeInt(os, 0); // priority[0]
        StreamUtils.writeInt(os, 0); // event[0]

        for (int i = 0; i < size; i++) {
            PriorityItem item = priorityList[i];
            StreamUtils.writeInt(os, (int) item.priority);
            StreamUtils.writeInt(os, item.event);
        }
    }

    @Override
    public int getSize() {
        return size();
    }
}

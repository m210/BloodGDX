package ru.m210projects.Blood;

import ru.m210projects.Blood.Fonts.QFNFont;
import ru.m210projects.Blood.Types.EpisodeManager;
import ru.m210projects.Blood.Types.SFX;
import ru.m210projects.Blood.Types.Seq.SeqType;
import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Blood.filehandlers.EpisodeEntry;
import ru.m210projects.Blood.filehandlers.UserEntry;
import ru.m210projects.Blood.filehandlers.scripts.BloodDef;
import ru.m210projects.Blood.filehandlers.scripts.RFScript;
import ru.m210projects.Build.Script.CueScript;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.filehandle.rff.RffEntry;
import ru.m210projects.Build.filehandle.rff.RffFile;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.util.*;

import static ru.m210projects.Blood.LEVELS.currentEpisode;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.Mirror.InitMirrorTiles;
import static ru.m210projects.Blood.SOUND.pSFXs;
import static ru.m210projects.Blood.SOUND.usertrack;
import static ru.m210projects.Blood.Tile.*;
import static ru.m210projects.Blood.VERSION.hasQFN;
import static ru.m210projects.Blood.View.InitBallBuffer;
import static ru.m210projects.Blood.Weapon.WeaponInit;
import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.HIGHEST;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_DIRECTORY;

public class ResourceHandler {

    public static EpisodeManager episodeManager = new EpisodeManager();

    public static boolean usecustomarts;
    public static boolean usecustomqavs;
    private static RffFile usergroup;

    /**
     * 1. searches rfs scripts (entry id remap)
     * 2. searches cue scripts (usertracks remap)
     * 3. searches 1,2 in subgroups
     * 4. adds subgroups to entry list
     * 5. adds to usergroup entries with remapped id
     */
    public static void searchEpisodeResources(Group container, RffFile resourceHolder) {
        Map<String, Integer> ids = new HashMap<>();
        List<Entry> resources = new ArrayList<>();

        searchScripts(container, resources, ids);

        for (Entry file : resources) {
            if (file instanceof RffEntry) {
                resourceHolder.addEntry((RffEntry) file);
                continue;
            }
            Integer id = ids.getOrDefault(file.getName().toUpperCase(), -1);
            resourceHolder.addEntry(new UserEntry(file, id));
        }
    }

    private static void searchScripts(Group container, List<Entry> entries, Map<String, Integer> ids) {
        //search rfs scripts first
        for (Entry file : container.getEntries()) {
            if (file.isExtension("rfs")) {
                RFScript scr = new RFScript(file.getName(), file);
                for (Map.Entry<Integer, SFX> sfx : scr.getSfxList().entrySet()) {
                    pSFXs[sfx.getKey()] = sfx.getValue();
                }
                ids.putAll(scr.getIds());
            } else if (file.isExtension("cue")) {
                CueScript cdTracks = new CueScript(file.getName(), file);
                String[] cdtracks = cdTracks.getTracks();
                int num = 0;
                for (int i = 0; i < cdtracks.length; i++) {
                    if (cdtracks[i] != null) {
                        usertrack[i] = game.getFilename(cdtracks[i]);
                        num++;
                    }
                }
                Console.out.println(num + " cd tracks found...");
            }
        }

        for (Entry file : container.getEntries()) {
            Group subContainer = DUMMY_DIRECTORY;
            if (file.isDirectory() && file instanceof FileEntry) {
                subContainer = ((FileEntry) file).getDirectory();
            } else if (file.isExtension("zip")
                    || file.isExtension("grp")
                    || file.isExtension("pk3")
                    || file.isExtension("rff")) {
                subContainer = game.getCache().newGroup(file);
            }

            if (!subContainer.equals(DUMMY_DIRECTORY)) {
                searchScripts(subContainer, entries, ids);
            } else {
                entries.add(file);
            }
        }
    }

    /**
     * 1. resets game resource to default
     * 2. creates new blooddef "addonScript" (should add to cache userdef group) - remap tiles and hrp + remap fileid (UserEntry)
     * 3. gets BloodIni file:
     * if file is group adding it to cache, loads internal bloodgdx.def to "addonScript" (remap tiles and hrp), calls preparegroup
     * if file is file calls searchResource anf loads bloodgdx.def to "addonScript"
     */
    public static void checkEpisodeResources(BloodIniFile ini) throws WarningException {
        if (ini == null) {
            return;
        }

        if (currentEpisode != null && ini.equals(currentEpisode.iniFile)) {
            return;
        }

        resetEpisodeResources();

        usergroup = new RffFile("RemovableGroup");

        EpisodeEntry addonEntry = ini.getEpisodeEntry();
        BloodDef addonScript;
        Group parent = addonEntry.getGroup();
        if (addonEntry.isPackageEpisode()) //if in main blood folder
        {
            addonScript = new BloodDef(game.getBaseDef(), addonEntry.getFileEntry());
            try {
                Entry res = parent.getEntry(appdef); // load def scripts before delete folders
                if (res.exists()) {
                    addonScript.loadScript(parent.getName() + " script", res);
                }
                searchEpisodeResources(parent, usergroup);
            } catch (Exception e) {
                throw new WarningException("Error found in " + ((EpisodeEntry.Pack) addonEntry).getName() + "\r\n" + e);
            }
        } else {
            addonScript = new BloodDef(game.getBaseDef(), addonEntry.getFileEntry());
            if (!game.getCache().isGameDirectory(parent)) {
                searchEpisodeResources(parent, usergroup);
                Entry def = parent.getEntry(appdef);
                if (def.exists()) {
                    addonScript.loadScript(def);
                }
            }
        }

        if (ini.getName().equalsIgnoreCase("cryptic.ini")) { //official addon
            engine.loadpic(parent.getEntry("CPART07.AR_"));
            engine.loadpic(parent.getEntry("CPART15.AR_"));
            engine.loadpic(parent.getEntry("BART07.AR_"));
            engine.loadpic(parent.getEntry("BART15.AR_"));
            usecustomarts = true;
        }

        game.getCache().addGroup(usergroup, HIGHEST);
        InitGroupResources(addonScript, usergroup.getEntries());
        game.setDefs(addonScript);

        InitBallBuffer();
    }

    /**
     * Loads resources to engine from entry list
     * 1. loads arts
     * 2. loads voxels
     * 3. loads surfaces
     * 4. loads shades
     * 5. loads qavs
     */
    public static void InitGroupResources(BloodDef script, List<Entry> list) {
        for (Entry res : list) {
            if (res.isExtension("art")) {
                engine.loadpic(res);
                usecustomarts = true;
            } else if (res.getName().equalsIgnoreCase("voxel.dat")) {
                voxelsInit(res.getName());
                Console.out.println("Found voxel.dat. Loading... ");
            } else if (res.getName().equalsIgnoreCase("surface.dat")) {
                script.surfaceInit(res.getName());
                Console.out.println("Found surface.dat. Loading... ");
            } else if (res.getName().equalsIgnoreCase("shade.dat")) {
                shadeInit(res.getName());
                Console.out.println("Found shade.dat. Loading... ");
            } else if (res.isExtension("qav")) {
                usecustomqavs = true;
            }
        }

        if (usecustomqavs) {
            Console.out.println("Found qav files. Loading... ");
            WeaponInit(); //reload qavs animations
        }
    }

    public static void resetEpisodeResources() {
        Console.out.println("Resetting custom resources", OsdColor.GREEN);
        if (usergroup != null) {
            game.getCache().removeGroup(usergroup);
        }
        usergroup = null;

        Arrays.fill(pSFXs, null); //reset user sounds
        SeqType.flushCache(); //reset user seqs
        Arrays.fill(usertrack, null); //reset cd tracks

        if (usecustomqavs) {
            WeaponInit();
            usecustomqavs = false;
        }

        if (!usecustomarts) {
            game.setDefs(game.baseDef);
            return;
        }

        //Reset to default resources

        System.err.println("Reset to default resources");
        if (engine.loadpics() == 0) {
            throw new AssertException("ART files not found " + game.getCache().getGameDirectory().getPath().resolve(engine.getTileManager().getTilesPath()));
        }

        tileInit();
        game.setDefs(game.baseDef);

        InitMirrorTiles();

        if (hasQFN) {
            for (int i = 0; i < 5; i++) {
                ((QFNFont) game.getFont(i)).rebuildChar();
            }
        }


        InitBallBuffer();
        usecustomarts = false;
    }

    public static BloodIniFile levelGetEpisode(Entry entry) {
        if (!entry.exists() || !entry.isExtension("ini")) {
            return null;
        }

        if (entry instanceof FileEntry) {
            List<EpisodeEntry> list = episodeManager.getEpisodeEntries((FileEntry) entry);
            if (!list.isEmpty()) {
                return episodeManager.getEpisode(list.get(0));
            }
        }

        return null;
    }
}

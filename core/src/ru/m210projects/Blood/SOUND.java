// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.utils.BufferUtils;
import ru.m210projects.Blood.Types.*;
import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Build.Architecture.common.audio.AudioChannel;
import ru.m210projects.Build.Architecture.common.audio.BuildAudio;
import ru.m210projects.Build.Architecture.common.audio.SoundData;
import ru.m210projects.Build.Architecture.common.audio.Source;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.rff.RffEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.currentEpisode;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.Strings.*;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.View.gViewIndex;
import static ru.m210projects.Build.Pragmas.*;

public class SOUND {

    public static final int BLUDSPLT = 385;
    private static final String TRACK = "blood%02d.ogg";

    public static final int MAXUSERTRACKS = 64;
    // sound of sound in XY units / sec
    public static final int kSoundVel = M2X(280.0);
    // Intra-aural delay in seconds
    public static final float kIntraAuralTime = 0.0006f;
    public static final int kIntraAuralDist = (int) (kIntraAuralTime / 2 * kSoundVel);
    // volume different between sound in front and behind pinna focus
    public static final int kBackFilter = 0x4000;
    public static final int kVolScale = 80;
    public static final int kMaxAmbients = 16;
    public static final int kMaxChannels = 32;
    public static final int kMaxSources = 256;
    public static final int[] sndRate = {11025, 11025, 11025, 11025, 11025, 22050, 22050, 22050, 22050, 44100, 44100, 44100, 44100};
    public static String[] usertrack = new String[MAXUSERTRACKS];
    public static SFX[] pSFXs = new SFX[65536];
    public static int nSndEffect;
    public static SOUNDEFFECT[] soundEffect;
    public static SAMPLE2D[] Channel;

    public static int earX0;
    public static int earX;
    public static int earY0;
    public static int earY;
    public static int earZ0;
    public static int earZ;
    public static int earVX;
    public static int earVY;
    public static int earVZ;
    public static int earVolume;
    public static int earVelocity;

    public static AMBIENT[] ambient;
    public static int numambients;
    public static int currTrack = -1;
    public static BloodIniFile currEpisode = null;
    public static String currSong = null;
    public static Music currMusic = null;

    private static BuildAudio audio;

    public static void sndHandlePause(boolean gPaused) {
        if (gPaused) {
            if (currMusic != null) {
                currMusic.pause();
            }
            stopAllSounds();
        } else {
            if (!cfg.isMuteMusic() && currMusic != null) {
                currMusic.play();
            }
        }
    }

    public static SoundData.Decoder getSoundDecoder(String extension) {
        return audio.getSoundDecoder(extension);
    }

    public static boolean sndLoadSound(int soundId) {
        if (soundId < 0) {
            return false;
        }

        if (pSFXs[soundId] == null) {
            Entry dat = game.getCache().getEntry(soundId, sfx);
            if (dat.exists()) {
                try {
                    pSFXs[soundId] = new SFX(dat);
                } catch (IOException e) {
                    return false;
                }
            } else {
                return false;
            }
        }

        if (pSFXs[soundId].hResource == null) {
            String rawName = pSFXs[soundId].rawName + "." + raw;
            Entry fp = game.getCache().getEntry(rawName, true);

            if (fp.exists()) {
                int size = (int) fp.getSize();
                pSFXs[soundId].hResource = ByteBuffer.allocateDirect(size);
                pSFXs[soundId].hResource.put(fp.getBytes());
                pSFXs[soundId].hResource.rewind();
                pSFXs[soundId].size = size;
            } else {
                return false; //can't load resource
            }
        }

        return true;
    }

    public static void sfxStart3DSound(Sprite pSprite, int soundId, int nChannel, int flags) {
        if (cfg.isNoSound()) {
            return;
        }

        if (pSprite != null && soundId >= 0) {
            if (!sndLoadSound(soundId)) {
                return;
            }

            SFX pEffect = pSFXs[soundId];

            if (pEffect.size <= 0) {
                return;
            }

            int nPitch = mulscale(sndGetSampleRate(pEffect.format), pEffect.pitch, 16);
            SOUNDEFFECT pSource = null;
            if (nChannel < 0) {
                if (nSndEffect >= kMaxSources) {
                    return;
                }

                pSource = soundEffect[nSndEffect++];
                pSource.pSprite = null;
            } else {
                int i;
                for (i = 0; i < nSndEffect; i++) {
                    pSource = soundEffect[i];

                    if (nChannel == pSource.channel && (pSprite == pSource.pSprite || (flags & 1) != 0)) {
                        if (pSource.hVoice != null /* && pSource.hVoice.data == pSource.hResource */ && ((flags & 4) != 0 && nChannel == pSource.channel || ((flags & 2) != 0 && soundId == pSource.soundId))) {
                            return;
                        }

                        if (pSource.hVoice != null) {
                            pSource.hVoice.stop();
                        }
                        break;
                    }
                }

                if (i == nSndEffect) {
                    if (nSndEffect >= kMaxSources) {
                        return;
                    }
                    pSource = soundEffect[nSndEffect++];
                }
                pSource.pSprite = pSprite;
                pSource.channel = nChannel;
            }

            pSource.x = pSprite.getX();
            pSource.y = pSprite.getY();
            pSource.z = pSprite.getZ();
            pSource.nSector = pSprite.getSectnum();
            pSource.oldX = pSource.x;
            pSource.oldY = pSource.y;
            pSource.oldZ = pSource.z;
            pSource.soundId = soundId;
//            pSource.hResource = pEffect.hResource;
            pSource.relVol = pEffect.relVol;
            pSource.nPitch = nPitch;

            Calc3DValues(pSource);
            int nPriority = 1;

            if (nPriority < earVolume) {
                nPriority = earVolume;
            }

            int loopStart = pEffect.loopStart;
            if (nChannel < 0) {
                loopStart = -1;
            }

            ByteBuffer pRaw = pEffect.hResource;
            pRaw.rewind();

            boolean global = pSprite == gPlayer[gViewIndex].pSprite;
            pSource.hVoice = newSound(pRaw, global ? pSource.nPitch : earVelocity, 8, nPriority);
            if (pSource.hVoice != null) {
                pSource.hVoice.setListener(pSource);
                if (global) {
                    pSource.pSprite = gPlayer[gViewIndex].pSprite;
                } else {
                    setPosition(pSource);
                }

                if (loopStart >= 0) {
                    pSource.hVoice.loop(earVolume / 255.0f, loopStart, ClipLow(pEffect.size - 1, 0));
                } else {
                    pSource.hVoice.play(earVolume / 255.0f);
                }
            }
        }
    }

    // same as previous, but allows to set custom pitch and volume
    public static void sfxStart3DSoundCP(Sprite pSprite, int soundId, int nChannel, int flags, long pitch, int volume) {
        if (cfg.isNoSound()) {
            return;
        }

        if (pSprite != null && soundId >= 0) {
            if (!sndLoadSound(soundId)) {
                return;
            }

            SFX pEffect = pSFXs[soundId];

            if (pitch <= 0) {
                pitch = pEffect.pitch;
            } else {
                pitch -= Gameutils.BiRandom(pEffect.pitchrange);
            }

            if (pEffect.size <= 0) {
                return;
            }

            int nPitch = mulscale(sndGetSampleRate(pEffect.format), pitch, 16);
            SOUNDEFFECT pSource = null;
            if (nChannel < 0) {
                if (nSndEffect >= kMaxSources) {
                    return;
                }

                pSource = soundEffect[nSndEffect++];
                pSource.pSprite = null;
            } else {
                int i;
                for (i = 0; i < nSndEffect; i++) {
                    pSource = soundEffect[i];

                    if (nChannel == pSource.channel && (pSprite == pSource.pSprite || (flags & 1) != 0)) {
                        if (pSource.hVoice != null /* && pSource.hVoice.data == pSource.hResource */ && ((flags & 4) != 0 && nChannel == pSource.channel || ((flags & 2) != 0 && soundId == pSource.soundId))) {
                            return;
                        }

                        if (pSource.hVoice != null) {
                            pSource.hVoice.stop();
                        }

//                        if (pSource.hResource != null) {
//                            pSource.hResource = null;
//                        }
                        break;
                    }
                }

                if (i == nSndEffect) {
                    if (nSndEffect >= kMaxSources) {
                        return;
                    }
                    pSource = soundEffect[nSndEffect++];
                }
                pSource.pSprite = pSprite;
                pSource.channel = nChannel;
            }

            pSource.x = pSprite.getX();
            pSource.y = pSprite.getY();
            pSource.z = pSprite.getZ();
            pSource.nSector = pSprite.getSectnum();
            pSource.oldX = pSource.x;
            pSource.oldY = pSource.y;
            pSource.oldZ = pSource.z;
            pSource.soundId = soundId;
//            pSource.hResource = pEffect.hResource;
            pSource.relVol = ((volume == 0) ? pEffect.relVol : ((volume == -1) ? 0 : (Math.min(volume, 255))));
            pSource.nPitch = nPitch;

            Calc3DValues(pSource);
            int nPriority = 1;

            if (nPriority < earVolume) {
                nPriority = earVolume;
            }

            int loopStart = pEffect.loopStart;
            if (nChannel < 0) {
                loopStart = -1;
            }

            ByteBuffer pRaw = pEffect.hResource;
            pRaw.rewind();

            boolean global = pSprite == gPlayer[gViewIndex].pSprite;
            pSource.hVoice = newSound(pRaw, global ? pSource.nPitch : earVelocity, 8, nPriority);
            if (pSource.hVoice != null) {
                pSource.hVoice.setListener(pSource);
                if (global) {
                    pSource.pSprite = gPlayer[gViewIndex].pSprite;
                } else {
                    setPosition(pSource);
                }

                if (loopStart >= 0) {
                    pSource.hVoice.loop(earVolume / 255.0f, loopStart, ClipLow(pEffect.size - 1, 0));
                } else {
                    pSource.hVoice.play(earVolume / 255.0f);
                }

            }
        }
    }

    public static void sfxCreate3DSound(int x, int y, int z, int soundId, int nSector) {
        if (cfg.isNoSound()) {
            return;
        }

        if (!boardService.isValidSector(nSector)) {
            return;
        }

        if (soundId < 0) {
            throw new AssertException("Invalid sound ID: " + soundId);
        }

        if (!sndLoadSound(soundId)) {
            return;
        }

        SFX pEffect = pSFXs[soundId];

        if (pEffect.size <= 0) {
            return;
        }

        int nPitch = mulscale(sndGetSampleRate(pEffect.format), pEffect.pitch, 16);

        if (nSndEffect < kMaxSources) {
            SOUNDEFFECT pSoundEffect = soundEffect[nSndEffect++];
            pSoundEffect.pSprite = null;
            pSoundEffect.x = x;
            pSoundEffect.y = y;
            pSoundEffect.z = z;
            pSoundEffect.nSector = nSector;
            FindSector(x, y, z, (short) pSoundEffect.nSector);
            pSoundEffect.nSector = foundSector;

            pSoundEffect.oldX = pSoundEffect.x;
            pSoundEffect.oldY = pSoundEffect.y;
            pSoundEffect.oldZ = pSoundEffect.z;
            pSoundEffect.soundId = soundId;
            pSoundEffect.relVol = pEffect.relVol;
            pSoundEffect.nPitch = nPitch;
            pSoundEffect.format = pEffect.format;

            ByteBuffer pRaw = pEffect.hResource;
            pRaw.rewind();

            Calc3DValues(pSoundEffect);

            int nPriority = 1;
            if (nPriority < earVolume) {
                nPriority = earVolume;
            }

            pSoundEffect.hVoice = newSound(pRaw, earVelocity, 8, nPriority);
            if (pSoundEffect.hVoice != null) {
                setPosition(pSoundEffect);
                pSoundEffect.hVoice.setListener(pSoundEffect);
                pSoundEffect.hVoice.play(earVolume / 255.0f);
            }
        }
    }

    public static void setPosition(SOUNDEFFECT pSoundEffect) {
        pSoundEffect.hVoice.setPosition(pSoundEffect.x, pSoundEffect.z >> 4, pSoundEffect.y);
    }

    public static void Calc3DValues(SOUNDEFFECT pSource) {
        if (cfg.isNoSound()) {
            return;
        }

        int dx = pSource.x - gPlayer[gViewIndex].pSprite.getX();
        int dy = pSource.y - gPlayer[gViewIndex].pSprite.getY();
        int dz = pSource.z - gPlayer[gViewIndex].pSprite.getZ();
        int nAngle = EngineUtils.getAngle(dx, dy);
        int dist = Dist3d(dx, dy, dz);
        dist = ClipLow((dist >> 3) + (dist >> 2), 64);
        int monoVol = muldiv(pSource.relVol, kVolScale, dist);

        // normal vector for listener -> source
        dx = Cos(nAngle);
        dy = Sin(nAngle);

        earVolume = Vol3d(nAngle - (gPlayer[gViewIndex].pSprite.getAng()), monoVol);
        earVelocity = pSource.nPitch;

        int div = dmulscaler(dx, pSource.x - pSource.oldX, dy, pSource.y - pSource.oldY, 30) + 5853;
        int kVel = klabs(dmulscaler(dx, earVX, dy, earVY, 30) + 5853);
        if (div != 0) {
            earVelocity = muldiv(kVel, pSource.nPitch, div);
        }
    }

    public static int Vol3d(int nAngle, int vol) {
        return vol - mulscale(vol, kBackFilter / 2 - mulscale(kBackFilter / 2, Cos(nAngle), 30), 16);
    }

    public static void sfxResetListener() {
        earX0 = earX = gPlayer[gViewIndex].pSprite.getX();
        earY0 = earY = gPlayer[gViewIndex].pSprite.getY();
        earZ0 = earZ = gPlayer[gViewIndex].pSprite.getZ();
    }

    public static void sfxUpdate3DSounds() {
        if (cfg.isNoSound() || nSndEffect >= kMaxSources) {
            return;
        }

        // update listener ear positions

        earX0 = earX;
        earY0 = earY;
        earZ0 = earZ;
        earX = gPlayer[gViewIndex].pSprite.getX();
        earY = gPlayer[gViewIndex].pSprite.getY();
        earZ = gPlayer[gViewIndex].pSprite.getZ();
        earVX = earX - earX0;
        earVY = earY - earY0;
        earVZ = earZ - earZ0;

        audio.setListener(earX0, earZ0 >> 4, earY0, gPlayer[gViewIndex].pSprite.getAng());

        for (int i = nSndEffect - 1; i >= 0; --i) {
            SOUNDEFFECT pSoundEffect = soundEffect[i];
            if (pSoundEffect.hVoice == null) {
                SoundCallback(i);
                continue;
            }

            Sprite pSprite = pSoundEffect.pSprite;
            if (pSprite != null) {
                pSoundEffect.oldX = pSoundEffect.x;
                pSoundEffect.oldY = pSoundEffect.y;
                pSoundEffect.oldZ = pSoundEffect.z;
                pSoundEffect.x = pSprite.getX();
                pSoundEffect.y = pSprite.getY();
                pSoundEffect.z = pSprite.getZ();
                pSoundEffect.nSector = pSprite.getSectnum();
            }

            Calc3DValues(pSoundEffect);
            if (pSoundEffect.hVoice != null) {
                if (pSoundEffect.pSprite != gPlayer[gViewIndex].pSprite) { //not global sound
                    pSoundEffect.hVoice.setVolume(earVolume / 255.0f);
                    float pitch = 0;
                    if (pSoundEffect.nPitch > 0) {
                        pitch = (float) earVelocity / pSoundEffect.nPitch;
                    }
                    pSoundEffect.hVoice.setPitch(pitch);
                    setPosition(pSoundEffect);
                }
            }
        }
    }

    public static void SoundCallback(int num) {
        SOUNDEFFECT pSoundEffect = soundEffect[num];
        pSoundEffect.clear();

        int tmp = nSndEffect - 1;
        soundEffect[num] = soundEffect[tmp];
        soundEffect[tmp] = pSoundEffect;
        nSndEffect = tmp;
    }

    public static void sfxKill3DSound(Sprite pSprite, int nChannel, int nSoundId) {
        int i = nSndEffect;
        while (--i >= 0) {
            SOUNDEFFECT pSoundEffect = soundEffect[i];
            if (pSprite == pSoundEffect.pSprite && (nChannel < 0 || nChannel == pSoundEffect.channel) && (nSoundId < 0 || nSoundId == pSoundEffect.soundId)) {
                if (pSoundEffect.hVoice != null) {
                    pSoundEffect.hVoice.stop();
                }
            }
        }
    }

    public static void sfxKillAll3DSounds() {
        int i = nSndEffect;
        while (--i >= 0) {
            SOUNDEFFECT pSoundEffect = soundEffect[i];

            if (pSoundEffect.hVoice != null) {
                pSoundEffect.hVoice.stop();
            }
        }
    }

    public static void varsInit() {
        nSndEffect = 0;
        Channel = new SAMPLE2D[kMaxChannels];
        for (int i = 0; i < kMaxChannels; i++) {
            Channel[i] = new SAMPLE2D();
        }

        soundEffect = new SOUNDEFFECT[kMaxSources];
        for (int i = 0; i < kMaxSources; i++) {
            soundEffect[i] = new SOUNDEFFECT();
        }

        ambient = new AMBIENT[kMaxAmbients];
        for (int i = 0; i < kMaxAmbients; i++) {
            ambient[i] = new AMBIENT();
        }
        numambients = 0;
    }

    public static void sndInit() {
        cfg.setAudioDriver(cfg.getAudioDriver());
        cfg.setMidiDevice(cfg.getMidiDevice());
        SOUND.audio = cfg.getAudio();

        varsInit();
    }

    public static int sndGetSampleRate(int SND_FMT) {
        if (SND_FMT >= 13) {
            return 11025;
        } else {
            return sndRate[SND_FMT];
        }
    }

    public static SAMPLE2D FindChannel() {
        for (int i = kMaxChannels - 1; i >= 0; --i) {
            if (Channel[i].hResource == null) {
                return Channel[i];
            }
        }
        return null;
    }

    public static void sndStopSound(SAMPLE2D pChannel) {
        if ((pChannel.loop & 1) != 0) {
            pChannel.loop &= ~1;
        }

        pChannel.hVoice.stop();
        pChannel.hVoice = null;
    }

    public static void sndStopAllSounds() {
        sndStopAllSamples();
        sndStopMusic();
        sfxKillAll3DSounds();
        ambStopAll();
        stopAllSounds();
    }

    public static void sndStopAllSamples() {
        for (int i = 0; i < kMaxChannels; i++) {
            SAMPLE2D pChannel = Channel[i];
            if (pChannel.hVoice != null) {
                sndStopSound(pChannel);
            }
            if (pChannel.hResource != null) {
                pChannel.hResource = null;
            }
        }
    }

    public static void sndStartSample(int soundId, int nVol, int nChannel, boolean loop) {
        if (cfg.isNoSound()) {
            return;
        }

        if (!(nChannel >= -1 && nChannel < kMaxChannels)) {
            throw new AssertException("nChannel >= -1 && nChannel < kMaxChannels");
        }

        if (!sndLoadSound(soundId)) {
            Console.out.println("Could not load sample" + ((pSFXs[soundId] != null) ? pSFXs[soundId].rawName : soundId), OsdColor.RED);
            return; //can't load resource
        }

        SFX pEffect = pSFXs[soundId];

        if (pEffect.size <= 0) {
            return;
        }

        SAMPLE2D pChannel;
        if (nChannel == -1) {
            pChannel = FindChannel();
        } else {
            pChannel = Channel[nChannel];
        }

        if (pChannel == null) {
            return;
        }

        // if this is a fixed channel with an active voice, kill the sound
        if (pChannel.hVoice != null) {
            sndStopSound(pChannel);
        }

        pChannel.hResource = pEffect.hResource;

        if (nVol < 0) {
            nVol = pEffect.relVol;
        }

        int sampleSize = pEffect.size;
        if (sampleSize > 0) {
            ByteBuffer pRaw = pChannel.hResource;
            pRaw.rewind();

            if (nChannel < 0) {
                loop = false;
            }

            pChannel.hVoice = newSound(pRaw, sndGetSampleRate(pEffect.format), 8, 80 * nVol);
            if (pChannel.hVoice != null) {
                pChannel.loop &= ~1;
                pChannel.hVoice.setListener(pChannel);
                if (loop) {
                    int loopStart = pEffect.loopStart;
                    int loopEnd = ClipLow(sampleSize - 1, 0);
                    pChannel.loop |= 1;

                    pChannel.hVoice.loop((80 * nVol) / 255.0f, loopStart, loopEnd);
                } else {
                    pChannel.hVoice.play((80 * nVol) / 255.0f);
                }
            }
        }
    }

    public static void sndPreloadSounds() {
        Group sounds = game.getCache().getGroup("sounds.rff");
        for (Entry res : sounds.getEntries()) {
            if (res instanceof RffEntry && res.isExtension("sfx")) {
                sndLoadSound(((RffEntry) res).getId());
            }
        }
    }

    public static void sndProcess() {
        if (cfg.isNoSound()) {
            return;
        }

        for (SAMPLE2D sample2D : Channel) {
            if ((sample2D.hVoice == null || !sample2D.hVoice.isPlaying()) && sample2D.hResource != null) {
                sample2D.hResource = null;
            }
        }
    }

    public static void ambProcess() {
        if (cfg.isNoSound() || (!game.isCurrentScreen(gGameScreen) && !game.isCurrentScreen(gDemoScreen))) {
            return;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(kStatAmbient); node != null; node = node.getNext()) {
            int i = node.getIndex();

            Sprite pSprite = boardService.getSprite(i);
            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());

            if (pXSprite != null && pXSprite.getState() != 0 && pSprite.getOwner() != -1) {
                int dx = (pSprite.getX() - gPlayer[gViewIndex].pSprite.getX()) >> 4;
                int dy = (pSprite.getY() - gPlayer[gViewIndex].pSprite.getY()) >> 4;
                int dz = (pSprite.getZ() - gPlayer[gViewIndex].pSprite.getZ()) >> 8;

                int dist = EngineUtils.sqrt(dx * dx + dy * dy + dz * dz);

                int volume = mulscale(pXSprite.getBusy(), pXSprite.getData4(), 16);
                int radius = ClipRange(volume + (-volume * (dist - pXSprite.getData1())) / (pXSprite.getData2() - pXSprite.getData1()), 0, volume);

                ambient[pSprite.getOwner()].volume += radius;
            }
        }

        int nAmbient = 0;
        while (nAmbient < numambients) {
            AMBIENT pAmbient = ambient[nAmbient];
            int volume = pAmbient.volume;
            if (pAmbient.hVoice == null || !pAmbient.hVoice.isPlaying()) {
                if (volume > 0) {
                    pAmbient.hVoice = newSound(pAmbient.pRaw, sndGetSampleRate(pAmbient.format), 8, volume);
                    if (pAmbient.hVoice != null) {
                        pAmbient.hVoice.setListener(pAmbient);
                        pAmbient.hVoice.loop(volume / 255.0f, 0, pAmbient.nSize);
                    }
                }
            } else {
                pAmbient.hVoice.setVolume(volume / 255.0f);
                pAmbient.hVoice.setPriority(volume);
//                if (pAmbient.hVoice != null && pAmbient.hVoice.data == pAmbient.pRaw) {
//                    pAmbient.hVoice.setVolume(volume / 255.0f);
//                    pAmbient.hVoice.setPriority(volume);
//                } else {
//                    pAmbient.hVoice = null;
//                }
            }

            pAmbient.volume = 0;
            nAmbient++;
        }
    }

    public static void ambStopAll() {
        int nAmbient = 0;
        while (nAmbient < numambients) {
            AMBIENT pAmbient = ambient[nAmbient];
            if (pAmbient.hVoice != null) {
                pAmbient.hVoice.stop();
            }

            pAmbient.clear();
            nAmbient++;
        }
        numambients = 0;
    }

    public static void ambPrepare() {
        ambStopAll();
        if (cfg.isNoSound()) {
            return;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(kStatAmbient); node != null; node = node.getNext()) {
            int i = node.getIndex();
            Sprite pSprite = boardService.getSprite(i);
            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
            if (pXSprite != null && pXSprite.getData2() > pXSprite.getData1()) {
                int nAmbient = 0;
                while (nAmbient < numambients && pXSprite.getData3() != ambient[nAmbient].nSoundId) {
                    nAmbient++;
                }

                if (nAmbient != numambients) {
                    pSprite.setOwner((short) nAmbient);
                    continue;
                }

                if (numambients < kMaxAmbients) {
                    int soundId = pXSprite.getData3();

                    if (!sndLoadSound(soundId)) {
                        Console.out.println("Missing sound #" + soundId + " used in ambient sound generator " + nAmbient, OsdColor.RED);
                        continue; //can't load resource
                    }

                    SFX pEffect = pSFXs[soundId];

                    if (pEffect.size <= 0) {
                        continue;
                    }

                    int nSize = pEffect.size;
                    pEffect.hResource.rewind();
                    AMBIENT pAmbient = ambient[nAmbient];
                    pAmbient.nSoundId = soundId;
                    pAmbient.nSize = nSize;
                    pAmbient.pRaw = pEffect.hResource;
                    pAmbient.format = pEffect.format;
                    pSprite.setOwner((short) nAmbient);
                    numambients++;
                }
            }
        }
    }

    public static void sndPlayMenu() {
        String himus;
        Music hisource;
        sndStopMusic();
        if (game.currentDef != null && (himus = game.currentDef.audInfo.getDigitalInfo("mainmenu")) != null) {
            Entry entry = game.getCache().getEntry(himus, true);
            if (!entry.exists()) {
                return;
            }

            int sign = 0;
            try(InputStream is = entry.getInputStream()) {
                sign = StreamUtils.readInt(is);
            } catch (IOException e) {
                return;
            }

            if (sign == 1684558925 && (hisource = newMusic(entry)) != null) { //MThd
                currMusic = hisource;
                currMusic.setLooping(true);
                currMusic.play();
            } else if (cfg.getMusicType() != 0 && (hisource = newMusic(entry)) != null) {
                currMusic = hisource;
                currMusic.setLooping(true);
                currMusic.play();
            }
        }
    }

    public static boolean sndPlayTrack(int nTrack) {
        if (cfg.getMusicType() != 2) {
            return false;
        }

        if (currMusic != null && currMusic.isPlaying() && currTrack == pGameInfo.nTrackNumber && (mUserFlag == UserFlag.UserMap || (mUserFlag != UserFlag.UserMap && currentEpisode.iniFile.equals(currEpisode)))) {
            return true;
        }

        sndStopMusic();
        if (nTrack > 0 && nTrack <= MAXUSERTRACKS && usertrack[nTrack - 1] != null && (currMusic = newMusic(usertrack[nTrack - 1])) != null) {
            currTrack = nTrack;
            currEpisode = currentEpisode.iniFile;
            currMusic.setLooping(true);
            currMusic.play();
            return true;
        }

        if ((currMusic = newMusic(String.format(TRACK, nTrack))) != null) {
            currTrack = nTrack;
            if (mUserFlag != UserFlag.UserMap) {
                currEpisode = currentEpisode.iniFile;
            }
            currMusic.setLooping(true);
            currMusic.play();
            return true;
        }

        return false;
    }

    public static void sndPlaySong(String name) {
        if (name == null || name.isEmpty()) {
            sndStopMusic();
            return;
        }

        if (currMusic != null && currMusic.isPlaying() && currSong != null && currSong.equals(pGameInfo.zLevelSong)) {
            return;
        }

        sndStopMusic();
        Entry entry = game.getCache().getEntry(name + "." + mid, true);

        if (!entry.exists()) {
            return;
        }

        currSong = name;
        currMusic = newMusic(entry);
        if (currMusic != null) {
            currMusic.setLooping(true);
            currMusic.play();
        }
    }

    public static void sndPlayMusic() {
        if (cfg.isMuteMusic()) {
            return;
        }

        if (cfg.getMusicType() == 1 && game.currentDef != null) { //music from def file
            String himus = game.currentDef.audInfo.getDigitalInfo(pGameInfo.zLevelSong);
            if (himus != null) {
                if (currMusic != null && currMusic.isPlaying() && currSong.equals(himus)) {
                    return;
                }

                sndStopMusic();
                if ((currMusic = newMusic(himus)) != null) {
                    currSong = himus;
                    currMusic.setLooping(true);
                    currMusic.play();
                    return;
                }
            }
        }

        if (!sndPlayTrack(pGameInfo.nTrackNumber)) {
            sndPlaySong(pGameInfo.zLevelSong);
        }
    }

    public static void sndStopMusic() {
        // #GDX 31.12.2024 added isPlaying() check
        if (currMusic != null && currMusic.isPlaying()) {
            currMusic.stop();
        }

        currSong = null;
        currTrack = -1;
        currEpisode = null;
        currMusic = null;
    }

    private static ByteBuffer loadSample(String sampleName) {
        ByteBuffer buf = null;
        Entry res = game.getCache().getEntry(sampleName, true);
        if (res.exists()) {
            try(InputStream is = res.getInputStream()) {
                StreamUtils.readBuffer(is, buf = ByteBuffer.allocateDirect((int) res.getSize()));
            } catch (Exception e) {
                buf = ByteBuffer.allocateDirect(1);
            }
        }
        return buf;
    }

    public static void sndStartSample(String sampleName, int nVol, int nChannel) {
        if (cfg.isNoSound()) {
            return;
        }

        if (sampleName.length() == 0) {
            return;
        }
        if (!(nChannel >= -1 && nChannel < kMaxChannels)) {
            throw new AssertException("nChannel >= -1 && nChannel < kMaxChannels");
        }

        SAMPLE2D pChannel;
        if (nChannel == -1) {
            pChannel = FindChannel();
        } else {
            pChannel = Channel[nChannel];
        }

        if (pChannel == null) {
            return;
        }

        // if this is a fixed channel with an active voice, kill the sound
        if (pChannel.hVoice != null) {
            sndStopSound(pChannel);
        }

        if (!sampleName.contains(".")) {
            sampleName = sampleName + ".raw";
        }

        ByteBuffer buf = loadSample(sampleName);
        if (buf == null || buf.capacity() <= 0) {
            System.err.println("Could not load sample " + sampleName);
            return;
        }

        pChannel.hResource = buf;
        pChannel.hVoice = newSound(buf, sndGetSampleRate(1), 8, nVol);
        if (pChannel.hVoice != null) {
            pChannel.loop &= ~1;
            pChannel.hVoice.setListener(pChannel);
            pChannel.hVoice.play(nVol / 255.0f);
        }
    }

    public static void sndStartWAV(String sampleName, int nVol, int nChannel) {
        if (cfg.isNoSound()) {
            return;
        }

        if (sampleName.length() == 0) {
            return;
        }
        if (!(nChannel >= -1 && nChannel < kMaxChannels)) {
            throw new AssertException("nChannel >= -1 && nChannel < kMaxChannels");
        }
        SAMPLE2D pChannel;
        if (nChannel == -1) {
            pChannel = FindChannel();
        } else {
            pChannel = Channel[nChannel];
        }

        if (pChannel == null) {
            return;
        }

        // if this is a fixed channel with an active voice, kill the sound
        if (pChannel.hVoice != null) {
            sndStopSound(pChannel);
        }

        Entry entry = game.getCache().getEntry(sampleName, true);
        if (!entry.exists()) {
            Console.out.println("Could not load wav file: " + sampleName, OsdColor.RED);
            return;
        }

        try(InputStream is = entry.getInputStream()) {
            ByteBuffer pRaw = BufferUtils.newByteBuffer((int) entry.getSize());
            StreamUtils.readBuffer(is, pRaw);
            pChannel.hVoice = newSound(pRaw, sndGetSampleRate(8), 8, nVol);
            if (pChannel.hVoice != null) {
                pChannel.loop &= ~1;
                pChannel.hVoice.setListener(pChannel);
                pChannel.hVoice.play(nVol / 255.0f);
            }
        } catch (IOException ignored) {
        }
    }

    public static Source newSound(ByteBuffer buffer, int rate, int bits, int priority) {
        return (Source) audio.newSound(buffer, rate, bits, priority);
    }

    public static Source newSound(ByteBuffer buffer, int rate, int bits, int channel, int priority) {
        // channel: mono / stereo
        return (Source) audio.newSound(buffer, rate, bits, AudioChannel.parseChannel(channel), priority);
    }

    public static Music newMusic(Entry entry) {
        return audio.newMusic(entry);
    }

    public static Music newMusic(String filePath) {
        Entry entry = game.cache.getEntry(filePath, true);
        return audio.newMusic(entry);
    }

    public static void stopAllSounds() {
        audio.stopAllSounds();
    }

    public static void setReverb(boolean enable, float delay) {
        audio.setReverb(enable, delay);
    }

}

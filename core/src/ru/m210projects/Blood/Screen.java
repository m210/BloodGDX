// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.Types.ScreenEffect.*;
import static ru.m210projects.Blood.View.*;

public class Screen {

    public static void scrSetView(int mode) {
        switch (mode) {
            case kView2D:
                if (cfg.gOverlayMap == 1) {
                    gViewMode = kView3D;
                } else {
                    gViewMode = kView2DIcon;
                }
                break;
            case kView3D:
                if (cfg.gOverlayMap != 0) {
                    gViewMode = kView2D;
                } else {
                    gViewMode = kView2DIcon;
                }
                break;
            default:
                gViewMode = kView3D;
                viewResizeView(cfg.gViewSize);
                break;
        }
    }

    public static void scrReset() {
        gMe.pickupEffect = 0;
        gMe.hitEffect = 0;
        gMe.blindEffect = 0;
        gMe.drownEffect = 0;
        resetDacEffects();
    }
}

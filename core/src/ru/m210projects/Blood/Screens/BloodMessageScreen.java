package ru.m210projects.Blood.Screens;

import ru.m210projects.Build.Architecture.MessageType;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.MessageScreen;
import ru.m210projects.Build.Render.Renderer;

import static ru.m210projects.Blood.Globals.kPalNormal;
import static ru.m210projects.Blood.Main.engine;
import static ru.m210projects.Blood.Screen.scrReset;
import static ru.m210projects.Blood.View.viewShowLoadingTile;

public class BloodMessageScreen extends MessageScreen {

    public BloodMessageScreen(BuildGame game, String header, String message, MessageType type) {
        super(game, header, message, game.getFont(1), game.getFont(3), type);
    }

    @Override
    public void show() {
        super.show();

        engine.getPaletteManager().setPalette(kPalNormal);
        scrReset();
    }

    @Override
    public void drawBackground(Renderer renderer) {
        viewShowLoadingTile();
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2020  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Screens;

import ru.m210projects.Blood.Factory.BloodEngine;
import ru.m210projects.Blood.Main;
import ru.m210projects.Blood.SOUND;
import ru.m210projects.Build.Architecture.common.audio.SoundData;
import ru.m210projects.Build.Architecture.common.audio.Source;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.MovieScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.BuildSmacker.SMKFile;
import ru.m210projects.BuildSmacker.SMKFile.Track;

import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static ru.m210projects.Blood.Globals.kAngleMask;
import static ru.m210projects.Blood.Main.cfg;
import static ru.m210projects.Blood.SOUND.sndStopAllSounds;
import static ru.m210projects.Blood.Strings.cutskip;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Build.Engine.MAXTILES;
import static ru.m210projects.Build.Pragmas.mulscale;

public class CutsceneScreen extends MovieScreen {

    private final BloodEngine engine;

    public CutsceneScreen(BuildGame game) {
        super(game, MAXTILES - 3);
        this.engine = Main.engine;

        this.nFlags |= 4;
    }

    public boolean init(String path, String sndPath) {
        if (!cfg.showCutscenes || isInited()) {
            return false;
        }

        if (!open(path)) {
            return false;
        }

        ((SMKMovieFile) mvfil).setSoundPath(sndPath);
        return true;
    }

    @Override
    protected MovieFile GetFile(String file) {
        try {
            return new SMKMovieFile(file);
        } catch (FileNotFoundException fnf) {
            Console.out.println(file + " is not found!", OsdColor.RED);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void StopAllSounds() {
        sndStopAllSounds();
    }

    @Override
    protected byte[] DoDrawFrame(int num) {
        byte[] pic = mvfil.getFrame(num);
        if (((SMKMovieFile) mvfil).paletteChanged) {
            changepalette(mvfil.getPalette());
            engine.getPaletteManager().invalidate(); // reset palette index
        }
        return pic;
    }

    @Override
    protected Font GetFont() {
        return game.getFont(4);
    }

    @Override
    protected void DrawEscText(Font font, int pal) {
        Renderer renderer = game.getRenderer();
        int shade = 32 + mulscale(32, Sin((20 * engine.getTotalClock()) & kAngleMask), 30);
        font.drawTextScaled(renderer, 160, 5, cutskip, 1.0f, shade, pal, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
    }

    protected static class SMKMovieFile implements MovieFile {
        protected SMKFile smkfil;
        protected boolean paletteChanged;
        protected String sndPath;
        private Source smkSource;

        public SMKMovieFile(String file) throws Exception {
            Entry entry = Main.game.getCache().getEntry(file, true);
            if (!entry.exists()) {
                throw new FileNotFoundException();
            }

            byte[] smkbuf = entry.getBytes();
            ByteBuffer bb = ByteBuffer.wrap(smkbuf);
            bb.order(ByteOrder.LITTLE_ENDIAN);

            smkfil = new SMKFile(bb);
            smkfil.setEnable(Track.All, Track.Video.mask());
        }

        @Override
        public int getFrames() {
            return smkfil.getFrames();
        }

        @Override
        public float getRate() {
            return smkfil.getRate() / 1000f;
        }

        @Override
        public byte[] getFrame(int num) {
            paletteChanged = smkfil.setFrame(num) != 0;
            return smkfil.getVideoBuffer().array();
        }

        @Override
        public byte[] getPalette() {
            return smkfil.getPalette();
        }

        @Override
        public int getWidth() {
            return (short) smkfil.getWidth();
        }

        @Override
        public int getHeight() {
            return (short) smkfil.getHeight();
        }

        @Override
        public void close() {
            if (smkSource != null) {
                smkSource.stop();
            }
            smkSource = null;
        }

        @Override
        public void playAudio() {
            smkStartWAV(sndPath);
        }

        public void setSoundPath(String sndPath) {
            this.sndPath = sndPath;
        }

        private void smkStartWAV(String sampleName) {
            if (cfg.isNoSound() || sampleName == null || sampleName.isEmpty()) {
                return;
            }

            Entry entry = Main.game.getCache().getEntry(sampleName, true);
            if (!entry.exists()) {
                Console.out.println("Could not load wav file: " + sampleName, OsdColor.RED);
                return;
            }

            byte[] buf = entry.getBytes();
            int sampleSize = buf.length;
            if (sampleSize == 0) {
                return;
            }

            try {
                SoundData wavData = SOUND.getSoundDecoder("WAV").decode(entry);
                if (wavData != null) {
                    smkSource = SOUND.newSound(wavData.getData(), wavData.getRate(), wavData.getBits(), wavData.getChannels(), 255);
                    if (smkSource != null) {
                        smkSource.play(1.0f);
                    }
                }
            } catch (Exception e) {
                Console.out.println(e + " in " + sampleName, OsdColor.RED);
            }
        }
    }
}

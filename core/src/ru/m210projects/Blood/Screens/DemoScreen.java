// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Screens;

import ru.m210projects.Blood.Main;
import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Blood.Types.INPUT;
import ru.m210projects.Blood.filehandlers.DemoFile;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.settings.GameKeys;

import java.io.InputStream;
import java.util.List;

import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.currentEpisode;
import static ru.m210projects.Blood.LOADSAVE.lastload;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.Types.DemoUtils.*;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;

public class DemoScreen extends GameScreen {

    protected Entry lastDemoFile = DUMMY_ENTRY;

    public DemoScreen(Main game) {
        super(game);
    }

    @Override
    public void show() {
        lastload = null;
        resetQuotes();
    }

    public boolean showDemo(Entry res, BloodIniFile ini) {
        String name = res.getName();
        if (res.exists()) {
            if (!res.isExtension("dem")) {
                Console.out.println("Wrong file format: " + name, OsdColor.RED);
                return false;
            }

            gInfiniteAmmo = false;
            gFullMap = false;
            gNoClip = false;
            kFakeMultiplayer = false;
            pGameInfo.copy(defGameInfo);
            if (numplayers > 1) game.pNet.NetDisconnect(myconnectindex);

            try (InputStream is = res.getInputStream()) {
                demfile = new DemoFile(is);
            } catch (Exception e) {
                Console.out.println("Can't play the demo file: " + name, OsdColor.RED);
                return false;
            }

//            CompareService.prepare(FileUtils.getPath("D:\\DemosData\\Blood\\" + name + ".jdm"), CompareService.Type.Read);

            if (demfile.nInputCount == 0) {
                Console.out.println("Can't play the demo file: " + name, OsdColor.RED);
                return false;
            }

            gViewIndex = demfile.nMyConnectIndex;
            connecthead = demfile.nConnectHead;
            System.arraycopy(demfile.connectPoints, 0, connectpoint2, 0, kMaxPlayers);

            lastDemoFile = res;
            gDemoScreen.newgame(false, ini, pGameInfo.nEpisode, pGameInfo.nLevel, pGameInfo.nDifficulty, pGameInfo.nDifficulty, pGameInfo.nDifficulty, false);

            Console.out.println("Playing demo " + name);

            return true;
        }

        Console.out.println("Can't play the demo file: " + name, OsdColor.RED);
        return false;
    }

    @Override
    protected void startboard(final Runnable startboard) {
        game.doPrecache(() -> {
            startboard.run(); //call faketimehandler
            pNet.ResetTimers(); //reset ototalclock
            pNet.ready2send = false;
        });
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        if (gameKeyDownCommon(gameKey, false)) {
            return true;
        }

        if (GameKeys.Shrink_Screen.equals(gameKey)) {
            viewResizeView(cfg.gViewSize + 1);
        }

        if (GameKeys.Enlarge_Screen.equals(gameKey)) {
            viewResizeView(cfg.gViewSize - 1);
        }

        return false;
    }

    @Override
    public void render(float delta) {
//        if (numplayers > 1) {
//            pEngine.faketimerhandler();
//        }

        DemoRender();

        float smoothratio = 65536;
        if (!game.gPaused) {
            smoothratio = pEngine.getTimer().getsmoothratio(delta);
            if (smoothratio < 0 || smoothratio > 0x10000) {
                smoothratio = BClipRange(smoothratio, 0, 0x10000);
            }
        }

        game.pInt.dointerpolations(smoothratio);
        DrawWorld(smoothratio); //smooth sprites

        DrawHud(smoothratio);
        game.pInt.restoreinterpolations();

        if (pMenu.gShowMenu) pMenu.mDrawMenu();

        PostFrame(pNet);

        pEngine.nextpage(delta);
    }

    private void DemoRender() {
        pNet.ready2send = false;

        if (!game.isCurrentScreen(this)) return;

        while (!game.gPaused && engine.getTotalClock() >= pNet.ototalclock) {
//            CompareService.update();
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                INPUT pInput = demfile.pDemoInput[demfile.rcnt][i];
                if (pInput != null) {
                    pNet.gFifoInput[pNet.gNetFifoHead[i] & 0xFF][i].Copy(pInput);
                    pNet.gNetFifoHead[i]++;
                }

                if (++demfile.rcnt >= demfile.nInputCount) {
//                     CompareService.update();
                    demfile = null;
                    Group group = lastDemoFile.getParent();
                    if (!showDemo(group)) {
                        game.changeScreen(gMenuScreen);
                    }
                    return;
                }
            }

            game.pInt.clearinterpolations();
            pNet.ototalclock += game.pEngine.getTimer().getFrameTicks();

            ProcessFrame(pNet);
        }
    }

    public boolean showDemo(Group group) {
        List<Entry> list = checkDemoEntry(group);
        switch (cfg.gDemoSeq) {
            case 0: //OFF
                return false;
            case 1: //Consistently
                if (nDemonum < (list.size() - 1)) nDemonum++;
                else {
                    nDemonum = 0;
                    // throw new RuntimeException("Done");
                }
                break;
            case 2: //Accidentally
                int nextnum = nDemonum;
                if (list.size() > 1) {
                    while (nextnum == nDemonum) {
                        nextnum = (int) (Math.random() * (list.size()));
                    }
                }
                nDemonum = BClipRange(nextnum, 0, list.size() - 1); // #GDX 28.12.2024
                break;
        }

        if (!list.isEmpty()) {
            boolean result = showDemo(list.get(nDemonum), currentEpisode != null ? currentEpisode.iniFile : null);
            if (!result) {
                list.remove(nDemonum);
                return showDemo(group);
            }

            return true;
        }

        return false;
    }


    public boolean isRecordEnabled() {
        return false;
    }

    public void onPrepareboard(GameScreen screen) {
        if (screen != this && isDemoPlaying()) {
            gDemoScreen.onStopPlaying();
        }
    }

    public boolean isDemoRecording() {
        return false;
    }

    public static boolean isDemoPlaying() {
        return demfile != null;
    }

    public void onLoad() {

    }

    public void onStopPlaying() {
        demfile = null; // reset demo flag
    }

    public void onRecord() {

    }

    public void onStopRecord() {

    }


//    @Override
//    public void resume() {
//        if (game.nNetMode == NetMode.Single && numplayers < 2) {
//            game.gPaused = false;
//            sndHandlePause(game.gPaused);
//        }
//    }
}

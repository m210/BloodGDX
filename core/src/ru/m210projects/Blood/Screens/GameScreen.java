// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Screens;

import ru.m210projects.Blood.Config.BloodKeys;
import ru.m210projects.Blood.*;
import ru.m210projects.Blood.Factory.BloodMenuHandler;
import ru.m210projects.Blood.Factory.BloodRenderer;
import ru.m210projects.Blood.Menus.MenuInterfaceSet;
import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Blood.Types.INPUT;
import ru.m210projects.Blood.Types.MapInfo;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Pattern.ScreenAdapters.GameAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Renderer.RenderType;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.fs.AbsoluteFileEntry;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.settings.GameKeys;

import java.io.IOException;
import java.nio.file.Path;

import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.Cheats.cheatSubInventory;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.evProcess;
import static ru.m210projects.Blood.Factory.BloodMenuHandler.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.*;
import static ru.m210projects.Blood.LOADSAVE.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.*;
import static ru.m210projects.Blood.QAV.*;
import static ru.m210projects.Blood.ResourceHandler.checkEpisodeResources;
import static ru.m210projects.Blood.ResourceHandler.resetEpisodeResources;
import static ru.m210projects.Blood.SECTORFX.DoSectorPanning;
import static ru.m210projects.Blood.SOUND.*;
import static ru.m210projects.Blood.Strings.*;
import static ru.m210projects.Blood.Trigger.trProcessBusy;
import static ru.m210projects.Blood.Types.DemoUtils.demfile;
import static ru.m210projects.Blood.Types.GAMEINFO.*;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqKillAll;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.seqProcess;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Blood.Weapon.WeaponDraw;
import static ru.m210projects.Build.Gameutils.BClampAngle;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.mulscale;

public class GameScreen extends GameAdapter {

    private final Main game;
    public int gNameShowTime;
    private int nonsharedtimer;

    public GameScreen(Main game) {
        super(game, Main.gLoadingScreen);
        this.game = game;
    }

    @Override
    public void ProcessFrame(BuildNet net) {
        nonsharedtimer = engine.getTotalClock();
        for (short i = connecthead; i >= 0; i = connectpoint2[i]) {
            INPUT src = (INPUT) net.gFifoInput[net.gNetFifoTail & 0xFF][i];
            gPlayer[i].pInput.syncFlags = src.syncFlags;
            gPlayer[i].pInput.Forward = src.Forward;
            gPlayer[i].pInput.Turn = src.Turn;
            gPlayer[i].pInput.Strafe = src.Strafe;
            gPlayer[i].pInput.mlook = src.mlook;
            gPlayer[i].pInput.Run = src.Run;
            gPlayer[i].pInput.Jump = src.Jump;
            gPlayer[i].pInput.Crouch = src.Crouch;
            gPlayer[i].pInput.Shoot = src.Shoot;
            gPlayer[i].pInput.AltShoot = src.AltShoot;
            gPlayer[i].pInput.Lookup = src.Lookup;
            gPlayer[i].pInput.Lookdown = src.Lookdown;
            gPlayer[i].pInput.TurnAround = src.TurnAround;
            gPlayer[i].pInput.Use |= src.Use;
            gPlayer[i].pInput.InventoryLeft |= src.InventoryLeft;
            gPlayer[i].pInput.InventoryRight |= src.InventoryRight;
            gPlayer[i].pInput.InventoryUse |= src.InventoryUse;
            gPlayer[i].pInput.PrevWeapon |= src.PrevWeapon;
            gPlayer[i].pInput.NextWeapon |= src.NextWeapon;
            gPlayer[i].pInput.HolsterWeapon |= src.HolsterWeapon;
            gPlayer[i].pInput.LookCenter |= src.LookCenter;
            gPlayer[i].pInput.LookLeft = src.LookLeft;
            gPlayer[i].pInput.LookRight = src.LookRight;
            gPlayer[i].pInput.Pause |= src.Pause;
            gPlayer[i].pInput.Quit |= src.Quit;
            gPlayer[i].pInput.Restart |= src.Restart;
            gPlayer[i].pInput.CrouchMode = src.CrouchMode;
            gPlayer[i].pInput.LastWeapon = src.LastWeapon;
            gPlayer[i].pInput.UseBeastVision |= src.UseBeastVision;
            gPlayer[i].pInput.UseCrystalBall |= src.UseCrystalBall;
            gPlayer[i].pInput.UseJumpBoots |= src.UseJumpBoots;
            gPlayer[i].pInput.UseMedKit |= src.UseMedKit;
            if (src.newWeapon != 0) {
                gPlayer[i].pInput.newWeapon = src.newWeapon;
            }
        }
        net.gNetFifoTail++;
        net.CalcChecksum();

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            if (gPlayer[i].pInput.Quit) {
                gPlayer[i].pInput.Quit = false;
                game.pNet.NetDisconnect(i);
                return;
            }

            if (gPlayer[i].pInput.Restart) {
                gPlayer[i].pInput.Restart = false;
                gGameScreen.loadboard(pGameInfo.zLevelName, null);
                return;
            }

            if (gPlayer[i].pInput.Pause) {
                game.gPaused = !game.gPaused;
                sndHandlePause(game.gPaused);
                if (game.gPaused && pGameInfo.nGameType > kNetModeOff && numplayers > 1) {
                    viewSetMessage(game.net.gProfile[i].name + " paused the game", -1);
                }
                gPlayer[i].pInput.Pause = false;
            }
        }

        if (game.gPaused || (!game.isCurrentScreen(gGameScreen) && !game.isCurrentScreen(gDemoScreen))) {
            return;
        }

        if (game.isCurrentScreen(gGameScreen) && (pGameInfo.nGameType == kNetModeOff && (game.menu.gShowMenu || Console.out.isShowing()))) {
            return;
        }

        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
//			if(pGameInfo.nGameType != kNetModeOff) //GDX 13/12/2018 Will make lags in multiplayer
//				viewUpdatePlayerLoc(gPlayer[i]);
//			else
            viewBackupView(i);
            playerMove(gPlayer[i]); // Move player
        }

        trProcessBusy(); // process busy triggers
        evProcess(gFrameClock); // process event queue
        seqProcess(kFrameTicks);
        DoSectorPanning();
        actProcessSprites(); // Actor code: projectiles, etc.
        actPostProcess(); // clean up deleted sprites or sprites changing status
        net.CorrectPrediction();
        sndProcess();
        ambProcess();
        sfxUpdate3DSounds();

        gFrame++;
        gFrameClock += kFrameTicks;

        if ((pGameInfo.uGameFlags & EndOfLevel) != 0) {
            game.pNet.ready2send = false;
            seqKillAll();
            if (game.isCurrentScreen(gDemoScreen)) {
                return;
            }

            if ((pGameInfo.uGameFlags & EndOfGame) != 0) {
                if (pGameInfo.nGameType == kNetModeOff) {
                    Runnable rEndGame = () -> {
                        game.resetState();
                        game.changeScreen(gMenuScreen);
                        game.menu.mOpen(game.menu.mMenus[CREDITS], -1);
                    };
                    if (checkCutsceneB()) {
                        game.changeScreen(gCutsceneScreen.setCallback(rEndGame).escSkipping(true));
                    } else {
                        rEndGame.run();
                    }
                } else {
                    game.Disconnect();
                }

                pGameInfo.uGameFlags &= ~(EndOfLevel | EndOfGame);
            } else {
                game.changeScreen(gStatisticScreen);
            }
        }
    }

    @Override
    public void DrawWorld(float smooth) {
        viewDrawScreen(gViewIndex, (int) smooth);
    }

    @Override
    public void DrawHud(float smooth) {
        PLAYER gView = gPlayer[gViewIndex];
        if (gView == null || gView.pSprite == null) {
            return;
        }
        BloodRenderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        if ((gViewMode == kView3D) || gViewMode == kView2D) {
            short nSector = gView.pSprite.getSectnum();

            if (gViewPos == kViewPosCenter) {
                ArtEntry pic = engine.getTile(2319);
                if ((!game.menu.gShowMenu || (game.menu.getCurrentMenu() instanceof MenuInterfaceSet)) && cfg.gCrosshair) {
                    renderer.rotatesprite((int) ((viewCrossX - (pic.getWidth() * 320 / xdim)) * 65536.0f), (int) ((viewCrossY + (pic.getHeight() * 240 / ydim)) * 65536.0f), viewCrossZoom, 0, 2319, 0, 0, 8);
                }

                int nShade = 0;
                int nPLU = 0;
                if (nSector != -1) {
                    nShade = boardService.getSector(nSector).getFloorshade();
                    if (boardService.getSector(nSector).getExtra() > 0 && xsector[boardService.getSector(nSector).getExtra()].color) {
                        nPLU = boardService.getSector(nSector).getFloorpal();
                    }
                }

                int nZoom = 65536;
                if (SplitScreen) {
                    nZoom = 32768;
                }

                WeaponDraw(gView, nShade, viewWeaponX, viewWeaponY, nPLU, nZoom);
                if (gView.pXsprite.getBurnTime() > 60) {
                    viewDrawBurn(gView.pXsprite.getBurnTime());
                }
            }

            if (inventoryCheck(gView, kInventoryDivingSuit)) {
                renderer.rotatesprite(0, 0, 65536, 0, 2344, 0, 0, kQFrameScale | kQFrameCorner | kQFrameUnclipped | 256, gViewX0, gViewY0, gViewX1, gViewY1);
                renderer.rotatesprite(320 << 16, 0, 65536, 1024, 2344, 0, 0, kQFrameScale | kQFrameCorner | kQFrameUnclipped | kQFrameYFlip | 512, gViewX0, gViewY0, gViewX1, gViewY1);
                renderer.rotatesprite(0, 200 << 16, 65536, 0, 2344, 0, 0, kQFrameScale | kQFrameCorner | kQFrameUnclipped | kQFrameYFlip | 256, gViewX0, gViewY0, gViewX1, gViewY1);
                renderer.rotatesprite(320 << 16, 200 << 16, 65536, 1024, 2344, 0, 0, kQFrameScale | kQFrameCorner | kQFrameUnclipped | 512, gViewX0, gViewY0, gViewX1, gViewY1);
                if (cfg.gDetail >= 4) {
                    renderer.rotatesprite(15 << 16, 3 << 16, 65536, 0, 2346, 32, 0, kQFrameScale | kQFrameCorner | kQFrameUnclipped | kQFrameTranslucent | 256, gViewX0, gViewY0, gViewX1, gViewY1);
                    renderer.rotatesprite(212 << 16, 77 << 16, 65536, 0, 2347, 32, 0, kQFrameScale | kQFrameCorner | kQFrameUnclipped | kQFrameTranslucent | 512, gViewX0, gViewY0, gViewX1, gViewY1);
                }
            }

            if (powerupCheck(gView, kItemAsbestosArmor - kItemBase) > 0) {
                renderer.rotatesprite(0, 200 << 16, 65536, 0, 2358, 0, 0, kQFrameScale | kQFrameCorner | kQFrameUnclipped | kQFrameYFlip | 256, gViewX0, gViewY0, gViewX1, gViewY1);
                renderer.rotatesprite(320 << 16, 200 << 16, 65536, 1024, 2358, 0, 0, kQFrameScale | kQFrameCorner | kQFrameUnclipped | 512, gViewX0, gViewY0, gViewX1, gViewY1);
            }

            if ((powerupCheck(gView, kItemCrystalBall - kItemBase) > 0) && (numplayers > 1 || (game.isCurrentScreen(gGameScreen) && kFakeMultiplayer && nFakePlayers > 1))) {
                // render screen with lens effect to BALLBUFFER2
                DoLensEffect((DynamicArtEntry) engine.getTile(BALLBUFFER2));

                renderer.setaspect(0x10000, 0xD555);

                int crysX = 280;
                int crysY = 35;

                byte nLensPLU = kPLUNormal;
                if (nSector != -1 && IsUnderwaterSector(nSector)) {
                    nLensPLU = kPLUCold;
                }

                // draw the warped image
                renderer.rotatesprite(crysX << 16, crysY << 16, 0xD000, (renderer.getType() == RenderType.Software) ? kAngle90 : -kAngle180, BALLBUFFER2, 0, nLensPLU, kRotateYFlip | kRotateScale, gViewX0, gViewY0, gViewX1, gViewY1);

                // draw frescette and highlight
                renderer.rotatesprite((crysX - 1) << 16, (crysY - 1) << 16, 0xD000, 0, 1683, 0, kPLUNormal, kRotateTranslucent | kRotateTranslucentR | kRotateScale, gViewX0, gViewY0, gViewX1, gViewY1);

                renderer.setview(gViewX0, gViewY0, gViewX1, gViewY1);
                // engine.setaspect(0x10000, (int) divscale(ydim * 320, xdim * 200, 16));
            }
        }

        if (gViewMode == kView2DIcon || gViewMode == kView2D) {
            if (gViewMode == kView2DIcon) {
                renderer.clearview(0);
            }
            updateviewmap();

            int x = gView.pSprite.getX();
            int y = gView.pSprite.getY();
            float ang = gView.ang;

            char[] mode;
            if (gMapScrollMode) {
                x = scrollOX + mulscale(scrollX - scrollOX, smoothratio, 16);
                y = scrollOY + mulscale(scrollY - scrollOY, smoothratio, 16);
                ang = (scrollOAng + ((BClampAngle(scrollAng - scrollOAng + kAngle180) - kAngle180) * smoothratio) / 65536.0f);
                mode = scrollmode;
            } else {

                x = gPrevView[gViewIndex].x + mulscale(x - gPrevView[gViewIndex].x, smoothratio, 16);
                y = gPrevView[gViewIndex].y + mulscale(y - gPrevView[gViewIndex].y, smoothratio, 16);
                ang = (gPrevView[gViewIndex].ang + ((BClampAngle(ang - gPrevView[gViewIndex].ang + kAngle180) - kAngle180) * smoothratio) / 65536.0f);

                mode = followmode;
            }

            int oldSize = cfg.gViewSize;
            viewResizeView(0);
            if (gViewMode == kView2DIcon) {
                renderer.drawmapview(x, y, kMapZoom >> 1, (short) ang);
            }
            renderer.drawoverheadmap(x, y, kMapZoom >> 1, (short) ang);

            int tx = 315;
            int ty = 16;
            String mapname = boardfilename;
            if (mUserFlag == UserFlag.UserMap && gUserMapInfo.Title != null) {
                mapname = gUserMapInfo.Title;
            }

            String info = "E" + (pGameInfo.nEpisode + 1) + "M" + (pGameInfo.nLevel + 1) + ": " + mapname;
            game.getFont(3).drawTextScaled(renderer, tx, ty, info, 1.0f, 0, 0, TextAlign.Right, Transparent.None, ConvertType.AlignRight, false);
            game.getFont(3).drawTextScaled(renderer, tx, ty + 10, mode, 1.0f, 0, 0, TextAlign.Right, Transparent.None, ConvertType.AlignRight, false);
            viewResizeView(oldSize);
        }

        if (!game.menu.gShowMenu || (game.menu.getCurrentMenu() instanceof MenuInterfaceSet)) {
            if (!SplitScreen) {
                viewDrawHUD(gView);
            } else {
                viewDrawSplitHUD(gView);
            }
        }

        if (game.getProcessor().getBloodMessage().isCaptured()) {
            game.getProcessor().getBloodMessage().draw();
        }

        viewDisplayMessage(0); // gViewIndex

        if (gPlayerIndex != -1 && gPlayerIndex != gViewIndex) {
            PLAYER pPlayer = gPlayer[gPlayerIndex];
            int plu = pPlayer.pSprite.getPal();
            if (plu == 13) {
                plu = 4;
            }
            game.getFont(4).drawTextScaled(renderer, 160, 120, toCharArray(game.net.gProfile[gPlayerIndex].name), 1.0f, 0, plu, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            if (pGameInfo.nReviveMode && pGameInfo.nGameType == kNetModeCoop && pPlayer.deathTime > 0 && pPlayer.pXsprite.getHealth() <= 0) {
                int hitDist = EngineUtils.qdist(gView.pSprite.getX() - pPlayer.pSprite.getX(), gView.pSprite.getY() - pPlayer.pSprite.getY()) >> 4;
                if (hitDist < kPushXYDist) {
                    int shade = 32 - (engine.getTotalClock() & 0x3F);
                    game.getFont(4).drawTextScaled(renderer, 160, 130, toCharArray("Press \"USE\" to revive player"), 1.0f, 0, shade, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                }
            }
        }

        if (pGameInfo.nReviveMode && pGameInfo.nGameType == kNetModeCoop && gView.deathTime > 0 && gView.pXsprite.getHealth() <= 0) {
            int shade = 32 - (engine.getTotalClock() & 0x3F);
            game.getFont(4).drawTextScaled(renderer, 160, 130, toCharArray("Wait for revive or press \"USE\" to respawn"), 1.0f, 0, shade, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        if (game.gPaused) {
            game.getFont(1).drawTextScaled(renderer, 160, 10, paused, 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        if (game.isCurrentScreen(gGameScreen) && gFrameClock < gNameShowTime) {
            Transparent transp = Transparent.None;
            if (engine.getTotalClock() > gNameShowTime - 20) {
                transp = Transparent.Bit2;
            }
            if (engine.getTotalClock() > gNameShowTime - 10) {
                transp = Transparent.Bit1;
            }

            if (/* mUserFlag != UserFlag.UserMap && */cfg.showMapInfo != 0 && !game.menu.gShowMenu) {
                switch (cfg.showMapInfo) {
                    case 1:
                        if (boardfilename != null) {
                            game.getFont(1).drawTextScaled(renderer, 160, 60, boardfilename, 1.0f, -128, 0, TextAlign.Center, transp, ConvertType.Normal, true);
                        }
                        break;
                    case 2:
                        if (boardfilename != null) {
                            game.getFont(3).drawTextScaled(renderer, 160, 20, boardfilename, 1.0f, 0, 0, TextAlign.Center, transp, ConvertType.Normal, false);
                        }
                        if (currentEpisode != null && currentEpisode.gMapInfo[pGameInfo.nLevel] != null && currentEpisode.gMapInfo[pGameInfo.nLevel].Author != null) {
                            game.getFont(3).drawTextScaled(renderer, 160, 30, "by " + currentEpisode.gMapInfo[pGameInfo.nLevel].Author, 1.0f, 0, 0, TextAlign.Center, transp, ConvertType.Normal, false);
                        }
                        break;
                }
            }
        }

//		if (fInterpolateRangeError)
//			viewDrawText(0, InterpolateRangeError, 160, 20, 65536, 0, 0, 1, 0, false);

        if (game.net.bOutOfSync) {
            game.getFont(3).drawTextScaled(renderer, 160, 20, "Out of sync!", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

            switch (game.net.bOutOfSyncByte / 4) {
                case 0: // bseed
                    game.getFont(3).drawTextScaled(renderer, 160, 30, "bseed checksum error", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                    break;
                case 1: // player
                    game.getFont(3).drawTextScaled(renderer, 160, 30, "player struct checksum error", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                    break;
                case 2: // sprite
                    game.getFont(3).drawTextScaled(renderer, 160, 30, "player sprite checksum error", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                    break;
                case 3: // xsprite
                    game.getFont(3).drawTextScaled(renderer, 160, 30, "player xsprite checksum error", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                    break;
            }
        }

        if (gView.pSprite.getStatnum() == kStatDude && gView.handDamage) {
            int x = 160;
            int y = ((gView.weaponAboveZ - gView.viewOffZ - 3072) >> 7) + 220;

            if (viewHandAnim.pQAV != null) {
                int oldFrameClock = gFrameClock;
                gFrameClock = engine.getTotalClock();

                QAV pQAV = viewHandAnim.pQAV;
                pQAV.origin.x = x;
                pQAV.origin.y = y;

                int ticks = engine.getTotalClock() - viewHandAnim.clock;
                viewHandAnim.clock = engine.getTotalClock();
                viewHandAnim.duration -= ticks;
                if (viewHandAnim.duration <= 0 || viewHandAnim.duration > pQAV.duration) {
                    viewHandAnim.duration = pQAV.duration;
                }

                int t = pQAV.duration - viewHandAnim.duration;
                pQAV.Play(t - ticks, t, -1, null);

//                int oldwx1 = windowx1;
//                int oldwy1 = windowy1;
//                int oldwx2 = windowx2;
//                int oldwy2 = windowy2;
//                windowy1 = 0;
//                windowx2 = xdim - 1;
//                windowx1 = 0;
//                windowy2 = ydim - 1;
                pQAV.Draw(t, 0, (kQFrameUnclipped | kQFrameScale), 0, 65536);
//                windowx1 = oldwx1;
//                windowy1 = oldwy1;
//                windowx2 = oldwx2;
//                windowy2 = oldwy2;
                gFrameClock = oldFrameClock;
            }
        }
    }

    @Override
    public void PostFrame(BuildNet net) {
        if (gQuickSaving) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(), "[quicksave_" + quickslot + "]", "quicksav" + quickslot + ".sav");
                quickslot ^= 1;
                gQuickSaving = false;
            } else {
                gGameScreen.capture(320, 200);
            }
        }

        if (gAutosaveRequest) {
            if (captBuffer != null) {
                savegame(game.getUserDirectory(), "[autosave]", "autosave.sav");
                gAutosaveRequest = false;
            } else {
                gGameScreen.capture(320, 200);
            }
        }

        fire.process();

        viewPaletteHandler(gPlayer[gViewIndex]);
    }

    protected boolean gameKeyDownCommon(GameKey gameKey, boolean inGame) {
        // the super has console button handling
        if (super.gameKeyDown(gameKey)) {
            return true;
        }

        BloodMenuHandler menu = game.menu;
        if (GameKeys.Menu_Toggle.equals(gameKey)) {
            if (inGame) {
                menu.mOpen(menu.mMenus[GAME], -1);
            } else {
                menu.mOpen(menu.mMenus[MAIN], -1);
            }
            return true;
        }

        if (BloodKeys.Show_LoadMenu.equals(gameKey)) {
            if (numplayers > 1 /*|| mFakeMultiplayer*/) {
                return false;
            }
            menu.mOpen(menu.mMenus[LOADGAME], -1);
            return true;
        }

        if (BloodKeys.Show_SoundSetup.equals(gameKey)) {
            menu.mOpen(menu.mMenus[SOUNDSET], -1);
            return true;
        }

        if (BloodKeys.Show_Options.equals(gameKey)) {
            menu.mOpen(menu.mMenus[OPTIONS], -1);
            return true;
        }

        if (BloodKeys.Toggle_messages.equals(gameKey)) {
            cfg.MessageState = !cfg.MessageState;
            if (cfg.MessageState) {
                viewSetMessage("Messages on", gViewIndex, 2);
            }
            return true;
        }

        if (BloodKeys.Quit.equals(gameKey)) {
            menu.mOpen(menu.mMenus[QUIT], -1);
            return true;
        }

        if (BloodKeys.Gamma.equals(gameKey)) {
            openGamma(menu);
            return true;
        }

        if (BloodKeys.Make_Screenshot.equals(gameKey)) {
            makeScreenshot();
            return true;
        }

        return false;
    }

    @Override
    public void processInput(GameProcessor processor) {
        if (gViewMode != kView3D) {
            int j = engine.getTotalClock() - nonsharedtimer;
            nonsharedtimer += j;

            if (processor.isGameKeyPressed(GameKeys.Shrink_Screen)) {
                kMapZoom = ClipLow(kMapZoom - mulscale(j, Math.max(kMapZoom, 256), 6), 16);
            }
            if (processor.isGameKeyPressed(GameKeys.Enlarge_Screen)) {
                kMapZoom = ClipHigh(kMapZoom + mulscale(j, Math.max(kMapZoom, 256), 6), 4096);
            }
        }
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        if (gameKeyDownCommon(gameKey, true)) {
            return true;
        }

        BloodMenuHandler menu = game.menu;
        if (BloodKeys.Show_SaveMenu.equals(gameKey)) {
            if (numplayers > 1) {
                return false;
            }
            if (gMe.pXsprite.getHealth() != 0) {
                gGameScreen.capture(320, 200);
                menu.mOpen(menu.mMenus[SAVEGAME], -1);
            }
        }

        if (BloodKeys.Quicksave.equals(gameKey)) { // quick save
            quicksave();
        }

        if (BloodKeys.Quickload.equals(gameKey)) { // quick load
            quickload();
        }

        if (gViewMode == kView3D) {
            if (GameKeys.Shrink_Screen.equals(gameKey)) {
                viewResizeView(cfg.gViewSize + 1);
            }

            if (GameKeys.Enlarge_Screen.equals(gameKey)) {
                viewResizeView(cfg.gViewSize - 1);
            }
        }

        return false;
    }

    protected void makeScreenshot() {
        String name = "scrxxxx.png";
        if (mUserFlag == UserFlag.UserMap && gUserMapInfo != null) {
            name = "scr-" + game.getFilename(gUserMapInfo.getName()) + "-xxxx.png";
        }
        if (mUserFlag != UserFlag.UserMap && currentEpisode != null) {
            name = "scr-e" + (pGameInfo.nEpisode + 1) + "m" + (pGameInfo.nLevel + 1) + "[" + currentEpisode.getIniName() + "]-xxxx.png";
        }

        Renderer renderer = game.getRenderer();
        String filename = renderer.screencapture(game.getUserDirectory(), name);
        if (filename != null) {
            viewSetMessage(filename + " saved", gViewIndex, 10);
        } else {
            viewSetMessage("Screenshot not saved. Access denied!", gViewIndex, 7);
        }
    }

    protected void openGamma(BloodMenuHandler menu) {
//		if (cfg.gGamma++ >= gGammaLevels - 1)
//		cfg.gGamma = 0;
//	viewSetMessage("Gamma correction level " + cfg.gGamma, gViewIndex);
//	scrSetGamma(cfg.gGamma);

        menu.mOpen(menu.mMenus[COLORCORR], -1);
    }

    @Override
    public void sndHandlePause(boolean pause) {
        SOUND.sndHandlePause(pause);
    }

    // Change map methods

    @Override
    protected boolean prepareboard(Entry map) {
        gNameShowTime = 500;
        if (!(GameScreen.this instanceof DemoScreen)) {
            gDemoScreen.onStopPlaying();
        }
        return DB.prepareboard(this);
    }

    /**
     * @param item should be BloodIniFile or FileEntry (map)
     */
    public void newgame(final boolean isMultiplayer, final Object item, final int episodeNum, final int nLevel, final int nGlobalDifficulty, final int nEnemyQuantity, final int nEnemyDamage, final boolean nPitchforkOnly) {
        pNet.ready2send = false;
        game.changeScreen(load); // checkEpisodeResources is slow, so we make other loading screens
        load.init(() -> {
            if (!isMultiplayer) {
                kFakeMultiplayer = false;
                if (numplayers > 1) {
                    pNet.NetDisconnect(myconnectindex);
                }
                pGameInfo.copy(defGameInfo);
                game.nNetMode = NetMode.Single;
            } else {
                pGameInfo.nGameType = pNetInfo.nGameType;
                pGameInfo.nEpisode = pNetInfo.nEpisode;
                pGameInfo.nLevel = pNetInfo.nLevel;
                pGameInfo.nDifficulty = pNetInfo.nDifficulty;
                pGameInfo.nMonsterSettings = pNetInfo.nMonsterSettings;
                pGameInfo.nWeaponSettings = pNetInfo.nWeaponSettings;
                pGameInfo.nItemSettings = pNetInfo.nItemSettings;

                pGameInfo.nFriendlyFire = pNetInfo.nFriendlyFire;
                pGameInfo.nReviveMode = pNetInfo.nReviveMode;

                pGameInfo.nEnemyQuantity = pNetInfo.nDifficulty;
                pGameInfo.nEnemyDamage = pNetInfo.nDifficulty;

                pGameInfo.nPitchforkOnly = false;
                pGameInfo.nFragLimit = pNetInfo.nFragLimit;
                game.nNetMode = NetMode.Multiplayer;
            }

            UserFlag flag = UserFlag.None;
            if (item instanceof BloodIniFile && !item.equals(MainINI)) {
                flag = UserFlag.Addon;
                BloodIniFile ini = (BloodIniFile) item;
                checkEpisodeResources(ini);
                getEpisodeInfo(gUserEpisodeInfo, ini);
                Console.out.println("Start user episode: " + episodeNum);
            } else {
                resetEpisodeResources();
            }

            if (item instanceof FileEntry) {
                flag = UserFlag.UserMap;
            }
            mUserFlag = flag;

            if (flag == UserFlag.UserMap) {
                FileEntry mLevelEntry = (FileEntry) item;
                if (!mLevelEntry.isExtension("map")) {
                    throw new WarningException("Not a map!");
                }
                MapInfo userMapInfo = getUserMapInfo(mLevelEntry);

                pGameInfo.nEpisode = 0;
                pGameInfo.nLevel = kMaxMap;

                pGameInfo.zLevelSong = userMapInfo.Song;
                pGameInfo.nTrackNumber = userMapInfo.Track;
                pGameInfo.zLevelName = mLevelEntry;
                currentEpisode = null;
                Console.out.println("Start user map: " + userMapInfo.Title);
            } else {
                loadMapInfo(episodeNum, nLevel);
            }

            // #GDX 22.12.2024 Try to find map in demo by filename
            if (DemoScreen.isDemoPlaying() && !pGameInfo.zLevelName.exists()) {
                Entry demoMapEntry = demfile.getGameInfo().zLevelName;
                if (demoMapEntry.getExtension().isEmpty()) {
                    Path newPath = FileUtils.getPath(demoMapEntry.getName() + ".map");
                    pGameInfo.zLevelName = game.getCache().getEntry(newPath, true);
                }
            }

            pGameInfo.nDifficulty = nGlobalDifficulty;
            pGameInfo.nEnemyQuantity = nEnemyQuantity;
            pGameInfo.nEnemyDamage = nEnemyDamage;
            pGameInfo.nPitchforkOnly = nPitchforkOnly;
            pGameInfo.uGameFlags = 0; // new game flag

            cheatsOn = false;

            playerGodMode(gMe, 0);
            gInfiniteAmmo = false;
            gFullMap = false;
            cheatSubInventory(gMe);
            gNoClip = false;

            String title = getTitle();
            if (!hasCutsceneA(title)) {
                loadboard(pGameInfo.zLevelName, null).setTitle(title);
            }
        });
    }

    public void nextmap() {
        // setting level options from Finish
        loadMapInfo(pGameInfo.nEpisode, gNextMap);

        String title = getTitle();
        if (!hasCutsceneA(title)) {
            loadboard(pGameInfo.zLevelName, null).setTitle(title);
        }
        if (pGameInfo.nGameType == 0) {
            gAutosaveRequest = true;
        }
    }

    @Override
    public GameAdapter setTitle(String title) {
        boardfilename = title;
        return super.setTitle(title);
    }

    private boolean hasCutsceneA(final String loadTitle) {
        if (pGameInfo.nGameType != kNetModeOff || kFakeMultiplayer || mUserFlag == UserFlag.UserMap || !currentEpisode.hasCutsceneA(pGameInfo.nLevel) || this == gDemoScreen) {
            return false;
        }

        if (gCutsceneScreen.init(currentEpisode.CutSceneA, currentEpisode.CutWavA)) {
            pGameInfo.uGameFlags |= CutsceneA;
            gCutsceneScreen.setCallback(() -> {
                pGameInfo.uGameFlags &= ~CutsceneA;
                loadboard(pGameInfo.zLevelName, null).setTitle(loadTitle);
            }).escSkipping(true);
            game.changeScreen(gCutsceneScreen);
            return true;
        }

        return false;
    }

    private boolean checkCutsceneB() {
        if (pGameInfo.nGameType != kNetModeOff || kFakeMultiplayer || mUserFlag == UserFlag.UserMap || !currentEpisode.hasCutsceneB(pGameInfo.nLevel) || this == gDemoScreen) {
            return false;
        }

        return (pGameInfo.uGameFlags & CutsceneB) != 0 && gCutsceneScreen.init(currentEpisode.CutSceneB, currentEpisode.CutWavB);
    }

    public String getTitle() {
        String title = "null";
        if (mUserFlag != UserFlag.UserMap) {
            if (mUserFlag == UserFlag.None) {
                title = getEpisodeTitle();
            } else if (mUserFlag == UserFlag.Addon) {
                if ((title = getUserEpisodeTitle()) == null) {
                    return "null";
                }
            }
        } else {
            title = getMapTitle();
        }

        return title;
    }

    public String getUserEpisodeTitle() {
        if (gUserEpisodeInfo[pGameInfo.nEpisode] != null && gUserEpisodeInfo[pGameInfo.nEpisode].gMapInfo[pGameInfo.nLevel] != null) {
            return gUserEpisodeInfo[pGameInfo.nEpisode].gMapInfo[pGameInfo.nLevel].Title;
        }

        return null;
    }

    public String getEpisodeTitle() {
        if (gEpisodeInfo[pGameInfo.nEpisode].gMapInfo[pGameInfo.nLevel] != null) {
            return gEpisodeInfo[pGameInfo.nEpisode].gMapInfo[pGameInfo.nLevel].Title;
        }

        return null;
    }

    public String getMapTitle() {
        if (gUserMapInfo != null) {
            return gUserMapInfo.Title;
        }

        return null;
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Screens;

import ru.m210projects.Blood.Factory.BloodEngine;
import ru.m210projects.Blood.Main;
import ru.m210projects.Build.Pattern.ScreenAdapters.LoadingAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.Blood.Globals.kPalNormal;
import static ru.m210projects.Blood.SOUND.sndStopAllSounds;
import static ru.m210projects.Blood.Screen.scrReset;
import static ru.m210projects.Blood.Strings.loading;
import static ru.m210projects.Blood.Strings.wait;
import static ru.m210projects.Blood.View.viewShowLoadingTile;

public class LoadingScreen extends LoadingAdapter {

    private final Main app;
    private final BloodEngine engine;

    public LoadingScreen(Main game) {
        super(game);
        this.app = game;
        this.engine = Main.engine;
    }

    @Override
    public void draw(String title, float delta) {
        viewShowLoadingTile();
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 20 << 16, 65536, 0, 2038, -128, 0, 2 | 4 | 8 | 64);

        app.getFont(1).drawTextScaled(renderer, 160, 13, loading, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        app.getFont(3).drawTextScaled(renderer, 160, 134, wait, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

        app.getFont(1).drawTextScaled(renderer, 160, 60, title, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

    @Override
    public void show() {
        super.show();

        sndStopAllSounds();
        engine.getPaletteManager().setPalette(kPalNormal);
        scrReset();
    }

}

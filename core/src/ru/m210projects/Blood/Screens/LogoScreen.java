// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Screens;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Blood.Factory.BloodEngine;
import ru.m210projects.Blood.Main;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.SkippableAdapter;
import ru.m210projects.Build.Render.Renderer;

import static ru.m210projects.Blood.Globals.kPalNormal;
import static ru.m210projects.Blood.SOUND.sndStartSample;
import static ru.m210projects.Blood.SOUND.sndStopAllSounds;
import static ru.m210projects.Blood.Screen.*;

public class LogoScreen extends SkippableAdapter {

    private final float gShowTime;
    private int nTile;
    private float gTicks;
    private Runnable callback;
    private final BloodEngine engine;

    public LogoScreen(BuildGame game, float gShowTime) {
        super(game);
        this.gShowTime = gShowTime;
        this.engine = Main.engine;
    }

    public LogoScreen setTile(int nTile) {
        this.nTile = nTile;
        return this;
    }

    public LogoScreen setCallback(Runnable callback) {
        this.callback = callback;
        this.setSkipping(callback);
        return this;
    }

    @Override
    public void show() {
        this.gTicks = 0;

        sndStopAllSounds();
        sndStartSample(255, 128, 0, false);
        engine.getPaletteManager().setPalette(kPalNormal);
        scrReset();
    }

    @Override
    public void draw(float delta) {

        if ((gTicks += delta) >= gShowTime && callback != null && skipCallback != null) {
            Gdx.app.postRunnable(callback);
            callback = null;
        }

        Renderer renderer = game.getRenderer();
        renderer.clearview(0);
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, nTile, 0, 0, 10 | 64);
    }
}

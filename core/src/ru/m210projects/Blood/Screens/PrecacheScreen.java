// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Screens;

import ru.m210projects.Blood.Main;
import ru.m210projects.Blood.SOUND;
import ru.m210projects.Blood.Types.Seq.SeqType;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Blood.filehandlers.art.BloodArtEntry;
import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Types.AnimType;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.osd.Console;

import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.PLAYER.weaponQAVs;
import static ru.m210projects.Blood.Strings.loading;
import static ru.m210projects.Blood.Strings.wait;
import static ru.m210projects.Blood.Tile.tileLoadVoxel;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Hud.HudRenderer.*;
import static ru.m210projects.Blood.VERSION.*;
import static ru.m210projects.Blood.View.viewHandAnim;
import static ru.m210projects.Blood.View.viewShowLoadingTile;
import static ru.m210projects.Blood.Weapon.kQAVEnd;
import static ru.m210projects.Blood.Weapon.kQAVEnd100;

public class PrecacheScreen extends DefaultPrecacheScreen {

    public PrecacheScreen(Runnable toLoad, PrecacheListener listener) {
        super(Main.game, toLoad, listener);

        BoardService service = engine.getBoardService();

        addQueue("Preload sounds...", SOUND::sndPreloadSounds);

        addQueue("Preload hud fonts...", () -> {
            for (int i = 0; i < 10; i++) {
                tilePreloadTile(kBigRed + i);
                tilePreloadTile(kBigBlue + i);
                tilePreloadTile(kLittleWhite + i);
                tilePreloadTile(kLittleRed + i);
            }
        });

        addQueue("Preload floor and ceiling tiles...", () -> {
            int nSkyTile = -1;
            for (int i = 0; i < service.getSectorCount(); i++) {
                tilePreloadTile(boardService.getSector(i).getFloorpicnum());
                tilePreloadTile(boardService.getSector(i).getCeilingpicnum());
                if ((boardService.getSector(i).getCeilingstat() & kSectorParallax) != 0) {
                    if (nSkyTile == -1)
                        nSkyTile = boardService.getSector(i).getCeilingpicnum();
                }
            }
            doprecache(0);

            if (nSkyTile > -1 && nSkyTile < kMaxTiles)
                for (int j = 1; j < gSkyCount; j++)
                    tilePreloadTile(nSkyTile + j);
            doprecache(1);
        });

        addQueue("Preload wall tiles...", () -> {
            for (int i = 0; i < service.getWallCount(); i++) {
                tilePreloadTile(boardService.getWall(i).getPicnum());
                if (boardService.getWall(i).getOverpicnum() >= 0)
                    tilePreloadTile(boardService.getWall(i).getOverpicnum());
            }
            doprecache(0);
        });

        addQueue("Preload sprite tiles...", () -> {
            for (int i = dudeInfo[kDudePlayer1 - kDudeBase].seqStartID;
                 i <= dudeInfo[kDudePlayer1 - kDudeBase].seqStartID + 18; i++)
                SeqPreload(i);

            for (int i = 0; i < boardService.getSpriteCount(); i++) {
                Sprite pSprite = boardService.getSprite(i);
                if (pSprite == null) {
                    continue;
                }

                if (pSprite.getStatnum() < kMaxStatus) {
                    if (pSprite.getStatnum() == kStatDude)
                        DudePrecache(pSprite);
                    else if (pSprite.getStatnum() == kStatThing)
                        ThingPrecache(pSprite);
                    tilePreloadTile(pSprite.getPicnum());
                }

                XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                if (pSprite.getStatnum() != kStatFree && pXSprite != null) {
                    int nObject = pXSprite.getDropMsg();
                    if (nObject >= kItemBase && nObject < kItemMax) {
                        int item = pXSprite.getDropMsg() - kItemBase;
                        if (item >= 0 && item < gItemInfo.length) {
                            int nTile = gItemInfo[item].picnum;
                            tilePreloadTile(nTile);
                        }
                    } else if (nObject >= kAmmoItemBase && nObject < kAmmoItemMax) {
                        int ammo = pXSprite.getDropMsg() - kAmmoItemBase;
                        if (ammo >= 0 && ammo < gAmmoItemData.length) {
                            int nTile = gAmmoItemData[ammo].picnum;
                            tilePreloadTile(nTile);
                        }
                    } else if (nObject >= kWeaponItemBase
                            && nObject < kWeaponItemMax) {
                        int weap = pXSprite.getDropMsg() - kWeaponItemBase;
                        if (weap >= 0 && weap < gWeaponItemData.length) {
                            int nTile = gWeaponItemData[weap].picnum;
                            tilePreloadTile(nTile);
                        }
                    }

                    if (pXSprite.getKey() > 0 && pXSprite.getKey() < 7) {
                        int nTile = gItemInfo[pXSprite.getKey() - 1].picnum;
                        tilePreloadTile(nTile);
                    }
                }
            }

            // Cultists pickup preload voxels
            tilePreloadTile(gWeaponItemData[1].picnum); // Shotgun
            tilePreloadTile(gWeaponItemData[2].picnum); // TommyGun

            tilePreloadTile(gAmmoItemData[7].picnum);
            tilePreloadTile(gAmmoItemData[9].picnum);

            doprecache(1);
        });

        addQueue("Preload qav animation...", () -> {
            int kEnd = kQAVEnd;
            if (GAMEVER == VER100)
                kEnd = kQAVEnd100;

            for (int i = 0; i < kEnd; i++) {
                if (weaponQAVs[i] != null)
                    weaponQAVs[i].Preload(PrecacheScreen.this);
            }
            if (!SHAREWARE)
                viewHandAnim.pQAV.Preload(PrecacheScreen.this);
            doprecache(1);
        });

        addQueue("Preload effects...", () -> {
            //Explosions
            SeqPreload(3);
            SeqPreload(4);
            SeqPreload(5);
            SeqPreload(9);
            //LifeLeech sparks
            SeqPreload(55);
            SeqPreload(56);
            //TeslaCannon sparks
            SeqPreload(11);
            SeqPreload(30);
            //SprayCan fire
            SeqPreload(0);
            SeqPreload(1);
            //NaPalm weapon projectile
            SeqPreload(60);
            SeqPreload(61);
            //Tommygun eject
            SeqPreload(62);
            SeqPreload(63);
            SeqPreload(64);
            //Shotgun eject
            SeqPreload(65);
            SeqPreload(66);
            SeqPreload(67);
            //Ricochet
            SeqPreload(44);
            SeqPreload(45);
            SeqPreload(46);
            //Blood
            SeqPreload(49);
            SeqPreload(50);
            SeqPreload(51);
            SeqPreload(52);
            SeqPreload(57);
            SeqPreload(58);
            SeqPreload(59);

            SeqPreload(7); //Water Drip
            SeqPreload(8); //Blood Drip

            Console.out.println("Preload effects");
            for (int i = 0; i < kFXMax; i++)
                tilePreloadTile(gEffectInfo[i].picnum);

            doprecache(1);
        });

        addQueue("Preload projectiles...", () -> {
            tilePreloadTile(thingInfo[kThingZombieHead - kThingBase].picnum);
            tilePreloadTile(thingInfo[kThingAltNapalm - kThingBase].picnum);
            tilePreloadTile(thingInfo[kThingTNTStick - kThingBase].picnum);
            tilePreloadTile(thingInfo[kThingTNTBundle - kThingBase].picnum);
            tilePreloadTile(thingInfo[kThingSprayBundle - kThingBase].picnum);
            tilePreloadTile(thingInfo[kThingTNTProx - kThingBase].picnum);
            tilePreloadTile(thingInfo[kThingTNTRem - kThingBase].picnum);
            tilePreloadTile(gMissileData[kMissileFlare - kMissileBase].picnum);
            tilePreloadTile(gMissileData[kMissileAltTesla - kMissileBase].picnum);
            tilePreloadTile(gMissileData[kMissileTesla - kMissileBase].picnum);

            doprecache(1);
        });

//        addQueue("Preload other tiles...", new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i < MAXTILES; i++) {
//                    if (engine.getTile(i).data == null)
//                        engine.loadtile(i);
//                }
//            }
//        });
    }

    @Override
    public void draw(String title, int index) {
        viewShowLoadingTile();
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 20 << 16, 65536, 0, 2038, -128, 0, 2 | 4 | 8 | 64);

        game.getFont(1).drawTextScaled(renderer, 160, 13, loading, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        game.getFont(3).drawTextScaled(renderer, 160, 134, wait, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

        String mapname = gGameScreen.getTitle();
        if (mapname != null)
            game.getFont(1).drawTextScaled(renderer, 160, 60, mapname, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);


        game.getFont(3).drawTextScaled(renderer, 160, 60 + 32, title, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
    }

    private void SeqPreload(int nSeqID) {
        SeqType pSeq = SeqType.getInstance(nSeqID);
        if (pSeq != null && pSeq.getFrames() > 0) {
            for (int i = 0; i < pSeq.getFrames(); i++) {
                tilePreloadTile(pSeq.getFrame(i).nTile);
            }
        }
    }

    private void DudePrecache(Sprite pSprite) {
        int type = pSprite.getLotag() - kDudeBase;
        SeqPreload(dudeInfo[type].seqStartID);
        SeqPreload(dudeInfo[type].seqStartID + 1);
        SeqPreload(dudeInfo[type].seqStartID + 2);
        SeqPreload(dudeInfo[type].seqStartID + 3);
        SeqPreload(dudeInfo[type].seqStartID + 5);
        switch (pSprite.getLotag()) {
            case kDudeEel:
                SeqPreload(dudeInfo[type].seqStartID + 7);
                break;
            case kDudeTinyCaleb:
                SeqPreload(dudeInfo[type].seqStartID + 4);
                SeqPreload(dudeInfo[type].seqStartID + 6);
                SeqPreload(dudeInfo[type].seqStartID + 8);
                SeqPreload(dudeInfo[type].seqStartID + 10);
                break;
            case kDudeFleshGargoyle:
            case kDudeStoneGargoyle:
            case kDudeHand:
            case kDudeCerberus:
            case kDudeCerberus2:
            case kDudeBat:
            case kDudeRat:
                SeqPreload(dudeInfo[type].seqStartID + 6);
                SeqPreload(dudeInfo[type].seqStartID + 7);
                break;
            case kDudeTommyCultist:
            case kDudeShotgunCultist:
            case kDudeTeslaCultist:
            case kDudeDynamiteCultist:
            case kDudeBeastCultist:
                SeqPreload(dudeInfo[type].seqStartID + 3);
                SeqPreload(dudeInfo[type].seqStartID + 4);
                SeqPreload(dudeInfo[type].seqStartID + 6);
                SeqPreload(dudeInfo[type].seqStartID + 7);
                SeqPreload(dudeInfo[type].seqStartID + 8);
                SeqPreload(dudeInfo[type].seqStartID + 9);
                SeqPreload(dudeInfo[type].seqStartID + 13);
                SeqPreload(dudeInfo[type].seqStartID + 14);
                SeqPreload(dudeInfo[type].seqStartID + 15);
                SeqPreload(dudeInfo[type].seqStartID + 17);
                SeqPreload(4099); //cultist burning
                SeqPreload(4111); //cultist burned
                SeqPreload(4112); //cultist burned2
                break;

            case kDudeInnocent:
                SeqPreload(12551); //Innocent burned
            case kDudePhantasm:
                SeqPreload(dudeInfo[type].seqStartID + 4);
            case kDudeFleshStatue:
            case kDudeStoneStatue:
                SeqPreload(dudeInfo[type].seqStartID + 6);
                break;
            case kDudeGillBeast:
                SeqPreload(dudeInfo[type].seqStartID + 6);
                SeqPreload(dudeInfo[type].seqStartID + 7);
                SeqPreload(dudeInfo[type].seqStartID + 9);
                SeqPreload(dudeInfo[type].seqStartID + 10);
                break;
            case kDudeTheBeast:
                SeqPreload(dudeInfo[type].seqStartID + 9);
                SeqPreload(2576);
            case kDudeHound:
                SeqPreload(dudeInfo[type].seqStartID + 4);
            case kDudeGreenPod:
            case kDudeGreenTentacle:
            case kDudeFirePod:
            case kDudeFireTentacle:
            case kDudeMotherPod:
            case kDudeMotherTentacle:
            case kDudeBrownSpider:
            case kDudeRedSpider:
            case kDudeBlackSpider:
            case kDudeMotherSpider:
            case kDudeTchernobog:
                SeqPreload(dudeInfo[type].seqStartID + 6);
                SeqPreload(dudeInfo[type].seqStartID + 7);
                SeqPreload(dudeInfo[type].seqStartID + 8);
                break;
            case kDudeButcher:
                SeqPreload(dudeInfo[type].seqStartID + 4);
                SeqPreload(dudeInfo[type].seqStartID + 6);
                SeqPreload(dudeInfo[type].seqStartID + 8);
                SeqPreload(dudeInfo[type].seqStartID + 9);
                SeqPreload(dudeInfo[type].seqStartID + 10);
                break;
            case kDudeAxeZombie:
            case kDudeEarthZombie:
            case kDudeSleepZombie:
                SeqPreload(dudeInfo[type].seqStartID + 4);
                SeqPreload(dudeInfo[type].seqStartID + 6);
                SeqPreload(dudeInfo[type].seqStartID + 7);
                SeqPreload(dudeInfo[type].seqStartID + 8);
                SeqPreload(dudeInfo[type].seqStartID + 11);
                SeqPreload(dudeInfo[type].seqStartID + 13);
                SeqPreload(dudeInfo[type].seqStartID + 14);
                if (pSprite.getLotag() == kDudeEarthZombie) {
                    SeqPreload(dudeInfo[type].seqStartID + 9);
                    SeqPreload(dudeInfo[type].seqStartID + 12);
                }
                if (pSprite.getLotag() == kDudeSleepZombie)
                    SeqPreload(dudeInfo[type].seqStartID + 10);
                break;
        }
    }

    private void ThingPrecache(Sprite pSprite) {
        switch (pSprite.getLotag()) {
            case kThingClearGlass:
            case kThingFluorescent:
                SeqPreload(12);
                break;
            case kThingWeb:
                SeqPreload(15);
                break;
            case kThingMetalGrate1:
                SeqPreload(21);
                break;
            case kThingFlammableTree:
                SeqPreload(25);
                SeqPreload(26);
                break;
            case kThingMachineGun:
                SeqPreload(38);
                SeqPreload(40);
                SeqPreload(28);
                break;
            case kThingGibObject:
                break;
        }
    }

    public void tilePreloadTile(int nTile) {
        if (nTile < 0 || nTile >= kMaxTiles)
            return;

        ArtEntry art = engine.getTile(nTile);

        if (art instanceof BloodArtEntry) {
            int view = 0;
            switch (((BloodArtEntry) art).getView()) {
                case kSpriteViewSingle:
                    view = 1;
                    break;
                case kSpriteView5Full:
                    view = 5;
                    break;
                case kSpriteView8Full:
                    view = 8;
                    break;
                case kSpriteView5Half:
                    view = 2;
                    break;
                case kSpriteViewVoxel:
                case kSpriteViewSpinVoxel:
                    tileLoadVoxel(nTile);
                    view = 1;
                    break;
                default:
                    break;
            }

            while (view > 0) {
                ArtEntry pic = engine.getTile(nTile);
                if (pic.getType() != AnimType.NONE) {
                    for (int i = pic.getAnimFrames(); i >= 0; i--) {
                        if (pic.getType() == AnimType.BACKWARD)
                            addTile(nTile - i);
                        else addTile(nTile + i);
                    }
                } else addTile(nTile);

                nTile += pic.getAnimFrames() + 1;
                view--;
            }
        }
    }
}

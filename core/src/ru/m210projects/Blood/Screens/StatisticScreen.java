// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Screens;

import ru.m210projects.Blood.Factory.BloodEngine;
import ru.m210projects.Blood.Main;
import ru.m210projects.Build.Pattern.ScreenAdapters.SkippableAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;

import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.gGameScreen;
import static ru.m210projects.Blood.SOUND.sndStartSample;
import static ru.m210projects.Blood.SOUND.sndStopAllSounds;
import static ru.m210projects.Blood.Screen.scrReset;
import static ru.m210projects.Blood.Strings.*;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;

public class StatisticScreen extends SkippableAdapter {

    protected float gTicks;
    protected float gShowTime;
    protected boolean maySkipped;
    protected Main app;
    protected BloodEngine engine;

    public StatisticScreen(Main game, float gShowTime) {
        super(game);
        this.app = game;
        this.engine = Main.engine;
        this.gShowTime = gShowTime;
    }

    @Override
    public void show() {
        this.gTicks = 0;
        this.maySkipped = false;

        scrReset();
        engine.getPaletteManager().setPalette(kPalNormal);

        sndStopAllSounds();
        sndStartSample(268, 128, -1, false); // MOANS
    }

    @Override
    public void draw(float delta) {
        Renderer renderer = game.getRenderer();
        renderer.clearview(0);
        viewShowStatus(maySkipped);

        if ((gTicks += delta) >= gShowTime) maySkipped = true;
    }

    public void viewShowStatus(boolean maySkipped) {
        viewShowLoadingTile();
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(160 << 16, 20 << 16, 65536, 0, 2038, -128, 0, 78);

        if (pGameInfo.nGameType > kNetModeCoop) {
            game.getFont(1).drawTextScaled(renderer, 160, 20 - game.getFont(1).getSize() / 2, frags, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

            game.getFont(3).drawTextScaled(renderer, 85, 35, "#", 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            game.getFont(3).drawTextScaled(renderer, 110, 35, "name", 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            game.getFont(3).drawTextScaled(renderer, 225, 35, "frags", 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                viewDrawNumber(3, i, 85, 50 + i * 10, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                game.getFont(3).drawTextScaled(renderer, 110, 50 + i * 10, app.net.gProfile[i].name, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                viewDrawNumber(3, gPlayer[i].fragCount, 225, 50 + i * 10, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            }
        } else {
            game.getFont(1).drawTextScaled(renderer, 160, 20 - game.getFont(1).getSize() / 2, levelstats, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            if (cheatsOn)
                game.getFont(3).drawTextScaled(renderer, 160, 32, ">>> YOU CHEATED! <<<", 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
            viewFragStat();
            viewSecretStat();
        }

        game.getFont(3).drawTextScaled(renderer, 20, 191, Main.game.sversion, 1.0f, 32, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        if (maySkipped && (engine.getTotalClock() & 0x20) != 0)
            game.getFont(3).drawTextScaled(renderer, 160, 134, keycontinue, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

    @Override
    public void skip() {
        if (maySkipped) gGameScreen.nextmap();
        super.skip();
    }

}

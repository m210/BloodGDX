// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import ru.m210projects.Blood.filehandlers.art.BloodArtEntry;
import ru.m210projects.Blood.filehandlers.art.BloodArtEntry.ViewType;
import ru.m210projects.Build.Render.ModelHandle.Voxel.VoxelData;
import ru.m210projects.Build.Render.ModelHandle.VoxelInfo;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.InputStream;
import java.util.Arrays;

import static ru.m210projects.Blood.Globals.kMaxTiles;
import static ru.m210projects.Blood.Main.*;

public class Tile {

    public static int kSurfNone = 0;
    public static int kSurfStone = 1;
    public static int kSurfMetal = 2;
    public static int kSurfWood = 3;
    public static int kSurfFlesh = 4;
    public static int kSurfWater = 5;
    public static int kSurfDirt = 6;
    public static int kSurfClay = 7;
    public static int kSurfSnow = 8;
    public static int kSurfIce = 9;
    public static int kSurfLeaves = 10;
    public static int kSurfCloth = 11;
    public static int kSurfPlant = 12;
    public static int kSurfGoo = 13;
    public static int kSurfLava = 14;
    public static int kSurfMax = 15;

    public static int[] surfSfxLand = {
            -1,
            600,
            601,
            602,
            603,
            604,
            605,
            605,
            605,
            600,
            605,
            605,
            605,
            604,
            603,
    };

    public static int[][] surfSfxMove = {
            new int[]{-1, -1},
            new int[]{802, 803},
            new int[]{804, 805},
            new int[]{806, 807},
            new int[]{808, 809},
            new int[]{810, 811},
            new int[]{812, 813},
            new int[]{814, 815},
            new int[]{816, 817},
            new int[]{818, 819},
            new int[]{820, 821},
            new int[]{822, 823},
            new int[]{824, 825},
            new int[]{826, 827},
            new int[]{828, 829},
    };

    public static short[] gVoxelData = new short[kMaxTiles];
    public static byte[] shadeTable = new byte[kMaxTiles];

    public static void tileInit() {
        voxelsInit("VOXEL.DAT");
        shadeInit("SHADE.DAT");
    }

    public static void voxelsInit(String name) {
        Arrays.fill(gVoxelData, (byte) -1);
        Entry entry = game.getCache().getEntry(name, true);
        if (entry.exists()) {
            try(InputStream is = entry.getInputStream()) {
                for (int i = 0; i < entry.getSize() / 2; i++)
                    gVoxelData[i] = (short) StreamUtils.readShort(is);
            } catch (Exception ignored) {
            }
        }
    }

    public static void tileLoadVoxel(int nTile) {
        if (engine.getTile(nTile) instanceof BloodArtEntry) {
            BloodArtEntry pic = (BloodArtEntry) engine.getTile(nTile);

            if (pic.getView() != ViewType.kSpriteViewVoxel && pic.getView() != ViewType.kSpriteViewSpinVoxel)
                return;

            int nVoxel = gVoxelData[nTile];
            if (game.currentDef.mdInfo.getVoxelInfo(nTile) == null && nVoxel >= 0 && nVoxel < 512) {
                Entry buffer = game.getCache().getEntry(nVoxel, "KVX");
                if (buffer.exists()) {
                    try {
                        VoxelData vox = new VoxelData(buffer);
                        for (int i = 0; i < vox.zpiv.length; i++)
                            vox.zpiv[i] += (pic.getOffsetY() << 8) / (1 << i);

                        game.currentDef.mdInfo.addVoxelInfo(new VoxelInfo(vox), nTile);
                    } catch (Exception ignored) {
                    }
                }
            }
        }
    }

    public static void shadeInit(String name) {
        Entry entry = game.getCache().getEntry(name, true);
        if (!entry.exists()) return;

        byte[] data = entry.getBytes();
        System.arraycopy(data, 0, shadeTable, 0, Math.min(data.length, shadeTable.length));
    }
}

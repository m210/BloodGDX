// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.AI.AISTATEFUNC.Type;
import ru.m210projects.Blood.AI.DudeExtra;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.Types.*;
import ru.m210projects.Build.Board;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Blood.AI.AIUNICULT.*;
import static ru.m210projects.Blood.AI.Ai.*;
import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.getIndex;
import static ru.m210projects.Blood.EVENT.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Gib.walGenerateGib;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.SECTORFX.*;
import static ru.m210projects.Blood.SOUND.*;
import static ru.m210projects.Blood.Strings.seq;
import static ru.m210projects.Blood.Trig.Cos;
import static ru.m210projects.Blood.Trig.Sin;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.DudeInfo.dudeInfo;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.*;
import static ru.m210projects.Blood.VERSION.*;
import static ru.m210projects.Blood.View.*;
import static ru.m210projects.Blood.Weapon.LeechOperate;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClampAngle;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.mulscale;

public class Trigger {
    public static final int kMaxBusyArray = 128;
    public static final int kRemoteDelay = 18;
    public static final int kProxDelay = 30;
    public static final int kBusyOk = 0;
    public static final int kBusyRestore = 1;
    public static final int kBusyReverse = 2;
    public static final int kBusyComplete = 3;
    private static final Vector3 wallVel = new Vector3(100, 100, 250);
    public static int gBusyCount = 0;
    public static BUSY[] gBusy = new BUSY[kMaxBusyArray];
    public static Vector2[] kwall = new Vector2[MAXWALLS];
    public static int[] secFloorZ = new int[MAXSECTORS];
    public static int[] secCeilZ = new int[MAXSECTORS];
    public static int[] secPath = new int[MAXSECTORS];
    public static int getHighestZ;
    public static int getRangepzTop, getRangepzBot;
    public static BUSYPROC[] gBusyProc = {
            /*0*/ Trigger::VCrushBusy,
            /*1*/ Trigger::VSpriteBusy,
            /*2*/ Trigger::VDoorBusy,
            /*3*/ Trigger::HDoorBusy,
            /*4*/ Trigger::RDoorBusy,
            /*5*/ Trigger::SRDoorBusy,
            /*6*/ (nIndex, nBusy) -> {
        if (!(nIndex >= 0 && nIndex < boardService.getSectorCount())) {
            throw new AssertException("nSector >= 0 && nSector < boardService.getSectorCount()");
        }
        Sector pSector = boardService.getSector(nIndex);
        int nXSector = pSector.getExtra();
        if (!(nXSector > 0 && nXSector < kMaxXSectors)) {
            throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
        }
        XSECTOR pXSector = xsector[nXSector];

        pXSector.busy = nBusy;

        if (pXSector.command == kCommandLink && pXSector.txID != 0) {
            evSend(nIndex, SS_SECTOR, pXSector.txID, kCommandLink);
        }

        if ((nBusy & kFluxMask) == 0) {
            SetSectorState(nIndex, pXSector, nBusy >> 16);
            sfxSectorStop(nIndex, nBusy >> 16);
            return kBusyComplete;
        }

        return kBusyOk;
    },
            /*7*/ Trigger::PathSectorBusy,};

	/*public static int SetSpriteState( int nSprite, XSPRITE pXSprite, int state )
	{
		if ( (pXSprite.busy & kFluxMask) == 0 && pXSprite.state == state )
			return state ^ pXSprite.state;

		pXSprite.busy = state << 16;
		pXSprite.state = (short) state;

		checkEventList(nSprite, SS_SPRITE);
		if ( (boardService.getSprite(nSprite).hitag & kAttrRespawn) != 0 && (boardService.getSprite(nSprite).zvel >= kDudeBase && boardService.getSprite(nSprite).zvel < kDudeMax) )
		{
			pXSprite.respawnPending = 3;
			evPostCallback(nSprite, 3, pGameInfo.nMonsterRespawnTime, kCallbackRespawn);
			return 1;
		}

		if ( state != pXSprite.restState && pXSprite.waitTime > 0 ) {
			evPost(nSprite, SS_SPRITE, pXSprite.waitTime * kTimerRate / 10,
				pXSprite.restState != 0 ? kCommandOn : kCommandOff );
		}

		if (pXSprite.txID != 0 && ((pXSprite.triggerOn && pXSprite.state == 1) || (pXSprite.triggerOff && pXSprite.state == 0))) {

		   switch (boardService.getSprite(nSprite).lotag) {
			   case kGDXObjPropertiesChanger:
			   case kGDXObjPicnumChanger:
			   case kGDXObjSizeChanger:
			   case kGDXSectorFXChanger:
			   case kGDXObjDataChanger:
			   case kGDXSpriteDamager:
				   // by NoOne: Sending new command instead of link is *required*, because types above
				   //are universal and can paste properties in different objects.
				   switch (pXSprite.command) {
				       case kCommandLink:
				       case kGDXCommandPaste:
				           evSend(nSprite, 3, pXSprite.txID, kGDXCommandPaste); // just send command to change properties
				           return 1;
					   case kCommandUnlock:
					       evSend(nSprite, 3, pXSprite.txID, pXSprite.command); // send normal command first
					       evSend(nSprite, 3, pXSprite.txID, kGDXCommandPaste); // then send command to change properties
					       return 1;
					   default:
					       evSend(nSprite, 3, pXSprite.txID, kGDXCommandPaste); // send first command to change properties
					       evSend(nSprite, 3, pXSprite.txID, pXSprite.command); // then send normal command
					       return 1;
				       }
			   default:
			       if (pXSprite.command != kCommandLink) evSend(nSprite, 3, pXSprite.txID, pXSprite.command);
			       break;
		   }
		}

		return 1;
	}*/

    public static boolean valueIsBetween(int val, int min, int max) {
        return (val > min && val < max);
    }

    public static int Lin2Sin(int nBusy, int opt) {
        switch (opt) {
            case 0:
                return (1 << 15) - (Cos(nBusy * kAngle180 / kMaxBusyValue) >> 15);
            case 2:
                return (1 << 16) - (Cos(nBusy * kAngle90 / kMaxBusyValue) >> 14);
            case 3:
                return (Sin(nBusy * kAngle90 / kMaxBusyValue) >> 14);
        }
        return nBusy;
    }

    // by NoOne: almost same as SetSpriteState() but designed for modern types to complete some special tasks.
    public static int gdxTypeSetSpriteState(int nSprite, XSPRITE pXSprite, int state) {
        if ((pXSprite.getBusy() & kFluxMask) == 0 && pXSprite.getState() == state) {
            return state ^ pXSprite.getState();
        }

        pXSprite.setBusy(state << 16);
        pXSprite.setState((short) state);

        checkEventList(nSprite, SS_SPRITE);
        if ((boardService.getSprite(nSprite).getHitag() & kAttrRespawn) != 0 && (boardService.getSprite(nSprite).getZvel() >= kDudeBase && boardService.getSprite(nSprite).getZvel() < kDudeMax)) {
            pXSprite.setRespawnPending(3);
            evPostCallback(nSprite, 3, Integer.toUnsignedLong(pGameInfo.nMonsterRespawnTime), kCallbackRespawn);
            return 1;
        }

        if (state != pXSprite.getRestState() && pXSprite.getWaitTime() > 0) {
            evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong(pXSprite.getWaitTime() * kTimerRate) / 10L, pXSprite.getRestState() != 0 ? kCommandOn : kCommandOff);
        }

        if (pXSprite.getTxID() != 0 && ((pXSprite.isTriggerOn() && pXSprite.getState() == 1) || (pXSprite.isTriggerOff() && pXSprite.getState() == 0))) {

            // by NoOne: Sending new command instead of link is *required*, because types above
            //are universal and can paste properties in different objects.
            switch (pXSprite.getCommand()) {
                case kCommandLink:
                case kGDXCommandPaste:
                    evSend(nSprite, 3, pXSprite.getTxID(), kGDXCommandPaste); // just send command to change properties
                    break;
                case kCommandUnlock:
                    evSend(nSprite, 3, pXSprite.getTxID(), pXSprite.getCommand()); // send normal command first
                    evSend(nSprite, 3, pXSprite.getTxID(), kGDXCommandPaste); // then send command to change properties
                    break;
                default:
                    evSend(nSprite, 3, pXSprite.getTxID(), kGDXCommandPaste); // send first command to change properties
                    evSend(nSprite, 3, pXSprite.getTxID(), pXSprite.getCommand()); // then send normal command
                    break;
            }
        }

        return 1;
    }

    public static int SetSpriteState(int nSprite, XSPRITE pXSprite, int state) {
        if ((pXSprite.getBusy() & kFluxMask) == 0 && pXSprite.getState() == state) {
            return state ^ pXSprite.getState();
        }

        pXSprite.setBusy(state << 16);
        pXSprite.setState((short) state);

        checkEventList(nSprite, SS_SPRITE);
        if ((boardService.getSprite(nSprite).getHitag() & kAttrRespawn) != 0 && (boardService.getSprite(nSprite).getZvel() >= kDudeBase && boardService.getSprite(nSprite).getZvel() < kDudeMax)) {
            pXSprite.setRespawnPending(3);
            evPostCallback(nSprite, 3, Integer.toUnsignedLong(pGameInfo.nMonsterRespawnTime), kCallbackRespawn);
            return 1;
        }

        if (state != pXSprite.getRestState() && pXSprite.getWaitTime() > 0) {
            evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong(pXSprite.getWaitTime() * kTimerRate) / 10L, pXSprite.getRestState() != 0 ? kCommandOn : kCommandOff);
        }

        if (pXSprite.getTxID() != 0) {
            if (pXSprite.getCommand() == kCommandLink) {
                return 1;
            }

            if (pXSprite.isTriggerOn() && pXSprite.getState() == 1) {
                evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand());
            }

            if (pXSprite.isTriggerOff() && pXSprite.getState() == 0) {
                evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand());
            }
        }
        return 1;
    }

    public static int SetWallState(int nWall, XWALL pXWall, int state) {
        if ((pXWall.busy & kFluxMask) == 0 && pXWall.state == state) {
            return state ^ pXWall.state;
        }

        pXWall.busy = state << 16;
        pXWall.state = (short) state;

        checkEventList(nWall, SS_WALL);

        if (state != pXWall.restState && pXWall.waitTime > 0) {
            evPost(nWall, SS_WALL, Integer.toUnsignedLong(pXWall.waitTime * kTimerRate) / 10L, pXWall.restState != 0 ? kCommandOn : kCommandOff);
        }

        if (pXWall.txID != 0) {
            if (pXWall.command == kCommandLink) {
                return 1;
            }

            if (pXWall.triggerOn && pXWall.state == 1) {
//				dprintf( "evSend(%i,SS_WALL, %i, %i)\n", nWall, pXWall.txID, pXWall.command );
                evSend(nWall, SS_WALL, pXWall.txID, pXWall.command);
            }
            if (pXWall.triggerOff && pXWall.state == 0) {
//				dprintf( "evSend(%i,SS_WALL, %i, %i)\n", nWall, pXWall.txID, pXWall.command );
                evSend(nWall, SS_WALL, pXWall.txID, pXWall.command);
            }
        }
        return 1;
    }

    public static void SetSectorState(int nSector, XSECTOR pXSector, int state) {
        if ((pXSector.busy & kFluxMask) == 0 && pXSector.state == state) {
            return;
        }

        pXSector.busy = state << 16;
        pXSector.state = (short) state;

        checkEventList(nSector, SS_SECTOR);

        if (state == 1) {
            if (pXSector.command != kCommandLink && pXSector.triggerOn) {
                if (pXSector.txID != 0) {
                    evSend(nSector, SS_SECTOR, pXSector.txID, pXSector.command);
                }
            }
            if (pXSector.stopFlag[kCommandOn]) {
                pXSector.stopFlag[kCommandOn] = false;
                pXSector.stopFlag[kCommandOff] = false;
                return;
            }
            if (pXSector.waitFlag[kCommandOn]) {
                evPost(nSector, SS_SECTOR, Integer.toUnsignedLong(pXSector.waitTime[kCommandOn] * kTimerRate) / 10L, kCommandOff);
            }
            return;
        }

        if (pXSector.command != kCommandLink && pXSector.triggerOff) {
            if (pXSector.txID != 0) {
                evSend(nSector, SS_SECTOR, pXSector.txID, pXSector.command);
            }
        }
        if (pXSector.stopFlag[kCommandOff]) {
            pXSector.stopFlag[kCommandOn] = false;
            pXSector.stopFlag[kCommandOff] = false;
            return;
        }
        if (pXSector.waitFlag[kCommandOff]) {
            evPost(nSector, SS_SECTOR, Integer.toUnsignedLong(pXSector.waitTime[kCommandOff] * kTimerRate) / 10L, kCommandOn);
        }
    }

    public static void AddBusy(int nIndex, int busyProc, int nDelta) {
        if (nDelta == 0) {
            throw new AssertException("nDelta != 0");
        }
        int i;
        // find an existing nIndex busy, or an unused busy slot
        for (i = 0; i < gBusyCount; i++) {
            if ((nIndex == gBusy[i].nIndex) && (busyProc == gBusy[i].busyProc)) {
                break;
            }
        }

        // adding a new busy?
        if (i == gBusyCount) {
            if (gBusyCount == kMaxBusyArray) {
//				dprintf("OVERFLOW: AddBusy() ignored\n");
                return;
            }

            gBusy[i].nIndex = nIndex;
            gBusy[i].busyProc = busyProc;
            gBusy[i].nBusy = (nDelta > 0) ? 0 : kMaxBusyValue;
            gBusyCount++;
        }
        gBusy[i].nDelta = nDelta;
    }

    public static void ReverseBusy(int nIndex, int busyProc) {
        // find an existing nIndex busy, or an unused busy slot
        for (int i = 0; i < gBusyCount; i++) {
            if (nIndex == gBusy[i].nIndex && busyProc == gBusy[i].busyProc) {
                gBusy[i].nDelta = -gBusy[i].nDelta;
                return;
            }
        }
//		dprintf("ReverseBusy: matching busy not found!\n");
    }

    public static int GetSourceBusy(int event) {
        int nXIndex;

        switch (getType(event)) {
            case SS_SECTOR:
                nXIndex = boardService.getSector(getIndex(event)).getExtra();
                if (!(nXIndex > 0 && nXIndex < kMaxXSectors)) {
                    throw new AssertException("nXIndex > 0 && nXIndex < kMaxXSectors");
                }
                return xsector[nXIndex].busy;

            case SS_WALL:
                nXIndex = boardService.getWall(getIndex(event)).getExtra();
                if (!(nXIndex > 0 && nXIndex < kMaxXWalls)) {
                    throw new AssertException("nXIndex > 0 && nXIndex < kMaxXWalls");
                }
                return xwall[nXIndex].busy;

            case SS_SPRITE:
                nXIndex = boardService.getSprite(getIndex(event)).getExtra();
                return boardService.getXSprite(nXIndex).getBusy();
        }

        // shouldn't reach this point
        return 0;
    }

    public static void OperateSprite(int nSprite, XSPRITE pXSprite, int evCommand) {
        BloodSprite pSprite = boardService.getSprite(nSprite);

        // special handling for lock/unlock commands
        switch (evCommand) {
            case kCommandLock:
                pXSprite.setLocked(1);
                if (pSprite.getLotag() == kGDXWindGenerator) {
                    stopWindOnSectors(pXSprite);
                }
                return;

            case kCommandUnlock:
                pXSprite.setLocked(0);
                return;

            case kCommandToggleLock:
                pXSprite.setLocked(pXSprite.getLocked() ^ 1);
                if (pSprite.getLotag() == kGDXWindGenerator) {
                    if (pXSprite.getLocked() == 1) {
                        stopWindOnSectors(pXSprite);
                    }
                }
                return;
        }

        if (pSprite.getStatnum() == kStatDude) {
            if (IsDudeSprite(pSprite)) {
                switch (evCommand) {
                    case kCommandOff:
                        SetSpriteState(nSprite, pXSprite, 0);
                        break;
                    case kCommandOn:
                    case kCommandSpritePush:
                    case kCommandSpriteTouch:
                        if (pXSprite.getState() == 0) {
                            SetSpriteState(nSprite, pXSprite, 1);
                        }
                        aiActivateDude(pSprite, pXSprite);
                        break;
                    case kCommandSpriteProximity:
                        if (pXSprite.getState() == 0) {
                            SetSpriteState(nSprite, pXSprite, 1);
                            aiActivateDude(pSprite, pXSprite);
                        }
                        break;
                }
                return;
            }
        }

        switch (pSprite.getLotag()) {

            /* - Random Event Switch takes random data field and uses it as TX ID - */
            case kGDXRandomTX:

                int tx = 0;
                int maxRetries = 10;
                if (pXSprite.getData1() > 0 && pXSprite.getData2() <= 0 && pXSprite.getData3() <= 0 && pXSprite.getData4() > 0) {

                    // data1 must be less than data4
                    if (pXSprite.getData1() > pXSprite.getData4()) {
                        int tmp = pXSprite.getData1();
                        pXSprite.setData1((short) pXSprite.getData4());
                        pXSprite.setData4(tmp);
                    }

                    int total = pXSprite.getData4() - pXSprite.getData1();
                    while (maxRetries > 0) {
                        if (pGameInfo.nGameType != kNetModeOff || numplayers > 1) {
                            tx = Gameutils.Random(total) + pXSprite.getData1();
                        } else {
                            tx = (int) (total * Math.random()) + pXSprite.getData1();
                        }

                        if (tx != pXSprite.getTxID()) {
                            break;
                        }
                        maxRetries--;
                    }

                } else {

                    while (maxRetries > 0) {
                        if ((tx = GetRandDataVal(null, pSprite)) > 0 && tx != pXSprite.getTxID()) {
                            break;
                        }
                        maxRetries--;
                    }

                }

                pXSprite.setTxID((short) tx);
                SetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1);
                break;

            /* - Sequential Switch takes values from data fields starting from data1 and uses it as TX ID - */
            /* - ranged TX ID is now supported also - */
            case kGDXSequentialTX:
                boolean range = false;
                int cnt = 3;
                tx = 0;
                // set range of TX ID if data2 and data3 is empty.
                if (pXSprite.getData1() > 0 && pXSprite.getData2() <= 0 && pXSprite.getData3() <= 0 && pXSprite.getData4() > 0) {

                    // data1 must be less than data4
                    if (pXSprite.getData1() > pXSprite.getData4()) {
                        int tmp = pXSprite.getData1();
                        pXSprite.setData1((short) pXSprite.getData4());
                        pXSprite.setData4(tmp);
                    }

                    // force send command to all TX id in a range
                    if (pSprite.getHitag() == 1) {
                        for (int i = pXSprite.getData1(); i <= pXSprite.getData4(); i++) {
                            evSend(nSprite, SS_SPRITE, i, pXSprite.getCommand());
                        }

                        pXSprite.setDropMsg(0);
                        return;
                    }

                    // Make sure dropMsg is correct as we store current index of TX ID here.
                    if (pXSprite.getDropMsg() < pXSprite.getData1()) {
                        pXSprite.setDropMsg(pXSprite.getData1());
                    } else if (pXSprite.getDropMsg() > pXSprite.getData4()) {
                        pXSprite.setDropMsg((short) pXSprite.getData4());
                    }

                    range = true;

                } else {

                    // force send command to all TX id by data index
                    if (pSprite.getHitag() == 1) {
                        for (int i = 0; i < 4; i++) {
                            if ((tx = GetDataVal(pSprite, i)) > 0) {
                                evSend(nSprite, SS_SPRITE, tx, pXSprite.getCommand());
                            }
                        }

                        pXSprite.setDropMsg(0);
                        return;
                    }

                    // Make sure dropMsg is correct as we store current index of data field here.
                    if (pXSprite.getDropMsg() > 3) {
                        pXSprite.setDropMsg(0);
                    } else if (pXSprite.getDropMsg() < 0) {
                        pXSprite.setDropMsg(3);
                    }
                }

                switch (evCommand) {
                    case kCommandOff:
                        if (!range) {
                            while (cnt-- >= 0) { // skip empty data fields
                                pXSprite.setDropMsg(pXSprite.getDropMsg() - 1);
                                if (pXSprite.getDropMsg() < 0) {
                                    pXSprite.setDropMsg(3);
                                }
                                tx = GetDataVal(pSprite, pXSprite.getDropMsg());
                                if (tx < 0) {
                                    throw new AssertException(" -- Current data index is negative");
                                }
                                if (tx > 0) {
                                    break;
                                }
                            }
                        } else {
                            pXSprite.setDropMsg(pXSprite.getDropMsg() - 1);
                            if (pXSprite.getDropMsg() < pXSprite.getData1()) {
                                pXSprite.setDropMsg((short) pXSprite.getData4());
                            }
                            tx = pXSprite.getDropMsg();
                        }
                        break;

                    default:
                        if (!range) {
                            while (cnt-- >= 0) { // skip empty data fields
                                if (pXSprite.getDropMsg() > 3) {
                                    pXSprite.setDropMsg(0);
                                }
                                tx = GetDataVal(pSprite, pXSprite.getDropMsg());
                                if (tx < 0) {
                                    throw new AssertException(" ++ Current data index is negative");
                                }
                                pXSprite.setDropMsg(pXSprite.getDropMsg() + 1);
                                if (tx > 0) {
                                    break;
                                }
                                continue;
                            }
                        } else {
                            tx = pXSprite.getDropMsg();
                            if (pXSprite.getDropMsg() >= pXSprite.getData4()) {
                                pXSprite.setDropMsg(pXSprite.getData1());
                                break;
                            }
                            pXSprite.setDropMsg(pXSprite.getDropMsg() + 1);
                        }
                        break;
                }

                pXSprite.setTxID((short) tx);
                SetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1);
                break;
            case kGDXCustomDudeSpawn:
                if (pGameInfo.nMonsterSettings != 0 && actSpawnCustomDude(pSprite, -1) != null) {
                    totalKills += 1;
                }
                break;
            case kDudeSpawn:
                if (pGameInfo.nMonsterSettings != 0 && pXSprite.getData1() >= kDudeBase && pXSprite.getData1() < kDudeMax) {
                    Sprite pSpawn = null;
                    // Random dude works only if at least 2 data fields
                    // are not empty and is not original demo.
                    if (!IsOriginalDemo()) {
                        if ((pSpawn = spawnRandomDude(pSprite)) == null) {
                            pSpawn = actSpawnDude(pSprite, pXSprite.getData1(), -1);
                        }

                    } else {
                        pSpawn = actSpawnDude(pSprite, pXSprite.getData1(), -1);
                    }

                    if (pSpawn != null) {
                        totalKills += 1;
                        switch (pSpawn.getLotag()) {
                            case kDudeBurning:
                            case kDudeCultistBurning:
                            case kDudeAxeZombieBurning:
                            case kDudeBloatedButcherBurning:
                            case kDudeTinyCalebburning:
                            case kDudeTheBeastburning:
                                XSPRITE pXSpawn = boardService.getXSprite(pSpawn.getExtra());
                                pXSpawn.setHealth(dudeInfo[pXSprite.getData1() - kDudeBase].startHealth << 4);
                                pXSpawn.setBurnTime(10);
                                pXSpawn.setTarget(-1);
                                aiActivateDude((BloodSprite) pSpawn, pXSpawn);
                                break;
                        }
                    }
                }
                break;
            case kEarthQuake:
                if (pGameInfo.nGameType < 2) {
                    pXSprite.setTriggered(true);
                    pXSprite.setTriggerOn(false);
                    SetSpriteState(nSprite, pXSprite, 1);
                    for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                        int dx = (pSprite.getX() - gPlayer[i].pSprite.getX()) >> 4;
                        int dy = (pSprite.getY() - gPlayer[i].pSprite.getY()) >> 4;
                        int dz = (pSprite.getZ() - gPlayer[i].pSprite.getZ()) >> 8;
                        gPlayer[i].quakeTime = (pXSprite.getData1() << 16) / (0x40000 + dx * dx + dy * dy + dz * dz);
                    }
                }
                break;

            // by NoOne: various modern types
            case kMarkerWarpDest:
                if (pXSprite.getTxID() <= 0) {
                    if (SetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1) == 1) {
                        useTeleportTarget(pXSprite, null);
                    }
                    break;
                }
                gdxTypeSetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1);
                break;
            case kGDXSpriteDamager:
                if (pXSprite.getTxID() <= 0) {
                    if (SetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1) == 1) {
                        useSpriteDamager(pXSprite, null);
                    }
                    break;
                }
                gdxTypeSetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1);
                break;
            case kGDXObjPropertiesChanger:
            case kGDXObjPicnumChanger:
            case kGDXObjSizeChanger:
            case kGDXSectorFXChanger:
            case kGDXObjDataChanger:
                gdxTypeSetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1);
                break;
            case kSwitchToggle:
                switch (evCommand) {
                    case kCommandOff:
                        if (SetSpriteState(nSprite, pXSprite, 0) == 1) {
                            sfxStart3DSound(pSprite, pXSprite.getData2(), 0, 0);
                        }
                        break;
                    case kCommandOn:
                        if (SetSpriteState(nSprite, pXSprite, 1) == 1) {
                            sfxStart3DSound(pSprite, pXSprite.getData1(), 0, 0);
                        }
                        break;
                    default:
                        if (SetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1) == 1) {
                            if (pXSprite.getState() == 0) {
                                sfxStart3DSound(pSprite, pXSprite.getData2(), 0, 0);
                            } else {
                                sfxStart3DSound(pSprite, pXSprite.getData1(), 0, 0);
                            }
                        }
                        break;
                }
                break;
            case kSwitchMomentary:
                switch (evCommand) {
                    case kCommandOff:
                        if (SetSpriteState(nSprite, pXSprite, 0) == 1) {
                            sfxStart3DSound(pSprite, pXSprite.getData2(), 0, 0);
                        }
                        break;
                    case kCommandOn:
                        if (SetSpriteState(nSprite, pXSprite, 1) == 1) {
                            sfxStart3DSound(pSprite, pXSprite.getData1(), 0, 0);
                        }
                        break;
                    default:
                        if (SetSpriteState(nSprite, pXSprite, pXSprite.getRestState() ^ 1) == 1) {
                            if (pXSprite.getState() == 0) {
                                sfxStart3DSound(pSprite, pXSprite.getData2(), 0, 0);
                            } else {
                                sfxStart3DSound(pSprite, pXSprite.getData1(), 0, 0);
                            }
                        }
                        break;
                }
                break;

            // by NoOne: add linking for markers and stacks
            case kMarkerLowerWater:
            case kMarkerUpperWater:
            case kMarkerUpperGoo:
            case kMarkerLowerGoo:
            case kMarkerUpperLink:
            case kMarkerLowerLink:
            case kMarkerUpperStack:
            case kMarkerLowerStack:
                if (pXSprite.getCommand() == kCommandLink && pXSprite.getTxID() != 0) {
                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kCommandLink);
                }
                break;
            // by NoOne: add triggering sprite feature. Path sector will trigger the marker after
            // it gets reached so it can send commands.
            case kPathMarker:
                switch (evCommand) {
                    case kCommandOff:
                        SetSpriteState(nSprite, pXSprite, 0);
                        break;
                    case kCommandOn:
                        SetSpriteState(nSprite, pXSprite, 1);
                        break;
                    case kCommandLink:
                        if (pXSprite.getTxID() != 0) {
                            evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kCommandLink); // don't forget linking!
                        }
                        break;
                    default:
                        SetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1);
                        break;
                }
                break;
            case kSwitchCombination:
                if (evCommand == kCommandOff) {
                    pXSprite.setData1(pXSprite.getData1() - 1);
                    if (pXSprite.getData1() < 0) {            // underflow?
                        pXSprite.setData1(pXSprite.getData1() + pXSprite.getData3());
                    }
                } else {
                    pXSprite.setData1(pXSprite.getData1() + 1);
                    if (pXSprite.getData1() >= pXSprite.getData3())    // overflow?
                    {
                        pXSprite.setData1(pXSprite.getData1() - pXSprite.getData3());
                    }
                }

                // handle master switches
                if (pXSprite.getCommand() == kCommandLink && pXSprite.getTxID() != 0) {
                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kCommandLink);
                }

                sfxStart3DSound(pSprite, pXSprite.getData4(), -1, 0);

                // at right combination?
                if (pXSprite.getData1() == pXSprite.getData2()) {
                    SetSpriteState(nSprite, pXSprite, 1);
                } else {
                    SetSpriteState(nSprite, pXSprite, 0);
                }
                break;
            case kSwitchPadlock:
                switch (evCommand) {
                    case kCommandOff:
                        SetSpriteState(nSprite, pXSprite, 0);
                        break;

                    case kCommandOn:
                        SetSpriteState(nSprite, pXSprite, 1);
                        seqSpawn(getSeq(kPadlock), SS_SPRITE, pSprite.getExtra(), null);
                        break;

                    default:
                        SetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1);
                        if (pXSprite.getState() != 0) {
                            seqSpawn(getSeq(kPadlock), SS_SPRITE, pSprite.getExtra(), null);
                        }
                        break;
                }
                break;
            case kThingTNTRem:
                if (boardService.getSprite(nSprite).getStatnum() == kStatRespawn) {
                    break;
                }

                if (evCommand == kCommandOn) {
                    sfxStart3DSound(pSprite, 454, 0, 0); //kSfxTNTDetRemote
                    evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong(kRemoteDelay), kCommandOff);
                } else {
                    actExplodeSprite((short) nSprite);
                }
                break;
            case kThingCrateFace:
            case kThingWallCrack:
                if (SetSpriteState(nSprite, pXSprite, 0) == 0) {
                    return;
                }
                actPostSprite(nSprite, kStatFree);
                break;
            case kThingTNTBarrel:
                if ((pSprite.getHitag() & 0x10) == 0) {
                    actExplodeSprite(nSprite);
                }
                break;
            case 418:
            case kThingTNTBundle:
            case kThingSprayBundle:
                actExplodeSprite(nSprite);
                break;
            case kThingTNTProx:
            case kGDXThingTNTProx:
                if (boardService.getSprite(nSprite).getStatnum() == kStatRespawn) {
                    break;
                }

                switch (evCommand) {
                    case kCommandOn:
                        sfxStart3DSound(pSprite, 451, 0, 0); //kSfxTNTDetProx
                        pXSprite.setProximity(true);
                        break;

                    case kCommandSpriteProximity:
                        if (pXSprite.getState() != 0)                        // don't trigger it if already triggered
                        {
                            break;
                        }

                        sfxStart3DSound(pSprite, 452, 0, 0); //kSfxTNTArmProx
                        evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong(kProxDelay), kCommandOff);
                        pXSprite.setState(1);
                        break;

                    default:
                        actExplodeSprite(nSprite);
                        break;
                }
                break;

            case kThingMachineGun:
                if (pXSprite.getHealth() > 0) {
                    if (evCommand == kCommandOn) {
                        if (SetSpriteState(nSprite, pXSprite, 1) != 0) {
                            seqSpawn(getSeq(kMGunOpen), SS_SPRITE, pSprite.getExtra(), callbacks[MGunOpenCallback]);

                            // data1 = max ammo (defaults to infinite)
                            // data2 = dynamic ammo if data1 > 0
                            if (pXSprite.getData1() > 0) {
                                pXSprite.setData2(pXSprite.getData1());
                            }
                        }
                    } else if (evCommand == kCommandOff) {
                        if (SetSpriteState(nSprite, pXSprite, 0) != 0) {
                            seqSpawn(getSeq(kMGunClose), SS_SPRITE, pSprite.getExtra(), null);
                        }
                    }
                }
                break;
            case kThingFlameTrap:
                if (evCommand == kCommandOn) {
                    if (SetSpriteState(nSprite, pXSprite, 1) != 0) {
                        seqSpawn(getSeq(kMGunOpen), SS_SPRITE, pSprite.getExtra(), null);
                        sfxStart3DSound(pSprite, 441, 0, 0);
                    }
                } else if (evCommand == kCommandOff) {
                    if (SetSpriteState(nSprite, pXSprite, 0) != 0) {
                        seqSpawn(getSeq(kMGunClose), SS_SPRITE, pSprite.getExtra(), null);
                        sfxKill3DSound(pSprite, -1, -1);
                    }
                }
                break;
            case kThingFallingRock:
                if (SetSpriteState(nSprite, pXSprite, 1) != 0) {
                    pSprite.setHitag(pSprite.getHitag() | (kAttrMove | kAttrGravity | kAttrFalling));
                }
                break;
            case kThingGibObject:
            case kThingExplodeObject:
            case 425:
            case 426:
            case kThingZombieHead:
                switch (evCommand) {
                    case kCommandOff:
                        if (SetSpriteState(nSprite, pXSprite, 0) == 1) {
                            actGibObject(pSprite, pXSprite);
                        }
                        break;
                    case kCommandOn:
                        if (SetSpriteState(nSprite, pXSprite, 1) == 1) {
                            actGibObject(pSprite, pXSprite);
                        }
                        break;
                    default:
                        if (SetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1) == 1) {
                            actGibObject(pSprite, pXSprite);
                        }
                        break;
                }
                break;
            case kTrapPoweredZap:
                switch (evCommand) {
                    case kCommandOff:
                        pXSprite.setState(0);
                        pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                        pSprite.setCstat(pSprite.getCstat() & ~kSpriteBlocking);
                        break;

                    case kCommandOn:
                        pXSprite.setState(1);
                        pSprite.setCstat(pSprite.getCstat() & ~kSpriteInvisible);
                        pSprite.setCstat(pSprite.getCstat() | kSpriteBlocking);
                        break;

                    case kCommandToggle:
                        pXSprite.setState(pXSprite.getState() ^ 1);
                        pSprite.setCstat(pSprite.getCstat() ^ kSpriteInvisible);
                        pSprite.setCstat(pSprite.getCstat() ^ kSpriteBlocking);
                        break;
                }
                break;

            case kThingHiddenExploder:
                if (evCommand != kCommandOn) {
                    pSprite.setCstat(pSprite.getCstat() & ~kSpriteInvisible);
                    actExplodeSprite(pSprite.getXvel());
                    return;
                }
                SetSpriteState(nSprite, pXSprite, 1);
                break;
            case kThingLifeLeech:
                LeechOperate(pSprite, pXSprite, evCommand);
                break;
            case kGDXThingCustomDudeLifeLeech:
                dudeLeechOperate(pSprite, pXSprite, evCommand);
                break;
            case kGDXSeqSpawner:
            case kGDXEffectSpawner:
                switch (evCommand) {
                    case kCommandOff:
                        if (pXSprite.getState() == 1) {
                            SetSpriteState(nSprite, pXSprite, 0);
                        }
                        break;
                    case kCommandOn:
                        checkEventList(nSprite, SS_SPRITE); // queue overflow protect
                        if (pXSprite.getState() == 0) {
                            SetSpriteState(nSprite, pXSprite, 1);
                        }
                        // go below
                    case kCommandRespawn:
                        if (pXSprite.getTxID() <= 0) {
                            if (pSprite.getLotag() == kGDXSeqSpawner) {
                                useSeqSpawnerGen(pXSprite, 3, pSprite.getXvel());
                            } else {
                                useEffectGen(pXSprite, pSprite);
                            }
                        } else {

                            switch (pXSprite.getCommand()) {
                                case kCommandLink:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste); // just send command to change properties
                                    break;
                                case kCommandUnlock:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand()); // send normal command first
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste);  // then send command to change properties
                                    break;
                                default:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste); // send first command to change properties
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand()); // then send normal command
                                    break;

                            }
                        }

                        if (pXSprite.getBusyTime() > 0) {
                            evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong((pXSprite.getBusyTime() + BiRandom(pXSprite.getData1())) * kTimerRate) / 10L, kCommandRespawn);
                        }

                        break;
                    default:
                        if (pXSprite.getState() == 0) {
                            evPost(nSprite, SS_SPRITE, 0, kCommandOn);
                        } else {
                            evPost(nSprite, SS_SPRITE, 0, kCommandOff);
                        }
                        break;
                }

                break;

            case kGDXWindGenerator:
                switch (evCommand) {
                    case kCommandOff:
                        stopWindOnSectors(pXSprite);
                        if (pXSprite.getState() == 1) {
                            SetSpriteState(nSprite, pXSprite, 0);
                        }
                        break;
                    case kCommandOn:
                        checkEventList(nSprite, SS_SPRITE); // queue overflow protect
                        if (pXSprite.getState() == 0) {
                            SetSpriteState(nSprite, pXSprite, 1);
                        }
                        // go below
                    case kCommandRespawn:
                        if (pXSprite.getTxID() <= 0) {
                            useSectorWindGen(pXSprite, boardService.getSector(pSprite.getSectnum()));
                        } else {

                            switch (pXSprite.getCommand()) {
                                case kCommandLink:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste); // just send command to change properties
                                    break;
                                case kCommandUnlock:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand()); // send normal command first
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste);  // then send command to change properties
                                    break;
                                default:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste); // send first command to change properties
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand()); // then send normal command
                                    break;

                            }

                        }

                        if (pXSprite.getBusyTime() > 0) {
                            evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong(pXSprite.getBusyTime()), kCommandRespawn);
                        }

                        break;
                    default:
                        if (pXSprite.getState() == 0) {
                            evPost(nSprite, SS_SPRITE, 0, kCommandOn);
                        } else {
                            evPost(nSprite, SS_SPRITE, 0, kCommandOff);
                        }
                        break;
                }

                break;
            case kGDXDudeTargetChanger:

                // this one is required if data4 of generator was dynamically changed
                // it turns monsters in normal idle state instead of genIdle, so they
                // not ignore the world.
                boolean activated = false;
                if (pXSprite.getDropMsg() == 3 && 3 != pXSprite.getData4()) {
                    activateDudes(pXSprite.getTxID());
                    activated = true;
                }

                switch (evCommand) {
                    case kCommandOff:
                        if (pXSprite.getData4() == 3 && !activated) {
                            activateDudes(pXSprite.getTxID());
                        }
                        if (pXSprite.getState() == 1) {
                            SetSpriteState(nSprite, pXSprite, 0);
                        }
                        break;
                    case kCommandOn:
                        checkEventList(nSprite, SS_SPRITE); // queue overflow protect
                        if (pXSprite.getState() == 0) {
                            SetSpriteState(nSprite, pXSprite, 1);
                        }
                        // go below
                    case kCommandRespawn:

                        if (pXSprite.getTxID() <= 0 || !getDudesForTargetChg(pXSprite)) {
                            evPost(nSprite, SS_SPRITE, 0, kCommandOff);
                            break;
                        } else {

                            switch (pXSprite.getCommand()) {
                                case kCommandLink:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste); // just send command to change properties
                                    break;
                                case kCommandUnlock:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand()); // send normal command first
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste);  // then send command to change properties
                                    break;
                                default:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste); // send first command to change properties
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand()); // then send normal command
                                    break;

                            }

                        }
                        if (pXSprite.getBusyTime() > 0) {
                            evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong(pXSprite.getBusyTime()), kCommandRespawn);
                        }
                        break;
                    default:
                        if (pXSprite.getState() == 0) {
                            evPost(nSprite, SS_SPRITE, 0, kCommandOn);
                        } else {
                            evPost(nSprite, SS_SPRITE, 0, kCommandOff);
                        }
                        break;
                }

                pXSprite.setDropMsg((short) pXSprite.getData4());
                break;

            case kGDXObjDataAccumulator:
                switch (evCommand) {
                    case kCommandOff:
                        if (pXSprite.getState() == 1) {
                            SetSpriteState(nSprite, pXSprite, 0);
                        }
                        break;
                    case kCommandOn:
                        checkEventList(nSprite, SS_SPRITE); // queue overflow protect
                        if (pXSprite.getState() == 0) {
                            SetSpriteState(nSprite, pXSprite, 1);
                        }
                        // go below
                    case kCommandRespawn:

                        // force OFF after *all* TX objects reach the goal value
                        if (pXSprite.getTxID() <= 0 || (pSprite.getHitag() == 0 && goalValueIsReached(pXSprite))) {
                            //System.err.println("GOING OFF");
                            evPost(nSprite, SS_SPRITE, 0, kCommandOff);
                            break;
                        }

                        if (pXSprite.getTxID() > 0 && pXSprite.getData1() > 0 && pXSprite.getData1() <= 4) {

                            switch (pXSprite.getCommand()) {
                                case kCommandLink:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste); // just send command to change properties
                                    break;
                                case kCommandUnlock:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand()); // send normal command first
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste);  // then send command to change properties
                                    break;
                                default:
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), kGDXCommandPaste); // send first command to change properties
                                    evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand()); // then send normal command
                                    break;
                            }

                            if (pXSprite.getBusyTime() > 0) {
                                evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong(pXSprite.getBusyTime()), kCommandRespawn);
                            }
                        }

                        break;
                    default:
                        if (pXSprite.getState() == 0) {
                            evPost(nSprite, SS_SPRITE, 0, kCommandOn);
                        } else {
                            evPost(nSprite, SS_SPRITE, 0, kCommandOff);
                        }
                        break;
                }
                break;
            case kGenEctoSkull: // can shot any projectile, default 307 (ectoskull)
                if (!IsOriginalDemo()) {
                    switch (evCommand) {
                        case kCommandOff:
                            if (pXSprite.getState() == 1) {
                                SetSpriteState(nSprite, pXSprite, 0);
                            }
                            break;
                        case kCommandOn:
                            checkEventList(nSprite, SS_SPRITE); // queue overflow protect
                            if (pXSprite.getState() == 0) {
                                SetSpriteState(nSprite, pXSprite, 1);
                            }
                            // go below
                        case kCommandRespawn:
                            ActivateGenerator(nSprite);
                            if (pXSprite.getTxID() != 0) {
                                evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand());
                            }
                            if (pXSprite.getBusyTime() > 0) {
                                evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong((pXSprite.getBusyTime() * kTimerRate)) / 10L, kCommandRespawn);
                            }
                            break;
                        default:
                            if (pXSprite.getState() == 0) {
                                evPost(nSprite, SS_SPRITE, 0, kCommandOn);
                            } else {
                                evPost(nSprite, SS_SPRITE, 0, kCommandOff);
                            }
                            break;
                    }
                }
                break;
            case kWeaponItemBase: // Random weapon
            case kAmmoItemRandom: // Random ammo
                if (!IsOriginalDemo()) {
                    switch (evCommand) {
                        case kCommandOff:
                            if (pXSprite.getState() == 1) {
                                SetSpriteState(nSprite, pXSprite, 0);
                            }
                            break;
                        case kCommandOn:
                            checkEventList(nSprite, SS_SPRITE); // queue overflow protect
                            if (pXSprite.getState() == 0) {
                                SetSpriteState(nSprite, pXSprite, 1);
                            }
                            // go below
                        case kCommandRespawn:
                            ActivateGenerator(nSprite);
                            if (pXSprite.getBusyTime() > 0) {
                                evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong((pXSprite.getBusyTime() * kTimerRate)) / 10L, kCommandRespawn);
                            }
                            break;
                        default:
                            if (pXSprite.getState() == 0) {
                                evPost(nSprite, SS_SPRITE, 0, kCommandOn);
                            } else {
                                evPost(nSprite, SS_SPRITE, 0, kCommandOff);
                            }
                            break;
                    }
                }
                break;
            case kGenTrigger:
            case kGenWaterDrip:
            case kGenBloodDrip:
            case kGenFireball:
            case kGenDart:
            case kGenSFX:
            case kGenBubble:
            case kGenMultiBubble:
                switch (evCommand) {
                    case kCommandOff:
                        SetSpriteState(nSprite, pXSprite, 0);
                        break;
                    case kCommandRespawn:
                        if (pSprite.getLotag() != kGenTrigger) {
                            ActivateGenerator(nSprite);
                        }
                        if (pXSprite.getTxID() != 0) {
                            evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand());
                        }
                        if (pXSprite.getBusyTime() > 0) {
                            evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong((pXSprite.getBusyTime() + BiRandom(pXSprite.getData1())) * kTimerRate) / 10L, kCommandRespawn);
                        }
                        break;
                    default:
                        if (pXSprite.getState() == 0) {
                            SetSpriteState(nSprite, pXSprite, 1);
                            evPost(nSprite, SS_SPRITE, 0, kCommandRespawn);
                        }
                        break;
                }
                break;

            case kGenPlayerSFX:
                if (pGameInfo.nGameType == 0) {
                    if (gMe == null || gMe.pXsprite == null || gMe.pXsprite.getHealth() == 0) {
                        return;
                    }
                    gMe.stayTime = 0;
                }

                sndStartSample(pXSprite.getData1(), -1, 1, false);
                break;
            default:
                switch (evCommand) {
                    case kCommandOff:
                        SetSpriteState(nSprite, pXSprite, 0);
                        break;

                    case kCommandOn:
                        SetSpriteState(nSprite, pXSprite, 1);
                        break;

                    default:
                        SetSpriteState(nSprite, pXSprite, pXSprite.getState() ^ 1);
                        break;
                }
                break;
        }
    }

    public static void OperateWall(int nWall, XWALL pXWall, int evCommand) {
        Wall pWall = boardService.getWall(nWall);

        // special handling for lock/unlock commands
        switch (evCommand) {
            case kCommandLock:
                pXWall.locked = 1;
                return;

            case kCommandUnlock:
                pXWall.locked = 0;
                return;

            case kCommandToggleLock:
                pXWall.locked ^= 1;
                return;
        }

        if (pWall.getLotag() == kWallGlass) {
            int action = 0;
            switch (evCommand) {
                case kCommandOff:
                    action = SetWallState(nWall, pXWall, 0);
                    break;

                case kCommandOn:
                case kCommandWallImpact:
                    action = SetWallState(nWall, pXWall, 1);
                    break;

                default:
                    action = (SetWallState(nWall, pXWall, pXWall.state ^ 1));
                    break;
            }
            if (action != 0) {
                Wall nextWall = null;
                if (pWall.getNextwall() >= 0) {
                    nextWall = boardService.getWall(pWall.getNextwall());
                }
                if (pXWall.state != 0) {
                    pWall.setCstat(pWall.getCstat() & ~(kWallBlocking | kWallHitscan));
                    if (nextWall != null) {
                        nextWall.setCstat(nextWall.getCstat() & ~(kWallBlocking | kWallHitscan));
                        pWall.setCstat(pWall.getCstat() & ~kWallMasked);
                        nextWall.setCstat(nextWall.getCstat() & ~kWallMasked);
                    }
                } else {
                    pWall.setCstat(pWall.getCstat() | 1);
                    if (pXWall.triggerVector) {
                        pWall.setCstat(pWall.getCstat() | kWallHitscan);
                    }
                    if (nextWall != null) {
                        nextWall.setCstat(nextWall.getCstat() & ~kWallBlocking);
                        if (pXWall.triggerVector) {
                            nextWall.setCstat(nextWall.getCstat() | kWallHitscan);
                        }
                        pWall.setCstat(pWall.getCstat() | kWallMasked);
                        nextWall.setCstat(nextWall.getCstat() | kWallMasked);
                    }
                }

                if (pXWall.state != 0) {
                    int spartType = ClipRange(pXWall.data, 0, 31);
                    if (spartType > 0) {
                        walGenerateGib(nWall, spartType, wallVel);
                    }
                }
            }
        } else {
            switch (evCommand) {
                case kCommandOff:
                    SetWallState(nWall, pXWall, 0);
                    break;

                case kCommandOn:
                    SetWallState(nWall, pXWall, 1);
                    break;

                default:
                    SetWallState(nWall, pXWall, pXWall.state ^ 1);
                    break;
            }
        }
    }

    public static void sfxSectorBusy(int nSector, int nBusy) {
        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
            BloodSprite pSprite = (BloodSprite) node.get();
            if (pSprite.getStatnum() == 0 && pSprite.getLotag() == kGenSectorSFX) {
                XSPRITE pXSprite = pSprite.getXSprite();
                if (pXSprite != null) {
                    if (nBusy != 0) {
                        if (pXSprite.getData3() != 0) {
                            sfxStart3DSound(pSprite, pXSprite.getData3(), 0, 0);
                        }
                    } else {
                        if (pXSprite.getData1() != 0) {
                            sfxStart3DSound(pSprite, pXSprite.getData1(), 0, 0);
                        }
                    }
                } else {
                    Console.out.println("Warning: Sector " + nSector + " has wrong SFX generator", OsdColor.RED);
                }
            }
        }
    }

    public static void sfxSectorBusy(int nSector, int soundId, int flags) {
        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
            Sprite pSprite = node.get();
            if (pSprite.getStatnum() == 0 && pSprite.getLotag() == kGenSectorSFX) {
                sfxStart3DSound(pSprite, soundId, 0, flags);
            }
        }
    }

    public static void sfxSectorStop(int nSector, int nBusy) {
        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
            Sprite pSprite = node.get();
            if (pSprite.getStatnum() == 0 && pSprite.getLotag() == kGenSectorSFX) {
                XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                if (pXSprite != null) {
                    if (nBusy != 0) {
                        if (pXSprite.getData2() != 0) {
                            sfxStart3DSound(pSprite, pXSprite.getData2(), 0, 0);
                        }
                    } else {
                        if (pXSprite.getData4() != 0) {
                            sfxStart3DSound(pSprite, pXSprite.getData4(), 0, 0);
                        }
                    }
                } else {
                    Console.out.println("Warning: Sector " + nSector + " has wrong SFX generator", OsdColor.RED);
                }
            }
        }
    }

    public static void TranslateSector(int nSector, int i0, int i1, int cx, int cy, int x0, int y0, int a0, int x1, int y1, int a1, boolean fAllWalls) {
        XSECTOR pXSector = xsector[boardService.getSector(nSector).getExtra()];
        int x, y;

        int vX = x1 - x0;
        int vY = y1 - y0;
        int vA = a1 - a0;

        int Xi0 = x0 + mulscale(i0, vX, 16);
        int Xi1 = x0 + mulscale(i1, vX, 16);
        int dX = Xi1 - Xi0;

        int Yi0 = y0 + mulscale(i0, vY, 16);
        int Yi1 = y0 + mulscale(i1, vY, 16);
        int dY = Yi1 - Yi0;

        int Ai0 = a0 + mulscale(i0, vA, 16);
        int Ai1 = a0 + mulscale(i1, vA, 16);
        int dA = Ai1 - Ai0;

        short nWall = boardService.getSector(nSector).getWallptr();
        if (fAllWalls) {
            for (int i = 0; i < boardService.getSector(nSector).getWallnum(); nWall++, i++) {
                x = (int) kwall[nWall].x;
                y = (int) kwall[nWall].y;

                if (Ai1 != 0) {
                    Point out = RotatePoint(x, y, Ai1, cx, cy);
                    x = out.getX();
                    y = out.getY();
                }
                // move vertex
                engine.dragpoint(nWall, x + Xi1 - cx, y + Yi1 - cy);
            }
        } else {
            for (int i = 0; i < boardService.getSector(nSector).getWallnum(); nWall++, i++) {
                short nWall2 = boardService.getWall(nWall).getPoint2();

                x = (int) kwall[nWall].x;
                y = (int) kwall[nWall].y;

                if ((boardService.getWall(nWall).getCstat() & kWallMoveForward) != 0) {
                    if (Ai1 != 0) {
                        Point out = RotatePoint(x, y, Ai1, cx, cy);
                        x = out.getX();
                        y = out.getY();
                    }

                    engine.dragpoint(nWall, x + Xi1 - cx, y + Yi1 - cy);

                    // move next vertex if not explicitly tagged
                    if ((boardService.getWall(nWall2).getCstat() & kWallMoveMask) == 0) {
                        x = (int) kwall[nWall2].x;
                        y = (int) kwall[nWall2].y;

                        if (Ai1 != 0) {
                            Point out = RotatePoint(x, y, Ai1, cx, cy);
                            x = out.getX();
                            y = out.getY();
                        }

                        engine.dragpoint(nWall2, x + Xi1 - cx, y + Yi1 - cy);
                    }
                } else if ((boardService.getWall(nWall).getCstat() & kWallMoveBackward) != 0) {
                    if (Ai1 != 0) {
                        Point out = RotatePoint(x, y, -Ai1, cx, cy);
                        x = out.getX();
                        y = out.getY();
                    }

                    engine.dragpoint(nWall, x - (Xi1 - cx), y - (Yi1 - cy));

                    // move next vertex if not explicitly tagged
                    if ((boardService.getWall(nWall2).getCstat() & kWallMoveMask) == 0) {
                        x = (int) kwall[nWall2].x;
                        y = (int) kwall[nWall2].y;

                        if (Ai1 != 0) {
                            Point out = RotatePoint(x, y, -Ai1, cx, cy);
                            x = out.getX();
                            y = out.getY();
                        }

                        engine.dragpoint(nWall2, x - (Xi1 - cx), y - (Yi1 - cy));
                    }
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            BloodSprite pSprite = (BloodSprite) node.get();

            // don't move markers if their hitag != 1
            if (pSprite.getStatnum() == kStatMarker || pSprite.getStatnum() == kStatMarker2) {
                if (IsOriginalDemo() || pSprite.getHitag() != 0x0001) {
                    continue;
                }
            }

            x = (int) pSprite.getKSprite().x;
            y = (int) pSprite.getKSprite().y;

            if ((boardService.getSprite(nSprite).getCstat() & kSpriteMoveForward) != 0) {
                if (Ai1 != 0) {
                    Point out = RotatePoint(x, y, Ai1, cx, cy);
                    x = out.getX();
                    y = out.getY();
                }

                viewBackupSpriteLoc(nSprite, pSprite);
                pSprite.setAng((short) ((pSprite.getAng() + dA) & kAngleMask));
                if (IsPlayerSprite(pSprite)) {
                    PLAYER pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];
                    pPlayer.ang = BClampAngle(pPlayer.ang + dA);
                }
                pSprite.setX(x + Xi1 - cx);
                pSprite.setY(y + Yi1 - cy);
            } else if ((boardService.getSprite(nSprite).getCstat() & kSpriteMoveReverse) != 0) {
                if (Ai1 != 0) {
                    Point out = RotatePoint(x, y, -Ai1, cx, cy);
                    x = out.getX();
                    y = out.getY();
                }

                viewBackupSpriteLoc(nSprite, pSprite);
                pSprite.setAng((short) ((pSprite.getAng() - dA) & kAngleMask));
                if (IsPlayerSprite(pSprite)) {
                    PLAYER pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];
                    pPlayer.ang = BClampAngle(pPlayer.ang + dA);
                }
                pSprite.setX(x - (Xi1 - cx));
                pSprite.setY(y - (Yi1 - cy));
            } else if (pXSector.Drag) {
                GetSpriteExtents(pSprite);
                int z = engine.getflorzofslope((short) nSector, pSprite.getX(), pSprite.getY());

                if ((pSprite.getCstat() & kSpriteRMask) == 0 && extents_zBot >= z) {
                    // translate relatively (degenerative)
                    if (dA != 0) {
                        Point out = RotatePoint(pSprite.getX(), pSprite.getY(), dA, Xi0, Yi0);
                        pSprite.setX(out.getX());
                        pSprite.setY(out.getY());
                    }

                    viewBackupSpriteLoc(nSprite, pSprite);
                    pSprite.setAng((short) ((pSprite.getAng() + dA) & kAngleMask));
                    if (IsPlayerSprite(pSprite)) {
                        PLAYER pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];
                        pPlayer.ang = BClampAngle(pPlayer.ang + dA);
                    }
                    pSprite.setX(pSprite.getX() + dX);
                    pSprite.setY(pSprite.getY() + dY);
                }
            }
        }
    }

    public static void zMotion(int nSector, XSECTOR pXSector, int busy, int opt) {
        Sector pSector = boardService.getSector(nSector);
        viewBackupSectorLoc(nSector, pSector);

        int floorZRange = pXSector.onFloorZ - pXSector.offFloorZ;
        if (floorZRange != 0) {
            int oldFloorZ = pSector.getFloorz();
            pSector.setFloorz(mulscale(floorZRange, Lin2Sin(busy, opt), 16) + pXSector.offFloorZ);
            secFloorZ[nSector] = pSector.getFloorz();
            floorVel[nSector] += (long) (pSector.getFloorz() - oldFloorZ) << 8;

            // adjust the z for any floor relative sprites or face sprites in the floor
            for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                int nSprite = node.getIndex();
                Sprite pSprite = node.get();
                if (!IsOriginalDemo() || pSprite.getStatnum() != kStatMarker && pSprite.getStatnum() != kStatMarker2) { //!lower water %% alignate region
                    GetSpriteExtents(pSprite);
                    if ((pSprite.getCstat() & kSpriteMoveFloor) != 0) {
                        viewBackupSpriteLoc(nSprite, pSprite);
                        pSprite.setZ(pSprite.getZ() + (pSector.getFloorz() - oldFloorZ));
                    } else if ((pSprite.getHitag() & kAttrGravity) != 0) {
                        pSprite.setHitag(pSprite.getHitag() | kAttrFalling);
                    } else if (oldFloorZ <= extents_zBot && (pSprite.getCstat() & kSpriteRMask) == 0) {
                        viewBackupSpriteLoc(nSprite, pSprite);
                        pSprite.setZ(pSprite.getZ() + (pSector.getFloorz() - oldFloorZ));
                    }
                }
            }
        }

        int ceilZRange = pXSector.onCeilZ - pXSector.offCeilZ;
        if (ceilZRange != 0) {
            int oldCeilZ = pSector.getCeilingz();
            pSector.setCeilingz(mulscale(ceilZRange, Lin2Sin(busy, opt), 16) + pXSector.offCeilZ);
            secCeilZ[nSector] = pSector.getCeilingz();
            ceilingVel[nSector] += (long) (pSector.getCeilingz() - oldCeilZ) << 8;

            // adjust the z for any floor relative sprites or face sprites in the floor
            for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                int nSprite = node.getIndex();
                Sprite pSprite = node.get();
                if (!IsOriginalDemo() || pSprite.getStatnum() != kStatMarker && pSprite.getStatnum() != kStatMarker2) { //!lower water %% alignate region//!lower water %% alignate region

                    GetSpriteExtents(pSprite);
                    if ((pSprite.getCstat() & kSpriteMoveCeiling) != 0) {
                        viewBackupSpriteLoc(nSprite, pSprite);
                        pSprite.setZ(pSprite.getZ() + (pSector.getCeilingz() - oldCeilZ));
                    }
                }
            }
        }
    }

    public static int GetHighestSprite(int nSector, int nStatus) {
        getHighestZ = boardService.getSector(nSector).getFloorz();
        int retSprite = -1;

        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            Sprite pSprite = node.get();
            if (pSprite.getStatnum() == nStatus || nStatus == kMaxStatus) {
                GetSpriteExtents(pSprite);

                if (pSprite.getZ() - extents_zTop < getHighestZ) {
                    getHighestZ = pSprite.getZ() - extents_zTop;
                    retSprite = nSprite;
                }
            }
        }
        return retSprite;
    }

    public static int GetSpriteRange(int nSector) {
        if (!(nSector >= 0 && nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector >= 0 && nSector < boardService.getSectorCount()");
        }
        int retSprite = -1;

        int ceilingz = boardService.getSector(nSector).getCeilingz();
        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
            int nSprite = node.getIndex();
            Sprite pSprite = node.get();
            if (pSprite.getStatnum() == kStatThing || pSprite.getStatnum() == kStatDude) {
                GetSpriteExtents(pSprite);

                if (ceilingz > extents_zTop) {
                    ceilingz = extents_zTop;
                    getRangepzTop = extents_zTop;
                    getRangepzBot = extents_zBot;
                    retSprite = nSprite;
                }
            }
        }

        return retSprite;
    }

    public static int VCrushBusy(int nSector, int nBusy) {
        System.out.println("crush");
        return 0;
    }

    public static int VSpriteBusy(int nSector, int nBusy) {
        if (!(nSector >= 0 && nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector >= 0 && nSector < boardService.getSectorCount()");
        }
        Sector pSector = boardService.getSector(nSector);
        int nXSector = pSector.getExtra();
        if (!(nXSector > 0 && nXSector < kMaxXSectors)) {
            throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
        }
        XSECTOR pXSector = xsector[nXSector];

        int opt = getWave(pXSector, nBusy);

        int floorZRange = pXSector.onFloorZ - pXSector.offFloorZ;
        if (floorZRange != 0) {
            // adjust the z for any floor relative sprites or face sprites in the floor
            for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                int nSprite = node.getIndex();
                BloodSprite pSprite = (BloodSprite) node.get();

                if ((pSprite.getCstat() & kSpriteMoveFloor) != 0) {
                    viewBackupSpriteLoc(nSprite, pSprite);
                    pSprite.setZ((int) (pSprite.getKSprite().z + mulscale(floorZRange, Lin2Sin(nBusy, opt), 16)));
                }
            }
        }

        int ceilZRange = pXSector.onCeilZ - pXSector.offCeilZ;
        if (ceilZRange != 0) {
            // adjust the z for any ceiling relative sprites or face sprites in the ceiling
            for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                int nSprite = node.getIndex();
                BloodSprite pSprite = (BloodSprite) node.get();

                if ((pSprite.getCstat() & kSpriteMoveCeiling) != 0) {
                    viewBackupSpriteLoc(nSprite, pSprite);
                    pSprite.setZ((int) (pSprite.getKSprite().z + mulscale(ceilZRange, Lin2Sin(nBusy, opt), 16)));
                }
            }
        }

        pXSector.busy = nBusy;

        if (pXSector.command == kCommandLink && pXSector.txID != 0) {
            evSend(nSector, SS_SECTOR, pXSector.txID, kCommandLink);
        }

        if ((nBusy & kFluxMask) == 0) {
            SetSectorState(nSector, pXSector, nBusy >> 16);
            sfxSectorStop(nSector, nBusy >> 16);
            return kBusyComplete;
        }

        return kBusyOk;
    }

    public static int VDoorBusy(int nSector, int nBusy) {
        if (!(nSector >= 0 && nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector >= 0 && nSector < boardService.getSectorCount()");
        }
        Sector pSector = boardService.getSector(nSector);
        int nXSector = pSector.getExtra();

        if (!(nXSector > 0 && nXSector < kMaxXSectors)) {
            throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
        }
        XSECTOR pXSector = xsector[nXSector];
        int nDelta;
        if (pXSector.state != 0) {
            nDelta = kMaxBusyValue / ClipLow(pXSector.busyTime[kCommandOn] * kTimerRate / 10, 1);
        } else {
            nDelta = -kMaxBusyValue / ClipLow(pXSector.busyTime[kCommandOff] * kTimerRate / 10, 1);
        }

        int nSprite = GetSpriteRange(nSector);
        if (nSprite >= 0) {
            BloodSprite pSprite = boardService.getSprite(nSprite);

            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
            if (pXSprite == null) {
                throw new AssertException("pXSprite != null");
            }

            if (nBusy < pXSector.busy) {
                if (pXSector.offCeilZ > pXSector.onCeilZ || pXSector.offFloorZ < pXSector.onFloorZ) {
                    if (pXSector.interruptable) {
                        if (pXSector.Crush) {
                            if (pXSprite.getHealth() == 0) {
                                return kBusyReverse;
                            }
                            actDamageSprite(nSprite, pSprite, kDamageFall, pXSector.data != 0 ? (pXSector.data << 4) : 8000);
                        }
                        nBusy = ClipRange(nBusy + kFrameTicks * (nDelta / 2), 0, kMaxBusyValue);
                    } else {
                        if (pXSector.Crush && pXSprite.getHealth() > 0) {
                            actDamageSprite(nSprite, pSprite, kDamageFall, pXSector.data != 0 ? (pXSector.data << 4) : 8000);
                            nBusy = ClipRange(nBusy + kFrameTicks * (nDelta / 2), 0, kMaxBusyValue);
                        }
                    }
                }
            } else if (nBusy > pXSector.busy) {
                if (pXSector.onCeilZ > pXSector.offCeilZ || pXSector.onFloorZ < pXSector.offFloorZ) {
                    if (pXSector.interruptable) {
                        if (pXSector.Crush) {
                            if (pXSprite.getHealth() == 0) {
                                return kBusyReverse;
                            }
                            actDamageSprite(nSprite, pSprite, kDamageFall, pXSector.data != 0 ? (pXSector.data << 4) : 8000);
                        }
                        nBusy = ClipRange(nBusy - kFrameTicks * (nDelta / 2), 0, kMaxBusyValue);
                    } else {
                        if (pXSector.Crush && pXSprite.getHealth() > 0) {
                            actDamageSprite(nSprite, pSprite, kDamageFall, pXSector.data != 0 ? (pXSector.data << 4) : 8000);
                            nBusy = ClipRange(nBusy - kFrameTicks * (nDelta / 2), 0, kMaxBusyValue);
                        }
                    }
                }
            }
        }

        zMotion(nSector, pXSector, nBusy, getWave(pXSector, nBusy));

        pXSector.busy = nBusy;

        if (pXSector.command == kCommandLink && pXSector.txID != 0) {
            evSend(nSector, SS_SECTOR, pXSector.txID, kCommandLink);
        }

        if ((nBusy & kFluxMask) == 0) {
            SetSectorState(nSector, pXSector, nBusy >> 16);
            sfxSectorStop(nSector, nBusy >> 16);
            return kBusyComplete;
        }

        return kBusyOk;
    }

    public static int HDoorBusy(int nSector, int nBusy) {

        if (!(nSector >= 0 && nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector >= 0 && nSector < boardService.getSectorCount()");
        }
        Sector pSector = boardService.getSector(nSector);
        int nXSector = pSector.getExtra();
        if (!(nXSector > 0 && nXSector < kMaxXSectors)) {
            throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
        }
        XSECTOR pXSector = xsector[nXSector];

        int opt = getWave(pXSector, nBusy);

        Sprite pMark0 = boardService.getSprite(pXSector.marker0);
        Sprite pMark1 = boardService.getSprite(pXSector.marker1);

        TranslateSector(nSector, Lin2Sin(pXSector.busy, opt), Lin2Sin(nBusy, opt), pMark0.getX(), pMark0.getY(), pMark0.getX(), pMark0.getY(), pMark0.getAng(), pMark1.getX(), pMark1.getY(), pMark1.getAng(), pSector.getLotag() == kSectorSlide);
        zMotion(nSector, pXSector, nBusy, opt);

        pXSector.busy = nBusy;

        if (pXSector.command == kCommandLink && pXSector.txID != 0) {
            evSend(nSector, SS_SECTOR, pXSector.txID, kCommandLink);
        }

        if ((nBusy & kFluxMask) == 0) {
            SetSectorState(nSector, pXSector, nBusy >> 16);
            sfxSectorStop(nSector, nBusy >> 16);
            return kBusyComplete;
        }

        return kBusyOk;

    }

    public static int RDoorBusy(int nSector, int nBusy) {
        if (!(nSector >= 0 && nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector >= 0 && nSector < boardService.getSectorCount()");
        }
        Sector pSector = boardService.getSector(nSector);
        int nXSector = pSector.getExtra();
        if (!(nXSector > 0 && nXSector < kMaxXSectors)) {
            throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
        }
        XSECTOR pXSector = xsector[nXSector];

        int opt = getWave(pXSector, nBusy);

        Sprite pMark = boardService.getSprite(pXSector.marker0);
        TranslateSector(nSector, Lin2Sin(pXSector.busy, opt), Lin2Sin(nBusy, opt), pMark.getX(), pMark.getY(), pMark.getX(), pMark.getY(), 0, pMark.getX(), pMark.getY(), pMark.getAng(), pSector.getLotag() == kSectorRotate);
        zMotion(nSector, pXSector, nBusy, opt);

        pXSector.busy = nBusy;

        if (pXSector.command == kCommandLink && pXSector.txID != 0) {
            evSend(nSector, SS_SECTOR, pXSector.txID, kCommandLink);
        }

        if ((nBusy & kFluxMask) == 0) {
            SetSectorState(nSector, pXSector, nBusy >> 16);
            sfxSectorStop(nSector, nBusy >> 16);
            return kBusyComplete;
        }

        return kBusyOk;
    }

    public static int SRDoorBusy(int nSector, int nBusy) {
        int a0, a1, opt, data;
        if (!(nSector >= 0 && nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector >= 0 && nSector < boardService.getSectorCount()");
        }
        Sector pSector = boardService.getSector(nSector);
        int nXSector = pSector.getExtra();
        if (!(nXSector > 0 && nXSector < kMaxXSectors)) {
            throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
        }
        XSECTOR pXSector = xsector[nXSector];

        Sprite pMark = boardService.getSprite(pXSector.marker0);
        opt = getWave(pXSector, nBusy);

        if (pXSector.busy >= nBusy) {
            a0 = pXSector.data - pMark.getAng();
            a1 = pXSector.data;
            data = pXSector.data - pMark.getAng();
        } else {
            a0 = pXSector.data;
            a1 = pXSector.data + pMark.getAng();
            data = pXSector.data + pMark.getAng();
        }

        TranslateSector(nSector, Lin2Sin(pXSector.busy, opt), Lin2Sin(nBusy, opt), pMark.getX(), pMark.getY(), pMark.getX(), pMark.getY(), a0, pMark.getX(), pMark.getY(), a1, true);

        pXSector.busy = nBusy;

        if (pXSector.command == kCommandLink && pXSector.txID != 0) {
            evSend(nSector, SS_SECTOR, pXSector.txID, kCommandLink);
        }

        if ((nBusy & kFluxMask) == 0) {
            SetSectorState(nSector, pXSector, nBusy >> 16);
            pXSector.data = data;
            sfxSectorStop(nSector, nBusy >> 16);
            return kBusyComplete;
        }

        return kBusyOk;
    }

    public static int PathSectorBusy(int nSector, int nBusy) {

        if (!(nSector >= 0 && nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector >= 0 && nSector < boardService.getSectorCount()");
        }
        Sector pSector = boardService.getSector(nSector);
        int nXSector = pSector.getExtra();
        if (!(nXSector > 0 && nXSector < kMaxXSectors)) {
            throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
        }
        XSECTOR pXSector = xsector[nXSector];

        Sprite pMarker0 = boardService.getSprite(pXSector.marker0);
        XSPRITE pXMarker0 = boardService.getXSprite(pMarker0.getExtra());
        Sprite pMarker1 = boardService.getSprite(pXSector.marker1);
        XSPRITE pXMarker1 = boardService.getXSprite(pMarker1.getExtra());
        Sprite pPivot = boardService.getSprite(secPath[nSector]);

        TranslateSector(nSector, Lin2Sin(pXSector.busy, pXMarker0.getWave()), Lin2Sin(nBusy, pXMarker0.getWave()), pPivot.getX(), pPivot.getY(), pMarker0.getX(), pMarker0.getY(), pMarker0.getAng(), pMarker1.getX(), pMarker1.getY(), pMarker1.getAng(), true);

        zMotion(nSector, pXSector, nBusy, pXMarker0.getWave());

        pXSector.busy = nBusy;

        if ((nBusy & kFluxMask) == 0) {
            evPost(nSector, SS_SECTOR, Integer.toUnsignedLong(pXMarker1.getWaitTime() * kTimerRate) / 10L, kCommandOn);
            pXSector.busy = 0;
            pXSector.state = 0;

            if (pXMarker0.getData4() != 0) {
                sfxSectorBusy(nSector, pXMarker0.getData4(), pXSector.busy);
            }
            pXSector.marker0 = pXSector.marker1;
            pXSector.data = pXMarker1.getData1();

            return kBusyComplete;
        }

        return kBusyOk;
    }

    public static void OperateDoor(int nSector, XSECTOR pXSector, int evCommand, int busyProc) {
        int nDelta;
        switch (evCommand) {
            case kCommandOff:
                if (pXSector.busy != 0) {
                    nDelta = -kMaxBusyValue / ClipLow(pXSector.busyTime[kCommandOff] * kTimerRate / 10, 1);
                    AddBusy(nSector, busyProc, nDelta);
                    sfxSectorBusy(nSector, 1);
                }
                break;

            case kCommandOn:
                if (pXSector.busy != kMaxBusyValue) {
                    nDelta = kMaxBusyValue / ClipLow(pXSector.busyTime[kCommandOn] * kTimerRate / 10, 1);
                    AddBusy(nSector, busyProc, nDelta);
                    sfxSectorBusy(nSector, 0);
                }
                break;

            default:

                if ((pXSector.busy & kFluxMask) != 0) {
                    if (pXSector.interruptable) {
                        ReverseBusy(nSector, busyProc);
                        pXSector.state = (short) ((pXSector.state == 0) ? 1 : 0);
                    }
                } else {
                    if (pXSector.state == 0) {
                        nDelta = kMaxBusyValue / ClipLow(pXSector.busyTime[kCommandOn] * kTimerRate / 10, 1);
                    } else {
                        nDelta = -kMaxBusyValue / ClipLow(pXSector.busyTime[kCommandOff] * kTimerRate / 10, 1);
                    }
                    AddBusy(nSector, busyProc, nDelta);
                    sfxSectorBusy(nSector, pXSector.state);
                }

                break;
        }
    }

    public static boolean TeleportDudes(int sectnum) {
        for (ListNode<Sprite> node = boardService.getSectNode(sectnum); node != null; node = node.getNext()) {
            if (node.get().getStatnum() == kStatDude) {
                return true;
            }
        }
        return false;
    }

    public static void TeleportDamage(int nSource, int nSector) {
        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
            BloodSprite pSprite = (BloodSprite) node.get();
            if (pSprite.getStatnum() == kStatDude) {
                actDamageSprite(nSource, pSprite, kDamageExplode, 4000);
            } else if (pSprite.getStatnum() == kStatThing) {
                actDamageSprite(nSource, pSprite, kDamageExplode, 8000);
            }
        }
    }

    public static void OperateTeleport(int nSector, XSECTOR pXSector) {
        if (!(nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector < boardService.getSectorCount() " + nSector + " < " + boardService.getSectorCount());
        }
        int nDest = pXSector.marker0;

        if (!boardService.isValidSprite(nDest)) {
            throw new AssertException("isValidSprite(nDest)");
        }
        Sprite pDest = boardService.getSprite(nDest);
        if (pDest.getStatnum() != kStatMarker) {
            throw new AssertException("pDest.statnum != kStatMarker");
        }
        if (pDest.getLotag() != kMarkerWarpDest) {
            throw new AssertException("pDest.type != kMarkerWarpDest");
        }
        if (!boardService.isValidSector(pDest.getSectnum())) {
            throw new AssertException("boardService.isValidSector(pDest.getSectnum())");
        }
        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
            final int i = node.getIndex();
            final BloodSprite pSprite = (BloodSprite) node.get();
            if (pSprite.getStatnum() != kStatDude) {
                continue;
            }

            PLAYER pPlayer = null;
            if (IsPlayerSprite(pSprite)) {
                pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];
            }

            if (pPlayer != null || !TeleportDudes(pDest.getSectnum())) {
//XXX			if ( (byte_1A9C79 & 2) == 0 )
                TeleportDamage(pXSector.data, pDest.getSectnum());

                pSprite.setX(pDest.getX());
                pSprite.setY(pDest.getY());
                pSprite.setZ(pSprite.getZ() + (boardService.getSector(pDest.getSectnum()).getFloorz() - boardService.getSector(nSector).getFloorz()));
                pSprite.setAng(pDest.getAng());
                engine.changespritesect((short) i, pDest.getSectnum());
                sfxStart3DSound(pDest, 201, -1, 0);
                pSprite.setVelocity(0, 0, 0);
                viewBackupSpriteLoc(i, pSprite);

                if (pPlayer != null) {
                    // fix backup position for interpolation to work properly
                    viewUpdatePlayerLoc(pPlayer);
                    gPlayer[pPlayer.nPlayer].weapOffdZ = gPlayer[pPlayer.nPlayer].viewOffdZ = 0;
                    gPlayer[pPlayer.nPlayer].ang = pSprite.getAng();
                }
            }
        }
    }

    public static void OperatePath(int nSector, XSECTOR pXSector, int evCommand) {
        if (!(nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector < boardService.getSectorCount()");
        }

        if (pXSector.marker0 == -1) {
            return;
        }

        Sprite pMarker = boardService.getSprite(pXSector.marker0);
        XSPRITE pXMarker = boardService.getXSprite(pMarker.getExtra());

        int nMarker2 = -1;
        for (ListNode<Sprite> node = boardService.getStatNode(kStatMarker2); node != null; node = node.getNext()) {
            Sprite pMarker2 = node.get();
            if (pMarker2.getLotag() == kPathMarker && boardService.getXSprite(pMarker2.getExtra()).getData1() == pXMarker.getData2()) {
                nMarker2 = node.getIndex();
                break;
            }
        }

        if (nMarker2 == -1) {
            System.err.println("Unable to find path marker with id " + pXMarker.getData2());
            return;
        }

        // by NoOne: trigger marker after it gets reached
        if (!IsOriginalDemo() && pXMarker.getState() != 1) {
            trTriggerSprite(pMarker.getXvel(), pXMarker, kCommandOn);
        }

        pXSector.marker1 = nMarker2;
        pXSector.onFloorZ = boardService.getSprite(nMarker2).getZ();
        pXSector.offFloorZ = pMarker.getZ();

        if (evCommand == kCommandOn) {
            pXSector.busy = 0;
            pXSector.state = 0;
            int nDelta = kMaxBusyValue / ClipLow(pXMarker.getBusyTime() * kTimerRate / 10, 1);

            AddBusy(nSector, 7, nDelta);
            if (pXMarker.getData3() != 0) {
                sfxSectorBusy(nSector, pXMarker.getData3(), 0);
            }
        }
    }

    public static void OperateSector(int nSector, XSECTOR pXSector, int evCommand) {
        Sector pSector = boardService.getSector(nSector);

        // special handling for lock/unlock commands
        switch (evCommand) {
            case kCommandLock:
                pXSector.locked = 1;
                break;

            case kCommandUnlock:
                pXSector.locked = 0;
                if (!IsOriginalDemo() && pSector.getLotag() == kSectorCounter) {
                    pXSector.state = 0;
                    evPostCallback(nSector, SS_SECTOR, 0, kCommandCounter);
                }
                break;

            case kCommandToggleLock:
                pXSector.locked ^= 1;
                if (!IsOriginalDemo() && (pSector.getLotag() == kSectorCounter && pXSector.locked != 1)) {
                    pXSector.state = 0;
                    evPostCallback(nSector, SS_SECTOR, 0, kCommandCounter);
                }
                break;

            case kCommandStopOff:
                pXSector.stopFlag[0] = true;
                break;

            case kCommandStopOn:
                pXSector.stopFlag[1] = true;
                break;

            case kCommandStopNext:
                pXSector.stopFlag[0] = true;
                pXSector.stopFlag[1] = true;
                break;

            default:
                switch (pSector.getLotag()) {
                    case kSectorZSprite:
                        OperateDoor(nSector, pXSector, evCommand, 1);
                        break;
                    case kSectorZMotion:
                        OperateDoor(nSector, pXSector, evCommand, 2);
                        break;
                    case kSectorSlideMarked:
                    case kSectorSlide:
                        OperateDoor(nSector, pXSector, evCommand, 3);
                        break;
                    case kSectorRotateMarked:
                    case kSectorRotate:
                        OperateDoor(nSector, pXSector, evCommand, 4);
                        break;
                    case kSectorRotateStep:
                        if (evCommand == kCommandOn) {
                            pXSector.busy = 0;
                            pXSector.state = 0;
                            int nDelta = kMaxBusyValue / ClipLow(pXSector.busyTime[kCommandOn] * kTimerRate / 10, 1);
                            AddBusy(nSector, 5, nDelta);
                            sfxSectorBusy(nSector, 0);
                        }

                        if (evCommand == kCommandOff) {
                            pXSector.busy = 65536;
                            pXSector.state = 1;
                            int nDelta = -kMaxBusyValue / ClipLow(pXSector.busyTime[kCommandOff] * kTimerRate / 10, 1);
                            AddBusy(nSector, 5, nDelta);
                            sfxSectorBusy(nSector, 0);
                        }
                        break;
                    case kSectorTeleport:
                        OperateTeleport(nSector, pXSector);
                        break;
                    case kSectorPath:
                        OperatePath(nSector, pXSector, evCommand);
                        break;
                    default:
                        if (pXSector.busyTime[0] != 0 || pXSector.busyTime[1] != 0) {
                            OperateDoor(nSector, pXSector, evCommand, 6);
                        } else {
                            if (evCommand == kCommandOff) {
                                SetSectorState(nSector, pXSector, 0);
                            } else if (evCommand == kCommandOn) {
                                SetSectorState(nSector, pXSector, 1);
                            } else {
                                SetSectorState(nSector, pXSector, pXSector.state ^ 1);
                            }
                        }

                        break;
                }
                break;
        }
    }

    public static void PathSectorInit(int nSector, XSECTOR pXSector) {
        if (!(nSector >= 0 && nSector < boardService.getSectorCount())) {
            throw new AssertException("nSector >= 0 && nSector < boardService.getSectorCount()");
        }

        int nMarker2 = -1;
        for (ListNode<Sprite> node = boardService.getStatNode(kStatMarker2); node != null; node = node.getNext()) {
            Sprite pSprite = node.get();
            if (pSprite.getLotag() == kPathMarker && boardService.getXSprite(pSprite.getExtra()).getData1() == pXSector.data) {
                nMarker2 = node.getIndex();
                break;
            }
        }

        pXSector.marker0 = nMarker2;
        secPath[nSector] = nMarker2;
        if (pXSector.state != 0) {
            evPost(nSector, SS_SECTOR, 0, kCommandOn);
        }
    }

    public static void LinkSector(int nSector, XSECTOR pXSector, int event) {
        Sector pSector = boardService.getSector(nSector);
        int nBusy = GetSourceBusy(event);

        switch (pSector.getLotag()) {
            case kSectorZSprite:
                VSpriteBusy(nSector, nBusy);
                break;

            case kSectorZMotion:
                VDoorBusy(nSector, nBusy);
                break;

            case kSectorSlide:
            case kSectorSlideMarked:
                HDoorBusy(nSector, nBusy);
                break;

            case kSectorRotate:
            case kSectorRotateMarked:
                RDoorBusy(nSector, nBusy);
                break;

			/*
				add link support for counter sectors
				they can now change necessary type and count of types
			*/
            case kSectorCounter:
                if (!IsOriginalDemo()) {
                    int nXIndex = boardService.getSector(getIndex(event)).getExtra();
                    if (!(nXIndex > 0 && nXIndex < kMaxXSectors)) {
                        throw new AssertException("nXIndex > 0 && nXIndex < kMaxXSectors");
                    }
                    pXSector.waitTime[1] = xsector[nXIndex].waitTime[1];
                    pXSector.data = xsector[nXIndex].data;
                    break;
                }
            default:
                pXSector.busy = nBusy;
                if ((nBusy & kFluxMask) == 0) {
                    SetSectorState(nSector, pXSector, nBusy >> 16);
                }
                break;
        }
    }

    public static void LinkSprite(int nSprite, XSPRITE pXSprite, int event) {
        BloodSprite pSprite = boardService.getSprite(nSprite);
        int nBusy = GetSourceBusy(event);
        //System.out.println("LINKING: "+pSprite.lotag);
        switch (pSprite.getLotag()) {
            // These can be linked too now
            case kMarkerLowerWater:
            case kMarkerUpperWater:
            case kMarkerUpperGoo:
            case kMarkerLowerGoo:
            case kMarkerUpperLink:
            case kMarkerLowerLink:
            case kMarkerUpperStack:
            case kMarkerLowerStack:
                if (getType(event) != SS_SPRITE) {
                    break;
                }
                Sprite pSprite2 = boardService.getSprite(getIndex(event));
                XSPRITE pXSprite2 = boardService.getXSprite(pSprite2.getExtra());
                if (pXSprite2 == null) {
                    break;
                }

                // Only lower to lower and upper to upper linking allowed.
                switch (pSprite.getLotag()) {
                    case kMarkerLowerWater:
                    case kMarkerLowerLink:
                    case kMarkerLowerStack:
                    case kMarkerLowerGoo:
                        switch (pSprite2.getLotag()) {
                            case kMarkerLowerWater:
                            case kMarkerLowerLink:
                            case kMarkerLowerStack:
                            case kMarkerLowerGoo:
                                break;
                            default:
                                return;
                        }
                        break;

                    case kMarkerUpperWater:
                    case kMarkerUpperLink:
                    case kMarkerUpperStack:
                    case kMarkerUpperGoo:
                        switch (pSprite2.getLotag()) {
                            case kMarkerUpperWater:
                            case kMarkerUpperLink:
                            case kMarkerUpperStack:
                            case kMarkerUpperGoo:
                                break;
                            default:
                                return;
                        }
                        break;
                }

                // swap link location
                /*short tmp1 = pXSprite2.data1;*/
                /*pXSprite2.data1 = pXSprite.data1;*/
                /*pXSprite.data1 = tmp1;*/

                if (pXSprite.getData2() < Globals.kMaxPalettes && pXSprite2.getData2() < Globals.kMaxPalettes) {
                    // swap palette
                    short tmp2 = pXSprite2.getData2();
                    pXSprite2.setData2(pXSprite.getData2());
                    pXSprite.setData2(tmp2);
                }


                // swap link type						// swap link owners (sectors)
                short tmp3 = pSprite2.getLotag();            //short tmp7 = pSprite2.owner;
                pSprite2.setLotag(pSprite.getLotag());            //pSprite2.owner = pSprite.owner;
                pSprite.setLotag(tmp3);                    //pSprite.owner = tmp7;

                // Deal with linked sectors
                Sector pSector = boardService.getSector(pSprite.getSectnum());
                Sector pSector2 = boardService.getSector(pSprite2.getSectnum());

                // Check for underwater
                XSECTOR pXSector = null;
                XSECTOR pXSector2 = null;
                if (pSector.getExtra() > 0) {
                    pXSector = xsector[pSector.getExtra()];
                }
                if (pSector2.getExtra() > 0) {
                    pXSector2 = xsector[pSector2.getExtra()];
                }
                if (pXSector != null && pXSector2 != null) {
                    boolean tmp6 = pXSector.Underwater;
                    pXSector.Underwater = pXSector2.Underwater;
                    pXSector2.Underwater = tmp6;
                }

                // optionally swap floorpic
                if (pXSprite2.getData3() == 1) {
                    short tmp4 = pSector.getFloorpicnum();
                    pSector.setFloorpicnum(pSector2.getFloorpicnum());
                    pSector2.setFloorpicnum(tmp4);
                }

                // optionally swap ceilpic
                if (pXSprite2.getData4() == 1) {
                    short tmp5 = pSector.getCeilingpicnum();
                    pSector.setCeilingpicnum(pSector2.getCeilingpicnum());
                    pSector2.setCeilingpicnum(tmp5);
                }
                break;
            // add a way to link between path markers, so path sectors can change their path.
            case kPathMarker:

                // only path marker to path marker link allowed
                if (getType(event) == SS_SPRITE) {
                    pXSprite2 = boardService.getXSprite(boardService.getSprite(getIndex(event)).getExtra());
                    // get master path marker data fields
                    pXSprite.setData1(pXSprite2.getData1());
                    pXSprite.setData2(pXSprite2.getData2());
                    pXSprite.setData3(pXSprite2.getData3()); // include soundId(?)

                    // get master path marker busy and wait times
                    pXSprite.setBusyTime(pXSprite2.getBusyTime());
                    pXSprite.setWaitTime(pXSprite2.getWaitTime());

                }
                break;

            case kSwitchCombination:
                // should only be linked to a master switch
                if (getType(event) == SS_SPRITE) {
                    pXSprite2 = boardService.getXSprite(boardService.getSprite(getIndex(event)).getExtra());
                    if (pXSprite2 == null) {
                        throw new AssertException("pXSprite2 != null");
                    }

                    // get master switch selection
                    pXSprite.setData1(pXSprite2.getData1());

                    // at right combination?
                    if (pXSprite.getData1() == pXSprite.getData2()) {
                        SetSpriteState(nSprite, pXSprite, 1);
                    } else {
                        SetSpriteState(nSprite, pXSprite, 0);
                    }
                }
                break;

            default:
                pXSprite.setBusy(nBusy);
                if ((nBusy & kFluxMask) == 0) {
                    SetSpriteState(nSprite, pXSprite, nBusy >> 16);
                }
                break;
        }
    }

    public static void LinkWall(int nWall, XWALL pXWall, int event) {
        Wall pWall = boardService.getWall(nWall);

        int nBusy = GetSourceBusy(event);

        pXWall.busy = nBusy;
        if ((nBusy & kFluxMask) == 0) {
            SetWallState(nWall, pXWall, nBusy >> 16);
        }
    }

    public static void trTriggerSector(int nSector, XSECTOR pXSector, int command) {
//		dprintf("TriggerSector: nSector=%i, command=%i\n", nSector, command);

        // bypass locked XObjects
        if (pXSector.locked != 0) {
            return;
        }

        // bypass triggered one-shots
        if (pXSector.isTriggered) {
            return;
        }

        if (pXSector.triggerOnce) {
            pXSector.isTriggered = true;
        }

        if (pXSector.decoupled) {
            if (pXSector.txID != 0) {
                evSend(nSector, SS_SECTOR, pXSector.txID, pXSector.command);
            }
        } else {
            // operate the sector
            OperateSector(nSector, pXSector, command);
        }
    }

    public static void pastePropertiesInObj(int type, int nDest, int event) {

        if (getType(event) != SS_SPRITE) {
            return;
        }
        Sprite pSource = boardService.getSprite(getIndex(event));
        XSPRITE pXSource = boardService.getXSprite(pSource.getExtra());

        if (pSource.getLotag() == kMarkerWarpDest) {
            /* - Allows teleport any sprite from any location to the source destination - */
            useTeleportTarget(pXSource, boardService.getSprite(nDest));
        } else if (pSource.getLotag() == kGDXSpriteDamager) {
            /* - damages xsprite via TX ID	- */
            if (boardService.getXSprite(boardService.getSprite(nDest).getExtra()).getHealth() > 0) {
                useSpriteDamager(pXSource, boardService.getSprite(nDest));
            }

        } else if (pSource.getLotag() == kGDXEffectSpawner) {
            /* - Effect Spawner can spawn any effect passed in data2 on it's or txID sprite - */
            if (pXSource.getData2() < 0 || pXSource.getData2() >= kFXMax) {
                return;
            } else if (type == SS_SPRITE) {
                useEffectGen(pXSource, boardService.getSprite(nDest));
            }
        } else if (pSource.getLotag() == kGDXSeqSpawner) {
            /* - SEQ Spawner takes data2 as SEQ ID and spawns it on it's or TX ID object - */
            if (pXSource.getData2() > 0 && !game.getCache().contains(pXSource.getData2(), seq)) {
                return;
            } else if (pXSource.getData2() >= 0) {
                useSeqSpawnerGen(pXSource, type, nDest);
            }
        } else if (pSource.getLotag() == kGDXWindGenerator) {
            /* - Wind generator via TX or for current sector if TX ID not specified - */
            /* - sprite.ang = sector wind direction									- */
            /* - data1 = randomness settings										- */
            /* - 		 0: no randomness											- */
            /* - 		 1: randomize wind velocity in data2						- */
            /* - 		 2: randomize current generator sprite angle				- */
            /* - 		 3: randomize both wind velocity and sprite angle			- */
            /* - data2 = wind velocity												- */
            /* - data3 = pan floor and ceiling settings								- */
            /* - 		 0: use sector pan settings									- */
            /* - 		 1: pan only floor											- */
            /* - 		 2: pan only ceiling										- */
            /* - 		 3: pan both												- */

            /* - hi-tag = 1: force windAlways and panAlways							- */

            if (type != SS_SECTOR) {
                return;
            }
            useSectorWindGen(pXSource, boardService.getSector(nDest));
        } else if (pSource.getLotag() == kGDXObjDataAccumulator) {
            /* - Object Data Accumulator allows to perform sum and sub operations in data fields of object - */
            /* - data1 = destination data index 															- */
            /* - data2 = min value																			- */
            /* - data3 = max value																			- */
            /* - data4 = step value																			- */
            /* - min > max = sub, 	min < max = sum															- */

            /* - hitag: 0 = force OFF if goal value was reached for all objects		     					- */
            /* - hitag: 2 = force swap min and max if goal value was reached								- */
            /* - hitag: 3 = force reset counter	                                           					- */

            long data = getDataFieldOfObject(type, nDest, pXSource.getData1());
            if (data == -65535) {
                throw new AssertException("Can't get data field #" + pXSource.getData1() + " of object #" + nDest + ", type " + type + "!\n");
            }

            if (pXSource.getData2() < pXSource.getData3()) {

                if (data < pXSource.getData2()) {
                    data = pXSource.getData2();
                }
                if (data > pXSource.getData3()) {
                    data = pXSource.getData3();
                }

                if ((data += pXSource.getData4()) >= pXSource.getData3()) {

                    switch (pSource.getHitag()) {
                        case 0:
                        case 1:
                            if (data > pXSource.getData3()) {
                                data = pXSource.getData3();
                            }
                            break;
                        case 2:
                            if (data > pXSource.getData3()) {
                                data = pXSource.getData3();
                            }
                            if (!goalValueIsReached(pXSource)) {
                                break;
                            }
                            short tmp = pXSource.getData3();
                            pXSource.setData3(pXSource.getData2());
                            pXSource.setData2(tmp);
                            break;
                        case 3:
                            if (data > pXSource.getData3()) {
                                data = pXSource.getData2();
                            }
                            break;
                    }
                }

                //System.err.println("++ "+pXSource.txID+": "+data);

            } else if (pXSource.getData2() > pXSource.getData3()) {

                if (data > pXSource.getData2()) {
                    data = pXSource.getData2();
                }
                if (data < pXSource.getData3()) {
                    data = pXSource.getData3();
                }

                if ((data -= pXSource.getData4()) <= pXSource.getData3()) {
                    switch (pSource.getHitag()) {
                        case 0:
                        case 1:
                            if (data < pXSource.getData3()) {
                                data = pXSource.getData3();
                            }
                            break;
                        case 2:
                            if (data < pXSource.getData3()) {
                                data = pXSource.getData3();
                            }
                            if (!goalValueIsReached(pXSource)) {
                                break;
                            }
                            short tmp = pXSource.getData3();
                            pXSource.setData3(pXSource.getData2());
                            pXSource.setData2(tmp);
                            break;
                        case 3:
                            if (data < pXSource.getData3()) {
                                data = pXSource.getData2();
                            }
                            break;
                    }
                }

                //System.err.println("-- "+pXSource.txID+": "+data);
            }

            setDataValueOfObject(type, nDest, pXSource.getData1(), (short) data);

        } else if (pSource.getLotag() == kGDXObjDataChanger) {

            /* - Data field changer via TX - */
            /* - data1 = sprite data1 / sector data / wall data	- */
            /* - data2 = sprite data2	- */
            /* - data3 = sprite data3	- */
            /* - data4 = sprite data4	- */

            /* - hitag: 1 = treat "ignore value" as actual value - */

            switch (type) {
                case SS_SECTOR:
                    if ((pSource.getHitag() & 1) != 0 || (pXSource.getData1() != -1 && pXSource.getData1() != 32767)) {
                        xsector[boardService.getSector(nDest).getExtra()].data = pXSource.getData1();
                    }
                    break;

                case SS_SPRITE:
                    XSPRITE pXSprite = boardService.getXSprite(boardService.getSprite(nDest).getExtra());
                    if ((pSource.getHitag() & 1) != 0 || (pXSource.getData1() != -1 && pXSource.getData1() != 32767)) {
                        pXSprite.setData1(pXSource.getData1());
                    }

                    if ((pSource.getHitag() & 1) != 0 || (pXSource.getData2() != -1 && pXSource.getData2() != 32767)) {
                        pXSprite.setData2(pXSource.getData2());
                    }

                    if ((pSource.getHitag() & 1) != 0 || (pXSource.getData3() != -1 && pXSource.getData3() != 32767)) {
                        pXSprite.setData3(pXSource.getData3());
                    }

                    if ((pSource.getHitag() & 1) != 0 || (pXSource.getData4() != -1 && pXSource.getData1() != 65535)) {
                        pXSprite.setData4(pXSource.getData4());
                    }
                    break;

                case SS_WALL:
                    if ((pSource.getHitag() & 1) != 0 || (pXSource.getData1() != -1 && pXSource.getData1() != 32767)) {
                        xwall[boardService.getWall(nDest).getExtra()].data = pXSource.getData1();
                    }
                    break;
            }

        } else if (pSource.getLotag() == kGDXSectorFXChanger) {

            /* - FX Wave changer for sector via TX - */
            /* - data1 = Wave 	- */
            /* - data2 = Amplitude	- */
            /* - data3 = Freq	- */
            /* - data4 = Phase	- */

            if (type != SS_SECTOR) {
                return;
            }
            XSECTOR pXSector = xsector[boardService.getSector(nDest).getExtra()];

            if (valueIsBetween(pXSource.getData1(), -1, 32767)) {
                pXSector.wave = (pXSource.getData1() > 11) ? 11 : pXSource.getData1();
            }

            int oldAmplitude = pXSector.amplitude;
            if (pXSource.getData2() >= 0) {
                pXSector.amplitude = (pXSource.getData2() > 127) ? 127 : pXSource.getData2();
            } else if (pXSource.getData2() < -1) {
                pXSector.amplitude = (pXSource.getData2() < -127) ? -127 : pXSource.getData2();
            }

            if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                pXSector.freq = (pXSource.getData3() > 255) ? 255 : pXSource.getData3();
            }

            if (valueIsBetween(pXSource.getData4(), -1, 65535)) {
                pXSector.phase = (pXSource.getData4() > 255) ? 255 : (short) pXSource.getData4();
            }

            // force shadeAlways
            if ((pSource.getHitag() & 1) != 0) {
                pXSector.shadeAlways = true;
            }

            // add to shadeList if amplitude was set to 0 previously
            if (oldAmplitude == 0 && pXSector.amplitude != 0) {
                for (int i = shadeCount; i >= 0; i--) {
                    if (shadeList[i] == boardService.getSector(nDest).getExtra()) {
                        break;
                    } else if (i == 0) {
                        shadeList[shadeCount++] = boardService.getSector(nDest).getExtra();
                    }
                }
            }


        } else if (pSource.getLotag() == kGDXDudeTargetChanger) {

            /* - Target changer for dudes via TX																		- */

            /* - data1 = target dude data1 value (can be zero)															- */
            /* 			 666: attack everyone, even if data1 id does not fit, except mates (if any)						- */
            /* - data2 = 0: AI deathmatch mode																			- */
            /*			 1: AI team deathmatch mode																		- */
            /* - data3 = 0: do not force target to fight dude back and *do not* awake some inactive monsters in sight	- */
            /* 			 1: force target to fight dude back	and *do not* awake some inactive monsters in sight			- */
            /*			 2: force target to fight dude back	and awake some inactive monsters in sight					- */
            /* - data4 = 0: do not ignore player(s) (even if enough targets in sight)									- */
            /*			 1: try to ignore player(s) (while enough targets in sight)										- */
            /*			 2: ignore player(s) (attack only when no targets in sight at all)								- */
            /*			 3: go to idle state if no targets in sight and ignore player(s) always							- */
            /*			 4: follow player(s) when no targets in sight, attack targets if any in sight					- */

            if (type != SS_SPRITE || !IsDudeSprite(boardService.getSprite(nDest)) || boardService.getSprite(nDest).getStatnum() != kStatDude) {
                return;
            }

            BloodSprite pSprite = boardService.getSprite(nDest);
            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
            DudeExtra pXDude = pXSprite.getDudeExtra();
            BloodSprite pTarget = null;
            XSPRITE pXTarget = null;
            int receiveHp = 33 + Gameutils.Random(33);
            DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
            int matesPerEnemy = 1;

            // dude is burning?
            if (pXSprite.getBurnTime() > 0) {
                if (!IsBurningDude(pSprite)) {
                    Sprite pBurnSource = boardService.getSprite(pXSprite.getBurnSource());
                    if (pBurnSource != null) {
                        XSPRITE pXBurn = boardService.getXSprite(pBurnSource.getExtra());
                        if (pXBurn != null) {
                            if (pXSource.getData2() == 1 && isMateOf(pXSprite, pXBurn)) {
                                pXSprite.setBurnTime(0);

                                // heal dude a bit in case of friendly fire
                                if (pXSprite.getData4() > 0 && pXSprite.getHealth() < pXSprite.getData4()) {
                                    actHealDude(pXSprite, receiveHp, pXSprite.getData4());
                                } else if (pXSprite.getHealth() < pDudeInfo.startHealth) {
                                    actHealDude(pXSprite, receiveHp, pDudeInfo.startHealth);
                                }

                                //aiClock[pSprite.xvel] = gFrameClock;

                            } else if (pBurnSource.getXvel() == pSprite.getXvel() || pXBurn.getHealth() <= 0) {
                                pXSprite.setBurnTime(0);
                            }
                        }
                    }
                } else {
                    actKillSprite(pSource.getXvel(), pSprite, 0, 65535);
                    return;
                }
            }

            Sprite pPlayer = targetIsPlayer(pXSprite);
            // special handling for player(s) if target changer data4 > 2.
            if (pPlayer != null && pXSprite.getAIState() != genIdle) {
                if (pXSource.getData4() == 3) {
                    aiSetTarget(pXSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ());
                    aiNewState(pSprite, pXSprite, genIdle);
                    pXDude.setActive(false);
                    if (pSprite.getLotag() == 254) {
                        removeLeech(leechIsDropped(pSprite));
                    }
                } else if (pXSource.getData4() == 4) {
                    aiSetTarget(pXSprite, pPlayer.getX(), pPlayer.getY(), pPlayer.getZ());
                    if (pSprite.getLotag() == 254) {
                        removeLeech(leechIsDropped(pSprite));
                    }
                }
            }

            int maxAlarmDudes = 8 + Random(8);
            pTarget = boardService.getSprite(pXSprite.getTarget());
            if (pTarget != null && (pXTarget = boardService.getXSprite(pTarget.getExtra())) != null && pPlayer == null) {
                if (unitCanFly(pSprite) && isMeleeUnit(pTarget) && !unitCanFly(pTarget)) {
                    pSprite.setHitag(pSprite.getHitag() | 0x0002);
                } else if (unitCanFly(pSprite)) {
                    pSprite.setHitag(pSprite.getHitag() & ~0x0002);
                }

                if (!IsDudeSprite(pTarget) || pXTarget.getHealth() < 1 || !dudeCanSeeTarget(pXSprite, pDudeInfo, pTarget)) {
                    aiSetTarget(pXSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ());

                    // dude attack or attacked by target that does not fit by data id?
                } else if (pXSource.getData1() != 666 && pXTarget.getData1() != pXSource.getData1()) {
                    if (affectedByTargetChg(pXTarget) != null) {

                        // force stop attack target
                        aiSetTarget(pXSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ());
                        if (pXSprite.getBurnSource() == pTarget.getXvel()) {
                            pXSprite.setBurnTime(0);
                            pXSprite.setBurnSource(-1);
                        }

                        // force stop attack dude
                        aiSetTarget(pXTarget, pTarget.getX(), pTarget.getY(), pTarget.getZ());
                        if (pXTarget.getBurnSource() == pSprite.getXvel()) {
                            pXTarget.setBurnTime(0);
                            pXTarget.setBurnSource(-1);
                        }
                    }
                    // instantly kill annoying spiders, rats, hands etc if dude is big enough
                } else if (isAnnoyingUnit(pTarget) && !isAnnoyingUnit(pSprite) && engine.getTile(pSprite.getPicnum()).getHeight() >= 60 && getTargetDist(pSprite, pDudeInfo, pTarget) < 2) {

                    actKillSprite(pSource.getXvel(), pTarget, 0, 65535);
                    aiSetTarget(pXSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ());

                } else if (pXSource.getData2() == 1 && isMateOf(pXSprite, pXTarget)) {
                    Sprite pMate = pTarget;
                    XSPRITE pXMate = pXTarget;

                    // heal dude
                    if (pXSprite.getData4() > 0 && pXSprite.getHealth() < pXSprite.getData4()) {
                        actHealDude(pXSprite, receiveHp, pXSprite.getData4());
                    } else if (pXSprite.getHealth() < pDudeInfo.startHealth) {
                        actHealDude(pXSprite, receiveHp, pDudeInfo.startHealth);
                    }

                    // heal mate
                    if (pXMate.getData4() > 0 && pXMate.getHealth() < pXMate.getData4()) {
                        actHealDude(pXMate, receiveHp, pXMate.getData4());
                    } else {
                        DudeInfo pTDudeInfo = dudeInfo[pMate.getLotag() - kDudeBase];
                        if (pXMate.getHealth() < pTDudeInfo.startHealth) {
                            actHealDude(pXMate, receiveHp, pTDudeInfo.startHealth);
                        }
                    }

                    if (pXMate.getTarget() > -1 && boardService.getSprite(pXMate.getTarget()).getExtra() >= 0) {
                        pTarget = boardService.getSprite(pXMate.getTarget());
                        // force mate stop attack dude, if he does
                        if (pXMate.getTarget() == pSprite.getXvel()) {
                            aiSetTarget(pXMate, pMate.getX(), pMate.getY(), pMate.getZ());
                        } else if (!isMateOf(pXSprite, boardService.getXSprite(pTarget.getExtra()))) {
                            // force dude to attack same target that mate have
                            //System.out.println("---------------> MATE CHANGES TARGET OF DUDE TO: "+pTarget.lotag);
                            aiSetTarget(pXSprite, pTarget.getXvel());
                            return;

                        } else {
                            // force mate to stop attack another mate
                            aiSetTarget(pXMate, pMate.getX(), pMate.getY(), pMate.getZ());
                        }
                    }

                    // force dude stop attack mate, if target was not changed previously
                    if (pXSprite.getTarget() == pMate.getXvel()) {
                        aiSetTarget(pXSprite, pSprite.getX(), pSprite.getY(), pSprite.getZ());
                    }

                    // check if targets aims player then force this target to fight with dude
                } else if (targetIsPlayer(pXTarget) != null) {
                    //System.err.println("!!!!!!!! TARGET AIMS TO PLAYER");
                    aiSetTarget(pXTarget, pSprite.getXvel());
                }

                int mDist = 3;
                if (isMeleeUnit(pSprite)) {
                    mDist = 2;
                }
                //int mDist = 2;
                if (pXSprite.getTarget() >= 0 && getTargetDist(pSprite, pDudeInfo, boardService.getSprite(pXSprite.getTarget())) < mDist) {
                    //System.out.println("====> CONTINUING FIGHT WITH TARGET "+boardService.getSprite(pXSprite.target).lotag);
                    //aiSetTarget(pXSprite,boardService.getSprite(pXSprite.target).xvel);
                    if (!isActive(pXSprite)) {
                        aiActivateDude(pSprite, pXSprite);
                    }
                    return;
                    // lets try to look for target that fits better by distance
                } else if ((gFrameClock & 256) != 0 && (pXSprite.getTarget() < 0 || getTargetDist(pSprite, pDudeInfo, pTarget) >= mDist)) {
                    pTarget = getTargetInRange(pSprite, 0, mDist, pXSource.getData1(), pXSource.getData2());
                    if (pTarget != null) {
                        pXTarget = boardService.getXSprite(pTarget.getExtra());

                        // Make prev target not aim in dude
                        if (pXSprite.getTarget() > -1) {
                            //System.out.println(" => FOUND BETTER TARGET");
                            Sprite prvTarget = boardService.getSprite(pXSprite.getTarget());
                            aiSetTarget(boardService.getXSprite(prvTarget.getExtra()), prvTarget.getX(), prvTarget.getY(), prvTarget.getZ());
                            if (!isActive(pXTarget)) {
                                aiActivateDude(pTarget, pXTarget);
                            }
                        }

                        // Change target for dude
                        aiSetTarget(pXSprite, pTarget.getXvel());
                        if (!isActive(pXSprite)) {
                            aiActivateDude(pSprite, pXSprite);
                        }

                        // ...and change target of target to dude to force it fight
                        if (pXSource.getData3() > 0 && pXTarget.getTarget() != pSprite.getXvel()) {
                            aiSetTarget(pXTarget, pSprite.getXvel());
                            if (!isActive(pXTarget)) {
                                aiActivateDude(pTarget, pXTarget);
                            }
                        }
                        //System.out.println(" => FOUND BETTER TARGET");
                        return;
                    }
                }
            }

            if ((pTarget == null || pPlayer != null) && (gFrameClock & 32) != 0) {
                // try find first target that dude can see
                for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
                    pTarget = (BloodSprite) node.get();
                    pXTarget = boardService.getXSprite(pTarget.getExtra());

                    if (pXTarget.getTarget() == pSprite.getXvel()) {
                        //System.out.println(" ====> TARGET ALREADY AIMING DUDE");
                        aiSetTarget(pXSprite, pTarget.getXvel());
                        return;
                    }

                    // skip non-dudes and players
                    if (!IsDudeSprite(pTarget) || (IsPlayerSprite(pTarget) && pXSource.getData4() > 0)) {
                        continue;
                    }
                    // avoid self aiming, those who dude can't see, and those who dude own
                    else if (!dudeCanSeeTarget(pXSprite, pDudeInfo, pTarget) || pSprite.getXvel() == pTarget.getXvel() || pTarget.getOwner() == pSprite.getXvel()) {
                        continue;
                        // if Target Changer have data1 = 666, everyone can be target, except AI team mates.
                    } else if (pXSource.getData1() != 666) {
                        if (pXSource.getData1() != pXTarget.getData1()) {
                            continue;
                        }
                    }

                    // don't attack immortal, burning dudes and mates
                    if (IsBurningDude(pTarget) || isImmortalDude(pTarget) || (pXSource.getData2() == 1 && isMateOf(pXSprite, pXTarget))) {
                        continue;
                    }

                    if (pXSource.getData2() == 0 || (pXSource.getData2() == 1 && !isMatesHaveSameTarget(pXSprite, pTarget, matesPerEnemy))) {

                        // Change target for dude
                        aiSetTarget(pXSprite, pTarget.getXvel());
                        if (!isActive(pXSprite)) {
                            aiActivateDude(pSprite, pXSprite);
                        }

                        // ...and change target of target to dude to force it fight
                        if (pXSource.getData3() > 0 && pXTarget.getTarget() != pSprite.getXvel()) {
                            aiSetTarget(pXTarget, pSprite.getXvel());
                            if (!isActive(pXTarget)) {
                                aiActivateDude(pTarget, pXTarget);
                            }

                            if (pXSource.getData3() == 2) {
                                disturbDudesInSight(pTarget, maxAlarmDudes);
                            }
                        }
                        return;
                    }
                    break;
                }
            }

            // got no target - let's ask mates if they have targets
            if ((pXSprite.getTarget() < 0 || pPlayer != null) && pXSource.getData2() == 1 && (gFrameClock & 64) != 0) {
                //System.out.println("/////// REQUESTING TARGETS");
                Sprite pMateTarget;
                XSPRITE pXMateTarget;
                if ((pMateTarget = getMateTargets(pXSprite)) != null && (pXMateTarget = boardService.getXSprite(pMateTarget.getExtra())) != null) {
                    if (dudeCanSeeTarget(pXSprite, pDudeInfo, pMateTarget)) {
                        //System.out.println("/////// GOING TO HELP MATE");
                        if (pXMateTarget.getTarget() < 0) {
                            aiSetTarget(pXMateTarget, pSprite.getXvel());
                            if (IsDudeSprite(pMateTarget) && !isActive(pXMateTarget)) {
                                aiActivateDude((BloodSprite) pMateTarget, pXMateTarget);
                            }
                        }

                        aiSetTarget(pXSprite, pMateTarget.getXvel());
                        if (!isActive(pXSprite)) {
                            aiActivateDude(pSprite, pXSprite);
                        }

                        // try walk in mate direction in case if not see the target
                    } else if (pXMateTarget.getTarget() >= 0 && dudeCanSeeTarget(pXSprite, pDudeInfo, boardService.getSprite(pXMateTarget.getTarget()))) {
                        //System.out.println("?????????? CAN'T SEE THE TARGET");
                        Sprite pMate = boardService.getSprite(pXMateTarget.getTarget());
                        pXSprite.setTarget(pMateTarget.getXvel());
                        //pXSprite.target = -1;
                        pXSprite.setTargetX(pMate.getX());
                        pXSprite.setTargetY(pMate.getY());
                        pXSprite.setTargetZ(pMate.getZ());
                        if (!isActive(pXSprite)) {
                            aiActivateDude(pSprite, pXSprite);
                        }
                    }
                }
            }

        } else if (pSource.getLotag() == kGDXObjSizeChanger) {

            /* - size and pan changer of sprite/wall/sector via TX ID 	- */
            /* - data1 = sprite xrepeat / wall xrepeat / floor xpan 	- */
            /* - data2 = sprite yrepeat / wall yrepeat / floor ypan 	- */
            /* - data3 = sprite xoffset / wall xoffset / ceil xpan 		- */
            /* - data3 = sprite yoffset / wall yoffset / ceil ypan 		- */

            if (pXSource.getData1() > 255) {
                pXSource.setData1(255);
            }
            if (pXSource.getData2() > 255) {
                pXSource.setData2(255);
            }
            if (pXSource.getData3() > 255) {
                pXSource.setData3(255);
            }
            if (valueIsBetween(pXSource.getData4(), 255, 65535)) {
                pXSource.setData4(255);
            }

            switch (type) {
                case SS_SECTOR:
                    if (valueIsBetween(pXSource.getData1(), -1, 32767)) {
                        boardService.getSector(nDest).setFloorxpanning(pXSource.getData1());
                    }

                    if (valueIsBetween(pXSource.getData2(), -1, 32767)) {
                        boardService.getSector(nDest).setFloorypanning(pXSource.getData2());
                    }

                    if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                        boardService.getSector(nDest).setCeilingxpanning(pXSource.getData3());
                    }

                    if (valueIsBetween(pXSource.getData4(), -1, 65535)) {
                        boardService.getSector(nDest).setCeilingypanning((short) pXSource.getData4());
                    }
                    break;

                case SS_SPRITE:
                    if (valueIsBetween(pXSource.getData1(), -1, 32767)) {
                        if (pXSource.getData1() < 1) {
                            boardService.getSprite(nDest).setXrepeat(0);
                        } else {
                            boardService.getSprite(nDest).setXrepeat(pXSource.getData1());
                        }
                    }

                    if (valueIsBetween(pXSource.getData2(), -1, 32767)) {
                        if (pXSource.getData2() < 1) {
                            boardService.getSprite(nDest).setYrepeat(0);
                        } else {
                            boardService.getSprite(nDest).setYrepeat(pXSource.getData2());
                        }
                    }

                    if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                        boardService.getSprite(nDest).setXoffset(pXSource.getData3());
                    }

                    if (valueIsBetween(pXSource.getData4(), -1, 65535)) {
                        boardService.getSprite(nDest).setYoffset((short) pXSource.getData4());
                    }

                    break;

                case SS_WALL:
                    if (valueIsBetween(pXSource.getData1(), -1, 32767)) {
                        boardService.getWall(nDest).setXrepeat(pXSource.getData1());
                    }

                    if (valueIsBetween(pXSource.getData2(), -1, 32767)) {
                        boardService.getWall(nDest).setYrepeat(pXSource.getData2());
                    }

                    if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                        boardService.getWall(nDest).setXpanning(pXSource.getData3());
                    }

                    if (valueIsBetween(pXSource.getData4(), -1, 65535)) {
                        boardService.getWall(nDest).setYpanning((short) pXSource.getData4());
                    }

                    break;
            }

        } else if (pSource.getLotag() == kGDXObjPicnumChanger) {

            /* - picnum changer can change picnum of sprite/wall/sector via TX ID - */
            /* - data1 = sprite pic / wall pic / sector floor pic 				 - */
            /* - data3 = sprite pal / wall pal / sector floor pic				- */

            switch (type) {
                case SS_SECTOR:
                    if (valueIsBetween(pXSource.getData1(), -1, 32767)) {
                        boardService.getSector(nDest).setFloorpicnum(pXSource.getData1());
                    }

                    if (valueIsBetween(pXSource.getData2(), -1, 32767)) {
                        boardService.getSector(nDest).setCeilingpicnum(pXSource.getData2());
                    }

                    XSECTOR pXSector = xsector[boardService.getSector(nDest).getExtra()];
                    if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                        boardService.getSector(nDest).setFloorpal(pXSource.getData3());
                        if ((pSource.getHitag() & 0x0001) != 0) {
                            pXSector.floorpal = pXSource.getData3();
                        }
                    }

                    if (valueIsBetween(pXSource.getData4(), -1, 65535)) {
                        boardService.getSector(nDest).setCeilingpal((short) pXSource.getData4());
                        if ((pSource.getHitag() & 0x0001) != 0) {
                            pXSector.ceilpal = (short) pXSource.getData4();
                        }
                    }
                    break;

                case SS_SPRITE:
                    if (valueIsBetween(pXSource.getData1(), -1, 32767)) {
                        boardService.getSprite(nDest).setPicnum(pXSource.getData1());
                    }

                    if (pXSource.getData2() >= 0) {
                        boardService.getSprite(nDest).setShade((byte) ((pXSource.getData2() > 127) ? 127 : pXSource.getData2()));
                    } else if (pXSource.getData2() < -1) {
                        boardService.getSprite(nDest).setShade((byte) ((pXSource.getData2() < -127) ? -127 : pXSource.getData2()));
                    }

                    if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                        boardService.getSprite(nDest).setPal(pXSource.getData3());
                    }

                    break;

                case SS_WALL:
                    if (valueIsBetween(pXSource.getData1(), -1, 32767)) {
                        boardService.getWall(nDest).setPicnum(pXSource.getData1());
                    }

                    if (valueIsBetween(pXSource.getData2(), -1, 32767)) {
                        boardService.getWall(nDest).setOverpicnum(pXSource.getData2());
                    }

                    if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                        boardService.getWall(nDest).setPal(pXSource.getData3());
                    }

                    break;
            }

        } else if (pSource.getLotag() == kGDXObjPropertiesChanger) {

            /* - properties changer can change various properties of sprite/wall/sector via TX ID 	- */
            /* - data1 = sector depth level 														- */
            /* - data2 = sector visibility 															- */
            /* - data3 = sector ceiling cstat / sprite / wall hitag 								- */
            /* - data4 = sector floor / sprite / wall cstat 										- */

            switch (type) {
                case SS_SECTOR:
                    XSECTOR pXSector = xsector[boardService.getSector(nDest).getExtra()];
                    switch (pXSource.getData1()) {
                        case 0:
                            pXSector.Underwater = false;
                            break;
                        case 1:
                            pXSector.Underwater = true;
                            break;
                        case 2:
                            pXSector.Depth = 0;
                            break;
                        case 3:
                            pXSector.Depth = 1;
                            break;
                        case 4:
                            pXSector.Depth = 2;
                            break;
                        case 5:
                            pXSector.Depth = 3;
                            break;
                        case 6:
                            pXSector.Depth = 4;
                            break;
                        case 7:
                            pXSector.Depth = 5;
                            break;
                        case 8:
                            pXSector.Depth = 6;
                            break;
                        case 9:
                            pXSector.Depth = 7;
                            break;
                    }

                    if (valueIsBetween(pXSource.getData2(), -1, 32767)) {
                        if (pXSource.getData2() > 255) {
                            boardService.getSector(nDest).setVisibility(255);
                        } else {
                            boardService.getSector(nDest).setVisibility(pXSource.getData2());
                        }
                    }

                    if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                        boardService.getSector(nDest).setCeilingstat(pXSource.getData3());
                    }

                    if (valueIsBetween(pXSource.getData4(), -1, 65535)) {
                        boardService.getSector(nDest).setFloorstat((short) pXSource.getData4());
                    }
                    break;

                case SS_SPRITE:

                    if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                        boardService.getSprite(nDest).setHitag(pXSource.getData3());
                    }

                    if (valueIsBetween(pXSource.getData4(), -1, 65535)) {
                        pXSource.setData4(pXSource.getData4() | kSpriteOriginAlign);
                        boardService.getSprite(nDest).setCstat((short) pXSource.getData4());
                    }


                    break;
                case SS_WALL:

                    if (valueIsBetween(pXSource.getData3(), -1, 32767)) {
                        boardService.getWall(nDest).setHitag(pXSource.getData3());
                    }

                    if (valueIsBetween(pXSource.getData4(), -1, 65535)) {
                        boardService.getWall(nDest).setCstat((short) pXSource.getData4());
                    }

                    break;
            }
        }
    }

    public static void trMessageSector(int nSector, int event) {
        if (nSector >= boardService.getSectorCount()) {
            throw new AssertException("nSector < boardService.getSectorCount() " + nSector + " < " + boardService.getSectorCount());
        }
        if (!(boardService.getSector(nSector).getExtra() > 0 && boardService.getSector(nSector).getExtra() < kMaxXSectors)) {
            throw new AssertException("boardService.getSector(nSector).extra > 0 && boardService.getSector(nSector).extra < kMaxXSectors");
        }
        XSECTOR pXSector = xsector[boardService.getSector(nSector).getExtra()];
        int nCommand = getCommand(event);

        // don't send it to the originator
        if (pXSector.locked != 0 && nCommand != kCommandUnlock && nCommand != kCommandToggleLock) {
            return;
        }

        // operate the sector
        if (nCommand == kCommandLink) {
            LinkSector(nSector, pXSector, event);
        } else if (nCommand == kGDXCommandPaste) {
            pastePropertiesInObj(SS_SECTOR, nSector, event);
        } else {
            OperateSector(nSector, pXSector, nCommand);
        }
    }

    public static void trTriggerWall(int nWall, XWALL pXWall, int command) {
//		System.out.println("TriggerWall: nWall=" + nWall + ", command=" + command);

        if (nWall >= boardService.getWallCount()) {
            throw new AssertException("nWall < boardService.getWallCount()");
        }
        // bypass locked XObjects
        if (pXWall.locked != 0) {
            return;
        }

        // bypass triggered one-shots
        if (pXWall.isTriggered) {
            return;
        }

        if (pXWall.triggerOnce) {
            pXWall.isTriggered = true;
        }

        if (pXWall.decoupled) {
            if (pXWall.txID != 0) {
                evSend(nWall, SS_WALL, pXWall.txID, pXWall.command);
            }
        } else {
            // operate the wall
            OperateWall(nWall, pXWall, command);
        }
    }

    public static void trMessageWall(int nWall, int event) {
        if (nWall >= boardService.getWallCount()) {
            throw new AssertException("nWall < boardService.getWallCount()");
        }
        if (!(boardService.getWall(nWall).getExtra() > 0 && boardService.getWall(nWall).getExtra() < kMaxXWalls)) {
            throw new AssertException("boardService.getWall(nWall).extra > 0 && boardService.getWall(nWall).extra < kMaxXWalls");
        }
        XWALL pXWall = xwall[boardService.getWall(nWall).getExtra()];
        int nCommand = getCommand(event);

        // don't send it to the originator
        if (pXWall.locked != 0 && nCommand != kCommandUnlock && nCommand != kCommandToggleLock) {
            return;
        }

        // operate the wall
        if (nCommand == kCommandLink) {
            LinkWall(nWall, pXWall, event);
        } else if (nCommand == kGDXCommandPaste) {
            pastePropertiesInObj(SS_WALL, nWall, event);
        } else {
            OperateWall(nWall, pXWall, nCommand);
        }
    }

    public static void trTriggerSprite(int nSprite, XSPRITE pXSprite, int command) {
//		dprintf("TriggerSprite: nSprite=%i, type= %i command=%i\n", nSprite, boardService.getSprite(nSprite).type, command);

        // bypass locked XObjects
        if (pXSprite.getLocked() != 0) {
            return;
        }

        // bypass triggered one-shots
        if (pXSprite.isTriggered()) {
            return;
        }

        if (pXSprite.isTriggerOnce()) {
            pXSprite.setTriggered(true);
        }

        if (pXSprite.isDecoupled()) {
            if (pXSprite.getTxID() != 0) {
                evSend(nSprite, SS_SPRITE, pXSprite.getTxID(), pXSprite.getCommand());
            }
        } else {
            // operate the sprite
            OperateSprite(nSprite, pXSprite, command);
        }
    }

    public static void trMessageSprite(int nSprite, int event) {
        BloodSprite pSprite = boardService.getSprite(nSprite);
        // return immediately if sprite has been deleted
        if (pSprite.getStatnum() == kMaxStatus) {
            return;
        }

        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        int nCommand = getCommand(event);
        // don't send it to the originator
        if (pXSprite.getLocked() != 0 && nCommand != kCommandUnlock && nCommand != kCommandToggleLock) {
            return;
        }

        // operate the sprite
        if (nCommand == kCommandLink) {
            LinkSprite(nSprite, pXSprite, event);
        } else if (nCommand == kGDXCommandPaste) {
            pastePropertiesInObj(SS_SPRITE, nSprite, event);
        } else {
            OperateSprite(nSprite, pXSprite, nCommand);
        }

    }

    public static void trProcessMotion() {

        for (int nSector = 0; nSector < boardService.getSectorCount(); nSector++) {
            Sector pSector = boardService.getSector(nSector);

            if (pSector.getExtra() > 0) {
                XSECTOR pXSector = xsector[pSector.getExtra()];

                if (pXSector.bobSpeed != 0) {


                    if (pXSector.bobAlways) {
                        pXSector.bobTheta += pXSector.bobSpeed;
                    } else {
                        if (pXSector.busy == 0) {
                            continue;
                        }
                        pXSector.bobTheta += mulscale(pXSector.busy, pXSector.bobSpeed, 16);
                    }

                    int phase = mulscale((long) pXSector.bobZRange << 8, Sin(pXSector.bobTheta), 30);
                    for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                        int nSprite = node.getIndex();
                        Sprite pSprite = node.get();
                        if ((pSprite.getCstat() & 0x6000) != 0) {
                            viewBackupSpriteLoc(nSprite, pSprite);
                            pSprite.setZ(pSprite.getZ() + phase);
                        }
                    }

                    if (pXSector.bobFloor) {
                        int oldFloorz = pSector.getFloorz();
                        viewBackupSectorLoc(nSector, pSector);
                        pSector.setFloorz(phase + secFloorZ[nSector]);
                        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                            int nSprite = node.getIndex();
                            Sprite pSprite = node.get();
                            if ((pSprite.getHitag() & kAttrGravity) != 0) {
                                pSprite.setHitag(pSprite.getHitag() | kAttrFalling);
                            } else {
                                GetSpriteExtents(pSprite);
                                if (extents_zBot >= oldFloorz && (pSprite.getCstat() & 0x30) == 0) {
                                    viewBackupSpriteLoc(nSprite, pSprite);
                                    pSprite.setZ(pSprite.getZ() + phase);
                                }
                            }
                        }
                    }
                    if (pXSector.bobCeiling) {
                        int oldCeilz = pSector.getCeilingz();
                        viewBackupSectorLoc(nSector, pSector);
                        pSector.setCeilingz(phase + secCeilZ[nSector]);

                        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                            int nSprite = node.getIndex();
                            Sprite pSprite = node.get();

                            GetSpriteExtents(pSprite);
                            if (extents_zTop <= oldCeilz && (pSprite.getCstat() & 0x30) == 0) {
                                viewBackupSpriteLoc(nSprite, pSprite);
                                pSprite.setZ(pSprite.getZ() + phase);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void trProcessSlope() {
        for (int nSector = 0; nSector < boardService.getSectorCount(); nSector++) {
            Sector pSector = boardService.getSector(nSector);
            if (pSector.getFiller() != 0) {
                Wall pWall = boardService.getWall(pSector.getFiller() + pSector.getWallptr());
                Wall pWall2 = boardService.getWall(pWall.getPoint2());
                if (pWall.getNextsector() >= 0) {
                    int dx = (pWall2.getX() + pWall.getX()) / 2;
                    int dy = (pWall2.getY() + pWall.getY()) / 2;
                    viewBackupSectorLoc(nSector, pSector);
                    int floorslope = engine.getflorzofslope(pWall.getNextsector(), dx, dy);
                    engine.alignflorslope((short) nSector, dx, dy, floorslope);
                    int ceilingslope = engine.getceilzofslope(pWall.getNextsector(), dx, dy);
                    engine.alignceilslope((short) nSector, dx, dy, ceilingslope);
                }
            }
        }
    }

    public static void trProcessBusy() {
        Arrays.fill(floorVel, 0);
        Arrays.fill(ceilingVel, 0);

        for (int i = gBusyCount - 1; i >= 0; i--) {
            // temporarily store nBusy before normalization
            int tempBusy = gBusy[i].nBusy;
            gBusy[i].nBusy = ClipRange(gBusy[i].nBusy + gBusy[i].nDelta * kFrameTicks, 0, kMaxBusyValue);
            int rcode = gBusyProc[gBusy[i].busyProc].run(gBusy[i].nIndex, gBusy[i].nBusy);

            switch (rcode) {
                case kBusyRestore:
                    gBusy[i].nBusy = tempBusy; // restore previous nBusy
                    break;
                case kBusyReverse:
                    gBusy[i].nBusy = tempBusy; // restore previous nBusy
                    gBusy[i].nDelta = -gBusy[i].nDelta; // reverse delta
                    break;
                case kBusyComplete:
                    gBusyCount--;
                    gBusy[i].nIndex = gBusy[gBusyCount].nIndex;
                    gBusy[i].nBusy = gBusy[gBusyCount].nBusy;
                    gBusy[i].nDelta = gBusy[gBusyCount].nDelta;
                    gBusy[i].busyProc = gBusy[gBusyCount].busyProc;
                    break;
            }
        }

        trProcessMotion();
        trProcessSlope();
    }

    public static void trInitStructs() {
        gBusyCount = 0;
        for (int i = 0; i < kMaxBusyArray; i++) {
            if (gBusy[i] == null) {
                gBusy[i] = new BUSY();
            } else {
                gBusy[i].clear();
            }
        }

        for (int nWall = 0; nWall < kwall.length; nWall++) {
            if (kwall[nWall] == null) {
                kwall[nWall] = new Vector2();
            } else {
                kwall[nWall].set(0, 0);
            }
        }
    }

    public static void trInit(Board board) {

        int i;
        int nSector;

        trInitStructs();

        // get wall vertice positions
        Wall[] wall = board.getWalls();
        for (int nWall = 0; nWall < wall.length; nWall++) {
            kwall[nWall].x = boardService.getWall(nWall).getX();
            kwall[nWall].y = boardService.getWall(nWall).getY();
        }

        // get sprite positions
        List<Sprite> sprites = board.getSprites();
        for (Sprite value : sprites) {
            BloodSprite sprite = (BloodSprite) value;
            if (sprite.getStatnum() < kMaxStatus) {
                Vector3 ksprite = sprite.getKSprite();
                ksprite.x = sprite.getX();
                ksprite.y = sprite.getY();
                ksprite.z = sprite.getZ();
                sprite.setZvel(sprite.getLotag());
            } else {
                sprite.setZvel(-1);
            }
        }

        // init wall trigger masks (must be done first)
        for (int nWall = 0; nWall < wall.length; nWall++) {
            if (boardService.getWall(nWall).getExtra() > 0) {
                int nXWall = boardService.getWall(nWall).getExtra();
                if (nXWall >= kMaxXWalls) {
                    throw new AssertException("nXWall < kMaxXWalls");
                }

                XWALL pXWall = xwall[nXWall];

                if (pXWall.state != 0) {
                    pXWall.busy = kMaxBusyValue;
                }
            }
        }

        Sector[] sectors = board.getSectors();
        for (nSector = 0; nSector < sectors.length; nSector++) {
            Sector pSector = sectors[nSector];

            secFloorZ[nSector] = pSector.getFloorz();
            secCeilZ[nSector] = pSector.getCeilingz();

            int startWall, endWall;
            int nXSector = pSector.getExtra();
            if (nXSector > 0) {
                if (nXSector >= kMaxXSectors) {
                    throw new AssertException("nXSector < kMaxXSectors");
                }
                XSECTOR pXSector = xsector[nXSector];

                if (pXSector.state != 0) {
                    pXSector.busy = kMaxBusyValue;
                }

                switch (pSector.getLotag()) {
                    case kSectorZMotion:
                    case kSectorZSprite:
                        zMotion(nSector, pXSector, pXSector.busy, 1);
                        break;
                    case kSectorPath:
                        PathSectorInit(nSector, pXSector);
                        break;
                    case kSectorSlideMarked:
                    case kSectorSlide:

                        Sprite pMark0 = boardService.getSprite(pXSector.marker0);
                        Sprite pMark1 = boardService.getSprite(pXSector.marker1);

                        // move door to off position by reversing markers
                        TranslateSector(nSector, 0, -0x10000, pMark0.getX(), pMark0.getY(), pMark0.getX(), pMark0.getY(), pMark0.getAng(), pMark1.getX(), pMark1.getY(), pMark1.getAng(), pSector.getLotag() == kSectorSlide);

                        // grab updated positions of walls
                        startWall = pSector.getWallptr();
                        endWall = pSector.getEndWall();
                        for (i = startWall; i <= endWall; i++) {
                            kwall[i].x = boardService.getWall(i).getX();
                            kwall[i].y = boardService.getWall(i).getY();
                        }

                        // grab updated positions of sprites
                        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                            BloodSprite pSprite = (BloodSprite) node.get();
                            Vector3 ksprite = pSprite.getKSprite();
                            ksprite.x = pSprite.getX();
                            ksprite.y = pSprite.getY();
                            ksprite.z = pSprite.getZ();
                        }

                        // open door if necessary
                        TranslateSector(nSector, 0, pXSector.busy, pMark0.getX(), pMark0.getY(), pMark0.getX(), pMark0.getY(), pMark0.getAng(), pMark1.getX(), pMark1.getY(), pMark1.getAng(), pSector.getLotag() == kSectorSlide);

                        zMotion(nSector, pXSector, pXSector.busy, 1);

                        break;
                    case kSectorRotate:
                    case kSectorRotateMarked:

                        Sprite pMark = board.getSprite(pXSector.marker0);

                        // move door to off position
                        TranslateSector(nSector, 0, -0x10000, pMark.getX(), pMark.getY(), pMark.getX(), pMark.getY(), 0, pMark.getX(), pMark.getY(), pMark.getAng(), pSector.getLotag() == kSectorRotate);

                        // grab updated positions of walls
                        startWall = pSector.getWallptr();
                        endWall = pSector.getEndWall();
                        for (i = startWall; i <= endWall; i++) {
                            kwall[i].x = boardService.getWall(i).getX();
                            kwall[i].y = boardService.getWall(i).getY();
                        }

                        // grab updated positions of sprites
                        for (ListNode<Sprite> node = boardService.getSectNode(nSector); node != null; node = node.getNext()) {
                            BloodSprite pSprite = (BloodSprite) node.get();
                            Vector3 ksprite = pSprite.getKSprite();
                            ksprite.x = pSprite.getX();
                            ksprite.y = pSprite.getY();
                            ksprite.z = pSprite.getZ();
                        }

                        // open door if necessary
                        TranslateSector(nSector, 0, pXSector.busy, pMark.getX(), pMark.getY(), pMark.getX(), pMark.getY(), 0, pMark.getX(), pMark.getY(), pMark.getAng(), pSector.getLotag() == kSectorRotate);

                        zMotion(nSector, pXSector, pXSector.busy, 1);

                        break;

                    case kSectorCounter:
                        if (IsOriginalDemo()) {
							/* No need to trigger once it, instead lock so it can be
						    unlocked and used again */
                            pXSector.triggerOnce = true;
                        }
                        evPostCallback(nSector, SS_SECTOR, 0, kCommandCounter);
                        break;
                }
            }
        }

        // init sprite trigger masks
        for (int nSprite = 0; nSprite < sprites.size(); nSprite++) {
            Sprite pSprite = sprites.get(nSprite);
            XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
            if (pSprite.getStatnum() < kMaxStatus && pXSprite != null) {
                if (pXSprite.getState() != 0) {
                    pXSprite.setBusy(kMaxBusyValue);
                }

                // special initialization for implicit trigger types
                switch (pSprite.getLotag()) {
                    case kSwitchPadlock:
                        pXSprite.setTriggerOnce(true);    // force trigger once
                        break;
                    case kThingTNTProx:
                    case kGDXThingTNTProx:
                        pXSprite.setProximity(true);
                        break;
                    case kThingFallingRock:
                        if (pXSprite.getState() != 0) {
                            pSprite.setHitag(pSprite.getHitag() | (kAttrMove | kAttrGravity | kAttrFalling));
                        } else {
                            pSprite.setHitag(pSprite.getHitag() & ~(kAttrMove | kAttrGravity | kAttrFalling));
                        }

                        break;

                    case kWeaponItemBase: // Random weapon
                    case kAmmoItemRandom: // Random ammo
                    case kGDXSeqSpawner:
                    case kGDXObjDataAccumulator:
                    case kGDXDudeTargetChanger:
                    case kGDXEffectSpawner:
                    case kGDXWindGenerator:
                    case kGenTrigger:
                    case kGenWaterDrip:
                    case kGenBloodDrip:
                    case kGenFireball:
                    case kGenEctoSkull:  // can shot any projectile, default 307 (ectoskull)
                    case kGenDart:
                    case kGenSFX:
                    case kGenBubble:
                    case kGenMultiBubble:
                        InitGenerator(nSprite);
                        break;
                }

                if (pXSprite.isPush()) {
                    pSprite.setCstat(pSprite.getCstat() | kSpritePushable);
                }
                if (pXSprite.isVector()) {
                    pSprite.setCstat(pSprite.getCstat() | kSpriteHitscan);
                }
            }
        }

        evSend(0, 0, kChannelTriggerStart, kCommandOn);

        if (pGameInfo.nGameType == kNetModeCoop) {
            evSend(0, 0, kChannelTriggerCoop, kCommandOn);
        }
        if (pGameInfo.nGameType == kNetModeBloodBath || pGameInfo.nGameType == kNetModeTeams) {
            evSend(0, 0, kChannelTriggerMatch, kCommandOn);
        }
        if (pGameInfo.nGameType == kNetModeTeams) {
            evSend(0, 0, kChannelTriggerTeam, kCommandOn);
        }
    }

    public static void useTeleportTarget(XSPRITE pXSource, BloodSprite pSprite) {
        Sprite pSource = boardService.getSprite(pXSource.getReference());
        XSECTOR pXSector = (boardService.getSector(pSource.getSectnum()).getExtra() >= 0) ? xsector[boardService.getSector(pSource.getSectnum()).getExtra()] : null;

        if (pSprite == null) {

            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {

                if (pXSource.getData1() < kMaxPlayers)  // relative to connected players
                {
                    if (pXSource.getData1() != (i + 1)) {
                        continue;
                    } else if (pXSource.getData1() < (kDudePlayer1 + kMaxPlayers))    // absolute lotag
                    {
                        if (pXSource.getData1() >= kDudePlayer1 && (pXSource.getData1() + (kDudePlayer1 - 1)) == gPlayer[i].pSprite.getLotag()) {
                            continue;
                        }
                    }
                }

                useTeleportTarget(pXSource, gPlayer[i].pSprite);
                return;

            }

            return;
        }

        pSprite.setX(pSource.getX());
        pSprite.setY(pSource.getY());
        pSprite.setZ(pSprite.getZ() + (boardService.getSector(pSource.getSectnum()).getFloorz() - boardService.getSector(pSprite.getSectnum()).getFloorz()));

        if ((pSource.getHitag() & 1) != 0) // force telefrag
        {
            TeleportDamage(pSprite.getXvel(), pSource.getSectnum());
        }

        engine.changespritesect(pSprite.getXvel(), pSource.getSectnum());
        if (pXSector != null && pXSector.Underwater) {
            boardService.getXSprite(pSprite.getExtra()).setPalette(1);
        } else {
            boardService.getXSprite(pSprite.getExtra()).setPalette(0);
        }

        if (pXSource.getData2() == 1) {
            pSprite.setAng(pSource.getAng());
        }

        if (pXSource.getData3() == 1) {
            pSprite.setVelocity(0, 0, 0);
        }

        viewBackupSpriteLoc(pSprite.getXvel(), pSprite);

        if (pXSource.getData4() > 0) {
            sfxStart3DSound(pSource, pXSource.getData4(), -1, 0);
        }

        if (IsPlayerSprite(pSprite)) {

            PLAYER pPlayer = gPlayer[pSprite.getLotag() - kDudePlayer1];

            viewUpdatePlayerLoc(pPlayer);
            if (pXSource.getData2() == 1) {
                gPlayer[pPlayer.nPlayer].weapOffdZ = gPlayer[pPlayer.nPlayer].viewOffdZ = 0;
                gPlayer[pPlayer.nPlayer].ang = pSprite.getAng();
            }

        }
    }


    public static void useSpriteDamager(XSPRITE pXSource, BloodSprite pSprite) {

        int dmg = (pXSource.getData4() == 0 || pXSource.getData4() > 65534) ? 65535 : pXSource.getData4();
        int dmgType = (pXSource.getData3() >= kDamageMax) ? Random(6) : ((pXSource.getData3() < 0) ? 0 : pXSource.getData3());

        // just damage / heal TX ID sprite
        if (pSprite != null) {
            actDamageSprite(pSprite.getXvel(), pSprite, dmgType, dmg);

            // or damage / heal player# specified in data2 (or all players if data2 is empty)
        } else if (pXSource.getData2() > 0 && pXSource.getData2() <= kMaxPlayers) {

            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                if (pXSource.getData1() < kMaxPlayers)  // relative to connected players
                {
                    if (pXSource.getData1() != (i + 1)) {
                        continue;
                    } else if (pXSource.getData1() < (kDudePlayer1 + kMaxPlayers))    // absolute lotag
                    {
                        if (pXSource.getData1() >= kDudePlayer1 && (pXSource.getData1() + (kDudePlayer1 - 1)) == gPlayer[i].pSprite.getLotag()) {
                            continue;
                        }
                    }
                }
                actDamageSprite(boardService.getSprite(pXSource.getReference()).getXvel(), gPlayer[i].pSprite, dmgType, dmg);
                return;
            }
        }
    }

    public static void useSeqSpawnerGen(XSPRITE pXSource, int objType, int index) {
        switch (objType) {
            case SS_SECTOR:
                if (pXSource.getData2() <= 0) {
                    if (pXSource.getData3() == 3 || pXSource.getData3() == 1) {
                        seqKill(SS_FLOOR, boardService.getSector(index).getExtra());
                    }
                    if (pXSource.getData3() == 3 || pXSource.getData3() == 2) {
                        seqKill(SS_CEILING, boardService.getSector(index).getExtra());
                    }
                } else {
                    if (pXSource.getData3() == 3 || pXSource.getData3() == 1) {
                        seqSpawn(pXSource.getData2(), SS_FLOOR, boardService.getSector(index).getExtra(), null);
                    }
                    if (pXSource.getData3() == 3 || pXSource.getData3() == 2) {
                        seqSpawn(pXSource.getData2(), SS_CEILING, boardService.getSector(index).getExtra(), null);
                    }
                }
                return;

            case SS_WALL:
                if (pXSource.getData2() <= 0) {
                    if (pXSource.getData3() == 3 || pXSource.getData3() == 1) {
                        seqKill(SS_WALL, boardService.getWall(index).getExtra());
                    }
                    if ((pXSource.getData3() == 3 || pXSource.getData3() == 2) && (boardService.getWall(index).getCstat() & kWallMasked) != 0) {
                        seqKill(SS_MASKED, boardService.getWall(index).getExtra());
                    }
                } else {

                    if (pXSource.getData3() == 3 || pXSource.getData3() == 1) {
                        seqSpawn(pXSource.getData2(), SS_WALL, boardService.getWall(index).getExtra(), null);
                    }
                    if (pXSource.getData3() == 3 || pXSource.getData3() == 2) {

                        if (boardService.getWall(index).getNextwall() < 0) {
                            if (pXSource.getData3() == 3) {
                                seqSpawn(pXSource.getData2(), SS_WALL, boardService.getWall(index).getExtra(), null);
                            }

                        } else {
                            if ((boardService.getWall(index).getCstat() & kWallMasked) == 0) {
                                boardService.getWall(index).setCstat(boardService.getWall(index).getCstat() | kWallMasked);
                            }
                            seqSpawn(pXSource.getData2(), SS_MASKED, boardService.getWall(index).getExtra(), null);
                        }
                    }

                    if (pXSource.getData4() > 0) {

                        int cx = (boardService.getWall(boardService.getWall(index).getPoint2()).getX() + boardService.getWall(index).getX()) / 2;
                        int cy = (boardService.getWall(boardService.getWall(index).getPoint2()).getY() + boardService.getWall(index).getY()) / 2;
                        int nSector = engine.sectorofwall((short) index);
                        engine.getzsofslope((short) nSector, cx, cy, floorz, ceilz);
                        int floorz = Globals.floorz.get();
                        int ceilz = Globals.ceilz.get();
                        if (boardService.getWall(index).getNextsector() != -1) {
                            engine.getzsofslope(boardService.getWall(index).getNextsector(), cx, cy, Globals.floorz, Globals.ceilz);
                        }
                        int nextfloorz = Globals.floorz.get();
                        int nextceilz = Globals.ceilz.get();

                        int cz = ceilz;
                        int fz = floorz;
                        if (ceilz < nextceilz) {
                            cz = nextceilz;
                        }
                        if (floorz >= nextfloorz) {
                            fz = nextfloorz;
                        }


                        sfxCreate3DSound(cx, cy, (fz + cz) >> 1, pXSource.getData4(), nSector);
                    }

                }
                return;

            case SS_SPRITE:
                if (pXSource.getData2() <= 0) {
                    seqKill(SS_SPRITE, boardService.getSprite(index).getExtra());
                } else {
                    seqSpawn(pXSource.getData2(), SS_SPRITE, boardService.getSprite(index).getExtra(), (pXSource.getData3() > 0) ? callbacks[pXSource.getData3()] : null);
                    if (pXSource.getData4() > 0) {
                        sfxStart3DSound(boardService.getSprite(index), pXSource.getData4(), -1, 0);
                    }
                }
        }
    }

    public static void useSectorWindGen(XSPRITE pXSource, Sector pSector) {
        Sprite pSource = boardService.getSprite(pXSource.getReference());
        XSECTOR pXSector = null;
        boolean forceWind = false;

        if (pSector.getExtra() >= 0) {
            pXSector = xsector[pSector.getExtra()];
        } else {
            int nXSector = dbInsertXSector(engine.getBoardService().getSector(pSource.getSectnum()), pSource.getSectnum());
            if (nXSector < 0) {
                return;
            }

            pXSector = xsector[nXSector];
            forceWind = true;

        }

        if ((pSource.getHitag() & 0x0001) != 0) {
            pXSector.panAlways = true;
            pXSector.windAlways = true;
        } else if (forceWind) {
            pXSector.windAlways = true;
        }

        if (pXSource.getData2() > 32766) {
            pXSource.setData2(32767);
        }

        if (pXSource.getData1() == 1 || pXSource.getData1() == 3) {
            pXSector.windVel = Random(pXSource.getData2());
        } else {
            pXSector.windVel = pXSource.getData2();
        }

        if (pXSource.getData1() == 2 || pXSource.getData1() == 3) {
            short ang = pSource.getAng();
            while (pSource.getAng() == ang) {
                pSource.setAng((short) BiRandom(kAngle360));
            }
        }

        pXSector.windAng = pSource.getAng();

        if (pXSource.getData3() > 0 && pXSource.getData3() < 4) {
            switch (pXSource.getData3()) {
                case 1:
                    pXSector.panFloor = true;
                    pXSector.panCeiling = false;
                    break;
                case 2:
                    pXSector.panFloor = false;
                    pXSector.panCeiling = true;
                    break;
                case 3:
                    pXSector.panFloor = true;
                    pXSector.panCeiling = true;
                    break;
            }

            short oldPan = pXSector.panVel;
            pXSector.panAngle = (short) pXSector.windAng;
            pXSector.panVel = (short) pXSector.windVel;

            // add to panList if panVel was set to 0 previously
            if (oldPan == 0 && pXSector.panVel != 0) {
                for (int i = panCount; i >= 0; i--) {
                    if (panList[i] == pSector.getExtra()) {
                        break;
                    } else if (i == 0) {
                        panList[panCount++] = pSector.getExtra();
                    }
                }
            }
        }


    }

    public static void useEffectGen(XSPRITE pXSource, Sprite pSprite) {
        if (boardService.getXSprite(pSprite.getExtra()) == null) {
            return;
        }

        GetSpriteExtents(pSprite);
        Sprite pEffect = null;
        int dx = 0, dy = 0;
        int cnt = Math.min(pXSource.getData4(), 32);
        while (cnt-- >= 0) {

            if (cnt > 0) {
                dx = BiRandom(250);
                dy = BiRandom(150);
            }

            pEffect = actSpawnEffect(pXSource.getData2(), pSprite.getSectnum(), pSprite.getX() + dx, pSprite.getY() + dy, extents_zTop, 0);
            if (pEffect != null) {
                if (pEffect.getPal() <= 0) {
                    pEffect.setPal(pSprite.getPal());
                }
                if (pEffect.getXrepeat() <= 0) {
                    pEffect.setXrepeat(pSprite.getXrepeat());
                }
                if (pEffect.getYrepeat() <= 0) {
                    pEffect.setYrepeat(pSprite.getYrepeat());
                }
                if (pEffect.getShade() == 0) {
                    pEffect.setShade(pSprite.getShade());
                }
            }
        }

        if (pXSource.getData3() > 0) {
            sfxStart3DSound(pSprite, pXSource.getData3(), -1, 0);
        }

    }


    public static void stopWindOnSectors(XSPRITE pXSource) {
        Sprite pSource = boardService.getSprite(pXSource.getReference());

        if (pXSource.getTxID() <= 0) {

            if (boardService.getSector(pSource.getSectnum()).getExtra() >= 0) {
                xsector[boardService.getSector(pSource.getSectnum()).getExtra()].windVel = 0;
            }

            return;
        }

        for (int i = bucketHead[pXSource.getTxID()]; i < bucketHead[pXSource.getTxID() + 1]; i++) {
            if (rxBucket[i].type != SS_SECTOR) {
                continue;
            }
            XSECTOR pXSector = xsector[boardService.getSector(rxBucket[i].index).getExtra()];
            if ((pXSector.state == 1 && !pXSector.windAlways) || pSource.getHitag() == 1) {
                pXSector.windVel = 0;
            }
        }
    }

    public static boolean targetMustBeForced(Sprite pDude) {
        switch (pDude.getLotag()) {
            case kDudeEarthZombie:
            case kDudeSleepZombie:
                return true;
            default:
                return false;
        }
    }

    public static boolean dudeCanSeeTarget(XSPRITE pXDude, DudeInfo pDudeInfo, Sprite pTarget) {
        Sprite pDude = boardService.getSprite(pXDude.getReference());
        int dx = pTarget.getX() - pDude.getX();
        int dy = pTarget.getY() - pDude.getY();

        int dist = EngineUtils.qdist(dx, dy);

        // check target
        if (dist < pDudeInfo.seeDist) {
            int eyeAboveZ = pDudeInfo.eyeHeight * pDude.getYrepeat() << 2;

            // is there a line of sight to the target
            //// 360deg periphery here ////
            //int nAngle = engine.getangle(dx, dy);
            //int losAngle = ((kAngle180 + nAngle - pDude.ang) & kAngleMask) - kAngle180;
            // is the target visible?
            //if (ru.m210projects.Build.Pragmas.klabs(losAngle) < Globals.kAngle360 )
            return engine.cansee(pDude.getX(), pDude.getY(), pDude.getZ(), pDude.getSectnum(), pTarget.getX(), pTarget.getY(), pTarget.getZ() - eyeAboveZ, pTarget.getSectnum());
        }

        return false;

    }

    public static void disturbDudesInSight(Sprite pSprite, int max) {
        Sprite pDude = null;
        XSPRITE pXDude = null;
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            pDude = node.get();
            pXDude = boardService.getXSprite(pDude.getExtra());
            if (pDude.getXvel() == pSprite.getXvel() || !IsDudeSprite(pDude) || pXDude == null) {
                continue;
            }

            if (dudeCanSeeTarget(pXSprite, pDudeInfo, pDude)) {
                if (pXDude.getTarget() != -1 || pXDude.getRxID() > 0) {
                    continue;
                }

                aiSetTarget(pXDude, pDude.getX(), pDude.getY(), pDude.getZ());
                aiActivateDude((BloodSprite) pDude, pXDude);
                //System.out.println("//////////// => DUDE DISTURBED "+pDude.lotag);
                if (max-- < 1) {
                    break;
                }
            }
        }
    }

    public static int getTargetDist(Sprite pSprite, DudeInfo pDudeInfo, Sprite pTarget) {
        int x = pTarget.getX();
        int y = pTarget.getY(); //int z = pTarget.z;
        int dx = x - pSprite.getX();
        int dy = y - pSprite.getY();

        int dist = EngineUtils.qdist(dx, dy);
        if (dist <= pDudeInfo.meleeDist) {
            return 0;
        }
        if (dist >= pDudeInfo.seeDist) {
            return 13;
        }
        if (dist <= pDudeInfo.seeDist / 12) {
            return 1;
        }
        if (dist <= pDudeInfo.seeDist / 11) {
            return 2;
        }
        if (dist <= pDudeInfo.seeDist / 10) {
            return 3;
        }
        if (dist <= pDudeInfo.seeDist / 9) {
            return 4;
        }
        if (dist <= pDudeInfo.seeDist / 8) {
            return 5;
        }
        if (dist <= pDudeInfo.seeDist / 7) {
            return 6;
        }
        if (dist <= pDudeInfo.seeDist / 6) {
            return 7;
        }
        if (dist <= pDudeInfo.seeDist / 5) {
            return 8;
        }
        if (dist <= pDudeInfo.seeDist / 4) {
            return 9;
        }
        if (dist <= pDudeInfo.seeDist / 3) {
            return 10;
        }
        if (dist <= pDudeInfo.seeDist / 2) {
            return 11;
        }
        return 12;
    }

    public static int getFineTargetDist(Sprite pSprite, DudeInfo pDudeInfo, Sprite pTarget) {
        int x = pTarget.getX();
        int y = pTarget.getY();
        int dx = x - pSprite.getX();
        int dy = y - pSprite.getY();

        int dist = EngineUtils.qdist(dx, dy);
        return dist;
    }

    public static BloodSprite getTargetInRange(Sprite pSprite, int minDist, int maxDist, short data, short teamMode) {
        DudeInfo pDudeInfo = dudeInfo[pSprite.getLotag() - kDudeBase];
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        BloodSprite pTarget = null;
        XSPRITE pXTarget = null;
        Sprite cTarget = null;
        for (ListNode<Sprite> node = boardService.getStatNode(kStatDude); node != null; node = node.getNext()) {
            pTarget = (BloodSprite) node.get();
            pXTarget = boardService.getXSprite(pTarget.getExtra());
            if (!dudeCanSeeTarget(pXSprite, pDudeInfo, pTarget)) {
                continue;
            }

            int dist = getTargetDist(pSprite, pDudeInfo, pTarget);
            if (dist < minDist || dist > maxDist) {
                continue;
            } else if (pXSprite.getTarget() == pTarget.getXvel()) {
                return pTarget;
            } else if (!IsDudeSprite(pTarget) || pTarget.getXvel() == pSprite.getXvel() || IsPlayerSprite(pTarget)) {
                continue;
            } else if (IsBurningDude(pTarget) || isImmortalDude(pTarget) || pTarget.getOwner() == pSprite.getXvel()) {
                continue;
            } else if ((teamMode == 1 && isMateOf(pXSprite, pXTarget)) || isMatesHaveSameTarget(pXSprite, pTarget, 1)) {
                continue;
            } else if (data == 666 || pXTarget.getData1() == data) {

                if (pXSprite.getTarget() > 0) {
                    cTarget = boardService.getSprite(pXSprite.getTarget());
                    int fineDist1 = getFineTargetDist(pSprite, pDudeInfo, cTarget);
                    int fineDist2 = getFineTargetDist(pSprite, pDudeInfo, pTarget);
                    if (fineDist1 < fineDist2) {
                        continue;
                    }
                }
                //System.out.println(" => FOUND BETTER TARGET FOR: "+pSprite.lotag+",TARGET LOTAG: "+pTarget.lotag+", TARGET RX ID:"+pXTarget.rxID+", TARGET DATA1: "+pXTarget.data1);
                return pTarget;
            }
        }

        //System.out.println("!!!!!!! => NO TARGETS IN SIGHT");
        return null;
    }

    public static boolean isActive(XSPRITE pXSprite) {
        Type type = pXSprite.getAIState().getType();
        if (type == Type.idle) {
            return false;
        }

        if ((gFrameClock & 48) != 0) {
            if (type == Type.tgoto || type == Type.search) {
                return false;
            }
            //System.err.println("NOT ACTIVE"+boardService.getSprite(nSprite).lotag+" / "+aiActive[nSprite]);
            return pXSprite.getDudeExtra().isActive();
        }
        return true;
    }

    public static boolean isAnnoyingUnit(Sprite pDude) {
        switch (pDude.getLotag()) {
            case kDudeHand:
            case kDudeBrownSpider:
            case kDudeRedSpider:
            case kDudeBlackSpider:
            case kDudeMotherSpider:
            case kDudeEel:
            case kDudeBat:
            case kDudeRat:
            case kDudeGreenTentacle:
            case kDudeFireTentacle:
            case kDudeMotherTentacle:
            case kDudeGreenPod:
            case kDudeFirePod:
                return true;
            default:
                return false;
        }
    }

    public static boolean unitCanFly(Sprite pDude) {
        switch (pDude.getLotag()) {
            case kDudeBat:
            case kDudeFleshGargoyle:
            case kDudeStoneGargoyle:
            case kDudePhantasm:
                return true;
            default:
                return false;
        }
    }

    public static boolean isMeleeUnit(Sprite pDude) {
        switch (pDude.getLotag()) {
            case kDudeAxeZombie:
            case kDudeEarthZombie:
            case kDudeFleshGargoyle:
            case kDudeFleshStatue:
            case kDudeHand:
            case kDudeBrownSpider:
            case kDudeRedSpider:
            case kDudeBlackSpider:
            case kDudeMotherSpider:
            case kDudeGillBeast:
            case kDudeEel:
            case kDudeBat:
            case kDudeRat:
            case kDudeGreenTentacle:
            case kDudeFireTentacle:
            case kDudeMotherTentacle:
            case kDudeSleepZombie:
            case kDudeInnocent:
            case kDudeTinyCaleb:
            case kDudeTheBeast:
                return true;
            case kGDXDudeUniversalCultist:
                if (dudeIsMelee(boardService.getXSprite(pDude.getExtra()))) {
                    return true;
                }
            default:
                return false;
        }
    }

    public static Sprite targetIsPlayer(XSPRITE pXSprite) {
        if (pXSprite.getTarget() >= 0 && IsPlayerSprite(boardService.getSprite(pXSprite.getTarget()))) {
            return boardService.getSprite(pXSprite.getTarget());
        }
        return null;
    }

    public static boolean isTargetAimsDude(XSPRITE pXTarget, Sprite pDude) {
        return (pXTarget.getTarget() == pDude.getXvel());
    }

    public static boolean isMateOf(XSPRITE pXDude, XSPRITE pXSprite) {
        return (pXDude.getRxID() == pXSprite.getRxID());
    }


    public static boolean getDudesForTargetChg(XSPRITE pXSprite) {
        for (int i = bucketHead[pXSprite.getTxID()]; i < bucketHead[pXSprite.getTxID() + 1]; i++) {
            if (rxBucket[i].type != SS_SPRITE) {
                continue;
            } else if (IsDudeSprite(boardService.getSprite(rxBucket[i].index)) && boardService.getXSprite(boardService.getSprite(rxBucket[i].index).getExtra()).getHealth() > 0) {
                return true;
            }
        }

        return false;
    }

    public static XSPRITE affectedByTargetChg(XSPRITE pXDude) {
        if (pXDude.getRxID() <= 0 || pXDude.getLocked() == 1) {
            return null;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(kStatGDXDudeTargetChanger); node != null; node = node.getNext()) {
            XSPRITE pXSprite = boardService.getXSprite(node.get().getExtra());
            if (pXSprite == null || pXSprite.getTxID() <= 0 || pXSprite.getState() != 1) {
                continue;
            }

            for (int i = bucketHead[pXSprite.getTxID()]; i < bucketHead[pXSprite.getTxID() + 1]; i++) {
                if (rxBucket[i].type != SS_SPRITE) {
                    continue;
                }

                Sprite pSprite = boardService.getSprite(rxBucket[i].index);
                if (boardService.getXSprite(pSprite.getExtra()) == null || !IsDudeSprite(pSprite)) {
                    continue;
                } else if (pSprite.getXvel() == boardService.getSprite(pXDude.getReference()).getXvel()) {
                    return pXSprite;
                }
            }
        }
        return null;
    }

    public static int getDataFieldOfObject(int objType, int objIndex, int dataIndex) {
        int data = -65535;
        switch (objType) {
            case SS_SPRITE:
                XSPRITE pXSprite = boardService.getXSprite(boardService.getSprite(objIndex).getExtra());
                switch (dataIndex) {
                    case 1:
                        return pXSprite.getData1();
                    case 2:
                        return pXSprite.getData2();
                    case 3:
                        return pXSprite.getData3();
                    case 4:
                        return pXSprite.getData4();
                    default:
                        return data;
                }
            case SS_SECTOR:
                return xsector[boardService.getSector(objIndex).getExtra()].data;
            case SS_WALL:
                return xwall[boardService.getWall(objIndex).getExtra()].data;
            default:
                return data;
        }
    }

    public static boolean setDataValueOfObject(int objType, int objIndex, int dataIndex, int value) {
        switch (objType) {
            case SS_SPRITE:
                XSPRITE pXSprite = boardService.getXSprite(boardService.getSprite(objIndex).getExtra());
                switch (dataIndex) {
                    case 1:
                        pXSprite.setData1(value);
                        return true;
                    case 2:
                        pXSprite.setData2(value);
                        return true;
                    case 3:
                        pXSprite.setData3(value);
                        return true;
                    case 4:
                        pXSprite.setData4(value);
                        return true;
                    default:
                        return false;
                }
            case SS_SECTOR:
                xsector[boardService.getSector(objIndex).getExtra()].data = (short) value;
                return true;
            case SS_WALL:
                xwall[boardService.getWall(objIndex).getExtra()].data = (short) value;
                return true;
            default:
                return false;
        }
    }

    public static boolean goalValueIsReached(XSPRITE pXSprite) {
        for (int i = bucketHead[pXSprite.getTxID()]; i < bucketHead[pXSprite.getTxID() + 1]; i++) {
            if (getDataFieldOfObject(rxBucket[i].type, rxBucket[i].index, pXSprite.getData1()) != pXSprite.getData3()) {
                return false;
            }
        }
        return true;
    }

    public static void activateDudes(int rx) {
        for (int i = bucketHead[rx]; i < bucketHead[rx + 1]; i++) {
            if (rxBucket[i].type != SS_SPRITE || !IsDudeSprite(boardService.getSprite(rxBucket[i].index))) {
                continue;
            }
            aiInit(boardService.getSprite(rxBucket[i].index), false);
        }
    }

    public static Sprite getMateTargets(XSPRITE pXSprite) {
        int rx = pXSprite.getRxID();
        Sprite pMate = null;
        XSPRITE pXMate = null;
        for (int i = bucketHead[rx]; i < bucketHead[rx + 1]; i++) {

            if (rxBucket[i].type == SS_SPRITE) {
                pMate = boardService.getSprite(rxBucket[i].index);
                pXMate = boardService.getXSprite(pMate.getExtra());
                if (pXMate == null || pMate.getXvel() == boardService.getSprite(pXSprite.getReference()).getXvel() || !Actor.IsDudeSprite(pMate)) {
                    continue;
                }

                if (pXMate.getTarget() > -1) {
                    if (!IsPlayerSprite(boardService.getSprite(pXMate.getTarget()))) {
                        return boardService.getSprite(pXMate.getTarget());
                    }
                }

            }
        }

        return null;
    }

    public static boolean isMatesHaveSameTarget(XSPRITE pXLeader, Sprite pTarget, int allow) {
        int rx = pXLeader.getRxID();
        Sprite pMate = null;
        XSPRITE pXMate = null;
        //if (rx <= 0) return false;
        for (int i = bucketHead[rx]; i < bucketHead[rx + 1]; i++) {

            if (rxBucket[i].type != SS_SPRITE) {
                continue;
            }

            pMate = boardService.getSprite(rxBucket[i].index);
            pXMate = boardService.getXSprite(pMate.getExtra());
            if (pXMate == null || pMate.getXvel() == boardService.getSprite(pXLeader.getReference()).getXvel() || !IsDudeSprite(pMate)) {
                continue;
            }


            if (pXMate.getTarget() == pTarget.getXvel() && allow-- <= 0) {
                return true;
            }
        }

        return false;

    }


    public static void trTextOver(int nMessage) {
        if (nMessage >= kMaxMessages) {
            throw new AssertException("nMessage < kMaxMessages");
        }
        if (currentEpisode != null) {
            MapInfo pMap = currentEpisode.gMapInfo[pGameInfo.nLevel];
            if (pMap != null && pMap.gMessage[nMessage] != null) {
                viewSetMessage(pMap.gMessage[nMessage], gPlayer[gViewIndex].nPlayer, 2);
            }
        }
    }

    public static void FireballTrapCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        int dx, dy, dz;

        if ((pSprite.getCstat() & kSpriteFloor) != 0)        // floor sprite
        {
            dx = dy = 0;
            if ((pSprite.getCstat() & kSpriteFlipY) != 0)    // face down floor sprite
            {
                dz = 1 << 14;
            } else                                    // face up floor sprite
            {
                dz = -1 << 14;
            }
        } else                                        // wall sprite or face sprite
        {
            dx = Cos(pSprite.getAng()) >> 16;
            dy = Sin(pSprite.getAng()) >> 16;
            dz = 0;
        }
        actFireMissile(pSprite, 0, 0, dx, dy, dz, kMissileFireball);
    }

    public static void UniMissileTrapCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int dx = 0, dy = 0, dz = 0;
        BloodSprite pSprite = boardService.getSprite(pXSprite.getReference());

        if (pXSprite.getData1() < kMissileBase || pXSprite.getData1() >= kMissileMax) {
            return;
        }

        if ((pSprite.getCstat() & kSpriteFloor) != 0) {
            if ((pSprite.getCstat() & kSpriteFlipY) != 0) {
                dz = 1 << 14;
            } else {
                dz = -1 << 14;
            }
        } else {
            dx = Cos(pSprite.getAng()) >> 16;
            dy = Sin(pSprite.getAng()) >> 16;
            dz = pXSprite.getData3() << 6; // add slope controlling
            if (dz > 0x10000) {
                dz = 0x10000;
            } else if (dz < -0x10000) {
                dz = -0x10000;
            }
        }


        BloodSprite pMissile = null;
        pMissile = actFireMissile(pSprite, 0, 0, dx, dy, dz, pXSprite.getData1());
        if (pMissile != null) {

            // inherit some properties of the generator
            if ((pSprite.getHitag() & 0x0001) != 0) {

                pMissile.setXrepeat(pSprite.getXrepeat());
                pMissile.setYrepeat(pSprite.getYrepeat());

                pMissile.setPal(pSprite.getPal());
                pMissile.setShade(pSprite.getShade());

            }

            // add velocity controlling
            if (pXSprite.getData2() > 0) {

                long velocity = pXSprite.getData2() << 12;
                pMissile.setVelocity(mulscale(dx, velocity, 14),
                mulscale(dy, velocity, 14),
                mulscale(dz, velocity, 14));

            }

            // add bursting for missiles
            if (pMissile.getLotag() != kMissileStarburstFlare && pXSprite.getData4() > 0) {
                evPostCallback(pMissile.getXvel(), SS_SPRITE, Integer.toUnsignedLong((pXSprite.getData4() > 500) ? 500 : pXSprite.getData4() - 1), 23);
            }

        }
    }

    public static void MGunOpenCallback(int nXIndex) {
        seqSpawn(getSeq(kMGunFire), SS_SPRITE, nXIndex, callbacks[MGunFireCallback]);
    }

    public static void MGunFireCallback(int nXIndex) {
        XSPRITE pXSprite = boardService.getXSprite(nXIndex);
        int nSprite = pXSprite.getReference();
        BloodSprite pSprite = boardService.getSprite(nSprite);

        // if dynamic ammo left or infinite ammo
        if (pXSprite.getData2() > 0 || pXSprite.getData1() == 0) {
            if (pXSprite.getData2() > 0) {
                pXSprite.setData2(pXSprite.getData2() - 1);    // subtract ammo
                if (pXSprite.getData2() == 0) {
                    evPost(nSprite, SS_SPRITE, 1, kCommandOff);    // empty! turn it off
                }
            }
            int dx = (Cos(pSprite.getAng()) >> 16) + BiRandom(1000);
            int dy = (Sin(pSprite.getAng()) >> 16) + BiRandom(1000);
            int dz = BiRandom(1000);

            actFireVector(pSprite, 0, 0, dx, dy, dz, 2); //kVectorBullet
            sfxStart3DSound(pSprite, 359, -1, 0); //kSfxTomFire
        }
    }

    public static int getWave(XSECTOR pXSector, int nBusy) {
        int wave;

        if (pXSector.busy >= nBusy) {
            wave = pXSector.waveTime[0];
        } else {
            wave = pXSector.waveTime[1];
        }

        return wave;
    }

    public static void InitGenerator(int nSprite) {
        if (!boardService.isValidSprite(nSprite)) {
            throw new AssertException("isValidSprite(nSprite)");
        }
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite.getStatnum() == kMaxStatus) {
            throw new AssertException("pSprite.statnum != kMaxStatus");
        }

        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        switch (pSprite.getLotag()) {
            case kGDXDudeTargetChanger:
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteBlocking);
                pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                if (pXSprite.getBusyTime() <= 0) {
                    pXSprite.setBusyTime(5);
                }
                if (pXSprite.getState() != pXSprite.getRestState()) {
                    evPost(nSprite, SS_SPRITE, 0, kCommandRespawn);
                }
                return;
            case kGDXObjDataAccumulator:
            case kGDXSeqSpawner:
            case kGDXEffectSpawner:
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteBlocking);
                pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                if (pXSprite.getState() != pXSprite.getRestState()) {
                    evPost(nSprite, SS_SPRITE, 0, kCommandRespawn);
                }
                return;
            case kGDXWindGenerator:
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteBlocking);
                if (pXSprite.getState() != pXSprite.getRestState()) {
                    evPost(nSprite, SS_SPRITE, 0, kCommandRespawn);
                }
                return;
            case kWeaponItemBase: // Random weapon
            case kAmmoItemRandom: // Random ammo
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteBlocking);
                pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                if (pXSprite.getState() != pXSprite.getRestState()) {
                    evPost(nSprite, SS_SPRITE, 0, kCommandRespawn);
                }
                return;
            case kGenTrigger:
                pSprite.setCstat(pSprite.getCstat() & ~kSpriteBlocking);
                pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                break;
            default:
                break;
        }

        if (pXSprite.getState() != pXSprite.getRestState() && pXSprite.getBusyTime() > 0) {
            evPost(nSprite, SS_SPRITE, Integer.toUnsignedLong((pXSprite.getBusyTime() + BiRandom(pXSprite.getData1())) * kTimerRate) / 10L, kCommandRespawn);
        }
    }

    public static void ActivateGenerator(int nSprite) {
        if (!boardService.isValidSprite(nSprite)) {
            throw new AssertException("isValidSprite(nSprite)");
        }
        BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite.getStatnum() == kMaxStatus) {
            throw new AssertException("pSprite.statnum != kMaxStatus");
        }

        //int nXSprite = pSprite.getExtra();
        XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
        if (pXSprite == null) {
            throw new AssertException("pXSprite != null");
        }

        switch (pSprite.getLotag()) {
            case kWeaponItemBase:
            case kAmmoItemRandom:
                // let's first search for previously dropped items and remove it
                if (pXSprite.getDropMsg() > 0) {
                    for (ListNode<Sprite> node = boardService.getStatNode(kStatItem); node != null; node = node.getNext()) {
                        Sprite pItem = node.get();
                        if (pItem.getLotag() == pXSprite.getDropMsg() && pItem.getX() == pSprite.getX() && pItem.getY() == pSprite.getY() && pItem.getZ() == pSprite.getZ()) {
                            actSpawnEffect(29, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), pSprite.getZ(), 0);
                            engine.deletesprite(node.getIndex());
                            break;
                        }
                    }
                }
                // then drop item
                DropRandomPickupObject(pSprite, pXSprite.getDropMsg());
                break;
            case kGenWaterDrip:
                GetSpriteExtents(pSprite);
                actSpawnThing(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), extents_zBot, kThingWaterDrip);
                break;
            case kGenBloodDrip:
                GetSpriteExtents(pSprite);
                actSpawnThing(pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), extents_zBot, kThingBloodDrip);
                break;
            case kGenSFX:
                if (IsOriginalDemo()) {
                    sfxStart3DSound(pSprite, pXSprite.getData2(), -1, 0);
                } else {
                    long pitch = ((long) pXSprite.getData4() << 1);
                    int volume = pXSprite.getData3();
                    sfxStart3DSoundCP(pSprite, pXSprite.getData2(), -1, 0, (pitch < 2000) ? 0 : pitch, volume);
                }
                break;
            case kGenEctoSkull: // can shot any projectile, default 307 (ectoskull)
                if (!IsOriginalDemo()) {
                    UniMissileTrapCallback(pSprite.getExtra());
                }
                break;
            case kGenDart:
                break;
            case kGenFireball:
                switch (pXSprite.getData2()) {
                    case 0:
                        FireballTrapCallback(pSprite.getExtra());
                        break;
                    case 1:
                        seqSpawn(getSeq(kFireTrap1), SS_SPRITE, pSprite.getExtra(), callbacks[FireballTrapCallback]);
                        break;
                    case 2:
                        seqSpawn(getSeq(kFireTrap2), SS_SPRITE, pSprite.getExtra(), callbacks[FireballTrapCallback]);
                        break;
                }
                break;
            case kGenBubble:
                GetSpriteExtents(pSprite);
                actSpawnEffect(23, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), extents_zTop, 0);
                break;
            case kGenMultiBubble:
                GetSpriteExtents(pSprite);
                actSpawnEffect(26, pSprite.getSectnum(), pSprite.getX(), pSprite.getY(), extents_zTop, 0);
                break;
        }
    }

}

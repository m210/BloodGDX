package ru.m210projects.Blood.Types;

import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.LinkedMap;
import ru.m210projects.Build.Types.collections.LinkedList;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.collections.ValueSetter;

import java.util.List;

import static ru.m210projects.Blood.Globals.kSpriteOriginAlign;
import static ru.m210projects.Build.Engine.MAXSPRITES;

public class BloodSpriteMap extends LinkedMap<Sprite> {

    public BloodSpriteMap(int listCount, List<Sprite> spriteList, ValueSetter<Sprite> valueSetter) {
        super(listCount, spriteList, MAXSPRITES, valueSetter);
    }

    @Override
    protected int insert(ListNode<Sprite> node, int value) {
        final LinkedList<Sprite> list = basket[value];
        list.addLast(node);
        setValue(node, value);
        return node.getIndex();
    }

    @Override
    protected BloodSprite getInstance() {
        BloodSprite sprite = new BloodSprite();

        sprite.setXrepeat(0);
        sprite.setYrepeat(0);
        sprite.setExtra(0);
        sprite.setClipdist(0);
        sprite.setOwner(0);

        sprite.setSectnum(-1);
        sprite.setXvel(-1);
        sprite.setCstat(kSpriteOriginAlign); // initialize default cstat for sprites

        return sprite;
    }
}

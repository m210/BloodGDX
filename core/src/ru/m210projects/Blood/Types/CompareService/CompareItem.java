package ru.m210projects.Blood.Types.CompareService;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface CompareItem {

    boolean compare(InputStream is) throws IOException;

    void write(OutputStream os) throws IOException;

    String getName();

}

package ru.m210projects.Blood.Types.CompareService;

import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Build.osd.Console;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Blood.Gameutils.bseed;
import static ru.m210projects.Blood.Globals.pGameInfo;
import static ru.m210projects.Blood.Types.DemoUtils.demfile;

public class CompareService {

    private static OutputStream writer;
    private static InputStream reader;
    private static Type type;

    public static void prepare(Path path, Type t) {
        type = t;
        try {
            if (type == Type.Write) {
                if (writer != null) {
                    writer.close();
                }
                writer = new BufferedOutputStream(Files.newOutputStream(path));
            } else {
                if (reader != null) {
                    reader.close();
                }
                reader = new BufferedInputStream(Files.newInputStream(path));
            }
        } catch (FileNotFoundException | NoSuchFileException fnf) {
            reader = null;
            writer = null;

            Console.out.println(path + " is not found!", OsdColor.RED);
        } catch (IOException e) {
            reader = null;
            writer = null;
            e.printStackTrace();
        }
    }

    public static void update() {
        if (type == null) {
            return;
        }

        switch (type) {
            case Read:
                if (reader != null) {
                    try {
                        Packet packet = new Packet(demfile.rcnt);
                        if (!packet.read(reader)) {
//                            throw new Error("Unsync at " + packet.rcnt);
                            System.err.println("Unsync at " + packet.rcnt);
                        } else {
                            System.out.println("\033[32mDemo file " + pGameInfo.zLevelName + " is OK\033[0m");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        reader = null;
                    }
                }
                break;
            case Write:
                if (writer != null) {
                    try {
                        Packet packet = new Packet(demfile.rcnt);
                        packet.write(writer);
                    } catch (IOException e) {
                        e.printStackTrace();
                        writer = null;
                    }
                }
                break;
        }
    }

    public enum Type {
        Read, Write
    }

    public static class Packet {
        private final int rcnt;
        private final CompareItem[] items;

        public Packet(int rcnt) {
            this.rcnt = rcnt;

            List<CompareItem> list = new ArrayList<>();
            list.add(new IntegerItem(bseed, "bseed"));
//            list.add(new QueueItem(origEventQ.qList, origEventQ.fNodeCount));
//            list.add(new SpriteListItem());
//            list.add(new SpriteLinkedListItem());

//            Long[] xvel = new Long[MAXSPRITESV7];
//            Long[] yvel = new Long[MAXSPRITESV7];
//            Long[] zvel = new Long[MAXSPRITESV7];
//            for (int j = 0; j < MAXSPRITESV7; j++) {
//                BloodSprite sprite = boardService.getSprite(j);
//                xvel[j] = sprite.getVelocityX();
//                yvel[j] = sprite.getVelocityY();
//                zvel[j] = sprite.getVelocityZ();
//            }
//
//            list.add(new ArrayItem(xvel, "xvel"));
//            list.add(new ArrayItem(yvel, "yvel"));
//            list.add(new ArrayItem(zvel, "zvel"));

            this.items = new CompareItem[list.size()];
            list.toArray(items);
        }

        public boolean read(InputStream is) throws IOException {
            boolean[] equals = new boolean[items.length + 1];
            equals[0] = rcnt == StreamUtils.readInt(is);
            for (int i = 0; i < items.length; i++) {
                equals[i + 1] = items[i].compare(is);
            }

            for (boolean equal : equals) {
                if (!equal) {
                    return false;
                }
            }
            return true;
        }

        public void write(OutputStream os) throws IOException {
            StreamUtils.writeInt(os, rcnt);
            for (CompareItem item : items) {
                item.write(os);
            }
        }
    }
}

package ru.m210projects.Blood.Types.CompareService;

import ru.m210projects.Blood.PriorityQueue.PriorityItem;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Blood.EVENT.kPQueueSize;

public class QueueItem implements CompareItem {

    private final PriorityItem[] qList;
    private final int nodeCount;

    public QueueItem(PriorityItem[] qList, int nodeCount) {
        this.nodeCount = nodeCount;
        this.qList = qList;
    }

    @Override
    public boolean compare(InputStream is) throws IOException {
        int readCount =  StreamUtils.readInt(is);
        boolean equals = nodeCount == readCount;
        for (int i = 0; i < kPQueueSize; i++) {
            PriorityItem item = qList[i];
            int readPriority = StreamUtils.readInt(is);
            int readEvent = StreamUtils.readInt(is);
            if (item.priority != readPriority || item.event != readEvent) {
                System.out.println("Unsync in " + getName() + ": " + i);
                equals = false;
            }
        }
        return equals;
    }

    @Override
    public void write(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, nodeCount);
        for (int i = 0; i < kPQueueSize; i++) {
            StreamUtils.writeInt(os, (int) qList[i].priority);
            StreamUtils.writeInt(os, qList[i].event);
        }
    }

    @Override
    public String getName() {
        return "PriorityQueue";
    }
}

package ru.m210projects.Blood.Types.CompareService;

import ru.m210projects.Build.Types.Sprite;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SpriteItem extends StructItem<Sprite> {

    public SpriteItem(Sprite objectStruct, int index) {
        super(objectStruct, index);
    }

    @Override
    protected Sprite readObject(InputStream is) throws IOException {
        return new Sprite().readObject(is);
    }

    @Override
    public void write(OutputStream os) throws IOException {
        objectStruct.writeObject(os);
    }
}

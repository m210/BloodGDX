package ru.m210projects.Blood.Types.CompareService;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Build.Engine.*;

public class SpriteLinkedListItem implements CompareItem {

    @Override
    public boolean compare(InputStream is) throws IOException {
        boolean equals = true;
        for (int i = 0; i < MAXSTATUS; i++) {
            List<Integer> list = new ArrayList<>();
            int size = StreamUtils.readInt(is);
            for (int j = 0; j < size; j++) {
                list.add(StreamUtils.readInt(is));
            }

            if (boardService.getStatNode(i) != null && size > 0) {
                int index = 0;
                for (ListNode<Sprite> node = boardService.getStatNode(i); node != null; node = node.getNext()) {
                    int compValue = index < size ? list.get(index++) : -1;
                    if (compValue != node.getIndex()) {
                        System.out.println("Unsync in " + getName() + "::stat: " + i + " at index " + compValue + "(read) != (this) " + node.getIndex());
                        equals = false;
                    }
                }
            } else if (boardService.getStatNode(i) != null || size > 0) {
                System.out.println("Unsync in " + getName() + "::stat: " + i);
                equals = false;
            }
        }

        for (int i = 0; i < boardService.getSectorCount(); i++) {
            List<Integer> list = new ArrayList<>();
            int size = StreamUtils.readInt(is);
            for (int j = 0; j < size; j++) {
                list.add(StreamUtils.readInt(is));
            }

            if (boardService.getSectNode(i) != null && size > 0) {
                int index = 0;
                for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                    int compValue = index < size ? list.get(index++) : -1;
                    if (compValue != node.getIndex()) {
                        System.out.println("Unsync in " + getName() + "::sect: " + i + " at index " + compValue + "(read) != (this) " + node.getIndex());
                        equals = false;
                    }
                }
            } else if (boardService.getSectNode(i) != null || size > 0) {
                System.out.println("Unsync in " + getName() + "::sect: " + i);
                equals = false;
            }
        }

        return equals;
    }

    @Override
    public void write(OutputStream os) throws IOException {
        for (int i = 0; i < MAXSTATUS; i++) {
            if (boardService.getStatNode(i) != null) {
                for (ListNode<Sprite> node = boardService.getStatNode(i); node != null; node = node.getNext()) {
                    StreamUtils.writeInt(os, node.getIndex());
                }
            }
        }

        for (int i = 0; i < boardService.getSectorCount(); i++) {
            if (boardService.getSectNode(i) != null) {
                for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                    StreamUtils.writeInt(os, node.getIndex());
                }
            }
        }
    }

    @Override
    public String getName() {
        return "SpriteLinkedListItem";
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import com.badlogic.gdx.ScreenAdapter;
import ru.m210projects.Blood.filehandlers.DemoFile;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.m210projects.Blood.Globals.kNetModeOff;
import static ru.m210projects.Blood.Globals.pGameInfo;
import static ru.m210projects.Blood.Main.*;

public class DemoUtils {

    public static Map<Group, List<Entry>> demofiles = new HashMap<>();
    public static int nDemonum = -1;
    public static DemoFile demfile = null;

    public static boolean checkDemoEntry(Entry file, boolean showErrorMessage) {
        if (file.exists() && file.isExtension("dem")) {
            try (InputStream is = file.getInputStream()) {
                String signature = StreamUtils.readString(is, 4);
                int version = StreamUtils.readShort(is);
                if (signature.equals(DemoFile.kBloodDemSig) && (version == 277)) {
                    return true;
                } else if (showErrorMessage) {
                    /*
                     * 266 - v1.10 267 - v1.11 277 - v1.21
                     */
                    String demVersion = "unknown";
                    if (version == 256) demVersion = "v1.00";
                    if (version == 266) demVersion = "v1.10";
                    else if (version == 267) demVersion = "v1.11";

                    String name = file.getName();
                    Console.out.println("Wrong version of the demofile found: " + name + " (" + demVersion + " != v1.21)", OsdColor.RED);
                }
            } catch (Exception ignore) {
            }
        }
        return false;
    }

    public static List<Entry> checkDemoEntry(Group group) {
        if (demofiles.containsKey(group)) {
            return demofiles.get(group);
        }

        nDemonum = -1;
        List<Entry> demos = group.stream()
                .filter(e -> checkDemoEntry(e, true))
                .sorted(Entry::compareTo)
                .collect(Collectors.toList());

        demofiles.put(group, demos);
        Console.out.println("There are " + demos.size() + " demo(s) in the loop", OsdColor.YELLOW);

        if (cfg.gDemoSeq == 2) {
            int nextnum = nDemonum;
            if (demos.size() > 1) {
                while (nextnum == nDemonum) nextnum = (int) (Math.random() * (demos.size()));
            }
            nDemonum = nextnum;
        }

        return demos;
    }

    public static boolean IsOriginalDemo(final ScreenAdapter screen) {
        return (screen == gDemoScreen && demfile != null && demfile.nVersion == 277) || (pGameInfo.nGameType == kNetModeOff && cfg.gVanilla);
    }

    public static boolean IsOriginalDemo() {
        return (pGameInfo.nGameType == kNetModeOff && cfg.gVanilla) || (game.isCurrentScreen(gDemoScreen) && demfile != null && demfile.nVersion == 277);
    }
}

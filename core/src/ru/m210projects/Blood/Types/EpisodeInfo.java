// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import ru.m210projects.Blood.filehandlers.BloodIniFile;

import static ru.m210projects.Blood.LEVELS.kMaxMap;

public class EpisodeInfo {

    public BloodIniFile iniFile = null;
    public String Title = "";
    public int nMaps = 0;

    public MapInfo[] gMapInfo = new MapInfo[kMaxMap + 1];
    public int BBOnly = 0;
    public int CutSceneALevel = 0;

    public String CutSceneA;
    public String CutSceneB;
    public int CutA;
    public int CutB;
    public String CutWavA;
    public String CutWavB;

    public void clear() {
        iniFile = null;
        Title = null;
        nMaps = 0;
        BBOnly = 0;
        CutSceneALevel = 0;

        for (int i = 0; i <= kMaxMap; i++)
            if (gMapInfo[i] != null)
                gMapInfo[i].clear();

        CutSceneA = null;
        CutSceneB = null;
        CutA = 0;
        CutB = 0;
        CutWavA = null;
        CutWavB = null;
    }

    public boolean hasCutsceneA(int nLvl) {
        return CutSceneALevel == nLvl && CutSceneA != null && !CutSceneA.isEmpty();
    }

    public boolean hasCutsceneB(int nLvl) {
        return CutSceneB != null && !CutSceneB.isEmpty();
    }

    public String getIniName() {
        if (iniFile != null) {
            return iniFile.getName();
        }
        return "";
    }

    public MapInfo getMapInfo(int nMap) {
        if (nMap >= 0 && nMap < gMapInfo.length && gMapInfo[nMap] != null && gMapInfo[nMap].MapName != null) {
            return gMapInfo[nMap];
        }
        return null;
    }
}

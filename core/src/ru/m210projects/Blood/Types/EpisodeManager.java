package ru.m210projects.Blood.Types;

import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Blood.filehandlers.EpisodeEntry;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.FileEntry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.m210projects.Blood.LEVELS.kMaxEpisode;
import static ru.m210projects.Blood.Main.game;

public class EpisodeManager {

    private final Map<String, BloodIniFile> episodeCache = new HashMap<>();

    public void putEpisode(BloodIniFile bloodIniFile) {
        episodeCache.put(bloodIniFile.getEpisodeEntry().getHashKey(), bloodIniFile);
    }

    public List<EpisodeEntry> getEpisodeEntries(FileEntry file) {
        List<EpisodeEntry> list = new ArrayList<>();
        if (file.isExtension("zip")
                || file.isExtension("pk3")
                || file.isExtension("grp")
                || file.isExtension("rff")) {

            Group group = game.getCache().getGroup(file.getRelativePath().toString());
            if (group.isEmpty()) {
                group = game.getCache().newGroup(file);
            }

            for (Entry groupEntry : group.getEntries()) {
                if (groupEntry.isExtension("ini")) {
                    EpisodeEntry.Pack entry = new EpisodeEntry.Pack(file, groupEntry);
                    BloodIniFile ini = episodeCache.computeIfAbsent(entry.getHashKey(), e -> buildEpisode(entry));
                    if (ini != null) {
                        list.add(entry);
                    }
                }
            }
        } else if (file.isExtension("ini")) {
            EpisodeEntry.File entry = new EpisodeEntry.File(file);
            BloodIniFile ini = episodeCache.computeIfAbsent(entry.getHashKey(), e -> buildEpisode(entry));
            if (ini != null) {
                list.add(entry);
            }
        }
        return list;
    }

    public BloodIniFile getEpisode(EpisodeEntry entry) {
        if (entry != null) {
            return episodeCache.get(entry.getHashKey());
        }
        return null;
    }

    private BloodIniFile buildEpisode(EpisodeEntry file) {
        BloodIniFile ini = new BloodIniFile(file);

        for (int i = 0; i < kMaxEpisode; i++) {
            if (ini.set("Episode" + (i + 1))) {
                return ini;
            }
        }

        return null;
    }

}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class NETINFO implements Serializable<NETINFO> {

    public int nGameType = 2;
    public int nEpisode;
    public int nLevel;
    public int nDifficulty;
    public int nMonsterSettings;
    public int nWeaponSettings;
    public int nItemSettings;
    public short nFriendlyFire;
    public boolean nReviveMode;
    public int nFragLimit;

    @Override
    public NETINFO readObject(InputStream is) throws IOException {
        nGameType = StreamUtils.readUnsignedByte(is);
        nEpisode = StreamUtils.readUnsignedByte(is);
        nLevel = StreamUtils.readUnsignedByte(is);
        nDifficulty = StreamUtils.readUnsignedByte(is);
        nMonsterSettings = StreamUtils.readUnsignedByte(is);
        nWeaponSettings = StreamUtils.readUnsignedByte(is);
        nItemSettings = StreamUtils.readUnsignedByte(is);
        nFriendlyFire = StreamUtils.readShort(is);
        nReviveMode = StreamUtils.readBoolean(is);
        nFragLimit = StreamUtils.readInt(is);

        return this;
    }

    @Override
    public NETINFO writeObject(OutputStream os) throws IOException {
        StreamUtils.writeByte(os, (byte) nGameType);
        StreamUtils.writeByte(os, (byte) nEpisode);
        StreamUtils.writeByte(os, (byte) nLevel);
        StreamUtils.writeByte(os, (byte) nDifficulty);
        StreamUtils.writeByte(os, (byte) nMonsterSettings);
        StreamUtils.writeByte(os, (byte) nWeaponSettings);
        StreamUtils.writeByte(os, (byte) nItemSettings);
        StreamUtils.writeShort(os, nFriendlyFire);
        StreamUtils.writeBoolean(os, nReviveMode);
        StreamUtils.writeInt(os, nFragLimit);

        return this;
    }
}

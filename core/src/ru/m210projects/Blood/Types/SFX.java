// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

import static ru.m210projects.Build.StringUtils.toUnicode;

public class SFX implements Serializable<SFX> {
    public int relVol;
    public int pitch;
    public int pitchrange;
    public int format;
    public int loopStart;
    public String rawName;
    public ByteBuffer hResource;
    public int size;

    public SFX(Entry entry) throws IOException {
        try (InputStream is = entry.getInputStream()) {
            readObject(is);
        }
    }

    public SFX(int relVol, int pitch, int pitchrange, int format, int loopStart, String rawName) {
        this.relVol = relVol;
        this.pitch = pitch;
        this.pitchrange = pitchrange;
        if (format == -1) format = 0;
        this.format = format;
        this.loopStart = loopStart;
        this.rawName = toUnicode(rawName);
    }

    @Override
    public SFX readObject(InputStream is) throws IOException {
        relVol = StreamUtils.readInt(is);
        pitch = StreamUtils.readInt(is);
        pitchrange = StreamUtils.readInt(is);
        format = StreamUtils.readInt(is);
        if (format == -1) format = 0;
        loopStart = StreamUtils.readInt(is);
        rawName = StreamUtils.readString(is, is.available());
        rawName = toUnicode(rawName);
        return this;
    }

    @Override
    public SFX writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, relVol);
        StreamUtils.writeInt(os, pitch);
        StreamUtils.writeInt(os, pitchrange);
        StreamUtils.writeInt(os, format);
        StreamUtils.writeInt(os, loopStart);
        StreamUtils.writeString(os, rawName);

        return this;
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.LongArray;
import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.PriorityQueue.PriorityItem;
import ru.m210projects.Blood.Types.Seq.*;
import ru.m210projects.Blood.filehandlers.BloodIniFile;
import ru.m210projects.Build.Types.Hitscan;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.BitMap;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.filehandle.fs.NotFoundEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.*;

import static ru.m210projects.Blood.DB.kMaxXSectors;
import static ru.m210projects.Blood.DB.kMaxXWalls;
import static ru.m210projects.Blood.EVENT.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.kMaxMap;
import static ru.m210projects.Blood.LOADSAVE.*;
import static ru.m210projects.Blood.Main.game;
import static ru.m210projects.Blood.Mirror.MAXMIRRORS;
import static ru.m210projects.Blood.ResourceHandler.episodeManager;
import static ru.m210projects.Blood.Trigger.kMaxBusyArray;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.kMaxSequences;
import static ru.m210projects.Build.Engine.*;

public class SafeLoader {

    public short[] pskyoff = new short[MAXPSKYTILES];
    public short pskybits;
    public int parallaxyscale;

    public short connecthead;
    public short[] connectpoint2 = new short[kMaxPlayers];
    public int randomseed;

    public int visibility, parallaxvisibility;
    public byte automapping;
    public BitMap show2dsector = new BitMap(MAXSECTORSV7);
    public BitMap show2dwall = new BitMap(MAXWALLSV7);
    public BitMap show2dsprite = new BitMap(MAXSPRITESV7);
    public byte[] gotpic = new byte[(MAXTILES + 7) >> 3];

    public Hitscan safeHitInfo;

    //MapInfo

    public int numsectors, numwalls, numsprites;

    public List<Sector> sector;
    public List<Wall> wall;
    public List<Sprite> sprite;

    //Blood variables

    public Array<Vector2> kwall = new Array<>(true, MAXWALLSV7, Vector2.class);

    public IntArray secFloorZ = new IntArray(MAXSECTORSV7);
    public IntArray secCeilZ = new IntArray(MAXSECTORSV7);
    public IntArray secPath = new IntArray(MAXSECTORSV7);
    public LongArray floorVel = new LongArray(MAXSECTORSV7);
    public LongArray ceilingVel = new LongArray(MAXSECTORSV7);
    public IntArray gUpperLink = new IntArray(MAXSECTORSV7);
    public IntArray gLowerLink = new IntArray(MAXSECTORSV7);

    public Map<Integer, XSPRITE> xspriteMap = new HashMap<>();
    public XWALL[] xwall = new XWALL[kMaxXWalls];
    public XSECTOR[] xsector = new XSECTOR[kMaxXSectors];

    public int[] nextXWall = new int[kMaxXWalls];
    public int[] nextXSector = new int[kMaxXSectors];

    public GAMEINFO safeGameInfo = new GAMEINFO();

    public boolean showinvisibility;
    public boolean gNoClip, gFogMode, gFullMap, gPaused, gInfiniteAmmo, cheatsOn;
    public int gSkyCount;
    public int gFrameClock, gTicks, gFrame, gGameClock;

    public ZONE[] gStartZone = new ZONE[kMaxPlayers];

    public int mirrorcnt;
    public int[] MirrorType = new int[MAXMIRRORS];
    public int[] MirrorX = new int[MAXMIRRORS];
    public int[] MirrorY = new int[MAXMIRRORS];
    public int[] MirrorZ = new int[MAXMIRRORS];
    public int[] MirrorLower = new int[MAXMIRRORS];
    public int[] MirrorUpper = new int[MAXMIRRORS];
    public int MirrorSector;
    public int[] MirrorWall = new int[4];

    public SeqInst[] siWall = new SeqInst[kMaxXWalls], siMasked = new SeqInst[kMaxXWalls];
    public SeqInst[] siCeiling = new SeqInst[kMaxXSectors], siFloor = new SeqInst[kMaxXSectors];

    public short[] actListIndex = new short[kMaxSequences];
    public byte[] actListType = new byte[kMaxSequences];

    public int activeCount;

    public int[] rxBucketIndex = new int[kMaxChannels];
    public int[] rxBucketType = new int[kMaxChannels];
    public short[] bucketHead = new short[kMaxID + 1];

    public PriorityItem[] qEventItems = new PriorityItem[1025];
    public int fNodeCount;

    public int gBusyCount = 0;
    public BUSY[] gBusy = new BUSY[kMaxBusyArray];

    public int[] nTeamCount = new int[kMaxPlayers];
    public int gNetPlayers;

    public byte[] autoaim = new byte[kMaxPlayers];
    public byte[] slopetilt = new byte[kMaxPlayers];
    public byte[] skill = new byte[kMaxPlayers];
    public String[] name = new String[kMaxPlayers];

    public PLAYER[] safePlayer = new PLAYER[kMaxPlayers];

    public IntArray gWallExp = new IntArray(kMaxXWalls);
    public IntArray gSectorExp = new IntArray(MAXSECTORSV7);

    public Array<POSTPONE> gPost = new Array<>(true, MAXSPRITESV7, POSTPONE.class);
    public int gPostCount;

    public int gNextMap;
    public int foundSecret;
    public int totalSecrets;
    public int superSecrets;
    public int totalKills;
    public int kills;

    public int deliriumTilt = 0;
    public int deliriumTurn = 0;
    public int deliriumPitch = 0;

    public boolean gUserEpisode, gForceMap;
    public BloodIniFile addon;
    public String addonFileName;
    public String addonPackedIniName;
    private String message;

    public SafeLoader() {
        for (int i = 0; i < kMaxXWalls; i++)
            xwall[i] = new XWALL();
        for (int i = 0; i < kMaxXSectors; i++)
            xsector[i] = new XSECTOR();

        for (int i = 0; i < kMaxPlayers; i++) {
            gStartZone[i] = new ZONE();
            safePlayer[i] = new PLAYER();
        }

        for (int i = 0; i < kMaxXWalls; i++)
            siWall[i] = new WallInst();
        for (int i = 0; i < kMaxXWalls; i++)
            siMasked[i] = new MaskedWallInst();
        for (int i = 0; i < kMaxXSectors; i++)
            siCeiling[i] = new CeilingInst();
        for (int i = 0; i < kMaxXSectors; i++)
            siFloor[i] = new FloorInst();
        for (int i = 0; i < kMaxBusyArray; i++)
            gBusy[i] = new BUSY();

        safeHitInfo = new Hitscan();
    }

    public String getMessage() {
        return message;
    }

    public boolean load(InputStream is) {
        addon = null;
        addonFileName = null;
        addonPackedIniName = null;
        message = null;
        gUserEpisode = false;
        try {
            gPost.clear();
            xspriteMap.clear();
            kwall.clear();
            gSectorExp.clear();
            gWallExp.clear();
            secFloorZ.clear();
            secCeilZ.clear();
            secPath.clear();
            floorVel.clear();
            ceilingVel.clear();
            gUpperLink.clear();
            gLowerLink.clear();
            Arrays.fill(gotpic, (byte) 0);

            for (int i = 0; i < kMaxPlayers; i++) {
                autoaim[i] = -1;
                slopetilt[i] = -1;
                skill[i] = -1;
                name[i] = null;
            }

            StreamUtils.skip(is, SAVETIME + SAVENAME + SAVELEVELINFO + SAVESCREENSHOTSIZE);
            LoadGDXBlock(is);

            MyLoad110(is);
            cheatsOn = StreamUtils.readBoolean(is);
            WarpLoad(is);
            MirrorLoad(is);
            SeqLoad(is);
            EventLoad(is);
            TriggersLoad(is);
            PlayersLoad(is, currentGdxSave);
            ActorsLoad(is);
            gNextMap = StreamUtils.readInt(is);
            StatsLoad(is);
            ScreenLoad(is);

            if (gUserEpisode) { //try to find addon
                addon = findAddon(addonFileName, addonPackedIniName);
                if (addon == null) {
                    message = "Can't find user episode file: " + addonFileName;
                    gUserEpisode = false;

                    safeGameInfo.nEpisode = 0;
                    safeGameInfo.nLevel = kMaxMap;
                    gForceMap = true;
                }
            }

            if (is.available() == 0) return true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public void MyLoad110(InputStream is) throws IOException {
        LoadGameInfo(is);
        numsectors = StreamUtils.readInt(is);
        numwalls = StreamUtils.readInt(is);
        numsprites = StreamUtils.readInt(is);

        sector = new ArrayList<>(numsectors * 2);
        for (int i = 0; i < numsectors + 1; i++)
            sector.add(new Sector().readObject(is));

        wall = new ArrayList<>(numwalls * 2);
        for (int i = 0; i < numwalls + 4; i++)
            wall.add(new Wall().readObject(is));

        sprite = new ArrayList<>(numsprites * 2);
        for (int i = 0; i < numsprites; i++) {
            BloodSprite spr = new BloodSprite();
            sprite.add(spr.readObject(is));

            if (spr.getStatnum() < kStatFree && spr.getExtra() >= 0) {
                XSPRITE pXSprite = new XSPRITE();

                pXSprite.readObject(is);
                pXSprite.getSpriteHit().readObject(is);
                pXSprite.getSeqInst().readObject(is);
                pXSprite.getDudeExtra().readObject(is);
                xspriteMap.put((int) spr.getExtra(), pXSprite);
            }
        }

        randomseed = StreamUtils.readInt(is);
        StreamUtils.readUnsignedByte(is); //parallaxtype
        showinvisibility = StreamUtils.readBoolean(is);
        StreamUtils.readInt(is); //parallaxyoffs
        parallaxyscale = StreamUtils.readInt(is);
        visibility = StreamUtils.readInt(is);
        parallaxvisibility = StreamUtils.readInt(is);

        for (int i = 0; i < MAXPSKYTILES; i++)
            pskyoff[i] = StreamUtils.readShort(is);

        pskybits = StreamUtils.readShort(is);
        show2dsector.readObject(is);
        show2dwall.readObject(is);
        show2dsprite.readObject(is);

        automapping = (byte) StreamUtils.readUnsignedByte(is);
        gFrameClock = StreamUtils.readInt(is);
        gTicks = StreamUtils.readInt(is);
        gFrame = StreamUtils.readInt(is);
        gGameClock = StreamUtils.readInt(is);
        gPaused = StreamUtils.readBoolean(is);

        for (int i = 0; i < numwalls; i++) {
            kwall.add(new Vector2(StreamUtils.readInt(is), StreamUtils.readInt(is)));
        }

        for (int i = 0; i < numsectors; i++)
            secFloorZ.add(StreamUtils.readInt(is));
        for (int i = 0; i < numsectors; i++)
            secCeilZ.add(StreamUtils.readInt(is));
        for (int i = 0; i < numsectors; i++)
            floorVel.add(StreamUtils.readInt(is));
        for (int i = 0; i < numsectors; i++)
            ceilingVel.add(StreamUtils.readInt(is));

        safeHitInfo.hitsect = StreamUtils.readShort(is);
        safeHitInfo.hitwall = StreamUtils.readShort(is);
        safeHitInfo.hitsprite = StreamUtils.readShort(is);
        safeHitInfo.hitx = StreamUtils.readInt(is);
        safeHitInfo.hity = StreamUtils.readInt(is);
        safeHitInfo.hitz = StreamUtils.readInt(is);
        gForceMap = StreamUtils.readBoolean(is);
        StreamUtils.skip(is, 128); //size of xsystem and copyright

        for (int i = 0; i < kMaxXWalls; i++)
            nextXWall[i] = StreamUtils.readShort(is);
        for (int i = 0; i < kMaxXSectors; i++)
            nextXSector[i] = StreamUtils.readShort(is);
        for (int i = 0; i < kMaxXWalls; i++)
            xwall[i].free();

        for (int i = 0; i < numwalls; i++) {
            Wall pWall = wall.get(i);
            if (pWall.getExtra() > 0) {
                xwall[pWall.getExtra()].readObject(is);
            }
        }

        for (int i = 0; i < kMaxXSectors; i++)
            xsector[i].free();

        for (int i = 0; i < numsectors; i++) {
            Sector pSector = sector.get(i);
            if (pSector.getExtra() > 0) {
                xsector[pSector.getExtra()].readObject(is);
            }
        }

        gSkyCount = StreamUtils.readInt(is);
        gFogMode = StreamUtils.readBoolean(is);

        gNoClip = false;
        gFullMap = false;
    }

    public void LoadGameInfo(InputStream is) throws IOException {
        safeGameInfo.nGameType = StreamUtils.readUnsignedByte(is);
        safeGameInfo.nDifficulty = StreamUtils.readUnsignedByte(is);
        safeGameInfo.nEpisode = StreamUtils.readInt(is);
        safeGameInfo.nLevel = StreamUtils.readInt(is);
        Path path = FileUtils.getPath(StreamUtils.readString(is, 144).trim());
        safeGameInfo.zLevelName = game.getCache().getEntry(path, true);
        if (!safeGameInfo.zLevelName.exists()) {
            safeGameInfo.zLevelName = new NotFoundEntry(path);
        }
        safeGameInfo.zLevelSong = StreamUtils.readString(is, 144).trim();
        safeGameInfo.nTrackNumber = StreamUtils.readInt(is);

        safeGameInfo.uMapCRC = StreamUtils.readInt(is);
        safeGameInfo.nMonsterSettings = StreamUtils.readUnsignedByte(is);
        safeGameInfo.uGameFlags = StreamUtils.readInt(is);
        safeGameInfo.uNetGameFlags = StreamUtils.readInt(is);
        safeGameInfo.nWeaponSettings = StreamUtils.readUnsignedByte(is);
        safeGameInfo.nItemSettings = StreamUtils.readUnsignedByte(is);
        safeGameInfo.nRespawnSettings = StreamUtils.readUnsignedByte(is);
        safeGameInfo.nTeamSettings = StreamUtils.readUnsignedByte(is);
        safeGameInfo.nMonsterRespawnTime = StreamUtils.readInt(is);
        safeGameInfo.nWeaponRespawnTime = StreamUtils.readInt(is);
        safeGameInfo.nItemRespawnTime = StreamUtils.readInt(is);
        safeGameInfo.nSpecialRespawnTime = StreamUtils.readInt(is);
    }

    public void WarpLoad(InputStream is) throws IOException {
        for (int i = 0; i < kMaxPlayers; i++) {
            gStartZone[i].x = StreamUtils.readInt(is);
            gStartZone[i].y = StreamUtils.readInt(is);
            gStartZone[i].z = StreamUtils.readInt(is);
            gStartZone[i].sector = StreamUtils.readShort(is);
            gStartZone[i].angle = StreamUtils.readShort(is);
        }

        for (int i = 0; i < numsectors; i++) {
            gUpperLink.add(StreamUtils.readInt(is));
            gLowerLink.add(StreamUtils.readInt(is));
        }
    }

    public void MirrorLoad(InputStream is) throws IOException {
        mirrorcnt = StreamUtils.readInt(is);
        MirrorSector = StreamUtils.readInt(is);
        for (int i = 0; i < MAXMIRRORS; i++) {
            MirrorType[i] = StreamUtils.readShort(is);
            MirrorLower[i] = StreamUtils.readInt(is);
            MirrorX[i] = StreamUtils.readInt(is);
            MirrorY[i] = StreamUtils.readInt(is);
            MirrorZ[i] = StreamUtils.readInt(is);
            MirrorUpper[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 4; i++)
            MirrorWall[i] = StreamUtils.readInt(is);
    }

    public void SeqLoad(InputStream is) throws IOException {
        for (int i = 0; i < kMaxXWalls; i++) {
            siWall[i].readObject(is);
        }
        for (int i = 0; i < kMaxXWalls; i++) {
            siMasked[i].readObject(is);
        }
        for (int i = 0; i < kMaxXSectors; i++) {
            siCeiling[i].readObject(is);
        }
        for (int i = 0; i < kMaxXSectors; i++) {
            siFloor[i].readObject(is);
        }

        activeCount = StreamUtils.readInt(is);
        Arrays.fill(actListType, (byte) 0);
        Arrays.fill(actListIndex, (short) 0);

        for (int i = 0; i < activeCount; i++) {
            actListType[i] = StreamUtils.readByte(is);
            actListIndex[i] = StreamUtils.readShort(is);
            SeqInst pInst = GetInstance(actListType[i], actListIndex[i]);
            if (pInst != null) {
                if ((pInst.isPlaying() && !game.getCache().contains(pInst.getSeqIndex(), "SEQ")))
                    throw new FileNotFoundException("hSeq != null, id=" + pInst.getSeqIndex() + " \n\rWrong Blood.RFF version or file corrupt!");
            } else {
                throw new FileNotFoundException("pInst != null" + "\n\rWrong Blood.RFF version or file corrupt!");
            }
        }
    }

    private SeqInst GetInstance(int type, int nXIndex) {
        switch (type) {
            case SS_WALL:
                if (nXIndex <= 0 || nXIndex >= kMaxXWalls) return null;
                return siWall[nXIndex];

            case SS_CEILING:
                if (nXIndex <= 0 || nXIndex >= kMaxXSectors) return null;
                return siCeiling[nXIndex];

            case SS_FLOOR:
                if (nXIndex <= 0 || nXIndex >= kMaxXSectors) return null;
                return siFloor[nXIndex];

            case SS_SPRITE:
                if (!xspriteMap.containsKey(nXIndex)) return null;
                return xspriteMap.get(nXIndex).getSeqInst();

            case SS_MASKED:
                if (nXIndex <= 0 || nXIndex >= kMaxXWalls) return null;
                return siMasked[nXIndex];

        }

        return null;
    }

    public void EventLoad(InputStream is) throws IOException {
        fNodeCount = StreamUtils.readInt(is);
        for (int i = 0; i < fNodeCount; i++) {
            qEventItems[i] = new PriorityItem(StreamUtils.readInt(is), StreamUtils.readInt(is));
        }

        for (int i = 0; i < kMaxChannels; i++) {
            int data = StreamUtils.readInt(is);
            rxBucketIndex[i] = getIndex(data);
            rxBucketType[i] = getType(data);
        }

        for (int i = 0; i <= kMaxID; i++)
            bucketHead[i] = StreamUtils.readShort(is);
    }

    public void TriggersLoad(InputStream is) throws IOException {
        gBusyCount = StreamUtils.readInt(is);
        for (int i = 0; i < kMaxBusyArray; i++) {
            gBusy[i].nIndex = StreamUtils.readInt(is);
            gBusy[i].nDelta = StreamUtils.readInt(is);
            gBusy[i].nBusy = StreamUtils.readInt(is);
            gBusy[i].busyProc = StreamUtils.readUnsignedByte(is);
        }
        for (int i = 0; i < numsectors; i++)
            secPath.add(StreamUtils.readInt(is));
    }

    public void PlayersLoad(InputStream is, int nVersion) throws IOException {
        for (int i = 0; i < kMaxPlayers; i++)
            nTeamCount[i] = StreamUtils.readInt(is);
        gNetPlayers = StreamUtils.readInt(is);

        for (int i = 0; i < gNetPlayers - 1; i++)
            connectpoint2[i] = (short) (i + 1);
        connectpoint2[gNetPlayers - 1] = -1;

        byte[] plname = new byte[15];
        for (int i = 0; i < kMaxPlayers; i++) {
            autoaim[i] = (byte) StreamUtils.readUnsignedByte(is);
            if (nVersion >= gdxSave + 2) slopetilt[i] = (byte) StreamUtils.readUnsignedByte(is);
            skill[i] = (byte) StreamUtils.readUnsignedByte(is);
            StreamUtils.readBytes(is, plname);
            name[i] = new String(plname).trim();
        }

        for (int i = 0; i < kMaxPlayers; i++) {
            safePlayer[i].pInput = new INPUT(is, nVersion);
            safePlayer[i].setVersion(nVersion).readObject(is);
        }
    }

    public void ActorsLoad(InputStream is) throws IOException {
        for (int i = 0; i < numsectors; i++)
            gSectorExp.add(StreamUtils.readShort(is));

        for (int i = 0; i < kMaxXWalls; i++) {
            gWallExp.add(StreamUtils.readShort(is));
        }

        gPostCount = StreamUtils.readInt(is);
        for (int i = 0; i < gPostCount; i++) {
            POSTPONE pPost = new POSTPONE();
            pPost.nSprite = StreamUtils.readShort(is);
            pPost.nStatus = StreamUtils.readShort(is);
            gPost.add(pPost);
        }
    }

    public void StatsLoad(InputStream is) throws IOException {
        totalSecrets = StreamUtils.readInt(is);
        foundSecret = StreamUtils.readInt(is);
        superSecrets = StreamUtils.readInt(is);
        totalKills = StreamUtils.readInt(is);
        kills = StreamUtils.readInt(is);
    }

    public void ScreenLoad(InputStream is) throws IOException {
        deliriumTilt = StreamUtils.readInt(is);
        deliriumTurn = StreamUtils.readInt(is);
        deliriumPitch = StreamUtils.readInt(is);
    }

    public void LoadUserEpisodeInfo(InputStream is) throws IOException {
        gUserEpisode = StreamUtils.readBoolean(is);
        if (gUserEpisode) {
            boolean isPacked = StreamUtils.readBoolean(is);
            addonFileName = StreamUtils.readDataString(is).toLowerCase();
            if (isPacked) {
                addonPackedIniName = StreamUtils.readDataString(is).toLowerCase();
            }
        }
    }

    public void LoadGDXBlock(InputStream is) throws IOException {
        LoadUserEpisodeInfo(is);

        byte[] data = StreamUtils.readBytes(is, SAVEGDXDATA);

        int pos = 0;
        safeGameInfo.nEnemyDamage = data[pos++];
        safeGameInfo.nEnemyQuantity = data[pos++];
        safeGameInfo.nDifficulty = data[pos++];
        safeGameInfo.nPitchforkOnly = data[pos++] == 1;
        gInfiniteAmmo = data[pos] == 1;
    }

    public BloodIniFile LoadGDXHeader(InputStream is) throws IOException {
        addon = null;
        addonFileName = null;
        addonPackedIniName = null;
        message = null;
        gUserEpisode = false;
        safeGameInfo.nDifficulty = -1;
        safeGameInfo.nEpisode = -1;
        safeGameInfo.nLevel = -1;

        StreamUtils.skip(is, SAVETIME + SAVENAME + SAVELEVELINFO + SAVESCREENSHOTSIZE);

        LoadUserEpisodeInfo(is);
        StreamUtils.skip(is, SAVEGDXDATA);
        LoadGameInfo(is);

        if (gUserEpisode) {//try to find addon
            addon = findAddon(addonFileName, addonPackedIniName);
        }

        return addon;
    }

    public static BloodIniFile findAddon(String addonFileName, String iniName) {
        try {
            FileEntry addonEntry = game.getCache().getGameDirectory().getEntry(FileUtils.getPath(addonFileName));
            if (addonEntry.exists()) {
                if (iniName == null) {
                    iniName = addonEntry.getName();
                }

                String finalIniName = iniName;
                return episodeManager.getEpisodeEntries(addonEntry)
                        .stream()
                        .filter(e -> e.getIniFile().getName().equalsIgnoreCase(finalIniName))
                        .map(e -> episodeManager.getEpisode(e))
                        .findAny().orElse(null);
            }
        } catch (Exception e) {
            Console.out.println(e.toString(), OsdColor.RED);
        }
        return null;
    }
}

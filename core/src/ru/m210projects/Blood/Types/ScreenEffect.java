// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import ru.m210projects.Build.Render.Types.ScreenFade;

public class ScreenEffect {

    public final static ScreenFade PICKUP_DAC = new PickupEffect();
    public final static ScreenFade HIT_DAC = new HitEffect();
    public final static ScreenFade BLIND_DAC = new BlindEffect();
    public final static ScreenFade DROWN_DAC = new DrownEffect();
    public final static ScreenFade[] SCREEN_DAC_ARRAY = {
            PICKUP_DAC, HIT_DAC, BLIND_DAC, DROWN_DAC
    };

    public static void resetDacEffects() {
        PICKUP_DAC.setIntensive(0);
        HIT_DAC.setIntensive(0);
        BLIND_DAC.setIntensive(0);
        DROWN_DAC.setIntensive(0);

        // FIXME: have to update palette
    }

    public static final class PickupEffect implements ScreenFade {

        private int pickupEffect;

        @Override
        public int getIntensive() {
            return pickupEffect;
        }

        @Override
        public PickupEffect setIntensive(int intensive) {
            this.pickupEffect = Math.min(255, intensive);
            return this;
        }

        @Override
        public String getName() {
            return "Pickup";
        }
    }

    public static final class HitEffect implements ScreenFade {

        private int hitEffect;

        @Override
        public int getRed() {
            return 2 * hitEffect;
        }

        @Override
        public int getGreen() {
            return -3 * hitEffect;
        }

        @Override
        public int getBlue() {
            return 3 * hitEffect;
        }

        @Override
        public int getIntensive() {
            return hitEffect;
        }

        @Override
        public ScreenFade setIntensive(int intensive) {
            this.hitEffect = Math.min(255, intensive);
            return this;
        }

        @Override
        public String getName() {
            return "Hit";
        }
    }

    public static final class BlindEffect implements ScreenFade {

        private int blindEffect;

        @Override
        public int getRed() {
            return -blindEffect;
        }

        @Override
        public int getGreen() {
            return -blindEffect;
        }

        @Override
        public int getIntensive() {
            return blindEffect;
        }

        @Override
        public ScreenFade setIntensive(int intensive) {
            this.blindEffect = Math.min(255, intensive);
            return this;
        }

        @Override
        public String getName() {
            return "Blind";
        }
    }

    public static final class DrownEffect implements ScreenFade {

        private int drownEffect;

        @Override
        public int getRed() {
            return -(drownEffect >> 6);
        }

        @Override
        public int getGreen() {
            return -(drownEffect >> 5);
        }

        @Override
        public int getBlue() {
            return (drownEffect >> 6);
        }

        @Override
        public int getIntensive() {
            return drownEffect;
        }

        @Override
        public ScreenFade setIntensive(int intensive) {
            this.drownEffect = intensive;
            return this;
        }

        @Override
        public String getName() {
            return "Drown";
        }
    }
}

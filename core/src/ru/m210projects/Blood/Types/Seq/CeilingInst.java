package ru.m210projects.Blood.Types.Seq;

import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sector;

import static ru.m210projects.Blood.DB.kMaxXSectors;
import static ru.m210projects.Blood.DB.xsector;
import static ru.m210projects.Blood.Main.boardService;

public class CeilingInst extends SeqInst {
    @Override
    protected void updateInstance(int index) {
        if (!(index > 0 && index < kMaxXSectors)) throw new AssertException("nXSector > 0 && nXSector < kMaxXSectors");
        int nSector = xsector[index].reference;
        if (!boardService.isValidSector(nSector)) throw new AssertException("boardService.isValidSector(nSector)");
        Sector pSector = boardService.getSector(nSector);
        if (pSector.getExtra() != index) throw new AssertException("pSector.extra == nXSector");

        SeqFrame pFrame = pSequence.getFrame(frameIndex);

        pSector.setCeilingpicnum((short) pFrame.nTile);
        pSector.setCeilingshade(pFrame.shade);
        pSector.setCeilingpal((short) pFrame.pal);
    }
}
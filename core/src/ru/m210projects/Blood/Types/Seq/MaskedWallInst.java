package ru.m210projects.Blood.Types.Seq;

import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Wall;

import static ru.m210projects.Blood.DB.kMaxXWalls;
import static ru.m210projects.Blood.DB.xwall;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;

public class MaskedWallInst extends SeqInst {

    @Override
    protected void updateInstance(int index) {
        if (!(index > 0 && index < kMaxXWalls)) throw new AssertException("nXWall > 0 && nXWall < kMaxXWalls");
        int nWall = xwall[index].reference;
        if (!boardService.isValidWall(nWall)) throw new AssertException("boardService.isValidWall(nWall)");
        Wall pWall = boardService.getWall(nWall);
        if (pWall.getExtra() != index) throw new AssertException("pWall.extra == nXWall");
        if (pWall.getNextwall() < 0) throw new AssertException("pWall.nextwall >= 0");        // it must be a 2 sided wall
        Wall pWall2 = boardService.getWall(pWall.getNextwall());

        SeqFrame pFrame = pSequence.getFrame(frameIndex);

        pWall.setOverpicnum(pFrame.nTile);
        pWall2.setOverpicnum(pFrame.nTile);

        if (pFrame.pal != 0) {
            pWall2.setPal(pFrame.pal);
            pWall.setPal(pFrame.pal);
        }

        if (pFrame.translucent) {
            pWall.setCstat(pWall.getCstat() | kWallTranslucent);
            pWall2.setCstat(pWall2.getCstat() | kWallTranslucent);
        } else {
            pWall.setCstat(pWall.getCstat() & ~kWallTranslucent);
            pWall2.setCstat(pWall2.getCstat() & ~kWallTranslucent);
        }

        if (pFrame.translucentR) {
            pWall.setCstat(pWall.getCstat() | kWallTranslucentR);
            pWall2.setCstat(pWall2.getCstat() | kWallTranslucentR);
        } else {
            pWall.setCstat(pWall.getCstat() & ~kWallTranslucentR);
            pWall2.setCstat(pWall2.getCstat() & ~kWallTranslucentR);
        }

        if (pFrame.blocking) {
            pWall.setCstat(pWall.getCstat() | kWallBlocking);
            pWall2.setCstat(pWall2.getCstat() | kWallBlocking);
        } else {
            pWall.setCstat(pWall.getCstat() & ~kWallBlocking);
            pWall2.setCstat(pWall2.getCstat() & ~kWallBlocking);
        }

        if (pFrame.hitscan) {
            pWall.setCstat(pWall.getCstat() | kWallHitscan);
            pWall2.setCstat(pWall2.getCstat() | kWallHitscan);
        } else {
            pWall.setCstat(pWall.getCstat() & ~kWallHitscan);
            pWall2.setCstat(pWall2.getCstat() & ~kWallHitscan);
        }
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types.Seq;

import ru.m210projects.Blood.Actor;
import ru.m210projects.Blood.PLAYER;
import ru.m210projects.Blood.Trigger;
import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Blood.Actor.DamageDude;
import static ru.m210projects.Blood.Actor.kAttrRespawn;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.EVENT.checkEventList;
import static ru.m210projects.Blood.EVENT.evPostCallback;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;

public class SeqHandling {

    public static final int kMaxSequences = 4096; //16384; MAXSPRITES!
    public static final int ReviveCallback = 1; //45
    public static final int MGunOpenCallback = 2; //44
    public static final int MGunFireCallback = 3; //43
    public static final int FireballTrapCallback = 4; //42
    public static final int DamageTreeCallback = 5; //51
    public static final int SmokeCallback = 6; //49
    public static final int FireballCallback = 7; //47
    public static final int TchernobogCallback1 = 8; //50
    public static final int TchernobogCallback2 = 9; //48
    public static final int DamageDude = 10; //52
    public static final int DamageDude2 = 11; //53
    public static final int FatalityDead = 12; //46
    public static final int UniMissileTrapCallback = 13; // Add new callback for EctoSkullGen and DartGen
    public static WallInst[] siWall = new WallInst[kMaxXWalls];
    public static MaskedWallInst[] siMasked = new MaskedWallInst[kMaxXWalls];
    public static CeilingInst[] siCeiling = new CeilingInst[kMaxXSectors];
    public static FloorInst[] siFloor = new FloorInst[kMaxXSectors];
    public static ActiveList activeList = new ActiveList(kMaxSequences);

    public static int getIndex(CALLPROC callback) {
        for (int i = 0; i < callbacks.length; i++)
            if (callback == callbacks[i]) return i;

        if (callback != null) //enemy callback
            return 0;

        return -1;
    }

    public static void SeqInit() {
        Console.out.println("Initializing SEQ animation");
        for (int i = 0; i < kMaxXWalls; i++)
            siWall[i] = new WallInst();
        for (int i = 0; i < kMaxXWalls; i++)
            siMasked[i] = new MaskedWallInst();
        for (int i = 0; i < kMaxXSectors; i++)
            siCeiling[i] = new CeilingInst();
        for (int i = 0; i < kMaxXSectors; i++)
            siFloor[i] = new FloorInst();
    }    public static CALLPROC[] callbacks = {
            nIndex -> { /* dummy */ },
            PLAYER::FReviveCallback,
            Trigger::MGunOpenCallback,
            Trigger::MGunFireCallback,
            Trigger::FireballTrapCallback,
            Actor::DamageTreeCallback,
            Actor::SmokeCallback,
            Actor::FireballCallback,
            Actor::TchernobogCallback1,
            Actor::TchernobogCallback2,
            nIndex -> DamageDude(nIndex, 8),
            nIndex -> DamageDude(nIndex, 3),
            PLAYER::FatalityDeadCallback,
            Trigger::UniMissileTrapCallback
    };

    public static SeqInst GetInstance(int type, int nXIndex) {
        switch (type) {
            case SS_WALL:
                if (!(nXIndex > 0 && nXIndex < kMaxXWalls))
                    throw new AssertException("nXIndex > 0 && nXIndex < kMaxXWalls: " + nXIndex);
                return siWall[nXIndex];

            case SS_CEILING:
                if (!(nXIndex > 0 && nXIndex < kMaxXSectors))
                    throw new AssertException("nXIndex > 0 && nXIndex < kMaxXSectors: " + nXIndex);
                return siCeiling[nXIndex];

            case SS_FLOOR:
                if (!(nXIndex > 0 && nXIndex < kMaxXSectors))
                    throw new AssertException("nXIndex > 0 && nXIndex < kMaxXSectors: " + nXIndex);
                return siFloor[nXIndex];

            case SS_SPRITE:
                XSPRITE pXSprite = boardService.getXSprite(nXIndex);
                if (pXSprite == null) {
                    throw new AssertException("pXSprite != null" + nXIndex);
                }
                return pXSprite.getSeqInst();

            case SS_MASKED:
                if (!(nXIndex > 0 && nXIndex < kMaxXWalls))
                    throw new AssertException("nXIndex > 0 && nXIndex < kMaxXWalls: " + nXIndex);
                return siMasked[nXIndex];
        }

        Console.out.println("Unexpected object type: " + type, OsdColor.RED);
        return null;
    }

    public static void seqKill(int type, int nXIndex) {
        SeqInst pInst = GetInstance(type, nXIndex);
        if (pInst != null && pInst.isPlaying) {
            if (activeList.remove(pInst) == -1)
                throw new AssertException("i < activeCount");    // should have been found

            UnlockInstance(pInst);
        }
    }

    public static void seqKillAll() {
        int i;

        for (i = 0; i < kMaxXWalls; i++) {
            if (siWall[i].isPlaying) UnlockInstance(siWall[i]);

            if (siMasked[i].isPlaying) UnlockInstance(siMasked[i]);
        }

        for (i = 0; i < kMaxXSectors; i++) {
            if (siCeiling[i].isPlaying) UnlockInstance(siCeiling[i]);
            if (siFloor[i].isPlaying) UnlockInstance(siFloor[i]);
        }

        for (ListNode<XSPRITE> node = boardService.getXSpriteNode(); node != null; node = node.getNext()) {
            SeqInst pInst = node.get().getSeqInst();
            if (pInst.isPlaying) {
                UnlockInstance(pInst);
            }
        }

        activeList.clear();
    }

    public static void seqSpawn(int nSeqID, int type, int nXIndex, CALLPROC callback) {
        SeqInst pInst = GetInstance(type, nXIndex);
        if (pInst == null) {
            throw new AssertException("pInst != null");
        }

        if (!game.getCache().contains(nSeqID, "SEQ")) {
            throw new AssertException("hSeq != null, id = " + nSeqID);
        }

        boolean inList = false;
        if (pInst.isPlaying) {
            // already playing this sequence?
            if (pInst.nSeqID == nSeqID) //hSeqID - filenum in RFF {
                return;

            inList = activeList.getIndex(pInst) != -1;
            UnlockInstance(pInst);
        }

        SeqType pSequence = SeqType.getInstance(nSeqID);

        pInst.pSequence = pSequence;
        pInst.nSeqID = nSeqID;
        pInst.callback = callback;
        pInst.isPlaying = true;
        pInst.timeCounter = (short) pSequence.ticksPerFrame;
        pInst.frameIndex = 0;

        if (!inList) {
            if (!activeList.add(pInst, nXIndex)) {
                throw new AssertException("activeCount < kMaxSequences");
            }
        }

        pInst.update(nXIndex);
    }

    private static void UnlockInstance(SeqInst pInst) {
        if (pInst == null) throw new AssertException("pInst != null");
        if (pInst.pSequence == null) throw new AssertException("pInst.pSequence != null");

//		gSysRes.Unlock( pInst.hSeq );
        pInst.pSequence = null;
        pInst.isPlaying = false;
    }

    public static int seqFrame(int type, int nXIndex) {
        SeqInst pInst = GetInstance(type, nXIndex);
        if (pInst != null && pInst.isPlaying) return pInst.frameIndex;
        return -1;
    }

    public static void seqProcess(int nTicks) {
        for (int i = 0; i < activeList.getSize(); i++) {
            short index = activeList.getIndex(i);
            SeqInst pInst = activeList.getInst(i);
            SeqType pSeq = pInst.pSequence;

            if (pSeq == null) {
                throw new AssertException("pSeq != null");
            }

            if (pInst.frameIndex >= pSeq.nFrames) throw new AssertException("pInst.frameIndex < pSeq.nFrames");

            pInst.timeCounter -= (short) nTicks;
            while (pInst.timeCounter < 0) {
                pInst.timeCounter += (short) pSeq.ticksPerFrame;
                ++pInst.frameIndex;

                if (pInst.frameIndex == pSeq.nFrames) {
                    if (!pSeq.isLooping()) {
                        UnlockInstance(pInst);

                        if (pSeq.isRemovable()) {
                            if (pInst instanceof SpriteInst) {
                                short nSprite = (short) boardService.getXSprite(index).getReference();
                                if (!boardService.isValidSprite(nSprite))
                                    throw new AssertException("isValidSprite(nSprite)");
                                checkEventList(nSprite, SS_SPRITE);
                                if ((boardService.getSprite(nSprite).getHitag() & kAttrRespawn) != 0 && (boardService.getSprite(nSprite).getZvel() >= kDudeBase && boardService.getSprite(nSprite).getZvel() < kDudeMax))
                                    evPostCallback(nSprite, SS_SPRITE, Integer.toUnsignedLong(pGameInfo.nWeaponSettings), 9); //XXX Check
                                else engine.deletesprite(nSprite);    // safe to not use actPostSprite here
                            }

                            if (pInst instanceof MaskedWallInst) {
                                int nWall = xwall[index].reference;
                                if (!boardService.isValidWall(nWall))
                                    throw new AssertException("boardService.isValidWall(nWall)");
                                boardService.getWall(nWall).setCstat(boardService.getWall(nWall).getCstat() & (~kWallMasked & ~kWallFlipX & ~kWallOneWay));
                                if ((boardService.getWall(nWall).getNextwall()) != -1)
                                    boardService.getWall(boardService.getWall(nWall).getNextwall()).setCstat(boardService.getWall(boardService.getWall(nWall).getNextwall()).getCstat() & (~kWallMasked & ~kWallFlipX & ~kWallOneWay));
                            }
                        }

                        // remove it from the active list
                        activeList.remove(i--);
                        break;
                    } else pInst.frameIndex = 0;
                }

                pInst.update(index);
            }
        }
    }




}

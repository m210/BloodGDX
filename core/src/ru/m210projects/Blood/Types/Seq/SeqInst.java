// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types.Seq;

import ru.m210projects.Blood.Types.CALLPROC;
import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Blood.Types.Seq.SeqHandling.callbacks;
import static ru.m210projects.Blood.Types.Seq.SeqHandling.getIndex;
import static ru.m210projects.Blood.VERSION.getCallback;

public abstract class SeqInst implements Serializable<SeqInst> {

    protected SeqType pSequence;
    protected CALLPROC callback;
    protected int nSeqID;
    protected short timeCounter;    // age of current frame in ticks
    protected short frameIndex;        // current frame
    protected boolean isPlaying;

    protected abstract void updateInstance(int index);

    protected void update(int index) {
        if (frameIndex >= pSequence.getFrames()) throw new AssertException("frameIndex < pSequence.nFrames");

        this.updateInstance(index);

        if (pSequence.getFrame(frameIndex).trigger && callback != null) {
            callback.run(index);
//			if(callback == AIEnemyCallback) {
//				if(boardService.getXSprite(index).aiState != null)
//					boardService.getXSprite(index).aiState.callback(index);
//			}
//			else {
//				if(callback > seqCallback.length)
//					callback = getCallback(callback);
//				seqCallback[callback].run(index);
//			}
        }
    }

    public int getSeqIndex() {
        return nSeqID;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    @Override
    public SeqInst writeObject(OutputStream os) throws IOException {
        StreamUtils.writeInt(os, 0); // nSeq
        StreamUtils.writeInt(os, 0); // pSeq
        StreamUtils.writeInt(os, nSeqID);
        StreamUtils.writeInt(os, getIndex(callback));
        StreamUtils.writeShort(os,timeCounter);
        StreamUtils.writeByte(os, (byte) frameIndex);
        StreamUtils.writeBoolean(os, isPlaying);

        return this;
    }

    @Override
    public SeqInst readObject(InputStream is) throws IOException {
        StreamUtils.skip(is,4); //nSeq
        StreamUtils.skip(is,4); //pSeq

        this.pSequence = null;
        this.nSeqID = StreamUtils.readInt(is);
        int callb = StreamUtils.readInt(is);
        if (callb != -1) {
            if (callb > callbacks.length) {
                this.callback = getCallback(callb);
            }
            else {
                this.callback = callbacks[callb];
            }
        }
        this.timeCounter = StreamUtils.readShort(is);
        this.frameIndex = (short) StreamUtils.readUnsignedByte(is);
        this.isPlaying = StreamUtils.readBoolean(is);

        return this;
    }

    public void clear() {
        this.pSequence = null;
        this.callback = null;
        this.nSeqID = 0;
        this.timeCounter = 0;
        this.frameIndex = 0;
        this.isPlaying = false;
    }

    public void copy(SeqInst src) {
        this.pSequence = src.pSequence;
        this.nSeqID = src.nSeqID;
        this.timeCounter = src.timeCounter;
        this.frameIndex = src.frameIndex;
        this.isPlaying = src.isPlaying;
    }
}

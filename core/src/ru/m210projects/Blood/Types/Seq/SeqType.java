package ru.m210projects.Blood.Types.Seq;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import static ru.m210projects.Blood.Main.game;

public class SeqType {

    private static final HashMap<Integer, SeqType> pSEQs = new HashMap<Integer, SeqType>();
    private final int kSeqLoop = 1;
    private final int kSeqRemove = 2;
    private final int kSEQSig = 0x1A514553; //SEQ\032;
    protected int nFrames;
    protected int ticksPerFrame;
    protected int soundId;
    protected byte flags;
    protected SeqFrame[] frame;

    public SeqType(InputStream is) throws IOException {
        int signature = StreamUtils.readInt(is);

        if (signature != kSEQSig) System.err.println("Invalid sequence");

        short version = (short) StreamUtils.readShort(is);
        if ((version & 0xFF00) != 0x0300) System.err.println("Obsolete sequence version");

        nFrames = StreamUtils.readShort(is);
        ticksPerFrame = StreamUtils.readShort(is);
        soundId = StreamUtils.readShort(is);
        flags = (byte) StreamUtils.readUnsignedByte(is);
        StreamUtils.skip(is, 3); //pad

        frame = new SeqFrame[nFrames];
        for (int i = 0; i < nFrames; i++)
            frame[i] = new SeqFrame(is);
    }

    public static SeqType getInstance(int nSeqId) {
        SeqType pSequence = pSEQs.get(nSeqId);
        if (pSequence == null) {
            Entry data = game.getCache().getEntry(nSeqId, "SEQ");
            if (data.exists()) {
                try (InputStream is = data.getInputStream()) {
                    pSequence = new SeqType(is);
                    pSEQs.put(nSeqId, pSequence);
                } catch (Exception e) {
                    return null;
                }
            }
        }

        return pSequence;
    }

    public static void flushCache() {
        pSEQs.clear();
    }

    public int getFrames() {
        return nFrames;
    }

    public SeqFrame getFrame(int num) {
        if (num < 0 || num >= nFrames) return null;

        return frame[num];
    }

    public int getTicks() {
        return ticksPerFrame;
    }

    public int getSound() {
        return soundId;
    }

    public boolean isLooping() {
        return (flags & kSeqLoop) != 0;
    }

    public boolean isRemovable() {
        return (flags & kSeqRemove) != 0;
    }

}

package ru.m210projects.Blood.Types.Seq;

import ru.m210projects.Blood.Factory.BloodSprite;
import ru.m210projects.Blood.Gameutils;
import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Build.exceptions.AssertException;

import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;

public class SpriteInst extends SeqInst {

    @Override
    protected void updateInstance(int nXSprite) {
        XSPRITE pXSprite = boardService.getXSprite(nXSprite);
        assert pXSprite != null;

        final int nSprite = pXSprite.getReference();
        if (!boardService.isValidSprite(nSprite)) throw new AssertException("isValidSprite(nSprite)");
        final BloodSprite pSprite = boardService.getSprite(nSprite);
        if (pSprite.getExtra() != nXSprite) throw new AssertException("pSprite.extra == nXSprite");

        SeqFrame pFrame = pSequence.getFrame(frameIndex);

        // set the motion bit if the vertical size or position of the sprite changes

        if ((pSprite.getHitag() & kAttrGravity) != 0 && (pSprite.getPicnum() >= 0 && (engine.getTile(pSprite.getPicnum()).getHeight() != engine.getTile(pFrame.nTile).getHeight() || engine.getTile(pSprite.getPicnum()).getOffsetY() != engine.getTile(pFrame.nTile).getOffsetY()) || pFrame.yrepeat != 0 && pFrame.yrepeat != pSprite.getYrepeat()))
            pSprite.setHitag(pSprite.getHitag() | kAttrFalling);

        pSprite.setPicnum((short) pFrame.nTile);

        if (pFrame.pal != 0) pSprite.setPal((byte) pFrame.pal);
        pSprite.setShade(pFrame.shade);

        int scale = (Gameutils.IsPlayerSprite(pSprite)) ? pXSprite.getData1() : 0; // player SEQ size scale
        if (pFrame.xrepeat != 0) {
            if (scale < 0) pSprite.setXrepeat((short) (pFrame.xrepeat / Math.abs(scale)));
            else if (scale > 0) pSprite.setXrepeat((short) (pFrame.xrepeat * scale));
            else pSprite.setXrepeat(pFrame.xrepeat);
        }

        if (pFrame.yrepeat != 0) {
            if (scale < 0) pSprite.setYrepeat((short) (pFrame.yrepeat / Math.abs(scale)));
            else if (scale > 0) pSprite.setYrepeat((short) (pFrame.yrepeat * scale));
            else pSprite.setYrepeat(pFrame.yrepeat);

        }

        if (pFrame.translucent) pSprite.setCstat(pSprite.getCstat() | kSpriteTranslucent);
        else pSprite.setCstat(pSprite.getCstat() & ~kSpriteTranslucent);

        if (pFrame.translucentR) pSprite.setCstat(pSprite.getCstat() | kSpriteTranslucentR);
        else pSprite.setCstat(pSprite.getCstat() & ~kSpriteTranslucentR);

        if (pFrame.blocking) pSprite.setCstat(pSprite.getCstat() | kSpriteBlocking);
        else pSprite.setCstat(pSprite.getCstat() & ~kSpriteBlocking);

        if (pFrame.hitscan) pSprite.setCstat(pSprite.getCstat() | kSpriteHitscan);
        else pSprite.setCstat(pSprite.getCstat() & ~kSpriteHitscan);

        if (pFrame.invisible) pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
        else pSprite.setCstat(pSprite.getCstat() & ~kSpriteInvisible);

        if (pFrame.pushable) pSprite.setCstat(pSprite.getCstat() | kSpritePushable);
        else pSprite.setCstat(pSprite.getCstat() & ~kSpritePushable);

        if (pFrame.smoke) pSprite.setHitag(pSprite.getHitag() | kAttrSmoke);
        else pSprite.setHitag(pSprite.getHitag() & ~kAttrSmoke);

        if (pFrame.aiming) pSprite.setHitag(pSprite.getHitag() | kAttrAiming);
        else pSprite.setHitag(pSprite.getHitag() & ~kAttrAiming);

        if (pFrame.flipx) pSprite.setHitag(pSprite.getHitag() | kAttrFlipX);
        else pSprite.setHitag(pSprite.getHitag() & ~kAttrFlipX);
        if (pFrame.flipy) pSprite.setHitag(pSprite.getHitag() | kAttrFlipY);
        else pSprite.setHitag(pSprite.getHitag() & ~kAttrFlipY);

//		if///
//		sfxStart3DSound XXX
    }
}
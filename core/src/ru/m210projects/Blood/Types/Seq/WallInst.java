package ru.m210projects.Blood.Types.Seq;

import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Wall;

import static ru.m210projects.Blood.DB.kMaxXWalls;
import static ru.m210projects.Blood.DB.xwall;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;

public class WallInst extends SeqInst {
    @Override
    protected void updateInstance(int index) {
        if (!(index > 0 && index < kMaxXWalls)) throw new AssertException("nXWall > 0 && nXWall < kMaxXWalls");
        int nWall = xwall[index].reference;
        if (!boardService.isValidWall(nWall)) throw new AssertException("boardService.isValidWall(nWall)");

        Wall pWall = boardService.getWall(nWall);
        if (pWall.getExtra() != index) throw new AssertException("pWall.extra == nXWall");

        SeqFrame pFrame = pSequence.getFrame(frameIndex);

        pWall.setPicnum((short) pFrame.nTile);
        if (pFrame.pal != 0) pWall.setPal((short) pFrame.pal);

        if (pFrame.translucent) pWall.setCstat(pWall.getCstat() | kWallTranslucent);
        else pWall.setCstat(pWall.getCstat() & ~kWallTranslucent);

        if (pFrame.translucentR) pWall.setCstat(pWall.getCstat() | kWallTranslucentR);
        else pWall.setCstat(pWall.getCstat() & ~kWallTranslucentR);

        if (pFrame.blocking) pWall.setCstat(pWall.getCstat() | kWallBlocking);
        else pWall.setCstat(pWall.getCstat() & ~kWallBlocking);

        if (pFrame.hitscan) pWall.setCstat(pWall.getCstat() | kWallHitscan);
        else pWall.setCstat(pWall.getCstat() & ~kWallHitscan);
    }
}

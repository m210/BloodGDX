// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.Types;

import ru.m210projects.Blood.AI.AISTATE;
import ru.m210projects.Blood.AI.DudeExtra;
import ru.m210projects.Blood.Types.Seq.SeqInst;
import ru.m210projects.Blood.Types.Seq.SpriteInst;
import ru.m210projects.Build.Types.LittleEndian;
import ru.m210projects.Build.Types.Serializable;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This class is a part of Blood's XSystem. {@link XSPRITE} types are:
 * <ul>
 * <li>Markers (not implemented)</li>
 * <li>Switches</li>
 * <li>Weapons</li>
 * </ul>
 */

public class XSPRITE implements Serializable<XSPRITE> {
    public static final int sizeof = 56;

    private final SeqInst pSeqInst = new SpriteInst();
    private final SPRITEHIT pSpriteHit = new SPRITEHIT();
    private final DudeExtra pDudeExtra = new DudeExtra();

    private int reference = -1;
    private short state;
    private int busy;
    private short txID;
    private short rxID;
    private short command;
    private boolean triggerOn;
    private boolean triggerOff;
    private short busyTime;
    private short waitTime;
    private short restState;
    private boolean Push;
    private boolean Vector;
    private boolean Impact;
    private boolean Pickup;
    private boolean Touch;
    private boolean Sight;
    private boolean Proximity;
    private boolean DudeLockout;
    private boolean triggerOnce;
    private int l1;
    private int l2;
    private int l3;
    private int l4;
    private int l5;
    private int lSkill;
    private boolean lS;
    private boolean lB;
    private boolean lC;
    private boolean lT;
    private short data1;
    private short data2;
    private short data3;
    private int data4;
    private short key;
    private short wave;
    private short lockMsg;
    private short dropMsg;
    private boolean Decoupled;
    private boolean isTriggered;
    private boolean Interrutable;
    private int respawnPending; // 0:not, 1: red, 2:yellow, 3:green
    private int goalAng; // dudes
    private int dodgeDir; // dudes
    private int Locked;
    private int palette;
    private int respawn; // 0=optional never, 1=optional always, 2=always, 3=never
    // this stuff needed for dudes
    private int moveState; // same as player move states
    private int health;
    private boolean dudeDeaf; // can't hear players
    private boolean dudeAmbush; // must be triggered manually
    private boolean dudeGuard; // won't leave sector
    private boolean dudeFlag4;
    private int target;
    private int targetX;
    private int targetY;
    private int targetZ;
    private int burnTime;
    private int burnSource;
    private int height;
    private int stateTimer;
    private int floorZ;
    private AISTATE aiState;

    public void init(byte[] bytes) {
//			int breference = BitReader.bread(data, 0, 0, 13);
        setState((short) BitHandler.buread(bytes, 1, 6, 6));
        setBusy(BitHandler.buread(bytes, 1, 7, 23));
        setTxID((short) BitHandler.buread(bytes, 4, 0, 9));
        setRxID((short) BitHandler.buread(bytes, 5, 2, 11));
        setCommand((short) BitHandler.buread(bytes, 6, 4, 11));
        setTriggerOn(BitHandler.buread(bytes, 7, 4, 4) != 0);
        setTriggerOff(BitHandler.buread(bytes, 7, 5, 5) != 0);
        setWave((short) BitHandler.buread(bytes, 7, 6, 7));
        setBusyTime((short) BitHandler.buread(bytes, 8, 0, 11));
        setWaitTime((short) BitHandler.buread(bytes, 9, 4, 15));
        setRestState((short) BitHandler.buread(bytes, 11, 0, 0));
        setInterrutable(BitHandler.buread(bytes, 11, 1, 1) != 0);
//			boolean difficulty = BitReader.bread(data, 11, 2, 3) != 0;
        setRespawnPending(BitHandler.buread(bytes, 11, 4, 6));
        setlT(BitHandler.buread(bytes, 11, 7, 7) != 0);
        setDropMsg(LittleEndian.getUByte(bytes, 12));
        setDecoupled(BitHandler.buread(bytes, 13, 0, 0) != 0);
        setTriggerOnce(BitHandler.buread(bytes, 13, 1, 1) != 0);
        setTriggered(BitHandler.buread(bytes, 13, 2, 2) != 0);
        setKey((short) BitHandler.buread(bytes, 13, 3, 5));
        setPush(BitHandler.buread(bytes, 13, 6, 6) != 0);
        setVector(BitHandler.buread(bytes, 13, 7, 7) != 0);
        setImpact(BitHandler.buread(bytes, 14, 0, 0) != 0);
        setPickup(BitHandler.buread(bytes, 14, 1, 1) != 0);
        setTouch(BitHandler.buread(bytes, 14, 2, 2) != 0);
        setSight(BitHandler.buread(bytes, 14, 3, 3) != 0);
        setProximity(BitHandler.buread(bytes, 14, 4, 4) != 0);
//			int breserved1 = BitReader.bread(data, 14, 5, 5);
//			int breserved2 = BitReader.bread(data, 14, 6, 6);
        setL1(BitHandler.buread(bytes, 14, 7, 7));
        setL2(BitHandler.buread(bytes, 15, 0, 0));
        setL3(BitHandler.buread(bytes, 15, 1, 1));
        setL4(BitHandler.buread(bytes, 15, 2, 2));
        setL5(BitHandler.buread(bytes, 15, 3, 3));
        setlS(BitHandler.buread(bytes, 15, 4, 4) != 0);
        setlB(BitHandler.buread(bytes, 15, 5, 5) != 0);
        setlC(BitHandler.buread(bytes, 15, 6, 6) != 0);

        setLSkill(getLSkill() | (getL1() | (getL2() << 1) | (getL3() << 2) | (getL4() << 3) | (getL5() << 4)));

        setDudeLockout(BitHandler.buread(bytes, 15, 7, 7) != 0);
        setData1(LittleEndian.getShort(bytes, 16));
        setData2(LittleEndian.getShort(bytes, 18));
        setData3(LittleEndian.getShort(bytes, 20));
        setGoalAng(BitHandler.buread(bytes, 22, 0, 10));
        setDodgeDir(BitHandler.bsread(bytes, 23, 3, 4));
        setLocked(BitHandler.buread(bytes, 23, 5, 5));
        setPalette(BitHandler.buread(bytes, 23, 6, 7));
        setRespawn(BitHandler.buread(bytes, 24, 0, 1));
        setData4(BitHandler.buread(bytes, 24, 2, 17));
        setMoveState(BitHandler.buread(bytes, 26, 2, 7));
        setLockMsg(LittleEndian.getUByte(bytes, 27));
        setHealth(BitHandler.buread(bytes, 28, 0, 11));
        setDudeDeaf(BitHandler.buread(bytes, 29, 4, 4) != 0);
        setDudeAmbush(BitHandler.buread(bytes, 29, 5, 5) != 0);
        setDudeGuard(BitHandler.buread(bytes, 29, 6, 6) != 0);
        setDudeFlag4(BitHandler.buread(bytes, 29, 7, 7) != 0);
        setTarget(LittleEndian.getShort(bytes, 30));
        setTargetX(LittleEndian.getInt(bytes, 32));
        setTargetY(LittleEndian.getInt(bytes, 36));
        setTargetZ(LittleEndian.getInt(bytes, 40));
        setBurnTime(LittleEndian.getShort(bytes, 44));
        setBurnSource(LittleEndian.getShort(bytes, 46));
        setHeight(LittleEndian.getShort(bytes, 48));
        setStateTimer(LittleEndian.getShort(bytes, 50));
//			int aiState = LittleEndian.getInt(data, 52);
    }

    @Override
    public XSPRITE readObject(InputStream is) throws IOException {
        byte[] bytes = StreamUtils.readBytes(is, XSPRITE.sizeof);

        setReference(BitHandler.bsread(bytes, 0, 0, 13));
        setState((short) BitHandler.buread(bytes, 1, 6, 6));
        setBusy(BitHandler.buread(bytes, 1, 7, 23));
        setTxID((short) BitHandler.buread(bytes, 4, 0, 9));
        setRxID((short) BitHandler.buread(bytes, 5, 2, 11));
        setCommand((short) BitHandler.buread(bytes, 6, 4, 11));
        setTriggerOn(BitHandler.buread(bytes, 7, 4, 4) != 0);
        setTriggerOff(BitHandler.buread(bytes, 7, 5, 5) != 0);
        setWave((short) BitHandler.buread(bytes, 7, 6, 7));
        setBusyTime((short) BitHandler.buread(bytes, 8, 0, 11));
        setWaitTime((short) BitHandler.buread(bytes, 9, 4, 15));
        setRestState((short) BitHandler.buread(bytes, 11, 0, 0));
        setInterrutable(BitHandler.buread(bytes, 11, 1, 1) != 0);
//			boolean difficulty = BitReader.bread(data, 11, 2, 3) != 0;
        setRespawnPending(BitHandler.buread(bytes, 11, 4, 6));
        setlT(BitHandler.buread(bytes, 11, 7, 7) != 0);
        setDropMsg(LittleEndian.getUByte(bytes, 12));
        setDecoupled(BitHandler.buread(bytes, 13, 0, 0) != 0);
        setTriggerOnce(BitHandler.buread(bytes, 13, 1, 1) != 0);
        setTriggered(BitHandler.buread(bytes, 13, 2, 2) != 0);
        setKey((short) BitHandler.buread(bytes, 13, 3, 5));
        setPush(BitHandler.buread(bytes, 13, 6, 6) != 0);
        setVector(BitHandler.buread(bytes, 13, 7, 7) != 0);
        setImpact(BitHandler.buread(bytes, 14, 0, 0) != 0);
        setPickup(BitHandler.buread(bytes, 14, 1, 1) != 0);
        setTouch(BitHandler.buread(bytes, 14, 2, 2) != 0);
        setSight(BitHandler.buread(bytes, 14, 3, 3) != 0);
        setProximity(BitHandler.buread(bytes, 14, 4, 4) != 0);
//			int breserved1 = BitReader.bread(data, 14, 5, 5);
//			int breserved2 = BitReader.bread(data, 14, 6, 6);
        setL1(BitHandler.buread(bytes, 14, 7, 7));
        setL2(BitHandler.buread(bytes, 15, 0, 0));
        setL3(BitHandler.buread(bytes, 15, 1, 1));
        setL4(BitHandler.buread(bytes, 15, 2, 2));
        setL5(BitHandler.buread(bytes, 15, 3, 3));
        setlS(BitHandler.buread(bytes, 15, 4, 4) != 0);
        setlB(BitHandler.buread(bytes, 15, 5, 5) != 0);
        setlC(BitHandler.buread(bytes, 15, 6, 6) != 0);
        setLSkill(getLSkill() | (getL1() | (getL2() << 1) | (getL3() << 2) | (getL4() << 3) | (getL5() << 4)));
        setDudeLockout(BitHandler.buread(bytes, 15, 7, 7) != 0);
        setData1(LittleEndian.getShort(bytes, 16));
        setData2(LittleEndian.getShort(bytes, 18));
        setData3(LittleEndian.getShort(bytes, 20));
        setGoalAng(BitHandler.buread(bytes, 22, 0, 10));
        setDodgeDir(BitHandler.bsread(bytes, 23, 3, 4));
        setLocked(BitHandler.buread(bytes, 23, 5, 5));
        setPalette(BitHandler.buread(bytes, 23, 6, 7));
        setRespawn(BitHandler.buread(bytes, 24, 0, 1));
        setData4(BitHandler.buread(bytes, 24, 2, 17));
        setMoveState(BitHandler.buread(bytes, 26, 2, 7));
        setLockMsg(LittleEndian.getUByte(bytes, 27));
        setHealth(BitHandler.buread(bytes, 28, 0, 11));
        setDudeDeaf(BitHandler.buread(bytes, 29, 4, 4) != 0);
        setDudeAmbush(BitHandler.buread(bytes, 29, 5, 5) != 0);
        setDudeGuard(BitHandler.buread(bytes, 29, 6, 6) != 0);
        setDudeFlag4(BitHandler.buread(bytes, 29, 7, 7) != 0);
        setTarget(LittleEndian.getShort(bytes, 30));
        setTargetX(LittleEndian.getInt(bytes, 32));
        setTargetY(LittleEndian.getInt(bytes, 36));
        setTargetZ(LittleEndian.getInt(bytes, 40));
        setBurnTime(LittleEndian.getShort(bytes, 44));
        setBurnSource(LittleEndian.getShort(bytes, 46));
        setHeight(LittleEndian.getShort(bytes, 48));
        setStateTimer(LittleEndian.getShort(bytes, 50));
//			int aiState = LittleEndian.getInt(data, 52);

        return this;
    }

    @Override
    public XSPRITE writeObject(OutputStream os) throws IOException {
        byte[] bytes = new byte[XSPRITE.sizeof];

        BitHandler.bput(bytes, 0, getReference(), 0, 13);
        BitHandler.bput(bytes, 1, getState(), 6, 6);
        BitHandler.bput(bytes, 1, getBusy(), 7, 23);
        BitHandler.bput(bytes, 4, getTxID(), 0, 9);
        BitHandler.bput(bytes, 5, getRxID(), 2, 11);
        BitHandler.bput(bytes, 6, getCommand(), 4, 11);
        BitHandler.bput(bytes, 7, isTriggerOn() ? 1 : 0, 4, 4);
        BitHandler.bput(bytes, 7, isTriggerOff() ? 1 : 0, 5, 5);
        BitHandler.bput(bytes, 7, getWave(), 6, 7);
        BitHandler.bput(bytes, 8, getBusyTime(), 0, 11);
        BitHandler.bput(bytes, 9, getWaitTime(), 4, 15);
        BitHandler.bput(bytes, 11, getRestState(), 0, 0);
        BitHandler.bput(bytes, 11, isInterrutable() ? 1 : 0, 1, 1);
        BitHandler.bput(bytes, 11, getRespawnPending(), 4, 6);
        BitHandler.bput(bytes, 11, islT() ? 1 : 0, 7, 7);
        BitHandler.bput(bytes, 12, getDropMsg(), 0, 7);
        BitHandler.bput(bytes, 13, isDecoupled() ? 1 : 0, 0, 0);
        BitHandler.bput(bytes, 13, isTriggerOnce() ? 1 : 0, 1, 1);
        BitHandler.bput(bytes, 13, isTriggered() ? 1 : 0, 2, 2);
        BitHandler.bput(bytes, 13, getKey(), 3, 5);
        BitHandler.bput(bytes, 13, isPush() ? 1 : 0, 6, 6);
        BitHandler.bput(bytes, 13, isVector() ? 1 : 0, 7, 7);
        BitHandler.bput(bytes, 14, isImpact() ? 1 : 0, 0, 0);
        BitHandler.bput(bytes, 14, isPickup() ? 1 : 0, 1, 1);
        BitHandler.bput(bytes, 14, isTouch() ? 1 : 0, 2, 2);
        BitHandler.bput(bytes, 14, isSight() ? 1 : 0, 3, 3);
        BitHandler.bput(bytes, 14, isProximity() ? 1 : 0, 4, 4);
        BitHandler.bput(bytes, 14, getL1(), 7, 7);
        BitHandler.bput(bytes, 15, getL2(), 0, 0);
        BitHandler.bput(bytes, 15, getL3(), 1, 1);
        BitHandler.bput(bytes, 15, getL4(), 2, 2);
        BitHandler.bput(bytes, 15, getL5(), 3, 3);
        BitHandler.bput(bytes, 15, islS() ? 1 : 0, 4, 4);
        BitHandler.bput(bytes, 15, islB() ? 1 : 0, 5, 5);
        BitHandler.bput(bytes, 15, islC() ? 1 : 0, 6, 6);
        BitHandler.bput(bytes, 15, isDudeLockout() ? 1 : 0, 7, 7);
        BitHandler.bput(bytes, 16, getData1(), 0, 15);
        BitHandler.bput(bytes, 18, getData2(), 0, 15);
        BitHandler.bput(bytes, 20, getData3(), 0, 15);
        BitHandler.bput(bytes, 22, getGoalAng(), 0, 10);
        BitHandler.bput(bytes, 23, getDodgeDir(), 3, 4);
        BitHandler.bput(bytes, 23, getLocked(), 5, 5);
        BitHandler.bput(bytes, 23, getPalette(), 6, 7);
        BitHandler.bput(bytes, 24, getRespawn(), 0, 1);
        BitHandler.bput(bytes, 24, getData4(), 2, 17);
        BitHandler.bput(bytes, 26, getMoveState(), 2, 7);
        BitHandler.bput(bytes, 27, getLockMsg(), 0, 7);
        BitHandler.bput(bytes, 28, getHealth(), 0, 11);
        BitHandler.bput(bytes, 29, isDudeDeaf() ? 1 : 0, 4, 4);
        BitHandler.bput(bytes, 29, isDudeAmbush() ? 1 : 0, 5, 5);
        BitHandler.bput(bytes, 29, isDudeGuard() ? 1 : 0, 6, 6);
        BitHandler.bput(bytes, 29, isDudeFlag4() ? 1 : 0, 7, 7);
        BitHandler.bput(bytes, 30, getTarget(), 0, 15);
        BitHandler.bput(bytes, 32, getTargetX(), 0, 31);
        BitHandler.bput(bytes, 36, getTargetY(), 0, 31);
        BitHandler.bput(bytes, 40, getTargetZ(), 0, 31);
        BitHandler.bput(bytes, 44, getBurnTime(), 0, 15);
        BitHandler.bput(bytes, 46, getBurnSource(), 0, 15);
        BitHandler.bput(bytes, 48, getHeight(), 0, 15);
        BitHandler.bput(bytes, 50, getStateTimer(), 0, 15);
        BitHandler.bput(bytes, 52, 0, 0, 31);

        StreamUtils.writeBytes(os, bytes);

        return this;
    }

    public String toString() {
        String out = "reference " + getReference() + " \r\n";
        out += "state " + getState() + " \r\n";
        out += "busy " + getBusy() + " \r\n";
        out += "txID " + getTxID() + " \r\n";
        out += "rxID " + getRxID() + " \r\n";
        out += "command " + getCommand() + " \r\n";
        out += "triggerOn " + isTriggerOn() + " \r\n";
        out += "triggerOff " + isTriggerOff() + " \r\n";
        out += "wave " + getWave() + " \r\n";
        out += "busyTime " + getBusyTime() + " \r\n";
        out += "waitTime " + getWaitTime() + " \r\n";
        out += "restState " + getRestState() + " \r\n";
        out += "Interrutable " + isInterrutable() + " \r\n";
        out += "respawnPending " + getRespawnPending() + " \r\n";
        out += "lT " + islT() + " \r\n";
        out += "dropMsg " + getDropMsg() + " \r\n";
        out += "Decoupled " + isDecoupled() + " \r\n";
        out += "triggerOnce " + isTriggerOnce() + " \r\n";
        out += "isTriggered " + isTriggered() + " \r\n";
        out += "key " + getKey() + " \r\n";
        out += "Push " + isPush() + " \r\n";
        out += "Vector " + isVector() + " \r\n";
        out += "Impact " + isImpact() + " \r\n";
        out += "Pickup " + isPickup() + " \r\n";
        out += "Touch " + isTouch() + " \r\n";
        out += "Sight " + isSight() + " \r\n";
        out += "Proximity " + isProximity() + " \r\n";
        out += "l1 " + getL1() + " \r\n";
        out += "l2 " + getL2() + " \r\n";
        out += "l3 " + getL3() + " \r\n";
        out += "l4 " + getL4() + " \r\n";
        out += "l5 " + getL5() + " \r\n";
        out += "lS " + islS() + " \r\n";
        out += "lB " + islB() + " \r\n";
        out += "lC " + islC() + " \r\n";
        out += "DudeLockout " + isDudeLockout() + " \r\n";
        out += "data1 " + getData1() + " \r\n";
        out += "data2 " + getData2() + " \r\n";
        out += "data3 " + getData3() + " \r\n";
        out += "goalAng " + getGoalAng() + " \r\n";
        out += "dodgeDir " + getDodgeDir() + " \r\n";
        out += "Locked " + getLocked() + " \r\n";
        out += "palette " + getPalette() + " \r\n";
        out += "respawn " + getRespawn() + " \r\n";
        out += "data4 " + getData4() + " \r\n";
        out += "moveState " + getMoveState() + " \r\n";
        out += "lockMsg " + getLockMsg() + " \r\n";
        out += "health " + getHealth() + " \r\n";
        out += "dudeDeaf " + isDudeDeaf() + " \r\n";
        out += "dudeAmbush " + isDudeAmbush() + " \r\n";
        out += "dudeGuard " + isDudeGuard() + " \r\n";
        out += "dudeFlag4 " + isDudeFlag4() + " \r\n";
        out += "target " + getTarget() + " \r\n";
        out += "targetX " + getTargetX() + " \r\n";
        out += "targetY " + getTargetY() + " \r\n";
        out += "targetZ " + getTargetZ() + " \r\n";
        out += "burnTime " + getBurnTime() + " \r\n";
        out += "burnSource " + getBurnSource() + " \r\n";
        out += "height " + getHeight() + " \r\n";
        out += "stateTimer " + getStateTimer() + " \r\n";
        out += "aiState " + getAIState() + " \r\n";
//		if (aiState != null)
//			out += "aiState " + aiState.name + " \r\n";

        return out;
    }

    public void free() {
        setReference(-1);
        setState(0);
        setBusy(0);
        setTxID(0);
        setRxID(0);

        setCommand(0);
        setTriggerOn(false);
        setTriggerOff(false);
        setBusyTime(0);
        setWaitTime(0);
        setRestState(0);

        setPush(false);
        setVector(false);
        setImpact(false);
        setPickup(false);
        setTouch(false);
        setSight(false);
        setProximity(false);
        setDudeLockout(false);
        setTriggerOnce(false);

        setL1(0);
        setL2(0);
        setL3(0);
        setL4(0);
        setL5(0);
        setLSkill(0);
        setlS(false);
        setlB(false);
        setlC(false);
        setlT(false);

        setData1(0);
        setData2(0);
        setData3(0);
        setData4(0);
        setKey(0);
        setWave(0);

        setLockMsg(0);
        setDropMsg(0);

        setDecoupled(false);
        setTriggered(false);
        setInterrutable(false);

        setGoalAng(0); // dudes
        setDodgeDir(0); // dudes
        setLocked(0);
        setPalette(0);

        setRespawn(0); // 0=optional never, 1=optional always, 2=always, 3=never
        setRespawnPending(0);

        // this stuff needed for dudes
        setMoveState(0); // same as player move states

        setHealth(0);
        setDudeDeaf(false); // can't hear players
        setDudeAmbush(false); // must be triggered manually
        setDudeGuard(false); // won't leave sector
        setDudeFlag4(false);

        setTarget(0);
        setTargetX(0);
        setTargetY(0);
        setTargetZ(0);

        setBurnTime(0);
        setBurnSource(0);
        setHeight(0);

        setStateTimer(0);
        setFloorZ(0);

        aiState = null;
        pSeqInst.clear();
        pSpriteHit.clear();
        pDudeExtra.clear();
    }

    public void copy(XSPRITE src) {
        pSeqInst.copy(src.pSeqInst);
        pSpriteHit.copy(src.pSpriteHit);
        pDudeExtra.copy(src.pDudeExtra);

        setReference(src.getReference());
        setState(src.getState());
        setBusy(src.getBusy());
        setTxID(src.getTxID());
        setRxID(src.getRxID());

        setCommand(src.getCommand());
        setTriggerOn(src.isTriggerOn());
        setTriggerOff(src.isTriggerOff());
        setBusyTime(src.getBusyTime());
        setWaitTime(src.getWaitTime());
        setRestState(src.getRestState());

        setPush(src.isPush());
        setVector(src.isVector());
        setImpact(src.isImpact());
        setPickup(src.isPickup());
        setTouch(src.isTouch());
        setSight(src.isSight());
        setProximity(src.isProximity());
        setDudeLockout(src.isDudeLockout());
        setTriggerOnce(src.isTriggerOnce());

        setL1(src.getL1());
        setL2(src.getL2());
        setL3(src.getL3());
        setL4(src.getL4());
        setL5(src.getL5());
        setLSkill(src.getLSkill());
        setlS(src.islS());
        setlB(src.islB());
        setlC(src.islC());
        setlT(src.islT());

        setData1(src.getData1());
        setData2(src.getData2());
        setData3(src.getData3());
        setData4(src.getData4());
        setKey(src.getKey());
        setWave(src.getWave());

        setLockMsg(src.getLockMsg());
        setDropMsg(src.getDropMsg());

        setDecoupled(src.isDecoupled());
        setTriggered(src.isTriggered());
        setInterrutable(src.isInterrutable());

        setGoalAng(src.getGoalAng());
        setDodgeDir(src.getDodgeDir());
        setLocked(src.getLocked());
        setPalette(src.getPalette());

        setRespawn(src.getRespawn());
        setRespawnPending(src.getRespawnPending());

        setMoveState(src.getMoveState());

        setHealth(src.getHealth());
        setDudeDeaf(src.isDudeDeaf());
        setDudeAmbush(src.isDudeAmbush());
        setDudeGuard(src.isDudeGuard());
        setDudeFlag4(src.isDudeFlag4());

        setTarget(src.getTarget());
        setTargetX(src.getTargetX());
        setTargetY(src.getTargetY());
        setTargetZ(src.getTargetZ());

        setBurnTime(src.getBurnTime());
        setBurnSource(src.getBurnSource());
        setHeight(src.getHeight());

        setStateTimer(src.getStateTimer());
        setFloorZ(src.getFloorZ());

        setAIState(src.getAIState());
    }

    public SeqInst getSeqInst() {
        return pSeqInst;
    }

    public SPRITEHIT getSpriteHit() {
        return pSpriteHit;
    }

    public DudeExtra getDudeExtra() {
        return pDudeExtra;
    }

    /**
     * An index to sprite array linking this
     * {@link XSPRITE} with its corresponding {@link Sprite}.
     */
    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public short getState() {
        return state;
    }

    public void setState(int state) {
        this.state = (short) state;
    }

    public int getBusy() {
        return busy;
    }

    public void setBusy(int busy) {
        this.busy = busy;
    }

    public short getTxID() {
        return txID;
    }

    public void setTxID(int txID) {
        this.txID = (short) txID;
    }

    public short getRxID() {
        return rxID;
    }

    public void setRxID(int rxID) {
        this.rxID = (short) rxID;
    }

    public short getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = (short) command;
    }

    public boolean isTriggerOn() {
        return triggerOn;
    }

    public void setTriggerOn(boolean triggerOn) {
        this.triggerOn = triggerOn;
    }

    public boolean isTriggerOff() {
        return triggerOff;
    }

    public void setTriggerOff(boolean triggerOff) {
        this.triggerOff = triggerOff;
    }

    public short getBusyTime() {
        return busyTime;
    }

    public void setBusyTime(int busyTime) {
        this.busyTime = (short) busyTime;
    }

    public short getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(int waitTime) {
        this.waitTime = (short) waitTime;
    }

    public short getRestState() {
        return restState;
    }

    public void setRestState(int restState) {
        this.restState = (short) restState;
    }

    public boolean isPush() {
        return Push;
    }

    public void setPush(boolean push) {
        Push = push;
    }

    public boolean isVector() {
        return Vector;
    }

    public void setVector(boolean vector) {
        Vector = vector;
    }

    public boolean isImpact() {
        return Impact;
    }

    public void setImpact(boolean impact) {
        Impact = impact;
    }

    public boolean isPickup() {
        return Pickup;
    }

    public void setPickup(boolean pickup) {
        Pickup = pickup;
    }

    public boolean isTouch() {
        return Touch;
    }

    public void setTouch(boolean touch) {
        Touch = touch;
    }

    public boolean isSight() {
        return Sight;
    }

    public void setSight(boolean sight) {
        Sight = sight;
    }

    public boolean isProximity() {
        return Proximity;
    }

    public void setProximity(boolean proximity) {
        Proximity = proximity;
    }

    public boolean isDudeLockout() {
        return DudeLockout;
    }

    public void setDudeLockout(boolean dudeLockout) {
        DudeLockout = dudeLockout;
    }

    public boolean isTriggerOnce() {
        return triggerOnce;
    }

    public void setTriggerOnce(boolean triggerOnce) {
        this.triggerOnce = triggerOnce;
    }

    public int getL1() {
        return l1;
    }

    public void setL1(int l1) {
        this.l1 = l1;
    }

    public int getL2() {
        return l2;
    }

    public void setL2(int l2) {
        this.l2 = l2;
    }

    public int getL3() {
        return l3;
    }

    public void setL3(int l3) {
        this.l3 = l3;
    }

    public int getL4() {
        return l4;
    }

    public void setL4(int l4) {
        this.l4 = l4;
    }

    public int getL5() {
        return l5;
    }

    public void setL5(int l5) {
        this.l5 = l5;
    }

    public int getLSkill() {
        return lSkill;
    }

    public void setLSkill(int lSkill) {
        this.lSkill = lSkill;
    }

    public boolean islS() {
        return lS;
    }

    public void setlS(boolean lS) {
        this.lS = lS;
    }

    public boolean islB() {
        return lB;
    }

    public void setlB(boolean lB) {
        this.lB = lB;
    }

    public boolean islC() {
        return lC;
    }

    public void setlC(boolean lC) {
        this.lC = lC;
    }

    public boolean islT() {
        return lT;
    }

    public void setlT(boolean lT) {
        this.lT = lT;
    }

    /**
     * Storage area for miscellaneous data. Use is {@link XSPRITE} type dependant.
     */
    public short getData1() {
        return data1;
    }

    public void setData1(int data1) {
        this.data1 = (short) data1;
    }

    public short getData2() {
        return data2;
    }

    public void setData2(int data2) {
        this.data2 = (short) data2;
    }

    public short getData3() {
        return data3;
    }

    public void setData3(int data3) {
        this.data3 = (short) data3;
    }

    public int getData4() {
        return data4;
    }

    public void setData4(int data4) {
        this.data4 = data4;
    }

    public short getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = (short) key;
    }

    public short getWave() {
        return wave;
    }

    public void setWave(int wave) {
        this.wave = (short) wave;
    }

    public short getLockMsg() {
        return lockMsg;
    }

    public void setLockMsg(int lockMsg) {
        this.lockMsg = (short) lockMsg;
    }

    public short getDropMsg() {
        return dropMsg;
    }

    public void setDropMsg(int dropMsg) {
        this.dropMsg = (short) dropMsg;
    }

    public boolean isDecoupled() {
        return Decoupled;
    }

    public void setDecoupled(boolean decoupled) {
        Decoupled = decoupled;
    }

    public boolean isTriggered() {
        return isTriggered;
    }

    public void setTriggered(boolean triggered) {
        isTriggered = triggered;
    }

    public boolean isInterrutable() {
        return Interrutable;
    }

    public void setInterrutable(boolean interrutable) {
        Interrutable = interrutable;
    }

    public int getRespawnPending() {
        return respawnPending;
    }

    public void setRespawnPending(int respawnPending) {
        this.respawnPending = respawnPending;
    }

    public int getGoalAng() {
        return goalAng;
    }

    public void setGoalAng(int goalAng) {
        this.goalAng = goalAng;
    }

    public int getDodgeDir() {
        return dodgeDir;
    }

    public void setDodgeDir(int dodgeDir) {
        this.dodgeDir = dodgeDir;
    }

    public int getLocked() {
        return Locked;
    }

    public void setLocked(int locked) {
        Locked = locked;
    }

    public int getPalette() {
        return palette;
    }

    public void setPalette(int palette) {
        this.palette = palette;
    }

    public int getRespawn() {
        return respawn;
    }

    public void setRespawn(int respawn) {
        this.respawn = respawn;
    }

    public int getMoveState() {
        return moveState;
    }

    public void setMoveState(int moveState) {
        this.moveState = moveState;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isDudeDeaf() {
        return dudeDeaf;
    }

    public void setDudeDeaf(boolean dudeDeaf) {
        this.dudeDeaf = dudeDeaf;
    }

    public boolean isDudeAmbush() {
        return dudeAmbush;
    }

    public void setDudeAmbush(boolean dudeAmbush) {
        this.dudeAmbush = dudeAmbush;
    }

    public boolean isDudeGuard() {
        return dudeGuard;
    }

    public void setDudeGuard(boolean dudeGuard) {
        this.dudeGuard = dudeGuard;
    }

    public boolean isDudeFlag4() {
        return dudeFlag4;
    }

    public void setDudeFlag4(boolean dudeFlag4) {
        this.dudeFlag4 = dudeFlag4;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public int getTargetX() {
        return targetX;
    }

    public void setTargetX(int targetX) {
        this.targetX = targetX;
    }

    public int getTargetY() {
        return targetY;
    }

    public void setTargetY(int targetY) {
        this.targetY = targetY;
    }

    public int getTargetZ() {
        return targetZ;
    }

    public void setTargetZ(int targetZ) {
        this.targetZ = targetZ;
    }

    public int getBurnTime() {
        return burnTime;
    }

    public void setBurnTime(int burnTime) {
        this.burnTime = burnTime;
    }

    public int getBurnSource() {
        return burnSource;
    }

    public void setBurnSource(int burnSource) {
        this.burnSource = burnSource;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getStateTimer() {
        return stateTimer;
    }

    public void setStateTimer(int stateTimer) {
        this.stateTimer = stateTimer;
    }

    public int getFloorZ() {
        return floorZ;
    }

    public void setFloorZ(int floorZ) {
        this.floorZ = floorZ;
    }

    public AISTATE getAIState() {
        return aiState;
    }

    public void setAIState(AISTATE aiState) {
        this.aiState = aiState;
    }
}

// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import com.badlogic.gdx.math.Vector3;
import ru.m210projects.Blood.Factory.BloodNetwork;
import ru.m210projects.Blood.Factory.BloodRenderer;
import ru.m210projects.Blood.Menus.MenuInterfaceSet;
import ru.m210projects.Blood.Types.*;
import ru.m210projects.Blood.Types.Hud.*;
import ru.m210projects.Blood.filehandlers.art.BloodArtEntry;
import ru.m210projects.Blood.filehandlers.art.BloodArtEntry.ViewType;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Renderer.RenderType;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static ru.m210projects.Blood.Actor.*;
import static ru.m210projects.Blood.DB.*;
import static ru.m210projects.Blood.Gameutils.*;
import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.LEVELS.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Blood.Mirror.*;
import static ru.m210projects.Blood.PLAYER.*;
import static ru.m210projects.Blood.QAV.*;
import static ru.m210projects.Blood.SECTORFX.DoSectorLighting;
import static ru.m210projects.Blood.Strings.*;
import static ru.m210projects.Blood.Tile.shadeTable;
import static ru.m210projects.Blood.Trig.*;
import static ru.m210projects.Blood.Types.DemoUtils.IsOriginalDemo;
import static ru.m210projects.Blood.Types.ScreenEffect.*;
import static ru.m210projects.Blood.filehandlers.art.BloodArtEntry.ViewType.kSpriteView5Full;
import static ru.m210projects.Blood.VERSION.SHAREWARE;
import static ru.m210projects.Blood.Warp.*;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Build.Strhandler.Bitoa;

public class View {

    public static final int kStatBarHeight = 25;
    public static final int kView2D = 2;
    public static final int kView3D = 3;
    public static final int kView2DIcon = 4;
    public static final int kViewPosCenter = 0;
    private static final int kMaxQuotes = 16;
    private static final QUOTE[] quotes = new QUOTE[kMaxQuotes];
    private static final char[] number_buffer = new char[256];
    private static final int kLensSize = 80;
    private static final byte[] otherMirrorGotpic = new byte[8]; // mirror gotpics for crystal ball
    private static final byte[] bakMirrorGotpic = new byte[8]; // backup for mirror gotpics
    private static final int kViewEffectShadow = 0;
    private static final int kViewEffectFlareHalo = 1;
    private static final int kViewEffectCeilGlow = 2;
    private static final int kViewEffectFloorGlow = 3;
    private static final int kViewEffectTorchHigh = 4;
    private static final int kViewEffectTorchLow = 5;
    private static final int kViewEffectSmokeHigh = 6;
    private static final int kViewEffectSmokeLow = 7;
    private static final int kViewEffectFlame = 8;
    private static final int kViewEffectSpear = 9;
    private static final int kViewEffectTrail = 10;
    private static final int kViewEffectPhase = 11;
    private static final int kViewEffectShowWeapon = 12;
    private static final int kViewEffectReflectiveBall = 13;
    private static final int kViewEffectShoot = 14;
    private static final int kViewEffectTesla = 15;
    private static final int kViewEffectFlag = 16;
    private static final int kViewEffectBigFlag = 17;
    private static final int kViewEffectAtom = 18;
    private static final int kViewEffectMax = 19;
    private static final short[] viewWeaponTile = {-1, -1, 524, 559, 558, 526, 589, 618, 539, 800, 525, 811, 810, -1};
    private static final int kViewDistance = (80 << 4);
    private static final int TA_LEFT = 0;
    private static final int TA_CENTER = 1;
    private static final int kDrawYFlip = 0x0004;
    // viewDrawSprite specific flags
    private static final int kDrawXFlip = 0x0800;
    private static final int[] gEffectDetail = {0, 4, 4, 4, 0, 0, 0, 0, 0, 1, 4, 4, 0, 0, 0, 1, 0, 0, 0};
    private static final Vector3[] atomEffectVelocity = new Vector3[16];
    private static final BURN[] burnTable = {new BURN(2101, 2, 0, 118784, 10, 220), new BURN(2101, 2, 0, 110592, 40, 220), new BURN(2101, 2, 0, 81920, 85, 220), new BURN(2101, 2, 0, 69632, 120, 220), new BURN(2101, 2, 0, 61440, 160, 220), new BURN(2101, 2, 0, 73728, 200, 220), new BURN(2101, 2, 0, 77824, 235, 220), new BURN(2101, 2, 0, 110592, 275, 220), new BURN(2101, 2, 0, 122880, 310, 220)};
    public static int smoothratio;
    public static int viewWeaponX, viewWeaponY;
    public static int viewCrossX, viewCrossY, viewCrossZoom;
    public static int gPlayerIndex;
    public static int nextY;
    public static int numQuotes;
    public static int yOffset = 11;
    public static int kMapZoom = 1024;
    public static int PaletteView = kPalNormal;
    public static int gViewIndex = 0;
    public static int gViewPos = kViewPosCenter;
    public static int gViewMode = kView3D;
    public static int deliriumTilt = 0;
    public static int deliriumTurn = 0;
    public static int deliriumPitch = 0;
    public static int scrollX, scrollOX;
    public static int scrollY, scrollOY;
    public static short scrollAng, scrollOAng;
    public static HANDANIM viewHandAnim;
    public static int gViewX0, gViewY0, gViewX1, gViewY1;
    public static int gViewX0Scaled;
    public static int gViewX1Scaled;
    public static int gViewY0Scaled;
    public static int gViewY1Scaled;
    protected static HudRenderer[] hudlist; // HudScaledRenderer
    protected static HudRenderer splitHud;
    private static int nextTime;
    private static int hideQuotes;
    private static int totalQuotes;
    private static int viewThirdDist = -1;
    private static int viewThirdClock = 0;
    private static byte[] lensTable;
    private static int othercameradist = kViewDistance, othercameraclock = 0;
    private static int lastDacUpdate;

    public static void viewHandInit() {
        viewHandAnim = new HANDANIM();

        Entry hQAV = game.getCache().getEntry(518, qav);
        if (!hQAV.exists()) {
            Console.out.println("Could not load QAVID 518", OsdColor.RED);
            SHAREWARE = true;
            return;
        }

        try (InputStream is = hQAV.getInputStream()) {
            viewHandAnim.pQAV = new QAV(is);
            viewHandAnim.duration = viewHandAnim.pQAV.duration;
            viewHandAnim.clock = engine.getTotalClock();
        } catch (IOException ignored) {
        }
    }

    public static void resetQuotes() {
        numQuotes = 0;
        totalQuotes = 0;
        hideQuotes = 0;
    }

    public static void viewResizeView(int size) {
        if (hudlist == null) {
            // resize was called before game started
            return;
        }
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        gViewX0Scaled = (xdim << 16) / 320;
        gViewY0Scaled = (ydim << 16) / 200;
        gViewX1Scaled = (320 << 16) / xdim;
        gViewY1Scaled = (200 << 16) / ydim;

        cfg.gViewSize = ClipRange(size, 0, hudlist.length - 1);
        if (cfg.gViewSize > (hudlist.length - 1)) {
            gViewX0 = 0;
            gViewY0 = 0;
            gViewX1 = xdim - 1;
            gViewY1 = ydim - 1 - scale(kStatBarHeight, ydim, 200);

            int vsiz = cfg.gViewSize - (hudlist.length - 1);

            gViewX0 += vsiz * xdim / 16;
            gViewX1 -= vsiz * xdim / 16;

            gViewY0 += vsiz * ydim / 16;
            gViewY1 -= vsiz * ydim / 16;

            int dView = (gViewY1 - gViewY0) * vsiz;

            gViewY0 += dView / 16;
            gViewY1 -= dView / 16;

            renderer.setview(gViewX0, gViewY0, gViewX1, gViewY1);
        } else {
            // full screen mode
            gViewX0 = 0;
            gViewY0 = 0;
            gViewX1 = xdim - 1;
            gViewY1 = ydim - 1;

            renderer.setview(gViewX0, gViewY0, gViewX1, gViewY1);
        }
    }

    public static QUOTE viewSetMessage(String message, int nPlayer) {
        return viewSetMessage(message, nPlayer, 0);
    }

    public static QUOTE viewSetMessage(String message, int nPlayer, int pal) {
        if (message.isEmpty()) return null;
//		if ( field_21 ) // == 15
        {
            QUOTE quote = quotes[totalQuotes];
            quote.messageText = message;
            if (nPlayer != -1 && nPlayer != myconnectindex) quote.pal = gPlayer[nPlayer].pSprite.getPal();
            else quote.pal = pal;

            Console.out.println(message, OsdColor.findColor(quote.pal));

            quote.messageTime = kTimerRate * cfg.quoteTime + gFrameClock;

            totalQuotes += 1;
            totalQuotes %= kMaxQuotes;
            numQuotes += 1;
            if (numQuotes > cfg.showQuotes) {
                hideQuotes += 1;
                hideQuotes %= kMaxQuotes;
                nextTime = 0;
                numQuotes = cfg.showQuotes;
                nextY = yOffset;
            }
            return quote;
        }
    }

    public static void InitBallBuffer() {
        DynamicArtEntry pLens = engine.allocatepermanenttile(BALLBUFFER2, kLensSize, kLensSize); // tileAllocTile
        Arrays.fill(pLens.getBytes(), (byte) 0xFF); // clear to mask color
    }

    public static void viewInit() {
        Console.out.println("Initializing status bar");

        hudlist = new HudRenderer[]{null, new MiniHud(), new AltHud(), new FullHud(),};
        splitHud = new SplitHud();

        viewResizeView(cfg.gViewSize);

        for (int i = 0; i < kMaxQuotes; i++) {
            quotes[i] = new QUOTE();
        }

        Entry tableEntry = game.getCache().getEntry("LENS.DAT", true);
        if (!tableEntry.exists()) {
            throw new AssertException("lens.dat == null");
        }

        lensTable = tableEntry.getBytes();
        if (lensTable.length != kLensSize * kLensSize * 4)
            throw new AssertException("gSysRes.Size(hLens) != kLensSize * kLensSize * sizeof(int)");

        InitBallBuffer();

        for (int i = 0; i < 16; i++) {
            atomEffectVelocity[i] = new Vector3();
            atomEffectVelocity[i].x = Random(2048);
            atomEffectVelocity[i].y = Random(2048);
            atomEffectVelocity[i].z = Random(2048);
        }
    }

    public static void viewUpdatePlayerLoc(PLAYER pPlayer) {
        POSTURE cp = gPosture[pPlayer.nLifeMode][pPlayer.moveState];

        pPlayer.viewOffZ = pPlayer.pSprite.getZ() - cp.viewSpeed;
        pPlayer.weaponAboveZ = pPlayer.pSprite.getZ() - cp.weapSpeed;
        viewBackupView(pPlayer.nPlayer);
    }

    public static void viewBackupView(int nPlayer) {
        Sprite pSprite = gPlayer[nPlayer].pSprite;
        PLOCATION pPLocation = gPrevView[nPlayer];
        pPLocation.x = pSprite.getX();
        pPLocation.y = pSprite.getY();
        pPLocation.ang = gPlayer[nPlayer].ang;
        pPLocation.horiz = gPlayer[nPlayer].horiz;
        pPLocation.horizOff = gPlayer[nPlayer].horizOff;
        pPLocation.slope = gPlayer[nPlayer].slope;

        pPLocation.viewOffZ = gPlayer[nPlayer].viewOffZ;
        pPLocation.weapOffZ = gPlayer[nPlayer].weaponAboveZ - gPlayer[nPlayer].viewOffZ - 3072;

        pPLocation.bobHeight = gPlayer[nPlayer].bobHeight;
        pPLocation.bobWidth = gPlayer[nPlayer].bobWidth;
        pPLocation.swayHeight = gPlayer[nPlayer].swayHeight;
        pPLocation.swayWidth = gPlayer[nPlayer].swayWidth;
    }

    public static void viewDrawSplitHUD(PLAYER gView) {
        splitHud.draw(gView, 0, 0);
    }

    private static boolean showItem(int item) {
        switch (item + kItemBase) {
            case kItemFeatherFall:
            case kItemLtdInvisibility:
            case kItemInvulnerability:
            case kItemRavenFlight:
            case kItemGunsAkimbo:
            case kItemGasMask:
            case kItemClone:
            case kItemDecoy:
            case kItemDoppleganger:
            case kItemReflectiveShots:
            case kItemShadowCloak:
            case kItemShroomRage:
            case kItemShroomDelirium:
            case kItemShroomGrow:
            case kItemShroomShrink:
            case kItemDeathMask:
            case kItemAsbestosArmor:
                return true;
        }
        return false;
    }

    public static void viewDrawHUD(PLAYER gView) {
        Renderer renderer = game.getRenderer();
        if (cfg.gViewSize >= 0 && cfg.gViewSize < hudlist.length && hudlist[cfg.gViewSize] != null) {
            // hudlist[cfg.gViewSize].setScale(cfg.gHudSize);
            hudlist[cfg.gViewSize].draw(gView, 0, 0);
        }

        int fx, posy = 20;
        for (int i = 0; i < kMaxPowerUps; i++) {
            if (powerupCheck(gView, i) > 0 && showItem(i)) {
                ArtEntry pic = engine.getTile(gItemInfo[i].picnum);

                DrawStatSprite(gItemInfo[i].picnum, 280 - renderer.getTile(gItemInfo[i].picnum + renderer.animateoffs(gItemInfo[i].picnum, 0)).getWidth() / 4, posy - (pic.getHeight() / 4), 0, 0, 512 | 16, 32768);
                fx = viewDrawNumber(3, (gView.powerUpTimer[i] * 100) / Math.max(gPowerUpInfo[i].addPower, 1), 276, posy + (pic.getHeight() / 4) + 4, 1.0f, 32, 0, TextAlign.Center, Transparent.None, ConvertType.AlignRight, false);
                game.getFont(3).drawTextScaled(renderer, 277 + fx, posy + (pic.getHeight() / 4) + 4, "%", 1.0f, 24, 0, TextAlign.Center, Transparent.None, ConvertType.AlignRight, true);
                posy += 40;
            }
        }

//		debugView(10, 30);

        if (pGameInfo.nGameType == kNetModeOff) return;

        if (pGameInfo.nGameType == kNetModeTeams) {
            if (nTeamClock[0] == 0 || (engine.getTotalClock() & 8) != 0) {
                game.getFont(0).drawTextScaled(renderer, 1, 1, "Blue", 1.0f, -128, 10, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
                nTeamClock[0] = ClipLow(nTeamClock[0] - kFrameTicks, 0);
                viewDrawNumber(0, nTeamCount[0], 1, 11, 1.0f, -128, 10, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
            }

            if (nTeamClock[1] == 0 || (engine.getTotalClock() & 8) != 0) {
                game.getFont(0).drawTextScaled(renderer, 319, 1, "Red", 1.0f, -128, 7, TextAlign.Right, Transparent.None, ConvertType.AlignRight, false);
                nTeamClock[1] = ClipLow(nTeamClock[1] - kFrameTicks, 0);
                viewDrawNumber(0, nTeamCount[1], 319, 11, 1.0f, -128, 7, TextAlign.Right, Transparent.None, ConvertType.AlignRight, false);
            }
        } else viewNetPlayers(0, true);

        if (gView != gMe) {
            final String viewed = "View from ";
            System.arraycopy(viewed.toCharArray(), 0, number_buffer, 0, viewed.length());
            int len = game.net.gProfile[gView.nPlayer].name.length();
            System.arraycopy(game.net.gProfile[gView.nPlayer].name.toCharArray(), 0, number_buffer, viewed.length(), len);
            number_buffer[viewed.length() + len] = 0;
            int shade = 32 - (engine.getTotalClock() & 0x3F);
            game.getFont(0).drawTextScaled(renderer, 160, 150, number_buffer, 1.0f, shade, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        // ----------End Draw HUD
    }

    public static void viewNetPlayers(int yoffset, boolean showpalette) {
        Renderer renderer = game.getRenderer();
        int row = (numplayers - 1) / 4;
        if (row >= 0) {
            if (yoffset > 0) yoffset -= 9 * row;

            ArtEntry pic = engine.getTile(2229);
            for (int r = 0; r <= row; r++)
                for (int i = 0; i < 4; i++)
                    DrawStatSprite(2229, 80 * i + 40, (9 * r + 8) - pic.getHeight() / 2 + yoffset, 16, 0, 10, 65536);

            int plu = (gPlayer[myconnectindex].teamID & 3) + 11;
            if (plu == 13) plu = 4; // green
            if (!showpalette) plu = 0;

            if (game.isCurrentScreen(gGameScreen) && pGameInfo.nReviveMode && pGameInfo.nGameType == kNetModeCoop && gMe.pXsprite.getHealth() <= 0) {
                int shade = 32 - (engine.getTotalClock() & 0x3F);
                DrawStatSprite(2229, 0, yoffset, shade, 2, 10 | 16, 65536);
            }

            game.getFont(4).drawTextScaled(renderer, 4, yoffset + 1, game.net.gProfile[myconnectindex].name, 1.0f, -128, plu, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            viewDrawNumber(4, gPlayer[myconnectindex].fragCount, 76, yoffset + 1, 1.0f, -128, plu, TextAlign.Right, Transparent.None, ConvertType.Normal, false);

            int p = 0;
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                if (i == myconnectindex) continue;
                p++;
                int posx = 80 * (p & 3);
                int posy = 9 * (p / 4);
                plu = (gPlayer[i].teamID & 3) + 11;
                if (plu == 13) plu = 4; // green
                if (!showpalette) plu = 0;

                if (game.isCurrentScreen(gGameScreen) && pGameInfo.nReviveMode && pGameInfo.nGameType == kNetModeCoop && gPlayer[i].pXsprite.getHealth() <= 0) {
                    int shade = 32 - (engine.getTotalClock() & 0x3F);
                    DrawStatSprite(2229, posx, posy + yoffset, shade, 2, 10 | 16, 65536);
                }

                game.getFont(4).drawTextScaled(renderer, posx + 4, posy + yoffset + 1, game.net.gProfile[i].name, 1.0f, -128, plu, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                viewDrawNumber(4, gPlayer[i].fragCount, posx + 76, posy + yoffset + 1, 1.0f, -128, plu, TextAlign.Right, Transparent.None, ConvertType.Normal, false);
            }
        }
    }

    public static void viewPaletteHandler(PLAYER gView) {
        BloodRenderer renderer = game.getRenderer();
        int vPalette = kPalNormal;
        lastDacUpdate = engine.getTotalClock();

        if (powerupCheck(gView, kItemInvulnerability - kItemBase) > 0) vPalette = kPalInvuln1;
        else if (powerupCheck(gView, kItemReflectiveShots - kItemBase) > 0) vPalette = kPalWater;
        else if (gView.Underwater) {
            vPalette = gView.pXsprite.getPalette();
            // Can be removed
            // if (gView.pXsprite.palette == 1)
            // vPalette = kPalWater;
            // else if (gView.pXsprite.palette == 2)
            // vPalette = kPalSewer;
        } else if (gView.nLifeMode == 1) vPalette = kPalBeast;

        if (vPalette != PaletteView) {
            engine.getPaletteManager().setPalette(vPalette);
            PaletteView = vPalette;
        }

        PICKUP_DAC.setIntensive(gPlayer[gViewIndex].pickupEffect);
        HIT_DAC.setIntensive(ClipHigh(gPlayer[gViewIndex].hitEffect, 85));
        BLIND_DAC.setIntensive(gPlayer[gViewIndex].blindEffect);
        DROWN_DAC.setIntensive(gPlayer[gViewIndex].drownEffect);

        renderer.scrSetDac();
        lastDacUpdate = engine.getTotalClock();
    }

    public static void viewDrawScreen(int gViewIndex, int smooth) {
        PLAYER gView = gPlayer[gViewIndex];
        if (gView == null || gView.pSprite == null) { // try switch to own screen
            gViewIndex = View.gViewIndex = myconnectindex;
            gView = gPlayer[gViewIndex];
        }

        if (gView == null || gView.pSprite == null) return;

        BloodRenderer renderer = game.getRenderer();

        smoothratio = smooth;
        gPlayerIndex = -1;

        if (!gMapScrollMode) {
            scrollOX = scrollX = gView.pSprite.getX();
            scrollOY = scrollY = gView.pSprite.getY();
            scrollOAng = scrollAng = gView.pSprite.getAng();
        }

        if ((gViewMode == kView3D) || gViewMode == kView2D) {
            DoSectorLighting();

            long x = gView.pSprite.getX();
            long y = gView.pSprite.getY();
            long z = gView.viewOffZ;

            float nAngle = gView.ang;
            float nHoriz = gView.horiz;
            int bobWidth = gView.bobWidth;
            int bobHeight = gView.bobHeight;
            int swayWidth = gView.swayWidth;
            int swayHeight = gView.swayHeight;
            int nSlope = gView.slope;

            int nSector = gView.pSprite.getSectnum();
            if (nSector == -1) return;

            int weapOffZ = gView.weaponAboveZ - gView.viewOffZ - 3072;
            if (cfg.gInterpolation != 0 && ((!game.menu.gShowMenu && !Console.out.isShowing()) || game.isCurrentScreen(gDemoScreen))) {
                if (numplayers > 1 && gView == gMe && gMe.pXsprite.getHealth() != 0) {
                    BloodNetwork net = (BloodNetwork) game.pNet;

//					weapOffZ = net.predict.weaponAboveZ - net.predict.viewOffZ - 3072;
                    nSector = net.predict.sectnum;
                    x = net.predictOld.x + mulscale(net.predict.x - net.predictOld.x, smoothratio, 16);
                    y = net.predictOld.y + mulscale(net.predict.y - net.predictOld.y, smoothratio, 16);
                    z = net.predictOld.viewOffZ + mulscale(net.predict.viewOffZ - net.predictOld.viewOffZ, smoothratio, 16);
                    weapOffZ = net.predictOld.weapOffZ + mulscale(net.predict.weapOffZ - net.predictOld.weapOffZ, smoothratio, 16);
                    nHoriz = net.predictOld.horiz + ((net.predict.horiz - net.predictOld.horiz) * smoothratio) / 65536.0f;
                    nAngle = net.predictOld.ang + ((BClampAngle(net.predict.ang - net.predictOld.ang + kAngle180) - kAngle180) * smoothratio) / 65536.0f;
                    bobWidth = net.predictOld.bobWidth + mulscale(net.predict.bobWidth - net.predictOld.bobWidth, smoothratio, 16);
                    bobHeight = net.predictOld.bobHeight + mulscale(net.predict.bobHeight - net.predictOld.bobHeight, smoothratio, 16);
                    swayWidth = net.predictOld.swayWidth + mulscale(net.predict.swayWidth - net.predictOld.swayWidth, smoothratio, 16);
                    swayHeight = net.predictOld.swayHeight + mulscale(net.predict.swayHeight - net.predictOld.swayHeight, smoothratio, 16);
                    nSlope = net.predictOld.slope + mulscale(net.predict.slope - net.predictOld.slope, smoothratio, 16);
                } else {
                    x = gPrevView[gViewIndex].x + mulscale(x - gPrevView[gViewIndex].x, smoothratio, 16);
                    y = gPrevView[gViewIndex].y + mulscale(y - gPrevView[gViewIndex].y, smoothratio, 16);
                    z = gPrevView[gViewIndex].viewOffZ + mulscale(z - gPrevView[gViewIndex].viewOffZ, smoothratio, 16);
                    weapOffZ = gPrevView[gViewIndex].weapOffZ + mulscale(weapOffZ - gPrevView[gViewIndex].weapOffZ, smoothratio, 16);
                    nHoriz = gPrevView[gViewIndex].horiz + ((nHoriz - gPrevView[gViewIndex].horiz) * smoothratio) / 65536.0f;
                    nAngle = gPrevView[gViewIndex].ang + ((BClampAngle(nAngle - gPrevView[gViewIndex].ang + kAngle180) - kAngle180) * smoothratio) / 65536.0f;
                    bobWidth = gPrevView[gViewIndex].bobWidth + mulscale(bobWidth - gPrevView[gViewIndex].bobWidth, smoothratio, 16);
                    bobHeight = gPrevView[gViewIndex].bobHeight + mulscale(bobHeight - gPrevView[gViewIndex].bobHeight, smoothratio, 16);
                    swayWidth = gPrevView[gViewIndex].swayWidth + mulscale(swayWidth - gPrevView[gViewIndex].swayWidth, smoothratio, 16);
                    swayHeight = gPrevView[gViewIndex].swayHeight + mulscale(swayHeight - gPrevView[gViewIndex].swayHeight, smoothratio, 16);
                    nSlope = gPrevView[gViewIndex].slope + mulscale(nSlope - gPrevView[gViewIndex].slope, smoothratio, 16);
                }
            }

            if (gView.explosion != 0) {
                int explCount = ClipRange(gView.explosion * 8, 0, 2000);
                x += ViRandom(explCount >> 4);
                y += ViRandom(explCount >> 4);
                z += ViRandom(explCount);

                nHoriz += ViRandom(explCount >> 8);
                nAngle += ViRandom(explCount >> 8);

                swayWidth += ViRandom(explCount);
                swayHeight += ViRandom(explCount);
            }

            if (gView.quakeTime != 0) {
                int explCount = ClipRange(gView.quakeTime * 8, 0, 2000);
                x += ViRandom(explCount >> 4);
                y += ViRandom(explCount >> 4);
                z += ViRandom(explCount);

                nHoriz += ViRandom(explCount >> 8);
                nAngle += ViRandom(explCount >> 8);

                swayWidth += ViRandom(explCount);
                swayHeight += ViRandom(explCount);
            }

            nHoriz += mulscale(30, (0x40000000L - Cos(4 * gView.tilt)), 30);
            nAngle += gView.lookang;

            if (gViewPos == kViewPosCenter) {
                if (cfg.gBobWidth) {
                    x -= bobWidth * BSinAngle(nAngle) / 261568.0f;
                    y += bobWidth * BCosAngle(nAngle) / 261568.0f;
                }
                if (cfg.gBobHeight) z += bobHeight;

                if (game.net.gProfile[myconnectindex].slopetilt) nHoriz += nSlope;

                z += 10 * nHoriz;
                viewThirdDist = -1;
                viewThirdClock = engine.getTotalClock();
            } else {
                float dx = (float) (kViewDistance * -BCosAngle(nAngle) / 16384.0f);
                float dy = (float) (kViewDistance * -BSinAngle(nAngle) / 16384.0f);

                int dz = 160 * (int) nHoriz - (16 << 8);
                short oldcstat = gView.pSprite.getCstat();
                gView.pSprite.setCstat(gView.pSprite.getCstat() & ~kSpriteHitscan);

                if (!boardService.isValidSector(nSector)) throw new AssertException("boardService.isValidSector(nSector)");
                FindSector((int) x, (int) y, (int) z, nSector);
                nSector = foundSector;

                hitscangoalx = 0x1FFFFFFF;
                hitscangoaly = 0x1FFFFFFF;
                engine.hitscan((int) x, (int) y, (int) z, nSector, (int) dx, (int) dy, dz, pHitInfo, 16777280);

                int hx = (int) (pHitInfo.hitx - x);
                int hy = (int) (pHitInfo.hity - y);

//				if( (klabs(hx) + klabs(hy)) < (klabs(dx) + klabs(dy)))
//				{
//					nSector = (short) pHitInfo.hitsect;
//
//					int wx = 1; if(dx < 0) wx = -1;
//					int wy = 1; if(dy < 0) wy = -1;
//
//					hx -= wx << 6;
//					hy -= wy << 6;
//
//					int dist;
//					if(klabs(dx) <= klabs(dy))
//						dist = ClipHigh((hy << 16) / dy, viewThirdDist);
//					else dist = ClipHigh((hx << 16) / dx, viewThirdDist);
//					viewThirdDist = dist;
//				}

                if ((klabs(hx) + klabs(hy)) - (Math.abs(dx) + Math.abs(dy)) < 1024) {
                    nSector = (short) pHitInfo.hitsect;

                    int wx = 1;
                    if (dx < 0) wx = -1;
                    int wy = 1;
                    if (dy < 0) wy = -1;

                    hx -= wx << 9;
                    hy -= wy << 9;

                    int dist = 0;
                    if (dx != 0 && dy != 0) {
                        if (Math.abs(dx) <= Math.abs(dy)) dist = (int) ClipHigh((hy << 16) / dy, viewThirdDist);
                        else dist = (int) ClipHigh((hx << 16) / dx, viewThirdDist);
                    }
                    viewThirdDist = dist;
                }

                x += mulscale(viewThirdDist, (int) dx, 16);
                y += mulscale(viewThirdDist, (int) dy, 16);
                z += mulscale(viewThirdDist, dz, 16);

                viewThirdDist = ClipHigh(viewThirdDist + ((engine.getTotalClock() - viewThirdClock) << 10), 65536);
                viewThirdClock = engine.getTotalClock();
                if (!boardService.isValidSector(nSector)) throw new AssertException("boardService.isValidSector(nSector)");
                FindSector((int) x, (int) y, (int) z, nSector);
                nSector = foundSector;
                gView.pSprite.setCstat(oldcstat);
            }

            checkWarping(x, y, z, nSector);
            x = checkWx;
            y = checkWy;
            z = checkWz;
            nSector = checkWs;


            boolean bDelirious = (powerupCheck(gView, kItemShroomDelirium - kItemBase) > 0);
            if (deliriumTilt != 0 || bDelirious) {
                renderer.setdrunk(deliriumTilt);
            } else {
                renderer.setdrunk(0);

                if ((powerupCheck(gView, kItemCrystalBall - kItemBase) > 0) && (numplayers > 1 || (game.isCurrentScreen(gGameScreen) && kFakeMultiplayer && nFakePlayers > 1))) {
                    int nP = numplayers;
                    if (kFakeMultiplayer) nP = nFakePlayers;

                    int nViewed, nEyes = (engine.getTotalClock() / (2 * kTimerRate)) % (nP - 1);
                    if (!kFakeMultiplayer) {
                        nViewed = connecthead;
                        while (true) {
                            if (nViewed == gViewIndex) nViewed = connectpoint2[nViewed];

                            if (nEyes == 0) break;

                            nViewed = connectpoint2[nViewed];
                            nEyes--;
                        }
                    } else {
                        nViewed = nEyes + 1;
                    }

                    if (nViewed != -1) {
                        PLAYER pViewed = gPlayer[nViewed];
                        ArtEntry pic = engine.getTile(BALLBUFFER);

                        if (!(pic instanceof DynamicArtEntry) || !pic.exists() || pic.getWidth() != 128 || pic.getHeight() != 128)
                            engine.allocatepermanenttile(BALLBUFFER, 128, 128);

                        renderer.setviewtotile(engine.getTileManager().getDynamicTile(BALLBUFFER));
                        renderer.setaspect(0x10000, 0x13333);

                        int cx = pViewed.pSprite.getX();
                        int cy = pViewed.pSprite.getY();
                        int cz = pViewed.viewOffZ;
                        int cnSector = pViewed.pSprite.getSectnum();
                        int cnAngle = pViewed.pSprite.getAng();
                        int cnHoriz = 0;

                        if (pViewed.explosion != 0) {
                            int explCount = ClipRange(gView.explosion * 8, 0, 2000);
                            cx += ViRandom(explCount >> 4);
                            cy += ViRandom(explCount >> 4);
                            cz += ViRandom(explCount);

                            cnHoriz += ViRandom(explCount >> 8);
                            cnAngle += ViRandom(explCount >> 8);
                        }

                        if (pViewed.quakeTime != 0) {
                            int explCount = ClipRange(gView.quakeTime * 8, 0, 2000);
                            cx += ViRandom(explCount >> 4);
                            cy += ViRandom(explCount >> 4);
                            cz += ViRandom(explCount);

                            cnHoriz += ViRandom(explCount >> 8);
                            cnAngle += ViRandom(explCount >> 8);
                        }

                        int nx = mulscale(-Cos(cnAngle), kViewDistance, 30);
                        int ny = mulscale(-Sin(cnAngle), kViewDistance, 30);
                        int nz = mulscale(cnHoriz, kViewDistance, 3);
                        nz -= 16 << 8;

                        Sprite sp = pViewed.pSprite;
                        short bakcstat = sp.getCstat();
                        sp.setCstat(sp.getCstat() & ~kSpriteHitscan);

                        FindSector(cx, cy, cz, cnSector);
                        cnSector = foundSector;

                        engine.hitscan(cx, cy, cz, cnSector, nx, ny, nz, pHitInfo, 16777280);

                        int hx = pHitInfo.hitx - cx;
                        int hy = pHitInfo.hity - cy;

                        if (klabs(nx) + klabs(ny) > klabs(hx) + klabs(hy)) {
                            cnSector = (short) pHitInfo.hitsect;

                            hx -= ksgn(nx) * 4 << 4;
                            hy -= ksgn(ny) * 4 << 4;

                            if (klabs(nx) > klabs(ny))
                                othercameradist = ClipHigh(othercameradist, divscale(hx, nx, 16));
                            else othercameradist = ClipHigh(othercameradist, divscale(hy, ny, 16));
                        }
                        cx += mulscale(nx, othercameradist, 16);
                        cy += mulscale(ny, othercameradist, 16);
                        cz += mulscale(nz, othercameradist, 16);
                        othercameradist += ((engine.getTotalClock() - othercameraclock) << 10);
                        if (othercameradist > 65536) othercameradist = 65536;

                        othercameraclock = engine.getTotalClock();

                        FindSector(cx, cy, cz, cnSector);
                        cnSector = foundSector;

                        sp.setCstat(bakcstat);

                        checkWarping(cx, cy, cz, cnSector); // make sure view works through linked sectors
                        cx = (int) checkWx;
                        cy = (int) checkWy;
                        cz = (int) checkWz;
                        cnSector = checkWs;

                        visibility = ClipLow(gVisibility - pViewed.visibility * 32, 0);

                        // backup mirror gotpics
                        System.arraycopy(renderer.getRenderedPics(), MIRRORLABEL >> 3, bakMirrorGotpic, 0, 8);

                        // restore other gotpics
                        System.arraycopy(otherMirrorGotpic, 0, renderer.getRenderedPics(), MIRRORLABEL >> 3, 8);

                        DrawMirrors(cx, cy, cz, cnAngle, kHorizDefault + cnHoriz);
                        renderer.drawrooms(cx, cy, cz, cnAngle, kHorizDefault + cnHoriz, cnSector);

                        // save other mirror gotpics
                        System.arraycopy(renderer.getRenderedPics(), MIRRORLABEL >> 3, otherMirrorGotpic, 0, 8);

                        // restore original gotpics
                        System.arraycopy(bakMirrorGotpic, 0, renderer.getRenderedPics(), MIRRORLABEL >> 3, 8);

                        viewProcessSprites(cx, cy, cz);
                        renderer.drawmasks();
                        renderer.setviewback();
                    }
                } else {
                    othercameradist = -1;
                    othercameraclock = engine.getTotalClock();
                }
            }

            int vis = 0;
            for (ListNode<Sprite> node = boardService.getStatNode(kStatExplosion); node != null; node = node.getNext()) {
                int i = node.getIndex();
                Sprite pSprite = boardService.getSprite(i);
                XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                if (pXSprite == null) {
                    throw new AssertException("pXSprite != null");
                }

                int sectnum = pSprite.getSectnum();
                if (renderer.gotSector(sectnum)) {
                    vis += 32 * pXSprite.getData3();
                }
            }

            for (ListNode<Sprite> node = boardService.getStatNode(kStatMissile); node != null; node = node.getNext()) {
                int i = node.getIndex();
                Sprite pSprite = boardService.getSprite(i);
                switch (pSprite.getLotag()) {
                    case kMissileFlare:
                    case kMissileAltTesla:
                    case kMissileStarburstFlare:
                    case kMissileTesla:
                        if (renderer.gotSector(pSprite.getSectnum()))
                            vis += 256;
                        break;
                }
            }
            visibility = ClipLow(gVisibility - 32 * gView.visibility - vis, 0);
            nAngle = BClampAngle(deliriumTurn + nAngle);

            int nUpper = gUpperLink[nSector], nLower = gLowerLink[nSector];

            engine.getzsofslope(nSector, (int) x, (int) y, floorz, ceilz);
            int lz = 4 << 8;
            long crossDz = 0;
            if (z < ceilz.get() + lz) {
                if (nLower == -1 || (renderer.getType().equals(RenderType.Polymost) // Polymost zNear Plane
                        // tweak
                        && boardService.getSector(nSector).getCeilingpicnum() < MIRRORLABEL || boardService.getSector(nSector).getCeilingpicnum() >= MIRRORLABEL + MAXMIRRORS)) {
                    crossDz = z;
                    z = ceilz.get() + lz;
                    crossDz -= z;
                }
            }
            if (z > floorz.get() - lz) {
                if (nUpper == -1 || (renderer.getType().equals(RenderType.Polymost) && boardService.getSector(nSector).getFloorpicnum() < MIRRORLABEL || boardService.getSector(nSector).getFloorpicnum() >= MIRRORLABEL + MAXMIRRORS)) {
                    crossDz = z;
                    z = floorz.get() - lz;
                    crossDz -= z;
                }
            }

            nHoriz = BClipRange(nHoriz, -200, 200);
            int defHoriz = kHorizDefault;
            if (newHoriz && !IsOriginalDemo()) defHoriz = newHorizDefault;

            renderer.settiltang(deliriumTilt);
            DrawMirrors(x, y, z, nAngle, defHoriz + nHoriz + deliriumPitch);
            boolean gotFire = renderer.gotPic(fire.getPicture());
            int oldcstat = gView.pSprite.getCstat();
            if (gViewPos == kViewPosCenter) gView.pSprite.setCstat(gView.pSprite.getCstat() | kSpriteInvisible);
            else gView.pSprite.setCstat(gView.pSprite.getCstat() | (kSpriteTranslucent | kSpriteTranslucentR));
            renderer.drawrooms(x, y, z, nAngle, defHoriz + nHoriz + deliriumPitch, nSector);
            if (gotFire) {
                renderer.getRenderedPics()[fire.getPicture() >> 3] |= (byte) pow2char[fire.getPicture() & 7];
            }
            viewProcessSprites(x, y, z);
            setMirrorParalax(true);
            renderer.drawmasks();
            setMirrorParalax(false);
            processMirror(x, y);
            gView.pSprite.setCstat((short) oldcstat);

            if (deliriumTilt != 0 || bDelirious) {
                if (cfg.getRenderType() == RenderType.Software) {
                    ArtEntry tiltBufferPic = engine.getTileManager().getTile(TILTBUFFER);
                    if (!(tiltBufferPic instanceof DynamicArtEntry) || !tiltBufferPic.exists()) {
                        throw new AssertException("waloff[ TILTBUFFER ] != null");
                    }

                    renderer.setviewback();

                    int nFlags = kRotateScale | kRotateNoMask | 1024;
                    if (bDelirious) {
                        nFlags |= kRotateTranslucent | kRotateTranslucentR;
                    }
                    nFlags |= kRotateYFlip;

                    int tilt = (deliriumTilt & 511);
                    if (tilt > 256) tilt = 512 - tilt;

                    renderer.rotatesprite(160 << 16, 100 << 16, dmulscale(256000, Cos(tilt), 160000, Sin(tilt), 32), deliriumTilt + 512, TILTBUFFER, 0, kPLUNormal, nFlags, gViewX0, gViewY0, gViewX1, gViewY1);
                }
            }

            if (!bDelirious) {
                deliriumTilt = 0;
                deliriumTurn = 0;
                deliriumPitch = 0;
            }

            if (gViewPos == kViewPosCenter) { // Calc weapon and crosshair coordinates for drawHud()
                if ((!game.menu.gShowMenu || (game.menu.getCurrentMenu() instanceof MenuInterfaceSet)) && cfg.gCrosshair) {
                    viewCrossX = 160 - (gView.lookang >> 1);
                    viewCrossY = 90;
                    if (newHoriz && !IsOriginalDemo()) viewCrossY = 100;
                    viewCrossY += (klabs(gView.lookang) / 9);
                    viewCrossY += crossDz >> 7;
                    int zoom = cfg.gCrossSize;
                    if (SplitScreen) zoom >>= 1;

                    viewCrossX = divscale(viewCrossX, gViewX1Scaled, 16);
                    viewCrossY = divscale(viewCrossY, gViewY1Scaled, 16);
                    viewCrossZoom = divscale(zoom, gViewY1Scaled, 16);
                }

                viewWeaponX = 160 + (swayWidth >> 8);
                viewWeaponY = 220 + (swayHeight >> 8) + (weapOffZ >> 7);
                if (SplitScreen) {
                    if (gViewIndex == 0) viewWeaponY = 120 + (swayHeight >> 8) + (weapOffZ >> 7);
                }
            }
        }

//      coords();
//		if( gForceMap && gViewMode == kView3D )
//			DrawStatSprite(2048, 305, 15, 0, 0, 0);
    }

    public static void updateviewmap() {
        int i;
        if ((i = gPlayer[gViewIndex].pSprite.getSectnum()) > -1) {
            int wallid = boardService.getSector(i).getWallptr();
            Wall wal; // = boardService.getWall(wallid);

            show2dsector.setBit(i);
            for (int j = boardService.getSector(i).getWallnum(); j > 0; j--) {
                wal = boardService.getWall(wallid++);
                i = wal.getNextsector();
                if (i < 0) continue;
                if ((wal.getCstat() & 0x0071) != 0) continue;
                if (boardService.getWall(wal.getNextwall()) != null && (boardService.getWall(wal.getNextwall()).getCstat() & 0x0071) != 0)
                    continue;
                if (boardService.getSector(i) != null && boardService.getSector(i).getCeilingz() >= boardService.getSector(i).getFloorz())
                    continue;
                show2dsector.setBit(i);
            }
        }
    }

    public static void viewDrawBurn(int burnTime) {
        if (burnTime == 0) return;
        Renderer renderer = game.getRenderer();

        int windowx1 = 0;
        int windowy1 = 0;
        int windowx2 = renderer.getWidth();
        int windowy2 = renderer.getHeight();

        for (int i = 0; i < 9; i++) {
            int nTile = burnTable[i].nTile + renderer.animateoffs((short) burnTable[i].nTile, i - 32768);
            int nPal = burnTable[i].nPal;
            int nFlags = burnTable[i].nFlags;
            int zoom = burnTable[i].zoom;
            if (burnTime < 600) zoom = (burnTime * (zoom / 600));

            viewDrawSprite(burnTable[i].x << 16, burnTable[i].y << 16, zoom, 0, nTile, 0, nPal, nFlags | 1024, windowx1, windowy1, windowx2, windowy2);
        }
    }

    public static void viewDisplayMessage(int nPlayer) {
        Renderer renderer = game.getRenderer();
        if (!cfg.MessageState || game.menu.gShowMenu) return;

        int x = 0, y = 0;
        int nShade = ClipHigh(numQuotes << 3, 48);

        if (gViewMode == 3) {
            x = mulscale(gViewX0, gViewX1Scaled, 16);
            y = mulscale(gViewY0, gViewY1Scaled, 16);
        }
        y += nextY;

        if (pGameInfo.nGameType != kNetModeOff && pGameInfo.nGameType != kNetModeTeams) {
            int row = (numplayers - 1) / 4;
            y += (row + 1) * 9;
        }
        if (pGameInfo.nGameType == kNetModeTeams) y += 22;

        for (int i = 0; i < numQuotes; i++) {
            QUOTE quote = quotes[(i + hideQuotes) % kMaxQuotes];
            if (gFrameClock < quote.messageTime) {
                game.getFont(cfg.MessageFont).drawTextScaled(renderer, x + 1, y, quote.messageText, 1.0f, nShade, quote.pal, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
                y += yOffset;
                nShade = ClipLow(nShade - 64 / numQuotes, -128);
            } else {
                numQuotes--;
                hideQuotes += 1;
                hideQuotes %= kMaxQuotes;
            }
        }
        if (nextY != 0) {
            nextY = nextTime * yOffset / kTimerRate;
            nextTime += gTicks;
        }
    }

    public static int viewDrawNumber(int nFontId, int number, int x, int y, float zoom, int shade, int nPLU, TextAlign nAlign, Transparent transparent, ConvertType convertType, boolean textShadow) {
        Bitoa(number, number_buffer);
        Renderer renderer = game.getRenderer();
        return game.getFont(nFontId).drawTextScaled(renderer, x, y, number_buffer, zoom, shade, nPLU, nAlign, transparent, convertType, textShadow);
    }

    private static void viewDrawSprite(int sx, int sy, int nZoom, int nAngle, int nTile, int nShade, int nPLU, int nFlags, int wx1, int wy1, int wx2, int wy2) {
        // convert x-flipping
        if ((nFlags & kDrawXFlip) != 0) {
            nAngle = (nAngle + kAngle180) & kAngleMask;
            nFlags ^= kDrawYFlip;
        }

        Renderer renderer = game.getRenderer();
        // call renderer.rotatesprite passing only compatible bits in nFlags
        renderer.rotatesprite(sx, sy, nZoom, (short) nAngle, nTile, nShade, nPLU, nFlags, wx1, wy1, wx2, wy2);
    }

    public static void viewProcessSprites(long nViewX, long nViewY, long nViewZ) {
        PLAYER gView = gPlayer[gViewIndex];
        if (gView == null) return;

        int nTSprite;
        long dx, dy;
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        if (renderedSpriteList.getSize() > kMaxViewSprites) return; // throw new AssertException("spritesortcnt <= kMaxViewSprites");

        final int ospritesortcnt = renderedSpriteList.getSize();

        int maphack_sprite = -1;
        if (maphack_highlight) maphack_sprite = game.getSprite();

        // count down so we don't process shadows
        for (nTSprite = renderedSpriteList.getSize() - 1; nTSprite >= 0; nTSprite--) {
            final Sprite pTSprite = renderedSpriteList.get(nTSprite);
            final XSPRITE pTXSprite = boardService.getXSprite(pTSprite.getExtra());

            if (pTSprite.getDetail() > cfg.gDetail || pTSprite.getSectnum() == -1) {
                pTSprite.setXrepeat(0);
                continue;
            }

            int nTile = pTSprite.getPicnum();
            if (nTile < 0 || nTile >= kMaxTiles) {
                System.err.println("tsprite[].cstat = " + pTSprite.getCstat());
                System.err.println("tsprite[].shade = " + pTSprite.getShade());
                System.err.println("tsprite[].pal = " + pTSprite.getPal());
                System.err.println("tsprite[].picnum = " + pTSprite.getPicnum());
                System.err.println("tsprite[].ang = " + pTSprite.getAng());
                System.err.println("tsprite[].owner = " + pTSprite.getOwner());
                System.err.println("tsprite[].sectnum = " + pTSprite.getSectnum());
                System.err.println("tsprite[].statnum = " + pTSprite.getStatnum());
                System.err.println("tsprite[].type = " + pTSprite.getLotag());
                System.err.println("tsprite[].flags = " + pTSprite.getHitag());
                System.err.println("tsprite[].extra = " + pTSprite.getExtra());
                throw new AssertException("nTile >= 0 && nTile < kMaxTiles");
            }

            // only interpolate certain moving things
            ILoc oldLoc = game.pInt.getsprinterpolate(pTSprite.getOwner());
            if (cfg.gInterpolation == 1 && oldLoc != null && (pTSprite.getHitag() & 0x0200) == 0) {
                int x = oldLoc.x;
                int y = oldLoc.y;
                int z = oldLoc.z;
                short nAngle = oldLoc.ang;

                // interpolate sprite position
                x += mulscale(pTSprite.getX() - oldLoc.x, smoothratio, 16);
                y += mulscale(pTSprite.getY() - oldLoc.y, smoothratio, 16);
                z += mulscale(pTSprite.getZ() - oldLoc.z, smoothratio, 16);
                nAngle += mulscale(((pTSprite.getAng() - oldLoc.ang + kAngle180) & kAngleMask) - kAngle180, smoothratio, 16);

                pTSprite.setX(x);
                pTSprite.setY(y);
                pTSprite.setZ(z);
                pTSprite.setAng(nAngle);
            }

            ArtEntry pic = engine.getTile(pTSprite.getPicnum());

            if (pic instanceof BloodArtEntry) {
                int nFrames = 0;
                switch (((BloodArtEntry) pic).getView()) {
                    case kSpriteViewSingle:
                        if (pTXSprite != null) {
                            switch (pTSprite.getLotag()) {
                                case kSwitchToggle:
                                case kSwitchMomentary:
                                    if (pTXSprite.getState() != 0) nFrames = 1;
                                    break;
                                case kSwitchCombination:
                                    nFrames = pTXSprite.getData1();
                                    break;
                            }
                        }
                        break;
                    case kSpriteView5Full:
                        dx = nViewX - pTSprite.getX();
                        dy = nViewY - pTSprite.getY();

                        RotateVector(dx, dy, -pTSprite.getAng() + kAngle45 / 2);
                        nFrames = GetOctant((int) rotated.x, (int) rotated.y);

                        if (nFrames > 4) {
                            nFrames = 8 - nFrames;
                            pTSprite.setCstat(pTSprite.getCstat() | kSpriteFlipX);
                        } else pTSprite.setCstat(pTSprite.getCstat() & ~kSpriteFlipX);
                        break;
                    case kSpriteView8Full:
                        // Calculate which of the 8 angles of the sprite to draw (0-7)
                        dx = nViewX - pTSprite.getX();
                        dy = nViewY - pTSprite.getY();

                        RotateVector(dx, dy, -pTSprite.getAng() + kAngle45 / 2);
                        nFrames = GetOctant((int) rotated.x, (int) rotated.y);
                        break;

                    case kSpriteView5Half:
                        if (pTXSprite == null) {
                            GetSpriteExtents(pTSprite);
                            if (engine.getflorzofslope(pTSprite.getSectnum(), pTSprite.getX(), pTSprite.getY()) > extents_zBot)
                                nFrames = 1;
                        } else if (pTXSprite.getSpriteHit().floorHit == 0) nFrames = 1;
                        break;
                    case kSpriteViewVoxel:
                    case kSpriteViewSpinVoxel:
//				if(BuildSettings.useVoxels.get())
//					pTSprite.z -= (gPicAnm[nTile].ycenter * pTSprite.yrepeat) << 2;
                        if (cfg.gDetail >= 4 && (pTSprite.getHitag() & kAttrRespawn) == 0) {
//		            pTSprite.picnum = gVoxelData[pTSprite.picnum];
                            if (((BloodArtEntry) pic).getView() == ViewType.kSpriteViewSpinVoxel)
                                pTSprite.setAng((short) ((8 * engine.getTotalClock()) & kAngleMask));
                        }
                        break;

                    default:
                        break;
                }

                while (nFrames > 0) {
                    pTSprite.setPicnum(pTSprite.getPicnum() + engine.getTile(pTSprite.getPicnum()).getAnimFrames() + 1);
                    if (pTSprite.getPicnum() >= kMaxTiles - 1) break;
                    nFrames--;
                }
            }

            int sprshade, shade1, shade2;

            if (pTSprite.getSectnum() < 0 || pTSprite.getSectnum() > boardService.getSectorCount()) continue;

            sprshade = pTSprite.getShade();
            Sector pSector = boardService.getSector(pTSprite.getSectnum());
            XSECTOR pXSector = null;

            if (pSector.getExtra() > 0) pXSector = xsector[pSector.getExtra()];

            if ((pSector.getCeilingstat() & 1) == 0 || (pSector.getFloorstat() & kSectorFloorShade) != 0) {
                shade1 = pSector.getFloorshade();
                shade2 = shadeTable[pSector.getFloorpicnum()];
            } else {
                shade1 = pSector.getCeilingshade();
                shade2 = shadeTable[pSector.getCeilingpicnum()];
            }
            sprshade = pTSprite.getShade() + shade1 + shade2 + ((pTSprite.getPicnum() >= 0) ? shadeTable[pTSprite.getPicnum()] : 0);

            pTSprite.setShade((byte) ClipRange(sprshade, -128, engine.getPaletteManager().getShadeCount() - 1));

            if ((pTSprite.getHitag() & kAttrRespawn) != 0 && boardService.getSprite(pTSprite.getOwner()).getOwner() == 3) {
                if (pTXSprite == null) {
                    throw new AssertException("pTXSprite != NULL");
                }

                pTSprite.setPicnum((short) (2272 + (2 * pTXSprite.getRespawnPending())));
                pTSprite.setXrepeat(48);
                pTSprite.setYrepeat(48);
                pTSprite.setShade(-128);
                pTSprite.setCstat(pTSprite.getCstat() & ~(kSpriteTranslucentR + kSpriteTranslucent));

                if ((IsItemSprite(pTSprite) || IsAmmoSprite(pTSprite)) && pGameInfo.nItemSettings == 2 || IsWeaponSprite(pTSprite) && pGameInfo.nWeaponSettings == 3)
                    pTSprite.setYrepeat(48);
                else pTSprite.setYrepeat(0);

                pTSprite.setXrepeat(pTSprite.getYrepeat());
            }

            if (renderedSpriteList.getSize() < kMaxViewSprites) {
                if (pTXSprite != null && actGetBurnTime(pTXSprite) > 0)
                    pTSprite.setShade((byte) ClipRange(pTSprite.getShade() - mulscale(8, vRandom(), 15) - 16, -128, 127));

                if ((pTSprite.getHitag() & kAttrSmoke) != 0) viewAddEffect(nTSprite, kViewEffectSmokeHigh);

                if (display_mirror) {
//		        	pTSprite.cstat |= kSpriteFlipX;
                }

                if ((pTSprite.getHitag() & kAttrFlipX) != 0) pTSprite.setCstat(pTSprite.getCstat() | kSpriteFlipX);
                if ((pTSprite.getHitag() & kAttrFlipY) != 0) pTSprite.setCstat(pTSprite.getCstat() | kSpriteFlipY);

                switch (pTSprite.getStatnum()) {
                    case kStatDefault:
                        switch (pTSprite.getLotag()) {
                            case 30:
                                if (pTXSprite != null) {
                                    if ((pTXSprite.getState()) != 0) {
                                        pTSprite.setPicnum(pTSprite.getPicnum() + 1);
                                        viewAddEffect(nTSprite, kViewEffectTorchHigh);
                                    } else {
                                        viewAddEffect(nTSprite, kViewEffectSmokeHigh);
                                    }
                                } else {
                                    pTSprite.setPicnum(pTSprite.getPicnum() + 1);
                                    viewAddEffect(nTSprite, kViewEffectTorchHigh);
                                }
                                break;
                            case 32:
                                if (pTXSprite != null && (pTXSprite.getState()) == 0) {
                                    pTSprite.setShade(-8);
                                } else {
                                    pTSprite.setShade(-128);
                                    viewAddEffect(nTSprite, kViewEffectPhase);
                                }
                                break;
                            default:
                                if (pXSector != null) {
                                    if (pXSector.color) pTSprite.setPal((byte) pSector.getFloorpal());
                                }
                                break;
                        }
                        break;
                    case kStatItem:
                        switch (pTSprite.getLotag()) {
                            case kItemBlueTeamBase:
                                if (pTXSprite != null) {
                                    if ((pTXSprite.getState()) != 0) {
                                        if (pGameInfo.nGameType == kNetModeTeams) {
                                            Sprite pEffect;
                                            if ((pEffect = viewAddEffect(nTSprite, kViewEffectBigFlag)) != null)
                                                pEffect.setPal(10);
                                        }
                                    }
                                }
                                break;
                            case kItemRedTeamBase:
                                if (pTXSprite != null) {
                                    if ((pTXSprite.getState()) != 0) {
                                        if (pGameInfo.nGameType == kNetModeTeams) {
                                            Sprite pEffect;
                                            if ((pEffect = viewAddEffect(nTSprite, kViewEffectBigFlag)) != null)
                                                pEffect.setPal(7);
                                        }
                                    }
                                }
                                break;
                            case kItemBlueFlag:
                                pTSprite.setPal(10);
                                pTSprite.setCstat(pTSprite.getCstat() | 4);
                                break;
                            case kItemRedFlag:
                                pTSprite.setPal(7);
                                pTSprite.setCstat(pTSprite.getCstat() | 4);
                                break;
                            default:
                                if (pTSprite.getLotag() >= kItemKey1 && pTSprite.getLotag() <= kItemKey7)
                                    pTSprite.setShade(-128);
                                if (pXSector != null && pXSector.color) pTSprite.setPal((byte) pSector.getFloorpal());
                                break;
                        }
                        break;
                    case kStatMissile:
                        switch (pTSprite.getLotag()) {
                            case kMissileAltTesla:
                                pTSprite.setCstat(pTSprite.getCstat() | 0x20);
                                pTSprite.setShade(-128);
                                break;
                            case kMissileTesla:
                                viewAddEffect(nTSprite, 15);
                                break;
                            case kMissileButcherKnife:
                                viewAddEffect(nTSprite, 10);
                                break;
                            case kMissileFlare:
                            case kMissileStarburstFlare:
                                if (pTSprite.getStatnum() != 14) {
                                    viewAddEffect(nTSprite, 1);
                                    if (pTSprite.getLotag() == kMissileFlare) {
                                        if ((pSector.getCeilingstat() & 1) == 0 && (pTSprite.getZ() - pSector.getCeilingz()) >> 8 < 64 && pSector.getCeilingpicnum() < MIRRORLABEL)
                                            viewAddEffect(nTSprite, 2);
                                        if ((pSector.getFloorstat() & 1) == 0 && (pSector.getFloorz() - pTSprite.getZ()) >> 8 < 64 && pSector.getFloorpicnum() < MIRRORLABEL)
                                            viewAddEffect(nTSprite, 3);
                                    }
                                    break;
                                }
                                if (pTXSprite == null) {
                                    throw new AssertException("pTXSprite != NULL");
                                }

                                if (pTXSprite.getTarget() != gView.nSprite) {
                                    viewAddEffect(nTSprite, 1);
                                    if (pTSprite.getLotag() == kMissileFlare) {
                                        if ((pSector.getCeilingstat() & 1) == 0 && (pTSprite.getZ() - pSector.getCeilingz()) >> 8 < 64 && pSector.getCeilingpicnum() < MIRRORLABEL)
                                            viewAddEffect(nTSprite, 2);
                                        if ((pSector.getFloorstat() & 1) == 0 && (pSector.getFloorz() - pTSprite.getZ()) >> 8 < 64 && pSector.getFloorpicnum() < MIRRORLABEL)
                                            viewAddEffect(nTSprite, 3);
                                    }
                                } else pTSprite.setXrepeat(0);

                                break;
                            default:
                                continue;
                        }
                        break;
                    case kStatDude:
                        if (pTSprite.getLotag() == kDudeBloatedButcherBurning) {
                            pTSprite.setXrepeat(48); // make BloatedButcherBurning fatter
                        }

//			        	if(pTSprite.lotag == kDudeHand)
//			        	{
//			        		if(pTXSprite.target >= 0 && IsPlayerSprite(boardService.getSprite(pTXSprite.target))) {
//			        			pTSprite.xrepeat = 0;
//			        			break;
//			        		}
//			        	}

                        if (boardService.getSector(pTSprite.getSectnum()).getExtra() > 0 && xsector[boardService.getSector(pTSprite.getSectnum()).getExtra()].color) {
                            switch (pTSprite.getLotag()) {
                                case kDudeTommyCultist:
                                case kDudeShotgunCultist:
                                case kDudeFanaticProne:
                                case kDudeCultistProne:
                                case kDudeTeslaCultist:
                                case kDudeDynamiteCultist:
                                case kDudeBeastCultist:
                                    break;
                                default:
                                    pTSprite.setPal((byte) boardService.getSector(pTSprite.getSectnum()).getFloorpal());
                                    break;
                            }
                        }

                        if (powerupCheck(gView, kItemBeastVision - kItemBase) > 0) pTSprite.setShade(-128);

                        if (IsPlayerSprite(pTSprite)) {
                            PLAYER pPlayer = gPlayer[pTSprite.getLotag() - kDudePlayer1];

                            if (pTSprite.getOwner() != gView.pSprite.getXvel()) {
                                int tx = pTSprite.getX() - (int) nViewX;
                                int ty = pTSprite.getY() - (int) nViewY;
                                int nAngle = EngineUtils.getAngle(tx, ty);
                                int losAngle = ((kAngle180 + nAngle - gView.pSprite.getAng()) & kAngleMask) - kAngle180;
                                long dist = EngineUtils.qdist(tx, ty);

                                if (klabs(mulscale(losAngle, dist, 14)) < 4) {
                                    long z1 = mulscale(dist, (int) gView.horizOff, 10) + nViewZ;
                                    GetSpriteExtents(pTSprite);

                                    if ((z1 < extents_zBot) && (z1 > extents_zTop)) {
                                        if (engine.cansee((int) nViewX, (int) nViewY, (int) nViewZ, gView.pSprite.getSectnum(), pTSprite.getX(), pTSprite.getY(), pTSprite.getZ(), pTSprite.getSectnum()))
                                            gPlayerIndex = pPlayer.nPlayer;
                                    }
                                }
                            }

                            if (powerupCheck(pPlayer, kItemLtdInvisibility - kItemBase) == 0 || powerupCheck(gView, kItemBeastVision - kItemBase) > 0) {
                                if (powerupCheck(pPlayer, kItemInvulnerability - kItemBase) != 0) {
                                    pTSprite.setShade(-128);
                                    pTSprite.setPal(5);
                                } else if (powerupCheck(pPlayer, kItemDoppleganger - kItemBase) != 0) {
                                    pTSprite.setPal((byte) ((gView.teamID & 3) + 11));
                                }
                            } else {
                                pTSprite.setCstat(pTSprite.getCstat() | 2);
                                pTSprite.setPal(5);
                            }

                            if (powerupCheck(pPlayer, kItemReflectiveShots - kItemBase) != 0)
                                viewAddEffect(nTSprite, 13);
                            if (cfg.gShowWeapon && pGameInfo.nGameType > kNetModeOff && pPlayer != gView)
                                viewAddEffect(nTSprite, kViewEffectShowWeapon);

                            if ((pPlayer.fireEffect & 1) != 0 && (pPlayer != gView || gViewPos != 0)) {
                                Sprite pEffect;
                                if ((pEffect = viewAddEffect(nTSprite, 14)) != null) {
                                    POSTURE cp = gPosture[pPlayer.nLifeMode][pPlayer.moveState];
                                    pEffect.setX(pEffect.getX() + mulscale(Cos(pTSprite.getAng()), cp.xoffset, 28));
                                    pEffect.setY(pEffect.getY() + mulscale(Sin(pTSprite.getAng()), cp.xoffset, 28));
                                    pEffect.setZ(pPlayer.pSprite.getZ() - cp.zoffset);
                                }
                            }

                            if (pPlayer.hasFlag > 0 && pGameInfo.nGameType == kNetModeTeams) {
                                if ((pPlayer.hasFlag & 1) != 0) {
                                    Sprite pEffect = viewAddEffect(nTSprite, 16);
                                    if (pEffect != null) {
                                        pEffect.setPal(10);
                                        pEffect.setCstat(pEffect.getCstat() | 4);
                                    }
                                }
                                if ((pPlayer.hasFlag & 2) != 0) {
                                    Sprite pEffect = viewAddEffect(nTSprite, 16);
                                    if (pEffect != null) {
                                        pEffect.setPal(7);
                                        pEffect.setCstat(pEffect.getCstat() | 4);
                                    }
                                }
                            }
                        }

                        if (pTSprite.getOwner() != gView.pSprite.getXvel() || gViewPos != 0) {
                            if (engine.getflorzofslope(pTSprite.getSectnum(), pTSprite.getX(), pTSprite.getY()) >= nViewZ) {
                                Sprite pTSpr = viewAddEffect(nTSprite, 0);
                                if (pTSpr != null) {
                                    int camangle = EngineUtils.getAngle((int) nViewX - pTSprite.getX(), (int) nViewY - pTSprite.getY());
                                    pTSpr.setX(pTSpr.getX() - mulscale(EngineUtils.sin(camangle + 512), 300, 16));
                                    pTSpr.setY(pTSpr.getY() + mulscale(EngineUtils.sin(camangle + 1024), 300, 16));
                                }
                            }
                        }

                        break;
                    case kStatTraps:
                        if (pTSprite.getLotag() == 454) {
                            if (pTXSprite != null && pTXSprite.getState() == 1) {
                                if (pTXSprite.getData1() != 0) {
                                    pTSprite.setPicnum(772);
                                    if (pTXSprite.getData2() != 0) viewAddEffect(nTSprite, 9);
                                }
                            } else if (pTXSprite.getData1() != 0) {
                                pTSprite.setPicnum(773);
                            } else {
                                pTSprite.setPicnum(656);
                            }
                        }
                        break;
                    case kStatThing:
                        if (pXSector != null && pXSector.color) {
                            pTSprite.setPal((byte) pSector.getFloorpal());
                        }

                        if ((pTSprite.getHitag() & 1) != 0) {
                            if (engine.getflorzofslope(pTSprite.getSectnum(), pTSprite.getX(), pTSprite.getY()) >= nViewZ) {
                                if (pTSprite.getLotag() < 400 || pTSprite.getLotag() >= 436 || pTXSprite.getSpriteHit().floorHit == 0) {
                                    viewAddEffect(nTSprite, 0);
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }

                if (maphack_sprite != -1 && maphack_sprite == pTSprite.getOwner()) {
                    pTSprite.setShade((byte) (32 - engine.getTotalClock() & 32));
                }
            }
        }

        for (nTSprite = renderedSpriteList.getSize() - 1; nTSprite >= ospritesortcnt; --nTSprite) {
            Sprite pTSprite = renderedSpriteList.get(nTSprite);
            ArtEntry art = engine.getTile(pTSprite.getPicnum());
            if (art instanceof BloodArtEntry) {
                int nFrames = 0;
                switch (((BloodArtEntry) art).getView()) {
                    case kSpriteView5Full:
                        // Calculate which of the 8 angles of the sprite to draw (0-7)
                        dx = nViewX - pTSprite.getX();
                        dy = nViewY - pTSprite.getY();

                        RotateVector(dx, dy, -pTSprite.getAng() + kAngle45 / 2);
                        nFrames = GetOctant((int) rotated.x, (int) rotated.y);

                        if (nFrames > 4) {
                            nFrames = 8 - nFrames;
                            pTSprite.setCstat(pTSprite.getCstat() | kSpriteFlipX);
                        } else {
                            pTSprite.setCstat(pTSprite.getCstat() & ~kSpriteFlipX);
                        }
                        break;
                    case kSpriteView8Full:
                        // Calculate which of the 8 angles of the sprite to draw (0-7)
                        dx = nViewX - pTSprite.getX();
                        dy = nViewY - pTSprite.getY();

                        RotateVector(dx, dy, -pTSprite.getAng() + kAngle45 / 2);
                        nFrames = GetOctant((int) rotated.x, (int) rotated.y);
                        break;
                    default:
                        break;
                }

                while (nFrames > 0) {
                    --nFrames;
                    pTSprite.setPicnum(pTSprite.getPicnum() + art.getAnimFrames() + 1);
                }
            }
        }
    }

    public static void viewShowLoadingTile() {
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        int flags = kQFrameNoMask | kQFrameScale | kQFrameUnclipped;
        int pic = 2049;

        switch (4 * ydim / xdim) {
            default:
            case 3:
                pic = 2049;
                break;
            case 2:
                if (engine.getTile(kWideLoading).exists()) {
                    pic = kWideLoading;
                }
                break;
            case 1:
                if (engine.getTile(kUltraWideLoading).exists()) {
                    pic = kUltraWideLoading;
                }
                break;
        }
        renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, pic, 0, 0, flags);
    }

    public static void DoLensEffect(DynamicArtEntry ballBuffer) {
        byte[] d = ballBuffer.getBytes();
        byte[] s = engine.getTile(BALLBUFFER).getBytes();
        for (int i = 0, dptr = 0; i < kLensSize * kLensSize; i++, dptr++) {
            int lensData = LittleEndian.getInt(lensTable, 4 * i);
            if (lensData >= 0) d[dptr] = s[lensData];
        }
        ballBuffer.invalidate();
    }

    public static void viewSecretStat() {
        Renderer renderer = game.getRenderer();
        game.getFont(1).drawTextScaled(renderer, 75, 70, secrets, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);

        viewDrawNumber(1, foundSecret, 160, 70, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        game.getFont(1).drawTextScaled(renderer, 190, 70, of, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        viewDrawNumber(1, totalSecrets, 220, 70, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        if (superSecrets > 0)
            game.getFont(1).drawTextScaled(renderer, 160, 100, supersecret, 1.0f, -128, 2, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

    public static void viewFragStat() {
        Renderer renderer = game.getRenderer();
        game.getFont(1).drawTextScaled(renderer, 75, 50, killsstat, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);

        viewDrawNumber(1, kills, 160, 50, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        game.getFont(1).drawTextScaled(renderer, 190, 50, of, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
        viewDrawNumber(1, totalKills, 220, 50, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
    }

    public static void viewBackupSpriteLoc(int nSprite, Sprite pSprite) {
        game.pInt.setsprinterpolate(nSprite, pSprite);
    }

    public static void viewBackupSectorLoc(int nSector, Sector pSector) {
        game.pInt.setceilinterpolate(nSector, pSector);
        game.pInt.setfheinuminterpolate(nSector, pSector);
        game.pInt.setfloorinterpolate(nSector, pSector);
    }

    public static TSprite viewInsertTSprite(int nSector, int nStatus, Sprite pSource) {
        Renderer renderer = game.getRenderer();
        TSprite pTSprite = renderer.getRenderedSprites().obtain();
        pTSprite.reset((byte) 0);
        pTSprite.setLotag(kNothing);
        pTSprite.setStatnum((short) nStatus);
        pTSprite.setCstat(kSpriteOriginAlign);
        pTSprite.setXrepeat(64);
        pTSprite.setYrepeat(64);
        pTSprite.setOwner(-1);
        pTSprite.setExtra(-1);

        if (pSource != null) {
            pTSprite.setOwner(pSource.getOwner());
            pTSprite.setAng(pSource.getAng());
            pTSprite.update(pSource.getX(), pSource.getY(), pSource.getZ(), (short) nSector);
        } else pTSprite.update(0, 0, 0, (short) nSector);

        return pTSprite;
    }

    private static Sprite viewAddEffect(int nTSprite, int nViewEffect) {
        if (!(nViewEffect >= 0 && nViewEffect < kViewEffectMax))
            throw new AssertException("nViewEffect >= 0 && nViewEffect < kViewEffectMax " + nViewEffect);
        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        Sprite pTSprite = renderedSpriteList.get(nTSprite);
        Sprite pTEffect;
        short size;

        if (cfg.gDetail < gEffectDetail[nViewEffect] || nViewEffect > kViewEffectMax - 1)
            return null;

        switch (nViewEffect) {
            case kViewEffectShadow:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setZ(engine.getflorzofslope(pTSprite.getSectnum(), pTEffect.getX(), pTEffect.getY()));
                pTEffect.setCstat(pTEffect.getCstat() | kSpriteTranslucent);

                ArtEntry art = engine.getTile(boardService.getSprite(pTSprite.getOwner()).getPicnum());
                if (art instanceof BloodArtEntry && ((BloodArtEntry) art).getView() == kSpriteView5Full) {
                    int dx = gPlayer[gViewIndex].pSprite.getX() - pTEffect.getX();
                    int dy = gPlayer[gViewIndex].pSprite.getY() - pTEffect.getY();

                    RotateVector(dx, dy, -pTEffect.getAng() + kAngle45 / 2);
                    int nFrames = GetOctant((int) rotated.x, (int) rotated.y);

                    if (nFrames > 4) {
                        nFrames = 8 - nFrames;
                        pTEffect.setCstat(pTEffect.getCstat() | kSpriteFlipX);
                    } else pTEffect.setCstat(pTEffect.getCstat() & ~kSpriteFlipX);
                }

                pTEffect.setShade(127);
                pTEffect.setXrepeat(pTSprite.getXrepeat());
                pTEffect.setYrepeat((short) (pTSprite.getYrepeat() >> 2));
                pTEffect.setPicnum(pTSprite.getPicnum());
                pTEffect.setPal(5);
                ArtEntry pic = engine.getTile(pTEffect.getPicnum());
                pTEffect.setZ(pTEffect.getZ() - ((pic.getHeight() - (pic.getOffsetY() + pic.getHeight() / 2)) * pTEffect.getYrepeat() << 2));
                return pTEffect;
            case kViewEffectFlareHalo:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setShade(-128);
                pTEffect.setPal(2);
                pTEffect.setZ(pTSprite.getZ());
                pTEffect.setCstat(pTEffect.getCstat() | kSpriteTranslucent);
                pTEffect.setPicnum(2427);
                pTEffect.setXrepeat(pTSprite.getXrepeat());
                pTEffect.setYrepeat(pTSprite.getYrepeat());
                break;
            case kViewEffectCeilGlow:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setX(pTSprite.getX());
                pTEffect.setY(pTSprite.getY());
                pTEffect.setPal(2);
                pTEffect.setXrepeat(64);
                pTEffect.setYrepeat(64);
                pTEffect.setPicnum(624);
                pTEffect.setZ(boardService.getSector(pTSprite.getSectnum()).getCeilingz());
                pTEffect.setShade((byte) ((pTSprite.getZ() - boardService.getSector(pTSprite.getSectnum()).getCeilingz() >> 8) - 64));
                pTEffect.setCstat(pTEffect.getCstat() | (kSpriteTranslucent | kSpriteFlipY | kSpriteFloor | kSpriteOneSided));
                pTEffect.setAng(pTSprite.getAng());
                pTEffect.setOwner(pTSprite.getOwner());
                break;
            case kViewEffectFloorGlow:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setX(pTSprite.getX());
                pTEffect.setY(pTSprite.getY());
                pTEffect.setPal(2);
                pTEffect.setPicnum(624);
                pTEffect.setZ(boardService.getSector(pTSprite.getSectnum()).getFloorz());
                pTEffect.setShade((byte) (((boardService.getSector(pTSprite.getSectnum()).getFloorz() - pTSprite.getZ()) >> 8) - 32));
                pTEffect.setXrepeat((short) ((boardService.getSector(pTSprite.getSectnum()).getFloorz() - pTSprite.getZ()) >> 8));
                pTEffect.setYrepeat((short) ((boardService.getSector(pTSprite.getSectnum()).getFloorz() - pTSprite.getZ()) >> 8));
                pTEffect.setCstat(pTEffect.getCstat() | (kSpriteTranslucent | kSpriteFloor | kSpriteOneSided));
                pTEffect.setAng(pTSprite.getAng());
                pTEffect.setOwner(pTSprite.getOwner());
                break;
            case kViewEffectTorchHigh:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                GetSpriteExtents(pTSprite);
                pTEffect.setPicnum(2101);
                pTEffect.setShade(-128);
                pTEffect.setZ(extents_zTop);
                size = (short) (engine.getTile(pTSprite.getPicnum()).getWidth() * pTSprite.getXrepeat() / 32);
                pTEffect.setYrepeat(size);
                pTEffect.setXrepeat(size);
                break;
            case kViewEffectTorchLow:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                GetSpriteExtents(pTSprite);
                pTEffect.setPicnum(2101);
                pTEffect.setShade(-128);
                pTEffect.setZ(extents_zBot);
                size = (short) (engine.getTile(pTSprite.getPicnum()).getWidth() * pTSprite.getXrepeat() / 32);
                pTEffect.setYrepeat(size);
                pTEffect.setXrepeat(size);
                break;
            case kViewEffectSmokeHigh:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                GetSpriteExtents(pTSprite);
                pTEffect.setZ(extents_zTop);
                if (pTSprite.getLotag() >= 200 && pTSprite.getLotag() < 247) pTEffect.setPicnum(672);
                else pTEffect.setPicnum(754);
                pTEffect.setShade(8);
                pTEffect.setCstat(pTEffect.getCstat() | kSpriteTranslucent);
                pTEffect.setXrepeat(pTSprite.getXrepeat());
                pTEffect.setYrepeat(pTSprite.getYrepeat());
                break;
            case kViewEffectSmokeLow:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                GetSpriteExtents(pTSprite);
                pTEffect.setZ(extents_zBot);
                if (pTSprite.getLotag() >= 200 && pTSprite.getLotag() < 247) pTEffect.setPicnum(672);
                else pTEffect.setPicnum(754);
                pTEffect.setShade(8);
                pTEffect.setCstat(pTEffect.getCstat() | kSpriteTranslucent);
                pTEffect.setXrepeat(pTSprite.getXrepeat());
                pTEffect.setYrepeat(pTSprite.getYrepeat());
                break;
            case kViewEffectFlame:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setShade(-128);
                pTEffect.setZ(pTSprite.getZ());
                size = (short) ((engine.getTile(pTSprite.getPicnum()).getWidth() * pTSprite.getXrepeat()) / 64);
                pTEffect.setPicnum(908);
                pTEffect.setStatnum(0);
                pTEffect.setYrepeat(size);
                pTEffect.setXrepeat(size);
                break;
            case kViewEffectSpear:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setZ(pTSprite.getZ());
                if (cfg.gDetail > 1) pTEffect.setCstat(pTEffect.getCstat() | 0x202);
                pTEffect.setShade((byte) ClipLow(pTSprite.getShade() - 32, -128));
                pTEffect.setYrepeat(64);
                pTEffect.setPicnum(775);
                pTEffect.setXrepeat(pTSprite.getXrepeat());
                break;
            case kViewEffectTrail:
                int nSector, nAngle = pTSprite.getAng();
                if ((pTSprite.getCstat() & kSpriteWall) != 0) nAngle += 512;
                else nAngle += 1024;
                nAngle &= 0x7FF;

                for (int i = 0; i < 5; i++) {
                    nSector = pTSprite.getSectnum();
                    pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, null);
                    pTEffect.setAng(pTSprite.getAng());
                    pTEffect.setX(pTSprite.getX() + mulscale(Cos(nAngle), ((long) i << 7) + 128, 30));
                    pTEffect.setY(pTSprite.getY() + mulscale(Sin(nAngle), ((long) i << 7) + 128, 30));
                    pTEffect.setZ(pTSprite.getZ());
                    if (!boardService.isValidSector(nSector)) throw new AssertException("boardService.isValidSector(nSector)");
                    FindSector(pTEffect.getX(), pTEffect.getY(), pTEffect.getZ(), pTSprite.getSectnum());

                    pTEffect.setOwner(pTSprite.getOwner());
                    pTEffect.setPicnum(pTSprite.getPicnum());
                    pTEffect.setCstat(pTEffect.getCstat() | kSpriteTranslucent);
                    if (i < 2) pTEffect.setCstat(pTEffect.getCstat() | 0x202);
                    pTEffect.setShade((byte) ClipLow(pTSprite.getShade() - 16, -128));
                    pTEffect.setXrepeat(pTSprite.getXrepeat());
                    pTEffect.setYrepeat(pTSprite.getYrepeat());
                    pTEffect.setPicnum(pTSprite.getPicnum());
                }
                break;
            case kViewEffectPhase:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                GetSpriteExtents(pTSprite);
                pTEffect.setShade(26);
                pTEffect.setPal(0);
                pTEffect.setYrepeat(24);
                pTEffect.setPicnum(626);
                pTEffect.setZ(extents_zTop);
                pTEffect.setCstat(pTEffect.getCstat() | kSpriteTranslucent);
                pTEffect.setXrepeat(pTEffect.getYrepeat());
                break;
            case kViewEffectShowWeapon:
                if (!IsPlayerSprite(pTSprite))
                    throw new AssertException("pTSprite.type >= kDudePlayer1 && pTSprite.type <= kDudePlayer8");
                PLAYER pPlayer = gPlayer[pTSprite.getLotag() - kDudePlayer1];
                if (viewWeaponTile[pPlayer.currentWeapon] == -1) break;
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setX(pTSprite.getX());
                pTEffect.setY(pTSprite.getY());
                pTEffect.setZ(pTSprite.getZ() - 0x2000);
                pTEffect.setPicnum(viewWeaponTile[pPlayer.currentWeapon]);
                pTEffect.setShade(pTSprite.getShade());
                pTEffect.setXrepeat(32);
                pTEffect.setYrepeat(32);

//		        spriteext[pTEffect.owner].flags |= 1; //SPREXT_NOTMD

                break;
            case kViewEffectReflectiveBall:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setShade(26);
                pTEffect.setPal(0);
                pTEffect.setYrepeat(64);
                pTEffect.setPicnum(2089);
                pTEffect.setCstat(pTEffect.getCstat() | kSpriteTranslucent);
                pTEffect.setXrepeat(pTEffect.getYrepeat());
                break;
            case kViewEffectShoot:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setShade(-128);
                pTEffect.setPal(0);
                pTEffect.setYrepeat(64);
                pTEffect.setPicnum(2605);
                pTEffect.setXrepeat(pTEffect.getYrepeat());
                return pTEffect;
            case kViewEffectTesla:
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setZ(pTSprite.getZ());
                pTEffect.setShade(-128);
                pTEffect.setCstat(pTEffect.getCstat() | kSpriteTranslucent);
                pTEffect.setXrepeat(pTSprite.getXrepeat());
                pTEffect.setPicnum(2135);
                pTEffect.setYrepeat(pTSprite.getYrepeat());
                break;
            case kViewEffectFlag:
            case kViewEffectBigFlag:
                GetSpriteExtents(pTSprite);
                pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                pTEffect.setShade(-128);
                pTEffect.setPal(0);
                pTEffect.setZ(extents_zTop);
                if (nViewEffect == 16) pTEffect.setYrepeat(kViewEffectFlag);
                else pTEffect.setYrepeat(64);
                pTEffect.setXrepeat(pTEffect.getYrepeat());
                pTEffect.setPicnum(3558); // team flag
                return pTEffect;
            case kViewEffectAtom:
                for (int i = 0; i < 16; i++) {
                    pTEffect = viewInsertTSprite(pTSprite.getSectnum(), 0x7FFF, pTSprite);
                    int velocity = (int) (divscale(gFrameClock, 120, 11) + atomEffectVelocity[i].z);

                    int dx = mulscale(Cos(velocity), 512, 30);
                    int dy = mulscale(Sin(velocity), 512, 30);
                    dy = (int) RotateVector(dy, 0, (int) atomEffectVelocity[i].x).x;
                    int dz = (int) RotateVector(dx, (int) rotated.y, (int) atomEffectVelocity[i].y).y;
                    dx = (int) rotated.x;

                    pTEffect.setX(pTSprite.getX() + dx);
                    pTEffect.setY(pTSprite.getY() + dy);
                    pTEffect.setZ(pTSprite.getZ() + (dz << 4));
                    pTEffect.setPicnum(1720);
                    pTEffect.setShade(-128);
                }
                break;
        }
        return null;
    }

    public static void coords() {
        short y = 30;

        Renderer renderer = game.getRenderer();

        Font font = EngineUtils.getSmallFont();
        font.drawText(renderer, 32, y, toCharArray("X= ", Integer.toString(gMe.pSprite.getX())), 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        font.drawText(renderer, 32, y + 7, toCharArray("Y= ",  Integer.toString(gMe.pSprite.getY())), 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        font.drawText(renderer, 32, y + 14, toCharArray("Z= ",  Integer.toString(gMe.pSprite.getZ())), 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        font.drawText(renderer, 32, y + 21, toCharArray("A= ", Integer.toString(gMe.pSprite.getAng())), 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        font.drawText(renderer, 32, y + 28, toCharArray("SECT= ", Integer.toString(gMe.pSprite.getSectnum())), 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
    }

    private static void DrawStatSprite(int nTile, int x, int y, int nShade, int nPLU, int nFlag, int nScale) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(x << 16, y << 16, nScale, 0, (short) nTile, nShade, nPLU, nFlag | kRotateStatus, gViewX0, gViewY0, gViewX1, gViewY1);
    }
}

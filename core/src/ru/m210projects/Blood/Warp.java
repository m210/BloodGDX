// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood;

import ru.m210projects.Blood.Types.XSPRITE;
import ru.m210projects.Blood.Types.ZONE;
import ru.m210projects.Build.Board;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.exceptions.WarningException;

import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.*;
import static ru.m210projects.Build.Engine.MAXSECTORS;

public class Warp {
    public static ZONE[] gStartZone = new ZONE[kMaxPlayers];
    public static ZONE[] gStartZoneTeam1 = new ZONE[kMaxPlayers];
    public static ZONE[] gStartZoneTeam2 = new ZONE[kMaxPlayers];
    public static int[] gUpperLink = new int[MAXSECTORS], gLowerLink = new int[MAXSECTORS];

    public static boolean gTeamsSpawnUsed = false;
    public static long checkWx, checkWy, checkWz;
    public static short checkWs;

    public static void InitPlayerStartZones(Board board) throws WarningException {
        int nSprite;

        // clear link values
        Arrays.fill(gUpperLink, -1);
        Arrays.fill(gLowerLink, -1);

        List<Sprite> sprites = board.getSprites();
        int team1 = 0;
        int team2 = 0; // increment if team start positions specified.
        for (nSprite = 0; nSprite < sprites.size(); nSprite++) {
            Sprite pSprite = sprites.get(nSprite);

            if (pSprite.getStatnum() < kMaxStatus) {
                XSPRITE pXSprite = boardService.getXSprite(pSprite.getExtra());
                if (pXSprite != null) {
                    switch (pSprite.getLotag()) {
                        case kMarkerPlayerStart: {
                            if ((pGameInfo.nGameType < 2) && (pXSprite.getData1() >= 0 && pXSprite.getData1() < kMaxPlayers)) {
                                ZONE pZone = gStartZone[pXSprite.getData1()];
                                pZone.x = pSprite.getX();
                                pZone.y = pSprite.getY();
                                pZone.z = pSprite.getZ();
                                pZone.sector = pSprite.getSectnum();
                                pZone.angle = pSprite.getAng();
                            }
                            engine.deletesprite(nSprite);
                            break;
                        }

                        case kMarkerDeathStart:
                            if (pXSprite.getData1() >= 0 && pXSprite.getData1() < kMaxPlayers) {
                                if (pGameInfo.nGameType >= 2) {
                                    // default if BB or teams without data2 specified
                                    ZONE pZone = gStartZone[pXSprite.getData1()];
                                    pZone.x = pSprite.getX();
                                    pZone.y = pSprite.getY();
                                    pZone.z = pSprite.getZ();
                                    pZone.sector = pSprite.getSectnum();
                                    pZone.angle = pSprite.getAng();

                                    if (pGameInfo.nGameType == 3) {
                                        // team 1
                                        if (pXSprite.getData2() == 1) {
                                            pZone = gStartZoneTeam1[team1];
                                            pZone.x = pSprite.getX();
                                            pZone.y = pSprite.getY();
                                            pZone.z = pSprite.getZ();
                                            pZone.sector = pSprite.getSectnum();
                                            pZone.angle = pSprite.getAng();
                                            team1++;

                                            // team 2
                                        } else if (pXSprite.getData2() == 2) {
                                            pZone = gStartZoneTeam2[team2];
                                            pZone.x = pSprite.getX();
                                            pZone.y = pSprite.getY();
                                            pZone.z = pSprite.getZ();
                                            pZone.sector = pSprite.getSectnum();
                                            pZone.angle = pSprite.getAng();
                                            team2++;
                                        }
                                    }
                                }
                            }
                            engine.deletesprite(nSprite);
                            break;

                        case kMarkerUpperLink:
                            gUpperLink[pSprite.getSectnum()] = (short) nSprite;
                            pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                            pSprite.setCstat(pSprite.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                            break;

                        case kMarkerLowerLink:
                            gLowerLink[pSprite.getSectnum()] = (short) nSprite;
                            pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                            pSprite.setCstat(pSprite.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                            break;
                        case kMarkerUpperWater:
                        case kMarkerUpperStack:
                        case kMarkerUpperGoo:
                            gUpperLink[pSprite.getSectnum()] = (short) nSprite;
                            pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                            pSprite.setCstat(pSprite.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                            pSprite.setZ(engine.getflorzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY()));
                            break;
                        case kMarkerLowerWater:
                        case kMarkerLowerStack:
                        case kMarkerLowerGoo:
                            gLowerLink[pSprite.getSectnum()] = (short) nSprite;
                            pSprite.setCstat(pSprite.getCstat() | kSpriteInvisible);
                            pSprite.setCstat(pSprite.getCstat() & ~(kSpriteBlocking | kSpriteHitscan));
                            pSprite.setZ(engine.getceilzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY()));
                            break;
                    }
                }
            }
        }

        // check if there is enough start positions for teams, if any used
        if (team1 > 0 || team2 > 0) {
            gTeamsSpawnUsed = true;
            if (team1 < kMaxPlayers / 2 || team2 < kMaxPlayers / 2) {
                throw new WarningException("At least " + (kMaxPlayers / 2) + " spawn positions for each team is recommended.");
            }
        }

        // verify links have mates and connect them
        for (int nFrom = 0; nFrom < gUpperLink.length; nFrom++) {
            if (gUpperLink[nFrom] >= 0) {
                Sprite pFromSprite = sprites.get(gUpperLink[nFrom]);
                XSPRITE pXSprite = boardService.getXSprite(pFromSprite.getExtra());
                if (pXSprite == null) {
                    throw new AssertException("pXSprite != null");
                }

                int nID = pXSprite.getData1();
                for (int nTo = 0; nTo < gLowerLink.length; nTo++) {
                    if (gLowerLink[nTo] >= 0) {
                        Sprite pToSprite = sprites.get(gLowerLink[nTo]);
                        pXSprite = boardService.getXSprite(pToSprite.getExtra());
                        if (pXSprite == null) {
                            throw new AssertException("pXSprite != null");
                        }

                        if (pXSprite.getData1() == nID) {
                            pFromSprite.setOwner(gLowerLink[nTo]);
                            pToSprite.setOwner(gUpperLink[nFrom]);
                        }
                    }
                }
            }
        }
    }

    public static int checkWarping(Sprite pSprite) {
        int nUpper = gUpperLink[pSprite.getSectnum()];
        int nLower = gLowerLink[pSprite.getSectnum()];

        Sprite pUpper = boardService.getSprite(nUpper);
        if (pUpper != null) {
            int uz;
            if (pUpper.getLotag() != kMarkerUpperLink) {
                uz = engine.getflorzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY());
            } else {
                uz = pUpper.getZ();
            }

            if (uz <= pSprite.getZ()) {
                nLower = pUpper.getOwner();
                Sprite pLower = boardService.getSprite(nLower);
                if (pLower == null) {
                    return 0;
                }

                if (!boardService.isValidSector(pLower.getSectnum())) {
                    throw new AssertException("boardService.isValidSector(pLower.getSectnum())");
                }
                engine.changespritesect(pSprite.getXvel(), pLower.getSectnum());
                pSprite.setX(pSprite.getX() + (pLower.getX() - pUpper.getX()));
                pSprite.setY(pSprite.getY() + (pLower.getY() - pUpper.getY()));
                int lz;
                if (pLower.getLotag() == kMarkerLowerLink) {
                    lz = pLower.getZ();
                } else {
                    lz = engine.getceilzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY());
                }

                pSprite.setZ(pSprite.getZ() + (lz - uz));
                game.pInt.clearspriteinterpolate(pSprite.getXvel());

                return pUpper.getLotag();
            }
        }

        Sprite pLower = boardService.getSprite(nLower);
        if (pLower != null) {
            int lz;
            if (pLower.getLotag() != kMarkerLowerLink) {
                lz = engine.getceilzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY());
            } else {
                lz = pLower.getZ();
            }

            if (lz >= pSprite.getZ()) {
                nUpper = pLower.getOwner();
                pUpper = boardService.getSprite(nUpper);
                if (pUpper == null) {
                    return 0;
                }
                if (!boardService.isValidSector(pUpper.getSectnum())) {
                    throw new AssertException("boardService.isValidSector(pUpper.getSectnum())");
                }
                engine.changespritesect(pSprite.getXvel(), pUpper.getSectnum());
                pSprite.setX(pSprite.getX() + (pUpper.getX() - pLower.getX()));
                pSprite.setY(pSprite.getY() + (pUpper.getY() - pLower.getY()));

                int uz;
                if (pUpper.getLotag() == kMarkerUpperLink) {
                    uz = pUpper.getZ();
                } else {
                    uz = engine.getflorzofslope(pSprite.getSectnum(), pSprite.getX(), pSprite.getY());
                }
                pSprite.setZ(pSprite.getZ() + (uz - lz));
                game.pInt.clearspriteinterpolate(pSprite.getXvel());

                return pLower.getLotag();
            }
        }
        return 0;
    }

    public static void checkWarping(long x, long y, long z, int nSector) {
        if (nSector != -1) {
            int nUpper = gUpperLink[nSector];
            int nLower = gLowerLink[nSector];
            Sprite pUpper = boardService.getSprite(nUpper);
            if (pUpper != null) {
                int uz;
                if (pUpper.getLotag() != kMarkerUpperLink) {
                    uz = engine.getflorzofslope((short) nSector, (int) x, (int) y);
                } else {
                    uz = pUpper.getZ();
                }

                if (uz <= z) {
                    Sprite pLower = boardService.getSprite(nLower);
                    if (pLower == null) {
                        return;
                    }

                    if (!boardService.isValidSector(pLower.getSectnum())) {
                        throw new AssertException("boardService.isValidSector(pLower.getSectnum())");
                    }
                    nSector = pUpper.getSectnum();
                    x += pLower.getX() - pUpper.getX();
                    y += pLower.getY() - pUpper.getY();
                    int lz;
                    if (pLower.getLotag() == kMarkerLowerLink) {
                        lz = pLower.getZ();
                    } else {
                        lz = engine.getceilzofslope((short) nSector, (int) x, (int) y);
                    }
                    z += lz - uz;

                    checkWx = x;
                    checkWy = y;
                    checkWz = z;
                    checkWs = (short) nSector;

                    return;
                }
            }

            Sprite pLower = boardService.getSprite(nLower);
            if (pLower != null) {
                int lz;
                if (pLower.getLotag() != kMarkerLowerLink) {
                    lz = engine.getceilzofslope((short) nSector, (int) x, (int) y);
                } else {
                    lz = pLower.getZ();
                }

                if (lz >= z) {
                    nUpper = pLower.getOwner();
                    pUpper = boardService.getSprite(nUpper);
                    if (pUpper == null) {
                        return;
                    }

                    if (!boardService.isValidSector(pUpper.getSectnum())) {
                        throw new AssertException("boardService.isValidSector(pUpper.getSectnum())");
                    }
                    nSector = pUpper.getSectnum();
                    x += pUpper.getX() - pLower.getX();
                    y += pUpper.getY() - pLower.getY();

                    int uz;
                    if (pUpper.getLotag() == kMarkerUpperLink) {
                        uz = pUpper.getZ();
                    } else {
                        uz = engine.getflorzofslope((short) nSector, (int) x, (int) y);
                    }
                    z += uz - lz;

                    checkWx = x;
                    checkWy = y;
                    checkWz = z;
                    checkWs = (short) nSector;

//				    int nMirror = boardService.getSector(nSector).floorpicnum;
//				    if ( (gotpic[nMirror >> 3] & pow2char[nMirror & 7]) == 0 )
//				    {
//				    	gotpic[Mirror.MIRRORLABEL >> 3] = 0;
//				    	gotpic[(Mirror.MIRRORLABEL >> 3) + 1] = 0;
//				    	engine.setgotpic(nMirror);
//				    }

                    return;
                }
            }
        }

        checkWx = x;
        checkWy = y;
        checkWz = z;
        checkWs = (short) nSector;

//	    if(nSector != -1 && gLowerLink[nSector] != -1) { // 12.12.2018 I forgot why I need this tweak. Maybe just an old code?
//		    int nMirror1 = boardService.getSector(nSector).ceilingpicnum;
//		    if ( (gotpic[nMirror1 >> 3] & pow2char[nMirror1 & 7]) == 0 )
//		    {
//		    	gotpic[Mirror.MIRRORLABEL >> 3] = 0;
//		    	gotpic[(Mirror.MIRRORLABEL >> 3) + 1] = 0;
//		    	engine.setgotpic(nMirror1);
//		    }
//	    }

    }
}

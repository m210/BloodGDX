package ru.m210projects.Blood.filehandlers;

import ru.m210projects.Blood.Main;
import ru.m210projects.Build.Pattern.SaveReader;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static ru.m210projects.Blood.Globals.pGameInfo;
import static ru.m210projects.Blood.LEVELS.currentEpisode;
import static ru.m210projects.Blood.Main.mUserFlag;

public class BloodSaveHeader extends SaveReader.SaveHeader {

    public int skill;
    public int episode;
    public int level;

    public boolean gUserEpisode;
    public String addonFileName;
    public String addonPackedIniName;

    public BloodSaveHeader() {
        signature = "BLUD";
        version = 304;
    }

    @Override
    public BloodSaveHeader readObject(InputStream is) throws IOException {
        super.readObject(is);

        skill = StreamUtils.readByte(is) + 1;
        episode = StreamUtils.readInt(is) + 1;
        level = StreamUtils.readInt(is) + 1;

        gUserEpisode = StreamUtils.readBoolean(is);
        if (gUserEpisode) {
            boolean isPacked = StreamUtils.readBoolean(is);
            addonFileName = StreamUtils.readDataString(is);
            if (isPacked) {
                addonPackedIniName = StreamUtils.readDataString(is);
            }
        }

        return this;
    }

    @Override
    public BloodSaveHeader writeObject(OutputStream os) throws IOException {
        super.writeObject(os);

        StreamUtils.writeByte(os, pGameInfo.nDifficulty);
        StreamUtils.writeInt(os, pGameInfo.nEpisode);
        StreamUtils.writeInt(os, pGameInfo.nLevel);

        StreamUtils.writeBoolean(os, mUserFlag == Main.UserFlag.Addon);
        if (mUserFlag == Main.UserFlag.Addon) {
            addonFileName = "";
            addonPackedIniName = "";

            if (currentEpisode != null && currentEpisode.iniFile != null) {
                EpisodeEntry episodeEntry = currentEpisode.iniFile.getEpisodeEntry();
                addonFileName = episodeEntry.getFileEntry().getRelativePath().toString();
                if(episodeEntry.isPackageEpisode()) {
                    addonPackedIniName = episodeEntry.getIniFile().getName();
                }
            }

            StreamUtils.writeBoolean(os, !addonPackedIniName.isEmpty());
            StreamUtils.writeDataString(os, addonFileName);
            if (!addonPackedIniName.isEmpty()) {
                StreamUtils.writeDataString(os, addonPackedIniName);
            }
        }

        return this;
    }
}

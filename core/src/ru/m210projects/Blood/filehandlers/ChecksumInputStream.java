package ru.m210projects.Blood.filehandlers;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.Checksum;

public class ChecksumInputStream extends InputStream {

    private final Checksum checksum;
    private final InputStream parent;

    public ChecksumInputStream(InputStream parent) {
        this.parent = parent;
        this.checksum = new java.util.zip.CRC32();
    }

    @Override
    public synchronized int read() throws IOException {
        int data = parent.read();
        if (data != -1) {
            checksum.update(data);
            return data & 0xFF;
        }
        return -1;
    }

    @Override
    public synchronized int read(byte[] b, int off, int len) throws IOException {
        int n = parent.read(b, off, len);
        if (n != -1) {
            checksum.update(b, off, len);
        }
        return n;
    }

    @Override
    public synchronized long skip(long n) throws IOException {
        byte[] skip = new byte[(int) n];
        long result = parent.read(skip);
        checksum.update(skip, 0, skip.length);
        return result;
    }

    @Override
    public int available() throws IOException {
        return parent.available();
    }

    @Override
    public void close() throws IOException {
        parent.close();
    }

    public long getChecksum() {
        return checksum.getValue();
    }

}

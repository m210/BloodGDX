package ru.m210projects.Blood.filehandlers;

import java.io.IOException;
import java.io.InputStream;

public class CryptedInputStream extends InputStream {

    private final int cryptLength;
    private final InputStream parent;
    private long cryptKey;
    private int pos = 0;

    public CryptedInputStream(InputStream parent, long cryptKey, int cryptLength) {
        this.parent = parent;
        this.cryptKey = cryptKey;
        this.cryptLength = cryptLength;
    }

    protected long calcCryptKey() {
        return cryptKey++;
    }

    @Override
    public synchronized int read() throws IOException {
        int data = parent.read();
        if (data != -1) {
            if (pos < cryptLength) {
                data ^= calcCryptKey();
            }
            pos++;
            return data & 0xFF;
        }
        return -1;
    }

    @Override
    public synchronized int read(byte[] b, int off, int len) throws IOException {
        int n = parent.read(b, off, len);
        if (n != -1) {
            if (pos < cryptLength) {
                encrypt(b, off, Math.min(cryptLength - pos, n));
            }
            pos += n;
        }
        return n;
    }

    @Override
    public synchronized long skip(long n) throws IOException {
        long result = 0;
        do {
            long len = parent.skip(n);
            pos += len;
            cryptKey += len;
            n -= len;
            result += len;
        } while (n > 0);
        return result;
    }

    @Override
    public int available() throws IOException {
        return parent.available();
    }

    @Override
    public void close() throws IOException {
        parent.close();
    }

    protected void encrypt(byte[] buffer, int offs, int size) {
        for (int i = 0; i < size; i++) {
            buffer[offs + i] ^= calcCryptKey();
        }
    }
}

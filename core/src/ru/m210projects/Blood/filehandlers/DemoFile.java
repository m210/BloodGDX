// This file is part of BloodGDX.
// Copyright (C) 2017-2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.filehandlers;

import ru.m210projects.Blood.Types.GAMEINFO;
import ru.m210projects.Blood.Types.INPUT;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

import static ru.m210projects.Blood.Globals.kMaxPlayers;
import static ru.m210projects.Blood.Globals.pGameInfo;
import static ru.m210projects.Blood.Main.game;

public class DemoFile {

    public static final String kBloodDemSig = "DEM\u001A";

    public int nInputCount;               // number of INPUT structures recorded
    public int nNetPlayers;            // number of players recorded
    public short nMyConnectIndex;        // the ID of the player
    public short nConnectHead;           // index into connectPoints of Player
    public short[] connectPoints = new short[kMaxPlayers];      // IDs of players
    public String signature;
    public int nVersion;
    public int rcnt;
    public INPUT[][] pDemoInput; // = new INPUT[16384][kMaxPlayers];
    private GAMEINFO gameInfo;

    public DemoFile(InputStream is) throws IOException {
        signature = StreamUtils.readString(is, 4);
        nVersion = StreamUtils.readShort(is);

        if (nVersion == 277) {
            StreamUtils.skip(is, 4); //int nBuild - for demos > 1.10
        }

		/*
		   // build of Blood that created the demo
           // builds are:
           // SW, SWCD:   2
           // Registered: 3
           // Plasma Pak: 4
		*/

        nInputCount = StreamUtils.readInt(is);
        nNetPlayers = StreamUtils.readInt(is);
        nMyConnectIndex = StreamUtils.readShort(is);
        nConnectHead = StreamUtils.readShort(is);
        for (int i = 0; i < 8; i++)
            connectPoints[i] = StreamUtils.readShort(is);

        //GameInfo
        gameInfo = new GAMEINFO();
        gameInfo.nGameType = StreamUtils.readUnsignedByte(is);
        gameInfo.nDifficulty = StreamUtils.readUnsignedByte(is);
        gameInfo.nEpisode = StreamUtils.readInt(is);
        gameInfo.nLevel = StreamUtils.readInt(is);

        String name = StreamUtils.readString(is, 144);

//        if (FileUtils.isExtension(name, "map")) name = name.substring(0, name.lastIndexOf('.'));

        gameInfo.zLevelName = game.getCache().getEntry(name, true);
        gameInfo.zLevelSong = StreamUtils.readString(is, 144);
        gameInfo.nTrackNumber = StreamUtils.readInt(is);
        for (int i = 0; i < 4; i++)
            StreamUtils.readInt(is); //szSaveGameName
        for (int i = 0; i < 4; i++)
            StreamUtils.readInt(is); //szUserGameName
        StreamUtils.readShort(is); //nSaveGameSlot
        StreamUtils.readInt(is); //picEntry
        gameInfo.uMapCRC = StreamUtils.readInt(is); //uMapCRC

        gameInfo.nMonsterSettings = StreamUtils.readUnsignedByte(is);
        gameInfo.uGameFlags = StreamUtils.readInt(is);
        gameInfo.uNetGameFlags = StreamUtils.readInt(is);
        gameInfo.nWeaponSettings = StreamUtils.readUnsignedByte(is);
        gameInfo.nItemSettings = StreamUtils.readUnsignedByte(is);
        gameInfo.nRespawnSettings = StreamUtils.readUnsignedByte(is);
        gameInfo.nTeamSettings = StreamUtils.readUnsignedByte(is);
        gameInfo.nMonsterRespawnTime = StreamUtils.readInt(is);
        gameInfo.nWeaponRespawnTime = StreamUtils.readInt(is);
        gameInfo.nItemRespawnTime = StreamUtils.readInt(is);
        gameInfo.nSpecialRespawnTime = StreamUtils.readInt(is);
        gameInfo.nEnemyQuantity = pGameInfo.nDifficulty;
        gameInfo.nEnemyDamage = pGameInfo.nDifficulty;
        gameInfo.nPitchforkOnly = false;

        pDemoInput = new INPUT[nInputCount][kMaxPlayers];

        for (int rcnt = 0; rcnt < nInputCount; rcnt++) {
            for (int i = nConnectHead; i >= 0; i = connectPoints[i]) {
                if (is.available() < 22) return;
                pDemoInput[rcnt][i] = new INPUT(is, nVersion);
            }
        }

        rcnt = 0;
        pGameInfo.copy(gameInfo);
    }

    public GAMEINFO getGameInfo() {
        return gameInfo;
    }
}

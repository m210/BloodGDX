package ru.m210projects.Blood.filehandlers;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.Group;
import ru.m210projects.Build.filehandle.fs.FileEntry;

import java.util.Locale;

import static ru.m210projects.Blood.Main.game;

public interface EpisodeEntry {

    String getHashKey();

    FileEntry getFileEntry(); // ini file or group (parent group) when ini file placed

    Entry getIniFile();

    Group getGroup();

    boolean isPackageEpisode();

    class Pack extends FileEntry implements EpisodeEntry {

        private final Entry iniFile;
        private final FileEntry groupFile;

        public Pack(FileEntry group, Entry iniFile) {
            super(group.getPath(), String.format("%s:%s", group.getName(), iniFile.getName()), group.getSize());
            this.iniFile = iniFile;
            this.groupFile = group;
            this.setParent(group.getParent());
        }

        @Override
        public String getHashKey() {
            return String.format("%s:%s", groupFile.getRelativePath().toString(), iniFile.getName()).toUpperCase(Locale.ROOT);
        }

        @Override
        public FileEntry getFileEntry() {
            return groupFile;
        }

        public Entry getIniFile() {
            return iniFile;
        }

        @Override
        public Group getGroup() {
            return game.getCache().newGroup(groupFile);
        }

        @Override
        public boolean isPackageEpisode() {
            return true;
        }

        @Override
        public String getExtension() {
            return groupFile.getExtension();
        }
    }

    class File extends FileEntry implements EpisodeEntry {

        public File(FileEntry fileEntry) {
            super(fileEntry);
        }

        @Override
        public String getHashKey() {
            return getRelativePath().toString().toUpperCase(Locale.ROOT);
        }

        @Override
        public FileEntry getFileEntry() {
            return this;
        }

        @Override
        public Entry getIniFile() {
            return this;
        }

        @Override
        public Group getGroup() {
            return getParent();
        }

        @Override
        public boolean isPackageEpisode() {
            return false;
        }
    }

}

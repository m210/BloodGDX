package ru.m210projects.Blood.filehandlers;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.rff.RffEntry;
import ru.m210projects.Build.filehandle.zip.ZipEntry;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

public class UserEntry extends RffEntry {

    private final Entry entry;

    public UserEntry(Entry entry, int id) {
        super(null, id, 0, (int) entry.getSize(), (int) entry.getSize(), LocalDateTime.now(), 0, FileUtils.getNameWithoutExtension(FileUtils.getPath(entry.getName()).getFileName().toString()), entry.getExtension());
        this.entry = entry;
        setParent(entry.getParent());
        if (entry instanceof ZipEntry && !entry.isDirectory()) {
            ((ZipEntry) entry).load();
        }
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return entry.getInputStream();
    }
}

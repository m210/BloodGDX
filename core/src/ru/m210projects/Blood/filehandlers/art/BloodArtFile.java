package ru.m210projects.Blood.filehandlers.art;

import ru.m210projects.Build.filehandle.InputStreamProvider;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.ArtFile;

public class BloodArtFile extends ArtFile {
    public BloodArtFile(String name, InputStreamProvider provider) {
        super(name, provider);
    }

    protected ArtEntry createArtEntry(InputStreamProvider provider, int num, int offset, int width, int height, int flags) {
        return new BloodArtEntry(provider, num, offset, width, height, flags);
    }
}

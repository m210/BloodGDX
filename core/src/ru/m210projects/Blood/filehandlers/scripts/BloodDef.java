// This file is part of BloodGDX.
// Copyright (C) 2017-2020 Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// BloodGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// BloodGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with BloodGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Blood.filehandlers.scripts;

import ru.m210projects.Blood.filehandlers.UserEntry;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Script.DefScript;
import ru.m210projects.Build.Script.Scriptfile;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.filehandle.rff.RffFile;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.m210projects.Blood.Globals.*;
import static ru.m210projects.Blood.Main.boardService;
import static ru.m210projects.Blood.Main.game;
import static ru.m210projects.Blood.Tile.kSurfMax;
import static ru.m210projects.Build.Engine.MAXTILES;
import static ru.m210projects.Build.Gameutils.BClipRange;
import static ru.m210projects.Build.Strhandler.toLowerCase;
import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.HIGHEST;
import static ru.m210projects.Build.filehandle.CacheResourceMap.CachePriority.NORMAL;

public class BloodDef extends DefScript {

//	public static class BloodMapHackInfo extends MapHackInfo {
//
//		public BloodMapHackInfo() {
//			super();
//		}
//
//		public BloodMapHackInfo(MapHackInfo src) {
//			super(src);
//		}
//
//		@Override
//		public boolean load(String mapname) {
//			unload();
//			if(hasMaphack(mapname)) {
//				if(load(mapname, pGameInfo.uMapCRC))
//					return true;
//			}
//
//			return false;
//		}
//	}

    private final DefineIdToken rffid = new DefineIdToken();
    private final NBloodDefineIdToken nbrffid = new NBloodDefineIdToken();
    private final TileFromTextureToken tft = new BloodTileFromTextureToken();
//    private final Map<String, Integer> fileids = new HashMap<String, Integer>();
    public byte[] surfType = new byte[kMaxTiles];
    private final RffFile userDef;

    public BloodDef(Engine engine) {
        super(engine);

        this.addToken("definefileid", rffid);
        this.addToken("rffdefineid", nbrffid);
        this.addToken("tilefromtexture", tft);

        surfaceInit("SURFACE.DAT");
        userDef = new RffFile("UserDef"); //game.getCache().add("UserDef", disposable);
    }

    public BloodDef(BloodDef src, Entry group) {
        super(src, group);

        this.addToken("definefileid", rffid);
        this.addToken("rffdefineid", nbrffid);
        this.addToken("tilefromtexture", tft);

        System.arraycopy(src.surfType, 0, surfType, 0, src.surfType.length);

        userDef = new RffFile("UserDef");
    }

    public void surfaceInit(String name) {
        Entry data = game.getCache().getEntry(name, true);
        if (!data.exists())
            return;

        byte[] surfs = data.getBytes();
        System.arraycopy(surfs, 0, this.surfType, 0, Math.min(surfs.length, surfType.length));
    }

    @Override
    public void apply() {
        List<String> defs;
        if (addonsIncludes != null && currentAddon != null
                && (defs = addonsIncludes.get(currentAddon.getName())) != null) {

            for (int i = 0; i < defs.size() / 2; i++) {
                String fn = defs.get(2 * i + 1);
                Entry res = game.getCache().getEntry(fn, true);
                if (!res.exists()) {
                    Console.out.println("Warning: Failed including " + fn + " as module", OsdColor.RED);
                    continue;
                }

                Scriptfile included = new Scriptfile(fn, res);
                included.path = defs.get(2 * i);

                defsparser(included);
            }
        }

        game.getCache().addGroup(userDef, disposable ? HIGHEST : NORMAL);

        for (int i = 0; i < MAXTILES; i++) {
            if (tiles[i] == null)
                continue;

            DefTile tile = tiles[i];
            ArtEntry pic = engine.getTile(i);

            if (tile.crc32 != 0) {
                long crc32 = pic.getChecksum();
                if (crc32 != tile.crc32) {
                    boolean found = false;
                    while (tile.next != null) {
                        tile = tile.next;
                        if (tile.crc32 == 0 || crc32 == tile.crc32) {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                        continue;
                }
            }

            if (tile.waloff == null)
                continue;

            DynamicArtEntry newPic = engine.allocatepermanenttile(i, tile.sizx, tile.sizy);
            newPic.copyData(tile.waloff);

            int flags = pic.getFlags();
            flags &= ~0x70FFFF00;
            flags |= (tile.optional & 0x07) << 28;
            newPic.setFlags(flags);
            newPic.setOffset(tile.xoffset, tile.yoffset);

            // replace hrp info
            texInfo.addTexture(i, 0, tile.hrp, (0xFF - (tile.alphacut & 0xFF)) * (1.0f / 255.0f), 1.0f, 1.0f, 1.0f,
                    1.0f, 0);
        }
    }

    public byte GetSurfType(int nHit) {
        int nHitType = nHit & kHitTypeMask;
        int nHitIndex = nHit & kHitIndexMask;

        switch (nHitType) {
            case kHitFloor:
                return surfType[boardService.getSector(nHitIndex).getFloorpicnum()];

            case kHitCeiling:
                return surfType[boardService.getSector(nHitIndex).getCeilingpicnum()];

            case kHitWall:
                return surfType[boardService.getWall(nHitIndex).getPicnum()];

            case kHitSprite:
                return surfType[boardService.getSprite(nHitIndex).getPicnum()];
        }
        return 0;
    }

    @Override
    public void dispose() {
        super.dispose();
        if (disposable) {
            game.getCache().removeGroup(userDef);
        }
    }

    public enum TileTextureTokens {
        FILE, ALPHACUT, XOFFSET, YOFFSET, TEXTURE, CRC, EXTRA, SURF
    }

    public class DefineIdToken implements Token {
        @Override
        public BaseToken parse(Scriptfile script) {
            String name;
            Integer fileId;

            if ((name = getFile(script)) == null)
                return BaseToken.Error;

            Entry entry = game.getCache().getEntry(name, false);
            if (!entry.exists()) {
                entry = game.getCache().getEntry(name, true);
                if (!entry.exists()) { // search as external file
                    Console.out.println("DefineId error: file " + name + " not found!", OsdColor.RED);
                    return BaseToken.Error;
                }
            }

            if ((fileId = script.getsymbol()) == null)
                return BaseToken.Error;

//            fileids.put(name, fileId);
            userDef.addEntry(new UserEntry(entry, fileId));

            return BaseToken.Ok;
        }
    }

    public class NBloodDefineIdToken implements Token {
        @Override
        public BaseToken parse(Scriptfile script) {
            String name;
            String ext;
            Integer fileId;

            if ((name = getFile(script)) == null)
                return BaseToken.Error;

            if ((ext = script.getstring()) == null)
                return BaseToken.Error;

            String filename = name + "." + ext;
            Entry entry = game.getCache().getEntry(filename, false);
            if (!entry.exists()) {
                entry = game.getCache().getEntry(filename, true);
                if (!entry.exists()) { // search as external file
                    Console.out.println("DefineId error: file " + filename + " not found!", OsdColor.RED);
                    return BaseToken.Error;
                }
            }

            if ((fileId = script.getsymbol()) == null)
                return BaseToken.Error;

            script.getstring();
            userDef.addEntry(new UserEntry(entry, fileId));

            return BaseToken.Ok;
        }
    }

    private class BloodTileFromTextureToken extends TileFromTextureToken {
        protected final Map<String, TileTextureTokens> tilefromtexturetokens = new HashMap<String, TileTextureTokens>() {
            private static final long serialVersionUID = 1L;

            {
                put("file", TileTextureTokens.FILE);
                put("name", TileTextureTokens.FILE);
                put("alphacut", TileTextureTokens.ALPHACUT);
                put("xoffset", TileTextureTokens.XOFFSET);
                put("xoff", TileTextureTokens.XOFFSET);
                put("yoffset", TileTextureTokens.YOFFSET);
                put("yoff", TileTextureTokens.YOFFSET);
                put("texture", TileTextureTokens.TEXTURE);
                put("ifcrc", TileTextureTokens.CRC);
                put("extra", TileTextureTokens.EXTRA);
                put("surface", TileTextureTokens.SURF);
            }
        };

        @Override
        public BaseToken parse(Scriptfile script) {
            int ttexturetokptr = script.ltextptr, ttextureend;
            String fn = null;
            Integer tile = -1, value;
            int talphacut = 255;
            Byte xoffset = null, yoffset = null;
            long tilecrc = 0;
            boolean istexture = false;
            Integer extra = null, surf = null;

            if ((tile = script.getsymbol()) == null)
                return BaseToken.Error;
            if ((ttextureend = script.getbraces()) == -1)
                return BaseToken.Error;

            while (script.textptr < ttextureend) {
                Object tk = gettoken(script, tilefromtexturetokens);
                if (tk instanceof BaseToken) {
                    int line = script.getlinum(script.ltextptr);
                    Console.out.println(
                            script.filename + " has unknown token \""
                                    + toLowerCase(script.textbuf.substring(script.ltextptr, script.textptr))
                                    + "\" on line: "
                                    + toLowerCase(
                                    script.textbuf.substring(getPtr(script, line), getPtr(script, line + 1))),
                            OsdColor.RED);
                    continue;
                }

                switch ((TileTextureTokens) tk) {
                    case EXTRA:
                        extra = script.getsymbol();
                        break;
                    case FILE:
                        fn = getFile(script);
                        break;
                    case ALPHACUT:
                        value = script.getsymbol();
                        if (value != null)
                            talphacut = value;
                        talphacut = BClipRange(talphacut, 0, 255);
                        break;
                    case XOFFSET:
                        String xoffs = script.getstring();
                        if (xoffs.equalsIgnoreCase("ART"))
                            xoffset = engine.getTile(tile).getOffsetX();
                        else {
                            try {
                                xoffset = Byte.parseByte(xoffs);
                            } catch (Exception e) {
                                Console.out.println("Xoffset value out of range. Value: \"" + xoffs + "\" was disabled.",
                                        OsdColor.RED);
                                break;
                            }
                        }
                        break;
                    case YOFFSET:
                        String yoffs = script.getstring();
                        if (yoffs.equalsIgnoreCase("ART"))
                            yoffset = engine.getTile(tile).getOffsetY();
                        else {
                            try {
                                yoffset = Byte.parseByte(yoffs);
                            } catch (Exception e) {
                                Console.out.println("Yoffset value out of range. Value: \"" + yoffs + "\" was disabled.",
                                        OsdColor.RED);
                                break;
                            }
                        }
                        break;
                    case TEXTURE:
                        istexture = true;
                        break;
                    case SURF:
                        value = script.getsymbol();
                        if (value != null)
                            surf = BClipRange(value, 0, 255);
                        break;
                    case CRC:
                        tilecrc = script.getsymbol() & 0xFFFFFFFFL;
                        break;
                }
            }

            DefTile dt;
            if ((dt = addTile(script, fn, tile, xoffset, yoffset, tilecrc, talphacut, istexture,
                    ttexturetokptr)) != null) {
                if (extra != null)
                    dt.optional = extra;
                if (surf != null) {
                    byte s = (byte) ((int) surf);
                    if (s >= kSurfMax) {
                        Console.out.println("Warning! surfaceType out of range: " + s + " resetting...",
                                OsdColor.RED);
                        s = 0;
                    }
                    surfType[tile] = s;
                }
                return BaseToken.Ok;
            }
            return BaseToken.Error;
        }
    }
}

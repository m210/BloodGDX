package ru.m210projects.Blood.desktop;

import com.badlogic.gdx.backends.LwjglLauncherUtil;
import com.badlogic.gdx.backends.lwjgl3.audio.OpenALAudio;
import ru.m210projects.Blood.Config;
import ru.m210projects.Blood.Main;
import ru.m210projects.Build.Architecture.common.audio.AudioDriver;
import ru.m210projects.Build.Architecture.common.audio.BuildAudio;
import ru.m210projects.Build.settings.GameConfig;

import javax.swing.*;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Locale;

public class DesktopLauncher {

    public static final String appname = "BloodGDX";

    public static void main(final String[] arg) throws IOException {
        Path gamePath;
        if (arg.length == 0) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Choose " + appname + " game directory");
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int result = fileChooser.showOpenDialog(null);
            if (result == JFileChooser.APPROVE_OPTION) {
                gamePath = Paths.get(fileChooser.getSelectedFile().getAbsolutePath(), (appname + ".ini").toLowerCase(Locale.ROOT));
            } else {
                System.err.println("Choose " + appname + " game directory");
                return;
            }
        } else {
            gamePath = Paths.get(arg[0], (appname + ".ini").toLowerCase(Locale.ROOT));
        }

        GameConfig cfg = new Config(gamePath);
        cfg.load();
        cfg.setGamePath(cfg.getCfgPath().getParent());
        cfg.registerAudioDriver(AudioDriver.DUMMY_AUDIO, new BuildAudio.DummyAudio(cfg));
        cfg.registerAudioDriver(AudioDriver.OPENAL_AUDIO, new OpenALAudio(cfg));
        cfg.addMidiDevices(LwjglLauncherUtil.getMidiDevices());
        LwjglLauncherUtil.launch(new Main(Collections.singletonList("game arguments"), cfg, appname, "?.??", false, false), null);
    }
}
